<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slots</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>

    <div class="wrapper">
        <a href="/games/elGallo" class="slot">
            <img src="images/slot-1.png" alt="Slot 1" />
            <p>el Gallo</p>
        </a>
        <a href="/games/lifeOfLuxury" class="slot">
            <img src="images/slot-2.png" alt="Slot 2" />
            <p>Life of Luxury</p>
        </a>
        <a href="/games/superballKeno" class="slot">
            <img src="images/slot-3.png" alt="Slot 3" />
            <p>Superball Keno</p>
        </a>
        <a href="/games/superDoubleUp" class="slot">
            <img src="images/slot-4.png" alt="Slot 4" />
            <p>Super Double Up</p>
        </a>
    </div>

    <script src="/js/autobahn.js"></script>
    <script>
        // var conn = new ab.connect('ws://localhost:8082', function (session) {
        //     session.subscribe('onNewData', function (topic, data) {
        //         console.log('New data: topic_id: ' + topic);
        //         console.log(data.data);
        //     })
        // },
        // function (code, reason, detail) {
        //     console.log('Websocket connection closed: code=' + code + '; reason=' + reason + '; detail=' + detail);
        // },
        // {
        //     'maxRetries': 60,
        //     'retryDelay': 4000,
        //     'skipSubprotocolCheck': true
        // });

        // var conn = new ab.Session('wss://game.play777games.com/wss/',

        //     function() {
        //         conn.subscribe('onJackpotNewWinner', function(topic, data) {
        //             console.log(data.data);
        //         });
        //     },
        //     function() {
        //         console.warn('WebSocket connection closed');
        //     },
        //     {'skipSubprotocolCheck': true}
        // );

        var conn = new ab.Session('wss://game.play777games.com/wss/',
            function() {
                conn.subscribe('onUpUserBalance', function(topic, data) {
                    console.log(data.data);  // баланс приходит от сервера в случае если он слат != 0
                });
            },
            function() {
                console.warn('WebSocket connection closed');
            },
            {'skipSubprotocolCheck': true}
        );
    </script>
</body>
</html>
