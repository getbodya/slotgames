<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../../css/test.css">
</head>
<body>
    <nav class="navbar clearfix">
        <div class="container">
            <ul class="nav">
                <li><a href="/admin">Dashboard</a></li>
                <li><a href="/admin/simulations">Simulations</a></li>
                <li><a href="/admin/statistics">Statistic</a></li>
                <li><a href="/admin/simelation/lol">Life of Luxury</a></li>
                <li><a href="/admin/simelation/elGallo">Loteria</a></li>
                <li><a href="/admin/simelation/superkeno">Superball Keno</a></li>
                <li><a href="/admin/simelation/doublekeno">Double-Up Keno</a></li>
            </ul>
        </div>
    </nav>
<div class="main">
    <div class="container">
        <div class="row">
            <h1>Set percentages for EG</h1>

            <div class="col-6">
                <div class="work_space">
                    <form action="/admin/simelation/elGallo/set-percentages"
                       method="POST">
                        {{ csrf_field() }}

                        <h2>Cell percantage for main game for bet 50:</h2>
                        @for ($i = 0; $i < 5; $i++)
                            <h3>b{{$i}}:</h3>
                            @for ($k = 0; $k < 12; $k++)
                                <div class="row_block">
                                    <div class="left_side">
                                        @if($k === 0)
                                            <label>Diablo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 1)
                                            <label>Nopal ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 2)
                                            <label>Gallo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 3)
                                            <label>Borracho ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 4)
                                            <label>Sirena ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 5)
                                            <label>Corazon ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 6)
                                            <label>Muerte ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 7)
                                            <label>Alacran ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 8)
                                            <label>Sol ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 9)
                                            <label>Mundo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 10)
                                            <label>Chalupa ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 11)
                                            <label>Calavera ({{$k}}):</label>
                                            <br>
                                        @endif

                                    </div>
                                    <div class="right_side">
                                        <input type="text" name="cell{{$k}}_b{{$i}}" value="{{$percentagesMainGame50[$i][$k]}}">
                                    </div>
                                </div>
                            @endfor
                        @endfor

                        <h2>Cell percantage for main game for bet 25:</h2>
                        @for ($i = 0; $i < 5; $i++)
                            <h3>b{{$i}}:</h3>
                            @for ($k = 0; $k < 12; $k++)
                                <div class="row_block">
                                    <div class="left_side">
                                        @if($k === 0)
                                            <label>Diablo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 1)
                                            <label>Nopal ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 2)
                                            <label>Gallo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 3)
                                            <label>Borracho ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 4)
                                            <label>Sirena ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 5)
                                            <label>Corazon ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 6)
                                            <label>Muerte ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 7)
                                            <label>Alacran ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 8)
                                            <label>Sol ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 9)
                                            <label>Mundo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 10)
                                            <label>Chalupa ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 11)
                                            <label>Calavera ({{$k}}):</label>
                                            <br>
                                        @endif

                                    </div>
                                    <div class="right_side">
                                        <input type="text" name="cell{{$k}}_b{{$i}}" value="{{$percentagesMainGame25[$i][$k]}}">
                                    </div>
                                </div>
                            @endfor
                        @endfor

                        <h2>Cell percantage for main game for bet 1:</h2>
                        @for ($i = 0; $i < 5; $i++)
                            <h3>b{{$i}}:</h3>
                            @for ($k = 0; $k < 12; $k++)
                                <div class="row_block">
                                    <div class="left_side">
                                        @if($k === 0)
                                            <label>Diablo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 1)
                                            <label>Nopal ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 2)
                                            <label>Gallo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 3)
                                            <label>Borracho ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 4)
                                            <label>Sirena ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 5)
                                            <label>Corazon ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 6)
                                            <label>Muerte ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 7)
                                            <label>Alacran ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 8)
                                            <label>Sol ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 9)
                                            <label>Mundo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 10)
                                            <label>Chalupa ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 11)
                                            <label>Calavera ({{$k}}):</label>
                                            <br>
                                        @endif

                                    </div>
                                    <div class="right_side">
                                        <input type="text" name="cell{{$k}}_b{{$i}}" value="{{$percentagesMainGame1[$i][$k]}}">
                                    </div>
                                </div>
                            @endfor
                        @endfor


                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>


                        <h2>Cell percantage for free game for bet 50:</h2>
                        @for ($i = 0; $i < 5; $i++)
                            <h3>b{{$i}}:</h3>
                            @for ($k = 0; $k < 12; $k++)
                                <div class="row_block">
                                    <div class="left_side">
                                        @if($k === 0)
                                            <label>Diablo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 1)
                                            <label>Nopal ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 2)
                                            <label>Gallo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 3)
                                            <label>Borracho ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 4)
                                            <label>Sirena ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 5)
                                            <label>Corazon ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 6)
                                            <label>Muerte ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 7)
                                            <label>Alacran ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 8)
                                            <label>Sol ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 9)
                                            <label>Mundo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 10)
                                            <label>Chalupa ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 11)
                                            <label>Calavera ({{$k}}):</label>
                                            <br>
                                        @endif
                                    </div>
                                    <div class="right_side">
                                        <input type="text" name="cellfg{{$k}}_b{{$i}}" value="{{$percentagesFreeGame50[$i][$k]}}">
                                    </div>
                                </div>
                            @endfor
                        @endfor

                        <h2>Cell percantage for free game for bet 25:</h2>
                        @for ($i = 0; $i < 5; $i++)
                            <h3>b{{$i}}:</h3>
                            @for ($k = 0; $k < 12; $k++)
                                <div class="row_block">
                                    <div class="left_side">
                                        @if($k === 0)
                                            <label>Diablo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 1)
                                            <label>Nopal ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 2)
                                            <label>Gallo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 3)
                                            <label>Borracho ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 4)
                                            <label>Sirena ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 5)
                                            <label>Corazon ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 6)
                                            <label>Muerte ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 7)
                                            <label>Alacran ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 8)
                                            <label>Sol ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 9)
                                            <label>Mundo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 10)
                                            <label>Chalupa ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 11)
                                            <label>Calavera ({{$k}}):</label>
                                            <br>
                                        @endif
                                    </div>
                                    <div class="right_side">
                                        <input type="text" name="cellfg{{$k}}_b{{$i}}" value="{{$percentagesFreeGame25[$i][$k]}}">
                                    </div>
                                </div>
                            @endfor
                        @endfor

                        <h2>Cell percantage for free game for bet 1:</h2>
                        @for ($i = 0; $i < 5; $i++)
                            <h3>b{{$i}}:</h3>
                            @for ($k = 0; $k < 12; $k++)
                                <div class="row_block">
                                    <div class="left_side">
                                        @if($k === 0)
                                            <label>Diablo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 1)
                                            <label>Nopal ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 2)
                                            <label>Gallo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 3)
                                            <label>Borracho ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 4)
                                            <label>Sirena ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 5)
                                            <label>Corazon ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 6)
                                            <label>Muerte ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 7)
                                            <label>Alacran ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 8)
                                            <label>Sol ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 9)
                                            <label>Mundo ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 10)
                                            <label>Chalupa ({{$k}}):</label>
                                            <br>
                                        @elseif($k === 11)
                                            <label>Calavera ({{$k}}):</label>
                                            <br>
                                        @endif
                                    </div>
                                    <div class="right_side">
                                        <input type="text" name="cellfg{{$k}}_b{{$i}}" value="{{$percentagesFreeGame1[$i][$k]}}">
                                    </div>
                                </div>
                            @endfor
                        @endfor



                        <div class="btn-wrap">
                            <button class="btn">Begin</button>
                        </div>
                    </form>

<br>
<hr>
<br>

                    <form action="/admin/simelation/elGallo/import-percentages"
                       method="POST">
                        {{ csrf_field() }}

                        <div class="row_block">
                             <div class="left_side">
                                 <label>JsonPercentsMain for bet 0.5$:</label>
                             </div>
                             <div class="right_side">
                                 <input type="text" name="jsonMainGame50" value="{{$jsonMainGame50}}">
                             </div>
                         </div>

                         <div class="row_block">
                              <div class="left_side">
                                  <label>JsonPercentsFree for bet 0.5$:</label>
                              </div>
                              <div class="right_side">
                                  <input type="text" name="jsonFreeGame50" value="{{$jsonFreeGame50}}">
                              </div>
                          </div>

                          <div class="row_block">
                               <div class="left_side">
                                   <label>JsonPercentsMain for bet 0.25$:</label>
                               </div>
                               <div class="right_side">
                                   <input type="text" name="jsonMainGame25" value="{{$jsonMainGame25}}">
                               </div>
                           </div>

                           <div class="row_block">
                                <div class="left_side">
                                    <label>JsonPercentsFree for bet 0.25$:</label>
                                </div>
                                <div class="right_side">
                                    <input type="text" name="jsonFreeGame25" value="{{$jsonFreeGame25}}">
                                </div>
                            </div>

                            <div class="row_block">
                                 <div class="left_side">
                                     <label>JsonPercentsMain for bet 0.01$:</label>
                                 </div>
                                 <div class="right_side">
                                     <input type="text" name="jsonMainGame1" value="{{$jsonMainGame1}}">
                                 </div>
                             </div>

                             <div class="row_block">
                                  <div class="left_side">
                                      <label>JsonPercentsFree for bet 0.01$:</label>
                                  </div>
                                  <div class="right_side">
                                      <input type="text" name="jsonFreeGame1" value="{{$jsonFreeGame1}}">
                                  </div>
                              </div>


                        <div class="btn-wrap">
                            <button class="btn">import</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
