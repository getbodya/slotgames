<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="../../css/test.css">
</head>
<body>
	<nav class="navbar clearfix">
		<div class="container">
			<ul class="nav">
				<li><a href="/admin">Dashboard</a></li>
				<li><a href="/admin/simulations">Simulations</a></li>
				<li><a href="/admin/statistics">Statistic</a></li>
			</ul>
		</div>
	</nav>
	<div class="main">
		<div class="container">
			<div class="row">
				<h1>Statistic</h1>
				<div class="col-6">
					<div class="work_space">
						<h2>Loteria</h2>
						@if($statEG !== null)
						Total spins (number) = {{$statEG->iteration_count}}<br />
						Total bet = {{$statEG->sum_bet}}<br />
						Total win = {{$statEG->sum_win}}<br />
						Number of winning spins = {{$statEG->number_of_winning_spins}}<br />
						Number of losing spins = {{$statEG->number_of_losing_spins}}<br />
						Number of jackpots = {{$statEG->number_of_jackpots}}<br />
						Number of free spins = {{$statEG->number_of_bonus_game}}<br />
						money returned on main game = {{$statEG->money_returned_on_main_game}}<br />
						money returned on freespin = {{$statEG->money_returned_on_bonus_game}}<br />
						money returned on jackpots = {{$statEG->total_jackpot_winning}}<br />
						<br />
						@else
						empty
						@endif
					</div>
				</div>

				<div class="col-6">
					<div class="work_space">
						<h2>Life Of Luxury</h2>
						@if($statLOL !== null)
						Total spins (number) = {{$statLOL->iteration_count}}<br />
						Total bet = {{$statLOL->sum_bet}}<br />
						Total win = {{$statLOL->sum_win}}<br />
						Number of winning spins = {{$statLOL->number_of_winning_spins}}<br />
						Number of losing spins = {{$statLOL->number_of_losing_spins}}<br />
						Number of free spins = {{$statLOL->number_of_bonus_game}}<br />
						money returned on main game = {{$statLOL->money_returned_on_main_game}}<br />
						money returned on freespin = {{$statLOL->money_returned_on_bonus_game}}<br />
						<br />
						@else
						empty
						@endif
					</div>
				</div>
				<div class="col-6">
					<div class="work_space">
						<h2>Superkeno</h2>
						@if($statKeno !== null)
						Total spins (number) = {{$statKeno->iteration_count}}<br />
						Total bet = {{$statKeno->sum_bet}}<br />
						Total win = {{$statKeno->sum_win}}<br />
						Number of winning spins = {{$statKeno->number_of_winning_spins}}<br />
						Number of losing spins = {{$statKeno->number_of_losing_spins}}<br />
						<br />
						@else
						empty
						@endif
					</div>
				</div>
				<div class="col-6">
					<div class="work_space">
						<h2>Doublekeno</h2>
						@if($statDKeno !== null)
						Total spins (number) = {{$statDKeno->iteration_count}}<br />
						Total bet = {{$statDKeno->sum_bet}}<br />
						Total win = {{$statDKeno->sum_win}}<br />
						Number of winning spins = {{$statDKeno->number_of_winning_spins}}<br />
						Number of losing spins = {{$statDKeno->number_of_losing_spins}}<br />
						<br />
						@else
						empty
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
