var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var gamename = 'gnomerus';
var checkHelm = false;

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');

        //var betOneSound = game.add.audio('game.betOneSound');
        //var betMaxSound = game.add.audio('game.betMaxSound');

        
        // изображения
        
        game.add.sprite(0,0, 'game.background');
        game.add.sprite(94,22, 'game.background1');
        topBarImage = game.add.sprite(94,22, 'topScoreGame1');
        
        
        addTableTitle(game, 'play1To',309,424);

        slotPosition = [[142, 87], [142, 199], [142, 311], [254, 87], [254, 199], [254, 311], [365, 87], [365, 199], [365, 311], [477, 87], [477, 199], [477, 311], [589, 87], [589, 199], [589, 311]];
        addSlots(game, slotPosition);

        var linePosition = [[132,249], [132,108], [132,383], [132,154], [132,144], [132,129], [132,318], [132,272], [132,159]];
        var numberPosition = [[94,233], [94,85], [94,376], [94,149], [94,312], [94,117], [94,344], [94,266], [94,201]];
        for (var i = 1; i <= 9; ++i) {
            game.add.sprite(94, numberPosition[i-1][1], 'unwin_' + i);
        }   
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);

        var pageCount = 6;

        // кнопки
        addButtonsGame1(game, pageCount);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[240, 24, 20], [460, 24, 20], [640, 24, 20], [101, 426, 16], [708, 426, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        var pageCoord = [[94, 22], [94, 22], [94, 22], [94, 22], [94, 22], [94, 22]];   
        var btnCoord = [[94, 444], [319, 444], [509, 444]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);

        full_and_sound();

    };



    game1.update = function () {      
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }  
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
            if(checkHelm == true) {
            }
        }
    };

    game.state.add('game1', game1);

};
