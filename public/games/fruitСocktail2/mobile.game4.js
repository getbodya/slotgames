// function winResultGame4(game, x,y) {
//     monkeyHangingHide();
//     mankeyLeftPoint.visible = true;
//     setTimeout("bitMonkey.play();", 500);
//     take_or_risk_anim.visible = false;
//     mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
//         winPoint1.play();
//         goldPoint = game.add.sprite(207-mobileX,263-mobileY, 'game.goldPoint');
//         goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
//         goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
//             mankeyLeftPoint.visible = false;
//             winPoint2.play();
//             mankeyWinPoint.visible = true;
//             mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
//                 updateBalanceGame4(game, scorePosions, balanceR);
//             });
//         });
//     });
// }

// function loseResultGame4(game, x,y) {
//     setTimeout("bitMonkey.play();", 500);
//     monkeyHangingHide();
//     take_or_risk_anim.visible = false;
//     mankeyLeftPoint.visible = true;
//     mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
//         //winPoint1.play();
//         losePoint.play();
//         spiderPoint = game.add.sprite(207-mobileX,263-mobileY, 'game.spiderPoint');
//         spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
//         spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
//             mankeyLeftPoint.visible = false;

//             mankeyLosePoint.visible = true;
//             mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
//                 updateBalanceGame4(game, scorePosions, balanceR);
//             });
//         });

//     });
// }

// function getResultGame4(game, x,y) {
//     if(ropeValues[5] != 0){
//         lockDisplay();
//         winResultGame4(game, x,y);
//     } else {
//         lockDisplay();
//         loseResultGame4(game, x,y);
//     }
// }


function game4() {
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {

            //изображения
            //Добавление фона
            background = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame4');
            backgroundGame4 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame4');

            losePoint = game.add.audio('game.losePoint');
            game4_winSound = game.add.audio('game4_win');
            game4_winSound.addMarker('win', 0.2, 5); 
            game4_loseSound = game.add.audio('game4_lose');  

            game.gnome4_1 = game.add.sprite(366-mobileX, 327-mobileY, 'game.gnome4_1');
            game.gnome4_1Animation = game.gnome4_1.animations.add('game.gnome4_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
            game.gnome4_1Animation.play();     
            game.gnome4_2 = game.add.sprite(366-mobileX, 327-mobileY, 'game.gnome4_2');
            game.gnome4_2Animation = game.gnome4_2.animations.add('game.gnome4_2', [0,1,2,3,4,5,6,7,8,9], 8, false);            
            game.gnome4_2.visible = false;
            game.gnome4_1Animation.onComplete.add(function(){
                game.gnome4_2Animation.play();
                game.gnome4_1.visible = false;
                game.gnome4_2.visible = true;
            });
            game.gnome4_2Animation.onComplete.add(function(){
                game.gnome4_1Animation.play();
                game.gnome4_1.visible = true;
                game.gnome4_2.visible = false;
            });  

            //счет
            scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);


            //кнопок

            // addButtonsGame4(game);

            //нужно добавить в игру кнопки buttonLine3 и buttonLine7

            game.chest_left = game.add.sprite(270-mobileX, 198-mobileY, 'game.chest_left');
            game.chest_left.inputEnabled = true;
            game.chest_right = game.add.sprite(526-mobileX, 198-mobileY, 'game.chest_right');
            game.chest_right.inputEnabled = true;



            //анимации

            game.chest_left.events.onInputDown.add(function(){

                game.chest_left.inputEnabled = false;

                if(ropeValues[5] != 0){
                    game4_winSound.play('win');
                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        game.chest_win_left.visible = true; 
                        game.chest_win_left.position.x = 271-mobileX;
                        game.chest_win_leftAnimation.play().onComplete.add(function(){   
                            game.gnome4_win.visible = true; 
                            game.gnome4_win.position.x = 159-mobileX; 
                            game.gnome4_key.visible = false;
                            game.chest_open2.visible = true;
                            game.gnome4_winAnimation.play().onComplete.add(function(){  
                                game.gnome4_win.visible = false; 
                                game.gnome4_win2.visible = true;
                                game.gnome4_win2.position.x = 159-mobileX;
                                game.gold.visible = true;
                                game.gold.position.x = 288-mobileX;
                                setTimeout(function() {
                                    game.gnome4_win3.visible = true; 
                                    game.gnome4_win3.position.x = 159-mobileX; 
                                    game.gnome4_win2.visible = false;
                                    game.gnome4_win3Animation.play().onComplete.add(function(){  
                                        updateBalanceGame4(game, scorePosions, balanceR);
                                    });         
                                }, 100);    
                            });         
                        });  

                    });

                } else {

                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        losePoint.play();
                        game.chest_lose_left.visible = true; 
                        game.chest_lose_leftAnimation.play().onComplete.add(function(){   
                            game.gnome4_key.visible = false;
                            game.chest_open2.visible = true;
                            game.gnome4_lose.visible = true; 
                            game.gnome4_loseAnimation.play().onComplete.add(function(){  
                                game.gnome4_lose.visible = false; 
                                game.gnome4_lose2.visible = true; 
                                game.gnome4_lose2Animation.play().onComplete.add(function(){  
                                    updateBalanceGame4(game, scorePosions, balanceR);
                                });         
                            });         
                        });  

                    });

                }

            });
            game.chest_right.events.onInputDown.add(function(){

                game.chest_right.inputEnabled = false;

                if(ropeValues[5] != 0){
                    game4_winSound.play('win');
                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_key.position.x = 415-mobileX;  
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        game.chest_win_right.visible = true; 
                        game.chest_win_rightAnimation.play().onComplete.add(function(){   
                            game.gnome4_win.visible = true; 
                            game.gnome4_key.visible = false;
                            game.chest_open3.visible = true;
                            game.chest_open3.position.x = 528-mobileX;
                            game.gnome4_winAnimation.play().onComplete.add(function(){  
                                game.gnome4_win.visible = false; 
                                game.gnome4_win2.visible = true;
                                game.gold.visible = true;
                                setTimeout(function() {
                                    game.gnome4_win3.visible = true; 
                                    game.gnome4_win2.visible = false;
                                    game.gnome4_win3Animation.play().onComplete.add(function(){  
                                        updateBalanceGame4(game, scorePosions, balanceR);
                                    });         
                                }, 100);    
                            });         
                        });  

                    });

                } else {

                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_key.position.x = 415-mobileX;  
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        losePoint.play();
                        game.chest_lose_right.visible = true; 
                        game.chest_lose_rightAnimation.play().onComplete.add(function(){   
                            game.gnome4_key.visible = false;
                            game.chest_open3.visible = true;
                            game.chest_open3.position.x = 528-mobileX;
                            game.gnome4_lose.visible = true; 
                            game.gnome4_lose.position.x = 415-mobileX;
                            game.gnome4_loseAnimation.play().onComplete.add(function(){  
                                game.gnome4_lose.visible = false; 
                                game.gnome4_lose2.visible = true; 
                                game.gnome4_lose2.position.x = 399-mobileX;
                                game.gnome4_lose2Animation.play().onComplete.add(function(){  
                                    updateBalanceGame4(game, scorePosions, balanceR);
                                });         
                            });         
                        });  

                    });

                }

            });


            game.chest_win_left = game.add.sprite(271-mobileX, 199-mobileY, 'game.chest_win_left');
            game.chest_win_leftAnimation = game.chest_win_left.animations.add('game.chest_win_left', [0,1,2,3,4,5], 8, false);
            game.chest_win_left.visible = false;

            game.chest_win_right = game.add.sprite(528-mobileX, 199-mobileY, 'game.chest_win_right');
            game.chest_win_rightAnimation = game.chest_win_right.animations.add('game.chest_win_right', [0,1,2,3,4,5], 8, false);
            game.chest_win_right.visible = false;

            game.chest_lose_left = game.add.sprite(271-mobileX, 199-mobileY, 'game.chest_lose_left');
            game.chest_lose_leftAnimation = game.chest_lose_left.animations.add('game.chest_lose_left', [0,1,2,3,4,5], 8, false);
            game.chest_lose_left.visible = false;   

            game.chest_lose_right = game.add.sprite(528-mobileX, 199-mobileY, 'game.chest_lose_right');
            game.chest_lose_rightAnimation = game.chest_lose_right.animations.add('game.chest_lose_right', [0,1,2,3,4,5], 8, false);
            game.chest_lose_right.visible = false;

            game.chest_open3 = game.add.sprite(271-mobileX, 199-mobileY, 'game.chest_open3');
            game.chest_open3.visible = false;

            game.chest_open2 = game.add.sprite(271-mobileX, 199-mobileY, 'game.chest_open2');
            game.chest_open2.visible = false;

            game.gnome4_win2 = game.add.sprite(415-mobileX, 232-mobileY, 'game.gnome4_win2');
            game.gnome4_win2.visible = false;

            game.gold = game.add.sprite(544-mobileX, 264-mobileY, 'game.gold');
            game.gold.visible = false;

            game.gnome4_key = game.add.sprite(159-mobileX, 232-mobileY, 'game.gnome4_key');
            game.gnome4_keyAnimation = game.gnome4_key.animations.add('game.gnome4_key', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 8, false);
            game.gnome4_key.visible = false;

            game.gnome4_win = game.add.sprite(415-mobileX, 123-mobileY, 'game.gnome4_win');
            game.gnome4_winAnimation = game.gnome4_win.animations.add('game.gnome4_win', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
            game.gnome4_win.visible = false;

            game.gnome4_win3 = game.add.sprite(415-mobileX, 232-mobileY, 'game.gnome4_win3');
            game.gnome4_win3Animation = game.gnome4_win3.animations.add('game.gnome4_win3', [0,1,2,3,4,4,4,4,4,4,4,4,4], 8, false);
            game.gnome4_win3.visible = false;

            game.gnome4_lose = game.add.sprite(159-mobileX, 233-mobileY, 'game.gnome4_lose');
            game.gnome4_loseAnimation = game.gnome4_lose.animations.add('game.gnome4_lose', [0,1,2,3,4], 8, false);
            game.gnome4_lose.visible = false;

            game.gnome4_lose2 = game.add.sprite(143-mobileX, 233-mobileY, 'game.gnome4_lose');
            game.gnome4_lose2Animation = game.gnome4_lose2.animations.add('game.gnome4_lose', [5,5,5,5,5,5], 8, false);
            game.gnome4_lose2.visible = false; 


            function gnome4Hide(){
                game.gnome4_1.visible = false;
                game.gnome4_2.visible = false;

                game.gnome4_1.animations.stop();
                game.gnome4_2.animations.stop();
            }


        };

        game4.update = function () {
        };

        game.state.add('game4', game4);

    })();


}