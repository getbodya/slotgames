function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {
        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        background1 = game.add.sprite(94,22, 'game.background1');
        //карты
        var cardPosition = [[124,151], [254,151], [366,151], [478,151], [590,151]];
        addCards(game, cardPosition);
        //изображения
        backgroundGame3 = game.add.sprite(94,22, 'game.backgroundGame2');
        // topBarImage2 = game.add.sprite(94,22, 'topScoreGame3');
        background = game.add.sprite(0,0, 'game.background');

        // addTableTitle(game, 'winTitleGame2', 334,54);
        hideTableTitle();
        
        // кнопки
        addButtonsGame2(game);



        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [463, 97, 24]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);


        openDCard(dcard);

        //анимация
        game.fire = game.add.sprite(318, 406, 'fire');
        game.fireAnimation = game.fire.animations.add('fire', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true).play();         
        game.tent = game.add.sprite(94, 422, 'tent');
        game.tentAnimation = game.tent.animations.add('tent', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true).play();   
        game.cup = game.add.sprite(414, 390, 'cup');
        game.cupAnimation = game.cup.animations.add('cup', [0,1,2,3,4,0], 6, false);
        game.cup.visible = false;  
        game.glasses = game.add.sprite(414, 406, 'glasses');
        game.glassesAnimation = game.glasses.animations.add('glasses', [0,1,2,3,4,5,6,2,1,0], 6, false).play();
        game.head_left_right = game.add.sprite(414, 390, 'head_left_right');
        game.cap = game.add.sprite(446, 391, 'cap');
        game.head_left_rightAnimation = game.head_left_right.animations.add('head_left_right', [0,1,2,0,3,4,5,6], 6, false);
        game.head_left_right.visible = false;
        game.win = game.add.sprite(414, 406, 'win');
        game.winAnimation = game.win.animations.add('win', [0,1,2,3,4,5,1,0], 6, false);
        game.win.visible = false;  
        game.glassesAnimation.onComplete.add(function(){
            game.glasses.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });
        game.head_left_rightAnimation.onComplete.add(function(){
            game.head_left_right.visible = false;
            game.glasses.visible = true;
            game.glassesAnimation.play();
            game.cap.visible = true;
        });
        game.cupAnimation.onComplete.add(function(){
            game.cup.visible = false;
            game.head_left_right.visible = true;
            game.head_left_rightAnimation.play();
            game.cap.visible = false;
        });
        game.winAnimation.onComplete.add(function(){
            game.win.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });

        showDoubleToAndTakeOrRiskTexts(game, totalWin, 565, 431);

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}