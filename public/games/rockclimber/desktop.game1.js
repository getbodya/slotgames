var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var gamename = 'rockclimberrus';
var checkHelm = false;

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');

        //var betOneSound = game.add.audio('game.betOneSound');
        //var betMaxSound = game.add.audio('game.betMaxSound');

        
        
        
        
        

        slotPosition = [[142, 87], [142, 199], [142, 311], [254, 87], [254, 199], [254, 311], [365, 87], [365, 199], [365, 311], [477, 87], [477, 199], [477, 311], [589, 87], [589, 199], [589, 311]];
        addSlots(game, slotPosition);
        // изображения

        game.add.sprite(94,22, 'game.background1');
        topBarImage = game.add.sprite(94,22, 'topScoreGame1');

        addTableTitle(game, 'play1To',540,422);

        var linePosition = [[134,243], [134,110], [134,383], [134,158], [134,132], [134,132], [134,261], [134,270], [134,159]];
        var numberPosition = [[94,230], [94,86], [94,374], [94,150], [94,310], [94,118], [94,342], [94,262], [94,198]];
        addLinesAndNumbers(game, linePosition, numberPosition);
        
        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [99, 59, 16], [707, 59, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        // анимация

        game.fire = game.add.sprite(318, 406, 'fire');
        game.fireAnimation = game.fire.animations.add('fire', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true).play();         
        game.tent = game.add.sprite(94, 422, 'tent');
        game.tentAnimation = game.tent.animations.add('tent', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true).play();   
        game.cup = game.add.sprite(414, 390, 'cup');
        game.cupAnimation = game.cup.animations.add('cup', [0,1,2,3,4,0], 6, false);
        game.cup.visible = false;  
        game.glasses = game.add.sprite(414, 406, 'glasses');
        game.glassesAnimation = game.glasses.animations.add('glasses', [0,1,2,3,4,5,6,2,1,0], 6, false).play();
        game.head_left_right = game.add.sprite(414, 390, 'head_left_right');
        game.cap = game.add.sprite(446, 391, 'cap');
        game.head_left_rightAnimation = game.head_left_right.animations.add('head_left_right', [0,1,2,0,3,4,5,6], 6, false);
        game.head_left_right.visible = false;
        game.win = game.add.sprite(414, 406, 'win');
        game.winAnimation = game.win.animations.add('win', [0,1,2,3,4,5,1,0], 6, false);
        game.win.visible = false;  
        game.glassesAnimation.onComplete.add(function(){
            game.glasses.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });
        game.head_left_rightAnimation.onComplete.add(function(){
            game.head_left_right.visible = false;
            game.glasses.visible = true;
            game.glassesAnimation.play();
            game.cap.visible = true;
        });
        game.cupAnimation.onComplete.add(function(){
            game.cup.visible = false;
            game.head_left_right.visible = true;
            game.head_left_rightAnimation.play();
            game.cap.visible = false;
        });
        game.winAnimation.onComplete.add(function(){
            game.win.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });
        game.add.sprite(0,0, 'game.background');
        var pageCount = 5;
        // кнопки
        addButtonsGame1(game, pageCount);
        var pageCoord = [[94, 22], [94, 22], [94, 22], [94, 22], [94, 22]];   
        var btnCoord = [[95, 454], [304, 454], [527, 454]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);
        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
        }
    };

    game.state.add('game1', game1);

};
