var stepTotalWinR = 0;

function showWin(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y, win, { font: '30px \"Impact\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

    // var timeInterval = 450;
    // var textCounter = setInterval(function () {
    //     text.position.y -= 3;
    // }, 10);

    // setTimeout(function() {
    //     clearInterval(textCounter);
    // }, timeInterval);

    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}






function game3() {
    (function () {

        var game3 = {};

        game3.preload = function () {};

        game3.create = function () {

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            gnomePositionsX = {1:62, 3:190, 5:318, 7:446, 9:574};
            //звуки
            // winRope1 = game.add.audio('game.winRope1');
            // winRope2 = game.add.audio('game.winRope2');
            // bitMonkey = game.add.audio('game.bitMonkey');
            
            // hitSound = game.add.audio('hit');
            // hitSound.addMarker('hits', 0.3, 5);     
            winSound = game.add.audio('winGame3');
            game3_1Sound = game.add.audio('game3_1');
            game3_2Sound = game.add.audio('game3_2');
            game3_loseSound = game.add.audio('game3_lose');
            game3_win_2Sound = game.add.audio('game3_win_2');
            
            //изображения
            background = game.add.sprite(94,22, 'topScoreGame4');
            background = game.add.sprite(94,54, 'game.backgroundGame3');
            background = game.add.sprite(0,0, 'game.background');

            // rope1 = game.add.sprite(263,55, 'game.rope');
            // rope2 = game.add.sprite(353,55, 'game.rope');
            // rope3 = game.add.sprite(443,55, 'game.rope');
            // rope4 = game.add.sprite(533,55, 'game.rope');
            // rope5 = game.add.sprite(623,55, 'game.rope');

            // for(var i in gnomePositionsX) {
            //     game.lever_arr[i] = game.add.sprite(94+((i-1)/2)*128, 390, 'game.lever'+i);
            //     game.lever_arr[i].visible = false;
            //     game.chest_open_arr[i] = game.add.sprite(94+((i-1)/2)*128, 54, 'game.chest_open');
            //     game.chest_open_arr[i].visible = false;
            // } 

            //счет
            scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);
            
            
            //кнопки

            var selectGame = game.add.sprite(70,510, 'selectGame_d');
            selectGame.scale.setTo(0.7, 0.7);
            selectGame.inputEnabled = false;

            var payTable = game.add.sprite(150,510, 'payTable_d');
            payTable.scale.setTo(0.7, 0.7);
            payTable.inputEnabled = false;

            var betone = game.add.sprite(490,510, 'betone_d');
            betone.scale.setTo(0.7, 0.7);
            betone.inputEnabled = false;


            var betmax = game.add.sprite(535,510, 'betmax_d');
            betmax.scale.setTo(0.7, 0.7);
            betmax.inputEnabled = false;

            var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
            automaricstart.scale.setTo(0.7, 0.7);
            automaricstart.inputEnabled = false;

            var startButton = game.add.sprite(597, 510, 'startButton_d');
            startButton.scale.setTo(0.7,0.7);
            startButton.inputEnabled = false;

            var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
            buttonLine1.scale.setTo(0.7,0.7);
            buttonLine1.inputEnabled = true;
            buttonLine1.input.useHandCursor = true;

            var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
            buttonLine3.scale.setTo(0.7,0.7);
            buttonLine3.inputEnabled = true;
            buttonLine3.input.useHandCursor = true;

            var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
            buttonLine5.scale.setTo(0.7,0.7);
            buttonLine5.inputEnabled = true;
            buttonLine5.input.useHandCursor = true;

            var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
            buttonLine7.scale.setTo(0.7,0.7);
            buttonLine7.inputEnabled = true;
            buttonLine7.input.useHandCursor = true;

            var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
            buttonLine9.scale.setTo(0.7,0.7);
            buttonLine9.inputEnabled = true;
            buttonLine9.input.useHandCursor = true;

            buttonLine1.events.onInputOver.add(function(){
                if(buttonLine1.inputEnabled == true) {
                    buttonLine1.loadTexture('buttonLine1_p');
                }
            });
            buttonLine1.events.onInputOut.add(function(){
                if(buttonLine1.inputEnabled == true) {
                    buttonLine1.loadTexture('buttonLine1');
                }
            });

            buttonLine3.events.onInputOver.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3_p');
                }
            });
            buttonLine3.events.onInputOut.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3');
                }
            });

            buttonLine5.events.onInputOver.add(function(){
                if(buttonLine5.inputEnabled == true) {
                    buttonLine5.loadTexture('buttonLine5_p');
                }
            });
            buttonLine5.events.onInputOut.add(function(){
                if(buttonLine5.inputEnabled == true) {
                    buttonLine5.loadTexture('buttonLine5');
                }
            });

            buttonLine7.events.onInputOver.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7_p');
                }
            });
            buttonLine7.events.onInputOut.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7');
                }
            });

            buttonLine9.events.onInputOver.add(function(){
                if(buttonLine9.inputEnabled == true) {
                    buttonLine9.loadTexture('buttonLine9_p');
                }
            });
            buttonLine9.events.onInputOut.add(function(){
                if(buttonLine9.inputEnabled == true) {
                    buttonLine9.loadTexture('buttonLine9');
                }
            });

            buttonLine1.events.onInputDown.add(function(){
                takeBananaRope(62,22,1);

                buttonLine1.loadTexture('buttonLine1_d');
                buttonLine1.inputEnabled = false;
                buttonLine1.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) {
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,3500, 139,81 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine3.events.onInputDown.add(function(){
                takeBananaRope(190,22,2);

                buttonLine3.loadTexture('buttonLine3_d');
                buttonLine3.inputEnabled = false;
                buttonLine3.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,3500, 139+128,81 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine5.events.onInputDown.add(function(){
                takeBananaRope(318,22,3);

                buttonLine5.loadTexture('buttonLine5_d');
                buttonLine5.inputEnabled = false;
                buttonLine5.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,3500, 139+128+128,81 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine7.events.onInputDown.add(function(){
                takeBananaRope(446,22,4);

                buttonLine7.loadTexture('buttonLine7_d');
                buttonLine7.inputEnabled = false;
                buttonLine7.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,3500, 139+128+128+128,81 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine9.events.onInputDown.add(function(){
                takeBananaRope(574,22,5);

                buttonLine9.loadTexture('buttonLine9_d');
                buttonLine9.inputEnabled = false;
                buttonLine9.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,3500, 139+128+128+128+128,81 , ropeValues[ropeStep], stepTotalWinR);
            });
            
            //анимации и логика

            function hideGnome3(){
                game.gnome_left.visible = false;
                game.gnome_right.visible = false;
            }

            function showRandGnome2(x,y){

                game.gnome_left = game.add.sprite(x, y, 'game.gnome_left');
                game.gnome_leftAnimation = game.gnome_left.animations.add('game.gnome_left', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 8, false);
                game.gnome_leftAnimation.play();   

                game.gnome_right = game.add.sprite(x, y-16, 'game.gnome_right');
                game.gnome_rightAnimation = game.gnome_right.animations.add('game.gnome_right', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,17,17,17], 8, false);
                game.gnome_right.visible = false;

                game.gnome_leftAnimation.onComplete.add(function(){
                    game.gnome_left.visible = false;
                    game.gnome_right.visible = true;
                    game.gnome_rightAnimation.play();
                });

                game.gnome_rightAnimation.onComplete.add(function(){
                    game.gnome_right.visible = false;
                    game.gnome_left.visible = true;
                    game.gnome_leftAnimation.play();
                });
            }

            var ropeStep = 0;
            //часть логики занесена в анимацию, так как в разных игра она может отличаться для данной бонусной игры
            //0 - банан, 1 - кувалда, 2 - кирпич
            var win = 0;
            var ropeStep = 0;
            var checkUpLvl = true; //проверка доступа ко второй бонусной игре

            function takeBananaRope(x,y,lineNumber){
                lockDisplay();

                hideGnome3();

                // monkeyTakeRope1 = game.add.sprite(x,y, 'game.monkeyTakeRope1');
                // monkeyTakeRope1.animations.add('monkeyTakeRope1', [0,1,2,3,4], 10, false);
                // monkeyTakeRope1.visible = false;

                // monkeyTakeRope2 = game.add.sprite(x,y, 'game.monkeyTakeRope2');
                // monkeyTakeRope2.animations.add('monkeyTakeRope2', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                // monkeyTakeRope2.visible = false;

                // monkeyTakeRope1H = game.add.sprite(x,y, 'game.monkeyTakeRope1H');
                // monkeyTakeRope1H.animations.add('monkeyTakeRope1H', [0,1,2,3,4], 10, false);
                // monkeyTakeRope1H.visible = false;

                // monkeyTakeRope2H = game.add.sprite(x,y, 'game.monkeyTakeRope2H');
                // monkeyTakeRope2H.animations.add('monkeyTakeRope2H', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                // monkeyTakeRope2H.visible = false;

                // monkeyEatBanana = game.add.sprite(x,y, 'game.monkeyEatBanana');
                // monkeyEatBanana.animations.add('monkeyEatBanana', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                // monkeyEatBanana.visible = false;

                // monkeyEatBananaH = game.add.sprite(x,y, 'game.monkeyEatBananaH');
                // monkeyEatBananaH.animations.add('monkeyEatBananaH', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                // monkeyEatBananaH.visible = false;

                // monkeyEatHammer1 = game.add.sprite(x,y, 'game.monkeyEatHammer1');
                // monkeyEatHammer1.animations.add('monkeyEatHammer1', [0,1,2], 10, false);
                // monkeyEatHammer1.visible = false;

                // monkeyEatHammer12 = game.add.sprite(x,y, 'game.monkeyEatHammer12');
                // monkeyEatHammer12.animations.add('monkeyEatHammer12', [0,1,2,3], 10, false);
                // monkeyEatHammer12.visible = false;

                // monkeyEatHammer2 = game.add.sprite(x,y, 'game.monkeyEatHammer2');
                // monkeyEatHammer2.animations.add('monkeyEatHammer2', [0,1,2], 10, false);
                // monkeyEatHammer2.visible = false;

                // monkeyEatHammer22 = game.add.sprite(x,y, 'game.monkeyEatHammer22');
                // monkeyEatHammer22.animations.add('monkeyEatHammer22', [0,1,2,3], 10, false);
                // monkeyEatHammer22.visible = false;

                // monkeyHeadache = game.add.sprite(x,y, 'game.monkeyHeadache');
                // monkeyHeadache.animations.add('monkeyHeadache', [0,1,2], 10, true);
                // monkeyHeadache.visible = false;
                game.lever_arr = game.add.sprite(x+32, 390, 'game.lever'+((lineNumber*2)-1));
                game.lever_arr.visible = false;

                game.chest_open = game.add.sprite(x+32, 54, 'game.chest_open');
                game.chest_open.visible = false;

                game.action1 = game.add.sprite(x, 326, 'game.action1');
                game.action1.animations.add('game.action1', [0,1,2,3,4,5,6,7,8,9,10,11], 14, false);
                game.action1.visible = false;  

                game.chest_action1 = game.add.sprite(x+32, 54, 'game.chest_action1');
                game.chest_action1.animations.add('game.chest_action1', [0,1,2,3,4,5,6,7,8], 12, false);
                game.chest_action1.visible = false;

                game.chest_win = game.add.sprite(x+32, 54, 'game.chest_win');
                game.chest_win.animations.add('game.chest_win', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 20, false);
                game.chest_win.visible = false;   

                game.gnome_win = game.add.sprite(x, 265, 'game.gnome_win');
                game.gnome_win.animations.add('game.gnome_win', [0,1,2,3,4,5,6,7,8,9,10], 14, false);
                game.gnome_win.visible = false;

                game.gnome_win2 = game.add.sprite(x, 326, 'game.gnome_win2');
                game.gnome_win2.animations.add('game.gnome_win2', [0,1,2], 12, false);
                game.gnome_win2.visible = false;

                game.chest_lose = game.add.sprite(x+32, 54, 'game.chest_lose');
                game.chest_lose.animations.add('game.chest_lose', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 12, false);
                game.chest_lose.visible = false;

                game.chest_lose2 = game.add.sprite(x+32, 54, 'game.chest_lose');
                game.chest_lose2.animations.add('game.chest_lose2', [15,16,17,18,19,20,21,22], 12, false);
                game.chest_lose2.visible = false;

                game.gnome_lose = game.add.sprite(x, 166, 'game.gnome_lose');
                game.gnome_lose.animations.add('game.gnome_lose', [0,1,2,3,4,5,6,7,8,9], 12, false);
                game.gnome_lose.visible = false; 

                game.gnome_lose2 = game.add.sprite(x, 310, 'game.gnome_lose2');
                game.gnome_lose2.animations.add('game.gnome_lose2', [0,1,2,3,4,5,6,7,8,9,6,7,8,9], 12, false);
                game.gnome_lose2.visible = false;   

                if(checkHelm == true) {
                    console.log('checkHelm = true');
//                     monkeyTakeRope1H.visible = true;
//                     monkeyTakeRope1H.animations.getAnimation('monkeyTakeRope1H').play().onComplete.add(function(){

//                         switch(lineNumber) {
//                             case 1:
//                             rope1.visible = false;
//                             break;
//                             case 2:
//                             rope2.visible = false;
//                             break;
//                             case 3:
//                             rope3.visible = false;
//                             break;
//                             case 4:
//                             rope4.visible = false;
//                             break;
//                             case 5:
//                             rope5.visible = false;
//                             break;
//                         }

//                         monkeyTakeRope1H.visible = false;
//                         monkeyTakeRope2H.visible = true;
//                         monkeyTakeRope2H.animations.getAnimation('monkeyTakeRope2H').play().onComplete.add(function(){
//                             monkeyTakeRope2H.visible = false;

//                             if(ropeValues[ropeStep] > 0) {
//                                 win = 0;
//                             } else {
//                                 if(checkHelm == true){
//                                     checkHelm = false;
//                                     win = 2;
//                                     checkUpLvl = false;
//                                 } else {
//                                     win = 1;
//                                 }
//                             }

//                             ropeStep += 1;


//                             if(win == 1) {
//                                 monkeyEatHammer1.visible = true;
//                                 monkeyEatHammer1.animations.getAnimation('monkeyEatHammer1').play().onComplete.add(function(){
//                                     monkeyEatHammer1.visible = false;
//                                     monkeyEatHammer12.visible = true;
//                                     bitMonkey.play();
//                                     monkeyEatHammer12.animations.getAnimation('monkeyEatHammer12').play().onComplete.add(function(){
//                                         monkeyEatHammer12.visible = false;

//                                         monkeyHeadache.visible = true;
//                                         monkeyHeadache.animations.getAnimation('monkeyHeadache').play();

//                                         updateBalanceGame3(game, scorePosions, balanceR);
//                                     });

//                                 });
//                             } else if(win == 2){
//                                 monkeyEatHammer2.visible = true;
//                                 monkeyEatHammer2.animations.getAnimation('monkeyEatHammer2').play().onComplete.add(function(){
//                                     monkeyEatHammer2.visible = false;
//                                     bitMonkey.play();
//                                     monkeyEatHammer22.visible = true;
//                                     monkeyEatHammer22.animations.getAnimation('monkeyEatHammer22').play().onComplete.add(function(){
//                                         monkeyEatHammer22.visible = false;

//                                         showRandMonkey2(x-100,-22+y);

//                                         if(ropeStep == 5) {
//                                             updateBalanceGame3(game, scorePosions, balanceR);
//                                         }
//                                     });
//                                 });
//                             } else {
//                                 if(checkHelm == true) {
//                                     monkeyEatBananaH.visible = true;
//                                     winRope1.play();
//                                     monkeyEatBananaH.animations.getAnimation('monkeyEatBananaH').play().onComplete.add(function(){
//                                         monkeyEatBananaH.visible = false;
//                                         winRope2.play();

//                                         showRandMonkey2(x-100,-22+y);

//                                         if(ropeStep == 5 && checkUpLvl == true) {
//                                             setTimeout("game.state.start('game4');", 1000);
//                                         } else if(ropeStep == 5) {
//                                             updateBalanceGame3(game, scorePosions, balanceR);
//                                         }
//                                     });
//                                 } else {
//                                     monkeyEatBanana.visible = true;
//                                     winRope1.play();
//                                     monkeyEatBanana.animations.getAnimation('monkeyEatBanana').play().onComplete.add(function(){
//                                         monkeyEatBanana.visible = false;
//                                         winRope2.play();

//                                         showRandMonkey2(x-100,-22+y);

//                                         if(ropeStep == 5 && checkUpLvl == true) {
//                                             setTimeout("game.state.start('game4');", 1000);
//                                         } else if(ropeStep == 5) {
//                                             updateBalanceGame3(game, scorePosions, balanceR);
//                                         }
//                                     });
//                                 }
//                             }
//                         });
// });
} else {
    console.log('checkHelm = false');
    game.action1.visible = true; 
    game.lever_arr.visible = true;
    game3_1Sound.play();
    game.action1.animations.getAnimation('game.action1').play().onComplete.add(function(){

        // switch(lineNumber) {
        //     case 1:
        //     rope1.visible = false;
        //     break;
        //     case 2:
        //     rope2.visible = false;
        //     break;
        //     case 3:
        //     rope3.visible = false;
        //     break;
        //     case 4:
        //     rope4.visible = false;
        //     break;
        //     case 5:
        //     rope5.visible = false;
        //     break;
        // }

        game.chest_action1.visible = true;
        // game.chest_action1Animation.play();
        game.chest_action1.animations.getAnimation('game.chest_action1').play().onComplete.add(function(){
            game3_2Sound.play();
            game.chest_action1.visible = false;
            game.chest_open.visible = true;

            if(ropeValues[ropeStep] > 0) {
                win = 0;
            } else {
                if(checkHelm == true){
                    checkHelm = false;
                    win = 2;
                    checkUpLvl = false;
                } else {
                    win = 1;
                }
            }

            ropeStep += 1;


            if(win == 1) {
                game.chest_lose.visible = true;
                game.chest_lose.animations.getAnimation('game.chest_lose').play().onComplete.add(function(){
                    game.chest_lose.visible = false;
                    game.chest_lose2.visible = true;
                    game.gnome_lose.visible = true;
                    game.chest_lose2.animations.getAnimation('game.chest_lose2').play();
                    setTimeout(function(){
                        game3_loseSound.play();
                    },300);
                    game.gnome_lose.animations.getAnimation('game.gnome_lose').play().onComplete.add(function(){
                        game.gnome_lose.visible = false;
                        game.gnome_lose2.visible = true;
                        game.gnome_lose2.animations.getAnimation('game.gnome_lose2').play();

                        updateBalanceGame3(game, scorePosions, balanceR);
                    });

                });
            } else if(win == 2){
                // monkeyEatHammer2.visible = true;
                // monkeyEatHammer2.animations.getAnimation('monkeyEatHammer2').play().onComplete.add(function(){
                //     monkeyEatHammer2.visible = false;
                //     bitMonkey.play();
                //     monkeyEatHammer22.visible = true;
                //     monkeyEatHammer22.animations.getAnimation('monkeyEatHammer22').play().onComplete.add(function(){
                //         monkeyEatHammer22.visible = false;

                //         showRandMonkey2(x-100,-22+y);

                //         if(ropeStep == 5) {
                //             updateBalanceGame3(game, scorePosions, balanceR);
                //         }
                //     });
                // });
            } else {
                if(checkHelm == true) {
                    // monkeyEatBananaH.visible = true;
                    // winRope1.play();
                    // monkeyEatBananaH.animations.getAnimation('monkeyEatBananaH').play().onComplete.add(function(){
                    //     monkeyEatBananaH.visible = false;
                    //     winRope2.play();

                    //     showRandMonkey2(x-100,-22+y);

                    //     if(ropeStep == 5 && checkUpLvl == true) {
                    //         setTimeout("game.state.start('game4');", 1000);
                    //     } else if(ropeStep == 5) {
                    //         updateBalanceGame3(game, scorePosions, balanceR);
                    //     }
                    // });
                } else {
                    game.chest_win.visible = true;
                    game.chest_win.animations.getAnimation('game.chest_win').play().onComplete.add(function(){
                        game.chest_win.visible = false;
                        game.action1.visible = false;
                        game.gnome_win.visible = true;
                        setTimeout(function(){
                            game3_win_2Sound.play();
                        },150);
                        game.gnome_win.animations.getAnimation('game.gnome_win').play().onComplete.add(function(){
                            game.gnome_win.visible = false;
                            game.gnome_win2.visible = true;
                            // showRandMonkey2(x-100,-22+y);
                            game.gnome_win2.animations.getAnimation('game.gnome_win2').play().onComplete.add(function(){
                                game.gnome_win2.visible = false;
                                // game.gnome_left.visible = true;
                                showRandGnome2(x,326);
                                if(ropeStep == 5 && checkUpLvl == true) {
                                    setTimeout("game.state.start('game4');", 500);
                                } else if(ropeStep == 5) {
                                    updateBalanceGame3(game, scorePosions, balanceR);
                                }
                            });
                        });
                    });
                }
            };
        });
});

}

setTimeout("unlockDisplay();", 4500);
}

// showRandMonkey2(0,0);
showRandGnome2(304,326);

full_and_sound();


};

game3.update = function () {
    if (game.scale.isFullScreen)
    {
      full.loadTexture('game.full');
      fullStatus = true;
  }
  else
  {
   full.loadTexture('game.non_full');
   fullStatus = false;
}
};

game.state.add('game3', game3);

})();

}