(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        game.load.image('game.background', needUrlPath + '/img/shape1206.svg');
        game.load.image('game.background1', needUrlPath + '/img/main_bg.png');

        game.load.image('startButton', needUrlPath + '/img/mobileButtons/spin.png');
        game.load.image('startButton_p', needUrlPath + '/img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', needUrlPath + '/img/mobileButtons/spin_d.png');
        game.load.image('bet1', needUrlPath + '/img/mobileButtons/bet1.png');
        game.load.image('bet1_p', needUrlPath + '/img/mobileButtons/bet1_p.png');
        game.load.image('home', needUrlPath + '/img/mobileButtons/home.png');
        game.load.image('home_p', needUrlPath + '/img/mobileButtons/home_p.png');
        game.load.image('dollar', needUrlPath + '/img/mobileButtons/dollar.png');
        game.load.image('gear', needUrlPath + '/img/mobileButtons/gear.png');
        game.load.image('double', needUrlPath + '/img/mobileButtons/double.png');
        game.load.image('collect', needUrlPath + '/img/mobileButtons/collect.png');
        game.load.image('collect_p', needUrlPath + '/img/mobileButtons/collect_p.png');
        game.load.image('collect_d', needUrlPath + '/img/mobileButtons/collect_d.png');

        game.load.image('game.number1', needUrlPath + '/img/win_1.png');
        game.load.image('game.number2', needUrlPath + '/img/win_2.png');
        game.load.image('game.number3', needUrlPath + '/img/win_3.png');
        game.load.image('game.number4', needUrlPath + '/img/win_4.png');
        game.load.image('game.number5', needUrlPath + '/img/win_5.png');
        game.load.image('game.number6', needUrlPath + '/img/win_6.png');
        game.load.image('game.number7', needUrlPath + '/img/win_7.png');
        game.load.image('game.number8', needUrlPath + '/img/win_8.png');
        game.load.image('game.number9', needUrlPath + '/img/win_9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.audio('sound', needUrlPath + '/sounds/spin.mp3');
        game.load.audio('rotate', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('stop', needUrlPath + '/sounds/stop.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('play', needUrlPath + '/sounds/play.mp3');

        game.load.image('line1', needUrlPath + '/img/lines/select/line1.png');
        game.load.image('line2', needUrlPath + '/img/lines/select/line2.png');
        game.load.image('line3', needUrlPath + '/img/lines/select/line3.png');
        game.load.image('line4', needUrlPath + '/img/lines/select/line4.png');
        game.load.image('line5', needUrlPath + '/img/lines/select/line5.png');
        game.load.image('line6', needUrlPath + '/img/lines/select/line6.png');
        game.load.image('line7', needUrlPath + '/img/lines/select/line7.png');
        game.load.image('line8', needUrlPath + '/img/lines/select/line8.png');
        game.load.image('line9', needUrlPath + '/img/lines/select/line9.png');

        game.load.image('linefull1', needUrlPath + '/img/lines/win/linefull1.png');
        game.load.image('linefull2', needUrlPath + '/img/lines/win/linefull2.png');
        game.load.image('linefull3', needUrlPath + '/img/lines/win/linefull3.png');
        game.load.image('linefull4', needUrlPath + '/img/lines/win/linefull4.png');
        game.load.image('linefull5', needUrlPath + '/img/lines/win/linefull5.png');
        game.load.image('linefull6', needUrlPath + '/img/lines/win/linefull6.png');
        game.load.image('linefull7', needUrlPath + '/img/lines/win/linefull7.png');
        game.load.image('linefull8', needUrlPath + '/img/lines/win/linefull8.png');
        game.load.image('linefull9', needUrlPath + '/img/lines/win/linefull9.png');

        game.load.audio('line1Sound', needUrlPath + '/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sounds/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sounds/stop.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('play', needUrlPath + '/sounds/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sounds/takeWin.mp3');
        // game.load.audio('game.winCards', needUrlPath + '/sounds/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sounds/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sounds/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sounds/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sounds/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sounds/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sounds/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sounds/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sounds/winLines/sound19.mp3');

        // game.load.spritesheet('game.flashNamber1', needUrlPath + '/img/flashingNumber1.png', 608, 32);
        // game.load.spritesheet('game.flashNamber2', needUrlPath + '/img/flashingNumber2.png', 608, 32);
        // game.load.spritesheet('game.flashNamber3', needUrlPath + '/img/flashingNumber3.png', 608, 32);
        // game.load.spritesheet('game.flashNamber4', needUrlPath + '/img/flashingNumber4.png', 608, 32);
        // game.load.spritesheet('game.flashNamber5', needUrlPath + '/img/flashingNumber5.png', 608, 32);
        // game.load.spritesheet('game.flashNamber6', needUrlPath + '/img/flashingNumber6.png', 608, 32);
        // game.load.spritesheet('game.flashNamber7', needUrlPath + '/img/flashingNumber7.png', 608, 32);
        // game.load.spritesheet('game.flashNamber8', needUrlPath + '/img/flashingNumber8.png', 608, 32);
        // game.load.spritesheet('game.flashNamber9', needUrlPath + '/img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.gold_row_win', needUrlPath + '/img/gold_row_win.png', 96, 96);
        // game.load.spritesheet('game.monkey', needUrlPath + '/img/monkey.png', 96, 96);
        game.load.audio('gnomeWin', needUrlPath + '/sounds/gnomeWin.mp3');


        game.load.image('game.backgroundGame2', needUrlPath + '/img/bg_game2.png');
        game.load.audio('openCard', needUrlPath + '/sounds/sound31.mp3');
        game.load.audio('winCard', needUrlPath + '/sounds/sound30.mp3');

        //карты
        game.load.image('card_bg', needUrlPath + '/img/shirt_cards.png');

        game.load.image('card_39', needUrlPath + '/img/cards/2b.png');
        game.load.image('card_40', needUrlPath + '/img/cards/3b.png');
        game.load.image('card_41', needUrlPath + '/img/cards/4b.png');
        game.load.image('card_42', needUrlPath + '/img/cards/5b.png');
        game.load.image('card_43', needUrlPath + '/img/cards/6b.png');
        game.load.image('card_44', needUrlPath + '/img/cards/7b.png');
        game.load.image('card_45', needUrlPath + '/img/cards/8b.png');
        game.load.image('card_46', needUrlPath + '/img/cards/9b.png');
        game.load.image('card_47', needUrlPath + '/img/cards/10b.png');
        game.load.image('card_48', needUrlPath + '/img/cards/11b.png');
        game.load.image('card_49', needUrlPath + '/img/cards/12b.png');
        game.load.image('card_50', needUrlPath + '/img/cards/13b.png');
        game.load.image('card_51', needUrlPath + '/img/cards/14b.png');

        game.load.image('card_26', needUrlPath + '/img/cards/2c.png');
        game.load.image('card_27', needUrlPath + '/img/cards/3c.png');
        game.load.image('card_28', needUrlPath + '/img/cards/4c.png');
        game.load.image('card_29', needUrlPath + '/img/cards/5c.png');
        game.load.image('card_30', needUrlPath + '/img/cards/6c.png');
        game.load.image('card_31', needUrlPath + '/img/cards/7c.png');
        game.load.image('card_32', needUrlPath + '/img/cards/8c.png');
        game.load.image('card_33', needUrlPath + '/img/cards/9c.png');
        game.load.image('card_34', needUrlPath + '/img/cards/10c.png');
        game.load.image('card_35', needUrlPath + '/img/cards/11c.png');
        game.load.image('card_36', needUrlPath + '/img/cards/12c.png');
        game.load.image('card_37', needUrlPath + '/img/cards/13c.png');
        game.load.image('card_38', needUrlPath + '/img/cards/14c.png');

        game.load.image('card_0', needUrlPath + '/img/cards/2t.png');
        game.load.image('card_1', needUrlPath + '/img/cards/3t.png');
        game.load.image('card_2', needUrlPath + '/img/cards/4t.png');
        game.load.image('card_3', needUrlPath + '/img/cards/5t.png');
        game.load.image('card_4', needUrlPath + '/img/cards/6t.png');
        game.load.image('card_5', needUrlPath + '/img/cards/7t.png');
        game.load.image('card_6', needUrlPath + '/img/cards/8t.png');
        game.load.image('card_7', needUrlPath + '/img/cards/9t.png');
        game.load.image('card_8', needUrlPath + '/img/cards/10t.png');
        game.load.image('card_9', needUrlPath + '/img/cards/11t.png');
        game.load.image('card_10', needUrlPath + '/img/cards/12t.png');
        game.load.image('card_11', needUrlPath + '/img/cards/13t.png');
        game.load.image('card_12', needUrlPath + '/img/cards/14t.png');

        game.load.image('card_13', needUrlPath + '/img/cards/2p.png');
        game.load.image('card_14', needUrlPath + '/img/cards/3p.png');
        game.load.image('card_15', needUrlPath + '/img/cards/4p.png');
        game.load.image('card_16', needUrlPath + '/img/cards/5p.png');
        game.load.image('card_17', needUrlPath + '/img/cards/6p.png');
        game.load.image('card_18', needUrlPath + '/img/cards/7p.png');
        game.load.image('card_19', needUrlPath + '/img/cards/8p.png');
        game.load.image('card_20', needUrlPath + '/img/cards/9p.png');
        game.load.image('card_21', needUrlPath + '/img/cards/10p.png');
        game.load.image('card_22', needUrlPath + '/img/cards/11p.png');
        game.load.image('card_23', needUrlPath + '/img/cards/12p.png');
        game.load.image('card_24', needUrlPath + '/img/cards/13p.png');
        game.load.image('card_25', needUrlPath + '/img/cards/14p.png');
        game.load.image('card_52', needUrlPath + '/img/cards/joker.png');

        game.load.image('pick', needUrlPath + '/img/pick.png');

        game.load.spritesheet('fireplace', needUrlPath + '/img/fireplace.png', 128, 48, 4);
        game.load.spritesheet('gear2', needUrlPath + '/img/gear2.png', 96, 32, 16);
        game.load.spritesheet('gnome_anim1_1', needUrlPath + '/img/gnome_anim1_1.png', 240, 176, 17);
        game.load.spritesheet('gnome_anim1_2', needUrlPath + '/img/gnome_anim1_2.png', 240, 176, 17);
        game.load.spritesheet('gold_row_win', needUrlPath + '/img/gold_row_win.png', 96, 96, 3);

        for (var i = 1; i <= 9; ++i) {
            if (i % 2 != 0) {           
                game.load.image('game.lever' + i, needUrlPath + '/img/lever' + i + '.png');            
                game.load.image('game.lever_p' + i, needUrlPath + '/img/lever_p' + i + '.png');            
            }
        }
        game.load.image('game.backgroundGame3', needUrlPath + '/img/bg_game3.png');
        game.load.image('game.chest_open', needUrlPath + '/img/game3_chest_open.png');
        game.load.spritesheet('game.action1', needUrlPath + '/img/game3_action1.png', 192, 176, 12);
        game.load.spritesheet('game.gnome_left', needUrlPath + '/img/game3_gnome_left.png', 160, 176, 23);
        game.load.spritesheet('game.gnome_right', needUrlPath + '/img/game3_gnome_right.png', 192, 192, 18);
        game.load.spritesheet('game.chest_action1', needUrlPath + '/img/game3_chest_action1.png', 128, 112, 9);
        game.load.spritesheet('game.chest_win', needUrlPath + '/img/game3_chest_win.png', 128, 240, 22);
        game.load.spritesheet('game.gnome_win', needUrlPath + '/img/game3_gnome_win.png', 160, 237, 11);
        game.load.spritesheet('game.gnome_win2', needUrlPath + '/img/game3_gnome_win2.png', 160, 176, 3);
        game.load.spritesheet('game.chest_lose', needUrlPath + '/img/game3_chest_lose.png', 128, 112, 23);
        game.load.spritesheet('game.gnome_lose', needUrlPath + '/img/game3_gnome_lose.png', 192, 336, 10);
        game.load.spritesheet('game.gnome_lose2', needUrlPath + '/img/game3_gnome_lose2.png', 192, 192, 10);

        game.load.audio('winGame3', needUrlPath + '/sounds/winGame3.mp3');
        game.load.audio('game3_1', needUrlPath + '/sounds/game3_1.mp3');
        game.load.audio('game3_2', needUrlPath + '/sounds/game3_2.mp3');
        game.load.audio('game3_lose', needUrlPath + '/sounds/game3_lose.mp3');
        game.load.audio('game3_win_2', needUrlPath + '/sounds/game3_win_2.mp3');


        game.load.image('game.backgroundGame4', needUrlPath + '/img/bg_game4.png');
        game.load.image('game.chest_open3', needUrlPath + '/img/game4_chest_open.png');
        game.load.image('game.chest_open2', needUrlPath + '/img/game4_chest_open2.png');
        game.load.image('game.gold', needUrlPath + '/img/game4_gold.png');
        game.load.image('game.gnome4_win2', needUrlPath + '/img/game4_gnome_win2.png');
        game.load.image('game.chest_left', needUrlPath + '/img/chest_left.png');
        game.load.image('game.chest_right', needUrlPath + '/img/chest_right.png');
        game.load.spritesheet('game.gnome4_1', needUrlPath + '/img/game4_gnome_1.png', 192, 176, 15);
        game.load.spritesheet('game.gnome4_2', needUrlPath + '/img/game4_gnome_2.png', 192, 176, 10);
        game.load.spritesheet('game.gnome4_key', needUrlPath + '/img/game4_gnome_key.png', 192, 176, 20);
        game.load.spritesheet('game.chest_win_left', needUrlPath + '/img/game4_chest_win_left.png', 128, 128, 6);
        game.load.spritesheet('game.chest_win_right', needUrlPath + '/img/game4_chest_win_right.png', 128, 128, 6);
        game.load.spritesheet('game.chest_lose_left', needUrlPath + '/img/game4_chest_lose_left.png', 128, 128, 6);
        game.load.spritesheet('game.chest_lose_right', needUrlPath + '/img/game4_chest_lose_right.png', 128, 128, 6);
        game.load.spritesheet('game.gnome4_win', needUrlPath + '/img/game4_gnome_win.png', 208, 285, 15);
        game.load.spritesheet('game.gnome4_win3', needUrlPath + '/img/game4_gnome_win3.png', 160, 176, 5);
        game.load.spritesheet('game.gnome4_lose', needUrlPath + '/img/game4_gnome_lose.png', 208, 176, 6);

        // game.load.image('game.backgroundGame2', needUrlPath + '/img/shape1150.png');
        // game.load.spritesheet('game.rope', needUrlPath + '/img/rope.png', 16, 320);

        // game.load.spritesheet('game.monkey21', needUrlPath + '/img/monkey21.png', 176, 480);
        // game.load.spritesheet('game.monkey22', needUrlPath + '/img/monkey22.png', 176, 480);
        // game.load.spritesheet('game.monkey23', needUrlPath + '/img/monkey23.png', 176, 480);
        // game.load.spritesheet('game.monkey24', needUrlPath + '/img/monkey24.png', 176, 480);

        // game.load.spritesheet('game.monkey21H', needUrlPath + '/img/monkey21H.png', 176, 480);
        // game.load.spritesheet('game.monkey22H', needUrlPath + '/img/monkey22H.png', 176, 480);
        // game.load.spritesheet('game.monkey23H', needUrlPath + '/img/monkey23H.png', 176, 480);
        // game.load.spritesheet('game.monkey24H', needUrlPath + '/img/monkey24H.png', 176, 480);

        // game.load.spritesheet('game.monkeyTakeRope1', needUrlPath + '/img/monkeyTakeRope1.png', 176, 480);
        // game.load.spritesheet('game.monkeyTakeRope2', needUrlPath + '/img/monkeyTakeRope2.png', 176, 480);
        // game.load.spritesheet('game.monkeyTakeRope1H', needUrlPath + '/img/monkeyTakeRope1H.png', 176, 480);
        // game.load.spritesheet('game.monkeyTakeRope2H', needUrlPath + '/img/monkeyTakeRope2H.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatBanana', needUrlPath + '/img/monkeyEatBanana.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatBananaH', needUrlPath + '/img/monkeyEatBananaH.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatHammer1', needUrlPath + '/img/monkeyEatHammer1.png', 160, 480);
        // game.load.spritesheet('game.monkeyEatHammer12', needUrlPath + '/img/monkeyEatHammer12.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatHammer2', needUrlPath + '/img/monkeyEatHammer2.png', 160, 480);
        // game.load.spritesheet('game.monkeyEatHammer22', needUrlPath + '/img/monkeyEatHammer22.png', 176, 480);
        // game.load.spritesheet('game.monkeyHeadache', needUrlPath + '/img/monkeyHeadache.png', 176, 480);

        // game.load.audio('game.winRope1', 'sound/winRope1.mp3');
        // game.load.audio('game.winRope2', 'sound/winRope2.mp3');
        // game.load.audio('game.bitMonkey', 'sound/bitMonkey.mp3');

        // game.load.image('game.backgroundGame4', needUrlPath + '/img/shape943.svg');

        // game.load.spritesheet('game.mankeyHanging1', needUrlPath + '/img/mankeyHanging1.png', 240, 304);
        // game.load.spritesheet('game.mankeyHanging2', needUrlPath + '/img/mankeyHanging2.png', 240, 304);
        // game.load.spritesheet('game.mankeyHanging3', needUrlPath + '/img/mankeyHanging3.png', 240, 304);
        // game.load.spritesheet('game.mankeyLeftPoint', needUrlPath + '/img/mankeyLeftPoint.png', 240, 304);
        // game.load.spritesheet('game.mankeyLosePoint', needUrlPath + '/img/mankeyLosePoint.png', 240, 304);
        // game.load.spritesheet('game.mankeyRightPoint', needUrlPath + '/img/mankeyRightPoint.png', 240, 304);
        // game.load.spritesheet('game.mankeyWinPoint', needUrlPath + '/img/mankeyWinPoint.png', 240, 304);

        // game.load.spritesheet('game.goldPoint', needUrlPath + '/img/goldPoint.png', 96, 128);
        // game.load.spritesheet('game.questionPoint', needUrlPath + '/img/questionPoint.png', 96, 80);
        // game.load.spritesheet('game.spiderPoint', needUrlPath + '/img/spiderPoint.png', 96, 128);
        // game.load.spritesheet('game.flashFive', needUrlPath + '/img/flashFive.png', 32, 48);
        // game.load.spritesheet('game.take_or_risk_anim', needUrlPath + '/img/take_or_risk_anim.png', 192, 160);

        // game.load.image('game.take_or_risk_bg', needUrlPath + '/img/shape953.svg');
        // game.load.audio('game.winPoint1', 'sound/winPoint1.mp3');
        // game.load.audio('game.winPoint2', 'sound/winPoint2.mp3');
        game.load.audio('game.losePoint', needUrlPath + '/sounds/losePoint.mp3');
        game.load.audio('game4_win', needUrlPath + '/sounds/game4_win.mp3');
        game.load.audio('game4_lose', needUrlPath + '/sounds/game4_lose.mp3');

        // game.load.image('game.monkeyR', needUrlPath + '/img/monkeyR.png');
        // game.load.image('game.butterflyR', needUrlPath + '/img/butterflyR.png');

        game.load.image('cell0', needUrlPath + '/img/0.png');
        game.load.image('cell1', needUrlPath + '/img/1.png');
        game.load.image('cell2', needUrlPath + '/img/2.png');
        game.load.image('cell3', needUrlPath + '/img/3.png');
        game.load.image('cell4', needUrlPath + '/img/4.png');
        game.load.image('cell5', needUrlPath + '/img/5.png');
        game.load.image('cell6', needUrlPath + '/img/6.png');
        game.load.image('cell7', needUrlPath + '/img/7.png');
        game.load.image('cell8', needUrlPath + '/img/8.png');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.png', 96, 96);

        game.load.image('bonusGame', needUrlPath + '/img/image536.png');
        game.load.image('wildSymbol', needUrlPath + '/img/image537.png');
        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('takeOrRisk1', needUrlPath + '/img/image474.png');
        game.load.image('takeOrRisk2', needUrlPath + '/img/image475.png');
        game.load.image('take', needUrlPath + '/img/image554.png');

        game.load.image('topScoreGame1', needUrlPath + '/img/main_bg_1.png');
        game.load.image('topScoreGame2', needUrlPath + '/img/main_bg_2.png');
        game.load.image('topScoreGame3', needUrlPath + '/img/main_bg_3.png');
        game.load.image('topScoreGame4', needUrlPath + '/img/main_bg_4.png');

        game.load.image('loseTitleGame2', needUrlPath + '/img/image484.png');
        game.load.image('winTitleGame2', needUrlPath + '/img/image486.png');
        game.load.image('forwadTitleGame2', needUrlPath + '/img/image504.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/gold_row_win.png', 96, 96);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

