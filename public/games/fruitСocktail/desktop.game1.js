var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var gamename = 'gnomerus';
var checkHelm = false;

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');

        //var betOneSound = game.add.audio('game.betOneSound');
        //var betMaxSound = game.add.audio('game.betMaxSound');

        slotPosition = [[142, 87], [142, 199], [142, 311], [254, 87], [254, 199], [254, 311], [365, 87], [365, 199], [365, 311], [477, 87], [477, 199], [477, 311], [589, 87], [589, 199], [589, 311]];
        addSlots(game, slotPosition);
        
        // изображения
        
        game.add.sprite(94,22, 'game.background1');
        ice = game.add.sprite(95, 406, 'ice');
        iceAnimation = ice.animations.add('ice', [0,1,2,3,4,5,6,7,8,9], 8, true).play();
        ice.visible = true;  
        winst = game.add.sprite(238, 387, 'win');
        winstAnimation = winst.animations.add('win', [0,1,2,3,4,5,6,7,0], 6, false);
        winst.visible = false;  
        game.add.sprite(0,0, 'game.background');
        topBarImage = game.add.sprite(94,22, 'topScoreGame1');
        
        addTableTitle(game, 'play1To',526,436);
        
        var linePosition = [[134,243], [134,110], [134,383], [134,158], [134,144], [134,132], [134,318], [134,270], [134,159]];
        var numberPosition = [[94,230], [94,86], [94,374], [94,150], [94,310], [94,118], [94,342], [94,262], [94,198]];
        for (var i = 1; i <= 9; ++i) {
            game.add.sprite(94, numberPosition[i-1][1], 'unwin_' + i);
        }   
        addLinesAndNumbers(game, linePosition, numberPosition);
        
        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);

        var pageCount = 5;

        // кнопки
        addButtonsGame1(game, pageCount);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[255, 26, 20], [455, 26, 20], [640, 26, 20], [98, 59, 16], [706, 59, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        // анимация

        strawberry_eyes2 = game.add.sprite(318, 422, 'strawberry_eyes2');
        strawberry_eyes2Animation = strawberry_eyes2.animations.add('strawberry_eyes2', [0,1,2,3,4,5,6,7,8], 6, false).play();
        strawberry_eyes2.visible = true;           
        strawberry_eyes = game.add.sprite(318, 422, 'strawberry_eyes');
        strawberry_eyesAnimation = strawberry_eyes.animations.add('strawberry_eyes', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
        strawberry_eyes.visible = false;  
        strawberry_eyes2Animation.onComplete.add(function(){
            strawberry_eyes2.visible = false;
            strawberry_eyes.visible = true;
            strawberry_eyesAnimation.play();
        });
        strawberry_eyesAnimation.onComplete.add(function(){
            strawberry_eyes.visible = false;
            strawberry_eyes2.visible = true;
            strawberry_eyes2Animation.play();
        });
        winstAnimation.onComplete.add(function(){
            winst.visible = false; 
            strawberry_eyes2Animation.play();
            strawberry_eyes2.visible = true;  
        });


        var pageCoord = [[94, 22], [94, 22], [94, 22], [94, 22], [94, 22]];   
        var btnCoord = [[95, 421], [341, 427], [559, 427]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);

        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
         full.loadTexture('game.non_full');
         fullStatus = false;
     }
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
            if(checkHelm == true) {

            }
        }
    };

    game.state.add('game1', game1);

};
