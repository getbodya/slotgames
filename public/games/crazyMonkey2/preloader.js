var gamePreload = {

	preload:function(){
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            $('#preLoaderProgress').css("width", progress + '%');
        });

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.setResizeCallback(function() {       
        });
        // game.load.image('main_window', 'img/main_window.png');

        game.load.image('Background', 'img/Background.jpg');
        game.load.image('Background2', 'img/fsBackground.jpg');
        game.load.image('BG1_bottom', 'img/BG1_bottom.png');
        game.load.image('BG2_bottom', 'img/BG2_bottom.png');
        game.load.image('ReelContainer', 'img/ReelContainer.png');
        game.load.image('ReelContainer2', 'img/fsReelContainer.png');
        game.load.image('Logo1', 'img/Logo1.png');
        game.load.image('Logo2', 'img/Logo2.png');
        game.load.image('freeSpins', 'img/txtFreeSpins.png');
        game.load.image('txtMultiplier', 'img/txtMultiplier.png');
        game.load.image('FSCongrats', 'img/FSCongrats.jpg');
        game.load.image('FreeSpinsWinSum', 'img/txtFreeSpinsWinSum.png');
        game.load.image('FreeSpinsWinSumCoin', 'img/txtFreeSpinsWinSumCoin.png');
        game.load.image('introBonus', 'img/introBonus.png');

        game.load.image('amount', 'img/btns/amount.png');
        game.load.image('amount_top', 'img/btns/amount_top.png');
        game.load.image('bet_btn_bottom', 'img/btns/bet_btn_bottom.png');
        game.load.image('bet_btn_middle', 'img/btns/bet_btn_middle.png');
        game.load.image('bet_btn_top', 'img/btns/bet_btn_top.png');
        game.load.image('bet', 'img/btns/bet.png');
        game.load.image('bet_d', 'img/btns/bet_d.png');
        game.load.image('bet_p', 'img/btns/bet_p.png');
        game.load.image('credits', 'img/btns/credits.png');
        game.load.image('spin', 'img/btns/spin.png');
        game.load.image('spin_d', 'img/btns/spin_d.png');
        game.load.image('spin_p', 'img/btns/spin_p.png');
        game.load.image('win', 'img/btns/win.png');
        game.load.image('win2', 'img/btns/win2.png');

        game.load.image('cell0', 'img/Symbol0.png');
        game.load.image('cell1', 'img/Symbol1.png');
        game.load.image('cell2', 'img/Symbol2.png');
        game.load.image('cell3', 'img/Symbol3.png');
        game.load.image('cell4', 'img/Symbol4.png');
        game.load.image('cell5', 'img/Symbol5.png');
        game.load.image('cell6', 'img/Symbol6.png');
        game.load.image('cell7', 'img/Symbol7.png');
        game.load.image('cell8', 'img/Symbol8.png');
        game.load.image('cell9', 'img/Symbol9.png');
        game.load.image('cell10', 'img/Symbol10.png');
        game.load.image('cell11', 'img/Symbol11.png');
        game.load.image('bar', 'img/bar.png');

        game.load.atlasJSONHash('fisherman1', 'fisherman_sprite.png', 'fisherman_sprite_1.json');
        game.load.atlasJSONHash('fisherman2', 'fisherman_sprite.png', 'fisherman_sprite_2.json');
        game.load.atlasJSONHash('fisherman3', 'fisherman_sprite.png', 'fisherman_sprite_3.json');
        game.load.atlasJSONHash('dog_idle', 'dog_fish_spot_sprite.png', 'dog_idle.json');
        game.load.atlasJSONHash('dog_look_left', 'dog_fish_spot_sprite.png', 'dog_look_left.json');
        game.load.atlasJSONHash('dog_look_right', 'dog_fish_spot_sprite.png', 'dog_look_right.json');
        game.load.atlasJSONHash('fish1', 'dog_fish_spot_sprite.png', 'fish_1.json');
        game.load.atlasJSONHash('fish2', 'dog_fish_spot_sprite.png', 'fish_2.json');
        game.load.atlasJSONHash('Spot', 'dog_fish_spot_sprite.png', 'Spot.json');
        game.load.atlasJSONHash('Spot2', 'dog_fish_spot_sprite2.png', 'Spot.json');

        game.load.image('BonusBackground', 'img/BonusBackground.jpg');
        game.load.image('TotalWinPlaque', 'img/TotalWinPlaque.png');
        game.load.spritesheet('picks_left_1', 'img/picks_left_1.png', 182, 131, 10);
        game.load.spritesheet('picks_left_2', 'img/picks_left_2.png', 182, 131, 10);
        game.load.spritesheet('picks_left_3', 'img/picks_left_3.png', 182, 131, 10);
        game.load.spritesheet('picks_left_4', 'img/picks_left_4.png', 182, 131, 10);
        game.load.spritesheet('picks_left_5', 'img/picks_left_5.png', 182, 131, 10);

        game.load.spritesheet('BigFish_1', 'img/txtBigFish_1.png', 860, 442, 4);
        game.load.spritesheet('BigFish_2', 'img/txtBigFish_2.png', 860, 442, 3);
        game.load.spritesheet('BigFish_3', 'img/txtBigFish_3.png', 860, 442, 4);
        game.load.spritesheet('BigFish_4', 'img/txtBigFish_4.png', 860, 442, 3);
        game.load.spritesheet('BigFish_5', 'img/txtBigFish_5.png', 860, 442, 4);
        game.load.spritesheet('BigFish_6', 'img/txtBigFish_6.png', 860, 442, 4);
        game.load.spritesheet('BigFish_7', 'img/txtBigFish_7.png', 860, 442, 1);

        game.load.spritesheet('MediumFish_1', 'img/txtMediumFish_1.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_2', 'img/txtMediumFish_2.png', 860, 442, 3);
        game.load.spritesheet('MediumFish_3', 'img/txtMediumFish_3.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_4', 'img/txtMediumFish_4.png', 860, 442, 3);
        game.load.spritesheet('MediumFish_5', 'img/txtMediumFish_5.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_6', 'img/txtMediumFish_6.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_7', 'img/txtMediumFish_7.png', 860, 442, 1);

        game.load.spritesheet('SmallFish_1', 'img/txtSmallFish_1.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_2', 'img/txtSmallFish_2.png', 860, 442, 3);
        game.load.spritesheet('SmallFish_3', 'img/txtSmallFish_3.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_4', 'img/txtSmallFish_4.png', 860, 442, 3);
        game.load.spritesheet('SmallFish_5', 'img/txtSmallFish_5.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_6', 'img/txtSmallFish_6.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_7', 'img/txtSmallFish_7.png', 860, 442, 1);

        game.load.spritesheet('bonus_Picks_5_1', 'img/bonus_Picks_5_intro_1.png', 520, 360, 7);
        game.load.spritesheet('bonus_Picks_5_2', 'img/bonus_Picks_5_intro_2.png', 520, 360, 7);
        game.load.spritesheet('bonus_Picks_5_3', 'img/bonus_Picks_5_loop_1.png', 548, 360, 7);
        game.load.spritesheet('bonus_Picks_5_4', 'img/bonus_Picks_5_loop_2.png', 530, 360, 7);
        game.load.spritesheet('bonus_Picks_5_5', 'img/bonus_Picks_5_exit_1.png', 520, 360, 7);
        game.load.spritesheet('bonus_Picks_5_6', 'img/bonus_Picks_5_exit_2.png', 520, 360, 7);
        
        game.load.spritesheet('Congratulations1', 'img/bonus_Congratulations_loop_1.png', 742, 371, 5);
        game.load.spritesheet('Congratulations2', 'img/bonus_Congratulations_loop_2.png', 742, 371, 5);
        game.load.spritesheet('Congratulations3', 'img/bonus_Congratulations_loop_3.png', 742, 371, 5);

        game.load.audio('sound', 'sounds/AlaskanFishing.mp3');

        game.load.spritesheet('cellAnim_0_1', 'img/Symbol0_WinAnim_gif_1.png', 174, 174);
        game.load.spritesheet('cellAnim_0_2', 'img/Symbol0_WinAnim_gif_2.png', 174, 174);
        game.load.spritesheet('cellAnim_1', 'img/Symbol1_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_2', 'img/Symbol2_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_3', 'img/Symbol3_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_4', 'img/Symbol4_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_5', 'img/Symbol5_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_6', 'img/Symbol6_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_7', 'img/Symbol7_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_8', 'img/Symbol8_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_9', 'img/Symbol9_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_10', 'img/Symbol10_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_11', 'img/Symbol11_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_12', 'img/Symbol1_Land.png', 174, 174);
        game.load.spritesheet('cellAnim_13', 'img/Symbol11_Land.png', 174, 174);

        game.load.spritesheet('cellAnim_0_1fs', 'img/Symbol0_fs_WinAnim_gif_1.png', 174, 174);
        game.load.spritesheet('cellAnim_0_2fs', 'img/Symbol0_fs_WinAnim_gif_2.png', 174, 174);
        game.load.spritesheet('cellAnim_1fs', 'img/Symbol1_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_2fs', 'img/Symbol2_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_3fs', 'img/Symbol3_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_4fs', 'img/Symbol4_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_5fs', 'img/Symbol5_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_6fs', 'img/Symbol6_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_7fs', 'img/Symbol7_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_8fs', 'img/Symbol8_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_9fs', 'img/Symbol9_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_10fs', 'img/Symbol1_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_11fs', 'img/Symbol1_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_12fs', 'img/Symbol1_WinAnim_1_1.png', 174, 174);
        game.load.spritesheet('cellAnim_13fs', 'img/Symbol1_WinAnim_1_2.png', 174, 174);

        game.load.spritesheet('cellAnimLand_1', 'img/Symbol1_Land.png', 174, 174);
        game.load.spritesheet('cellAnimLand_11', 'img/Symbol11_Land.png', 174, 174);
        game.load.spritesheet('cellAnimBonus_1', 'img/Symbol1_WinAnim_1_1.png', 174, 174);
        game.load.spritesheet('cellAnimBonus_2', 'img/Symbol1_WinAnim_1_2.png', 174, 174);

    },
    create:function(){
        game.state.start('game2'); //переключение на 1 игру
        document.getElementById('preloader').style.display = 'none';
    }
};
game.state.add('gamePreload', gamePreload); //добавление загрузчика в игру


// запуск игры

var sessionName;
$.ajax({
    type: "get",
    // url: 'http://localhost/inApi/old/slotgames/public/init',
    //url: 'http://localhost/inApi/old/slotgames/public/init',
    url: 'http://slotportal/test1.php',
    dataType: 'html',
    success: function (data) {

        dataString = data;

        if(dataString) {

            sessionName = data;



            $.ajax({
                type: "get",
                // url: "http://localhost/inApi/old/slotgames/public/state?sessionName="+sessionName,
                //url: "http://localhost/inApi/old/slotgames/public/state?sessionName="+sessionName,                
                url: 'http://slotportal/test2.php',
                dataType: 'html',
                success: function (data) {

                    dataArray = JSON.parse(data);

                    if(dataArray['state']) {

                        balance = dataArray['balance'];
                        info = dataArray['info'];

                                                game.state.start('gamePreload'); //начало игры с локации загрузчика

                                            } else {
                                                alert('Ошбика 2');
                                            }

                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                         var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                                         alert(errorText);
                                     }
                                 });



        } else {
            alert('Ошибка 1');
        }

    },
    error: function (xhr, ajaxOptions, thrownError) {
        var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
        console.log(errorText);
    }
});
