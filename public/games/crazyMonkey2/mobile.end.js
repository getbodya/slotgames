(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('game.background', 'img/canvas-bg.svg');
        game.load.image('game.background1', 'img/shape319.png');

        game.load.image('startButton', 'img/mobileButtons/spin.png');
        game.load.image('startButton_p', 'img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', 'img/mobileButtons/spin_d.png');
        game.load.image('bet1', 'img/mobileButtons/bet1.png');
        game.load.image('bet1_p', 'img/mobileButtons/bet1_p.png');
        game.load.image('home', 'img/mobileButtons/home.png');
        game.load.image('home_p', 'img/mobileButtons/home_p.png');
        game.load.image('dollar', 'img/mobileButtons/dollar.png');
        game.load.image('gear', 'img/mobileButtons/gear.png');
        game.load.image('double', 'img/mobileButtons/double.png');
        game.load.image('collect', 'img/mobileButtons/collect.png');
        game.load.image('collect_p', 'img/mobileButtons/collect_p.png');
        game.load.image('collect_d', 'img/mobileButtons/collect_d.png');

        game.load.image('game.number1', 'img/1.png');
        game.load.image('game.number2', 'img/2.png');
        game.load.image('game.number3', 'img/3.png');
        game.load.image('game.number4', 'img/4.png');
        game.load.image('game.number5', 'img/5.png');
        game.load.image('game.number6', 'img/6.png');
        game.load.image('game.number7', 'img/7.png');
        game.load.image('game.number8', 'img/8.png');
        game.load.image('game.number9', 'img/9.png');

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.audio('sound', 'sound/spin.mp3');
        game.load.audio('rotate', 'sound/rotate.wav');
        game.load.audio('stop', 'sound/stop.wav');
        game.load.audio('tada', 'sound/tada.wav');
        game.load.audio('play', 'sound/play.mp3');

        game.load.image('line1', 'lines/select/1.png');
        game.load.image('line2', 'lines/select/2.png');
        game.load.image('line3', 'lines/select/3.png');
        game.load.image('line4', 'lines/select/4.png');
        game.load.image('line5', 'lines/select/5.png');
        game.load.image('line6', 'lines/select/6.png');
        game.load.image('line7', 'lines/select/7.png');
        game.load.image('line8', 'lines/select/8.png');
        game.load.image('line9', 'lines/select/9.png');

        game.load.image('linefull1', 'lines/win/1.png');
        game.load.image('linefull2', 'lines/win/2.png');
        game.load.image('linefull3', 'lines/win/3.png');
        game.load.image('linefull4', 'lines/win/4.png');
        game.load.image('linefull5', 'lines/win/5.png');
        game.load.image('linefull6', 'lines/win/6.png');
        game.load.image('linefull7', 'lines/win/7.png');
        game.load.image('linefull8', 'lines/win/8.png');
        game.load.image('linefull9', 'lines/win/9.png');

        game.load.audio('line1Sound', 'lines/sounds/line1.wav');
        game.load.audio('line3Sound', 'lines/sounds/line3.wav');
        game.load.audio('line5Sound', 'lines/sounds/line5.wav');
        game.load.audio('line7Sound', 'lines/sounds/line7.wav');
        game.load.audio('line9Sound', 'lines/sounds/line9.wav');

        game.load.audio('sound', 'sound/spin.mp3');
        game.load.audio('rotateSound', 'sound/rotate.wav');
        game.load.audio('stopSound', 'sound/stop.wav');
        game.load.audio('tada', 'sound/tada.wav');
        game.load.audio('play', 'sound/play.mp3');
        game.load.audio('takeWin', 'sound/takeWin.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');

        game.load.audio('soundWinLine8', 'sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', 'sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', 'sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', 'sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', 'sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', 'sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', 'sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', 'sound/winLines/sound19.mp3');

        game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 608, 32);
        game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 608, 32);
        game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 608, 32);
        game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 608, 32);
        game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 608, 32);
        game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 608, 32);
        game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 608, 32);
        game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 608, 32);
        game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.mask', 'img/mask.png', 96, 96);
        game.load.spritesheet('game.monkey', 'img/monkey.png', 96, 96);
        game.load.audio('winMonkey', 'sound/winMonkey.mp3');


        game.load.image('game.backgroundGame3', 'img/shape164.svg');
        game.load.audio('openCard', 'sound/sound31.mp3');
        game.load.audio('winCard', 'sound/sound30.mp3');

        //карты
        game.load.image('card_bg', 'img/shape167.png');

        game.load.image('card_39', 'img/shape169.png');
        game.load.image('card_40', 'img/shape171.png');
        game.load.image('card_41', 'img/shape173.png');
        game.load.image('card_42', 'img/shape175.png');
        game.load.image('card_43', 'img/shape177.png');
        game.load.image('card_44', 'img/shape179.png');
        game.load.image('card_45', 'img/shape181.png');
        game.load.image('card_46', 'img/shape183.png');
        game.load.image('card_47', 'img/shape185.png');
        game.load.image('card_48', 'img/shape187.png');
        game.load.image('card_49', 'img/shape189.png');
        game.load.image('card_50', 'img/shape191.png');
        game.load.image('card_51', 'img/shape193.png');

        game.load.image('card_26', 'img/shape195.png');
        game.load.image('card_27', 'img/shape197.png');
        game.load.image('card_28', 'img/shape199.png');
        game.load.image('card_29', 'img/shape201.png');
        game.load.image('card_30', 'img/shape203.png');
        game.load.image('card_31', 'img/shape205.png');
        game.load.image('card_32', 'img/shape207.png');
        game.load.image('card_33', 'img/shape209.png');
        game.load.image('card_34', 'img/shape211.png');
        game.load.image('card_35', 'img/shape213.png');
        game.load.image('card_36', 'img/shape215.png');
        game.load.image('card_37', 'img/shape217.png');
        game.load.image('card_38', 'img/shape219.png');

        game.load.image('card_0', 'img/shape221.png');
        game.load.image('card_1', 'img/shape223.png');
        game.load.image('card_2', 'img/shape225.png');
        game.load.image('card_3', 'img/shape227.png');
        game.load.image('card_4', 'img/shape229.png');
        game.load.image('card_5', 'img/shape231.png');
        game.load.image('card_6', 'img/shape233.png');
        game.load.image('card_7', 'img/shape235.png');
        game.load.image('card_8', 'img/shape237.png');
        game.load.image('card_9', 'img/shape239.png');
        game.load.image('card_10', 'img/shape241.png');
        game.load.image('card_11', 'img/shape243.png');
        game.load.image('card_12', 'img/shape245.png');

        game.load.image('card_13', 'img/shape247.png');
        game.load.image('card_14', 'img/shape249.png');
        game.load.image('card_15', 'img/shape251.png');
        game.load.image('card_16', 'img/shape253.png');
        game.load.image('card_17', 'img/shape255.png');
        game.load.image('card_18', 'img/shape257.png');
        game.load.image('card_19', 'img/shape259.png');
        game.load.image('card_20', 'img/shape261.png');
        game.load.image('card_21', 'img/shape263.png');
        game.load.image('card_22', 'img/shape265.png');
        game.load.image('card_23', 'img/shape267.png');
        game.load.image('card_24', 'img/shape269.png');
        game.load.image('card_25', 'img/shape271.png');
        game.load.image('card_52', 'img/shape273.png');

        game.load.image('pick', 'img/shape281.png');

        game.load.spritesheet('game.monkey1', 'img/monkey1.png', 160, 144);
        game.load.spritesheet('game.monkey2', 'img/monkey2.png', 160, 144);
        game.load.spritesheet('game.monkey3', 'img/monkey3.png', 160, 144);
        game.load.spritesheet('game.monkey4', 'img/monkey4.png', 160, 144);
        game.load.spritesheet('game.monkey5', 'img/monkey5.png', 160, 144);
        game.load.spritesheet('game.monkeyWin', 'img/monkeyWin.png', 160, 144);
        game.load.spritesheet('game.monkeyTakeMushroom1', 'img/monkeyTakeMushroom1.png', 224, 144);
        game.load.spritesheet('game.monkeyTakeMushroom2', 'img/monkeyTakeMushroom2.png', 224, 144);
        game.load.spritesheet('game.butterfly1', 'img/butterfly1.png', 240, 96);
        game.load.spritesheet('game.butterfly2', 'img/butterfly2.png', 240, 96);
        game.load.spritesheet('game.butterfly3x', 'img/butterfly3x.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies1', 'img/butterflyFlies1.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies2', 'img/butterflyFlies2.png', 240, 96);
        game.load.spritesheet('game.mushroomGrow', 'img/mushroomGrow.png', 64, 80);
        game.load.spritesheet('game.mushroomJump', 'img/mushroomJump.png', 64, 80);

        game.load.image('game.backgroundGame2', 'img/shape1150.png');
        game.load.spritesheet('game.rope', 'img/rope.png', 16, 320);
        game.load.spritesheet('game.ropeButton', 'img/ropeButton.png', 64, 320);
        

        game.load.spritesheet('game.monkey21', 'img/monkey21.png', 176, 480);
        game.load.spritesheet('game.monkey22', 'img/monkey22.png', 176, 480);
        game.load.spritesheet('game.monkey23', 'img/monkey23.png', 176, 480);
        game.load.spritesheet('game.monkey24', 'img/monkey24.png', 176, 480);

        game.load.spritesheet('game.monkey21H', 'img/monkey21H.png', 176, 480);
        game.load.spritesheet('game.monkey22H', 'img/monkey22H.png', 176, 480);
        game.load.spritesheet('game.monkey23H', 'img/monkey23H.png', 176, 480);
        game.load.spritesheet('game.monkey24H', 'img/monkey24H.png', 176, 480);

        game.load.spritesheet('game.monkeyTakeRope1', 'img/monkeyTakeRope1.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope2', 'img/monkeyTakeRope2.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope1H', 'img/monkeyTakeRope1H.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope2H', 'img/monkeyTakeRope2H.png', 176, 480);
        game.load.spritesheet('game.monkeyEatBanana', 'img/monkeyEatBanana.png', 176, 480);
        game.load.spritesheet('game.monkeyEatBananaH', 'img/monkeyEatBananaH.png', 176, 480);
        game.load.spritesheet('game.monkeyEatHammer1', 'img/monkeyEatHammer1.png', 160, 480);
        game.load.spritesheet('game.monkeyEatHammer12', 'img/monkeyEatHammer12.png', 176, 480);
        game.load.spritesheet('game.monkeyEatHammer2', 'img/monkeyEatHammer2.png', 160, 480);
        game.load.spritesheet('game.monkeyEatHammer22', 'img/monkeyEatHammer22.png', 176, 480);
        game.load.spritesheet('game.monkeyHeadache', 'img/monkeyHeadache.png', 176, 480);

        game.load.audio('game.winRope1', 'sound/winRope1.mp3');
        game.load.audio('game.winRope2', 'sound/winRope2.mp3');
        game.load.audio('game.bitMonkey', 'sound/bitMonkey.mp3');

        game.load.image('game.backgroundGame4', 'img/shape943.svg');

        game.load.spritesheet('game.mankeyHanging1', 'img/mankeyHanging1.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging2', 'img/mankeyHanging2.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging3', 'img/mankeyHanging3.png', 240, 304);
        game.load.spritesheet('game.mankeyLeftPoint', 'img/mankeyLeftPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyLosePoint', 'img/mankeyLosePoint.png', 240, 304);
        game.load.spritesheet('game.mankeyRightPoint', 'img/mankeyRightPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyWinPoint', 'img/mankeyWinPoint.png', 240, 304);

        game.load.spritesheet('game.goldPoint', 'img/goldPoint.png', 96, 128);
        game.load.spritesheet('game.questionPoint', 'img/questionPoint.png', 96, 80);
        game.load.spritesheet('game.spiderPoint', 'img/spiderPoint.png', 96, 128);
        game.load.spritesheet('game.flashFive', 'img/flashFive.png', 32, 48);
        game.load.spritesheet('game.take_or_risk_anim', 'img/take_or_risk_anim.png', 192, 160);

        game.load.image('game.take_or_risk_bg', 'img/shape953.svg');
        game.load.audio('game.winPoint1', 'sound/winPoint1.mp3');
        game.load.audio('game.winPoint2', 'sound/winPoint2.mp3');
        game.load.audio('game.losePoint', 'sound/losePoint.mp3');

        game.load.image('game.monkeyR', 'img/monkeyR.png');
        game.load.image('game.butterflyR', 'img/butterflyR.png');

        game.load.image('cell0', 'img/cell0.jpg');
        game.load.image('cell1', 'img/cell1.jpg');
        game.load.image('cell2', 'img/cell2.jpg');
        game.load.image('cell3', 'img/cell3.jpg');
        game.load.image('cell4', 'img/cell4.jpg');
        game.load.image('cell5', 'img/cell5.jpg');
        game.load.image('cell6', 'img/cell6.jpg');
        game.load.image('cell7', 'img/cell7.jpg');
        game.load.image('cell8', 'img/cell8.jpg');

        game.load.spritesheet('cellAnim', 'img/cellAnim.jpg', 96, 96);

        game.load.image('bonusGame', 'img/image536.jpg');
        game.load.image('wildSymbol', 'img/image537.jpg');
        game.load.image('play1To', 'img/image546.png');
        game.load.image('takeOrRisk1', 'img/image474.jpg');
        game.load.image('takeOrRisk2', 'img/image475.jpg');
        game.load.image('take', 'img/image554.jpg');

        game.load.image('topScoreGame2', 'img/image457.png');

        game.load.image('loseTitleGame2', 'img/image484.png');
        game.load.image('winTitleGame2', 'img/image486.png');
        game.load.image('forwadTitleGame2', 'img/image504.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/monkeyBoom.png', 96, 96);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

