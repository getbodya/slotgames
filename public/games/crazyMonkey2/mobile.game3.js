var stepTotalWinR = 0;

function showWin(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y+100, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

    var timeInterval = 450;
    var textCounter = setInterval(function () {
        text.position.y -= 3;
    }, 10);

    setTimeout(function() {
        clearInterval(textCounter);
    }, timeInterval);

    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}






function game3() {
    (function () {

        var game3 = {};

        game3.preload = function () {};

        game3.create = function () {

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса

            //звуки
            winRope1 = game.add.audio('game.winRope1');
            winRope2 = game.add.audio('game.winRope2');
            bitMonkey = game.add.audio('game.bitMonkey');



            //изображения
            //background = game.add.sprite(0,0, 'game.background');
            background = game.add.sprite(95-mobileX,23-mobileY, 'topScoreGame2');
            background = game.add.sprite(95-mobileX,55-mobileY, 'game.backgroundGame2');


            rope1 = game.add.sprite(263-mobileX,55-mobileY, 'game.rope');
            rope2 = game.add.sprite(353-mobileX,55-mobileY, 'game.rope');
            rope3 = game.add.sprite(443-mobileX,55-mobileY, 'game.rope');
            rope4 = game.add.sprite(533-mobileX,55-mobileY, 'game.rope');
            rope5 = game.add.sprite(623-mobileX,55-mobileY, 'game.rope');

            rope1Button = game.add.sprite(263-mobileX-24,55-mobileY, 'game.ropeButton');
            rope2Button = game.add.sprite(353-mobileX-24,55-mobileY, 'game.ropeButton');
            rope3Button = game.add.sprite(443-mobileX-24,55-mobileY, 'game.ropeButton');
            rope4Button = game.add.sprite(533-mobileX-24,55-mobileY, 'game.ropeButton');
            rope5Button = game.add.sprite(623-mobileX-24,55-mobileY, 'game.ropeButton');
            rope5Button = game.add.sprite(623-mobileX-24,55-mobileY, 'game.ropeButton');
            rope1Button.inputEnabled = true;
            rope2Button.inputEnabled = true;
            rope3Button.inputEnabled = true;
            rope4Button.inputEnabled = true;
            rope5Button.inputEnabled = true;

            //счет
            scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);


            //кнопки

            rope1Button.events.onInputDown.add(function(){
                takeBananaRope(180-mobileX,22-mobileY,1);

                rope1Button.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) {
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 180+85-mobileX,22+200-mobileY , ropeValues[ropeStep], stepTotalWinR);
            });
            rope2Button.events.onInputDown.add(function(){
                takeBananaRope(275-mobileX,22-mobileY,2);

                rope2Button.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 275+85-mobileX,22+200-mobileY , ropeValues[ropeStep], stepTotalWinR);
            });
            rope3Button.events.onInputDown.add(function(){
                takeBananaRope(365-mobileX,22-mobileY,3);

                rope3Button.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 365+85-mobileX,22+200-mobileY , ropeValues[ropeStep], stepTotalWinR);
            });
            rope4Button.events.onInputDown.add(function(){
                takeBananaRope(455-mobileX,22-mobileY,4);

                rope4Button.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 455+85-mobileX,22+200-mobileY , ropeValues[ropeStep], stepTotalWinR);
            });
            rope5Button.events.onInputDown.add(function(){
                takeBananaRope(540-mobileX,22-mobileY,5);

                rope5Button.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 540+85-mobileX,22+200-mobileY , ropeValues[ropeStep], stepTotalWinR);
            });

            //анимации и логика

            function hideMonkey2(){
                monkey21.visible = false;
                monkey22.visible = false;
                monkey23.visible = false;
                monkey24.visible = false;

                monkey21H.visible = false;
                monkey22H.visible = false;
                monkey23H.visible = false;
                monkey24H.visible = false;
            }

            function showRandMonkey2(x,y){

                monkey21 = game.add.sprite(100+x,22+y, 'game.monkey21');
                monkey21.animations.add('monkey21', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
                monkey21.visible = false;

                monkey22 = game.add.sprite(100+x,22+y, 'game.monkey22');
                monkey22.animations.add('monkey22', [0,1,2,3,4,5], 10, false);
                monkey22.visible = false;

                monkey23 = game.add.sprite(100+x,22+y, 'game.monkey23');
                monkey23.animations.add('monkey23', [0,1,2,3,4], 10, false);
                monkey23.visible = false;

                monkey24 = game.add.sprite(100+x,22+y, 'game.monkey24');
                monkey24.animations.add('monkey24', [0,1,2,3,4,5,6,7], 10, false);
                monkey24.visible = false;

                monkey21H = game.add.sprite(100+x,22+y, 'game.monkey21H');
                monkey21H.animations.add('monkey21H', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
                monkey21H.visible = false;

                monkey22H = game.add.sprite(100+x,22+y, 'game.monkey22H');
                monkey22H.animations.add('monkey22H', [0,1,2,3,4,5], 10, false);
                monkey22H.visible = false;

                monkey23H = game.add.sprite(100+x,22+y, 'game.monkey23H');
                monkey23H.animations.add('monkey23H', [0,1,2,3,4], 10, false);
                monkey23H.visible = false;

                monkey24H = game.add.sprite(100+x,22+y, 'game.monkey24H');
                monkey24H.animations.add('monkey24H', [0,1,2,3,4,5,6,7], 10, false);
                monkey24H.visible = false;

                var randBaba = randomNumber(1,4);

                switch(randBaba) {
                    case 1:
                        if(checkHelm == true) {
                            monkey21H.visible = true;
                            monkey21H.animations.getAnimation('monkey21H').play().onComplete.add(function(){
                                monkey21H.animations.stop();
                                monkey21H.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        } else {
                            monkey21.visible = true;
                            monkey21.animations.getAnimation('monkey21').play().onComplete.add(function(){
                                monkey21.animations.stop();
                                monkey21.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        }
                    case 2:
                        if(checkHelm == true) {
                            monkey22H.visible = true;
                            monkey22H.animations.getAnimation('monkey22H').play().onComplete.add(function(){
                                monkey22H.animations.stop();
                                monkey22H.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        } else {
                            monkey22.visible = true;
                            monkey22.animations.getAnimation('monkey22').play().onComplete.add(function(){
                                monkey22.animations.stop();
                                monkey22.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        }

                    case 3:
                        if(checkHelm == true) {
                            monkey23H.visible = true;
                            monkey23H.animations.getAnimation('monkey23H').play().onComplete.add(function(){
                                monkey23H.animations.stop();
                                monkey23H.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        } else {
                            monkey23.visible = true;
                            monkey23.animations.getAnimation('monkey23').play().onComplete.add(function(){
                                monkey23.animations.stop();
                                monkey23.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        }
                    case 4:
                        if(checkHelm == true) {
                            monkey24H.visible = true;
                            monkey24H.animations.getAnimation('monkey24H').play().onComplete.add(function(){
                                monkey24H.animations.stop();
                                monkey24H.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        } else {
                            monkey24.visible = true;
                            monkey24.animations.getAnimation('monkey24').play().onComplete.add(function(){
                                monkey24.animations.stop();
                                monkey24.visible = false;
                                showRandMonkey2(x,y);
                            });
                            break;
                        }

                    default:
                        if(checkHelm == true) {
                            monkey24H.visible = true;
                            monkey24H.animations.getAnimation('monkey24H').play().onComplete.add(function(){
                                monkey24H.animations.stop();
                                monkey24H.visible = false;
                                showRandMonkey(x,y);
                            });
                            break;
                        } else {
                            monkey24.visible = true;
                            monkey24.animations.getAnimation('monkey24').play().onComplete.add(function(){
                                monkey24.animations.stop();
                                monkey24.visible = false;
                                showRandMonkey(x,y);
                            });
                            break;
                        }

                }
            }


            var ropeStep = 0;
            //часть логики занесена в анимацию, так как в разных игра она может отличаться для данной бонусной игры
            //0 - банан, 1 - кувалда, 2 - кирпич
            var win = 0;
            var ropeStep = 0;
            var checkUpLvl = true; //проверка доступа ко второй бонусной игре

            function takeBananaRope(x,y,ropeNumber){
                lockDisplay();

                hideMonkey2();

                monkeyTakeRope1 = game.add.sprite(x,y, 'game.monkeyTakeRope1');
                monkeyTakeRope1.animations.add('monkeyTakeRope1', [0,1,2,3,4], 10, false);
                monkeyTakeRope1.visible = false;

                monkeyTakeRope2 = game.add.sprite(x,y, 'game.monkeyTakeRope2');
                monkeyTakeRope2.animations.add('monkeyTakeRope2', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                monkeyTakeRope2.visible = false;

                monkeyTakeRope1H = game.add.sprite(x,y, 'game.monkeyTakeRope1H');
                monkeyTakeRope1H.animations.add('monkeyTakeRope1H', [0,1,2,3,4], 10, false);
                monkeyTakeRope1H.visible = false;

                monkeyTakeRope2H = game.add.sprite(x,y, 'game.monkeyTakeRope2H');
                monkeyTakeRope2H.animations.add('monkeyTakeRope2H', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                monkeyTakeRope2H.visible = false;

                monkeyEatBanana = game.add.sprite(x,y, 'game.monkeyEatBanana');
                monkeyEatBanana.animations.add('monkeyEatBanana', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                monkeyEatBanana.visible = false;

                monkeyEatBananaH = game.add.sprite(x,y, 'game.monkeyEatBananaH');
                monkeyEatBananaH.animations.add('monkeyEatBananaH', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
                monkeyEatBananaH.visible = false;

                monkeyEatHammer1 = game.add.sprite(x,y, 'game.monkeyEatHammer1');
                monkeyEatHammer1.animations.add('monkeyEatHammer1', [0,1,2], 10, false);
                monkeyEatHammer1.visible = false;

                monkeyEatHammer12 = game.add.sprite(x,y, 'game.monkeyEatHammer12');
                monkeyEatHammer12.animations.add('monkeyEatHammer12', [0,1,2,3], 10, false);
                monkeyEatHammer12.visible = false;

                monkeyEatHammer2 = game.add.sprite(x,y, 'game.monkeyEatHammer2');
                monkeyEatHammer2.animations.add('monkeyEatHammer2', [0,1,2], 10, false);
                monkeyEatHammer2.visible = false;

                monkeyEatHammer22 = game.add.sprite(x,y, 'game.monkeyEatHammer22');
                monkeyEatHammer22.animations.add('monkeyEatHammer22', [0,1,2,3], 10, false);
                monkeyEatHammer22.visible = false;

                monkeyHeadache = game.add.sprite(x,y, 'game.monkeyHeadache');
                monkeyHeadache.animations.add('monkeyHeadache', [0,1,2], 10, true);
                monkeyHeadache.visible = false;

                if(checkHelm == true) {
                    monkeyTakeRope1H.visible = true;
                    monkeyTakeRope1H.animations.getAnimation('monkeyTakeRope1H').play().onComplete.add(function(){

                        switch(ropeNumber) {
                            case 1:
                                rope1.visible = false;
                                break;
                            case 2:
                                rope2.visible = false;
                                break;
                            case 3:
                                rope3.visible = false;
                                break;
                            case 4:
                                rope4.visible = false;
                                break;
                            case 5:
                                rope5.visible = false;
                                break;
                        }

                        monkeyTakeRope1H.visible = false;
                        monkeyTakeRope2H.visible = true;
                        monkeyTakeRope2H.animations.getAnimation('monkeyTakeRope2H').play().onComplete.add(function(){
                            monkeyTakeRope2H.visible = false;

                            if(ropeValues[ropeStep] > 0) {
                                win = 0;
                            } else {
                                if(checkHelm == true){
                                    checkHelm = false;
                                    win = 2;
                                    checkUpLvl = false;
                                } else {
                                    win = 1;
                                }
                            }

                            ropeStep += 1;


                            if(win == 1) {
                                monkeyEatHammer1.visible = true;
                                monkeyEatHammer1.animations.getAnimation('monkeyEatHammer1').play().onComplete.add(function(){
                                    monkeyEatHammer1.visible = false;
                                    monkeyEatHammer12.visible = true;
                                    bitMonkey.play();
                                    monkeyEatHammer12.animations.getAnimation('monkeyEatHammer12').play().onComplete.add(function(){
                                        monkeyEatHammer12.visible = false;

                                        monkeyHeadache.visible = true;
                                        monkeyHeadache.animations.getAnimation('monkeyHeadache').play();

                                        updateBalanceGame3(game, scorePosions, balanceR);
                                    });

                                });
                            } else if(win == 2){
                                monkeyEatHammer2.visible = true;
                                monkeyEatHammer2.animations.getAnimation('monkeyEatHammer2').play().onComplete.add(function(){
                                    monkeyEatHammer2.visible = false;
                                    bitMonkey.play();
                                    monkeyEatHammer22.visible = true;
                                    monkeyEatHammer22.animations.getAnimation('monkeyEatHammer22').play().onComplete.add(function(){
                                        monkeyEatHammer22.visible = false;

                                        showRandMonkey2(x-100,-22+y);

                                        if(ropeStep == 5) {
                                            updateBalanceGame3(game, scorePosions, balanceR);
                                        }
                                    });
                                });
                            } else {
                                if(checkHelm == true) {
                                    monkeyEatBananaH.visible = true;
                                    winRope1.play();
                                    monkeyEatBananaH.animations.getAnimation('monkeyEatBananaH').play().onComplete.add(function(){
                                        monkeyEatBananaH.visible = false;
                                        winRope2.play();

                                        showRandMonkey2(x-100,-22+y);

                                        if(ropeStep == 5 && checkUpLvl == true) {
                                            setTimeout("game.state.start('game4');", 1000);
                                        } else if(ropeStep == 5) {
                                            updateBalanceGame3(game, scorePosions, balanceR);
                                        }
                                    });
                                } else {
                                    monkeyEatBanana.visible = true;
                                    winRope1.play();
                                    monkeyEatBanana.animations.getAnimation('monkeyEatBanana').play().onComplete.add(function(){
                                        monkeyEatBanana.visible = false;
                                        winRope2.play();

                                        showRandMonkey2(x-100,-22+y);

                                        if(ropeStep == 5 && checkUpLvl == true) {
                                            setTimeout("game.state.start('game4');", 1000);
                                        } else if(ropeStep == 5) {
                                            updateBalanceGame3(game, scorePosions, balanceR);
                                        }
                                    });
                                }
                            }
                        });
                    });
                } else {

                    monkeyTakeRope1.visible = true;
                    monkeyTakeRope1.animations.getAnimation('monkeyTakeRope1').play().onComplete.add(function(){

                        switch(ropeNumber) {
                            case 1:
                                rope1.visible = false;
                                break;
                            case 2:
                                rope2.visible = false;
                                break;
                            case 3:
                                rope3.visible = false;
                                break;
                            case 4:
                                rope4.visible = false;
                                break;
                            case 5:
                                rope5.visible = false;
                                break;
                        }

                        monkeyTakeRope1.visible = false;
                        monkeyTakeRope2.visible = true;
                        monkeyTakeRope2.animations.getAnimation('monkeyTakeRope2').play().onComplete.add(function(){
                            monkeyTakeRope2.visible = false;

                            if(ropeValues[ropeStep] > 0) {
                                win = 0;
                            } else {
                                if(checkHelm == true){
                                    checkHelm = false;
                                    win = 2;
                                    checkUpLvl = false;
                                } else {
                                    win = 1;
                                }
                            }

                            ropeStep += 1;


                            if(win == 1) {
                                monkeyEatHammer1.visible = true;
                                monkeyEatHammer1.animations.getAnimation('monkeyEatHammer1').play().onComplete.add(function(){
                                    monkeyEatHammer1.visible = false;
                                    monkeyEatHammer12.visible = true;
                                    bitMonkey.play();
                                    monkeyEatHammer12.animations.getAnimation('monkeyEatHammer12').play().onComplete.add(function(){
                                        monkeyEatHammer12.visible = false;

                                        monkeyHeadache.visible = true;
                                        monkeyHeadache.animations.getAnimation('monkeyHeadache').play();

                                        updateBalanceGame3(game, scorePosions, balanceR);
                                    });

                                });
                            } else if(win == 2){
                                monkeyEatHammer2.visible = true;
                                monkeyEatHammer2.animations.getAnimation('monkeyEatHammer2').play().onComplete.add(function(){
                                    monkeyEatHammer2.visible = false;
                                    bitMonkey.play();
                                    monkeyEatHammer22.visible = true;
                                    monkeyEatHammer22.animations.getAnimation('monkeyEatHammer22').play().onComplete.add(function(){
                                        monkeyEatHammer22.visible = false;

                                        showRandMonkey2(x-100,-22+y);

                                        if(ropeStep == 5) {
                                            updateBalanceGame3(game, scorePosions, balanceR);
                                        }
                                    });
                                });
                            } else {
                                if(checkHelm == true) {
                                    monkeyEatBananaH.visible = true;
                                    winRope1.play();
                                    monkeyEatBananaH.animations.getAnimation('monkeyEatBananaH').play().onComplete.add(function(){
                                        monkeyEatBananaH.visible = false;
                                        winRope2.play();

                                        showRandMonkey2(x-100,-22+y);

                                        if(ropeStep == 5 && checkUpLvl == true) {
                                            setTimeout("game.state.start('game4');", 1000);
                                        } else if(ropeStep == 5) {
                                            updateBalanceGame3(game, scorePosions, balanceR);
                                        }
                                    });
                                } else {
                                    monkeyEatBanana.visible = true;
                                    winRope1.play();
                                    monkeyEatBanana.animations.getAnimation('monkeyEatBanana').play().onComplete.add(function(){
                                        monkeyEatBanana.visible = false;
                                        winRope2.play();

                                        showRandMonkey2(x-100,-22+y);

                                        if(ropeStep == 5 && checkUpLvl == true) {
                                            setTimeout("game.state.start('game4');", 1000);
                                        } else if(ropeStep == 5) {
                                            updateBalanceGame3(game, scorePosions, balanceR);
                                        }
                                    });
                                }
                            }
                        });
                    });

                }

                setTimeout("unlockDisplay();", 4000);
            }

            showRandMonkey2(0,0);


        };

        game3.update = function () {
        };

        game.state.add('game3', game3);

    })();

}