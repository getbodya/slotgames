var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'phaser-example');

var fullStatus = false;
var soundStatus = true;

function hideMonkey(){
	monkey1.visible = false;
	monkey2.visible = false;
	monkey3.visible = false;
	monkey4.visible = false;
	monkey5.visible = false;
}

function hideButterfly(){
	butterfly1.visible = false;
	butterfly2.visible = false;
	butterfly3x.visible = false;
	butterflyFlies11.visible = false;
    butterflyFlies12.visible = false;
	butterflyFlies21.visible = false;
    butterflyFlies22.visible = false;
}

function hideNumbers() {
    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;
}

function showNumbers(n) {

    if(n == 1){
        number1.visible = true;
    } 

    if(n == 3) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
    } 

    if(n == 5) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
    }

    if(n == 7) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
    }

    if(n == 9) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    }
}

function full_and_sound(){ // Эту функцию вызывать в локациях
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

function createLevelButtons() {
    var lvl1 = game.add.sprite(0,0, 'x');
    lvl1.inputEnabled = true;
    lvl1.input.useHandCursor = true;
    lvl1.events.onInputUp.add(function () {
        game.state.start('game1');
    }, this);

    var lvl2 = game.add.sprite(20, 0, 'x');
    lvl2.inputEnabled = true;
    lvl2.input.useHandCursor = true;
    lvl2.events.onInputUp.add(function () {
        game.state.start('game2');
    }, this);
    
    var lvl3 = game.add.sprite(40, 0, 'x');
    lvl3.inputEnabled = true;
    lvl3.input.useHandCursor = true;
    lvl3.events.onInputUp.add(function () {
        game.state.start('game3');
    }, this);

    var lvl4 = game.add.sprite(60, 0, 'x');
    lvl4.inputEnabled = true;
    lvl4.input.useHandCursor = true;
    lvl4.events.onInputUp.add(function () {
        game.state.start('game4');
    }, this);
}

//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

(function () {

    var bars = [];
    var rotateSound;
    var stopSound;
    var tadaSound;
    var spinning = false;
    var barsCurrentSpins = [0, 0, 0, 0, 0];
    var barsTotalSpins = [];
    var spinningBars = 0;
    var button;
    var currentLine = 1;

    var lines = {
        1: {
            coord: 199,
            sprite: null,
            btncoord: 250,
            button: null
        },
        2: {
            coord: 71,
            sprite: null
        },
        3: {
            coord: 322,
            sprite: null,
            btncoord: 295,
            button: null
        },
        4: {
            coord: 130,
            sprite: null
        },
        5: {
            coord: 95,
            sprite: null,
            btncoord: 340,
            button: null
        },
        6: {
            coord: 102,
            sprite: null
        },
        7: {
            coord: 228,
            sprite: null,
            btncoord: 385,
            button: null
        },
        8: {
            coord: 226,
            sprite: null
        },
        9: {
            coord: 120,
            sprite: null,
            btncoord: 430,
            button: null
        }
    };

    var tmpSpins = 15;
    for (var i = 0; i < 5; ++i) {
        barsTotalSpins[i] = tmpSpins;
        tmpSpins += 12;
    }

    var game1 = {};


    function hideLines() {
        console.log(lines);
        for (var i in lines) {
            lines[i].sprite.visible = false;
        }
    }

    function selectLine(n) {
        currentLine = n;

        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.visible = true;
            lines[i].sprite.loadTexture('line_' + i);
        }
    }

    function preselectLine(n) {
        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.loadTexture('linefull_' + i);
            lines[i].sprite.visible = true;
        }
    }


    game1.preload = function () {

    };

    game1.soundStatus = true;

    game1.create = function () {

        var playSound = game.add.audio('play');
        rotateSound = game.add.audio('rotate');
        rotateSound.loop = true;
        stopSound = game.add.audio('stop');
        tadaSound = game.add.audio('tada');
        takeWin = game.add.audio('takeWin');
        takeWin.addMarker('take', 0, 0.6);
        winMonkey = game.add.audio('winMonkey');

        
        game.add.sprite(0,0, 'game.background');
        game.add.sprite(93,23, 'game.background1');
        

        selectGame = game.add.sprite(70,510, 'game.selectGame');
        selectGame.scale.setTo(0.7, 0.7);
        selectGame.inputEnabled = true;
        selectGame.input.useHandCursor = true;
        selectGame.events.onInputOver.add(function(){
            selectGame.loadTexture('game.selectGame_p');
        });
        selectGame.events.onInputOut.add(function(){
            selectGame.loadTexture('game.selectGame');
        });
        selectGame.events.onInputDown.add(function(){   
            
        });

        var payTable = game.add.sprite(150,510, 'game.payTable');
        payTable.scale.setTo(0.7, 0.7);
        payTable.inputEnabled = true;
        payTable.input.useHandCursor = true;
        payTable.events.onInputOver.add(function(){
            payTable.loadTexture('game.payTable_p');
        });
        payTable.events.onInputOut.add(function(){
            payTable.loadTexture('game.payTable');
        });

        betOneSound = game.add.audio('game.betOneSound');
        var betone = game.add.sprite(490,510, 'game.betone');
        betone.scale.setTo(0.7, 0.7);
        betone.inputEnabled = true;
        betone.input.useHandCursor = true;
        betone.events.onInputOver.add(function(){
            betone.loadTexture('game.betone_p');
        });
        betone.events.onInputDown.add(function(){
            betOneSound.play();
        });
        betone.events.onInputOut.add(function(){
            betone.loadTexture('game.betone');
        });

        betMaxSound = game.add.audio('game.betMaxSound');
        var betmax = game.add.sprite(535,510, 'game.betmax');
        betmax.scale.setTo(0.7, 0.7);
        betmax.inputEnabled = true;
        betmax.input.useHandCursor = true;
        betmax.events.onInputOver.add(function(){
            betmax.loadTexture('game.betmax_p');
        });
        betmax.events.onInputDown.add(function(){
            betMaxSound.play();
        });
        betmax.events.onInputOut.add(function(){
            betmax.loadTexture('game.betmax');
        });

        var automaricstart = game.add.sprite(685,510, 'game.automaricstart');
        automaricstart.scale.setTo(0.7, 0.7);
        automaricstart.inputEnabled = true;
        automaricstart.input.useHandCursor = true;
        automaricstart.events.onInputOver.add(function(){
            automaricstart.loadTexture('game.automaricstart_p');
        });
        automaricstart.events.onInputOut.add(function(){
            automaricstart.loadTexture('game.automaricstart');
        });

        
        var positions = [
            game.world.centerX - 226,
            game.world.centerX - 114,
            game.world.centerX - 2,
            game.world.centerX + 110,
            game.world.centerX + 221
        ];

        for (var i = 0; i < 5; ++i) {
            bars[i] = game.add.tileSprite(positions[i], game.world.centerY - 103, 96, 282, 'game.bars');
            bars[i].anchor.setTo(0.5, 0.5); //53
            bars[i].tilePosition.y = randomNumber(0, 8) * 96;
        }

        window.test = function () {
            bars[0].tilePosition.y -= 1;
        };

        function randomiseSpin() {
            return [
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106
            ];
        }

        button = game.add.sprite(627, 540, 'game.start');
        button.scale.setTo(0.7,0.7);

        line1x = game.add.sprite(250,510, 'game.line1_d');
        line1x.scale.setTo(0.7, 0.7);
        line3x = game.add.sprite(295,510, 'game.line3_d');
        line3x.scale.setTo(0.7, 0.7);
        line5x = game.add.sprite(340,510, 'game.line5_d');
        line5x.scale.setTo(0.7, 0.7);
        line7x = game.add.sprite(385,510, 'game.line7_d');
        line7x.scale.setTo(0.7, 0.7);
        line9x = game.add.sprite(430,510, 'game.line9_d');
        line9x.scale.setTo(0.7, 0.7);
        selectGamex = game.add.sprite(70,510, 'game.selectGame_d');
        selectGamex.scale.setTo(0.7, 0.7);
        payTablex = game.add.sprite(150,510, 'game.payTable_d');
        payTablex.scale.setTo(0.7, 0.7);
        betonex = game.add.sprite(490,510, 'game.betone_d');
        betonex.scale.setTo(0.7, 0.7);
        betmaxx = game.add.sprite(535,510, 'game.betmax_d');
        betmaxx.scale.setTo(0.7, 0.7);
        automaricstartx = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstartx.scale.setTo(0.7, 0.7);

        line1x.visible = false;
        line3x.visible = false;
        line5x.visible = false;
        line7x.visible = false;
        line9x.visible = false;
        selectGamex.visible = false;
        payTablex.visible = false;
        betonex.visible = false;
        betmaxx.visible = false;
        automaricstartx.visible = false;

        game.check_win = 0;

        function buttonClicked() {
            
            if (spinning) {
                return;
            }

            if(game.check_win == 0) {
                lines[1].button.visible = false;
                lines[3].button.visible = false;
                lines[5].button.visible = false;
                lines[7].button.visible = false;
                lines[9].button.visible = false;

                line1x.visible = true;
                line3x.visible = true;
                line5x.visible = true;
                line7x.visible = true;
                line9x.visible = true;
                selectGamex.visible = true;
                payTablex.visible = true;
                betonex.visible = true;
                betmaxx.visible = true;
                automaricstartx.visible = true;
                button.loadTexture('game.start_d');
            }
            
        }

        function buttonRelease() {
            if(game.check_win == 1) {
                hideLines();
                selectLine(3);
                takeWin.play('take');
                lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                line1x.visible = false;
                line3x.visible = false;
                line5x.visible = false;
                line7x.visible = false;
                line9x.visible = false;
                selectGamex.visible = false;
                payTablex.visible = false;
                betonex.visible = false;
                betmaxx.visible = false;
                automaricstartx.visible = false;
                button.loadTexture('game.start');

                game.check_win = 0;

                flashNamber1.animations.stop();
                flashNamber2.animations.stop();
                flashNamber3.animations.stop();

                flashNamber1.visible = false;
                flashNamber2.visible = false;
                flashNamber3.visible = false;

                monkeyWin.visible = false;
                showRandMonkey();

            } else {
                if (spinning) {
                    return;
                }
                hideLines();
                barsCurrentSpins = [0, 0, 0, 0, 0];
                spinningBars = bars.length;
                spinning = true;
                playSound.play();
            }
        }

        button.anchor.setTo(0.5, 0.5);
        button.inputEnabled = true;
        button.input.useHandCursor = true;
        button.events.onInputDown.add(buttonClicked, this);
        button.events.onInputUp.add(buttonRelease, this);

        for (var i = 1; i <= 9; ++i) {
            lines[i].sprite = game.add.sprite(134, lines[i].coord, 'line_' + i);
            lines[i].sprite.visible = false;
            if (i % 2 != 0) {
                lines[i].sound = game.add.audio('line' + i);
                lines[i].button = game.add.sprite(lines[i].btncoord, 510, 'btnline' + i);
                lines[i].button.scale.setTo(0.7,0.7);
                lines[i].button.inputEnabled = true;
                lines[i].button.input.useHandCursor = true;
                (function (n) {
                    lines[n].button.events.onInputDown.add(function () {

                        hideLines();
                        preselectLine(n);

                        hideNumbers();
                        showNumbers(n);

                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                    lines[n].button.events.onInputUp.add(function () {
                        hideLines();
                        selectLine(n);
                        lines[n].button.loadTexture('btnline' + n);
                        lines[n].sound.play();
                    }, this);
                    lines[n].button.events.onInputOut.add(function () {
                        lines[n].button.loadTexture('btnline' + n);
                    }, this);
                    lines[n].button.events.onInputOver.add(function () {
                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                })(i);
            }
        }

       	number1 = game.add.sprite(109,183, 'game.number1');
        number2 = game.add.sprite(109,54, 'game.number2');
        number3 = game.add.sprite(109,310, 'game.number3');
        number4 = game.add.sprite(109,118, 'game.number4');
        number5 = game.add.sprite(109,246, 'game.number5');
        number6 = game.add.sprite(109,86, 'game.number6');
        number7 = game.add.sprite(109,278, 'game.number7');
        number8 = game.add.sprite(109,214, 'game.number8');
        number9 = game.add.sprite(109,150, 'game.number9');

        preselectLine(9);
	    

	    

	    mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
	    mushroomGrow.animations.add('mushroomGrow', [0,1,2,3,4,5], 10, false);
	    mushroomGrow.animations.getAnimation('mushroomGrow').play().onComplete.add(function(){
	    	mushroomJump = game.add.sprite(353,423, 'game.mushroomJump');
		    mushroomJump.animations.add('mushroomJump', [0,1,2,3,4,5,6,7,8,9], 10, true);
		    mushroomJump.animations.getAnimation('mushroomJump').play();
	    });


	    showRandButterfly();

	    function showRandMonkey(){
			var randBaba = randomNumber(1,10);

			//add anim
		    monkey1 = game.add.sprite(193,359, 'game.monkey1');
		    monkey1.animations.add('monkey1', [0,1,2,3,4,5,6,7,8,9], 10, false);
		    monkey1.visible = false;

		    monkey2 = game.add.sprite(193,359, 'game.monkey2');
		    monkey2.animations.add('monkey2', [0,1,2,3,4,5,6,7], 10, false);
		    monkey2.visible = false;

		    monkey3 = game.add.sprite(193,359, 'game.monkey3');
		    monkey3.animations.add('monkey3', [0,1,2,3,4,5,6,7,8,9,10,11], 10, false);
		    monkey3.visible = false;

		    monkey4 = game.add.sprite(193,359, 'game.monkey4');
		    monkey4.animations.add('monkey4', [0,1,2,3,4,5], 10, false);
		    monkey4.visible = false;

		    monkey5 = game.add.sprite(193,359, 'game.monkey5');
		    monkey5.animations.add('monkey5', [0,1,2,3,4,5], 10, false);
		    monkey5.visible = false;
            

		    monkeyWin = game.add.sprite(193,359, 'game.monkeyWin');
		    monkeyWin.animations.add('monkeyWin', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true);
		    monkeyWin.visible = false;

			switch(randBaba) {
				case 1:
					monkey1.visible = true;
					monkey1.animations.getAnimation('monkey1').play().onComplete.add(function(){
						monkey1.animations.stop();
						monkey1.visible = false;
						showRandMonkey();
					});
					break;
				case 2:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				case 3:
					monkey3.visible = true;
					monkey3.animations.getAnimation('monkey3').play().onComplete.add(function(){
						monkey3.animations.stop();
						monkey3.visible = false;
						showRandMonkey();
					});
					break;
				case 4:
					monkey4.visible = true;
					monkey4.animations.getAnimation('monkey4').play().onComplete.add(function(){
						monkey4.animations.stop();
						monkey4.visible = false;
						showRandMonkey();
					});
					break;
				case 5:
					monkey5.visible = true;
					monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
						monkey5.animations.stop();
						monkey5.visible = false;
						showRandMonkey();
					});
					break;
				case 6:
					monkey5.visible = true;
					monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
						monkey5.animations.stop();
						monkey5.visible = false;
						showRandMonkey();
					});
					break;
				case 7:
					monkey5.visible = true;
					monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
						monkey5.animations.stop();
						monkey5.visible = false;
						showRandMonkey();
					});
					break;
				case 8:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				case 9:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				case 10:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				default: 
						monkey2.visible = true;
						monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
							monkey2.animations.stop();
							monkey1.visible = false;
							showRandMonkey();
						});
				break;
			}
		}

		var checkPosition = 1;

		function showRandButterfly(){
			var randBaba = randomNumber(1,3);

			butterfly1 = game.add.sprite(493,407, 'game.butterfly1');
		    butterfly1.animations.add('butterfly1', [0,1,2,3,4,5,6,7,8], 10, false);
		    butterfly1.visible = false;

		    butterfly2 = game.add.sprite(493,407, 'game.butterfly2');
		    butterfly2.animations.add('butterfly2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
		    butterfly2.visible = false;

		    butterfly3x = game.add.sprite(493,407, 'game.butterfly3x');
		    butterfly3x.animations.add('butterfly3x', [0,1,2,3,4,5], 10, false);
		    butterfly3x.visible = false;

		    butterflyFlies11 = game.add.sprite(493,407, 'game.butterflyFlies1');
		    butterflyFlies11.animations.add('butterflyFlies11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
		    butterflyFlies11.visible = false;

            butterflyFlies12 = game.add.sprite(493,407, 'game.butterflyFlies2');
            butterflyFlies12.animations.add('butterflyFlies12', [1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
            butterflyFlies12.visible = false;

            butterflyFlies21 = game.add.sprite(493,407, 'game.butterflyFlies1');
            butterflyFlies21.animations.add('butterflyFlies21', [15,14,13,12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies21.visible = false;

            butterflyFlies22 = game.add.sprite(493,407, 'game.butterflyFlies2');
            butterflyFlies22.animations.add('butterflyFlies22', [12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies22.visible = false;		

            butterflyR = game.add.sprite(453,409, 'game.butterflyR');
            monkeyR = game.add.sprite(95,403, 'game.monkeyR');
                			

			if(checkPosition == 1) {
				switch(randBaba) {
					case 1:
						butterfly1.visible = true;
						butterfly1.animations.getAnimation('butterfly1').play().onComplete.add(function(){
							butterfly1.animations.stop();
							butterfly1.visible = false;
							showRandButterfly();
						});
					break;
					case 2:
						butterfly2.visible = true;
						butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
							butterfly2.animations.stop();
							butterfly2.visible = false;
							showRandButterfly();
						});
					break;
					
					case 3:
						butterflyFlies11.visible = true;
						butterflyFlies11.animations.getAnimation('butterflyFlies11').play().onComplete.add(function(){
							butterflyFlies11.animations.stop();
							butterflyFlies11.visible = false;
                            
                            butterflyFlies12.visible = true;
                            butterflyFlies12.animations.getAnimation('butterflyFlies12').play().onComplete.add(function(){
                                butterflyFlies12.animations.stop();
                                butterflyFlies12.visible = false;

                                checkPosition = 2;
                                showRandButterfly();
                            });
						});
					break;

					default: 
							butterfly2.visible = true;
							butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
								butterfly2.animations.stop();
								butterfly2.visible = false;
								showRandButterfly();
							});
					break;
				}
			} else {
				switch(randBaba) {
					case 1:
						butterfly3x.visible = true;
						butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
							butterfly3x.animations.stop();
							butterfly3x.visible = false;
							showRandButterfly();
						});
					break;
					
					case 2:
						butterflyFlies22.visible = true;
						butterflyFlies22.animations.getAnimation('butterflyFlies22').play().onComplete.add(function(){
							butterflyFlies22.animations.stop();
							butterflyFlies22.visible = false;

							butterflyFlies21.visible = true;
                            butterflyFlies21.animations.getAnimation('butterflyFlies21').play().onComplete.add(function(){
                                butterflyFlies21.animations.stop();
                                butterflyFlies21.visible = false;
                                checkPosition = 1;
                                showRandButterfly();
                            });
						});
					break;

					case 3:
						butterfly3x.visible = true;
						butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
							butterfly3x.animations.stop();
							butterfly3x.visible = false;
							showRandButterfly();
						});
					break;

					default: 
							butterfly3x.visible = true;
							butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
								butterfly3x.animations.stop();
								butterfly3x.visible = false;
								showRandButterfly();
							});
					break;
				}
			}

				
		}

        showRandMonkey();

        game.add.sprite(235,343, 'game.title1');

        createLevelButtons();
        full_and_sound();

    };



    game1.update = function () {
        if (spinning) {
            for (var i in bars) {
                barsCurrentSpins[i]++;
                if (barsCurrentSpins[i] < barsTotalSpins[i]) {
                    bars[i].tilePosition.y += 96;
                } else if (barsCurrentSpins[i] == barsTotalSpins[i]) {
                    spinningBars--;
                }
            }
            if (!spinningBars) {
                spinning = false;
                rotateSound.stop();
                button.loadTexture('game.start');
                selectLine(currentLine);
                console.log('spin end');
                if (currentLine == 3) {

                	/*blin = game.add.sprite(365,156, 'game.blin');
        			blin.animations.add('blin', [0,1,2,3,4,5,6,7], 5, false);
        			blin.visible = true;
        			blin.animations.getAnimation('blin').play();

        			pech = game.add.sprite(477,156, 'game.pech');
        			pech.animations.add('pech', [0,1,2], 2, false);
        			pech.visible = true;
        			pech.animations.getAnimation('pech').play();*/

        			

                    preselectLine(3);
                    tadaSound.play();

                    hideNumbers();

                    game.check_win = 1;

                    hideMonkey();

                    monkeyWin.visible = true;
                    monkeyWin.animations.getAnimation('monkeyWin').play().onComplete.add(function(){
                    	monkeyWin.animations.stop();
                    	monkeyWin.visible = false;
                    });

                    flashNamber1 = game.add.sprite(93+16,183, 'game.flashNamber1');
                    flashNamber1.animations.add('flashNamber1', [0,1], 1, true);
                    flashNamber1.animations.getAnimation('flashNamber1').play();

                    flashNamber2 = game.add.sprite(93+16,55, 'game.flashNamber2');
                    flashNamber2.animations.add('flashNamber2', [0,1], 1, true);
                    flashNamber2.animations.getAnimation('flashNamber2').play();

                    flashNamber3 = game.add.sprite(93+16,311, 'game.flashNamber3');
                    flashNamber3.animations.add('flashNamber3', [0,1], 1, true);
                    flashNamber3.animations.getAnimation('flashNamber3').play();

                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                } else if( currentLine == 5){
                	winMonkey.play();
                	game.check_win = 0;
                	hideLines();
		            lockDisplay();

		            mask = game.add.sprite(477,156, 'game.mask');
        			mask.animations.add('mask', [0,1,2,3,0,1,2,3], 3, true);
        			mask.visible = true;
        			mask.animations.getAnimation('mask').play();

        			monkey = game.add.sprite(365,156, 'game.monkey');
        			monkey.animations.add('monkey', [0,1,2,3,4,5,6,7,8,9], 6, true);
        			monkey.visible = true;
        			monkey.animations.getAnimation('monkey').play();

        			monkey2 = game.add.sprite(253,156, 'game.monkey');
        			monkey2.animations.add('monkey2', [0,1,2,3,4,5,6,7,8,9], 6, true);
        			monkey2.visible = true;
        			monkey2.animations.getAnimation('monkey2').play();

        			hideMonkey();				    

        			mushroomJump.visible = false;

                    monkeyTakeMushroom1 = game.add.sprite(195,359, 'game.monkeyTakeMushroom1');
                    monkeyTakeMushroom1.animations.add('monkeyTakeMushroom1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 10, false);
                    monkeyTakeMushroom1.visible = false;

                    monkeyTakeMushroom2 = game.add.sprite(195,359, 'game.monkeyTakeMushroom2');
                    monkeyTakeMushroom2.animations.add('monkeyTakeMushroom2', [1,2,3,4], 10, false);
                    monkeyTakeMushroom2.visible = false;

                    function monkeyTakeMushroomAnim() {
                        monkeyTakeMushroom1.visible = true;
                        monkeyTakeMushroom1.animations.getAnimation('monkeyTakeMushroom1').play().onComplete.add(function(){
                            monkeyTakeMushroom1.visible = false;

                            monkeyTakeMushroom2.visible = true;
                            monkeyTakeMushroom2.animations.getAnimation('monkeyTakeMushroom2').play();
                        });
                    }

        			monkeyTakeMushroomAnim();

        			setTimeout("unlockDisplay(); game.state.start('game2');", 6000);

		            
                } else {
                    lines[1].button.visible = true;
                    lines[3].button.visible = true;
                    lines[5].button.visible = true;
                    lines[7].button.visible = true;
                    lines[9].button.visible = true;

                    line1x.visible = false;
                    line3x.visible = false;
                    line5x.visible = false;
                    line7x.visible = false;
                    line9x.visible = false;
                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                }
            }
        }
        
    };

    game.state.add('game1', game1);

})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================

(function () {

    var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {
        //Добавление фона
        background = game.add.sprite(0,0, 'game.background');
        background = game.add.sprite(95,23, 'game.background1');
        background = game.add.sprite(95,55, 'game.backgroundGame2');

        winRope1 = game.add.audio('game.winRope1');
        winRope2 = game.add.audio('game.winRope2');
        bitMonkey = game.add.audio('game.bitMonkey');
        
        //добавление кнопок

        var startButton = game.add.sprite(596, 511, 'game.start_d');
        startButton.scale.setTo(0.7, 0.7);
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOut.add(function(){
            startButton.loadTexture('game.start');
        });

        var selectGame = game.add.sprite(70,510, 'game.selectGame_d');
        selectGame.scale.setTo(0.7, 0.7);
        selectGame.events.onInputOver.add(function(){
            selectGame.loadTexture('game.selectGame_p');
        });
        selectGame.events.onInputOut.add(function(){
            selectGame.loadTexture('game.selectGame');
        });

        var payTable = game.add.sprite(150,510, 'game.payTable_d');
        payTable.scale.setTo(0.7, 0.7);
        payTable.events.onInputOver.add(function(){
            payTable.loadTexture('game.payTable_p');
        });
        payTable.events.onInputOut.add(function(){
            payTable.loadTexture('game.payTable');
        });

        var line1 = game.add.sprite(250,510, 'game.line1');
        line1.scale.setTo(0.7, 0.7);
        line1.inputEnabled = true;
        line1.input.useHandCursor = true;
        line1.events.onInputUp.add(function(){
            line1.loadTexture('game.line1');
        });
        line1.events.onInputOver.add(function(){
            line1.loadTexture('game.line1_p');
        });
        line1.events.onInputOut.add(function(){
            line1.loadTexture('game.line1');
        }); 
        line1.events.onInputDown.add(function(){
        	takeBananaRope(180,22,1,0);

        	line1x.visible = true;
        	line1.visible = false;
        });

        var line3 = game.add.sprite(295,510, 'game.line3');
        line3.scale.setTo(0.7, 0.7);
        line3.inputEnabled = true;
        line3.input.useHandCursor = true;
        line3.events.onInputUp.add(function(){
        	takeBananaRope(275,22,2,0);

        	line3x.visible = true;
        	line3.visible = false;
            
        });
        line3.events.onInputOver.add(function(){
            line3.loadTexture('game.line3_p');
        });
        line3.events.onInputOut.add(function(){
            line3.loadTexture('game.line3');
        });

        var line5 = game.add.sprite(340,510, 'game.line5');
        line5.scale.setTo(0.7, 0.7);
        line5.inputEnabled = true;
        line5.input.useHandCursor = true;
        line5.events.onInputOver.add(function(){
            line5.loadTexture('game.line5_p');
        });
        line5.events.onInputDown.add(function(){
            takeBananaRope(365,22,3,0);

        	line5x.visible = true;
        	line5.visible = false;

        });
        line5.events.onInputOut.add(function(){
            line5.loadTexture('game.line5');
        });


        var line7 = game.add.sprite(385,510, 'game.line7');
        line7.scale.setTo(0.7, 0.7);
        line7.inputEnabled = true;
        line7.input.useHandCursor = true;
        line7.events.onInputOver.add(function(){
            line7.loadTexture('game.line7_p');
        });
        line7.events.onInputDown.add(function(){
        	takeBananaRope(455,22,4,1);

        	line7x.visible = true;
        	line7.visible = false;
        });
        line7.events.onInputOut.add(function(){
            line7.loadTexture('game.line7');
        });

        var line9 = game.add.sprite(430,510, 'game.line9');
        line9.inputEnabled = true;
        line9.input.useHandCursor = true;
        line9.scale.setTo(0.7, 0.7);
        line9.events.onInputOver.add(function(){
            line9.loadTexture('game.line9_p');
        });
        line9.events.onInputDown.add(function(){
            takeBananaRope(540,22,5,2);

        	line9x.visible = true;
        	line9.visible = false;
        });
        line9.events.onInputOut.add(function(){
            line9.loadTexture('game.line9');
        });

        var betone = game.add.sprite(490,510, 'game.betone_d');
        betone.scale.setTo(0.7, 0.7);
        betone.events.onInputOver.add(function(){
            betone.loadTexture('game.betone_p');
        });
        betone.events.onInputOut.add(function(){
            betone.loadTexture('game.betone');
        });

        var betmax = game.add.sprite(535,510, 'game.betmax_d');
        betmax.scale.setTo(0.7, 0.7);
        betmax.events.onInputOver.add(function(){
            betmax.loadTexture('game.betmax_p');
        });
        betmax.events.onInputOut.add(function(){
            betmax.loadTexture('game.betmax');
        });

        var automaricstart = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstart.scale.setTo(0.7, 0.7);
        automaricstart.events.onInputOver.add(function(){
            automaricstart.loadTexture('game.automaricstart_p');
        });
        automaricstart.events.onInputOut.add(function(){
            automaricstart.loadTexture('game.automaricstart');
        });

        line1x = game.add.sprite(250,510, 'game.line1_d');
        line1x.inputEnabled = false;

        line3x = game.add.sprite(295,510, 'game.line3_d');
        line3x.inputEnabled = false;

        line5x = game.add.sprite(340,510, 'game.line5_d');
        line5x.inputEnabled = false;

        line7x = game.add.sprite(385,510, 'game.line7_d');
        line7x.inputEnabled = false;

        line9x = game.add.sprite(430,510, 'game.line9_d');
        line9x.inputEnabled = false;

        line1x.scale.setTo(0.7, 0.7);
        line3x.scale.setTo(0.7, 0.7);
        line5x.scale.setTo(0.7, 0.7);
        line7x.scale.setTo(0.7, 0.7);
        line9x.scale.setTo(0.7, 0.7);
        line1x.visible = false;
        line3x.visible = false;
        line5x.visible = false;
        line7x.visible = false;
        line9x.visible = false;

        
        //add anim

        function hideMonkey2(){
			monkey21.visible = false;
			monkey22.visible = false;
			monkey23.visible = false;
			monkey24.visible = false;
		}

        function showRandMonkey2(x,y){

			monkey21 = game.add.sprite(100+x,22+y, 'game.monkey21');
		    monkey21.animations.add('monkey21', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
		    monkey21.visible = false;

		    monkey22 = game.add.sprite(100+x,22+y, 'game.monkey22');
		    monkey22.animations.add('monkey22', [0,1,2,3,4,5], 10, false);
		    monkey22.visible = false;

		    monkey23 = game.add.sprite(100+x,22+y, 'game.monkey23');
		    monkey23.animations.add('monkey23', [0,1,2,3,4], 10, false);
		    monkey23.visible = false;

		    monkey24 = game.add.sprite(100+x,22+y, 'game.monkey24');
		    monkey24.animations.add('monkey24', [0,1,2,3,4,5,6,7], 10, false);
		    monkey24.visible = false;

			var randBaba = randomNumber(1,4);

			switch(randBaba) {
				case 1:
					monkey21.visible = true;
					monkey21.animations.getAnimation('monkey21').play().onComplete.add(function(){
						monkey21.animations.stop();
						monkey21.visible = false;
						showRandMonkey2(x,y);
					});
					break;
				case 2:
					monkey22.visible = true;
					monkey22.animations.getAnimation('monkey22').play().onComplete.add(function(){
						monkey22.animations.stop();
						monkey22.visible = false;
						showRandMonkey2(x,y);
					});
					break;
				case 3:
					monkey23.visible = true;
					monkey23.animations.getAnimation('monkey23').play().onComplete.add(function(){
						monkey23.animations.stop();
						monkey23.visible = false;
						showRandMonkey2(x,y);
					});
					break;
				case 4:
					monkey24.visible = true;
					monkey24.animations.getAnimation('monkey24').play().onComplete.add(function(){
						monkey24.animations.stop();
						monkey24.visible = false;
						showRandMonkey2(x,y);
					});
					break;

				default: 
					monkey24.visible = true;
					monkey24.animations.getAnimation('monkey24').play().onComplete.add(function(){
						monkey24.animations.stop();
						monkey24.visible = false;
						showRandMonkey(x,y);
					});
				break;
			}
		}
        

	    function takeBananaRope(x,y,ropeNumber, win){
	    	lockDisplay();

	    	hideMonkey2();

	    	monkeyTakeRope1 = game.add.sprite(x,y, 'game.monkeyTakeRope1');
		    monkeyTakeRope1.animations.add('monkeyTakeRope1', [0,1,2,3,4], 10, false);
		    monkeyTakeRope1.visible = false;

		    monkeyTakeRope2 = game.add.sprite(x,y, 'game.monkeyTakeRope2');
		    monkeyTakeRope2.animations.add('monkeyTakeRope2', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
		    monkeyTakeRope2.visible = false;

		    monkeyEatBanana = game.add.sprite(x,y, 'game.monkeyEatBanana');
		    monkeyEatBanana.animations.add('monkeyEatBanana', [0,1,2,3,4,5,6,7,8,9,10], 10, false);
		    monkeyEatBanana.visible = false;

		    monkeyEatHammer1 = game.add.sprite(x,y, 'game.monkeyEatHammer1');
		    monkeyEatHammer1.animations.add('monkeyEatHammer1', [0,1,2], 10, false);
		    monkeyEatHammer1.visible = false;

		    monkeyEatHammer12 = game.add.sprite(x,y, 'game.monkeyEatHammer12');
		    monkeyEatHammer12.animations.add('monkeyEatHammer12', [0,1,2,3], 10, false);
		    monkeyEatHammer12.visible = false;

		    monkeyEatHammer2 = game.add.sprite(x,y, 'game.monkeyEatHammer2');
		    monkeyEatHammer2.animations.add('monkeyEatHammer2', [0,1,2], 10, false);
		    monkeyEatHammer2.visible = false;

		    monkeyEatHammer22 = game.add.sprite(x,y, 'game.monkeyEatHammer22');
		    monkeyEatHammer22.animations.add('monkeyEatHammer22', [0,1,2,3], 10, false);
		    monkeyEatHammer22.visible = false;

		    monkeyHeadache = game.add.sprite(x,y, 'game.monkeyHeadache');
		    monkeyHeadache.animations.add('monkeyHeadache', [0,1,2], 10, true);
		    monkeyHeadache.visible = false;

	    	monkeyTakeRope1.visible = true;
	    	monkeyTakeRope1.animations.getAnimation('monkeyTakeRope1').play().onComplete.add(function(){

	    		switch(ropeNumber) {
	    			case 1:
	    				rope1.visible = false;
	    			break;
	    			case 2:
	    				rope2.visible = false;
	    			break;
	    			case 3:
	    				rope3.visible = false;
	    			break;
	    			case 4:
	    				rope4.visible = false;
	    			break;
	    			case 5:
	    				rope5.visible = false;
	    			break;
	    		}

	    		monkeyTakeRope1.visible = false;
				monkeyTakeRope2.visible = true;
				monkeyTakeRope2.animations.getAnimation('monkeyTakeRope2').play().onComplete.add(function(){
					monkeyTakeRope2.visible = false;

					if(win == 1) {
						monkeyEatHammer1.visible = true;
						monkeyEatHammer1.animations.getAnimation('monkeyEatHammer1').play().onComplete.add(function(){
							monkeyEatHammer1.visible = false;
							monkeyEatHammer12.visible = true;
							bitMonkey.play();
							monkeyEatHammer12.animations.getAnimation('monkeyEatHammer12').play().onComplete.add(function(){
								monkeyEatHammer12.visible = false;

								monkeyHeadache.visible = true;
								monkeyHeadache.animations.getAnimation('monkeyHeadache').play();
								setTimeout("game.state.start('game1');", 1000);
							});
							
						});
					} else if(win == 2){
						monkeyEatHammer2.visible = true;
						monkeyEatHammer2.animations.getAnimation('monkeyEatHammer2').play().onComplete.add(function(){
							monkeyEatHammer2.visible = false;
							bitMonkey.play();
							monkeyEatHammer22.visible = true;
							monkeyEatHammer22.animations.getAnimation('monkeyEatHammer22').play().onComplete.add(function(){
								monkeyEatHammer22.visible = false;

								showRandMonkey2(x-100,-22+y);
								
							});
						});
					} else {
						monkeyEatBanana.visible = true;
						winRope1.play();
						monkeyEatBanana.animations.getAnimation('monkeyEatBanana').play().onComplete.add(function(){
							monkeyEatBanana.visible = false;
							winRope2.play();

							showRandMonkey2(x-100,-22+y);
						});
					}
				});
			});

			setTimeout("unlockDisplay();", 4000);
	    }
	    

        showRandMonkey2(0,0);

        rope1 = game.add.sprite(263,55, 'game.rope');
	    rope2 = game.add.sprite(353,55, 'game.rope');
	    rope3 = game.add.sprite(443,55, 'game.rope');
	    rope4 = game.add.sprite(533,55, 'game.rope');  
        rope5 = game.add.sprite(623,55, 'game.rope');
	    

        createLevelButtons();
        full_and_sound();
    };

    game2.update = function () {
    };

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================

function randomCard() {
    var arr = ['2b','3b','4b','5b','6b','7b','8b','9b','10b','jb','qb','kb','ab','2ch','3ch','4ch','5ch','6ch','7ch','8ch','9ch','10ch','jch','qch','kch','ach','2k','3k','4k','5k','6k','7k','8k','9k',
        '10k','jk','qk','kk','ak','2p','3p','4p','5p','6p','7p','8p','9p','10p','jp','qp'];

    var rand = Math.floor(Math.random() * arr.length);

    return 'game.card_'+arr[rand];
}

function hidePick() {
    pick2.visible = false;
    pick3.visible = false;
    pick4.visible = false;
    pick5.visible = false;
}

function openCardSound() {
    openCardAudio.play();
}

/*function lineButtonHide() {
    line3 = game.add.sprite(295,510, 'game.line3');
}*/

function lineButtonShow() {
    line3.visible = true;
    line5.visible = true;
    line7.visible = true;
    line9.visible = true;

    line3x.visible = false;
    line5x.visible = false;
    line7x.visible = false;
    line9x.visible = false;
}

function lineButtonHide() {
    line3.visible = false;
    line5.visible = false;
    line7.visible = false;
    line9.visible = false;

    line3x.visible = true;
    line5x.visible = true;
    line7x.visible = true;
    line9x.visible = true;
}

(function () {

    var button;

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {

        background = game.add.sprite(0,0, 'game.background');
        background1 = game.add.sprite(95,23, 'game.background1');
        backgroundGame3 = game.add.sprite(95,55, 'game.backgroundGame3');
        

        var startButton = game.add.sprite(596, 511, 'game.start');
        startButton.scale.setTo(0.7, 0.7);
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOut.add(function(){
            startButton.loadTexture('game.start');
        });

        selectGame = game.add.sprite(70,510, 'game.selectGame_d');
        selectGame.scale.setTo(0.7, 0.7);

        payTable = game.add.sprite(150,510, 'game.payTable_d');
        payTable.scale.setTo(0.7, 0.7);

        line1 = game.add.sprite(250,510, 'game.line1_d');
        line1.scale.setTo(0.7, 0.7);

        line3 = game.add.sprite(295,510, 'game.line3');
        line3.scale.setTo(0.7, 0.7);
        line3.inputEnabled = true;
        line3.input.useHandCursor = true;
        line3.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);

            card2.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            lineButtonHide();
            
            setTimeout('openCardSound(); card3.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick5.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick(); lineButtonShow();',
                3000);
        });
        line3.events.onInputOver.add(function(){
            line3.loadTexture('game.line3_p');
        });
        line3.events.onInputOut.add(function(){
            line3.loadTexture('game.line3');
        });

        line5 = game.add.sprite(340,510, 'game.line5');
        line5.scale.setTo(0.7, 0.7);
        line5.inputEnabled = true;
        line5.input.useHandCursor = true;
        line5.events.onInputOver.add(function(){
            line5.loadTexture('game.line5_p');
        });
        line5.events.onInputOut.add(function(){
            line5.loadTexture('game.line5');
        });
        line5.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            lineButtonHide();
            card3.loadTexture(randomCard());
            openCardSound();
            winCards.play(); 
            setTimeout('card2.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick4.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick(); lineButtonShow();',
                3000);
        });

        line7 = game.add.sprite(385,510, 'game.line7');
        line7.scale.setTo(0.7, 0.7);
        line7.inputEnabled = true;
        line7.input.useHandCursor = true;
        line7.events.onInputOver.add(function(){
            line7.loadTexture('game.line7_p');
        });
        line7.events.onInputOut.add(function(){
            line7.loadTexture('game.line7');
        });
        line7.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            lineButtonHide();
            card4.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            setTimeout('card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick3.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage");hidePick(); lineButtonShow();',
                3000);
        });

        line9 = game.add.sprite(430,510, 'game.line9');
        line9.scale.setTo(0.7, 0.7);
        line9.inputEnabled = true;
        line9.input.useHandCursor = true;
        line9.events.onInputOver.add(function(){
            line9.loadTexture('game.line9_p');
        });
        line9.events.onInputOut.add(function(){
            line9.loadTexture('game.line9');
        });
        line9.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            openCardSound();
            card5.loadTexture(randomCard());
            pick2.visible = true;
            lineButtonHide();
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card4.loadTexture(randomCard());',1000);

            setTimeout('game.state.start("game1"); card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage");' +
                'card4.loadTexture("game.card_garage");' +
                'card5.loadTexture("game.card_garage");hidePick(); lineButtonShow();',
                3000);
        });

        line3x = game.add.sprite(295,510, 'game.line3_d');
        line5x = game.add.sprite(340,510, 'game.line5_d');
        line7x = game.add.sprite(385,510, 'game.line7_d');
        line9x = game.add.sprite(430,510, 'game.line9_d');
        line3x.scale.setTo(0.7, 0.7);
        line5x.scale.setTo(0.7, 0.7);
        line7x.scale.setTo(0.7, 0.7);
        line9x.scale.setTo(0.7, 0.7);
        line3x.visible = false;
        line5x.visible = false;
        line7x.visible = false;
        line9x.visible = false;

        betone = game.add.sprite(490,510, 'game.betone_d');
        betone.scale.setTo(0.7, 0.7);

        betmax = game.add.sprite(535,510, 'game.betmax_d');
        betmax.scale.setTo(0.7, 0.7);

        automaricstart = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstart.scale.setTo(0.7, 0.7);

        openCardAudio = game.add.audio("game.openCardAudio");
        winCards = game.add.audio("game.winCards");

        lockDisplay();
        setTimeout('unlockDisplay();',500);
        setTimeout('openCardSound(); card1 = game.add.sprite(128-15,133-14, randomCard());',500);

        card1 = game.add.sprite(128-15,133-14, 'game.card_garage');

        card2 = game.add.sprite(253+20,133-14, 'game.card_garage');
        card2.inputEnabled = true;
        card2.input.useHandCursor = true;
        card2.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);

            card2.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            lineButtonHide();
            
            setTimeout('openCardSound(); card3.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick5.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick(); lineButtonShow();',
                3000);
        });

        card3 = game.add.sprite(365+20,133-14, 'game.card_garage');
        card3.inputEnabled = true;
        card3.input.useHandCursor = true;
        card3.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            lineButtonHide();
            card3.loadTexture(randomCard());
            openCardSound();
            winCards.play(); 
            setTimeout('card2.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick4.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick(); lineButtonShow();',
                3000);
        });

        card4 = game.add.sprite(480+20,133-14, 'game.card_garage');
        card4.inputEnabled = true;
        card4.input.useHandCursor = true;
        card4.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            lineButtonHide();
            card4.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            setTimeout('card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick3.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage");hidePick(); lineButtonShow();',
                3000);
        });

        card5 = game.add.sprite(592+20,133-14, 'game.card_garage');
        card5.inputEnabled = true;
        card5.input.useHandCursor = true;
        card5.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            openCardSound();
            card5.loadTexture(randomCard());
            pick2.visible = true;
            lineButtonHide();
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card4.loadTexture(randomCard());',1000);

            setTimeout('game.state.start("game1"); card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage");' +
                'card4.loadTexture("game.card_garage");' +
                'card5.loadTexture("game.card_garage");hidePick(); lineButtonShow();',
                3000);
        });

        pick2 = game.add.sprite(605-10+2,300-22, 'game.pick');
        pick3 = game.add.sprite(483+2,300-22, 'game.pick');
        pick4 = game.add.sprite(385-17+2,300-22, 'game.pick');
        pick5 = game.add.sprite(275-20+2,300-22, 'game.pick');
        pick2.visible = false;
        pick3.visible = false;
        pick4.visible = false;
        pick5.visible = false;

        //add anim
        

	    mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
	    mushroomGrow.animations.add('mushroomGrow', [0,1,2,3,4,5], 5, false);
	    mushroomGrow.animations.getAnimation('mushroomGrow').play().onComplete.add(function(){
	    	mushroomJump = game.add.sprite(353,423, 'game.mushroomJump');
		    mushroomJump.animations.add('mushroomJump', [0,1,2,3,4,5,6,7,8,9], 5, true);
		    mushroomJump.animations.getAnimation('mushroomJump').play();
	    });

	    showRandButterfly();

	    function showRandMonkey(){
			var randBaba = randomNumber(1,10);

			//add anim
		    monkey1 = game.add.sprite(195,359, 'game.monkey1');
		    monkey1.animations.add('monkey1', [0,1,2,3,4,5,6,7,8,9], 10, false);
		    monkey1.visible = false;

		    monkey2 = game.add.sprite(195,359, 'game.monkey2');
		    monkey2.animations.add('monkey2', [0,1,2,3,4,5,6,7], 10, false);
		    monkey2.visible = false;

		    monkey3 = game.add.sprite(195,359, 'game.monkey3');
		    monkey3.animations.add('monkey3', [0,1,2,3,4,5,6,7,8,9,10,11], 10, false);
		    monkey3.visible = false;

		    monkey4 = game.add.sprite(195,359, 'game.monkey4');
		    monkey4.animations.add('monkey4', [0,1,2,3,4,5], 10, false);
		    monkey4.visible = false;

		    monkey5 = game.add.sprite(195,359, 'game.monkey5');
		    monkey5.animations.add('monkey5', [0,1,2,3,4,5], 10, false);
		    monkey5.visible = false;

		    monkeyWin = game.add.sprite(195,359, 'game.monkeyWin');
		    monkeyWin.animations.add('monkeyWin', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, false);
		    monkeyWin.visible = false;

			switch(randBaba) {
				case 1:
					monkey1.visible = true;
					monkey1.animations.getAnimation('monkey1').play().onComplete.add(function(){
						monkey1.animations.stop();
						monkey1.visible = false;
						showRandMonkey();
					});
					break;
				case 2:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				case 3:
					monkey3.visible = true;
					monkey3.animations.getAnimation('monkey3').play().onComplete.add(function(){
						monkey3.animations.stop();
						monkey3.visible = false;
						showRandMonkey();
					});
					break;
				case 4:
					monkey4.visible = true;
					monkey4.animations.getAnimation('monkey4').play().onComplete.add(function(){
						monkey4.animations.stop();
						monkey4.visible = false;
						showRandMonkey();
					});
					break;
				case 5:
					monkey5.visible = true;
					monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
						monkey5.animations.stop();
						monkey5.visible = false;
						showRandMonkey();
					});
					break;
				case 6:
					monkey5.visible = true;
					monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
						monkey5.animations.stop();
						monkey5.visible = false;
						showRandMonkey();
					});
					break;
				case 7:
					monkey5.visible = true;
					monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
						monkey5.animations.stop();
						monkey5.visible = false;
						showRandMonkey();
					});
					break;
				case 8:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				case 9:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				case 10:
					monkey2.visible = true;
					monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
						monkey2.animations.stop();
						monkey2.visible = false;
						showRandMonkey();
					});
					break;
				default: 
						monkey2.visible = true;
						monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
							monkey2.animations.stop();
							monkey1.visible = false;
							showRandMonkey();
						});
				break;
			}
		}

		var checkPosition = 1;

		function showRandButterfly(){
			var randBaba = randomNumber(1,3);

			butterfly1 = game.add.sprite(493,407, 'game.butterfly1');
		    butterfly1.animations.add('butterfly1', [0,1,2,3,4,5,6,7,8], 10, false);
		    butterfly1.visible = false;

		    butterfly2 = game.add.sprite(493,407, 'game.butterfly2');
		    butterfly2.animations.add('butterfly2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
		    butterfly2.visible = false;

		    butterfly3x = game.add.sprite(493,407, 'game.butterfly3x');
		    butterfly3x.animations.add('butterfly3x', [0,1,2,3,4,5], 10, false);
		    butterfly3x.visible = false;

		    butterflyFlies11 = game.add.sprite(493,407, 'game.butterflyFlies1');
            butterflyFlies11.animations.add('butterflyFlies11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
            butterflyFlies11.visible = false;

            butterflyFlies12 = game.add.sprite(493,407, 'game.butterflyFlies2');
            butterflyFlies12.animations.add('butterflyFlies12', [1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
            butterflyFlies12.visible = false;

            butterflyFlies21 = game.add.sprite(493,407, 'game.butterflyFlies1');
            butterflyFlies21.animations.add('butterflyFlies21', [15,14,13,12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies21.visible = false;

            butterflyFlies22 = game.add.sprite(493,407, 'game.butterflyFlies2');
            butterflyFlies22.animations.add('butterflyFlies22', [12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies22.visible = false;   			

			if(checkPosition == 1) {
				switch(randBaba) {
					case 1:
						butterfly1.visible = true;
						butterfly1.animations.getAnimation('butterfly1').play().onComplete.add(function(){
							butterfly1.animations.stop();
							butterfly1.visible = false;
							showRandButterfly();
						});
					break;
					case 2:
						butterfly2.visible = true;
						butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
							butterfly2.animations.stop();
							butterfly2.visible = false;
							showRandButterfly();
						});
					break;
					
					case 3:
						butterflyFlies11.visible = true;
						butterflyFlies11.animations.getAnimation('butterflyFlies11').play().onComplete.add(function(){
							butterflyFlies11.animations.stop();
							butterflyFlies11.visible = false;
							
                            butterflyFlies12.visible = true;
                            butterflyFlies12.animations.getAnimation('butterflyFlies12').play().onComplete.add(function(){
                                butterflyFlies12.animations.stop();
                                butterflyFlies12.visible = false;
                                checkPosition = 2;
                                showRandButterfly();
                            });
						});
					break;

					default: 
							butterfly2.visible = true;
							butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
								butterfly2.animations.stop();
								butterfly2.visible = false;
								showRandButterfly();
							});
					break;
				}
                butterflyR = game.add.sprite(453,409, 'game.butterflyR');
			} else {
				switch(randBaba) {
					case 1:
						butterfly3x.visible = true;
						butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
							butterfly3x.animations.stop();
							butterfly3x.visible = false;
							showRandButterfly();
						});
					break;
					
					case 2:
						butterflyFlies22.visible = true;
						butterflyFlies22.animations.getAnimation('butterflyFlies22').play().onComplete.add(function(){
							butterflyFlies22.animations.stop();
							butterflyFlies22.visible = false;

							butterflyFlies21.visible = true;
                            butterflyFlies21.animations.getAnimation('butterflyFlies21').play().onComplete.add(function(){
                                butterflyFlies21.animations.stop();
                                butterflyFlies21.visible = false;
                                checkPosition = 1;
                                showRandButterfly();
                            });
						});
					break;

					case 3:
						butterfly3x.visible = true;
						butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
							butterfly3x.animations.stop();
							butterfly3x.visible = false;
							showRandButterfly();
						});
					break;

					default: 
							butterfly3x.visible = true;
							butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
								butterfly3x.animations.stop();
								butterfly3x.visible = false;
								showRandButterfly();
							});
					break;
				}
			}

				butterflyR = game.add.sprite(453,409, 'game.butterflyR');
		}

        showRandMonkey();

        game.add.sprite(235,343, 'game.title1');
        monkeyR = game.add.sprite(98,403, 'game.monkeyR');
        
        createLevelButtons();
        full_and_sound();


    };

    game3.update = function () {

    };

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================

(function () {

    var button;

    var game4 = {};

    game4.preload = function () {

    };

    game4.create = function () {
        //Добавление фона
        background = game.add.sprite(0,0, 'game.background');
        background1 = game.add.sprite(95,23, 'game.background1');
        backgroundGame4 = game.add.sprite(95,55, 'game.backgroundGame4');

        winPoint1 = game.add.audio('game.winPoint1');
        winPoint2 = game.add.audio('game.winPoint2');
        losePoint = game.add.audio('game.losePoint');

        take_or_risk_bg = game.add.sprite(320,294, 'game.take_or_risk_bg');

        bitMonkey = game.add.audio('game.bitMonkey');

        take_or_risk_anim = game.add.sprite(320,294, 'game.take_or_risk_anim');	
        take_or_risk_anim.animations.add('take_or_risk_anim', [0,1], 2, true);
        take_or_risk_anim.animations.getAnimation('take_or_risk_anim').play();

        flashFive = game.add.sprite(400,354, 'game.flashFive');	
        flashFive.animations.add('flashFive', [0,1,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3], 6, true);
        flashFive.animations.getAnimation('flashFive').play();

        //добавление кнопок

        var position = 1;

        var startButton = game.add.sprite(596, 511, 'game.start_d');
        startButton.scale.setTo(0.7, 0.7);
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOut.add(function(){
            startButton.loadTexture('game.start');
        });

        var selectGame = game.add.sprite(70,510, 'game.selectGame_d');
        selectGame.scale.setTo(0.7, 0.7);
        selectGame.events.onInputOver.add(function(){
            selectGame.loadTexture('game.selectGame_p');
        });
        selectGame.events.onInputOut.add(function(){
            selectGame.loadTexture('game.selectGame');
        });

        var payTable = game.add.sprite(150,510, 'game.payTable_d');
        payTable.scale.setTo(0.7, 0.7);
        payTable.events.onInputOver.add(function(){
            payTable.loadTexture('game.payTable_p');
        });
        payTable.events.onInputOut.add(function(){
            payTable.loadTexture('game.payTable');
        });

        var line1 = game.add.sprite(250,510, 'game.line1_d');
        line1.scale.setTo(0.7, 0.7);
        /*line1.inputEnabled = true;
        line1.input.useHandCursor = true;*/
        line1.events.onInputUp.add(function(){
            line1.loadTexture('game.line1');
        });
        line1.events.onInputOver.add(function(){
            line1.loadTexture('game.line1_p');
        });
        line1.events.onInputOut.add(function(){
            line1.loadTexture('game.line1');
        });
        line1.events.onInputDown.add(function(){
            
        });

        var line3 = game.add.sprite(295,510, 'game.line3');
        line3.scale.setTo(0.7, 0.7);
        line3.inputEnabled = true;
        line3.input.useHandCursor = true;
        line3.events.onInputUp.add(function(){
        	lockDisplay();

        	monkeyHangingHide();
        	mankeyLeftPoint.visible = true;
        	setTimeout("bitMonkey.play();", 500);
        	take_or_risk_anim.visible = false;
        	mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        		winPoint1.play();
        		goldPoint = game.add.sprite(207,263, 'game.goldPoint');
		        goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
			    goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
			    	mankeyLeftPoint.visible = false;
			    	winPoint2.play();
	        		mankeyWinPoint.visible = true;
	        		mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
	        			mankeyWinPoint.visible = false;
	        			monkeyHanging();

	        			take_or_risk_anim.visible = true;

	        			goldPoint.visible = false;

	        			questionPoint1 = game.add.sprite(207,295, 'game.questionPoint');
				        questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
				        questionPoint1.animations.getAnimation('questionPoint1').play();
	        		});
			    });
        	});


            setTimeout("unlockDisplay();",4000);
            
        });
        line3.events.onInputOver.add(function(){
            line3.loadTexture('game.line3_p');
        });
        line3.events.onInputOut.add(function(){
            line3.loadTexture('game.line3');
        });

        var line5 = game.add.sprite(340,510, 'game.line5_d');
        line5.scale.setTo(0.7, 0.7);
        line5.events.onInputOver.add(function(){
            line5.loadTexture('game.line5_p');
        });
        line5.events.onInputDown.add(function(){
            

        });
        line5.events.onInputOut.add(function(){
            line5.loadTexture('game.line5');
        });


        var line7 = game.add.sprite(385,510, 'game.line7');
        line7.scale.setTo(0.7, 0.7);
        line7.inputEnabled = true;
        line7.input.useHandCursor = true;
        line7.events.onInputOver.add(function(){
            line7.loadTexture('game.line7_p');
        });
        line7.events.onInputDown.add(function(){
        	lockDisplay();
        	setTimeout("bitMonkey.play();", 500);
        	monkeyHangingHide();
        	take_or_risk_anim.visible = false;
        	mankeyRightPoint.visible = true;
        	mankeyRightPoint.animations.getAnimation('mankeyRightPoint').play().onComplete.add(function(){
        		winPoint1.play();
        		losePoint.play();
        		spiderPoint = game.add.sprite(527,263, 'game.spiderPoint');
		        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
			    spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
			    	mankeyRightPoint.visible = false;

	        		mankeyLosePoint.visible = true;
	        		mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
	        			setTimeout("game.state.start('game1');",1500);
	        		});
			    });

        	});


            setTimeout("unlockDisplay();",4500);
        });
        line7.events.onInputOut.add(function(){
            line7.loadTexture('game.line7');
        });

        var line9 = game.add.sprite(430,510, 'game.line9_d');
        line9.scale.setTo(0.7, 0.7);
        /*line9.inputEnabled = true;
        line9.input.useHandCursor = true;*/
        line9.events.onInputOver.add(function(){
            line9.loadTexture('game.line9_p');
        });
        line9.events.onInputDown.add(function(){
            

        });
        line9.events.onInputOut.add(function(){
            line9.loadTexture('game.line9');
        });

        var betone = game.add.sprite(490,510, 'game.betone_d');
        betone.scale.setTo(0.7, 0.7);
        betone.events.onInputOver.add(function(){
            betone.loadTexture('game.betone_p');
        });
        betone.events.onInputOut.add(function(){
            betone.loadTexture('game.betone');
        });

        var betmax = game.add.sprite(535,510, 'game.betmax_d');
        betmax.scale.setTo(0.7, 0.7);
        betmax.events.onInputOver.add(function(){
            betmax.loadTexture('game.betmax_p');
        });
        betmax.events.onInputOut.add(function(){
            betmax.loadTexture('game.betmax');
        });

        var automaricstart = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstart.scale.setTo(0.7, 0.7);
        automaricstart.events.onInputOver.add(function(){
            automaricstart.loadTexture('game.automaricstart_p');
        });
        automaricstart.events.onInputOut.add(function(){
            automaricstart.loadTexture('game.automaricstart');
        });


        mankeyHanging1 = game.add.sprite(305,74, 'game.mankeyHanging1');
        mankeyHanging1.animations.add('mankeyHanging1', [0,1,2,3,4,5], 5, false);
        mankeyHanging1.visible = false;

        mankeyHanging2 = game.add.sprite(305,74, 'game.mankeyHanging2');
        mankeyHanging2.animations.add('mankeyHanging2', [0,1,2,3], 5, false);
        mankeyHanging2.visible = false;

        mankeyHanging3 = game.add.sprite(305,74, 'game.mankeyHanging3');
        mankeyHanging3.animations.add('mankeyHanging3', [0,1,2,3], 5, false);
        mankeyHanging3.visible = false;

        function monkeyHanging(){

			var randBaba = randomNumber(1,7);

            monkeyHangingHide();

			switch(randBaba) {
				case 1:
					mankeyHanging1.visible = true;
					mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
						mankeyHanging1.animations.stop();
						mankeyHanging1.visible = false;
						monkeyHanging();
					});
				break;
				case 2:
					mankeyHanging2.visible = true;
					mankeyHanging2.animations.getAnimation('mankeyHanging2').play().onComplete.add(function(){
						mankeyHanging2.animations.stop();
						mankeyHanging2.visible = false;
						monkeyHanging();
					});
				break;
				case 3:
					mankeyHanging3.visible = true;
					mankeyHanging3.animations.getAnimation('mankeyHanging3').play().onComplete.add(function(){
						mankeyHanging3.animations.stop();
						mankeyHanging3.visible = false;
						monkeyHanging();
					});
				break;

				case 4:
					mankeyHanging1.visible = true;
					mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
						mankeyHanging1.animations.stop();
						mankeyHanging1.visible = false;
						monkeyHanging();
					});
				break;
				case 5:
					mankeyHanging1.visible = true;
					mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
						mankeyHanging1.animations.stop();
						mankeyHanging1.visible = false;
						monkeyHanging();
					});
				break;
				case 6:
					mankeyHanging1.visible = true;
					mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
						mankeyHanging1.animations.stop();
						mankeyHanging1.visible = false;
						monkeyHanging();
					});
				break;
				case 7:
					mankeyHanging1.visible = true;
					mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
						mankeyHanging1.animations.stop();
						mankeyHanging1.visible = false;
						monkeyHanging();
					});
				break;

				default: 
					mankeyHanging1.visible = true;
					mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
						mankeyHanging1.animations.stop();
						mankeyHanging1.visible = false;
						monkeyHanging();
					});
				break;
			}
		}

		function monkeyHangingHide(){
        	mankeyHanging1.visible = false;
        	mankeyHanging2.visible = false;
        	mankeyHanging3.visible = false;

        	mankeyHanging1.animations.stop();
        	mankeyHanging2.animations.stop();
        	mankeyHanging3.animations.stop();
        }

		function randomPoint(){
			if(position == 1){
				x = 0;
			} else {
				x = 320;
			}

			

		    spiderPoint = game.add.sprite(207+x,263, 'game.spiderPoint');
	        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
		    spiderPoint.visible = false;

			var randBaba = randomNumber(1,2);

			switch(randBaba) {
				case 1:
					goldPoint.visible = true;
					goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
						goldPoint.animations.stop();
						goldPoint.visible = false;
					});
				break;

				default: 
					spiderPoint.visible = true;
					spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
						spiderPoint.animations.stop();
						spiderPoint.visible = false;
					});
				break;
			}
		}

		monkeyHanging();
        

        questionPoint1 = game.add.sprite(207,295, 'game.questionPoint');
        questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
        questionPoint1.animations.getAnimation('questionPoint1').play();
        questionPoint1.inputEnabled = true;
        questionPoint1.input.useHandCursor = true;
        questionPoint1.events.onInputUp.add(function(){
        	lockDisplay();

        	monkeyHangingHide();
        	mankeyLeftPoint.visible = true;
        	setTimeout("bitMonkey.play();", 500);
        	take_or_risk_anim.visible = false;
        	mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        		winPoint1.play();
        		goldPoint = game.add.sprite(207,263, 'game.goldPoint');
		        goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
			    goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
			    	mankeyLeftPoint.visible = false;
			    	winPoint2.play();
	        		mankeyWinPoint.visible = true;
	        		mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
	        			mankeyWinPoint.visible = false;
	        			monkeyHanging();

	        			take_or_risk_anim.visible = true;

	        			goldPoint.visible = false;

	        			questionPoint1 = game.add.sprite(207,295, 'game.questionPoint');
				        questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
				        questionPoint1.animations.getAnimation('questionPoint1').play();
	        		});
			    });
        	});


            setTimeout("unlockDisplay();",4000);
            
        });

        questionPoint2 = game.add.sprite(527,295, 'game.questionPoint');
        questionPoint2.animations.add('questionPoint2', [0,1,2,3,4,5,6], 10, true);
        questionPoint2.animations.getAnimation('questionPoint2').play();
        questionPoint2.inputEnabled = true;
        questionPoint2.input.useHandCursor = true;
        questionPoint2.events.onInputDown.add(function(){
        	lockDisplay();
        	setTimeout("bitMonkey.play();", 500);
        	monkeyHangingHide();
        	take_or_risk_anim.visible = false;
        	mankeyRightPoint.visible = true;
        	mankeyRightPoint.animations.getAnimation('mankeyRightPoint').play().onComplete.add(function(){
        		winPoint1.play();
        		losePoint.play();
        		spiderPoint = game.add.sprite(527,263, 'game.spiderPoint');
		        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
			    spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
			    	mankeyRightPoint.visible = false;

	        		mankeyLosePoint.visible = true;
	        		mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
	        			setTimeout("game.state.start('game1');",1500);
	        		});
			    });

        	});


            setTimeout("unlockDisplay();",4500);
        });

        mankeyLeftPoint = game.add.sprite(305,74, 'game.mankeyLeftPoint');
        mankeyLeftPoint.animations.add('mankeyLeftPoint', [0,1,2,3,4,5,6,7], 10, false);
        mankeyLeftPoint.visible = false;

        mankeyRightPoint = game.add.sprite(305,74, 'game.mankeyRightPoint');
        mankeyRightPoint.animations.add('mankeyRightPoint', [0,1,2,3,4,5,6,7], 10, false);
        mankeyRightPoint.visible = false;

        mankeyWinPoint = game.add.sprite(305,74, 'game.mankeyWinPoint');
        mankeyWinPoint.animations.add('mankeyWinPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
        mankeyWinPoint.visible = false;

        mankeyLosePoint = game.add.sprite(305,74, 'game.mankeyLosePoint');	
        mankeyLosePoint.animations.add('mankeyLosePoint', [0,1,2,3,4,5,6], 10, false);
        mankeyLosePoint.visible = false;


        createLevelButtons();
        full_and_sound();
    };

    game4.update = function () {
    };

    game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('game.background', 'img/canvas-bg.svg'); 
        game.load.image('game.background1', 'img/shape319.png'); 

        game.load.image('game.start', 'img/image1445.png');
        game.load.image('game.start_p', 'img/image1447.png');
        game.load.image('game.start_d', 'img/image1451.png');

        game.load.image('game.selectGame', 'img/image1419.png');
        game.load.image('game.selectGame_p', 'img/image1421.png');
        game.load.image('game.selectGame_d', 'img/image1425.png');
        game.load.image('game.payTable', 'img/image1428.png');
        game.load.image('game.payTable_p', 'img/image1430.png');
        game.load.image('game.payTable_d', 'img/image1433.png');
        game.load.image('game.automaricstart', 'img/image1436.png');
        game.load.image('game.automaricstart_p', 'img/image1438.png');
        game.load.image('game.automaricstart_d', 'img/image1442.png');
        game.load.image('game.betone', 'img/image1471.png');
        game.load.image('game.betone_p', 'img/image1473.png');
        game.load.image('game.betone_d', 'img/image1477.png');
        game.load.image('game.line1', 'img/image1505.png');
        game.load.image('game.line1_p', 'img/image1507.png');
        game.load.image('game.line1_d', 'img/image1511.png');
        game.load.image('game.betmax', 'img/image1480.png');
        game.load.image('game.betmax_p', 'img/image1482.png');
        game.load.image('game.betmax_d', 'img/image1485.png');
        game.load.image('game.line3', 'img/image1496.png');
        game.load.image('game.line3_p', 'img/image1498.png');
        game.load.image('game.line3_d', 'img/image1502.png');
        game.load.image('game.line5', 'img/image1488.png');
        game.load.image('game.line5_p', 'img/image1490.png');
        game.load.image('game.line5_d', 'img/image1493.png');
        game.load.image('game.line7', 'img/image1462.png');
        game.load.image('game.line7_p', 'img/image1464.png');
        game.load.image('game.line7_d', 'img/image1468.png');
        game.load.image('game.line9', 'img/image1454.png');
        game.load.image('game.line9_p', 'img/image1456.png');
        game.load.image('game.line9_d', 'img/image1459.png');

        game.load.image('game.number1', 'img/1.png');
        game.load.image('game.number2', 'img/2.png');
        game.load.image('game.number3', 'img/3.png');
        game.load.image('game.number4', 'img/4.png');
        game.load.image('game.number5', 'img/5.png');
        game.load.image('game.number6', 'img/6.png');
        game.load.image('game.number7', 'img/7.png');
        game.load.image('game.number8', 'img/8.png');
        game.load.image('game.number9', 'img/9.png');

        game.load.image('game.bars', 'img/bars.png');
        
        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');

        for (var i = 1; i <= 9; ++i) {

        	if(i==5 || i==7 || i==9 || i==8){
        		game.load.image('line_' + i, 'lines/select/' + i + '.png');
        	} else { 	
        		game.load.image('line_' + i, 'lines/select/' + i + '.png');
        	}

            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        game.load.image('bars', 'bars.png');
        game.load.image('start', 'start.png');
        game.load.image('start_p', 'start_p.png');
        game.load.image('start_d', 'start_d.png');
        game.load.image('bet', 'bet.png');
        game.load.image('canvasbg', 'canvas-bg.png');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');
        game.load.audio('takeWin', 'takeWin.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');

        game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 608, 32);
        game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 608, 32);
        game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 608, 32);
        game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 608, 32);
        game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 608, 32);
        game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 608, 32);
        game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 608, 32);
        game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 608, 32);
        game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.mask', 'img/mask.png', 96, 96);
        game.load.spritesheet('game.monkey', 'img/monkey.png', 96, 96);
        game.load.audio('winMonkey', 'sound/winMonkey.mp3');


        game.load.image('game.backgroundGame3', 'img/shape164.svg');
        game.load.audio('game.openCardAudio', 'sound/sound31.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');
        
        //карты
        game.load.image('game.cards_bg', 'img/shape167.png');
        game.load.image('game.card_garage', 'img/shape167.png');

        game.load.image('game.card_2b', 'img/shape169.png');
        game.load.image('game.card_3b', 'img/shape171.png');
        game.load.image('game.card_4b', 'img/shape173.png');
        game.load.image('game.card_5b', 'img/shape175.png');
        game.load.image('game.card_6b', 'img/shape177.png');
        game.load.image('game.card_7b', 'img/shape179.png');
        game.load.image('game.card_8b', 'img/shape181.png');
        game.load.image('game.card_9b', 'img/shape183.png');
        game.load.image('game.card_10b', 'img/shape185.png');
        game.load.image('game.card_jb', 'img/shape187.png');
        game.load.image('game.card_qb', 'img/shape189.png');
        game.load.image('game.card_kb', 'img/shape191.png');
        game.load.image('game.card_ab', 'img/shape193.png');

        game.load.image('game.card_2ch', 'img/shape195.png');
        game.load.image('game.card_3ch', 'img/shape197.png');
        game.load.image('game.card_4ch', 'img/shape199.png');
        game.load.image('game.card_5ch', 'img/shape201.png');
        game.load.image('game.card_6ch', 'img/shape203.png');
        game.load.image('game.card_7ch', 'img/shape205.png');
        game.load.image('game.card_8ch', 'img/shape207.png');
        game.load.image('game.card_9ch', 'img/shape209.png');
        game.load.image('game.card_10ch', 'img/shape211.png');
        game.load.image('game.card_jch', 'img/shape213.png');
        game.load.image('game.card_qch', 'img/shape215.png');
        game.load.image('game.card_kch', 'img/shape217.png');
        game.load.image('game.card_ach', 'img/shape219.png');

        game.load.image('game.card_2k', 'img/shape221.png');
        game.load.image('game.card_3k', 'img/shape223.png');
        game.load.image('game.card_4k', 'img/shape225.png');
        game.load.image('game.card_5k', 'img/shape227.png');
        game.load.image('game.card_6k', 'img/shape229.png');
        game.load.image('game.card_7k', 'img/shape231.png');
        game.load.image('game.card_8k', 'img/shape233.png');
        game.load.image('game.card_9k', 'img/shape235.png');
        game.load.image('game.card_10k', 'img/shape237.png');
        game.load.image('game.card_jk', 'img/shape239.png');
        game.load.image('game.card_qk', 'img/shape241.png');
        game.load.image('game.card_kk', 'img/shape243.png');
        game.load.image('game.card_ak', 'img/shape245.png');

        game.load.image('game.card_2p', 'img/shape247.png');
        game.load.image('game.card_3p', 'img/shape249.png');
        game.load.image('game.card_4p', 'img/shape251.png');
        game.load.image('game.card_5p', 'img/shape253.png');
        game.load.image('game.card_6p', 'img/shape255.png');
        game.load.image('game.card_7p', 'img/shape257.png');
        game.load.image('game.card_8p', 'img/shape259.png');
        game.load.image('game.card_9p', 'img/shape261.png');
        game.load.image('game.card_10p', 'img/shape263.png');
        game.load.image('game.card_jp', 'img/shape265.png');
        game.load.image('game.card_qp', 'img/shape267.png');
        game.load.image('game.card_kp', 'img/shape269.png');
        game.load.image('game.card_ap', 'img/shape271.png');
		game.load.image('game.card_joker', 'img/shape273.png');

        game.load.image('game.pick', 'img/shape281.png');

        game.load.spritesheet('game.monkey1', 'img/monkey1.png', 160, 144);
        game.load.spritesheet('game.monkey2', 'img/monkey2.png', 160, 144);
        game.load.spritesheet('game.monkey3', 'img/monkey3.png', 160, 144);
        game.load.spritesheet('game.monkey4', 'img/monkey4.png', 160, 144);
        game.load.spritesheet('game.monkey5', 'img/monkey5.png', 160, 144);
        game.load.spritesheet('game.monkeyWin', 'img/monkeyWin.png', 160, 144);
        game.load.spritesheet('game.monkeyTakeMushroom1', 'img/monkeyTakeMushroom1.png', 224, 144);
        game.load.spritesheet('game.monkeyTakeMushroom2', 'img/monkeyTakeMushroom2.png', 224, 144);
        game.load.spritesheet('game.butterfly1', 'img/butterfly1.png', 240, 96);
        game.load.spritesheet('game.butterfly2', 'img/butterfly2.png', 240, 96);
        game.load.spritesheet('game.butterfly3x', 'img/butterfly3x.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies1', 'img/butterflyFlies1.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies2', 'img/butterflyFlies2.png', 240, 96);
        game.load.spritesheet('game.mushroomGrow', 'img/mushroomGrow.png', 64, 80);
        game.load.spritesheet('game.mushroomJump', 'img/mushroomJump.png', 64, 80);

        game.load.image('game.backgroundGame2', 'img/shape1150.svg');
        game.load.spritesheet('game.rope', 'img/rope.png', 16, 320);

        game.load.spritesheet('game.monkey21', 'img/monkey21.png', 176, 480);
        game.load.spritesheet('game.monkey22', 'img/monkey22.png', 176, 480);
        game.load.spritesheet('game.monkey23', 'img/monkey23.png', 176, 480);
        game.load.spritesheet('game.monkey24', 'img/monkey24.png', 176, 480);

        game.load.spritesheet('game.monkeyTakeRope1', 'img/monkeyTakeRope1.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope2', 'img/monkeyTakeRope2.png', 176, 480);
        game.load.spritesheet('game.monkeyEatBanana', 'img/monkeyEatBanana.png', 176, 480);
		game.load.spritesheet('game.monkeyEatHammer1', 'img/monkeyEatHammer1.png', 160, 480);
		game.load.spritesheet('game.monkeyEatHammer12', 'img/monkeyEatHammer12.png', 176, 480);
        game.load.spritesheet('game.monkeyEatHammer2', 'img/monkeyEatHammer2.png', 160, 480);
        game.load.spritesheet('game.monkeyEatHammer22', 'img/monkeyEatHammer22.png', 176, 480);
        game.load.spritesheet('game.monkeyHeadache', 'img/monkeyHeadache.png', 176, 480);
        
        game.load.audio('game.winRope1', 'sound/winRope1.mp3');
        game.load.audio('game.winRope2', 'sound/winRope2.mp3');
        game.load.audio('game.bitMonkey', 'sound/bitMonkey.mp3');

        game.load.image('game.backgroundGame4', 'img/shape943.svg');

        game.load.spritesheet('game.mankeyHanging1', 'img/mankeyHanging1.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging2', 'img/mankeyHanging2.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging3', 'img/mankeyHanging3.png', 240, 304);
        game.load.spritesheet('game.mankeyLeftPoint', 'img/mankeyLeftPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyLosePoint', 'img/mankeyLosePoint.png', 240, 304);
        game.load.spritesheet('game.mankeyRightPoint', 'img/mankeyRightPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyWinPoint', 'img/mankeyWinPoint.png', 240, 304);

        game.load.spritesheet('game.goldPoint', 'img/goldPoint.png', 96, 128);
        game.load.spritesheet('game.questionPoint', 'img/questionPoint.png', 96, 80);
        game.load.spritesheet('game.spiderPoint', 'img/spiderPoint.png', 96, 128);
        game.load.spritesheet('game.flashFive', 'img/flashFive.png', 32, 48);
        game.load.spritesheet('game.take_or_risk_anim', 'img/take_or_risk_anim.png', 192, 160);

        game.load.image('game.take_or_risk_bg', 'img/shape953.svg');
        game.load.audio('game.winPoint1', 'sound/winPoint1.mp3');
        game.load.audio('game.winPoint2', 'sound/winPoint2.mp3');
        game.load.audio('game.losePoint', 'sound/losePoint.mp3');   

        game.load.image('game.title1', 'img/title1.jpg');

        game.load.image('game.monkeyR', 'img/monkeyR.png');
        game.load.image('game.butterflyR', 'img/butterflyR.png');

        
             

    };

    preload.create = function() {
        game.state.start('game1');
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');