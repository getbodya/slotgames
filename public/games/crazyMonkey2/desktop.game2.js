function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        background = game.add.sprite(0,0, 'game.background');
        background1 = game.add.sprite(95,23, 'game.background1');
        background1 = game.add.sprite(95,23, 'topScoreGame2');
        backgroundGame3 = game.add.sprite(95,55, 'game.backgroundGame3');

        addTableTitle(game, 'winTitleGame2', 365,343);
        hideTableTitle();

        //счет
        step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[113,119], [273,119], [385,119], [500,119], [612,119]];
        addCards(game, cardPosition);

        openDCard(dcard);

        // кнопки
        addButtonsGame2(game);

        //анимация
        function showRandMonkey(){
            var randBaba = randomNumber(1,10);

            //add anim
            monkey1 = game.add.sprite(193,359, 'game.monkey1');
            monkey1.animations.add('monkey1', [0,1,2,3,4,5,6,7,8,9], 10, false);
            monkey1.visible = false;

            monkey2 = game.add.sprite(193,359, 'game.monkey2');
            monkey2.animations.add('monkey2', [0,1,2,3,4,5,6,7], 10, false);
            monkey2.visible = false;

            monkey3 = game.add.sprite(193,359, 'game.monkey3');
            monkey3.animations.add('monkey3', [0,1,2,3,4,5,6,7,8,9,10,11], 10, false);
            monkey3.visible = false;

            monkey4 = game.add.sprite(193,359, 'game.monkey4');
            monkey4.animations.add('monkey4', [0,1,2,3,4,5], 10, false);
            monkey4.visible = false;

            monkey5 = game.add.sprite(193,359, 'game.monkey5');
            monkey5.animations.add('monkey5', [0,1,2,3,4,5], 10, false);
            monkey5.visible = false;


            monkeyWin = game.add.sprite(193,359, 'game.monkeyWin');
            monkeyWin.animations.add('monkeyWin', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true);
            monkeyWin.visible = false;

            switch(randBaba) {
                case 1:
                    monkey1.visible = true;
                    monkey1.animations.getAnimation('monkey1').play().onComplete.add(function(){
                        monkey1.animations.stop();
                        monkey1.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 2:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 3:
                    monkey3.visible = true;
                    monkey3.animations.getAnimation('monkey3').play().onComplete.add(function(){
                        monkey3.animations.stop();
                        monkey3.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 4:
                    monkey4.visible = true;
                    monkey4.animations.getAnimation('monkey4').play().onComplete.add(function(){
                        monkey4.animations.stop();
                        monkey4.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 5:
                    monkey5.visible = true;
                    monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
                        monkey5.animations.stop();
                        monkey5.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 6:
                    monkey5.visible = true;
                    monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
                        monkey5.animations.stop();
                        monkey5.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 7:
                    monkey5.visible = true;
                    monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
                        monkey5.animations.stop();
                        monkey5.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 8:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 9:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 10:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                default:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey1.visible = false;
                        showRandMonkey();
                    });
                    break;
            }
        }

        var checkPositionButterfly = 1;
        function showRandButterfly(){
            var randBaba = randomNumber(1,3);

            butterfly1 = game.add.sprite(493,407, 'game.butterfly1');
            butterfly1.animations.add('butterfly1', [0,1,2,3,4,5,6,7,8], 10, false);
            butterfly1.visible = false;

            butterfly2 = game.add.sprite(493,407, 'game.butterfly2');
            butterfly2.animations.add('butterfly2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
            butterfly2.visible = false;

            butterfly3x = game.add.sprite(493,407, 'game.butterfly3x');
            butterfly3x.animations.add('butterfly3x', [0,1,2,3,4,5], 10, false);
            butterfly3x.visible = false;

            butterflyFlies11 = game.add.sprite(493,407, 'game.butterflyFlies1');
            butterflyFlies11.animations.add('butterflyFlies11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
            butterflyFlies11.visible = false;

            butterflyFlies12 = game.add.sprite(493,407, 'game.butterflyFlies2');
            butterflyFlies12.animations.add('butterflyFlies12', [1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
            butterflyFlies12.visible = false;

            butterflyFlies21 = game.add.sprite(493,407, 'game.butterflyFlies1');
            butterflyFlies21.animations.add('butterflyFlies21', [15,14,13,12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies21.visible = false;

            butterflyFlies22 = game.add.sprite(493,407, 'game.butterflyFlies2');
            butterflyFlies22.animations.add('butterflyFlies22', [12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies22.visible = false;

            butterflyR = game.add.sprite(453,409, 'game.butterflyR');
            monkeyR = game.add.sprite(95,403, 'game.monkeyR');


            if(checkPositionButterfly == 1) {
                switch(randBaba) {
                    case 1:
                        butterfly1.visible = true;
                        butterfly1.animations.getAnimation('butterfly1').play().onComplete.add(function(){
                            butterfly1.animations.stop();
                            butterfly1.visible = false;
                            showRandButterfly();
                        });
                        break;
                    case 2:
                        butterfly2.visible = true;
                        butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
                            butterfly2.animations.stop();
                            butterfly2.visible = false;
                            showRandButterfly();
                        });
                        break;

                    case 3:
                        butterflyFlies11.visible = true;
                        butterflyFlies11.animations.getAnimation('butterflyFlies11').play().onComplete.add(function(){
                            butterflyFlies11.animations.stop();
                            butterflyFlies11.visible = false;

                            butterflyFlies12.visible = true;
                            butterflyFlies12.animations.getAnimation('butterflyFlies12').play().onComplete.add(function(){
                                butterflyFlies12.animations.stop();
                                butterflyFlies12.visible = false;

                                checkPositionButterfly = 2;
                                showRandButterfly();
                            });
                        });
                        break;

                    default:
                        butterfly2.visible = true;
                        butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
                            butterfly2.animations.stop();
                            butterfly2.visible = false;
                            showRandButterfly();
                        });
                        break;
                }
            } else {
                switch(randBaba) {
                    case 1:
                        butterfly3x.visible = true;
                        butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
                            butterfly3x.animations.stop();
                            butterfly3x.visible = false;
                            showRandButterfly();
                        });
                        break;

                    case 2:
                        butterflyFlies22.visible = true;
                        butterflyFlies22.animations.getAnimation('butterflyFlies22').play().onComplete.add(function(){
                            butterflyFlies22.animations.stop();
                            butterflyFlies22.visible = false;

                            butterflyFlies21.visible = true;
                            butterflyFlies21.animations.getAnimation('butterflyFlies21').play().onComplete.add(function(){
                                butterflyFlies21.animations.stop();
                                butterflyFlies21.visible = false;
                                checkPositionButterfly = 1;
                                showRandButterfly();
                            });
                        });
                        break;

                    case 3:
                        butterfly3x.visible = true;
                        butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
                            butterfly3x.animations.stop();
                            butterfly3x.visible = false;
                            showRandButterfly();
                        });
                        break;

                    default:
                        butterfly3x.visible = true;
                        butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
                            butterfly3x.animations.stop();
                            butterfly3x.visible = false;
                            showRandButterfly();
                        });
                        break;
                }
            }


        }

        showRandButterfly();
        showRandMonkey();

        showDoubleToAndTakeOrRiskTexts(game, totalWin, 340, 345);

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
         full.loadTexture('game.non_full');
         fullStatus = false;
     }
    };

    game.state.add('game2', game2);

    // звуки
}