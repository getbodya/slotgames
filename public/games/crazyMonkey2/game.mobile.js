window.game = new Phaser.Game(640, 480, Phaser.AUTO, 'phaser-example');

isMobile = true;

var mobileX = 93;
var mobileY = 23;

var checkHelm = false; //проверка надела ли каска

//переменные получаемые из api
var gamename = 'crmonkey2rus'; //название игры
var result;
var state;
var sid;
var user;
var min;
var id;
var balance = 10000000;
var extralife = 45;
var jackpots;
var betline = 1;
var lines = 9;
var bet = 9;
var info;
var wl;
var dcard;
var dwin;
var dcard2;
var select;

//звуки и полноэкранный режим
var fullStatus = false;
var soundStatus = true;

//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

//Функции связанные с линиями и их номерами

var line1; var linefull1; var number1;
var line2; var linefull2; var number2;
var line3; var linefull3; var number3;
var line4; var linefull4; var number4;
var line5; var linefull5; var number5;
var line6; var linefull6; var number6;
var line7; var linefull7; var number7;
var line8; var linefull8; var number8;
var line9; var linefull9; var number9;

//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9'];
    var lineNames = ['line1', 'line2', 'line3', 'line4', 'line5', 'line6', 'line7', 'line8', 'line9'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);

    linefull1.visible = false;
    linefull2.visible = false;
    linefull3.visible = false;
    linefull4.visible = false;
    linefull5.visible = false;
    linefull6.visible = false;
    linefull7.visible = false;
    linefull8.visible = false;
    linefull9.visible = false;

    line1 = game.add.sprite(linePosition[0][0], linePosition[0][1], lineNames[0]);
    line2 = game.add.sprite(linePosition[1][0], linePosition[1][1], lineNames[1]);
    line3 = game.add.sprite(linePosition[2][0], linePosition[2][1], lineNames[2]);
    line4 = game.add.sprite(linePosition[3][0], linePosition[3][1], lineNames[3]);
    line5 = game.add.sprite(linePosition[4][0], linePosition[4][1], lineNames[4]);
    line6 = game.add.sprite(linePosition[5][0], linePosition[5][1], lineNames[5]);
    line7 = game.add.sprite(linePosition[6][0], linePosition[6][1], lineNames[6]);
    line8 = game.add.sprite(linePosition[7][0], linePosition[7][1], lineNames[7]);
    line9 = game.add.sprite(linePosition[8][0], linePosition[8][1], lineNames[8]);

    line1.visible = false;
    line2.visible = false;
    line3.visible = false;
    line4.visible = false;
    line5.visible = false;
    line6.visible = false;
    line7.visible = false;
    line8.visible = false;
    line9.visible = false;

    number1 = game.add.sprite(numberPosition[0][0], numberPosition[0][1], 'game.number1');
    number2 = game.add.sprite(numberPosition[1][0], numberPosition[1][1], 'game.number2');
    number3 = game.add.sprite(numberPosition[2][0], numberPosition[2][1], 'game.number3');
    number4 = game.add.sprite(numberPosition[3][0], numberPosition[3][1], 'game.number4');
    number5 = game.add.sprite(numberPosition[4][0], numberPosition[4][1], 'game.number5');
    number6 = game.add.sprite(numberPosition[5][0], numberPosition[5][1], 'game.number6');
    number7 = game.add.sprite(numberPosition[6][0], numberPosition[6][1], 'game.number7');
    number8 = game.add.sprite(numberPosition[7][0], numberPosition[7][1], 'game.number8');
    number9 = game.add.sprite(numberPosition[8][0], numberPosition[8][1], 'game.number9');

    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;

}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
var linesArray = [];
function hideLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        line1.visible = false;
        line2.visible = false;
        line3.visible = false;
        line4.visible = false;
        line5.visible = false;
        line6.visible = false;
        line7.visible = false;
        line8.visible = false;
        line9.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                    linefull1.visible = false;
                    break;
                case 2:
                    linefull2.visible = false;
                    break;
                case 3:
                    linefull3.visible = false;
                    break;
                case 4:
                    linefull4.visible = false;
                    break;
                case 5:
                    linefull5.visible = false;
                    break;
                case 6:
                    linefull6.visible = false;
                    break;
                case 7:
                    linefull7.visible = false;
                    break;
                case 8:
                    linefull8.visible = false;
                    break;
                case 9:
                    linefull9.visible = false;
                    break;
                case 11:
                    line1.visible = false;
                    break;
                case 12:
                    line2.visible = false;
                    break;
                case 13:
                    line3.visible = false;
                    break;
                case 14:
                    line4.visible = false;
                    break;
                case 15:
                    line5.visible = false;
                    break;
                case 16:
                    line6.visible = false;
                    break;
                case 17:
                    line7.visible = false;
                    break;
                case 18:
                    line8.visible = false;
                    break;
                case 19:
                    line9.visible = false;
                    break;
            }
        });
    }
}

function showLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        line1.visible = true;
        line2.visible = true;
        line3.visible = true;
        line4.visible = true;
        line5.visible = true;
        line6.visible = true;
        line7.visible = true;
        line8.visible = true;
        line9.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                    linefull1.visible = true;
                    break;
                case 2:
                    linefull2.visible = true;
                    break;
                case 3:
                    linefull3.visible = true;
                    break;
                case 4:
                    linefull4.visible = true;
                    break;
                case 5:
                    linefull5.visible = true;
                    break;
                case 6:
                    linefull6.visible = true;
                    break;
                case 7:
                    linefull7.visible = true;
                    break;
                case 8:
                    linefull8.visible = true;
                    break;
                case 9:
                    linefull9.visible = true;
                    break;
                case 11:
                    line1.visible = true;
                    break;
                case 12:
                    line2.visible = true;
                    break;
                case 13:
                    line3.visible = true;
                    break;
                case 14:
                    line4.visible = true;
                    break;
                case 15:
                    line5.visible = true;
                    break;
                case 16:
                    line6.visible = true;
                    break;
                case 17:
                    line7.visible = true;
                    break;
                case 18:
                    line8.visible = true;
                    break;
                case 19:
                    line9.visible = true;
                    break;
            }
        });
    }
}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = false;
        number2.visible = false;
        number3.visible = false;
        number4.visible = false;
        number5.visible = false;
        number6.visible = false;
        number7.visible = false;
        number8.visible = false;
        number9.visible = false;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                    number1.visible = false;
                    break;
                case 2:
                    number2.visible = false;
                    break;
                case 3:
                    number3.visible = false;
                    break;
                case 4:
                    number4.visible = false;
                    break;
                case 5:
                    number5.visible = false;
                    break;
                case 6:
                    number6.visible = false;
                    break;
                case 7:
                    number7.visible = false;
                    break;
                case 8:
                    number8.visible = false;
                    break;
                case 9:
                    number9.visible = false;
                    break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                    number1.visible = true;
                    break;
                case 2:
                    number2.visible = true;
                    break;
                case 3:
                    number3.visible = true;
                    break;
                case 4:
                    number4.visible = true;
                    break;
                case 5:
                    number5.visible = true;
                    break;
                case 6:
                    number6.visible = true;
                    break;
                case 7:
                    number7.visible = true;
                    break;
                case 8:
                    number8.visible = true;
                    break;
                case 9:
                    number9.visible = true;
                    break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}



// Функции связанные с кнопками
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;

//кнопки для слотов
function addButtonsGame1(game) {

    //звуки для кнопок
    var line1Sound = game.add.audio('line1Sound');
    var line3Sound = game.add.audio('line3Sound');
    var line5Sound = game.add.audio('line5Sound');
    var line7Sound = game.add.audio('line7Sound');
    var line9Sound = game.add.audio('line9Sound');

    //var soundForBattons = []; - массив содержащий объекты звуков для кнопок
    var soundForBattons = [];
    soundForBattons.push(line1Sound, line3Sound, line5Sound, line7Sound, line9Sound);

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150,510, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490,510, 'betone');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        betone.loadTexture('betone_p');
    });
    betone.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            upBetline(betlineOptions);
            updateBetinfo(game, scorePosions, lines, betline);
        }
    });
    betone.events.onInputOut.add(function(){
        betone.loadTexture('betone');
    });


    betmax = game.add.sprite(535,510, 'betmax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        betmax.loadTexture('betmax_p');
    });
    betmax.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            maxBetline();
            updateBetinfo(game, scorePosions, lines, betline);
            //betMaxSound.play();
        }
    });
    betmax.events.onInputOut.add(function(){
        betmax.loadTexture('betmax');
    });

    automaricstart = game.add.sprite(685,510, 'automaricstart');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = true;
    automaricstart.input.useHandCursor = true;
    automaricstart.events.onInputOver.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart_p');
        }
    });
    automaricstart.events.onInputOut.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart');
        }
    });
    automaricstart.events.onInputDown.add(function(){
        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;
                    hideLines([]);
                    requestSpin(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                autostart = true;
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton');
        }
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons();
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }

    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;
    buttonLine1.events.onInputOver.add(function(){
        buttonLine1.loadTexture('buttonLine1_p');
    });
    buttonLine1.events.onInputOut.add(function(){
        buttonLine1.loadTexture('buttonLine1');
    });
    buttonLine1.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1]);
    });
    buttonLine1.events.onInputDown.add(function(){
        soundForBattons[0].play();
        hideLines([]);
        linesArray = [11];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1];
        showNumbers(numberArray);

        lines = 1;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3]);
    });
    buttonLine3.events.onInputDown.add(function(){
        soundForBattons[1].play();

        hideLines([]);
        linesArray = [11, 12, 13];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3];
        showNumbers(numberArray);

        lines = 3;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5]);
    });
    buttonLine5.events.onInputDown.add(function(){
        soundForBattons[2].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5];
        showNumbers(numberArray);

        lines = 5;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7]);
    });
    buttonLine7.events.onInputDown.add(function(){
        soundForBattons[3].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7];
        showNumbers(numberArray);

        lines = 7;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });
    buttonLine9.events.onInputDown.add(function(){
        soundForBattons[4].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17, 18, 19];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        showNumbers(numberArray);

        lines = 9;
        updateBetinfo(game, scorePosions, lines, betline);
    });

}

//кнопки для карт
function addButtonsGame2(game) {

    var soundForBattons = [];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        startButton.loadTexture('startButton_p');
    });
    startButton.events.onInputOut.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputDown.add(function(){
        requestDouble(gamename, 1, lines, bet, sid);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputDown.add(function(){
        requestDouble(gamename, 2, lines, bet, sid);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputDown.add(function(){
        requestDouble(gamename, 3, lines, bet, sid);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputDown.add(function(){
        requestDouble(gamename, 4, lines, bet, sid);
    });

}

//кнопки для игры с последовательным выбором (действия при нажатии некоторых кнопок нужно задать в самой игре)
/*function addButtonsGame3(game) {
 var selectGame = game.add.sprite(70,510, 'selectGame_d');
 selectGame.scale.setTo(0.7, 0.7);
 selectGame.inputEnabled = false;

 var payTable = game.add.sprite(150,510, 'payTable_d');
 payTable.scale.setTo(0.7, 0.7);
 payTable.inputEnabled = false;

 var betone = game.add.sprite(490,510, 'betone_d');
 betone.scale.setTo(0.7, 0.7);
 betone.inputEnabled = false;


 var betmax = game.add.sprite(535,510, 'betmax_d');
 betmax.scale.setTo(0.7, 0.7);
 betmax.inputEnabled = false;

 var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
 automaricstart.scale.setTo(0.7, 0.7);
 automaricstart.inputEnabled = false;

 var startButton = game.add.sprite(597, 510, 'startButton_d');
 startButton.scale.setTo(0.7,0.7);
 startButton.inputEnabled = false;

 var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
 buttonLine1.scale.setTo(0.7,0.7);
 buttonLine1.inputEnabled = true;
 buttonLine1.input.useHandCursor = true;

 var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
 buttonLine3.scale.setTo(0.7,0.7);
 buttonLine3.inputEnabled = true;
 buttonLine3.input.useHandCursor = true;

 var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
 buttonLine5.scale.setTo(0.7,0.7);
 buttonLine5.inputEnabled = true;
 buttonLine5.input.useHandCursor = true;

 var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
 buttonLine7.scale.setTo(0.7,0.7);
 buttonLine7.inputEnabled = true;
 buttonLine7.input.useHandCursor = true;

 var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
 buttonLine9.scale.setTo(0.7,0.7);
 buttonLine9.inputEnabled = true;
 buttonLine9.input.useHandCursor = true;

 buttonLine1.events.onInputOver.add(function(){
 if(buttonLine1.inputEnabled == true) {
 buttonLine1.loadTexture('buttonLine1_p');
 }
 });
 buttonLine1.events.onInputOut.add(function(){
 if(buttonLine1.inputEnabled == true) {
 buttonLine1.loadTexture('buttonLine1');
 }
 });

 buttonLine3.events.onInputOver.add(function(){
 if(buttonLine3.inputEnabled == true) {
 buttonLine3.loadTexture('buttonLine3_p');
 }
 });
 buttonLine3.events.onInputOut.add(function(){
 if(buttonLine3.inputEnabled == true) {
 buttonLine3.loadTexture('buttonLine3');
 }
 });

 buttonLine5.events.onInputOver.add(function(){
 if(buttonLine5.inputEnabled == true) {
 buttonLine5.loadTexture('buttonLine5_p');
 }
 });
 buttonLine5.events.onInputOut.add(function(){
 if(buttonLine5.inputEnabled == true) {
 buttonLine5.loadTexture('buttonLine5');
 }
 });

 buttonLine7.events.onInputOver.add(function(){
 if(buttonLine7.inputEnabled == true) {
 buttonLine7.loadTexture('buttonLine7_p');
 }
 });
 buttonLine7.events.onInputOut.add(function(){
 if(buttonLine7.inputEnabled == true) {
 buttonLine7.loadTexture('buttonLine7');
 }
 });

 buttonLine9.events.onInputOver.add(function(){
 if(buttonLine9.inputEnabled == true) {
 buttonLine9.loadTexture('buttonLine9_p');
 }
 });
 buttonLine9.events.onInputOut.add(function(){
 if(buttonLine9.inputEnabled == true) {
 buttonLine9.loadTexture('buttonLine9');
 }
 });

 buttonLine1.events.onInputDown.add(function(){
 takeBananaRope(180,22,1);

 buttonLine1.loadTexture('buttonLine1_d');
 buttonLine1.inputEnabled = false;
 buttonLine1.input.useHandCursor = false;

 if(ropeValues[ropeStep] != 0) {
 stepTotalWinR += betline*lines*ropeValues[ropeStep];
 }

 setTimeout(showWinGame3,2500, 180+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
 });
 buttonLine3.events.onInputDown.add(function(){
 takeBananaRope(275,22,2);

 buttonLine3.loadTexture('buttonLine3_d');
 buttonLine3.inputEnabled = false;
 buttonLine3.input.useHandCursor = false;

 if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
 stepTotalWinR += betline*lines*ropeValues[ropeStep];
 }

 setTimeout(showWinGame3,2500, 275+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
 });
 buttonLine5.events.onInputDown.add(function(){
 takeBananaRope(365,22,3);

 buttonLine5.loadTexture('buttonLine5_d');
 buttonLine5.inputEnabled = false;
 buttonLine5.input.useHandCursor = false;

 if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
 stepTotalWinR += betline*lines*ropeValues[ropeStep];
 }

 setTimeout(showWinGame3,2500, 365+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
 });
 buttonLine7.events.onInputDown.add(function(){
 takeBananaRope(455,22,4);

 buttonLine7.loadTexture('buttonLine7_d');
 buttonLine7.inputEnabled = false;
 buttonLine7.input.useHandCursor = false;

 if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
 stepTotalWinR += betline*lines*ropeValues[ropeStep];
 }

 setTimeout(showWinGame3,2500, 455+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
 });
 buttonLine9.events.onInputDown.add(function(){
 takeBananaRope(540,22,5);

 buttonLine9.loadTexture('buttonLine9_d');
 buttonLine9.inputEnabled = false;
 buttonLine9.input.useHandCursor = false;

 if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
 stepTotalWinR += betline*lines*ropeValues[ropeStep];
 }

 setTimeout(showWinGame3,2500, 540+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
 });
 }*/

// для выбора одного из двух (действия при нажатии некоторых кнопок нужно задать в самой игре)
function addButtonsGame4(game) {
    var selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    var payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    var betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    var betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    var startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = false;

    var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = false;
}

//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
var buttonsArray = [];
function hideButtons(buttonsArray) {
    if(!isMobile){
        if(buttonsArray.length == 0) {
            if(autostart == false){
                automaricstart.loadTexture('automaricstart_d');
                automaricstart.inputEnabled = false;
                automaricstart.input.useHandCursor = false;
            }

            selectGame.loadTexture('selectGame_d');
            selectGame.inputEnabled = false;
            selectGame.input.useHandCursor = false;

            payTable.loadTexture('payTable_d');
            payTable.inputEnabled = false;
            payTable.input.useHandCursor = false;

            betone.loadTexture('betone_d');
            betone.inputEnabled = false;
            betone.input.useHandCursor = false;

            betmax.loadTexture('betmax_d');
            betmax.inputEnabled = false;
            betmax.input.useHandCursor = false;

            startButton.loadTexture('startButton_d');
            startButton.inputEnabled = false;
            startButton.input.useHandCursor = false;

            buttonLine1.loadTexture('buttonLine1_d');
            buttonLine1.inputEnabled = false;
            buttonLine1.input.useHandCursor = false;

            buttonLine3.loadTexture('buttonLine3_d');
            buttonLine3.inputEnabled = false;
            buttonLine3.input.useHandCursor = false;

            buttonLine5.loadTexture('buttonLine5_d');
            buttonLine5.inputEnabled = false;
            buttonLine5.input.useHandCursor = false;

            buttonLine7.loadTexture('buttonLine7_d');
            buttonLine7.inputEnabled = false;
            buttonLine7.input.useHandCursor = false;

            buttonLine9.loadTexture('buttonLine9_d');
            buttonLine9.inputEnabled = false;
            buttonLine9.input.useHandCursor = false;

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]+'_d');
                item[0].inputEnabled = false;
                item[0].input.useHandCursor = false;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = false;
            double.visible = false;
            bet1.visible = false;
            dollar.visible = false;
            gear.visible = false;
            home.visible = false;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = false;
            })
        }
    }
}

function showButtons(buttonsArray) {
    if(!isMobile) {
        if(buttonsArray.length == 0) {

            if(autostart == false){
                automaricstart.loadTexture('automaricstart');
                automaricstart.inputEnabled = true;
                automaricstart.input.useHandCursor = true;

                selectGame.loadTexture('selectGame');
                selectGame.inputEnabled = true;
                selectGame.input.useHandCursor = true;

                payTable.loadTexture('payTable');
                payTable.inputEnabled = true;
                payTable.input.useHandCursor = true;

                betone.loadTexture('betone');
                betone.inputEnabled = true;
                betone.input.useHandCursor = true;

                betmax.loadTexture('betmax');
                betmax.inputEnabled = true;
                betmax.input.useHandCursor = true;

                startButton.loadTexture('startButton');
                startButton.inputEnabled = true;
                startButton.input.useHandCursor = true;

                buttonLine1.loadTexture('buttonLine1');
                buttonLine1.inputEnabled = true;
                buttonLine1.input.useHandCursor = true;

                buttonLine3.loadTexture('buttonLine3');
                buttonLine3.inputEnabled = true;
                buttonLine3.input.useHandCursor = true;

                buttonLine5.loadTexture('buttonLine5');
                buttonLine5.inputEnabled = true;
                buttonLine5.input.useHandCursor = true;

                buttonLine7.loadTexture('buttonLine7');
                buttonLine7.inputEnabled = true;
                buttonLine7.input.useHandCursor = true;

                buttonLine9.loadTexture('buttonLine9');
                buttonLine9.inputEnabled = true;
                buttonLine9.input.useHandCursor = true;
            }

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]);
                item[0].inputEnabled = true;
                item[0].input.useHandCursor = true;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = true;
            double.visible = true;
            bet1.visible = true;
            dollar.visible = true;
            gear.visible = true;
            home.visible = true;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = true;
            })
        }
    }
}


//слоты и их вращение

//переменные содержащие все объекты слотов
var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
var slot1Anim; var slot2Anim; var slot3Anim;  var slot4Anim; var slot5Anim; var slot6Anim; var slot7Anim; var slot8Anim; var slot9Anim; var slot10Anim; var slot11Anim; var slot12Anim; var slot13Anim; var slot14Anim; var slot15Anim;

function addSlots(game, slotPosition) {
    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
    var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];
    var slotCellAnimName = 'cellAnim';

    slot1 = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[0]);
    slot2 = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[1]);
    slot3 = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[3]);
    slot4 = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[4]);
    slot5 = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[7]);
    slot6 = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[2]);
    slot7 = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[4]);
    slot8 = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[3]);
    slot9 = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[8]);
    slot10 = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[2]);
    slot11 = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[0]);
    slot12 = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[1]);
    slot13 = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[5]);
    slot14 = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[3]);
    slot15 = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[5]);

    slot1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotCellAnimName);
    slot2Anim = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotCellAnimName);
    slot3Anim = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotCellAnimName);
    slot4Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotCellAnimName);
    slot5Anim = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotCellAnimName);
    slot6Anim = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotCellAnimName);
    slot7Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotCellAnimName);
    slot8Anim = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotCellAnimName);
    slot9Anim = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotCellAnimName);
    slot10Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotCellAnimName);
    slot11Anim = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotCellAnimName);
    slot12Anim = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotCellAnimName);
    slot13Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotCellAnimName);
    slot14Anim = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotCellAnimName);
    slot15Anim = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotCellAnimName);

    slot1Anim.animations.add('slot1Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot2Anim.animations.add('slot2Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot3Anim.animations.add('slot3Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot4Anim.animations.add('slot4Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot5Anim.animations.add('slot5Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot6Anim.animations.add('slot6Anim', [5,6,7,8,0,1,2,3,4], 20, true);
    slot7Anim.animations.add('slot7Anim', [6,7,8,0,1,2,3,4,5], 20, true);
    slot8Anim.animations.add('slot8Anim', [7,8,0,1,2,3,4,5,6], 20, true);
    slot9Anim.animations.add('slot9Anim', [8,0,1,2,3,4,5,6,7], 20, true);
    slot10Anim.animations.add('slot10Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot11Anim.animations.add('slot11Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot12Anim.animations.add('slot12Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot13Anim.animations.add('slot13Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot14Anim.animations.add('slot14Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot15Anim.animations.add('slot15Anim', [7,8,0,1,2,3,4,5,6], 20, true);

    slot1Anim.animations.getAnimation('slot1Anim').play();
    slot2Anim.animations.getAnimation('slot2Anim').play();
    slot3Anim.animations.getAnimation('slot3Anim').play();
    slot4Anim.animations.getAnimation('slot4Anim').play();
    slot5Anim.animations.getAnimation('slot5Anim').play();
    slot6Anim.animations.getAnimation('slot6Anim').play();
    slot7Anim.animations.getAnimation('slot7Anim').play();
    slot8Anim.animations.getAnimation('slot8Anim').play();
    slot9Anim.animations.getAnimation('slot9Anim').play();
    slot10Anim.animations.getAnimation('slot10Anim').play();
    slot11Anim.animations.getAnimation('slot11Anim').play();
    slot12Anim.animations.getAnimation('slot12Anim').play();
    slot13Anim.animations.getAnimation('slot13Anim').play();
    slot14Anim.animations.getAnimation('slot14Anim').play();
    slot15Anim.animations.getAnimation('slot15Anim').play();

    slot1Anim.visible = false;
    slot2Anim.visible = false;
    slot3Anim.visible = false;
    slot4Anim.visible = false;
    slot5Anim.visible = false;
    slot6Anim.visible = false;
    slot7Anim.visible = false;
    slot8Anim.visible = false;
    slot9Anim.visible = false;
    slot10Anim.visible = false;
    slot11Anim.visible = false;
    slot12Anim.visible = false;
    slot13Anim.visible = false;
    slot14Anim.visible = false;
    slot15Anim.visible = false;
}

var finalValues; var wlValues; var balanceR; var totalWin; var totalWinR; var dcard; var linesR; var betlineR; //totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');
    
    if (find(dataArray, 'result=ok') !== -1 && find(dataArray, 'state=0') !== -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        if(typeof finalValues != "undefined") {
            totalWinR = finalValues[15];
            linesR = finalValues[16];
            betlineR = finalValues[17];

            checkSpinResult(totalWinR);

            slotRotation(game, finalValues);
        }




    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons([]);

        checkRopeGameAnim = 1;

        var winMonkey = game.add.audio('winMonkey');
        winMonkey.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 7000);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('topScoreGame2'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = []; // массив для объектов звуков
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');
        soundWinLines[5] = game.add.audio('soundWinLine6');
        soundWinLines[6] = game.add.audio('soundWinLine7');
        soundWinLines[7] = game.add.audio('soundWinLine8');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {
                if(!isMobile) {
                    showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);
                } else {
                    showButtons([[startButton], [home], [gear], [dollar], [double]]);
                }

                hideNumberWinLine();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk1');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий
                if(isMobile) {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 370-mobileX, 345-mobileY);
                } else {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 370, 345);
                }
                soundWinLines[soundWinLinesCounter].play();
                soundWinLinesCounter += 1;
                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            hideLines([]);
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('play1To');
            updateBalance(game, scorePosions, balanceOld, balance);
            if(isMobile){
                showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            } else {
                showButtons();
            }
            checkAutoStart = false;
        }
    }

}


//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons([]);
    //alert('betline: '+betline+', lines: '+lines+', bet: '+bet+', sid: '+sid);
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
        dataType: 'html',
        success: function (data) {
            //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|0&extralife=45&jackpots=1822.16|4122.16|6122.16';
            //alert(data);
            dataSpinRequest = data;

            parseSpinAnswer(dataSpinRequest);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8,9], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8,9], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

function hideSelectionOfTheManyCellAnim(monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item].visible = false;
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('take');
    hideButtons([]);

    hideNumbersAmin();
    hideLines([]);

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
var tableTitle; // название изображения заданное в прелодере
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle.visible = true;
    tableTitle.loadTexture(loadTexture);
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
        font: '20px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}


// ajax-запросы

//init-запрос
function requestInit() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=init',
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            dataString = data;

            initDataArray = dataString.split('&');

            initDataArray.forEach(function (item) {
                if(item.indexOf('SID=') + 1) {
                    sid = item.replace('SID=','');
                }
                if(item.indexOf('user=') + 1) {
                    user = item.replace('user=','');
                }
            });

            if (data.length != 0 && (find(initDataArray, 'result=ok')) != -1 && (find(initDataArray, 'state=0')) != -1) {
                requestState();
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//state-запрос
var jackpots;
function requestState() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=state&min=1&game='+gamename+'&SID='+sid,
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            var dataString = data;

            startDataArray = dataString.split('&');

            if (data.length !== 0 && (find(startDataArray, ' result=ok')) != -1 && (find(startDataArray, 'state=0')) != -1) {

                startDataArray = dataString.split('&');

                startDataArray.forEach(function (item) {
                    if(item.indexOf('balance=') + 1) {
                        balance = item.replace('balance=', '').replace('.00','');
                    }
                    if(item.indexOf('extralife=') + 1) {
                        extralife = item.replace('extralife=','');
                    }
                    if(item.indexOf('jackpots=') + 1) {
                        var jackpotsString = item.replace('jackpots=','');
                        jackpots =  jackpotsString.split('|');
                    }
                    if(item.indexOf('id=') + 1) {
                        id = item.replace('id=','');
                    }
                    if(item.indexOf('min=') + 1) {
                        min = item.replace('min=','');
                    }

                    /*game1();
                     game2();
                     game3();
                     game4();*/
                });
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    if(checkGame == 1){
        betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
            font: scorePosions[4][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    if(checkGame == 2){
        riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }
}

var takeWin;
var textCounter;
var topBarImage;
var ActionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'startButton']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        ActionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время
                if(isMobile) {
                    showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
                } else {
                    showButtons();
                }

                takeWin.stop();
                changeTableTitle('play1To');
                clearInterval(textCounter);

                topBarImage.loadTexture('game.background1'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    betScore.visible = false;
    linesScore.visible = false;
    betline1Score.visible = false;
    betline2Score.visible = false;

    bet = lines*betline;
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
        font: scorePosions[4][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];
    } else {
        betlineCounter = 0;
        betline = betlineOptions[betlineCounter];
    }
}

function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard, lines, bet, sid) {
    hideButtons([[startButton, 'startButton']]);
    disableInputCards();
    lockDisplay();
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
        dataType: 'html',
        success: function (data) {

            dataDoubleRequest = data.split('&');
            parseDoubleAnswer(dataDoubleRequest);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

var dwin; var dcard2; var selectedCardR;
function parseDoubleAnswer(dataDoubleRequest) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResult(dwin, selectedCardR, valuesOfAllCards);

    }
}

var step = 1;
function showDoubleResult(dwin, selectedCardR, valuesOfAllCards) {
    if (!isMobile) {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout('hideAllCards(cardArray); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step)', 2000);
            setTimeout('openDCard(dcard); showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "startButton"]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout("tableTitle.visible = false; game.state.start('game1');", 2000);
        }
    } else {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); lockDisplay(); disableInputCards();', 500);
            setTimeout('hideAllCards(cardArray); openDCard(dcard); disableInputCards(); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step);', 2000);
            setTimeout('enableInputCards(); unlockDisplay(); showButtons([[startButton]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); disableInputCards(); lockDisplay();', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 3000);
        }
    }
}

var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
function showDoubleToAndTakeOrRiskTexts(game, totalWin, x,y) {
    xSave = x;
    ySave = y;

    var i = 1;
    timerTitleAmin = setInterval(function() {
        if(i == 0) {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            doubleToText = game.add.text(x, y, 'DOUBLE TO '+totalWin*2+' ?', {
                font: '22px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 1;
        } else {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            takeOrRiskText = game.add.text(x, y, 'TAKE OR RISK', {
                font: '22px "Press Start 2P"',
                fill: '#ffffff',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 0;
        }

    }, 500);
}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    takeOrRiskText.visible = false;
    clearInterval(timerTitleAmin);
}

function updateTotalWin(game, dwin, step){
    //обновление totalWin в игре с картами

    linesScore.visible = false;
    riskStep.visible = false;

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], dwin, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], step, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];

//var cardPosition = [[x,y], [x,y], [x,y], [x,y], [x,y]]  - нулевой элемент массива карта диллера
var openCard; //звуки
var winCard;
function addCards(game, cardPosition) {
    if(!isMobile) {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    } else {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        card2.inputEnabled = true;
        card2.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid);
        });
        card3.inputEnabled = true;
        card3.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        card4.inputEnabled = true;
        card4.events.onInputDown.add(function(){
            requestDouble(gamename, 3, lines, bet, sid);
        });
        card5.inputEnabled = true;
        card5.events.onInputDown.add(function(){
            requestDouble(gamename, 4, lines, bet, sid);
        });

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    }
}

function openDCard(dcard) {
    lockDisplay();
    disableInputCards();
    setTimeout("card1.loadTexture('card_'+dcard); openCard.play(); enableInputCards(); unlockDisplay();", 1000);
}

var selectedCardValue; //значение карты выбранной игроком
function openSelectedCard(selectedCardR, valuesOfAllCards) {
    openCard.play();
    cardArray[selectedCardR].loadTexture("card_"+valuesOfAllCards[selectedCardR]);
}

var valuesOfAllCards; // значения остальных карт [,,,,]
function openAllCards(valuesOfAllCards) {
    openCard.play();
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_'+valuesOfAllCards[i]);
    });
}

function hideAllCards(cardArray) {
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_bg');
        item.inputEnabled
    });
}

function disableInputCards() {
    card2.inputEnabled = false;
    card3.inputEnabled = false;
    card4.inputEnabled = false;
    card5.inputEnabled = false;
}

function enableInputCards() {
    card2.inputEnabled = true;
    card3.inputEnabled = true;
    card4.inputEnabled = true;
    card5.inputEnabled = true;
}


//функции для игры с последовательным выбором (веревки, ящики, бочки и т.д.)

// отображает результат выбора веревки (подлетает цифра и пересчитвается значение totalWin)
var stepTotalWinR = 0;
function showWinGame3(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y+100, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

    var timeInterval = 450;
    var textCounter = setInterval(function () {
        text.position.y -= 3;
    }, 10);

    setTimeout(function() {
        clearInterval(textCounter);
    }, timeInterval);

    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}





//функции для игры с выбором из двух вариантов








//функции для мобильной версии

var startButton; var double; var bet1; var dollar; var gear; var home;
function addButtonsGame1Mobile(game) {

    startButton = game.add.sprite(588, 228, 'startButton');
    startButton.bringToTop();
    startButton.anchor.setTo(0.5, 0.5);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            startButton.loadTexture('startButton_d');
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    double = game.add.sprite(549, 133, 'double');
    double.inputEnabled = true;
    double.input.useHandCursor = true;
    double.events.onInputDown.add(function(){
        checkWin = 0;
        hideNumbersAmin();
        game.state.start('game2');
    });
    double.events.onInputUp.add(function(){
        double.loadTexture('double');
    });
    double.visible = false;

    bet1 = game.add.sprite(546, 274, 'bet1');
    bet1.inputEnabled = true;
    bet1.input.useHandCursor = true;
    bet1.events.onInputDown.add(function(){
        /*lines = 9;
         betline = 25;*/

        bet1.loadTexture('bet1_p');
        document.getElementById('betMode').style.display = 'block';
        widthVisibleZone = $('.betWrapper .visibleZone').height();
        console.log(widthVisibleZone);
        $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
        $('canvas').css('display', 'none');
    });
    bet1.events.onInputUp.add(function(){
        bet1.loadTexture('bet1');
    });

    dollar = game.add.sprite(445, 30, 'dollar');
    dollar.scale.setTo(0.7,0.7);
    dollar.inputEnabled = true;
    dollar.input.useHandCursor = true;
    dollar.events.onInputDown.add(function(){
        //game.state.start('game4');
    });

    gear = game.add.sprite(519, 30, 'gear');
    gear.scale.setTo(0.7,0.7);
    gear.inputEnabled = true;
    gear.input.useHandCursor = true;
    gear.events.onInputDown.add(function(){
        //game.state.start('game3');
    });

    home = game.add.sprite(45, 30, 'home');
    home.scale.setTo(0.7,0.7);
    home.inputEnabled = true;
    home.input.useHandCursor = true;
    home.events.onInputDown.add(function(){
        home.loadTexture('home_d');
    });
    home.events.onInputUp.add(function(){
        home.loadTexture('home');
    });
}

function addButtonsGame2Mobile(game) {
    startButton = game.add.sprite(538, 300, 'collect');
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('collect');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });
    startButton.scale.setTo(0.7,0.7);
}

//выбор множителя в меню bet
var denomination = 1;
function selectDenomination(el) {
    denomination = el.innerText;

    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;

    var selectedElement = document.getElementsByClassName('denomSize selected');
    selectedElement[0].className = 'denomSize';

    el.className = 'denomSize selected';
}

//выставление максимального значения ставки на линию
function maxBetlineForBetMenu() {
    maxBetline();
    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    document.getElementsByClassName('checkCssTopBetLineRange')[0].style.top = '34.5%';
    document.querySelectorAll('.checkCssTopBetLineRange')[0].querySelectorAll('.selected')[0].classList.remove('selected');
    document.getElementById('cellBetLine25').classList.add('selected');
}


//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

(function () {

    var game1 = {};


    game1.preload = function () {

    };

    game1.create = function () {
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;


        // звуки

        var playSound = game.add.audio('play');

        // изображения

        game.add.sprite(0,0, 'game.background1');
        topBarImage = game.add.sprite(93-mobileX,23-mobileY, 'game.background1');

        addTableTitle(game, 'play1To',235-mobileX,343-mobileY);

        slotPosition = [[142-mobileX, 55-mobileY], [142-mobileX, 147-mobileY], [142-mobileX, 242-mobileY], [254-mobileX, 55-mobileY], [254-mobileX, 147-mobileY], [254-mobileX, 242-mobileY], [365-mobileX, 55-mobileY], [365-mobileX, 147-mobileY], [365-mobileX, 242-mobileY], [477-mobileX, 55-mobileY], [477-mobileX, 147-mobileY], [477-mobileX, 242-mobileY], [589-mobileX, 55-mobileY], [589-mobileX, 147-mobileY], [589-mobileX, 242-mobileY]];
        addSlots(game, slotPosition);

        var linePosition = [[134-mobileX,199-mobileY], [134-mobileX,71-mobileY], [134-mobileX,322-mobileY], [134-mobileX,130-mobileY], [134-mobileX,95-mobileY], [134-mobileX,102-mobileY], [134-mobileX,228-mobileY], [134-mobileX,226-mobileY], [134-mobileX,120-mobileY]];
        var numberPosition = [[109-mobileX,183-mobileY], [109-mobileX,54-mobileY], [109-mobileX,310-mobileY], [109-mobileX,118-mobileY], [109-mobileX,246-mobileY], [109-mobileX,86-mobileY], [109-mobileX,278-mobileY], [109-mobileX,214-mobileY], [109-mobileX,150-mobileY]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        showLines(linesArray);
        showNumbers(numberArray);

        // кнопки
        addButtonsGame1Mobile(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [130-mobileX, 348-mobileY, 16], [685-mobileX, 348-mobileY, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        var checkPosition = 1;
        function showRandMonkey(){
            var randBaba = randomNumber(1,10);

            monkey1 = game.add.sprite(193-mobileX,359-mobileY, 'game.monkey1');
            monkey1.animations.add('monkey1', [0,1,2,3,4,5,6,7,8,9], 10, false);
            monkey1.visible = false;

            monkey2 = game.add.sprite(193-mobileX,359-mobileY, 'game.monkey2');
            monkey2.animations.add('monkey2', [0,1,2,3,4,5,6,7], 10, false);
            monkey2.visible = false;

            monkey3 = game.add.sprite(193-mobileX,359-mobileY, 'game.monkey3');
            monkey3.animations.add('monkey3', [0,1,2,3,4,5,6,7,8,9,10,11], 10, false);
            monkey3.visible = false;

            monkey4 = game.add.sprite(193-mobileX,359-mobileY, 'game.monkey4');
            monkey4.animations.add('monkey4', [0,1,2,3,4,5], 10, false);
            monkey4.visible = false;

            monkey5 = game.add.sprite(193-mobileX,359-mobileY, 'game.monkey5');
            monkey5.animations.add('monkey5', [0,1,2,3,4,5], 10, false);
            monkey5.visible = false;

            monkeyWin = game.add.sprite(193-mobileX,359-mobileY, 'game.monkeyWin');
            monkeyWin.animations.add('monkeyWin', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, false);
            monkeyWin.visible = false;

            switch(randBaba) {
                case 1:
                    monkey1.visible = true;
                    monkey1.animations.getAnimation('monkey1').play().onComplete.add(function(){
                        monkey1.animations.stop();
                        monkey1.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 2:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 3:
                    monkey3.visible = true;
                    monkey3.animations.getAnimation('monkey3').play().onComplete.add(function(){
                        monkey3.animations.stop();
                        monkey3.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 4:
                    monkey4.visible = true;
                    monkey4.animations.getAnimation('monkey4').play().onComplete.add(function(){
                        monkey4.animations.stop();
                        monkey4.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 5:
                    monkey5.visible = true;
                    monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
                        monkey5.animations.stop();
                        monkey5.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 6:
                    monkey5.visible = true;
                    monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
                        monkey5.animations.stop();
                        monkey5.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 7:
                    monkey5.visible = true;
                    monkey5.animations.getAnimation('monkey5').play().onComplete.add(function(){
                        monkey5.animations.stop();
                        monkey5.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 8:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 9:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                case 10:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
                default:
                    monkey2.visible = true;
                    monkey2.animations.getAnimation('monkey2').play().onComplete.add(function(){
                        monkey2.animations.stop();
                        monkey2.visible = false;
                        showRandMonkey();
                    });
                    break;
            }
        }

        var checkPositionButterfly = 1;
        function showRandButterfly(){
            var randBaba = randomNumber(1,3);

            butterfly1 = game.add.sprite(493-mobileX,407-mobileY, 'game.butterfly1');
            butterfly1.animations.add('butterfly1', [0,1,2,3,4,5,6,7,8], 10, false);
            butterfly1.visible = false;

            butterfly2 = game.add.sprite(493-mobileX,407-mobileY, 'game.butterfly2');
            butterfly2.animations.add('butterfly2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
            butterfly2.visible = false;

            butterfly3x = game.add.sprite(493-mobileX,407-mobileY, 'game.butterfly3x');
            butterfly3x.animations.add('butterfly3x', [0,1,2,3,4,5], 10, false);
            butterfly3x.visible = false;

            butterflyFlies11 = game.add.sprite(493-mobileX,407-mobileY, 'game.butterflyFlies1');
            butterflyFlies11.animations.add('butterflyFlies11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
            butterflyFlies11.visible = false;

            butterflyFlies12 = game.add.sprite(493-mobileX,407-mobileY, 'game.butterflyFlies2');
            butterflyFlies12.animations.add('butterflyFlies12', [1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
            butterflyFlies12.visible = false;

            butterflyFlies21 = game.add.sprite(493-mobileX,407-mobileY, 'game.butterflyFlies1');
            butterflyFlies21.animations.add('butterflyFlies21', [15,14,13,12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies21.visible = false;

            butterflyFlies22 = game.add.sprite(493-mobileX,407-mobileY, 'game.butterflyFlies2');
            butterflyFlies22.animations.add('butterflyFlies22', [12,11,10,9,8,7,6,5,4,3,2,1], 10, false);
            butterflyFlies22.visible = false;

            if(checkPosition == 1) {
                switch(randBaba) {
                    case 1:
                        butterfly1.visible = true;
                        butterfly1.animations.getAnimation('butterfly1').play().onComplete.add(function(){
                            butterfly1.animations.stop();
                            butterfly1.visible = false;
                            showRandButterfly();
                        });
                        break;
                    case 2:
                        butterfly2.visible = true;
                        butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
                            butterfly2.animations.stop();
                            butterfly2.visible = false;
                            showRandButterfly();
                        });
                        break;

                    case 3:
                        butterflyFlies11.visible = true;
                        butterflyFlies11.animations.getAnimation('butterflyFlies11').play().onComplete.add(function(){
                            butterflyFlies11.animations.stop();
                            butterflyFlies11.visible = false;

                            butterflyFlies12.visible = true;
                            butterflyFlies12.animations.getAnimation('butterflyFlies12').play().onComplete.add(function(){
                                butterflyFlies12.animations.stop();
                                butterflyFlies12.visible = false;

                                checkPosition = 2;
                                showRandButterfly();
                            });
                        });
                        break;

                    default:
                        butterfly2.visible = true;
                        butterfly2.animations.getAnimation('butterfly2').play().onComplete.add(function(){
                            butterfly2.animations.stop();
                            butterfly2.visible = false;
                            showRandButterfly();
                        });
                        break;
                }
            } else {
                switch(randBaba) {
                    case 1:
                        butterfly3x.visible = true;
                        butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
                            butterfly3x.animations.stop();
                            butterfly3x.visible = false;
                            showRandButterfly();
                        });
                        break;

                    case 2:
                        butterflyFlies22.visible = true;
                        butterflyFlies22.animations.getAnimation('butterflyFlies22').play().onComplete.add(function(){
                            butterflyFlies22.animations.stop();
                            butterflyFlies22.visible = false;

                            butterflyFlies21.visible = true;
                            butterflyFlies21.animations.getAnimation('butterflyFlies21').play().onComplete.add(function(){
                                butterflyFlies21.animations.stop();
                                butterflyFlies21.visible = false;
                                checkPosition = 1;
                                showRandButterfly();
                            });
                        });
                        break;

                    case 3:
                        butterfly3x.visible = true;
                        butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
                            butterfly3x.animations.stop();
                            butterfly3x.visible = false;
                            showRandButterfly();
                        });
                        break;

                    default:
                        butterfly3x.visible = true;
                        butterfly3x.animations.getAnimation('butterfly3x').play().onComplete.add(function(){
                            butterfly3x.animations.stop();
                            butterfly3x.visible = false;
                            showRandButterfly();
                        });
                        break;
                }
            }


        }

        showRandButterfly();
        showRandMonkey();
    };

    game1.update = function () {
        if(bet >= extralife && checkHelm == false) {
            mushroomGrow = game.add.sprite(353-mobileX,423-mobileY, 'game.mushroomGrow');
            mushroomGrow.animations.add('mushroomGrow', [0,1,2,3,4,5], 10, false);
            mushroomGrow.animations.getAnimation('mushroomGrow').play().onComplete.add(function(){
                mushroomJump = game.add.sprite(353-mobileX,423-mobileY, 'game.mushroomJump');
                mushroomJump.animations.add('mushroomJump', [0,1,2,3,4,5,6,7,8,9], 10, true);
                mushroomJump.animations.getAnimation('mushroomJump').play();
            });

            checkHelm = true;
        }

        if(bet < extralife && checkHelm == true) {
            checkHelm = false;

            mushroomGrow.visible = false;
            mushroomJump.visible = false;

            mushroomGrow = game.add.sprite(353-mobileX,423-mobileY, 'game.mushroomGrow');
            mushroomGrow.animations.add('mushroomGrow', [5,4,3,2,1,0], 10, false);
            mushroomGrow.animations.getAnimation('mushroomGrow').play();
        }

        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
            if(checkHelm == true) {
                function hideMonkey(){
                    monkey1.visible = false;
                    monkey2.visible = false;
                    monkey3.visible = false;
                    monkey4.visible = false;
                    monkey5.visible = false;
                }

                hideMonkey();

                monkeyTakeMushroom1 = game.add.sprite(195-mobileX,359-mobileY, 'game.monkeyTakeMushroom1');
                monkeyTakeMushroom1.animations.add('monkeyTakeMushroom1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 10, false);
                monkeyTakeMushroom1.visible = false;

                monkeyTakeMushroom2 = game.add.sprite(195-mobileX,359-mobileY, 'game.monkeyTakeMushroom2');
                monkeyTakeMushroom2.animations.add('monkeyTakeMushroom2', [1,2,3,4], 10, false);
                monkeyTakeMushroom2.visible = false;

                function monkeyTakeMushroomAnim() {
                    monkeyTakeMushroom1.visible = true;
                    monkeyTakeMushroom1.animations.getAnimation('monkeyTakeMushroom1').play().onComplete.add(function(){
                        monkeyTakeMushroom1.visible = false;

                        monkeyTakeMushroom2.visible = true;
                        monkeyTakeMushroom2.animations.getAnimation('monkeyTakeMushroom2').play();
                    });
                }

                monkeyTakeMushroomAnim();
            }
        }
    };

    game.state.add('game1', game1);


})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================


(function () {

    var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {

    };

    game2.update = function () {};

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================

(function () {

    var button;

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {

    };

    game3.update = function () {};

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================

(function () {

    var button;

    var game4 = {};

    game4.preload = function () {

    };

    game4.create = function () {
    };

    game4.update = function () {
    };

    game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('game.background', 'img/canvas-bg.svg');
        game.load.image('game.background1', 'img/shape319.png');

        game.load.image('startButton', 'img/mobileButtons/spin.png');
        game.load.image('startButton_p', 'img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', 'img/mobileButtons/spin_d.png');
        game.load.image('bet1', 'img/mobileButtons/bet1.png');
        game.load.image('bet1_p', 'img/mobileButtons/bet1_p.png');
        game.load.image('home', 'img/mobileButtons/home.png');
        game.load.image('home_p', 'img/mobileButtons/home_p.png');
        game.load.image('dollar', 'img/mobileButtons/dollar.png');
        game.load.image('gear', 'img/mobileButtons/gear.png');
        game.load.image('double', 'img/mobileButtons/double.png');
        game.load.image('collect', 'img/mobileButtons/collect.png');
        game.load.image('collect_p', 'img/mobileButtons/collect_p.png');
        game.load.image('collect_d', 'img/mobileButtons/collect_d.png');

        game.load.image('game.number1', 'img/1.png');
        game.load.image('game.number2', 'img/2.png');
        game.load.image('game.number3', 'img/3.png');
        game.load.image('game.number4', 'img/4.png');
        game.load.image('game.number5', 'img/5.png');
        game.load.image('game.number6', 'img/6.png');
        game.load.image('game.number7', 'img/7.png');
        game.load.image('game.number8', 'img/8.png');
        game.load.image('game.number9', 'img/9.png');

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.audio('sound', 'sound/spin.mp3');
        game.load.audio('rotate', 'sound/rotate.wav');
        game.load.audio('stop', 'sound/stop.wav');
        game.load.audio('tada', 'sound/tada.wav');
        game.load.audio('play', 'sound/play.mp3');

        game.load.image('line1', 'lines/select/1.png');
        game.load.image('line2', 'lines/select/2.png');
        game.load.image('line3', 'lines/select/3.png');
        game.load.image('line4', 'lines/select/4.png');
        game.load.image('line5', 'lines/select/5.png');
        game.load.image('line6', 'lines/select/6.png');
        game.load.image('line7', 'lines/select/7.png');
        game.load.image('line8', 'lines/select/8.png');
        game.load.image('line9', 'lines/select/9.png');

        game.load.image('linefull1', 'lines/win/1.png');
        game.load.image('linefull2', 'lines/win/2.png');
        game.load.image('linefull3', 'lines/win/3.png');
        game.load.image('linefull4', 'lines/win/4.png');
        game.load.image('linefull5', 'lines/win/5.png');
        game.load.image('linefull6', 'lines/win/6.png');
        game.load.image('linefull7', 'lines/win/7.png');
        game.load.image('linefull8', 'lines/win/8.png');
        game.load.image('linefull9', 'lines/win/9.png');

        game.load.audio('line1Sound', 'lines/sounds/line1.wav');
        game.load.audio('line3Sound', 'lines/sounds/line3.wav');
        game.load.audio('line5Sound', 'lines/sounds/line5.wav');
        game.load.audio('line7Sound', 'lines/sounds/line7.wav');
        game.load.audio('line9Sound', 'lines/sounds/line9.wav');

        game.load.audio('sound', 'sound/spin.mp3');
        game.load.audio('rotateSound', 'sound/rotate.wav');
        game.load.audio('stopSound', 'sound/stop.wav');
        game.load.audio('tada', 'sound/tada.wav');
        game.load.audio('play', 'sound/play.mp3');
        game.load.audio('takeWin', 'sound/takeWin.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');

        game.load.audio('soundWinLine8', 'sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', 'sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', 'sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', 'sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', 'sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', 'sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', 'sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', 'sound/winLines/sound19.mp3');

        game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 608, 32);
        game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 608, 32);
        game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 608, 32);
        game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 608, 32);
        game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 608, 32);
        game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 608, 32);
        game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 608, 32);
        game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 608, 32);
        game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.mask', 'img/mask.png', 96, 96);
        game.load.spritesheet('game.monkey', 'img/monkey.png', 96, 96);
        game.load.audio('winMonkey', 'sound/winMonkey.mp3');


        game.load.image('game.backgroundGame3', 'img/shape164.svg');
        game.load.audio('openCard', 'sound/sound31.mp3');
        game.load.audio('winCard', 'sound/sound30.mp3');

        //карты
        game.load.image('card_bg', 'img/shape167.png');

        game.load.image('card_39', 'img/shape169.png');
        game.load.image('card_40', 'img/shape171.png');
        game.load.image('card_41', 'img/shape173.png');
        game.load.image('card_42', 'img/shape175.png');
        game.load.image('card_43', 'img/shape177.png');
        game.load.image('card_44', 'img/shape179.png');
        game.load.image('card_45', 'img/shape181.png');
        game.load.image('card_46', 'img/shape183.png');
        game.load.image('card_47', 'img/shape185.png');
        game.load.image('card_48', 'img/shape187.png');
        game.load.image('card_49', 'img/shape189.png');
        game.load.image('card_50', 'img/shape191.png');
        game.load.image('card_51', 'img/shape193.png');

        game.load.image('card_26', 'img/shape195.png');
        game.load.image('card_27', 'img/shape197.png');
        game.load.image('card_28', 'img/shape199.png');
        game.load.image('card_29', 'img/shape201.png');
        game.load.image('card_30', 'img/shape203.png');
        game.load.image('card_31', 'img/shape205.png');
        game.load.image('card_32', 'img/shape207.png');
        game.load.image('card_33', 'img/shape209.png');
        game.load.image('card_34', 'img/shape211.png');
        game.load.image('card_35', 'img/shape213.png');
        game.load.image('card_36', 'img/shape215.png');
        game.load.image('card_37', 'img/shape217.png');
        game.load.image('card_38', 'img/shape219.png');

        game.load.image('card_0', 'img/shape221.png');
        game.load.image('card_1', 'img/shape223.png');
        game.load.image('card_2', 'img/shape225.png');
        game.load.image('card_3', 'img/shape227.png');
        game.load.image('card_4', 'img/shape229.png');
        game.load.image('card_5', 'img/shape231.png');
        game.load.image('card_6', 'img/shape233.png');
        game.load.image('card_7', 'img/shape235.png');
        game.load.image('card_8', 'img/shape237.png');
        game.load.image('card_9', 'img/shape239.png');
        game.load.image('card_10', 'img/shape241.png');
        game.load.image('card_11', 'img/shape243.png');
        game.load.image('card_12', 'img/shape245.png');

        game.load.image('card_13', 'img/shape247.png');
        game.load.image('card_14', 'img/shape249.png');
        game.load.image('card_15', 'img/shape251.png');
        game.load.image('card_16', 'img/shape253.png');
        game.load.image('card_17', 'img/shape255.png');
        game.load.image('card_18', 'img/shape257.png');
        game.load.image('card_19', 'img/shape259.png');
        game.load.image('card_20', 'img/shape261.png');
        game.load.image('card_21', 'img/shape263.png');
        game.load.image('card_22', 'img/shape265.png');
        game.load.image('card_23', 'img/shape267.png');
        game.load.image('card_24', 'img/shape269.png');
        game.load.image('card_25', 'img/shape271.png');
        game.load.image('card_52', 'img/shape273.png');

        game.load.image('pick', 'img/shape281.png');

        game.load.spritesheet('game.monkey1', 'img/monkey1.png', 160, 144);
        game.load.spritesheet('game.monkey2', 'img/monkey2.png', 160, 144);
        game.load.spritesheet('game.monkey3', 'img/monkey3.png', 160, 144);
        game.load.spritesheet('game.monkey4', 'img/monkey4.png', 160, 144);
        game.load.spritesheet('game.monkey5', 'img/monkey5.png', 160, 144);
        game.load.spritesheet('game.monkeyWin', 'img/monkeyWin.png', 160, 144);
        game.load.spritesheet('game.monkeyTakeMushroom1', 'img/monkeyTakeMushroom1.png', 224, 144);
        game.load.spritesheet('game.monkeyTakeMushroom2', 'img/monkeyTakeMushroom2.png', 224, 144);
        game.load.spritesheet('game.butterfly1', 'img/butterfly1.png', 240, 96);
        game.load.spritesheet('game.butterfly2', 'img/butterfly2.png', 240, 96);
        game.load.spritesheet('game.butterfly3x', 'img/butterfly3x.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies1', 'img/butterflyFlies1.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies2', 'img/butterflyFlies2.png', 240, 96);
        game.load.spritesheet('game.mushroomGrow', 'img/mushroomGrow.png', 64, 80);
        game.load.spritesheet('game.mushroomJump', 'img/mushroomJump.png', 64, 80);

        game.load.image('game.backgroundGame2', 'img/shape1150.png');
        game.load.spritesheet('game.rope', 'img/rope.png', 16, 320);
        game.load.spritesheet('game.ropeButton', 'img/ropeButton.png', 64, 320);


        game.load.spritesheet('game.monkey21', 'img/monkey21.png', 176, 480);
        game.load.spritesheet('game.monkey22', 'img/monkey22.png', 176, 480);
        game.load.spritesheet('game.monkey23', 'img/monkey23.png', 176, 480);
        game.load.spritesheet('game.monkey24', 'img/monkey24.png', 176, 480);

        game.load.spritesheet('game.monkey21H', 'img/monkey21H.png', 176, 480);
        game.load.spritesheet('game.monkey22H', 'img/monkey22H.png', 176, 480);
        game.load.spritesheet('game.monkey23H', 'img/monkey23H.png', 176, 480);
        game.load.spritesheet('game.monkey24H', 'img/monkey24H.png', 176, 480);

        game.load.spritesheet('game.monkeyTakeRope1', 'img/monkeyTakeRope1.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope2', 'img/monkeyTakeRope2.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope1H', 'img/monkeyTakeRope1H.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope2H', 'img/monkeyTakeRope2H.png', 176, 480);
        game.load.spritesheet('game.monkeyEatBanana', 'img/monkeyEatBanana.png', 176, 480);
        game.load.spritesheet('game.monkeyEatBananaH', 'img/monkeyEatBananaH.png', 176, 480);
        game.load.spritesheet('game.monkeyEatHammer1', 'img/monkeyEatHammer1.png', 160, 480);
        game.load.spritesheet('game.monkeyEatHammer12', 'img/monkeyEatHammer12.png', 176, 480);
        game.load.spritesheet('game.monkeyEatHammer2', 'img/monkeyEatHammer2.png', 160, 480);
        game.load.spritesheet('game.monkeyEatHammer22', 'img/monkeyEatHammer22.png', 176, 480);
        game.load.spritesheet('game.monkeyHeadache', 'img/monkeyHeadache.png', 176, 480);

        game.load.audio('game.winRope1', 'sound/winRope1.mp3');
        game.load.audio('game.winRope2', 'sound/winRope2.mp3');
        game.load.audio('game.bitMonkey', 'sound/bitMonkey.mp3');

        game.load.image('game.backgroundGame4', 'img/shape943.svg');

        game.load.spritesheet('game.mankeyHanging1', 'img/mankeyHanging1.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging2', 'img/mankeyHanging2.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging3', 'img/mankeyHanging3.png', 240, 304);
        game.load.spritesheet('game.mankeyLeftPoint', 'img/mankeyLeftPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyLosePoint', 'img/mankeyLosePoint.png', 240, 304);
        game.load.spritesheet('game.mankeyRightPoint', 'img/mankeyRightPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyWinPoint', 'img/mankeyWinPoint.png', 240, 304);

        game.load.spritesheet('game.goldPoint', 'img/goldPoint.png', 96, 128);
        game.load.spritesheet('game.questionPoint', 'img/questionPoint.png', 96, 80);
        game.load.spritesheet('game.spiderPoint', 'img/spiderPoint.png', 96, 128);
        game.load.spritesheet('game.flashFive', 'img/flashFive.png', 32, 48);
        game.load.spritesheet('game.take_or_risk_anim', 'img/take_or_risk_anim.png', 192, 160);

        game.load.image('game.take_or_risk_bg', 'img/shape953.svg');
        game.load.audio('game.winPoint1', 'sound/winPoint1.mp3');
        game.load.audio('game.winPoint2', 'sound/winPoint2.mp3');
        game.load.audio('game.losePoint', 'sound/losePoint.mp3');

        game.load.image('game.monkeyR', 'img/monkeyR.png');
        game.load.image('game.butterflyR', 'img/butterflyR.png');

        game.load.image('cell0', 'img/cell0.jpg');
        game.load.image('cell1', 'img/cell1.jpg');
        game.load.image('cell2', 'img/cell2.jpg');
        game.load.image('cell3', 'img/cell3.jpg');
        game.load.image('cell4', 'img/cell4.jpg');
        game.load.image('cell5', 'img/cell5.jpg');
        game.load.image('cell6', 'img/cell6.jpg');
        game.load.image('cell7', 'img/cell7.jpg');
        game.load.image('cell8', 'img/cell8.jpg');

        game.load.spritesheet('cellAnim', 'img/cellAnim.jpg', 96, 96);

        game.load.image('bonusGame', 'img/image536.jpg');
        game.load.image('wildSymbol', 'img/image537.jpg');
        game.load.image('play1To', 'img/image546.png');
        game.load.image('takeOrRisk1', 'img/image474.jpg');
        game.load.image('takeOrRisk2', 'img/image475.jpg');
        game.load.image('take', 'img/image554.jpg');

        game.load.image('topScoreGame2', 'img/image457.png');

        game.load.image('loseTitleGame2', 'img/image484.png');
        game.load.image('winTitleGame2', 'img/image486.png');
        game.load.image('forwadTitleGame2', 'img/image504.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/monkeyBoom.png', 96, 96);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

