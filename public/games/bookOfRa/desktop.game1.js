var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;

        // звуки
        // заполняем массив звуками выигрышей
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');

        soundWinLineDurations[0] = 1000;
        soundWinLineDurations[1] = 1000;
        soundWinLineDurations[2] = 6000;


        // изображения
        game.add.sprite(0,0, 'border');

        //слоты
        totalWinPosion = [490,468];
        slotPosition = [[142+10-16, 55+57-13],
            [142+10-16, 147+57+5-13],
            [142+10-16, 242+57+11-13],
            [254+10+2-16, 55+57-13],
            [254+10+2-16, 147+57+5-13],
            [254+10+2-16, 242+57+11-13],
            [365+10+4-16, 55+57-13],
            [365+10+4-16, 147+57+5-13],
            [365+10+4-16, 242+57+11-13],
            [477+10+7-16, 55+57-13],
            [477+10+7-16, 147+57+5-13],
            [477+10+7-16, 242+57+11-13],
            [589+10+9-16, 55+57-13],
            [589+10+9-16, 147+57+5-13],
            [589+10+9-16, 242+57+11-13]];
        addSlots(game, slotPosition, 8);

        // линии и номера
        numberPosition = [[94,248-13], [94,146-14], [94,352-13], [94,108-12], [94,372], [94,202], [94,280-12], [94,180-13], [94,180-13]];
        linePosition = [[99+21,248+12-13], [99+21,146+12-13], [99+21,350], [99+21,107], [99+21,141], [99+21,214], [99+21,124], [99+21,179], [99+21,179]];
        addLinesAndNumbers(game, linePosition, numberPosition);
        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);
        //addLinesTypeBOR(game, linePosition);

        //позиции ячеек относящиеся к линиям
        cellPositionOnLines = [[1,4,7,10,13], [0,3,6,9,12], [2,5,8,11,14], [0,4,8,10,12], [2,4,6,10,14]];
        slotAnimPosition = slotPosition; //позиции анимированных ячеек

        //создаем массив содержащий все анимации
        numberOfSlotValues = 10;
        addAllCellAnim(game, slotPosition, numberOfSlotValues);

        //цветные рамки для ячеек
        addSquares(game, slotPosition, 10);

        // кнопки
        addButtonsGame1TypeBOR(game);



        //счет
        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[690, 475, 22], [690, 440, 22], [200, 473, 22], [690, 420, 22], [690, 420, 22]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        addTitles();

        // анимация

        full_and_sound();

    };

    game1.update = function () {

    };

    game.state.add('game1', game1);

};
