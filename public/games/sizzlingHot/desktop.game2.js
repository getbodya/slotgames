function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        // звуки
        sound22 = game.add.audio('sound22');
        sound22.loop = true;
        sound22.play();


        //изображения
        game.add.sprite(0,0, 'border');
        var backgroundGame2 = game.add.sprite(99,97, 'backgroundGame2');
        var slotHeader = game.add.sprite(99,23, 'slotHeader');
        var slotFooter = game.add.sprite(99,431, 'slotFooter');
        slotHeader.scale.setTo(1.045, 1.045);
        slotFooter.scale.setTo(1.045, 1.045);
        backgroundGame2.scale.setTo(1.045, 1.045);

        // кнопки
        addButtonsGame2TypeBOR(game);

        //счет
        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[690+10, 475+25, 22], [690+10, 440+27, 22], [200+10, 473+25, 22], [690+10, 420+27, 22], [690+10, 420+27, 22]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        var gamblePosition = [[250,140],[580,140]];
        totalWinR = 25;
        addGamble(totalWinR, gamblePosition);

        addTitles();
        var text = 'CHOOSE RED OR BLACK OR TAKE WIN';
        changeTableTitleTypeBOR(text);


        buttonMPosition = [[165,260], [553,260]];
        addMButtons(buttonMPosition);

        //карты и масти
        cardPositionTypeBOR = [[382,234], [[415,179], [470,179], [526,179], [581,179], [638,179], [693,179]]];
        addCardsTypeBOR(cardPositionTypeBOR);

        addMs();

        //анимации
        addCardAnim(game);

        full_and_sound();
    };

    game2.update = function () {

    };

    game.state.add('game2', game2);


}