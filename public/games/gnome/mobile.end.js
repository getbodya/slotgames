(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('game.background', 'img/shape1206.svg');
        game.load.image('game.background1', 'img/main_bg.png');

        game.load.image('startButton', 'img/mobileButtons/spin.png');
        game.load.image('startButton_p', 'img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', 'img/mobileButtons/spin_d.png');
        game.load.image('bet1', 'img/mobileButtons/bet1.png');
        game.load.image('bet1_p', 'img/mobileButtons/bet1_p.png');
        game.load.image('home', 'img/mobileButtons/home.png');
        game.load.image('home_p', 'img/mobileButtons/home_p.png');
        game.load.image('dollar', 'img/mobileButtons/dollar.png');
        game.load.image('gear', 'img/mobileButtons/gear.png');
        game.load.image('double', 'img/mobileButtons/double.png');
        game.load.image('collect', 'img/mobileButtons/collect.png');
        game.load.image('collect_p', 'img/mobileButtons/collect_p.png');
        game.load.image('collect_d', 'img/mobileButtons/collect_d.png');

        game.load.image('game.number1', 'img/win_1.png');
        game.load.image('game.number2', 'img/win_2.png');
        game.load.image('game.number3', 'img/win_3.png');
        game.load.image('game.number4', 'img/win_4.png');
        game.load.image('game.number5', 'img/win_5.png');
        game.load.image('game.number6', 'img/win_6.png');
        game.load.image('game.number7', 'img/win_7.png');
        game.load.image('game.number8', 'img/win_8.png');
        game.load.image('game.number9', 'img/win_9.png');

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.audio('sound', 'sounds/spin.mp3');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('play', 'sounds/play.mp3');

        game.load.image('line1', 'img/lines/select/line1.png');
        game.load.image('line2', 'img/lines/select/line2.png');
        game.load.image('line3', 'img/lines/select/line3.png');
        game.load.image('line4', 'img/lines/select/line4.png');
        game.load.image('line5', 'img/lines/select/line5.png');
        game.load.image('line6', 'img/lines/select/line6.png');
        game.load.image('line7', 'img/lines/select/line7.png');
        game.load.image('line8', 'img/lines/select/line8.png');
        game.load.image('line9', 'img/lines/select/line9.png');

        game.load.image('linefull1', 'img/lines/win/linefull1.png');
        game.load.image('linefull2', 'img/lines/win/linefull2.png');
        game.load.image('linefull3', 'img/lines/win/linefull3.png');
        game.load.image('linefull4', 'img/lines/win/linefull4.png');
        game.load.image('linefull5', 'img/lines/win/linefull5.png');
        game.load.image('linefull6', 'img/lines/win/linefull6.png');
        game.load.image('linefull7', 'img/lines/win/linefull7.png');
        game.load.image('linefull8', 'img/lines/win/linefull8.png');
        game.load.image('linefull9', 'img/lines/win/linefull9.png');

        game.load.audio('line1Sound', 'sounds/line1.wav');
        game.load.audio('line3Sound', 'sounds/line3.wav');
        game.load.audio('line5Sound', 'sounds/line5.wav');
        game.load.audio('line7Sound', 'sounds/line7.wav');
        game.load.audio('line9Sound', 'sounds/line9.wav');

        game.load.audio('sound', 'sounds/spin.mp3');
        game.load.audio('rotateSound', 'sounds/rotate.wav');
        game.load.audio('stopSound', 'sounds/stop.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('play', 'sounds/play.mp3');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        // game.load.audio('game.winCards', 'sounds/sound30.mp3');

        game.load.audio('soundWinLine8', 'sounds/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', 'sounds/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', 'sounds/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', 'sounds/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', 'sounds/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', 'sounds/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', 'sounds/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', 'sounds/winLines/sound19.mp3');

        // game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 608, 32);
        // game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 608, 32);
        // game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 608, 32);
        // game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 608, 32);
        // game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 608, 32);
        // game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 608, 32);
        // game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 608, 32);
        // game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 608, 32);
        // game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.gold_row_win', 'img/gold_row_win.png', 96, 96);
        // game.load.spritesheet('game.monkey', 'img/monkey.png', 96, 96);
        game.load.audio('gnomeWin', 'sounds/gnomeWin.mp3');


        game.load.image('game.backgroundGame2', 'img/bg_game2.png');
        game.load.audio('openCard', 'sounds/sound31.mp3');
        game.load.audio('winCard', 'sounds/sound30.mp3');

        //карты
        game.load.image('card_bg', 'img/shirt_cards.png');

        game.load.image('card_39', 'img/cards/2b.png');
        game.load.image('card_40', 'img/cards/3b.png');
        game.load.image('card_41', 'img/cards/4b.png');
        game.load.image('card_42', 'img/cards/5b.png');
        game.load.image('card_43', 'img/cards/6b.png');
        game.load.image('card_44', 'img/cards/7b.png');
        game.load.image('card_45', 'img/cards/8b.png');
        game.load.image('card_46', 'img/cards/9b.png');
        game.load.image('card_47', 'img/cards/10b.png');
        game.load.image('card_48', 'img/cards/11b.png');
        game.load.image('card_49', 'img/cards/12b.png');
        game.load.image('card_50', 'img/cards/13b.png');
        game.load.image('card_51', 'img/cards/14b.png');

        game.load.image('card_26', 'img/cards/2c.png');
        game.load.image('card_27', 'img/cards/3c.png');
        game.load.image('card_28', 'img/cards/4c.png');
        game.load.image('card_29', 'img/cards/5c.png');
        game.load.image('card_30', 'img/cards/6c.png');
        game.load.image('card_31', 'img/cards/7c.png');
        game.load.image('card_32', 'img/cards/8c.png');
        game.load.image('card_33', 'img/cards/9c.png');
        game.load.image('card_34', 'img/cards/10c.png');
        game.load.image('card_35', 'img/cards/11c.png');
        game.load.image('card_36', 'img/cards/12c.png');
        game.load.image('card_37', 'img/cards/13c.png');
        game.load.image('card_38', 'img/cards/14c.png');

        game.load.image('card_0', 'img/cards/2t.png');
        game.load.image('card_1', 'img/cards/3t.png');
        game.load.image('card_2', 'img/cards/4t.png');
        game.load.image('card_3', 'img/cards/5t.png');
        game.load.image('card_4', 'img/cards/6t.png');
        game.load.image('card_5', 'img/cards/7t.png');
        game.load.image('card_6', 'img/cards/8t.png');
        game.load.image('card_7', 'img/cards/9t.png');
        game.load.image('card_8', 'img/cards/10t.png');
        game.load.image('card_9', 'img/cards/11t.png');
        game.load.image('card_10', 'img/cards/12t.png');
        game.load.image('card_11', 'img/cards/13t.png');
        game.load.image('card_12', 'img/cards/14t.png');

        game.load.image('card_13', 'img/cards/2p.png');
        game.load.image('card_14', 'img/cards/3p.png');
        game.load.image('card_15', 'img/cards/4p.png');
        game.load.image('card_16', 'img/cards/5p.png');
        game.load.image('card_17', 'img/cards/6p.png');
        game.load.image('card_18', 'img/cards/7p.png');
        game.load.image('card_19', 'img/cards/8p.png');
        game.load.image('card_20', 'img/cards/9p.png');
        game.load.image('card_21', 'img/cards/10p.png');
        game.load.image('card_22', 'img/cards/11p.png');
        game.load.image('card_23', 'img/cards/12p.png');
        game.load.image('card_24', 'img/cards/13p.png');
        game.load.image('card_25', 'img/cards/14p.png');
        game.load.image('card_52', 'img/cards/joker.png');

        game.load.image('pick', 'img/pick.png');

        game.load.spritesheet('fireplace', 'img/fireplace.png', 128, 48, 4);
        game.load.spritesheet('gear2', 'img/gear2.png', 96, 32, 16);
        game.load.spritesheet('gnome_anim1_1', 'img/gnome_anim1_1.png', 240, 176, 17);
        game.load.spritesheet('gnome_anim1_2', 'img/gnome_anim1_2.png', 240, 176, 17);
        game.load.spritesheet('gold_row_win', 'img/gold_row_win.png', 96, 96, 3);

        for (var i = 1; i <= 9; ++i) {
            if (i % 2 != 0) {           
                game.load.image('game.lever' + i, 'img/lever' + i + '.png');            
                game.load.image('game.lever_p' + i, 'img/lever_p' + i + '.png');            
            }
        }
        game.load.image('game.backgroundGame3', 'img/bg_game3.png');
        game.load.image('game.chest_open', 'img/game3_chest_open.png');
        game.load.spritesheet('game.action1', 'img/game3_action1.png', 192, 176, 12);
        game.load.spritesheet('game.gnome_left', 'img/game3_gnome_left.png', 160, 176, 23);
        game.load.spritesheet('game.gnome_right', 'img/game3_gnome_right.png', 192, 192, 18);
        game.load.spritesheet('game.chest_action1', 'img/game3_chest_action1.png', 128, 112, 9);
        game.load.spritesheet('game.chest_win', 'img/game3_chest_win.png', 128, 240, 22);
        game.load.spritesheet('game.gnome_win', 'img/game3_gnome_win.png', 160, 237, 11);
        game.load.spritesheet('game.gnome_win2', 'img/game3_gnome_win2.png', 160, 176, 3);
        game.load.spritesheet('game.chest_lose', 'img/game3_chest_lose.png', 128, 112, 23);
        game.load.spritesheet('game.gnome_lose', 'img/game3_gnome_lose.png', 192, 336, 10);
        game.load.spritesheet('game.gnome_lose2', 'img/game3_gnome_lose2.png', 192, 192, 10);

        game.load.audio('winGame3', 'sounds/winGame3.mp3');
        game.load.audio('game3_1', 'sounds/game3_1.mp3');
        game.load.audio('game3_2', 'sounds/game3_2.mp3');
        game.load.audio('game3_lose', 'sounds/game3_lose.mp3');
        game.load.audio('game3_win_2', 'sounds/game3_win_2.mp3');


        game.load.image('game.backgroundGame4', 'img/bg_game4.png');
        game.load.image('game.chest_open3', 'img/game4_chest_open.png');
        game.load.image('game.chest_open2', 'img/game4_chest_open2.png');
        game.load.image('game.gold', 'img/game4_gold.png');
        game.load.image('game.gnome4_win2', 'img/game4_gnome_win2.png');
        game.load.image('game.chest_left', 'img/chest_left.png');
        game.load.image('game.chest_right', 'img/chest_right.png');
        game.load.spritesheet('game.gnome4_1', 'img/game4_gnome_1.png', 192, 176, 15);
        game.load.spritesheet('game.gnome4_2', 'img/game4_gnome_2.png', 192, 176, 10);
        game.load.spritesheet('game.gnome4_key', 'img/game4_gnome_key.png', 192, 176, 20);
        game.load.spritesheet('game.chest_win_left', 'img/game4_chest_win_left.png', 128, 128, 6);
        game.load.spritesheet('game.chest_win_right', 'img/game4_chest_win_right.png', 128, 128, 6);
        game.load.spritesheet('game.chest_lose_left', 'img/game4_chest_lose_left.png', 128, 128, 6);
        game.load.spritesheet('game.chest_lose_right', 'img/game4_chest_lose_right.png', 128, 128, 6);
        game.load.spritesheet('game.gnome4_win', 'img/game4_gnome_win.png', 208, 285, 15);
        game.load.spritesheet('game.gnome4_win3', 'img/game4_gnome_win3.png', 160, 176, 5);
        game.load.spritesheet('game.gnome4_lose', 'img/game4_gnome_lose.png', 208, 176, 6);

        // game.load.image('game.backgroundGame2', 'img/shape1150.png');
        // game.load.spritesheet('game.rope', 'img/rope.png', 16, 320);

        // game.load.spritesheet('game.monkey21', 'img/monkey21.png', 176, 480);
        // game.load.spritesheet('game.monkey22', 'img/monkey22.png', 176, 480);
        // game.load.spritesheet('game.monkey23', 'img/monkey23.png', 176, 480);
        // game.load.spritesheet('game.monkey24', 'img/monkey24.png', 176, 480);

        // game.load.spritesheet('game.monkey21H', 'img/monkey21H.png', 176, 480);
        // game.load.spritesheet('game.monkey22H', 'img/monkey22H.png', 176, 480);
        // game.load.spritesheet('game.monkey23H', 'img/monkey23H.png', 176, 480);
        // game.load.spritesheet('game.monkey24H', 'img/monkey24H.png', 176, 480);

        // game.load.spritesheet('game.monkeyTakeRope1', 'img/monkeyTakeRope1.png', 176, 480);
        // game.load.spritesheet('game.monkeyTakeRope2', 'img/monkeyTakeRope2.png', 176, 480);
        // game.load.spritesheet('game.monkeyTakeRope1H', 'img/monkeyTakeRope1H.png', 176, 480);
        // game.load.spritesheet('game.monkeyTakeRope2H', 'img/monkeyTakeRope2H.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatBanana', 'img/monkeyEatBanana.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatBananaH', 'img/monkeyEatBananaH.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatHammer1', 'img/monkeyEatHammer1.png', 160, 480);
        // game.load.spritesheet('game.monkeyEatHammer12', 'img/monkeyEatHammer12.png', 176, 480);
        // game.load.spritesheet('game.monkeyEatHammer2', 'img/monkeyEatHammer2.png', 160, 480);
        // game.load.spritesheet('game.monkeyEatHammer22', 'img/monkeyEatHammer22.png', 176, 480);
        // game.load.spritesheet('game.monkeyHeadache', 'img/monkeyHeadache.png', 176, 480);

        // game.load.audio('game.winRope1', 'sound/winRope1.mp3');
        // game.load.audio('game.winRope2', 'sound/winRope2.mp3');
        // game.load.audio('game.bitMonkey', 'sound/bitMonkey.mp3');

        // game.load.image('game.backgroundGame4', 'img/shape943.svg');

        // game.load.spritesheet('game.mankeyHanging1', 'img/mankeyHanging1.png', 240, 304);
        // game.load.spritesheet('game.mankeyHanging2', 'img/mankeyHanging2.png', 240, 304);
        // game.load.spritesheet('game.mankeyHanging3', 'img/mankeyHanging3.png', 240, 304);
        // game.load.spritesheet('game.mankeyLeftPoint', 'img/mankeyLeftPoint.png', 240, 304);
        // game.load.spritesheet('game.mankeyLosePoint', 'img/mankeyLosePoint.png', 240, 304);
        // game.load.spritesheet('game.mankeyRightPoint', 'img/mankeyRightPoint.png', 240, 304);
        // game.load.spritesheet('game.mankeyWinPoint', 'img/mankeyWinPoint.png', 240, 304);

        // game.load.spritesheet('game.goldPoint', 'img/goldPoint.png', 96, 128);
        // game.load.spritesheet('game.questionPoint', 'img/questionPoint.png', 96, 80);
        // game.load.spritesheet('game.spiderPoint', 'img/spiderPoint.png', 96, 128);
        // game.load.spritesheet('game.flashFive', 'img/flashFive.png', 32, 48);
        // game.load.spritesheet('game.take_or_risk_anim', 'img/take_or_risk_anim.png', 192, 160);

        // game.load.image('game.take_or_risk_bg', 'img/shape953.svg');
        // game.load.audio('game.winPoint1', 'sound/winPoint1.mp3');
        // game.load.audio('game.winPoint2', 'sound/winPoint2.mp3');
        game.load.audio('game.losePoint', 'sounds/losePoint.mp3');
        game.load.audio('game4_win', 'sounds/game4_win.mp3');
        game.load.audio('game4_lose', 'sounds/game4_lose.mp3');

        // game.load.image('game.monkeyR', 'img/monkeyR.png');
        // game.load.image('game.butterflyR', 'img/butterflyR.png');

        game.load.image('cell0', 'img/0.png');
        game.load.image('cell1', 'img/1.png');
        game.load.image('cell2', 'img/2.png');
        game.load.image('cell3', 'img/3.png');
        game.load.image('cell4', 'img/4.png');
        game.load.image('cell5', 'img/5.png');
        game.load.image('cell6', 'img/6.png');
        game.load.image('cell7', 'img/7.png');
        game.load.image('cell8', 'img/8.png');

        game.load.spritesheet('cellAnim', 'img/cellAnim.png', 96, 96);

        game.load.image('bonusGame', 'img/image536.png');
        game.load.image('wildSymbol', 'img/image537.png');
        game.load.image('play1To', 'img/image546.png');
        game.load.image('takeOrRisk1', 'img/image474.png');
        game.load.image('takeOrRisk2', 'img/image475.png');
        game.load.image('take', 'img/image554.png');

        game.load.image('topScoreGame1', 'img/main_bg_1.png');
        game.load.image('topScoreGame2', 'img/main_bg_2.png');
        game.load.image('topScoreGame3', 'img/main_bg_3.png');
        game.load.image('topScoreGame4', 'img/main_bg_4.png');

        game.load.image('loseTitleGame2', 'img/image484.png');
        game.load.image('winTitleGame2', 'img/image486.png');
        game.load.image('forwadTitleGame2', 'img/image504.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/gold_row_win.png', 96, 96);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

