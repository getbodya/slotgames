function randomNumber(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example');

function selectLine(n) {
    location1.currentLine = n;

    for (var i = 1; i <= location1.lines.count; ++i) {
        location1.lines[i].sprite.visible = false;
    }
    for (var i = 1; i <= n; ++i) {
        location1.lines[i].sprite.visible = true;
        location1.lines[i].sprite.loadTexture('line_' + i);
    }
};
function preselectLine(n) {
    for (var i = 1; i <= location1.lines.count; ++i) {
        location1.lines[i].sprite.visible = false;
    }
    for (var i = 1; i <= n; ++i) {
        location1.lines[i].sprite.loadTexture('linefull_' + i);
        location1.lines[i].sprite.visible = true;
    }
}
function disableLinesBtn(){
    for (var i = 1; i <= 9; ++i) {
        if (i % 2 != 0) {
            location1.lines[i].button.loadTexture('btnline_d' + i);
        }
    }
};
function unDisableLinesBtn(){
    for (var i = 1; i <= 9; ++i) {
        if (i % 2 != 0) {
            location1.lines[i].button.loadTexture('btnline' + i);
        }
    }
};
function spawnPrize(rope) {

    var spritePicture;
    var prize;
    var stun = false;

    var percent = randomNumber(1,100);
    if(percent < 50) {
        spritePicture = 'prize_0';
        location2.monkeyStand.visible = false;
        location2.monkeyEat.visible = true;
        location2.monkeyEat.position.x = location2.monkeyStand.position.x;
        location2.monkeyEatAnimation.play();
    } else {
        var variant = randomNumber(1,3);
        spritePicture = 'prize_'+variant;
        stun = true;
    }
    prize = game.add.sprite(rope.position.x, rope.position.y, spritePicture);
    prize.anchor.set(0.5, 0);
    var tween = game.add.tween(prize.position).to({y:285}, 1000, Phaser.Easing.LINEAR, true);
    tween.onComplete.add(function(){
        prize.destroy();
        if(stun) {
            location2.monkeyStand.visible = false;
            location2.monkeyStun.visible = true;
            location2.monkeyStun.position.x = location2.monkeyStand.position.x - 45;
            location2.monkeyStunAnimation.play();
            location2.shit.play();
            setTimeout(function(){
                game.state.restart();
                location2.freeze = false;
                location2.ropesCount = 5;
            }, 2000);
        } else {
            if(!location2.ropesCount) {
                setTimeout(function(){
                    game.state.restart();
                    location2.freeze = false;
                    location2.ropesCount = 5;
                }, 2000);
            }
        }
    });

    rope.destroy();
    location2.prizeSound.play();
}
function shuffle(arr) {
    return arr.sort(function() {return 0.5 - Math.random()});
}
//локация 1
var location1 = {

    background: null,
    sound: null,
    monkey: null,
    monkeyBanan: null,
    monkeyLips: null,
    monkeyEyesAround: null,
    monkeyEyesClose: null,
    monkeySeeSlot : null,
    startButton: null,
    bars: [],
    spinStatus :  false,
    barsCurrentSpins : [0, 0, 0, 0, 0],
    barsTotalSpins : [15, 27, 39, 51, 63 ],
    countPlayBars : 0,
    currentLine : 1,
    lvl1 : null,
    lvl2 : null,
    lines : {
        1: {
            coord: 242,
            sprite: null,
            btncoord: 247,
            button: null
        },
        2: {
            coord: 99,
            sprite: null
        },
        3: {
            coord: 385,
            sprite: null,
            btncoord: 312,
            button: null
        },
        4: {
            coord: 153,
            sprite: null
        },
        5: {
            coord: 335 - 200,
            sprite: null,
            btncoord: 380,
            button: null
        },
        6: {
            coord: 126,
            sprite: null
        },
        7: {
            coord: 361 - 100,
            sprite: null,
            btncoord: 444,
            button: null
        },
        8: {
            coord: 266,
            sprite: null
        },
        9: {
            coord: 218 - 68,
            sprite: null,
            btncoord: 508,
            button: null
        }
    },

    create:function(){
        //Добавление фона
        location1.background = game.add.sprite(0,0, 'game1.background');

        //создание куска обезьяны
        location1.monkeyBody = game.add.sprite(292, 399, 'game1.monkeyBody');
        location1.monkeyHand = game.add.sprite(255, 399, 'game1.monkeyHand');

        //создание обезьяны с бананом
        location1.monkeyBanan = game.add.sprite(292, 399, 'game1.monkeyBanan');
        location1.monkeyBanan.visible = false;   

        location1.monkeyLips = game.add.sprite(292, 418, 'game1.monkey_lips_move');
        location1.monkeyLips.visible = false;

        location1.monkeyEyesClose = game.add.sprite(309.9, 421, 'game1.monkey_eyes_close');
        location1.monkeyEyesClose.visible = false;

        location1.monkeyEyesAround = game.add.sprite(309, 421, 'game1.monkey_eyes_around');
        location1.monkeyEyesAround.visible = false;

        location1.monkeySeeSlot = game.add.sprite(265, 403, 'game1.monkey_see_slot');
        location1.monkeySeeSlot.visible = false;

        location1.lvl1 = game.add.sprite(573, 526, 'btnline11');
        location1.lvl1.inputEnabled = true;
        location1.lvl1.input.useHandCursor = true;
        location1.lvl1.events.onInputDown.add(function () {
            location1.lvl1.loadTexture('btnline_p11');
        }, this);
        location1.lvl1.events.onInputUp.add(function () {
            location1.lvl1.loadTexture('btnline11');
            game.state.start('location1');
        }, this);
        location1.lvl2 = game.add.sprite(639, 526, 'btnline13');
        location1.lvl2.inputEnabled = true;
        location1.lvl2.input.useHandCursor = true;
        location1.lvl2.events.onInputDown.add(function(){
            location1.lvl2.loadTexture('btnline_p13');
        }, this);
        location1.lvl2.events.onInputUp.add(function(){
            location1.lvl2.loadTexture('btnline13');
            game.state.start('location2');
        }, this);
        lvl3 = game.add.sprite(703, 526, 'btnline15');
        lvl3.inputEnabled = true;
        lvl3.input.useHandCursor = true;
        lvl3.events.onInputDown.add(function () {
            lvl3.loadTexture('btnline_p15');
        }, this);
        lvl3.events.onInputUp.add(function () {
            lvl3.loadTexture('btnline15');
            game.state.start('location3');
        }, this);
        //создание анимации для обезьяны с бананом
        location1.monkeyBanan.animations.add('stand', [0,1,2,3,4,5,6,7], 5, false);
        location1.monkeyBanan.animations.getAnimation('stand').onComplete.add(function(){
            location1.monkeyBody.visible = true; 
            location1.monkeyBanan.visible = false;      
            monkeEyesAroundAnimation();
        });
        location1.monkeyLips.animations.add('stand', [0,1,2,3,4,5,6], 5, false);
        location1.monkeyLips.animations.getAnimation('stand').onComplete.add(function(){
            location1.monkeyLips.visible = false;     
            monkeEyesCloseAnimation();
        });
        location1.monkeyEyesAround.animations.add('stand', [0,1,2,3,4,5,6,7], 5, false);
        location1.monkeyEyesAround.animations.getAnimation('stand').onComplete.add(function(){
            location1.monkeyEyesAround.visible = false;     
            monkeLipsAnimation();     
        });        
        location1.monkeyEyesClose.animations.add('stand', [0,1,2,3,4,5,6], 5, false);
        location1.monkeyEyesClose.animations.getAnimation('stand').onComplete.add(function(){
            location1.monkeyEyesClose.visible = false;     
            monkeyBananAnimation();
        });
        location1.monkeySeeSlot.animations.add('stand', [0,1], 5, false);
        location1.monkeySeeSlot.animations.getAnimation('stand').onComplete.add(function(){
            location1.monkeySeeSlot.visible = false;    
            monkeyBananAnimation();
            if (location1.spinStatus){
                monkeSeeSlotAnimation();
            }
            else{
                location1.monkeyBody.visible = true;
                location1.monkeyHand.visible = true;
                location1.monkeySeeSlot.visible = false; 
                monkeyBananAnimation();
            }
        });


        function monkeyBananAnimation() {
            location1.monkeyBanan.visible = true;  
            location1.monkeyBody.visible = false;  
            location1.monkeyBanan.animations.getAnimation('stand').play();
        }
        function monkeLipsAnimation() {
            location1.monkeyLips.visible = true;   
            location1.monkeyLips.animations.getAnimation('stand').play();
        }
        function monkeEyesAroundAnimation() {
            location1.monkeyEyesAround.visible = true;   
            location1.monkeyEyesAround.animations.getAnimation('stand').play();
        }
        function monkeEyesCloseAnimation() {
            location1.monkeyEyesClose.visible = true;   
            location1.monkeyEyesClose.animations.getAnimation('stand').play();
        }
        function monkeSeeSlotAnimation() {
            location1.monkeyBody.visible = false;
            location1.monkeyHand.visible = false;
            location1.monkeyBanan.visible = false; 
            location1.monkeyLips.visible = false; 
            location1.monkeyEyesClose.visible = false; 
            location1.monkeyEyesAround.visible = false; 
            location1.monkeySeeSlot.visible = true;
            location1.monkeySeeSlot.animations.getAnimation('stand').play();
        }
        monkeyBananAnimation();
        //добавление кнопки
        location1.startButton = game.add.sprite(30, 517, 'game1.start');
        location1.startButton.inputEnabled = true;
        location1.startButton.input.useHandCursor = true;
        location1.startButton.events.onInputUp.add(btnStartUp, this);
        location1.startButton.events.onInputDown.add(function(){
            if(location1.spinStatus){
                return;
            }
            location1.startButton.loadTexture('game1.start_p');
        });

        //добавление слота
        location1.bars[0] = game.add.tileSprite(82.5, 82, 96, 320, 'game1.bar');
        location1.bars[0].tilePosition.y = - 8 - randomNumber(0,8)*112 ;       
        location1.bars[1] = game.add.tileSprite(194, 82, 96, 320, 'game1.bar');
        location1.bars[1].tilePosition.y = - 8 - randomNumber(0,8)*112;
        location1.bars[2] = game.add.tileSprite(305 , 82, 96, 320, 'game1.bar');
        location1.bars[2].tilePosition.y = - 8 - randomNumber(0,8)*112;
        location1.bars[3] = game.add.tileSprite(417, 82, 96, 320, 'game1.bar');
        location1.bars[3].tilePosition.y = - 8 - randomNumber(0,8)*112;        
        location1.bars[4] = game.add.tileSprite(529, 82, 96, 320, 'game1.bar');
        location1.bars[4].tilePosition.y = - 8 - randomNumber(0,8)*112;

        for (var i = 1; i <= 9; ++i) {
            location1.lines[i].sprite = game.add.sprite(74, location1.lines[i].coord, 'line_' + i);
            location1.lines[i].sprite.visible = false;
            if (i % 2 != 0) {
                location1.lines[i].sound = game.add.audio('line' + i);
                location1.lines[i].button = game.add.sprite(location1.lines[i].btncoord, 526, 'btnline' + i);
                location1.lines[i].button.inputEnabled = true;
                location1.lines[i].button.input.useHandCursor = true;
                (function (n) {
                    location1.lines[n].button.events.onInputDown.add(function () {
                        if(location1.spinStatus)
                            return;
                        hideLines();
                        preselectLine(n);
                        location1.lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                    location1.lines[n].button.events.onInputUp.add(function () {
                        if(location1.spinStatus)
                            return;
                        hideLines();
                        selectLine(n);
                        location1.lines[n].button.loadTexture('btnline' + n);
                        location1.lines[n].sound.play();
                    }, this);
                })(i);
            }
        }

        stopSound = game.add.audio('stop');
        rotateSound = game.add.audio('rotate');
        rotateSound.loop = true; 

        function btnStartUp(){
            if(location1.spinStatus)
                return;

            location1.startButton.loadTexture('game1.start_d');
            location1.countPlayBars = 5;
            location1.barsCurrentSpins = [0, 0, 0, 0, 0];
            location1.spinStatus = true;
            location1.monkeyBanan.animations.getAnimation('stand').stop(true);
            location1.monkeyLips.animations.getAnimation('stand').stop(true);
            location1.monkeyEyesAround.animations.getAnimation('stand').stop(true);
            location1.monkeyEyesClose.animations.getAnimation('stand').stop(true);
            disableLinesBtn();
            hideLines();
            monkeSeeSlotAnimation();
            rotateSound.play();
        };

        function hideLines() {
            for (var i in  location1.lines) {
               location1.lines[i].sprite.visible = false;
           }
       }     
       preselectLine(location1.currentLine);
   },
   update:function(){
    if (location1.spinStatus){
        for (var i in location1.bars) {
            location1.barsCurrentSpins[i]++;
            if (location1.barsCurrentSpins[i] < location1.barsTotalSpins[i]) {
                location1.bars[i].tilePosition.y += 112;
            } else if (location1.barsCurrentSpins[i] == location1.barsTotalSpins[i]) {
                location1.countPlayBars--;
                stopSound.play();
            }
        }
        if (location1.countPlayBars === 0){
            location1.spinStatus = false;
            rotateSound.stop();
            unDisableLinesBtn();
            selectLine(location1.currentLine);
            location1.startButton.loadTexture('game1.start');
        }
    }
        // location1.bar1.tilePosition.y += 10;
    }
};
game.state.add('location1', location1); //добавление локации 1 в игру

var location2 = {
 ropes : [],
 buttons : [],
 freeze : false,
 ropesCount : 5,
 stun : false,
 buttonsPositionsX : {1:247, 3:312, 5:380, 7:444, 9:508},
 monkeyPositionsX : {1:59, 2:137, 3:213, 4:290, 5:367},
 ropePositionX : 218,
 ropeIndexes : [1,2,3,4,5],
 selectedRope : null,

 create : function() {
    location2.ropePositionX = 218;
    location2.keypress = game.add.sound('b1_keypress');
    location2.prizeSound = game.add.sound('b1_prizedown');
    location2.shit = game.add.sound('b1_shit');

    game.add.sprite(0, 0, 'background2');

    for(var i in location2.ropeIndexes) {
        location2.sprite = game.add.sprite(location2.ropePositionX, 50, 'rope');
        location2.ropePositionX += 77;
        location2.sprite.inputEnabled = true;
        location2.sprite.input.useHandCursor = true;
        location2.standAnim = location2.sprite.animations.add('stand', [0,1,2,3,4,5,6,7,8,9,10], 5, true);
        location2.useAnim = location2.sprite.animations.add('use', [11,11,11,11,11,11,11,11,11]);
        location2.sprite.animations.play('stand');
        location2.ropes[location2.ropeIndexes[i]] = location2.sprite;
    }

    location2.monkeyStand = game.add.sprite(location2.monkeyPositionsX[1], 314, 'monkey_stand');
    location2.monkeyEat = game.add.sprite(location2.monkeyPositionsX[1], 279, 'monkey_eat');
    location2.monkeyStun = game.add.sprite(0, 255, 'monkey_stun');

    location2.monkeyEatAnimation = location2.monkeyEat.animations.add('eat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
    location2.monkeyEatAnimation.onComplete.add(function(){
        location2.monkeyEat.visible = false;
        location2.monkeyStand.visible = true;
        location2.monkeyStandStand.play();
        location2.freeze = false;
    });
    location2.monkeyEat.visible = false;

    location2.monkeyStunAnimation = location2.monkeyStun.animations.add('stun', [0,1,2,3,4], 5, true);
    location2.monkeyStun.visible = false;

    location2.monkeyStandStand = location2.monkeyStand.animations.add('stand', [0,1], 1, true);
    location2.monkeyStandTake = location2.monkeyStand.animations.add('take', [2,3,4,5,6,7], 7, false);
    location2.monkeyStandRun = location2.monkeyStand.animations.add('run', [8,9,10], 7, true);
    location2.monkeyStandTake.onComplete.add(function(){
        location2.monkeyStandRun.play();
        location2.monkeyMoveTween = game.add.tween(location2.monkeyStand.position);
        location2.monkeyMoveTween.onComplete.add(function(){
            location2.monkeyStandStand.play();
            spawnPrize(location2.ropes[location2.selectedRope]);
        });
        location2.monkeyMoveTween.to({x:location2.monkeyStand.position.x+75}, 200, Phaser.Easing.LINEAR, true)
    });

    location2.monkeyStandStand.play();

    for(var i in location2.monkeyPositionsX) {
        (function(n, btn){
            btn.events.onInputDown.add(function () {
                if(location2.freeze) {
                    return;
                }
                // btn.loadTexture('btnline_p' + n);
            }, this);
            btn.events.onInputUp.add(function () {
                if(location2.freeze) {
                    return;
                }
                location2.freeze = true;
                location2.ropesCount--;
                // btn.loadTexture('btnline_d' + n);
                btn.inputEnabled = false;
                location2.keypress.play();
                location2.monkeyStand.position.x = location2.monkeyPositionsX[n];
                btn.animations.play('use', 10, false, false);
                location2.monkeyStandTake.play();
                location2.selectedRope = n;
            }, this);
        })(i, location2.ropes[location2.ropeIndexes[i-1]]);
    }

    location2.startButton = game.add.sprite(30, 517, 'game1.start');
    location2.startButton.inputEnabled = true;
    location2.startButton.input.useHandCursor = true;
    location2.startButton.events.onInputDown.add(function(){
        location2.startButton.loadTexture('game1.start_p');
    }, this);
    location2.startButton.events.onInputUp.add(function(){
        location2.startButton.loadTexture('game1.start');
        game.state.restart();
        location2.freeze = false;
        location2.ropesCount = 5;
    }, this);

    // createLevelButtons();
}

};
game.state.add('location2', location2);
//загрузчик
var location3 = {
 cardValues : {
    1: 11,
    2: 7,
    3: 3,
    4: 14,
    5: 13
},
cards : [],

create : function() {
    location3.opencard = game.add.audio('opencard');
    location3.openuser = game.add.audio('openuser');

    game.add.sprite(0, 0, 'background3');
    location3.cardIndexes = shuffle(Object.keys(location3.cardValues));
    location3.cards[0] = game.add.sprite(83, 128, 'card_'+ location3.cardIndexes[0]);
    location3.cardPos = 194;
    for(var i=1; i<=4; ++i) {
        location3.cards[i] = game.add.sprite(location3.cardPos, 128, 'card_back');
        location3.cards[i].inputEnabled = true;
        location3.cards[i].input.useHandCursor = true;
        location3.cardPos += 112;
    }
    game.add.sprite(101, 276, 'dealer');

    location3.monkey = game.add.sprite(38, 291, 'monkey2');
    location3.monkeyStandAnim = location3.monkey.animations.add('stand', [8,9], 1, true);
    location3.monkeyLoseAnim = location3.monkey.animations.add('lose', [0,1,2,3,4,5,6,7], 5, false);
    location3.monkeyWinAnim = location3.monkey.animations.add('win', [0,1,2,3,4,5,10,11,12], 5, false);
    location3.monkeyStandWinAnim = location3.monkey.animations.add('standwin', [13,14], 1, true);
    location3.monkeyLoseAnim.onComplete.add(function(){
        location3.monkeyStandAnim.play();
    });
    location3.monkeyWinAnim.onComplete.add(function(){
        location3.monkeyStandWinAnim.play();
    });
    location3.monkeyStandAnim.play();

    location3.message = game.add.sprite(217, 310, 'message');
    location3.messageLose = location3.message.animations.add('lose', [0,1,2,3], 5, false);
    location3.messageWin = location3.message.animations.add('win', [0,1,2,4], 5, false);
    location3.message.visible = false;

    location3.buttonsPositionsX = {1:{pos:312,i:2}, 2:{pos:380,i:3}, 3:{pos:444,i:4}, 4:{pos:508,i:5}};
    location3.buttons = {};
    for(var i in location3.buttonsPositionsX) {
        (function(n, btn, pic){
            btn.events.onInputUp.add(function () {
                location3.card = location3.cards[n];
                location3.card.loadTexture('card_'+ location3.cardIndexes[n]);
                game.add.sprite(location3.card.position.x+20, location3.card.position.y+148, 'pick');

                location3.dealerValue = location3.cardValues[location3.cardIndexes[0]];
                location3.userValue = location3.cardValues[location3.cardIndexes[n]];


                location3.message.visible = true;
                if(location3.dealerValue < location3.userValue) {
                    location3.monkeyWinAnim.play();
                    location3.openuser.play();
                    location3.messageWin.play();
                } else {
                    location3.monkeyLoseAnim.play();
                    location3.opencard.play();
                    location3.messageLose.play();
                }

                for(var c in location3.cards) {
                    location3.cards[c].loadTexture('card_'+ location3.cardIndexes[c]);
                }
                for(var b in location3.cards) {
                    location3.cards[b].inputEnabled = false;
                }
            }, this);
        })(i, location3.cards[i], location3.buttonsPositionsX[i].i);
    }

    location3.startButton = game.add.sprite(30, 517, 'game1.start')
    location3.startButton.inputEnabled = true;
    location3.startButton.input.useHandCursor = true;
    location3.startButton.events.onInputDown.add(function(){
        location3.startButton.loadTexture('game1.start_p');
    }, this);
    location3.startButton.events.onInputUp.add(function(){
        location3.startButton.loadTexture('game1.start');
        game.state.restart();
    }, this);

    // createLevelButtons();
}
}
game.state.add('location3', location3);
//загрузчик
var locationPreload = {

    preload:function(){
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.load.image('game1.background', 'canvas-bg.png');

        game.load.image('game1.monkeyBody', 'monkey1/monkey.png');
        game.load.image('game1.monkeyHand', 'monkey1/monkey_left.png');

        game.load.spritesheet('game1.monkeyBanan', 'monkey1/banan_move.png', 124, 99, 8);
        game.load.spritesheet('game1.monkey_eyes_around', 'monkey1/eyes_around.png', 35, 9, 8);
        game.load.spritesheet('game1.monkey_eyes_close', 'monkey1/eyes_close.png', 34, 9, 7);
        game.load.spritesheet('game1.monkey_lips_move', 'monkey1/lips_move.png', 60, 50, 7);
        game.load.spritesheet('game1.monkey_see_slot', 'monkey1/see_slot.png', 116, 95, 2);
        game.load.spritesheet('game1.monkey_set_slot', 'monkey1/set_slot.png', 140, 95, 3);
        game.load.image('game1.start', 'start.png');
        game.load.image('game1.start_p', 'start_p.png');
        game.load.image('game1.start_d', 'start_d.png');

        game.load.audio('stop', 'stop.wav');
        game.load.audio('rotate', 'rotate.wav');

        game.load.image('game1.bar', 'bars.png');

        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }
        game.load.image('btnline11', 'lines/line11.png');
        game.load.image('btnline_p11', 'lines/line11_p.png');
        game.load.image('btnline13', 'lines/line13.png');
        game.load.image('btnline_p13', 'lines/line13_p.png');
        game.load.image('btnline15', 'lines/line15.png');
        game.load.image('btnline_p15', 'lines/line15_p.png');

        game.load.image('background2', 'bonus/back.png');
        game.load.spritesheet('rope', 'bonus/rope.png', 21, 335, 17);
        game.load.spritesheet('monkey_stand', 'bonus/monkey_take.png', 183, 185, 12);
        game.load.spritesheet('monkey_eat', 'bonus/monkey_eat.png', 138, 220, 14);
        game.load.spritesheet('monkey_stun', 'bonus/monkey_stun.png', 227, 244, 5);
        game.load.audio('b1_keypress', 'bonus/b1_keypress.wav');
        game.load.audio('b1_prizedown', 'bonus/b1_prizedwn.wav');
        game.load.audio('b1_shit', 'bonus/b1_shit.wav');

        for(var i=0; i<=3; ++i) {
            game.load.image('prize_' + i, 'bonus/prizes/' + i + '.png');
        }
        game.load.image('background3', 'cards/back.png');
        game.load.image('dealer', 'cards/dealer.png');
        game.load.image('pick', 'cards/pick.png');
        game.load.image('card_back', 'cards/cards/back.png');
        game.load.spritesheet('monkey2', 'cards/monkey.png', 182, 209, 15);
        game.load.spritesheet('message', 'cards/message.png', 179, 65, 5);
        var cardValues = {
            1: 11,
            2: 7,
            3: 3,
            4: 14,
            5: 13
        };
        for(var i in cardValues) {
            game.load.image('card_'+i, 'cards/cards/'+i+'.png');
        }
        game.load.audio('opencard', 'cards/opencard.wav');
        game.load.audio('openuser', 'cards/openuser.wav');
    },
    create:function(){
        game.state.start('location1'); //переключение на 1 игру
    }

};
game.state.add('locationPreload', locationPreload); //добавление загрузчика в игру

game.state.start('locationPreload'); //начало игры с локации загрузчика