var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var checkHelm = false;

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');

        //var betOneSound = game.add.audio('game.betOneSound');
        //var betMaxSound = game.add.audio('game.betMaxSound');

        
        // изображения
        
        slotPosition = [[142, 87], [142, 199], [142, 311], [254, 87], [254, 199], [254, 311], [365, 87], [365, 199], [365, 311], [477, 87], [477, 199], [477, 311], [589, 87], [589, 199], [589, 311]];
        addSlots(game, slotPosition);
        game.add.sprite(0,0, 'game.background');
        game.add.sprite(94,22, 'game.background1');
        topBarImage = game.add.sprite(94,22, 'topScoreGame1');
        
        //создание куска обезьяны
        
        addTableTitle(game, 'play1To',526,436);


        var linePosition = [[134,243], [134,110], [134,383], [134,158], [134,158], [134,132], [134,287], [134,270], [134,169]];
        var numberPosition = [[104,234], [103,88], [105,378], [104,153], [106,314], [102,120], [106,346], [106,266], [105,201]]; 
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);

        var pageCount = 5;

        // кнопки
        addButtonsGame1(game, pageCount);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[245, 34, 20], [455, 34, 20], [640, 34, 20], [116, 68, 14], [694, 68, 14]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        // анимация
        monkeyAnim1 = game.add.sprite(270,395, 'monkey_anim1');
        monkeyAnim1.animations.add('monkeyAnim', [0,1,2,3,4,5], 5, false);
        monkeyAnim1.visible = false;  
        monkeyAnim1.animations.getAnimation('monkeyAnim').onComplete.add(function(){
            monkeyAnim1.visible = false;     
            monkeyAnimationRand();
        });
        monkeyAnim2 = game.add.sprite(270,395, 'monkey_anim1');
        monkeyAnim2.animations.add('monkeyAnim', [6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        monkeyAnim2.visible = false;
        monkeyAnim2.animations.getAnimation('monkeyAnim').onComplete.add(function(){
            monkeyAnim2.visible = false;     
            monkeyAnimationRand();
        });
        monkeyAnim3 = game.add.sprite(270,395, 'monkey_anim1');
        monkeyAnim3.animations.add('monkeyAnim', [20,21,21,21,22], 5, false);
        monkeyAnim3.visible = false;
        monkeyAnim3.animations.getAnimation('monkeyAnim').onComplete.add(function(){
            monkeyAnim3.visible = false;     
            monkeyAnimationRand();
        });
        monkeyAnim4 = game.add.sprite(270,390, 'monkey_anim3');
        monkeyAnim4.animations.add('monkeyAnim', [0,1,2,3,4,5,6,7], 5, false);
        monkeyAnim4.visible = false;
        monkeyAnim4.animations.getAnimation('monkeyAnim').onComplete.add(function(){
            monkeyAnim4.visible = false;     
            monkeyAnimationRand();
        });
        monkeyWaitWin = game.add.sprite(254,390, 'monkey_anim2');
        monkeyWaitWin.animations.add('monkeyAnim', [0,1,2,3,4,5,6,7,8], 5, false);
        monkeyWaitWin.visible = false;
        monkeyWaitWin.animations.getAnimation('monkeyAnim');

        monkeyWin = game.add.sprite(254,390, 'monkey_anim2');
        monkeyWin.animations.add('monkeyAnim', [10,11,12], 5, false);
        monkeyWin.visible = false;
        monkeyWin.animations.getAnimation('monkeyAnim');
        
        monkeyBonusGame = game.add.sprite(254,390, 'monkey_anim2');
        monkeyBonusGame.animations.add('monkeyAnim', [12,13,14], 5, true);
        monkeyBonusGame.animations.getAnimation('monkeyAnim');
        monkeyBonusGame.visible = false;

        monkeyBonusGameHelm = game.add.sprite(254,390, 'monkey_anim2_helm');
        monkeyBonusGameHelm.animations.add('monkeyAnim', [12,13,14], 5, true);
        monkeyBonusGameHelm.animations.getAnimation('monkeyAnim');
        monkeyBonusGameHelm.visible = false;

        monkeyAnimationRand();

        function monkeyAnimationRand() {

            var randBaba = randomNumber(1,4);

            switch(randBaba) {
                case 1:
                monkeyAnim1.visible = true;
                monkeyAnim1.animations.getAnimation('monkeyAnim').play().onComplete.add(function(){
                    monkeyAnim1.visible = false;
                    monkeyAnimationRand();
                });
                break;
                case 2:
                monkeyAnim2.visible = true;
                monkeyAnim2.animations.getAnimation('monkeyAnim').play().onComplete.add(function(){
                    monkeyAnim2.visible = false;
                    monkeyAnimationRand();
                });
                break;
                case 3:
                monkeyAnim3.visible = true;
                monkeyAnim3.animations.getAnimation('monkeyAnim').play().onComplete.add(function(){
                    monkeyAnim3.visible = false;
                    monkeyAnimationRand();
                });
                break;
                case 4:
                monkeyAnim4.visible = true;
                monkeyAnim4.animations.getAnimation('monkeyAnim').play().onComplete.add(function(){
                    monkeyAnim4.visible = false;
                    monkeyAnimationRand();
                });
                break;            
            }
        }
        var pageCoord = [[94, 22], [94, 22], [94, 22], [94, 22], [94, 22]];   
        var btnCoord = [[94, 422], [334, 422], [558, 422]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);

        butterfly_btn = game.add.sprite(670, 406, 'butterfly_btn');
        butterfly_btnAnim = butterfly_btn.animations.add('butterfly_btn', [0,1,2,3,4,5,6,7,6,5,4,3,2,1,0,0,0,0,0,0,0,0,0,0], 8, true).play();       
        butterfly_btn.visible = false;
        сhameleon = game.add.sprite(300, 22, 'сhameleon');
        сhameleonAnim = сhameleon.animations.add('сhameleon', [0,1,2,3,4,5,6,7,8,0,0,0,0,0,0,0,0,0,0], 8, true).play();       
        сhameleon.visible = false;

        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
   if(bet >= extralife && checkHelm == false) {
            // mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
            // mushroomGrow.animations.add('mushroomGrow', [0,1,2,3,4,5], 10, false);
            // mushroomGrow.animations.getAnimation('mushroomGrow').play().onComplete.add(function(){
            //     mushroomJump = game.add.sprite(353,423, 'game.mushroomJump');
            //     mushroomJump.animations.add('mushroomJump', [0,1,2,3,4,5,6,7,8,9], 10, true);
            //     mushroomJump.animations.getAnimation('mushroomJump').play();
            // });
            // console.log('checkHelm = true');
            // checkHelm = true;
            checkHelm = false;
        }
        
        if(bet < extralife && checkHelm == true) {
            console.log('checkHelm = false');
            checkHelm = false;

            // mushroomGrow.visible = false;
            // mushroomJump.visible = false;

            // mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
            // mushroomGrow.animations.add('mushroomGrow', [5,4,3,2,1,0], 10, false);
            // mushroomGrow.animations.getAnimation('mushroomGrow').play();
        }
        
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
            if(checkHelm == true) {
                // function hideMonkey(){
                //     monkey1.visible = false;
                //     monkey2.visible = false;
                //     monkey3.visible = false;
                //     monkey4.visible = false;
                //     monkey5.visible = false;
                // }

                // hideMonkey();

                // monkeyTakeMushroom1 = game.add.sprite(195,359, 'game.monkeyTakeMushroom1');
                // monkeyTakeMushroom1.animations.add('monkeyTakeMushroom1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 10, false);
                // monkeyTakeMushroom1.visible = false;

                // monkeyTakeMushroom2 = game.add.sprite(195,359, 'game.monkeyTakeMushroom2');
                // monkeyTakeMushroom2.animations.add('monkeyTakeMushroom2', [1,2,3,4], 10, false);
                // monkeyTakeMushroom2.visible = false;

                // function monkeyTakeMushroomAnim() {
                //     monkeyTakeMushroom1.visible = true;
                //     monkeyTakeMushroom1.animations.getAnimation('monkeyTakeMushroom1').play().onComplete.add(function(){
                //         monkeyTakeMushroom1.visible = false;

                //         monkeyTakeMushroom2.visible = true;
                //         monkeyTakeMushroom2.animations.getAnimation('monkeyTakeMushroom2').play();
                //     });
                // }

                // monkeyTakeMushroomAnim();
            }
        }
    };

    game.state.add('game1', game1);

};
