var game = new Phaser.Game(1024, 768, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var platforms;
var buttonPanel;
var cellTop = 2;
var countAnimHold = 0;
var xb = [];
var xo = [];
var xb_2 = [];
var xo_2 = [];
var attention = [];
var WinSnd = [];
var stone_wheel = [];
var checkGame;
var afterFS = false;
function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = false;
    game1.create = function () {

        game.physics.startSystem(Phaser.Physics.ARCADE);

        checkGame = 1;
        multiplierValue = 0;
        cellTop = 2;
        countAnimHold = 0;
        xb = [];
        xo = [];
        xb_2 = [];
        xo_2 = [];
        attention = [];
        WinSnd = [];
        stone_wheel = [];
        freeSpinAnim = false;
        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        // звуки
        stone_wheel[1] = game.add.audio('stone_wheel1');
        stone_wheel[2] = game.add.audio('stone_wheel2');
        stone_wheel[3] = game.add.audio('stone_wheel3');
        stone_wheel[4] = game.add.audio('stone_wheel4');
        stone_wheel[5] = game.add.audio('stone_wheel5');
        Explosion = game.add.audio('Explosion');
        shake = game.add.audio('shake');
        WinSnd[1] = game.add.audio('WinSnd1');
        WinSnd[2] = game.add.audio('WinSnd2');
        WinSnd[3] = game.add.audio('WinSnd3');
        WinSnd[4] = game.add.audio('WinSnd3');
        attention[1] = game.add.audio('attention1Sound');
        attention[2] = game.add.audio('attention2Sound');
        attention[3] = game.add.audio('attention3Sound');
        bgSound = game.add.audio('bgSound');
        bgSound.loop = true;
        btn = game.add.audio('btn');
        btn_spin = game.add.audio('btn_spin');
        after_spin = game.add.audio('after_spin');
        shake = game.add.audio('shake');
        value_sound = game.add.audio('value_sound');
        enter = game.add.audio('enter');
        idle_1sound = game.add.audio('idle_1sound');
        idle_2sound = game.add.audio('idle_2sound');
        idle_3sound = game.add.audio('idle_3sound');        
        freeFall_1sound = game.add.audio('freeFall_1sound');
        freeFall_2sound = game.add.audio('freeFall_2sound');
        freeFall_3sound = game.add.audio('freeFall_3sound');
        free_spin_cell = game.add.audio('free_spin_cell');
        free_spin_soldier = game.add.audio('free_spin_soldier');
        moving_cell = game.add.audio('moving_cell');
        bigWin_1sound = game.add.audio('bigWin_1sound');
        bigWin_2sound = game.add.audio('bigWin_2sound');
        bigWin_3sound = game.add.audio('bigWin_3sound');
        bigWin_4sound = game.add.audio('bigWin_4sound');
        bgSound.play();

        // изображения
        var bg1 = game.add.sprite(0,0, 'bg1');
        if (afterFS === true){
            BlackBG = game.add.sprite(0,0, 'BlackBG');  
            BlackBG.alpha = 1;
            var i = 0;
            (function() {
                if (i < 10) {
                    BlackBG.alpha -= 0.1;
                    i++;
                    setTimeout(arguments.callee, 200);
                } else {
                    unlockDisplay();
                }
            })();
        }
        buttonPanel = game.add.sprite(0,548, 'buttonPanel');
        game.physics.enable(buttonPanel, Phaser.Physics.ARCADE);
        buttonPanel.enableBody = true;
        buttonPanel.body.immovable = true;
        buttonPanel.body.collideWorldBounds = true;
        border_flick = game.add.sprite(874,633, 'border_flick');
        border_flick.visible = false;

        // buttonPanel.body.checkCollision.up = false;
        // buttonPanel.body.checkCollision.down = false;
        var score = game.add.sprite(0,738, 'score');
        multiplier = game.add.sprite(538, 3, 'multiplier');
        multiWin = game.add.sprite(616, -41, 'multiWin');
        multiWin.visible = false;

        xb[0]  = game.add.sprite(571, 44, 'x1_b');
        xb[0].visible = false;
        xb[1]  = game.add.sprite(656, 44, 'x2_b');
        xb[2]  = game.add.sprite(748, 44, 'x3_b');
        xb[3]  = game.add.sprite(838, 44, 'x5_b');

        xo[0]  = game.add.sprite(571, 44, 'x1_o');
        // xo[0].visible = false;
        xo[1]  = game.add.sprite(656, 44, 'x2_o');
        xo[1].visible = false;
        xo[2]  = game.add.sprite(748, 44, 'x3_o');
        xo[2].visible = false;
        xo[3]  = game.add.sprite(838, 44, 'x5_o');
        xo[3].visible = false;

        //multi anim

        multiplier_2 = game.add.sprite(538, 3, 'multiplier');
        multiplier_top = game.add.sprite(541, 0, 'multiplier_top');
        multiplier_bottom = game.add.sprite(541, 93, 'multiplier_bottom');

        xb_2[0]  = game.add.sprite(571, 96, 'x1_b');
        xb_2[1]  = game.add.sprite(656, 44, 'x2_b');
        xb_2[2]  = game.add.sprite(748, 44, 'x3_b');
        xb_2[3]  = game.add.sprite(838, 44, 'x5_b');

        xo_2[0]  = game.add.sprite(571, 44, 'x1_o');
        xo_2[1]  = game.add.sprite(656, -8, 'x2_o');
        xo_2[2]  = game.add.sprite(748, -8, 'x3_o');
        xo_2[3]  = game.add.sprite(838, -8, 'x5_o');

        enterSoldier = game.add.sprite(0,312, 'enterSoldier');        
        enterSoldierAnim = enterSoldier.animations.add('enterSoldier', [], 10, false);
        holdSoldier_1 = game.add.sprite(0,314, 'holdSoldier_1');
        holdSoldier_1.visible = false;
        holdSoldier_2 = game.add.sprite(0,314, 'holdSoldier_2');
        holdSoldier_2.visible = false;
        holdSoldier_1Anim = holdSoldier_1.animations.add('holdSoldier_1', [], 10, false);
        holdSoldier_1Anim.onComplete.add(function () {
            holdSoldier_1.visible = false;
            holdSoldier_2 = game.add.sprite(0,314, 'holdSoldier_2');
            if (freeSpinAnim !== true)
                holdSoldier_2.visible = true;
            slotLayer3Group.add(holdSoldier_2);
            holdSoldier_2.animations.add('holdSoldier_2', [], 10, false).play().onComplete.add(function () {
                countAnimHold = countAnimHold + 1;
                holdSoldier_2.visible = false;
                if (freeSpinAnim !== true & afterFS === false){
                    if (winAnim){
                        randAnimWin()
                    } else {                    
                    // Рандомайз hold
                    if (countAnimHold > 2 & preFreeSpinAnim === false ){
                        randAnimHold();
                    } else {
                        holdSoldier_1.visible = true;
                        holdSoldier_1.animations.getAnimation('holdSoldier_1').play();
                    }
                }

                // Запуск анимации для перехода в фриспины
                // freeFalls_1.visible = true;
                // freeFalls_1Anim.play();
                // freeFall_1sound.play();                
                // setTimeout(function(){            
                //     freeFall_2sound.play()
                // }, 700);
                // Запуск анимации с колесом
                // freeFallsOut_1.visible = true;
                // freeFallsOut_1Anim.play();
                // запуск золотого дождя

            }
        });
        });
        ambient_1 = game.add.sprite(0,314, 'ambient_1');
        ambient_1.visible = false;        
        ambient_1Anim = ambient_1.animations.add('ambient_1', [], 10, false);
        ambient_1Anim.onComplete.add(function () {
            ambient_1.visible = false;
            ambient_2 = game.add.sprite(0,314, 'ambient_2');
            ambient_2.visible = true;
            slotLayer3Group.add(ambient_2);
            ambient_2.animations.add('ambient_2', [], 10, false).play().onComplete.add(function () {
                ambient_2.visible = false;
                if (freeSpinAnim ===true){
                    freeFalls_1.visible = true;
                    freeFalls_1Anim.play();
                    freeFall_1sound.play();    
                    free_spin_soldier.play();            
                    setTimeout(function(){            
                        freeFall_2sound.play()
                    }, 700);
                } else {
                    holdSoldier_1.visible = true;
                    holdSoldier_1Anim.play();
                    holdanim = true;
                }
            });
        });
        idle_1 = game.add.sprite(0,307, 'idle_1');
        idle_1.visible = false;
        idle_1Anim = idle_1.animations.add('idle_1', [], 10, false);
        idle_1Anim.onComplete.add(function () {
            idle_1.visible = false;
            idle_2 = game.add.sprite(0,307, 'idle_2');        
            idle_2.visible = true;
            slotLayer3Group.add(idle_2);
            setTimeout(function(){            
                idle_2sound.play();
            }, 1400);
            idle_2.animations.add('idle_2', [], 10, false).play().onComplete.add(function () {
                idle_2.visible = false;
                idle_3 = game.add.sprite(0,307, 'idle_3');
                idle_3.visible = true;
                slotLayer3Group.add(idle_3);
                idle_3.animations.add('idle_3', [], 10, false).play().onComplete.add(function () {
                    idle_3.visible = false;
                    idle_4 = game.add.sprite(0,307, 'idle_4');
                    idle_4.visible = true;
                    slotLayer3Group.add(idle_4);
                    setTimeout(function(){            
                        idle_3sound.play();
                    }, 1100);
                    idle_4.animations.add('idle_4', [], 10, false).play().onComplete.add(function () {
                        idle_4.visible = false;
                        idle_5 = game.add.sprite(0,307, 'idle_5');
                        idle_5.visible = true;
                        slotLayer3Group.add(idle_5);
                        idle_5.animations.add('idle_5', [], 10, false).play().onComplete.add(function () {
                            idle_5.visible = false;
                            if (freeSpinAnim ===true){
                                freeFalls_1.visible = true;
                                freeFalls_1Anim.play();
                                freeFall_1sound.play();    
                                free_spin_soldier.play();            
                                setTimeout(function(){            
                                    freeFall_2sound.play()
                                }, 700);
                            } else {
                                holdSoldier_1.visible = true;
                                holdSoldier_1Anim.play();
                                holdanim = true;
                            }
                        });
                    });
                });
            });
        });
        freeFalls_1 = game.add.sprite(0,247, 'freeFalls_1');
        freeFalls_1.visible = false;
        freeFalls_1Anim = freeFalls_1.animations.add('freeFalls_1', [], 10, false);
        freeFalls_1Anim.onComplete.add(function () {
            setTimeout(function(){            
                freeFall_3sound.play()
            }, 150);
            freeFalls_1.visible = false;
            freeFalls_2 = game.add.sprite(0,314, 'freeFalls_2');        
            freeFalls_2.visible = true;
            slotLayer3Group.add(freeFalls_2);
            freeFalls_2.animations.add('freeFalls_2', [], 10, false).play().onComplete.add(function () {
                freeFalls_2.visible = false;
                freeFalls_3 = game.add.sprite(0,314, 'freeFalls_3');
                freeFalls_3.visible = true;
                slotLayer3Group.add(freeFalls_3);
                freeFalls_3.animations.add('freeFalls_3', [], 10, false).play().onComplete.add(function () {
                    freeFalls_3.visible = false;
                    freeFalls_4 = game.add.sprite(0,314, 'freeFalls_4');
                    freeFalls_4.visible = true;
                    slotLayer3Group.add(freeFalls_4);
                    freeFalls_4.animations.add('freeFalls_4', [], 10, false).play().onComplete.add(function () {
                        freeFalls_4.visible = false;
                        freeFalls_5 = game.add.sprite(0,314, 'freeFalls_5');
                        freeFalls_5.visible = true;
                        slotLayer3Group.add(freeFalls_5);
                        freeFalls_5.animations.add('freeFalls_5', [], 10, false).play().onComplete.add(function () {
                            freeFalls_5.visible = false;
                            freeFalls_6 = game.add.sprite(0,314, 'freeFalls_6');
                            freeFalls_6.visible = true;
                            slotLayer3Group.add(freeFalls_6);
                            freeFalls_6.animations.add('freeFalls_6', [], 10, false).play().onComplete.add(function () {
                                freeFalls_6.visible = false;
                                freeFalls_7 = game.add.sprite(478,314, 'freeFalls_7');
                                freeFalls_7.visible = true;
                                slotLayer3Group.add(freeFalls_7);
                                BlackBG = game.add.sprite(0,0, 'BlackBG');    
                                treeLeavesGroup2.add(BlackBG);
                                BlackBG.alpha = 0;
                                var i = 0;
                                (function() {
                                    if (i < 10) {
                                        BlackBG.alpha += 0.1;
                                        i++;
                                        setTimeout(arguments.callee, 200);
                                    }
                                })();
                                freeFalls_7.animations.add('freeFalls_7', [], 10, false).play().onComplete.add(function () {
                                    freeFalls_7.visible = false;
                                    freeFalls_8 = game.add.sprite(478,314, 'freeFalls_8');
                                    freeFalls_8.visible = true;
                                    slotLayer3Group.add(freeFalls_8);                          
                                    freeFalls_8.animations.add('freeFalls_8', [], 10, false).play().onComplete.add(function () {
                                        freeFalls_8.visible = false;
                                        game.state.start('game2');
                                        bgSound.stop();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
        bigWin_gold_1 = game.add.sprite(0,0, 'bigWin_gold_1');
        bigWin_gold_1.visible = false;
        bigWin_gold_1Anim = bigWin_gold_1.animations.add('bigWin_gold_1', [], 10, false);
        bigWin_gold_1Anim.onComplete.add(function () {
            bigWin_gold_1.visible = false;
            bigWin_gold_2 = game.add.sprite(0,0, 'bigWin_gold_2');        
            bigWin_gold_2.visible = true;
            bigWin_soldier_1.visible = true;
            holdSoldier_1.visible = false;
            holdSoldier_2.visible = false;
            bigWin_soldier_1Anim.play();
            setTimeout(function(){  
                bigWin_2sound.play();
            }, 700);
            slotLayer3Group.add(bigWin_gold_2);
            bigWin_gold_2.animations.add('bigWin_gold_2', [], 10, false).play().onComplete.add(function () {
                bigWin_gold_2.visible = false;
                bigWin_gold_3 = game.add.sprite(0,0, 'bigWin_gold_3');
                bigWin_gold_3.visible = true;
                slotLayer3Group.add(bigWin_gold_3);
                bigWin_gold_3.animations.add('bigWin_gold_3', [], 10, false).play().onComplete.add(function () {
                    bigWin_gold_3.visible = false;
                    bigWin_gold_4 = game.add.sprite(0,0, 'bigWin_gold_4');
                    bigWin_gold_4.visible = true;
                    slotLayer3Group.add(bigWin_gold_4);
                    bigWin_gold_4.animations.add('bigWin_gold_4', [], 10, false).play().onComplete.add(function () {
                        bigWin_gold_4.visible = false;
                        bigWin_gold_5 = game.add.sprite(0,0, 'bigWin_gold_5');
                        bigWin_gold_5.visible = true;
                        slotLayer3Group.add(bigWin_gold_5);
                        bigWin_gold_5.animations.add('bigWin_gold_5', [], 10, false).play().onComplete.add(function () {
                            bigWin_gold_5.visible = false;
                            bigWin_gold_6 = game.add.sprite(0,0, 'bigWin_gold_6');
                            bigWin_gold_6.visible = true;
                            slotLayer3Group.add(bigWin_gold_6);
                            bigWin_gold_6.animations.add('bigWin_gold_6', [], 10, false).play().onComplete.add(function () {
                                bigWin_gold_6.visible = false;
                                bigWin_gold_7 = game.add.sprite(0,0, 'bigWin_gold_7');
                                bigWin_gold_7.visible = true;
                                slotLayer3Group.add(bigWin_gold_7);
                                bigWin_gold_7.animations.add('bigWin_gold_7', [], 10, false).play().onComplete.add(function () {
                                    bigWin_gold_7.visible = false;
                                    bigWin_gold_8 = game.add.sprite(384,0, 'bigWin_gold_8');
                                    bigWin_gold_8.visible = true;
                                    slotLayer3Group.add(bigWin_gold_8);                          
                                    bigWin_gold_8.animations.add('bigWin_gold_8', [], 10, false).play().onComplete.add(function () {
                                        bigWin_gold_8.visible = false;
                                        bigWin_gold_9 = game.add.sprite(384,0, 'bigWin_gold_9');
                                        bigWin_gold_9.visible = true;
                                        slotLayer3Group.add(bigWin_gold_9);                          
                                        bigWin_gold_9.animations.add('bigWin_gold_9', [], 10, false).play().onComplete.add(function () {
                                            bigWin_gold_9.visible = false;
                                            bigWin_gold_10 = game.add.sprite(384,0, 'bigWin_gold_10');
                                            bigWin_gold_10.visible = true;
                                            slotLayer3Group.add(bigWin_gold_10);                          
                                            bigWin_gold_10.animations.add('bigWin_gold_10', [], 10, false).play().onComplete.add(function () {
                                                bigWin_gold_10.visible = false;
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
bigWin_soldier_1 = game.add.sprite(0,314+5, 'bigWin_soldier_1');
bigWin_soldier_1.visible = false;
bigWin_soldier_1Anim = bigWin_soldier_1.animations.add('bigWin_soldier_1', [], 10, false);
bigWin_soldier_1Anim.onComplete.add(function () {
    bigWin_soldier_1.visible = false;
    bigWin_soldier_2 = game.add.sprite(0,314+5, 'bigWin_soldier_2');        
    bigWin_soldier_2.visible = true;
    slotLayer3Group.add(bigWin_soldier_2);
    bigWin_soldier_2.animations.add('bigWin_soldier_2', [], 10, false).play().onComplete.add(function () {
        bigWin_soldier_2.visible = false;
        bigWin_soldier_3 = game.add.sprite(252,314+5, 'bigWin_soldier_3');
        bigWin_soldier_3.visible = true;
        bigWin_3sound.play();
        setTimeout(function(){  
            bigWin_4sound.play();
        }, 500);
        slotLayer3Group.add(bigWin_soldier_3);
        bigWin_soldier_3.animations.add('bigWin_soldier_3', [], 10, false).play().onComplete.add(function () {
            bigWin_soldier_3.visible = false;
            bigWin_soldier_4 = game.add.sprite(252,314+5, 'bigWin_soldier_4');
            bigWin_soldier_4.visible = true;
            slotLayer3Group.add(bigWin_soldier_4);
            bigWin_soldier_4.animations.add('bigWin_soldier_4', [], 10, false).play().onComplete.add(function () {
                bigWin_soldier_4.visible = false;
                bigWin_soldier_5 = game.add.sprite(252,314+5, 'bigWin_soldier_5');
                bigWin_soldier_5.visible = true;
                slotLayer3Group.add(bigWin_soldier_5);
                bigWin_soldier_5.animations.add('bigWin_soldier_5', [], 10, false).play().onComplete.add(function () {
                    bigWin_soldier_5.visible = false;
                    bigWin_soldier_6 = game.add.sprite(252,314+5, 'bigWin_soldier_6');
                    bigWin_soldier_6.visible = true;
                    slotLayer3Group.add(bigWin_soldier_6);
                    bigWin_soldier_6.animations.add('bigWin_soldier_6', [], 10, false).play().onComplete.add(function () {
                        bigWin_soldier_6.visible = false;
                        bigWin_soldier_7 = game.add.sprite(0,314+5, 'bigWin_soldier_7');
                        bigWin_soldier_7.visible = true;
                        slotLayer3Group.add(bigWin_soldier_7);
                        bigWin_soldier_7.animations.add('bigWin_soldier_7', [], 10, false).play().onComplete.add(function () {
                            bigWin_soldier_7.visible = false;
                            bigWin_soldier_8 = game.add.sprite(0,314+5, 'bigWin_soldier_8');
                            bigWin_soldier_8.visible = true;
                            slotLayer3Group.add(bigWin_soldier_8);                          
                            bigWin_soldier_8.animations.add('bigWin_soldier_8', [], 10, false).play().onComplete.add(function () {
                                bigWin_soldier_8.visible = false;
                                bigWin_soldier_9 = game.add.sprite(0,314+6, 'bigWin_soldier_9');
                                bigWin_soldier_9.visible = true;
                                slotLayer3Group.add(bigWin_soldier_9);                          
                                bigWin_soldier_9.animations.add('bigWin_soldier_9', [], 10, false).play().onComplete.add(function () {
                                    bigWin_soldier_9.visible = false;
                                    holdSoldier_1.visible = true;
                                    holdSoldier_1.animations.getAnimation('holdSoldier_1').play();
                                    unlockDisplay();
                                    showButtons();
                                    afterFS = false;
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
        //слоты
        cellLayer1Position = [
        [176, 109],
        [176, 261],
        [176, 413],
        [328, 109],
        [328, 261],
        [328, 413],
        [480, 109],
        [480, 261],
        [480, 413],
        [632, 109],
        [632, 261],
        [632, 413],
        [784, 109],
        [784, 261],
        [784, 413]
        ];
        addCell(game, cellLayer1Position, 8);
        addCell2(game, cellLayer1Position, 8);
        
        // линии и номера
        var linePosition = [
        [190,335],
        [190,180],
        [190,485],
        [198,120],
        [193,185],
        [197,180],
        [191,317],
        [191,330],
        [193,180],
        [195,180],
        [195,330],
        [195,170],
        [195,320],
        [195,175],
        [195,335],
        [195,180],
        [195,330],
        [195,185],
        [195,185],
        [195,190]
        ];
        var numberPosition = [
        [[158,283], [158,148], [158,463], [158,103], [158,508], [0,0], [0,0], [158,373], [0,0], [158,238],[0,0], [0,0], [158,418], [0,0], [158,328], [158,193], [0,0], [0,0], [0,0], [0,0]],
        [[0,0], [0,0], [0,0], [0,0], [0,0], [915,148], [915,463], [0,0], [915,283], [0,0],[915,373], [915,238], [0,0], [915,328], [0,0], [0,0], [915,418], [915,508], [915,103], [915,193]]
        ];
        var squarePosition = [
        [177,110],
        [177,262],
        [177,414],
        [329,110],
        [329,262],
        [329,414],
        [481,110],
        [481,262],
        [481,414],
        [633,110],
        [633,262],
        [633,414],
        [785,110],
        [785,262],
        [785,414]
        ]; 
        var hoverSquarePostion = [
        [2,5,8,11,14],
        [1,4,7,10,13],
        [3,6,9,12,15],
        [1,5,9,11,13],
        [3,5,7,11,15],
        [1,4,8,10,13],
        [3,6,8,12,15],
        [2,6,9,12,14],
        [2,4,7,10,14],
        [2,4,8,10,14],
        [2,6,8,12,14],
        [1,5,7,11,13],
        [3,5,9,11,15],
        [2,5,7,11,14],
        [2,5,9,11,14],
        [1,5,8,11,13],
        [3,5,8,11,15],
        [1,5,9,12,15],
        [3,5,7,10,13],
        [1,6,7,12,13]
        ];
        addNumbers(game, numberPosition);
        addLines(game, linePosition);
        addSquares(game, squarePosition);
        addHoverForNumbers(hoverSquarePostion, numberPosition);
        hideLines();
        hideSquares();
        var treeLeavesTopLeft = game.add.sprite(0,0, 'treeLeavesTopLeft');
        var treeLeavesBottomLeft = game.add.sprite(0,456, 'treeLeavesBottomLeft');
        var treeLeavesBottomRight = game.add.sprite(880,475, 'treeLeavesBottomRight');
        var rightAncient = game.add.sprite(931,0, 'rightAncient');
        var leftAncient = game.add.sprite(0,0, 'leftAncient');

        // кнопки
        addButtonsGame1(game);

        //счет
        var betScorePosion = [[120, 638, 20], [300, 711, 15], [460, 711, 15], [625, 711, 15], [840, 711, 15], [885, 638, 20]];
        addBetScore(game, betScorePosion, bet, betline, betLines, coins, coinsValue, win);

        var mainScorePosition = [[285, 745, 15], [518, 745, 15], [685, 745, 15]];
        addMainScore(game, mainScorePosition, cash, mainBet, mainWin);

        multiplierGroup = game.add.group();
        multiplierGroup.add(multiplier_2);
        multiplierGroup.add(xb_2[0]);
        multiplierGroup.add(xb_2[1]);
        multiplierGroup.add(xb_2[2]);
        multiplierGroup.add(xb_2[3]);
        multiplierGroup.add(xo_2[0]);
        multiplierGroup.add(xo_2[1]);
        multiplierGroup.add(xo_2[2]);
        multiplierGroup.add(xo_2[3]);
        multiplierGroup.add(multiplier_top);
        multiplierGroup.add(multiplier_bottom);

        slotLayer1Group = game.add.group();
        cellArray.forEach(function (item) {
            slotLayer1Group.add(item);
        });        
        cellArray2.forEach(function (item) {
            slotLayer1Group.add(item);
        });
        slotLayer1Group.enableBody = true;

        slotLayer2Group = game.add.group();
        squaresArray.forEach(function (item) {
            slotLayer2Group.add(item);
        });
        linesArray.forEach(function (item) {
            slotLayer2Group.add(item);
        });

        treeLeavesGroup = game.add.group();
        treeLeavesGroup.add(rightAncient);
        treeLeavesGroup.add(leftAncient);
        treeLeavesGroup.add(treeLeavesBottomLeft);
        treeLeavesGroup.add(treeLeavesBottomRight);
        var cellPosition = [
        [176+8, 109+8],
        [176+8, 261+8],
        [176+8, 413+8],
        [328+8, 109+8],
        [328+8, 261+8],
        [328+8, 413+8],
        [480+8, 109+8],
        [480+8, 261+8],
        [480+8, 413+8],
        [632+8, 109+8],
        [632+8, 261+8],
        [632+8, 413+8],
        [784+8, 109+8],
        [784+8, 261+8],
        [784+8, 413+8]
        ];
        addCell3(game, cellLayer1Position, 8);
        CellWinAnim(game, cellLayer1Position);
        CellFreeSpinAnim(game, cellPosition);
        BoomAnim(game, cellLayer1Position);
        
        slotLayer3Group = game.add.group();
        slotLayer3Group.add(multiplier);
        slotLayer3Group.add(xb[0]);
        slotLayer3Group.add(xb[1]);
        slotLayer3Group.add(xb[2]);
        slotLayer3Group.add(xb[3]);
        slotLayer3Group.add(xo[0]);
        slotLayer3Group.add(xo[1]);
        slotLayer3Group.add(xo[2]);
        slotLayer3Group.add(xo[3]);
        slotLayer3Group.add(multiWin);
        leftNumbers.forEach(function (item) {
            slotLayer3Group.add(item);
        });
        rightNumbers.forEach(function (item) {
            slotLayer3Group.add(item);
        });
        slotLayer3Group.add(enterSoldier);
        slotLayer3Group.add(holdSoldier_1);
        slotLayer3Group.add(ambient_1);
        slotLayer3Group.add(idle_1);
        slotLayer3Group.add(freeFalls_1);
        slotLayer3Group.add(bigWin_gold_1);
        slotLayer3Group.add(bigWin_soldier_1);

        paytableGroup = game.add.group();
        paytableGroup.add(paytable);
        paytableGroup.add(pagePaytables[1]);
        paytableGroup.add(pagePaytables[2]);
        paytableGroup.add(pagePaytables[3]);
        paytableGroup.add(left_btn);
        paytableGroup.add(center_btn);
        paytableGroup.add(right_btn);

        panelGroup = game.add.group();
        panelGroup.add(buttonPanel);
        panelGroup.add(score);
        panelGroup.add(border_flick);

        scoreGroup = game.add.group();
        scoreGroup.add(betText);
        scoreGroup.add(betLvlText);
        scoreGroup.add(betLineText);
        scoreGroup.add(coinsText);
        scoreGroup.add(coinsValueText);
        scoreGroup.add(winText);
        scoreGroup.add(cashText);
        scoreGroup.add(mainBetText);
        scoreGroup.add(mainWinText);

        buttonGroup = game.add.group();
        buttonGroup.add(bet_level);
        buttonGroup.add(coin_value);
        buttonGroup.add(max_bet);
        buttonGroup.add(start);
        buttonGroup.add(fullButton);
        buttonGroup.add(soundButton);
        buttonGroup.add(settings);
        buttonGroup.add(autoStart);

        treeLeavesGroup2 = game.add.group();
        treeLeavesGroup2.add(treeLeavesTopLeft);
        if (afterFS === true){
            treeLeavesGroup2.add(BlackBG);
        }
        // platforms = game.add.group();


        // platforms.enableBody = true;
        // boom = game.add.sprite(176-124, 109-90, 'boom');
        // boom.animations.add('slot_border_win_1', [0,1,2,3,4,5], 3, true).play();


        // slot_border_win_1 = game.add.sprite(176, 109, 'slot_border_win_1');
        // slot_border_win_2 = game.add.sprite(176, 109, 'slot_border_win_2');
        // slot_border_win_2.visible = false;
        // slot_border_win_1.animations.add('slot_border_win_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 15, false).play().onComplete.add(function(){
        //     slot_border_win_1.visible = false;
        //     slot_border_win_2.visible = true;
        //     slot_border_win_2.animations.add('slot_border_win_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 15, false).play().onComplete.add(function(){
        //     });
        // });
        hideButtons();
        if (afterFS === true){
            setTimeout(function(){
                enterAnim();
            }, 1500);
        } else {            
            setTimeout(function(){
                enterAnim();
                showButtons();
            }, 1000);
        }

    };

    game1.update = function () {
        for (var i = 0; i < 15; i++) {
            // if (flyEnable2[i] == true ){
            //     if (cellTop == 1){
            //         cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 40;  
            //     } else {
            //         cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 40;
            //     }
            // }
            // if (flyEnable[i] == true & firstSpin == true){
            //     if (cellTop == 1){
            //         cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 40;
            //     } else {
            //         cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 40; 
            //     }
            // }
            if (cellTop == 1 & firstSpin == true){
                if (flyEnable[i] == true){
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 1;
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y * 1.1;
                }
                if (flyEnable2[i] == true){
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 1;
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y * 1.1;
                }
            }
            if (cellTop == 2 & firstSpin == true){
                if (flyEnable[i] == true){
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 1;
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y * 1.1;
                }
                if (flyEnable2[i] == true){
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 1;
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y * 1.1;
                }
            }
        }
        for (var i = 0; i < 15; i++) {
            if(cellTop == 2 & flyEnable2[i] == true ){ 
                if (cellArray2[i].position.y >= cellLayer1Position[i][1]){
                    flyEnable[i] = false;
                    flyEnable2[i] = false;
                    cellArray2[i].body.velocity.y = 0;
                    cellArray2[i].position.y = cellLayer1Position[i][1];                 
                    cellArray[i].body.velocity.y = 0;
                    rotateCell(cellArray2[i], i);
                    if (flyEnable2[12] == false){
                        cellTop = 1;                                     
                        console.log('Первый массив на верху'); 
                    }
                }
            } 
            if(cellTop == 1 & flyEnable[i] == true){  
                if (cellArray[i].position.y >= cellLayer1Position[i][1]){
                    flyEnable[i] = false;
                    flyEnable2[i] = false;
                    cellArray[i].body.velocity.y = 0;
                    cellArray[i].position.y = cellLayer1Position[i][1];
                    cellArray2[i].body.velocity.y = 0;
                    rotateCell(cellArray[i], i);
                    if (flyEnable[12] == false){
                        cellTop = 2; 
                        console.log('Второй массив на верху');  
                    }
                }
            }
        }   
        for (var i = 0; i < 15; i++) {
            if (flyEnable3[i] > 0 & cellTop == 2 & flyEnable[i] == true){
                cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 40; 
            }
            if (flyEnable3[i] > 0 & cellTop == 1 & flyEnable2[i] == true){
                cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 40;
            }
        }
        for (var i = 0; i < 15; i++) {
            if (flyEnable[i] == true & firstSpin == false & cellTop == 1){
                cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 40; 
            }
            if (flyEnable2[i] == true & firstSpin == false & cellTop == 2){
                cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 40;
            }
        }
        for (var i = 0; i < 15; i++) {
            if(cellTop == 2){ 
                if(flyEnable3[i] === 1 & flyEnable[i] == true){
                    if (cellArray[i].position.y >= cellLayer1Position[i+1][1]){
                        flyEnable[i] = false;                        
                        cellArray[i].body.velocity.y = 0;
                        cellArray[i].position.y = cellLayer1Position[i+1][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                } else if(flyEnable3[i] === 2 & flyEnable[i] == true){
                    if (cellArray[i].position.y >= cellLayer1Position[i+2][1]){
                        flyEnable[i] = false;
                        cellArray[i].body.velocity.y = 0;
                        cellArray[i].position.y = cellLayer1Position[i+2][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                }
            } else {
                if(flyEnable3[i] === 1 & flyEnable2[i] == true){
                    if (cellArray2[i].position.y >= cellLayer1Position[i+1][1]){
                        flyEnable2[i] = false;
                        cellArray2[i].body.velocity.y = 0;
                        cellArray2[i].position.y = cellLayer1Position[i+1][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray2[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                } else if(flyEnable3[i] === 2 & flyEnable2[i] == true){
                    if (cellArray2[i].position.y >= cellLayer1Position[i+2][1]){
                        flyEnable2[i] = false;
                        cellArray2[i].body.velocity.y = 0;
                        cellArray2[i].position.y = cellLayer1Position[i+2][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray2[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                }
            }
        } 
    };
    game.state.add('game1', game1);
};
