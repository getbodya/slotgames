var stepTotalWinR = 0;

function showWin(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y, win, { font: '30px \"Impact\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

    // var timeInterval = 450;
    // var textCounter = setInterval(function () {
    //     text.position.y -= 3;
    // }, 10);

    // setTimeout(function() {
    //     clearInterval(textCounter);
    // }, timeInterval);

    number_win = game.add.audio('number_win');
    number_win.play();
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1500);
    }, timeInterval);
}






function game3() {
    (function () {

        var game3 = {};

        game3.preload = function () {};

        game3.create = function () {

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            //звуки
            // winRope1 = game.add.audio('game.winRope1');
            // winRope2 = game.add.audio('game.winRope2');
            // bitMonkey = game.add.audio('game.bitMonkey');
            
            // hitSound = game.add.audio('hit');
            // hitSound.addMarker('hits', 0.3, 5);     
            hit_game3 = game.add.audio('hit_game3');
            lose_game3 = game.add.audio('lose_game3');
            //изображения
            background = game.add.sprite(94,22, 'topScoreGame4');
            background = game.add.sprite(94,54, 'game.backgroundGame3');

            game.bear_1 = game.add.sprite(0+(1-1)*113, 361, 'game4.bear_hit_1');
            game.bear_1.visible = false;
            game.bear2_1 = game.add.sprite(0+(1-1)*113, 361, 'game4.bear_hit_2');
            game.bear2_1.visible = false;
            game.hive_win_1 = game.add.sprite(158+(1-1)*113, 246, 'game4.hive_win');
            game.hive_win_1.visible = false;
            game.hive_lose_1 = game.add.sprite(158+(1-1)*113, 246, 'game4.hive_lose');
            game.hive_lose_1.visible = false;
            game.bear_win_1 = game.add.sprite(0+(1-1)*113, 361, 'game4.bear_win');
            game.bear_win_1.visible = false;
            game.bear_lose_1 = game.add.sprite(0+(1-1)*113, 361, 'game4.bear_lose');
            game.bear_lose_1.visible = false;

            background = game.add.sprite(0,0, 'game.black_bg');
            background = game.add.sprite(0,0, 'game.background');

            // rope1 = game.add.sprite(263,55, 'game.rope');
            // rope2 = game.add.sprite(353,55, 'game.rope');
            // rope3 = game.add.sprite(443,55, 'game.rope');
            // rope4 = game.add.sprite(533,55, 'game.rope');
            // rope5 = game.add.sprite(623,55, 'game.rope');

            // for(var i in gnomePositionsX) {
            //     game.lever_arr[i] = game.add.sprite(94+((i-1)/2)*128, 390, 'game.lever'+i);
            //     game.lever_arr[i].visible = false;
            //     game.chest_open_arr[i] = game.add.sprite(94+((i-1)/2)*128, 54, 'game.chest_open');
            //     game.chest_open_arr[i].visible = false;
            // } 

            //счет
            scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);
            
            
            //кнопки

            var selectGame = game.add.sprite(70,510, 'selectGame_d');
            selectGame.scale.setTo(0.7, 0.7);
            selectGame.inputEnabled = false;

            var payTable = game.add.sprite(150,510, 'payTable_d');
            payTable.scale.setTo(0.7, 0.7);
            payTable.inputEnabled = false;

            var betone = game.add.sprite(490,510, 'betone_d');
            betone.scale.setTo(0.7, 0.7);
            betone.inputEnabled = false;


            var betmax = game.add.sprite(535,510, 'betmax_d');
            betmax.scale.setTo(0.7, 0.7);
            betmax.inputEnabled = false;

            var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
            automaricstart.scale.setTo(0.7, 0.7);
            automaricstart.inputEnabled = false;

            var startButton = game.add.sprite(597, 510, 'startButton_d');
            startButton.scale.setTo(0.7,0.7);
            startButton.inputEnabled = false;

            var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
            buttonLine1.scale.setTo(0.7,0.7);
            buttonLine1.inputEnabled = true;
            buttonLine1.input.useHandCursor = true;

            var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
            buttonLine3.scale.setTo(0.7,0.7);
            buttonLine3.inputEnabled = true;
            buttonLine3.input.useHandCursor = true;

            var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
            buttonLine5.scale.setTo(0.7,0.7);
            buttonLine5.inputEnabled = true;
            buttonLine5.input.useHandCursor = true;

            var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
            buttonLine7.scale.setTo(0.7,0.7);
            buttonLine7.inputEnabled = true;
            buttonLine7.input.useHandCursor = true;

            var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
            buttonLine9.scale.setTo(0.7,0.7);
            buttonLine9.inputEnabled = true;
            buttonLine9.input.useHandCursor = true;

            buttonLine1.events.onInputOver.add(function(){
                if(buttonLine1.inputEnabled == true) {
                    buttonLine1.loadTexture('buttonLine1_p');
                }
            });
            buttonLine1.events.onInputOut.add(function(){
                if(buttonLine1.inputEnabled == true) {
                    buttonLine1.loadTexture('buttonLine1');
                }
            });

            buttonLine3.events.onInputOver.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3_p');
                }
            });
            buttonLine3.events.onInputOut.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3');
                }
            });

            buttonLine5.events.onInputOver.add(function(){
                if(buttonLine5.inputEnabled == true) {
                    buttonLine5.loadTexture('buttonLine5_p');
                }
            });
            buttonLine5.events.onInputOut.add(function(){
                if(buttonLine5.inputEnabled == true) {
                    buttonLine5.loadTexture('buttonLine5');
                }
            });

            buttonLine7.events.onInputOver.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7_p');
                }
            });
            buttonLine7.events.onInputOut.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7');
                }
            });

            buttonLine9.events.onInputOver.add(function(){
                if(buttonLine9.inputEnabled == true) {
                    buttonLine9.loadTexture('buttonLine9_p');
                }
            });
            buttonLine9.events.onInputOut.add(function(){
                if(buttonLine9.inputEnabled == true) {
                    buttonLine9.loadTexture('buttonLine9');
                }
            });

            buttonLine1.events.onInputDown.add(function(){
                takeBananaRope(62,22,1);

                buttonLine1.loadTexture('buttonLine1_d');
                buttonLine1.inputEnabled = false;
                buttonLine1.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) {
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 200,274 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine3.events.onInputDown.add(function(){
                takeBananaRope(190,22,2);

                buttonLine3.loadTexture('buttonLine3_d');
                buttonLine3.inputEnabled = false;
                buttonLine3.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 200+113,274 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine5.events.onInputDown.add(function(){
                takeBananaRope(318,22,3);

                buttonLine5.loadTexture('buttonLine5_d');
                buttonLine5.inputEnabled = false;
                buttonLine5.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 200+113+113,274 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine7.events.onInputDown.add(function(){
                takeBananaRope(446,22,4);

                buttonLine7.loadTexture('buttonLine7_d');
                buttonLine7.inputEnabled = false;
                buttonLine7.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 200+113+113+113,274 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine9.events.onInputDown.add(function(){
                takeBananaRope(574,22,5);

                buttonLine9.loadTexture('buttonLine9_d');
                buttonLine9.inputEnabled = false;
                buttonLine9.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                setTimeout(showWin,2500, 200+113+113+113+113,274 , ropeValues[ropeStep], stepTotalWinR);
            });
            
            //анимации и логика
            game.chicken = game.add.sprite(286, 246, 'game3.chicken');
            game.chicken.animations.add('game3.chicken', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31], 8, true).play();
            game.smoke = game.add.sprite(350, 182, 'game3.smoke');
            game.smoke.animations.add('game3.smoke', [0,1,2,3,4,5,6,7], 8, true).play();
            game.bear_wait = game.add.sprite(94, 326, 'game4.bear_wait');
            game.bear_wait.animations.add('game4.bear_wait', [0,1,2,3,3,3,3,3,2,1,4,5,6,7,8,9,8,7,8,9,8,7,6,5,4,10,11,12,13,14,15,16,17,18,19,20,21,22,23], 8, true).play();
            
            game.hive_wait_arr = [];
            for (var i = 1; i <= 5; ++i) {
                game.hive_wait_arr[i] = game.add.sprite(142+(i-1)*113, 246, 'game4.hive_wait');
                game.hive_wait_arr[i].animations.add('game4.hive_wait', [0,1,2,3,4,5,6,7], 8, true).play();
                game.add.sprite(179+(i-1)*113, 422, 'game4.bee_foot');
            }
            function hideBear3(){
                game.bear_wait.visible = false;
            }

            var ropeStep = 0;
            //часть логики занесена в анимацию, так как в разных игра она может отличаться для данной бонусной игры
            //0 - банан, 1 - кувалда, 2 - кирпич
            var win = 0;
            var ropeStep = 0;
            var checkUpLvl = true; //проверка доступа ко второй бонусной игре

            function takeBananaRope(x,y,lineNumber){
                lockDisplay();
                hideBear3();
                if (lineNumber == 1) {
                    game.bear_1.visible = true;
                    game.bear_1.animations.add('game4.bear_hit_1', [0,1,2,3,4,5,6,7,8,9], 8, false).play().onComplete.add(function(){
                        hit_game3.play();
                        game.bear_1.visible = false;
                        game.bear2_1.visible = true;
                        game.hive_wait_arr[lineNumber].visible = false;
                        if(ropeValues[ropeStep] > 0) {
                            game.hive_win_1.visible = true;
                            game.hive_win_1.animations.add('game4.hive_win', [0,1,2,3,4,5,6,7], 8, false).play();
                        } else {
                            game.hive_lose_1.visible = true;
                            game.hive_lose_1.animations.add('game4.hive_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play();
                        }
                        game.bear2_1.animations.add('game4.bear_hit_2', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
                            game.bear2_1.visible = false;   
                            if(ropeValues[ropeStep] > 0) {
                                game.bear_win_1.visible = true;
                                game.bear_win_1.animations.add('game4.bear_win', [0,1,2,3,4,5,6], 8, false).play().onComplete.add(function(){
                                    game.bear_win_1.visible = false;
                                    game.bear_wait.visible = true;
                                    if(ropeStep == 5 && checkUpLvl == true) {
                                        setTimeout("game.state.start('game4');", 1000);
                                    } else if(ropeStep == 5) {
                                        updateBalanceGame3(game, scorePosions, balanceR);
                                    }
                                });
                            } else {
                                lose_game3.play();
                                game.bear_lose_1.visible = true;
                                game.bear_lose_1.animations.add('game4.bear_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                                    updateBalanceGame3(game, scorePosions, balanceR);
                                });
                            }
                            ropeStep += 1;
                        });
                    });
                } else {
                    game.bear = game.add.sprite(0+(lineNumber-1)*113, 361, 'game4.bear_hit_1');
                    game.bear.visible = true;
                    game.bear.animations.add('game4.bear_hit_1', [0,1,2,3,4,5,6,7,8,9], 8, false).play().onComplete.add(function(){
                        hit_game3.play();
                        game.bear.visible = false;
                        game.bear2 = game.add.sprite(0+(lineNumber-1)*113, 361, 'game4.bear_hit_2');
                        game.bear2.visible = true;
                        game.hive_wait_arr[lineNumber].visible = false;
                        if(ropeValues[ropeStep] > 0) {
                            game.hive_win = game.add.sprite(158+(lineNumber-1)*113, 246, 'game4.hive_win');
                            game.hive_win.animations.add('game4.hive_win', [0,1,2,3,4,5,6,7], 8, false).play();
                        } else {
                            game.hive_lose = game.add.sprite(158+(lineNumber-1)*113, 246, 'game4.hive_lose');
                            game.hive_lose.animations.add('game4.hive_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play();
                        }
                        game.bear2.animations.add('game4.bear_hit_2', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
                            game.bear2.visible = false;   
                            if(ropeValues[ropeStep] > 0) {
                                game.bear_win = game.add.sprite(0+(lineNumber-1)*113, 361, 'game4.bear_win');
                                game.bear_win.animations.add('game4.bear_win', [0,1,2,3,4,5,6], 8, false).play().onComplete.add(function(){
                                    game.bear_win.visible = false;
                                    game.bear_wait.visible = true;
                                    if(ropeStep == 5 && checkUpLvl == true) {
                                        setTimeout("game.state.start('game4');", 1000);
                                    } else if(ropeStep == 5) {
                                        updateBalanceGame3(game, scorePosions, balanceR);
                                    }
                                });
                            } else {
                                game.bear_lose = game.add.sprite(0+(lineNumber-1)*113, 361, 'game4.bear_lose');
                                lose_game3.play();
                                game.bear_lose.animations.add('game4.bear_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                                    updateBalanceGame3(game, scorePosions, balanceR);
                                });
                            }
                            ropeStep += 1;
                        });
                    });
                }

                setTimeout("unlockDisplay();", 3200);
            }


            full_and_sound();


        };

        game3.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game3', game3);

})();

}