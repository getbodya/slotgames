function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем переменные с других экранов
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        // background = game.add.sprite(0,0, 'game.background');
        background1 = game.add.sprite(94-mobileX,54-mobileY, 'game.background1');
        topBarImage2 = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame3');
        backgroundGame3 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame2');

        addTableTitle(game, 'winTitleGame2', 239-mobileX,359-mobileY);
        hideTableTitle();


        //счет
        step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [485-mobileX, 59-mobileY, 24]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[124-mobileX,118-mobileY], [257-mobileX,118-mobileY], [369-mobileX,118-mobileY], [481-mobileX,118-mobileY], [593-mobileX,118-mobileY]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация
        fireplace = game.add.sprite(318-mobileX, 390-mobileY, 'fireplace');
        fireplace.animations.add('fireplace', [0,1,2,3], 6, true).play();

        function showGnome(){
            game1.gnome_anim1_1 = game.add.sprite(94-mobileX, 326-mobileY, 'gnome_anim1_1');
            game1.gnome_anim1_1Animation = game1.gnome_anim1_1.animations.add('gnome_anim1_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
            game1.gnome_anim1_1Animation.play();
            game1.gnome_anim1_2 = game.add.sprite(94-mobileX, 326-mobileY, 'gnome_anim1_2'); 
            game1.gnome_anim1_2Animation = game1.gnome_anim1_2.animations.add('gnome_anim1_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
            game1.gnome_anim1_2.visible = false;
            game1.gnome_anim1_1Animation.onComplete.add(function(){
                game1.gnome_anim1_2Animation.play();
                game1.gnome_anim1_1.visible = false;
                game1.gnome_anim1_2.visible = true;
            });
            game1.gnome_anim1_2Animation.onComplete.add(function(){
                game1.gnome_anim1_1Animation.play();
                game1.gnome_anim1_1.visible = true;
                game1.gnome_anim1_2.visible = false;
            });
        };

        showGnome();
        

        showDoubleToAndTakeOrRiskTexts(game, totalWin, 340-mobileX, 360-mobileY);

        // кнопки
        addButtonsGame2Mobile(game);

    };

    game2.update = function () {

    };

    game.state.add('game2', game2);

    // звуки
}