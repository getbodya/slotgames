(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        game.load.image('startButton', needUrlPath + '/img/image1445.png');
        game.load.image('startButton_p', needUrlPath + '/img/image1447.png');
        game.load.image('startButton_d', needUrlPath + '/img/image1451.png');
        game.load.image('selectGame', needUrlPath + '/img/image1419.png');
        game.load.image('selectGame_p', needUrlPath + '/img/image1421.png');
        game.load.image('selectGame_d', needUrlPath + '/img/image1425.png');
        game.load.image('payTable', needUrlPath + '/img/image1428.png');
        game.load.image('payTable_p', needUrlPath + '/img/image1430.png');
        game.load.image('payTable_d', needUrlPath + '/img/image1433.png');
        game.load.image('automaricstart', needUrlPath + '/img/image1436.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/image1438.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/image1442.png');
        game.load.image('betone', needUrlPath + '/img/image1471.png');
        game.load.image('betone_p', needUrlPath + '/img/image1473.png');
        game.load.image('betone_d', needUrlPath + '/img/image1477.png');
        game.load.image('betmax', needUrlPath + '/img/image1480.png');
        game.load.image('betmax_p', needUrlPath + '/img/image1482.png');
        game.load.image('betmax_d', needUrlPath + '/img/image1485.png');
        game.load.image('buttonLine1', needUrlPath + '/img/image1505.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/image1507.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/image1511.png');
        game.load.image('buttonLine3', needUrlPath + '/img/image1496.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/image1498.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/image1502.png');
        game.load.image('buttonLine5', needUrlPath + '/img/image1488.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/image1490.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/image1493.png');
        game.load.image('buttonLine7', needUrlPath + '/img/image1462.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/image1464.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/image1468.png');
        game.load.image('buttonLine9', needUrlPath + '/img/image1454.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/image1456.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/image1459.png');

        game.load.image('game.number1', needUrlPath + '/img/1.png');
        game.load.image('game.number2', needUrlPath + '/img/2.png');
        game.load.image('game.number3', needUrlPath + '/img/3.png');
        game.load.image('game.number4', needUrlPath + '/img/4.png');
        game.load.image('game.number5', needUrlPath + '/img/5.png');
        game.load.image('game.number6', needUrlPath + '/img/6.png');
        game.load.image('game.number7', needUrlPath + '/img/7.png');
        game.load.image('game.number8', needUrlPath + '/img/8.png');
        game.load.image('game.number9', needUrlPath + '/img/9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.image('line1', needUrlPath + '/lines/select/1.png');
        game.load.image('line2', needUrlPath + '/lines/select/2.png');
        game.load.image('line3', needUrlPath + '/lines/select/3.png');
        game.load.image('line4', needUrlPath + '/lines/select/4.png');
        game.load.image('line5', needUrlPath + '/lines/select/5.png');
        game.load.image('line6', needUrlPath + '/lines/select/6.png');
        game.load.image('line7', needUrlPath + '/lines/select/7.png');
        game.load.image('line8', needUrlPath + '/lines/select/8.png');
        game.load.image('line9', needUrlPath + '/lines/select/9.png');

        game.load.image('linefull1', needUrlPath + '/lines/win/1.png');
        game.load.image('linefull2', needUrlPath + '/lines/win/2.png');
        game.load.image('linefull3', needUrlPath + '/lines/win/3.png');
        game.load.image('linefull4', needUrlPath + '/lines/win/4.png');
        game.load.image('linefull5', needUrlPath + '/lines/win/5.png');
        game.load.image('linefull6', needUrlPath + '/lines/win/6.png');
        game.load.image('linefull7', needUrlPath + '/lines/win/7.png');
        game.load.image('linefull8', needUrlPath + '/lines/win/8.png');
        game.load.image('linefull9', needUrlPath + '/lines/win/9.png');

        game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sound/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sound/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sound/stop.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('play', needUrlPath + '/sound/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sound/winLines/sound19.mp3');

        //карты
        game.load.image('card_bg', needUrlPath + '/img/shirt_cards.png');

        game.load.image('card_39', needUrlPath + '/img/cards/2b.png');
        game.load.image('card_40', needUrlPath + '/img/cards/3b.png');
        game.load.image('card_41', needUrlPath + '/img/cards/4b.png');
        game.load.image('card_42', needUrlPath + '/img/cards/5b.png');
        game.load.image('card_43', needUrlPath + '/img/cards/6b.png');
        game.load.image('card_44', needUrlPath + '/img/cards/7b.png');
        game.load.image('card_45', needUrlPath + '/img/cards/8b.png');
        game.load.image('card_46', needUrlPath + '/img/cards/9b.png');
        game.load.image('card_47', needUrlPath + '/img/cards/10b.png');
        game.load.image('card_48', needUrlPath + '/img/cards/11b.png');
        game.load.image('card_49', needUrlPath + '/img/cards/12b.png');
        game.load.image('card_50', needUrlPath + '/img/cards/13b.png');
        game.load.image('card_51', needUrlPath + '/img/cards/14b.png');

        game.load.image('card_26', needUrlPath + '/img/cards/2c.png');
        game.load.image('card_27', needUrlPath + '/img/cards/3c.png');
        game.load.image('card_28', needUrlPath + '/img/cards/4c.png');
        game.load.image('card_29', needUrlPath + '/img/cards/5c.png');
        game.load.image('card_30', needUrlPath + '/img/cards/6c.png');
        game.load.image('card_31', needUrlPath + '/img/cards/7c.png');
        game.load.image('card_32', needUrlPath + '/img/cards/8c.png');
        game.load.image('card_33', needUrlPath + '/img/cards/9c.png');
        game.load.image('card_34', needUrlPath + '/img/cards/10c.png');
        game.load.image('card_35', needUrlPath + '/img/cards/11c.png');
        game.load.image('card_36', needUrlPath + '/img/cards/12c.png');
        game.load.image('card_37', needUrlPath + '/img/cards/13c.png');
        game.load.image('card_38', needUrlPath + '/img/cards/14c.png');

        game.load.image('card_0', needUrlPath + '/img/cards/2t.png');
        game.load.image('card_1', needUrlPath + '/img/cards/3t.png');
        game.load.image('card_2', needUrlPath + '/img/cards/4t.png');
        game.load.image('card_3', needUrlPath + '/img/cards/5t.png');
        game.load.image('card_4', needUrlPath + '/img/cards/6t.png');
        game.load.image('card_5', needUrlPath + '/img/cards/7t.png');
        game.load.image('card_6', needUrlPath + '/img/cards/8t.png');
        game.load.image('card_7', needUrlPath + '/img/cards/9t.png');
        game.load.image('card_8', needUrlPath + '/img/cards/10t.png');
        game.load.image('card_9', needUrlPath + '/img/cards/11t.png');
        game.load.image('card_10', needUrlPath + '/img/cards/12t.png');
        game.load.image('card_11', needUrlPath + '/img/cards/13t.png');
        game.load.image('card_12', needUrlPath + '/img/cards/14t.png');

        game.load.image('card_13', needUrlPath + '/img/cards/2p.png');
        game.load.image('card_14', needUrlPath + '/img/cards/3p.png');
        game.load.image('card_15', needUrlPath + '/img/cards/4p.png');
        game.load.image('card_16', needUrlPath + '/img/cards/5p.png');
        game.load.image('card_17', needUrlPath + '/img/cards/6p.png');
        game.load.image('card_18', needUrlPath + '/img/cards/7p.png');
        game.load.image('card_19', needUrlPath + '/img/cards/8p.png');
        game.load.image('card_20', needUrlPath + '/img/cards/9p.png');
        game.load.image('card_21', needUrlPath + '/img/cards/10p.png');
        game.load.image('card_22', needUrlPath + '/img/cards/11p.png');
        game.load.image('card_23', needUrlPath + '/img/cards/12p.png');
        game.load.image('card_24', needUrlPath + '/img/cards/13p.png');
        game.load.image('card_25', needUrlPath + '/img/cards/14p.png');

        game.load.image('card_52', needUrlPath + '/img/cards/joker.png');

        //game.load.image('pick', needUrlPath + '/img/cards/shape340.png');

        game.load.image('cell0', needUrlPath + '/img/cell0.png');
        game.load.image('cell1', needUrlPath + '/img/cell1.png');
        game.load.image('cell2', needUrlPath + '/img/cell2.png');
        game.load.image('cell3', needUrlPath + '/img/cell3.png');
        game.load.image('cell4', needUrlPath + '/img/cell4.png');
        game.load.image('cell5', needUrlPath + '/img/cell5.png');
        game.load.image('cell6', needUrlPath + '/img/cell6.png');
        game.load.image('cell7', needUrlPath + '/img/cell7.png');
        game.load.image('cell8', needUrlPath + '/img/cell8.png');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.png', 96, 112);
        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/bonus.png', 96, 96); //текущее кол-во кадров = 11

        game.load.image('play1To', needUrlPath + '/img/play1To.png');
        game.load.image('bonusGame', needUrlPath + '/img/bonusGame.png');
        game.load.image('takeOrRisk1', needUrlPath + '/img/takeOrRisk1.png');
        game.load.image('take', needUrlPath + '/img/take.png');

        game.load.image('winTitleGame2', needUrlPath + '/img/winTitleGame2.png');
        game.load.image('loseTitleGame2', needUrlPath + '/img/loseTitleGame2.png');
        game.load.image('forwadTitleGame2', needUrlPath + '/img/forwadTitleGame2.png');

        game.load.image('topBarType1', needUrlPath + '/img/topBarType1.png');
        game.load.image('topBarType2', needUrlPath + '/img/topBarType2.png');
        game.load.image('game.background', needUrlPath + '/img/canvas-bg.svg');

        game.load.audio('winMonkey', needUrlPath + '/sound/winMonkey.mp3');
        game.load.audio('openCard', needUrlPath + '/sound/sound31.mp3');
        game.load.audio('winCard', needUrlPath + '/sound/sound30.mp3');


        /* sources */

        if(!isMobile){
            game.load.image('main_window', needUrlPath + '/img/shape1206.svg');
        }
        game.load.image('game1.bg', needUrlPath + '/img/main_bg.png');
        game.load.image('game2.bg', needUrlPath + '/img/bg_game2.png');
        game.load.image('game3.bg', needUrlPath + '/img/game3_bg.png');
        game.load.image('game4.bg', needUrlPath + '/img/bg_game4.png');
        game.load.image('bg_top1', needUrlPath + '/img/main_bg_top1.png');
        game.load.image('bg_top2', needUrlPath + '/img/main_bg_top2.png');
        game.load.image('bg_top3', needUrlPath + '/img/main_bg_top3.png');
        game.load.image('bg_top4', needUrlPath + '/img/main_bg_top4.png');
        game.load.image('bg_top3_2', needUrlPath + '/img/main_bg_top3_2.png');
        game.load.image('bg_3_2', needUrlPath + '/img/bg_3_2.png');
        game.load.image('game1.bottom_line', needUrlPath + '/img/bottom_line.png');
        game.load.image('x', needUrlPath + '/img/x.png');
        
        game.load.audio('page', needUrlPath + '/sounds/page.mp3');
        game.load.image('prev_page', needUrlPath + '/img/prev_page.png');
        game.load.image('exit_btn', needUrlPath + '/img/exit_btn.png');
        game.load.image('next_page', needUrlPath + '/img/next_page.png');
        for (var i = 1; i <= 5; ++i) {
            game.load.image('pagePaytable_' + i, needUrlPath + '/img/page_' + i + '.png');            
        }

        game.load.spritesheet('bonus', needUrlPath + '/img/bonus.png', 96, 96, 8);
        game.load.spritesheet('game1.pirate_win', needUrlPath + '/img/pirate_win.png', 224, 144, 18);

        game.load.spritesheet('bird_anim_1', needUrlPath + '/img/bird_anim_1_80x96_45.png', 80, 96, 45);
        game.load.spritesheet('bird_anim_2', needUrlPath + '/img/bird_anim_2_80x96_22.png', 80, 96, 22);
        game.load.spritesheet('water_anim', needUrlPath + '/img/water_anim_192x16_4.png', 192, 16, 4);
        game.load.spritesheet('water_bottom_1', needUrlPath + '/img/water_bottom_1_368x32_10.png', 368, 32, 10);
        game.load.spritesheet('water_bottom_2', needUrlPath + '/img/water_bottom_2_368x32_4.png', 368, 32, 4);
        game.load.spritesheet('game1.man_1', needUrlPath + '/img/game1.man_1_368_112_11.png', 368, 112, 11);
        game.load.spritesheet('game1.win', needUrlPath + '/img/game1.win_368_112_6.png', 368, 112, 6);

        game.load.image('game1.bar', needUrlPath + '/img/shape_fullv3.png');
        game.load.image('dice_spin', needUrlPath + '/img/dice_spin.png');
        game.load.image('border_card', needUrlPath + '/img/border.png');

        game.load.image('shirt_cards', needUrlPath + '/img/shirt_cards.png');
        // game.load.image('dealer', needUrlPath + '/img/dealer.png');
        game.load.image('pick_card', needUrlPath + '/img/pick_game2.png');
        game.load.image('card_15', needUrlPath + '/img/cards/joker.png');
        game.load.image('card_2b', needUrlPath + '/img/cards/2b.png');
        game.load.image('card_5', needUrlPath + '/img/cards/5b.png');
        game.load.image('card_2c', needUrlPath + '/img/cards/2c.png');
        game.load.image('card_7', needUrlPath + '/img/cards/7c.png');
        game.load.image('card_2p', needUrlPath + '/img/cards/2p.png');
        game.load.image('card_12', needUrlPath + '/img/cards/12p.png');
        game.load.image('card_2t', needUrlPath + '/img/cards/2t.png');
        game.load.image('card_13', needUrlPath + '/img/cards/13t.png');

        game.load.image('shadow1', needUrlPath + '/img/shadow1.png');
        game.load.image('shadow2', needUrlPath + '/img/shadow2.png');
        game.load.image('shadow3', needUrlPath + '/img/shadow3.png');
        game.load.spritesheet('game3.meat', needUrlPath + '/img/meat_64x256_12.png', 64, 256, 12);
        game.load.spritesheet('game3.pick', needUrlPath + '/img/game3.pick_192x256_13.png', 192, 256, 13);
        game.load.spritesheet('game3.get_meat_star', needUrlPath + '/img/get_meat_star_192x416_6.png', 192, 416, 6);
        game.load.spritesheet('game3.get_meat_win', needUrlPath + '/img/get_meat_win_192x416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_1', needUrlPath + '/img/get_meat_lose_1_192_416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_2', needUrlPath + '/img/get_meat_lose_2_240x432_16.png', 240, 432, 16);

        game.load.spritesheet('game4.arrow', needUrlPath + '/img/arrow_64x48_5.png', 64, 48, 5);
        game.load.spritesheet('game4.captain_lose', needUrlPath + '/img/captain_lose_128x112_10.png', 128, 112, 10);
        game.load.spritesheet('game4.captain_pick', needUrlPath + '/img/captain_pick_128x112_25.png', 128, 112, 25);
        game.load.spritesheet('game4.captain_swim_1', needUrlPath + '/img/captain_swim_1_128x112_19.png', 128, 112, 19);
        game.load.spritesheet('game4.captain_swim_2', needUrlPath + '/img/captain_swim_2_256x160_6.png', 256, 160, 6);
        game.load.spritesheet('game4.captain_under', needUrlPath + '/img/captain_under_224x80_6.png', 224, 80, 6);
        game.load.spritesheet('game4.captain_win', needUrlPath + '/img/captain_win_128x112_5.png', 128, 112, 5);
        game.load.spritesheet('game4.dolphin', needUrlPath + '/img/dolphin_128х128_13.png', 128, 128, 13);
        game.load.spritesheet('game4.shark', needUrlPath + '/img/shark_128х144_13.png', 128, 144, 13);
        game.load.spritesheet('game4.fin', needUrlPath + '/img/fin_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.fin_left_end', needUrlPath + '/img/fin_left_end_128x64_6.png', 128, 64, 6);
        game.load.spritesheet('game4.fin_right_end', needUrlPath + '/img/fin_right_end_128x64_7.png', 128, 64, 7);
        game.load.spritesheet('game4.fin_end', needUrlPath + '/img/fin_left_end_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.water', needUrlPath + '/img/water_640x224_6.png', 640, 224, 6);

        game.load.spritesheet('game2.win_lose', needUrlPath + '/img/game2_win_lose.png', 112, 96, 5);

        game.load.audio('stop', needUrlPath + '/sounds/stop.wav');
        game.load.audio('rotate', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('takeWin', needUrlPath + '/sounds/takeWin.mp3');
        game.load.audio('pickCard', needUrlPath + '/sounds/pickCard.mp3');
        game.load.audio('cardWin', needUrlPath + '/sounds/cardWin.mp3');
        game.load.audio('hit', needUrlPath + '/sounds/hit.mp3');
        game.load.audio('winGame3', needUrlPath + '/sounds/winGame3.mp3');
        game.load.audio('rowWin', needUrlPath + '/sounds/rowWin.mp3');
        game.load.audio('page', needUrlPath + '/sounds/page.mp3');

        game.load.audio('rotate', needUrlPath + '/sounds/rotate.wav');

        game.load.image('winTryInGame3', needUrlPath + '/img/winTryInGame3.png');

        game.load.image('game1.bg', needUrlPath + '/img/main_bg.png');
        game.load.image('game2.bg', needUrlPath + '/img/bg_game2.png');
        game.load.image('game3.bg', needUrlPath + '/img/game3_bg.png');
        game.load.image('game4.bg', needUrlPath + '/img/bg_game4.png');
        game.load.image('bg_top1', needUrlPath + '/img/main_bg_top1.png');
        game.load.image('bg_top2', needUrlPath + '/img/main_bg_top2.png');
        game.load.image('bg_top3', needUrlPath + '/img/main_bg_top3.png');
        game.load.image('bg_top4', needUrlPath + '/img/main_bg_top4.png');
        game.load.image('bg_top3_2', needUrlPath + '/img/main_bg_top3_2.png');
        game.load.image('bg_3_2', needUrlPath + '/img/bg_3_2.png');
        game.load.image('game1.bottom_line', needUrlPath + '/img/bottom_line.png');
        game.load.image('x', needUrlPath + '/img/x.png');

        game.load.image('prev_page', needUrlPath + '/img/prev_page.png');
        game.load.image('exit_btn', needUrlPath + '/img/exit_btn.png');
        game.load.image('next_page', needUrlPath + '/img/next_page.png');

        game.load.spritesheet('bonus', needUrlPath + '/img/bonus.png', 96, 96, 8);
        game.load.spritesheet('game1.pirate_win', needUrlPath + '/img/pirate_win.png', 224, 144, 18);

        game.load.spritesheet('bird_anim_1', needUrlPath + '/img/bird_anim_1_80x96_45.png', 80, 96, 45);
        game.load.spritesheet('bird_anim_2', needUrlPath + '/img/bird_anim_2_80x96_22.png', 80, 96, 22);
        game.load.spritesheet('water_anim', needUrlPath + '/img/water_anim_192x16_4.png', 192, 16, 4);
        game.load.spritesheet('water_bottom_1', needUrlPath + '/img/water_bottom_1_368x32_10.png', 368, 32, 10);
        game.load.spritesheet('water_bottom_2', needUrlPath + '/img/water_bottom_2_368x32_4.png', 368, 32, 4);
        game.load.spritesheet('game1.man_1', needUrlPath + '/img/game1.man_1_368_112_11.png', 368, 112, 11);
        game.load.spritesheet('game1.win', needUrlPath + '/img/game1.win_368_112_6.png', 368, 112, 6);

        game.load.image('game1.bar', needUrlPath + '/img/shape_fullv3.png');
        game.load.image('dice_spin', needUrlPath + '/img/dice_spin.png');
        game.load.image('border_card', needUrlPath + '/img/border.png');

        game.load.image('pick_left', needUrlPath + '/img/pick_left.png');
        game.load.image('pick_right', needUrlPath + '/img/pick_right.png');
        game.load.image('triger_left', needUrlPath + '/img/triger_left.png');
        game.load.image('triger_right', needUrlPath + '/img/triger_right.png');
        game.load.image('game2.win', needUrlPath + '/img/game2.win.png');
        game.load.image('game2.loss', needUrlPath + '/img/game2.loss.png');
        game.load.image('left_pick', needUrlPath + '/img/left_pick.png');
        game.load.image('right_pick', needUrlPath + '/img/right_pick.png');
        game.load.image('eagle', needUrlPath + '/img/eagle.png');
        game.load.image('tails', needUrlPath + '/img/tails.png');
        game.load.spritesheet('game2.flip_coin', needUrlPath + '/img/flip_coin_192x352_11.png', 208, 352, 11);

        game.load.image('shadow1', needUrlPath + '/img/shadow1.png');
        game.load.image('shadow2', needUrlPath + '/img/shadow2.png');
        game.load.image('shadow3', needUrlPath + '/img/shadow3.png');
        game.load.spritesheet('game3.meat', needUrlPath + '/img/meat_64x256_12.png', 64, 256, 12);
        game.load.spritesheet('game3.pick', needUrlPath + '/img/game3.pick_192x256_13.png', 192, 256, 13);
        game.load.spritesheet('game3.get_meat_star', needUrlPath + '/img/get_meat_star_192x416_6.png', 192, 416, 6);
        game.load.spritesheet('game3.get_meat_win', needUrlPath + '/img/get_meat_win_192x416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_1', needUrlPath + '/img/get_meat_lose_1_192_416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_2', needUrlPath + '/img/get_meat_lose_2_240x432_16.png', 240, 432, 16);

        game.load.spritesheet('game4.arrow', needUrlPath + '/img/arrow_64x48_5.png', 64, 48, 5);
        game.load.spritesheet('game4.captain_lose', needUrlPath + '/img/captain_lose_128x112_10.png', 128, 112, 10);
        game.load.spritesheet('game4.captain_pick', needUrlPath + '/img/captain_pick_128x112_25.png', 128, 112, 25);
        game.load.spritesheet('game4.captain_swim_1', needUrlPath + '/img/captain_swim_1_128x112_19.png', 128, 112, 19);
        game.load.spritesheet('game4.captain_swim_2', needUrlPath + '/img/captain_swim_2_256x160_6.png', 256, 160, 6);
        game.load.spritesheet('game4.captain_under', needUrlPath + '/img/captain_under_224x80_6.png', 224, 80, 6);
        game.load.spritesheet('game4.captain_win', needUrlPath + '/img/captain_win_128x112_5.png', 128, 112, 5);
        game.load.spritesheet('game4.dolphin', needUrlPath + '/img/dolphin_128х128_13.png', 128, 128, 13);
        game.load.spritesheet('game4.shark', needUrlPath + '/img/shark_128х144_13.png', 128, 144, 13);
        game.load.spritesheet('game4.fin', needUrlPath + '/img/fin_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.fin_left_end', needUrlPath + '/img/fin_left_end_128x64_6.png', 128, 64, 6);
        game.load.spritesheet('game4.fin_right_end', needUrlPath + '/img/fin_right_end_128x64_7.png', 128, 64, 7);
        game.load.spritesheet('game4.fin_end', needUrlPath + '/img/fin_left_end_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.water', needUrlPath + '/img/water_640x224_6.png', 640, 224, 6);

        game.load.spritesheet('game2.win_lose', needUrlPath + '/img/game2_win_lose.png', 112, 96, 5);

        game.load.audio('stop', needUrlPath + '/sounds/stop.wav');
        game.load.audio('rotate', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('takeWin', needUrlPath + '/sounds/takeWin.mp3');
        game.load.audio('pickCard', needUrlPath + '/sounds/pickCard.mp3');
        game.load.audio('cardWin', needUrlPath + '/sounds/cardWin.mp3');
        game.load.audio('hit', needUrlPath + '/sounds/hit.mp3');
        game.load.audio('winGame3', needUrlPath + '/sounds/winGame3.mp3');
        game.load.audio('rowWin', needUrlPath + '/sounds/rowWin.mp3');
        game.load.audio('page', needUrlPath + '/sounds/page.mp3');

        game.load.audio('7_sound40', needUrlPath + '/sounds/7_sound40.mp3');


    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

