
function game4() {
    var game4 = {
        freeze : false,
        climb : false,
        buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
        ropesafeCount : 5,
        menPositionsX : {1:94, 3:222, 5:350, 7:478, 9:606},
        selectedRopeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
        barrel1: [3,4,5,9],
        barrel2: [1,2,6,7,8,10],
        baller_open : {1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0},
        selectedSafe : null,
        safe_hit_arr : [],
        barrel_arr : [],
        barrel_arr_open : null,
        barrel_top_arr : null,
        barrel_lose_arr : null,
        barrel_d_arr : null,
        spin_dice : true,
        deck1_val1 : null,
        deck1_val2 : null,
        dice_arr : [],
        dice_d_arr : [],
        pick_dice : null,
        bars_dice: [],
        currentbarrel : 10,
        stone_arr : [],
        hook_arr : [],
        blue_screen_arr : [],
        hookfinal_arr : [],
        bottom_bg_arr : [],
        safe_door_arr : [],
        safe_door_arr_anim : [],
        barrel_arr_i : null,
        tween1: null,
        tween2: null,
        tween3: null,
        tween4: null,
        bg_main : null,
        floor: 1,
        selectedRope : null,
        button : [],
        push_baller : [],
        currentBar : 1,
        bars_line: [],
        barsCurrentSpins : [0, 0, 0],
        barsTotalSpins : [27, 51, 75],
        countPlayBars : 0,
        bar_arr: [],
        bar_arr_w: [],
        baller_count : null,
        first_spin : true,
        meat_arr: [],
        pick_meat: null,

        create : function() {

            mobileX = 0;
            mobileY = 0;

            stopSound = game.add.audio('stop');
            rotateSound = game.add.audio('rotate');

            game4.bg = game.add.sprite(94+mobileX,54+mobileY, 'game3.bg');
            for (var i = 0; i <= 1; ++i) {
                game4.meat_arr[i] = game.add.sprite(260+mobileX+i*256, 54+mobileY, 'game3.meat');
                game4.meat_arr[i].animations.add('game3.meat', [0,1,2,3,4,5,6,7,8,9,10,11,10,9,8,7,6,5,4,3,2,1], 8, true).play();
                game4.meat_arr[i].inputEnabled = true;
            }
            if(!isMobile){
                game.add.sprite(0,0, 'main_window');
            }

            //
//game3.barrel_lose_arr.animations.getAnimation('barrel_lose').stop();
            //
            game4.shadow1 = game.add.sprite(286+mobileX, 374+mobileY, 'shadow1');
            game4.shadow2 = game.add.sprite(94+mobileX, 374+mobileY, 'shadow2');
            // game3.shadow2.visible = false;
            // game3.shadow3 = game.add.sprite(286+mobileX, 374+mobileY, 'shadow3');
            // game3.shadow3.visible = false;

            game4.pick = game.add.sprite(342+mobileX, 198+mobileY, 'game3.pick');
            game4.pick.animations.add('game3.pick', [0,1,2,3,4,5,6,5,6,5,6,4,3,2,1,0,0,0,7,8,9,10,11,12,11,12,11,12,10,9,8,7,0,0,0], 8, true).play();


            game4.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top3');

            game4.countPlayBars = 3;
            game4.spinStatus = true;



            //кнопки и анимации событий нажатия

            //для данной игры в каждую функцию вносится по две анимации (win и lose) для каждой кнопки
            selectionsAmin = [];
            timeout1Game4ToGame1 = 4000;
            timeout2Game4ToGame1 = 3000;


            // анимация для кнопки 3
            selectionsAmin[0] = function (ropeValues, ropeStep) {

                game4.get_meat_star = game.add.sprite(132+mobileX, 43+mobileY, 'game3.get_meat_star');
                game4.get_meat_star.animations.add('game3.get_meat_star', [0,1,2,3,4,5], 8, false);
                game4.get_meat_star.visible = false;
                game4.get_meat_win = game.add.sprite(132+mobileX, 43+mobileY, 'game3.get_meat_win');
                game4.get_meat_win.animations.add('game3.get_meat_win', [0,1,2], 8, false);
                game4.get_meat_win.visible = false;

                game4.freeze = true;
                game4.pick.visible = false;
                game4.get_meat_star.visible = true;

                game4.get_meat_lose_1 = game.add.sprite(132+mobileX, 43+mobileY, 'game3.get_meat_lose_1');
                game4.get_meat_lose_1.animations.add('game3.get_meat_lose_1', [0,1,2], 8, false);
                game4.get_meat_lose_1.visible = false;
                game4.get_meat_lose_2 = game.add.sprite(132+mobileX, 43+mobileY, 'game3.get_meat_lose_2');
                game4.get_meat_lose_2.animations.add('game3.get_meat_lose_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 8, false);
                game4.get_meat_lose_2.visible = false;

                game4.get_meat_star.animations.getAnimation('game3.get_meat_star').onComplete.add(function(){
                    game4.get_meat_star.visible = false ;
                    if (game4.pick_meat == 1) {
                        game4.get_meat_win.visible = true;
                        game4.get_meat_win.animations.getAnimation('game3.get_meat_win').play();
                    } else {
                        game4.get_meat_lose_1.visible = true;
                        game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').play();
                    }
                });
                game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').onComplete.add(function(){
                    game4.get_meat_lose_1.visible = false ;
                    game4.get_meat_lose_2.visible = true;
                    game4.get_meat_lose_2.animations.getAnimation('game3.get_meat_lose_2').play();
                });

                if(ropeValues[5] != 0){
                    game4.pick_meat = 1;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[0].visible = false;
                } else {
                    game4.pick_meat = 0;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[0].visible = false;
                }
            };

            // анимация для кнопки 7
            selectionsAmin[1] = function (ropeValues, ropeStep) {

                game4.get_meat_star = game.add.sprite(388+mobileX, 43+mobileY, 'game3.get_meat_star');
                game4.get_meat_star.animations.add('game3.get_meat_star', [0,1,2,3,4,5], 8, false);
                game4.get_meat_star.visible = false;
                game4.get_meat_win = game.add.sprite(388+mobileX, 43+mobileY, 'game3.get_meat_win');
                game4.get_meat_win.animations.add('game3.get_meat_win', [0,1,2], 8, false);
                game4.get_meat_win.visible = false;

                game4.freeze = true;
                game4.pick.visible = false;
                game4.get_meat_star.visible = true;

                game4.get_meat_lose_1 = game.add.sprite(388+mobileX, 43+mobileY, 'game3.get_meat_lose_1');
                game4.get_meat_lose_1.animations.add('game3.get_meat_lose_1', [0,1,2], 8, false);
                game4.get_meat_lose_1.visible = false;
                game4.get_meat_lose_2 = game.add.sprite(388+mobileX, 43+mobileY, 'game3.get_meat_lose_2');
                game4.get_meat_lose_2.animations.add('game3.get_meat_lose_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 8, false);
                game4.get_meat_lose_2.visible = false;

                game4.get_meat_star.animations.getAnimation('game3.get_meat_star').onComplete.add(function(){
                    game4.get_meat_star.visible = false ;
                    if (game4.pick_meat == 1) {
                        game4.get_meat_win.visible = true;
                        game4.get_meat_win.animations.getAnimation('game3.get_meat_win').play();
                    } else {
                        game4.get_meat_lose_1.visible = true;
                        game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').play();
                    }
                });
                game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').onComplete.add(function(){
                    game4.get_meat_lose_1.visible = false ;
                    game4.get_meat_lose_2.visible = true;
                    game4.get_meat_lose_2.animations.getAnimation('game3.get_meat_lose_2').play();
                });

                if(ropeValues[5] != 0){
                    game4.pick_meat = 1;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[1].visible = false;
                } else {
                    game4.pick_meat = 0;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[1].visible = false;
                }
            };


            addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1);


            //счет
            scorePosions = [[260, 25, 20], [435, 25, 20], [610, 25, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, stepTotalWinR, balance, betline);


            full_and_sound();
        },
        update: function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   }
}
game.state.add('game4', game4);


}