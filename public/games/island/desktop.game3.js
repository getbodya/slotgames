
function game3() {
    (function () {

        var game3 = {
            arrow_arr : [],
            dolphin_arr : [],
            shark_arr : [],
            fin_arr : [],
            button : [],
            pick_line : null,
            selectedRopeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
            buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
        };

        game3.preload = function () {};

        game3.create = function () {
            mobileX = 0;
            mobileY = 0;

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            ropeStep = 0;
            checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса
            
            //звуки

            
            //изображения
            background = game.add.sprite(0,0, 'game.background');
            game3.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top4');
            game3.bg = game.add.sprite(94+mobileX,22+mobileY, 'game4.bg');


            game3.arrow_arr = [];
            game3.fin_arr = [];
            for (var i = 1; i <= 5; ++i) {
                game3.arrow_arr[i] = game.add.sprite(130+mobileX+(i-1)*127, 272+mobileY, 'game4.arrow');
                game3.arrow_arr[i].animations.add('game4.arrow', [0,1,2,3,4], 6, true).play();
                if (i % 2 != 0) {
                    game3.fin_arr[i] = game.add.sprite(104+mobileX+(i-1)*127, 214+mobileY, 'game4.fin');
                    game3.fin_arr[i].animations.add('game4.fin', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, true).play();
                    game3.fin_arr[i].inputEnabled = true;
                } else {
                    game3.fin_arr[i] = game.add.sprite(104+mobileX+(i-1)*127, 214+mobileY, 'game4.fin');
                    game3.fin_arr[i].animations.add('game4.fin', [7,8,9,10,11,12,0,1,2,3,4,5,6], 12, true).play();
                    game3.fin_arr[i].inputEnabled = true;
                }
            }

            //счет
            scorePosions = [[260, 25, 20], [435, 25, 20], [610, 25, 20], [475, 68, 30]];
            addScore(game, scorePosions, '', '', balance, betline);



            //кнопки и анимации событий нажатия

            ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша
            
            winRopeNumberPosition = [[185,152],[185,120],[185,88],[185,56],[185,24]];
            timeoutForShowWinRopeNumber = 3000;
            timeoutGame3ToGame4 = 3000;
            typeWinRopeNumberAnim = 0;
            checkHelm = false;
            winRopeNumberSize = 22;

            //добавляем в ropesAnim анимации

            ropesAnim[0] = function (ropeValues, ropeStep, checkHelm) {
                n = 1;

                if(game3.freeze) {
                    return;
                }

                game3.freeze = true;
                game3.pick_line = game3.selectedRopeNormal[n];

                game3.captain_pick.visible = false;
                for (var i = 1; i <= 5; ++i) {
                    game3.arrow_arr[i].visible = false;
                };
                game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127;
                game3.captain_under.position.x = 53 + (game3.pick_line-1)*127;
                game3.captain_swim_1.visible = true;
                game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
                    game3.fin_arr[game3.pick_line].visible = false;
                    game3.captain_swim_1.visible = false;
                    game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127;
                    game3.captain_swim_2.visible = true;
                    game3.fin_end.position.x = 104 + (game3.pick_line-1)*127;
                    game3.fin_end.visible = true;
                    game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                        game3.fin_end.visible = false;
                        if (ropeValues[ropeStep] == 0) {
                            game3.shark.position.x = 104 + (game3.pick_line-1)*127;
                            game3.shark.visible = true;
                            game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                                game.state.start('game1');
                            });
                        } else {
                            game3.dolphin.position.x = 104 + (game3.pick_line-1)*127;
                            game3.dolphin.visible = true;
                            game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                                game3.dolphin.visible = false;
                                game3.captain_swim_2.visible = false;
                                game3.captain_pick.visible = true;
                                game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127;
                                game3.fin_end.visible = false;
                                game3.freeze = false;
                            });
                        }
                    });
                    game3.fin_end.animations.getAnimation('game4.fin_end').play();
                });
                lockDisplay();
                setTimeout('unlockDisplay()',5000);
            };
            ropesAnim[1] = function (ropeValues, ropeStep, checkHelm) {
                n = 3;

                if(game3.freeze) {
                    return;
                }

                game3.freeze = true;
                game3.pick_line = game3.selectedRopeNormal[n];

                game3.captain_pick.visible = false;
                for (var i = 1; i <= 5; ++i) {
                    game3.arrow_arr[i].visible = false;
                };
                game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127;
                game3.captain_under.position.x = 53 + (game3.pick_line-1)*127;
                game3.captain_swim_1.visible = true;
                game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
                    game3.fin_arr[game3.pick_line].visible = false;
                    game3.captain_swim_1.visible = false;
                    game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127;
                    game3.captain_swim_2.visible = true;
                    game3.fin_end.position.x = 104 + (game3.pick_line-1)*127;
                    game3.fin_end.visible = true;
                    game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                        game3.fin_end.visible = false;
                        if (ropeValues[ropeStep] == 0) {
                            game3.shark.position.x = 104 + (game3.pick_line-1)*127;
                            game3.shark.visible = true;
                            game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                                game.state.start('game1');
                            });
                        } else {
                            game3.dolphin.position.x = 104 + (game3.pick_line-1)*127;
                            game3.dolphin.visible = true;
                            game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                                game3.dolphin.visible = false;
                                game3.captain_swim_2.visible = false;
                                game3.captain_pick.visible = true;
                                game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127;
                                game3.fin_end.visible = false;
                                game3.freeze = false;
                            });
                        }
                    });
                    game3.fin_end.animations.getAnimation('game4.fin_end').play();
                });
                lockDisplay();
                setTimeout('unlockDisplay()',5000);
            };
            ropesAnim[2] = function (ropeValues, ropeStep, checkHelm) {
                n = 5;

                if(game3.freeze) {
                    return;
                }

                game3.freeze = true;
                game3.pick_line = game3.selectedRopeNormal[n];

                game3.captain_pick.visible = false;
                for (var i = 1; i <= 5; ++i) {
                    game3.arrow_arr[i].visible = false;
                };
                game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127;
                game3.captain_under.position.x = 53 + (game3.pick_line-1)*127;
                game3.captain_swim_1.visible = true;
                game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
                    game3.fin_arr[game3.pick_line].visible = false;
                    game3.captain_swim_1.visible = false;
                    game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127;
                    game3.captain_swim_2.visible = true;
                    game3.fin_end.position.x = 104 + (game3.pick_line-1)*127;
                    game3.fin_end.visible = true;
                    game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                        game3.fin_end.visible = false;
                        if (ropeValues[ropeStep] == 0) {
                            game3.shark.position.x = 104 + (game3.pick_line-1)*127;
                            game3.shark.visible = true;
                            game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                                game.state.start('game1');
                            });
                        } else {
                            game3.dolphin.position.x = 104 + (game3.pick_line-1)*127;
                            game3.dolphin.visible = true;
                            game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                                game3.dolphin.visible = false;
                                game3.captain_swim_2.visible = false;
                                game3.captain_pick.visible = true;
                                game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127;
                                game3.fin_end.visible = false;
                                game3.freeze = false;
                            });
                        }
                    });
                    game3.fin_end.animations.getAnimation('game4.fin_end').play();
                });
                lockDisplay();
                setTimeout('unlockDisplay()',5000);
            };
            ropesAnim[3] = function (ropeValues, ropeStep, checkHelm) {
                n = 7;

                if(game3.freeze) {
                    return;
                }

                game3.freeze = true;
                game3.pick_line = game3.selectedRopeNormal[n];

                game3.captain_pick.visible = false;
                for (var i = 1; i <= 5; ++i) {
                    game3.arrow_arr[i].visible = false;
                };
                game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127;
                game3.captain_under.position.x = 53 + (game3.pick_line-1)*127;
                game3.captain_swim_1.visible = true;
                game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
                    game3.fin_arr[game3.pick_line].visible = false;
                    game3.captain_swim_1.visible = false;
                    game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127;
                    game3.captain_swim_2.visible = true;
                    game3.fin_end.position.x = 104 + (game3.pick_line-1)*127;
                    game3.fin_end.visible = true;
                    game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                        game3.fin_end.visible = false;
                        if (ropeValues[ropeStep] == 0) {
                            game3.shark.position.x = 104 + (game3.pick_line-1)*127;
                            game3.shark.visible = true;
                            game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                                game.state.start('game1');
                            });
                        } else {
                            game3.dolphin.position.x = 104 + (game3.pick_line-1)*127;
                            game3.dolphin.visible = true;
                            game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                                game3.dolphin.visible = false;
                                game3.captain_swim_2.visible = false;
                                game3.captain_pick.visible = true;
                                game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127;
                                game3.fin_end.visible = false;
                                game3.freeze = false;
                            });
                        }
                    });
                    game3.fin_end.animations.getAnimation('game4.fin_end').play();
                });
                lockDisplay();
                setTimeout('unlockDisplay()',5000);
            };
            ropesAnim[4] = function (ropeValues, ropeStep, checkHelm) {
                n = 9;

                if(game3.freeze) {
                    return;
                }

                game3.freeze = true;
                game3.pick_line = game3.selectedRopeNormal[n];

                game3.captain_pick.visible = false;
                for (var i = 1; i <= 5; ++i) {
                    game3.arrow_arr[i].visible = false;
                };
                game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127;
                game3.captain_under.position.x = 53 + (game3.pick_line-1)*127;
                game3.captain_swim_1.visible = true;
                game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
                    game3.fin_arr[game3.pick_line].visible = false;
                    game3.captain_swim_1.visible = false;
                    game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127;
                    game3.captain_swim_2.visible = true;
                    game3.fin_end.position.x = 104 + (game3.pick_line-1)*127;
                    game3.fin_end.visible = true;
                    game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                        game3.fin_end.visible = false;
                        if (ropeValues[ropeStep] == 0) {
                            game3.shark.position.x = 104 + (game3.pick_line-1)*127;
                            game3.shark.visible = true;
                            game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                                game.state.start('game1');
                            });
                        } else {
                            game3.dolphin.position.x = 104 + (game3.pick_line-1)*127;
                            game3.dolphin.visible = true;
                            game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                                game3.dolphin.visible = false;
                                game3.captain_swim_2.visible = false;
                                game3.captain_pick.visible = true;
                                game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127;
                                game3.fin_end.visible = false;
                                game3.freeze = false;
                            });
                        }
                    });
                    game3.fin_end.animations.getAnimation('game4.fin_end').play();
                });
                lockDisplay();
                setTimeout('unlockDisplay()',5000);
            };



            //анимации

            game3.water = game.add.sprite(94+mobileX, 256+mobileY, 'game4.water');
            game3.water.animations.add('game4.water', [0,1,2,3,4,5], 12, true).play();
            game3.dolphin = game.add.sprite(104+mobileX, 170+mobileY, 'game4.dolphin');
            game3.dolphin.animations.add('game4.dolphin', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, false);
            game3.dolphin.visible = false;
            game3.shark = game.add.sprite(130+mobileX, 170+mobileY, 'game4.shark');
            game3.shark.animations.add('game4.shark', [0,1,2,3,4,5,6,7,8,9,10,11,12,12,12,12,12,12], 8, false);
            game3.shark.visible = false;
            game3.captain_lose = game.add.sprite(355+mobileX, 340+mobileY, 'game4.captain_lose');
            game3.captain_lose.animations.add('game4.captain_lose', [0,1,2,3,4,5,6,7,8,9], 8, false);
            game3.captain_lose.visible = false;
            game3.captain_pick = game.add.sprite(355+mobileX, 327+mobileY, 'game4.captain_pick');
            game3.captain_pick.animations.add('game4.captain_pick', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,12,11,10,9,8,8,8,8,14,15,16,17,18,19,20,21,22,23,24,0,0,0], 8, true).play();
            game3.captain_swim_1 = game.add.sprite(355+mobileX, 327+mobileY, 'game4.captain_swim_1');
            game3.captain_swim_1.animations.add('game4.captain_swim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 10, false);
            game3.captain_swim_1.visible = false;
            game3.captain_win = game.add.sprite(355+mobileX, 327+mobileY, 'game4.captain_win');
            game3.captain_win.animations.add('game4.captain_win', [0,1,2,3,4], 8, false);
            game3.captain_win.visible = false;
            game3.fin_left_end = game.add.sprite(104+mobileX, 199+mobileY, 'game4.fin_left_end');
            game3.fin_left_end.animations.add('game4.fin_left_end', [0,1,2,3,4,5], 12, false);
            game3.fin_left_end.visible = false;
            game3.fin_right_end = game.add.sprite(104+mobileX, 199+mobileY, 'game4.fin_right_end');
            game3.fin_right_end.animations.add('game4.fin_right_end', [0,1,2,3,4,5,6], 12, false);
            game3.fin_right_end.visible = false;
            game3.fin_end = game.add.sprite(104+mobileX, 214+mobileY, 'game4.fin_end');
            game3.fin_end.animations.add('game4.fin_end', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, false);
            game3.fin_end.visible = false;
            game3.captain_under = game.add.sprite(307+mobileX, 407+mobileY, 'game4.captain_under');
            game3.captain_under.animations.add('game4.captain_under', [0,1,2,3,4,5], 8, true).play();
            game3.captain_swim_2 = game.add.sprite(37+mobileX, 327+mobileY, 'game4.captain_swim_2');
            game3.captain_swim_2.animations.add('game4.captain_swim_2', [0,1,2,3,4,5,0,1,2,3,4,5,0], 12, false);
            game3.captain_swim_2.visible = false;

            group1 = game.add.group();
            group1.add(background);
            game.world.bringToTop(group1);

            addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);

            full_and_sound();


        };

        game3.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game3', game3);

})();

}