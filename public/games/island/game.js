function randomNumber(min, max) {
	return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}
var isMobile = false;
var mobileX = null;
var mobileY = null;
if (navigator.userAgent.match(/Android/i)
	|| navigator.userAgent.match(/webOS/i)
	|| navigator.userAgent.match(/iPhone/i)
	|| navigator.userAgent.match(/iPad/i)
	|| navigator.userAgent.match(/iPod/i)
	|| navigator.userAgent.match(/BlackBerry/i)
	|| navigator.userAgent.match(/Windows Phone/i)
	) {
	isMobile = true;
mobileX = -94;
mobileY = -54;
}
var width = 829.7;
var height = 598.95;

if(isMobile) {
	width = 640;
	height = 480;
}
var game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example');
var flickerInterval = '';
var fullStatus = false;
var soundStatus = true;
var game3on = false;
function createLevelButtons() {
	var lvl1 = game.add.sprite(0,0, 'x');
	lvl1.inputEnabled = true;
	lvl1.input.useHandCursor = true;
	lvl1.events.onInputUp.add(function () {
		game.state.start('game1');
		game3on = false;
	}, this);

	var lvl2 = game.add.sprite(30, 0, 'x');
	lvl2.inputEnabled = true;
	lvl2.input.useHandCursor = true;
	lvl2.events.onInputUp.add(function () {
		game.state.start('game2');     
		game2.ropePositionX = 254;
		game3on = false;
	}, this);

	var lvl3 = game.add.sprite(60, 0, 'x');
	lvl3.inputEnabled = true;
	lvl3.input.useHandCursor = true;
	lvl3.events.onInputUp.add(function () {
		game.state.start('game3');
		game3.freeze = false;
	}, this);

	var lvl4 = game.add.sprite(90, 0, 'x');
	lvl4.inputEnabled = true;
	lvl4.input.useHandCursor = true;
	lvl4.events.onInputUp.add(function () {
		game.state.start('game4');
	}, this);
}
function full_and_sound(){
	if (!fullStatus)
		full = game.add.sprite(740+mobileX,30+mobileY, 'non_full');
	else
		full = game.add.sprite(740+mobileX,30+mobileY, 'full');
	full.inputEnabled = true;
	full.input.useHandCursor = true;
	full.events.onInputUp.add(function(){
		if (fullStatus == false){
			full.loadTexture('full');
			fullStatus = true;
			if(document.documentElement.requestFullScreen) {
				document.documentElement.requestFullScreen();
			} else if(document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if(document.documentElement.webkitRequestFullScreen) {
				document.documentElement.webkitRequestFullScreen();
			}
		} else {
			full.loadTexture('non_full');
			fullStatus = false;
			if(document.cancelFullScreen) {
				document.cancelFullScreen();
			} else if(document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if(document.webkitCancelFullScreen) {
				document.webkitCancelFullScreen();
			}
		}
	});
	if (soundStatus)
		sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_on');
	else
		sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_off');
	sound.inputEnabled = true;
	sound.input.useHandCursor = true;
	sound.events.onInputUp.add(function(){
		if (soundStatus == true){
			sound.loadTexture('sound_off');
			soundStatus =false;
			game.sound.mute = true;
		} else {
			sound.loadTexture('sound_on');
			soundStatus = true;
			game.sound.mute = false;
		}
	});
}
function selectLine(n) {
	game1.currentLine = n;

	for (var i = 1; i <= 9; ++i) {
		game1.lines[i].sprite.visible = false;
		game1.lines[i].number.visible = false;
	}
	for (var i = 1; i <= n; ++i) {
		game1.lines[i].sprite.visible = true;
		game1.lines[i].number.visible = true;
		game1.lines[i].sprite.loadTexture('line_' + i);
		if (i === 5)
			game1.lines[5].sprite.position.y = 144+mobileY;
		if (i === 7)
			game1.lines[7].sprite.position.y = 318+mobileY;

	}
};
function preselectLine(n) {
	for (var i = 1; i <= 9; ++i) {
		game1.lines[i].sprite.visible = false;
		game1.lines[i].number.visible = false;
	}
	for (var i = 1; i <= n; ++i) {
		game1.lines[i].sprite.loadTexture('linefull_' + i);        
		game1.lines[i].sprite.visible = true;
		game1.lines[i].number.visible = true;
		if (i === 5)
			game1.lines[5].sprite.position.y = 131+mobileY;
		if (i === 7)
			game1.lines[7].sprite.position.y = 263+mobileY;

	}
}
function disableLinesBtn(){
	for (var i = 1; i <= 9; ++i) {
		if (i % 2 != 0) {
			game1.lines[i].button.loadTexture('btnline_d' + i);
		}
	}
};
function unDisableLines(){
	for (var i = 1; i <= 9; ++i) {
		if (i % 2 != 0) {
			game1.lines[i].button.loadTexture('btnline' + i);            
		}
	}    
};
function winLine(n) {
	game1.lines[n].sprite.loadTexture('linefull_' + n);
	game1.lines[n].sprite.visible = true;
	winNumber(n);
};
function winNumber(i){
	flickerInterval = setInterval(              
		function(){
			if (game1.takeWin){                        
				game1.lines[i].number.visible = false;
			} 
			unflicker(i);
		}, 1000);
};
function unflicker(i){
	setTimeout(function() {
		game1.lines[i].number.visible = true;
	}, 500);
}
function shuffle(arr) {
	return arr.sort(function() {return 0.5 - Math.random()});
}
function finalResult(currentBar) {
	barSound = game.add.audio('game3.hit');	
	var sp = randomNumber(3,7);
	setTimeout(go, 200);
	function go() {  	
		if (sp != 0){  	
			barSound.play();		
			if (game3.currentBar === 26){
				game3.currentBar = 1;
				game3.bar_arr_w[26].visible = false;
				game3.bar_arr_w[game3.currentBar].visible = true;
			}
			else{
				game3.currentBar +=1;
				game3.bar_arr_w[game3.currentBar-1].visible = false;
				game3.bar_arr_w[game3.currentBar].visible = true;
			}
			sp -=1;
			setTimeout(go, 200);
		}
	}
}
//локация 1
var game1 = {
	bars: [],
	currentLine : 9,
	spinStatus : false,
	barsCurrentSpins : [0, 0, 0, 0, 0],
	barsTotalSpins : [15, 27, 39, 51, 63 ],
	countPlayBars : 0,
	takeWin : false,
	pages : [],
	settingsMode : false,
	currentPage : null,
	lines : {
		1: {
			coord: 245,
			sprite: null,
			btncoord : 250,
			button : null,
			number : null,
			numbercoord : 230
		},
		2: {
			coord: 109,
			sprite: null,
			number : null,
			numbercoord :86
		},
		3: {
			coord: 380,
			sprite: null,
			btncoord : 295,
			button : null,
			number : null,
			numbercoord : 374
		},
		4: {
			coord: 156,
			sprite: null,
			number : null,
			numbercoord : 150
		},
		5: {
			coord: 130,
			sprite: null,
			btncoord : 340,
			button : null,
			number : null,
			numbercoord : 310
		},
		6: {
			coord: 130,
			sprite: null,
			number : null,
			numbercoord :118
		},
		7: {
			coord: 261,
			sprite: null,
			btncoord : 385,
			button : null,
			number : null,
			numbercoord : 342
		},
		8: {
			coord: 268,
			sprite: null,
			number : null,
			numbercoord : 262
		},
		9: {
			coord: 155,
			sprite: null,
			btncoord : 430,
			button : null,
			number : null,
			numbercoord : 198
		},

	},
	create:function(){
		for (var i = 0; i < 5; ++i) {
			game1.bars[i] = game.add.tileSprite(142+mobileX+i*112, 88+mobileY, 96, 322, 'game1.bar');
			game1.bars[i].tilePosition.y =  randomNumber(0,8)*112 ; 
		}
		game1.strawberry_bar_anim_1 = game.add.sprite(142+mobileX, 88+mobileY, 'bonus');
		game1.strawberry_bar_anim_1Animation = game1.strawberry_bar_anim_1.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
		game1.strawberry_bar_anim_1.visible = false;
		game1.strawberry_bar_anim_2 = game.add.sprite(254+mobileX, 88+mobileY, 'bonus');
		game1.strawberry_bar_anim_2Animation = game1.strawberry_bar_anim_2.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
		game1.strawberry_bar_anim_2.visible = false;
		game1.strawberry_bar_anim_3 = game.add.sprite(366+mobileX, 88+mobileY, 'bonus');
		game1.strawberry_bar_anim_3Animation = game1.strawberry_bar_anim_3.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
		game1.strawberry_bar_anim_3.visible = false;
		game1.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top1');
		game1.bg = game.add.sprite(94+mobileX,54+mobileY, 'game1.bg');		
		game1.water_anim = game.add.sprite(542+mobileX, 486+mobileY, 'water_anim');
		game1.water_animAnimation = game1.water_anim.animations.add('water_anim', [0,1,2,3], 8, true);
		game1.water_animAnimation.play();	
		game1.water_bottom_1 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_1');
		game1.water_bottom_1Animation = game1.water_bottom_1.animations.add('water_bottom_1', [0,1,2,3,4,5,6,7,8,9], 8, false);
		game1.water_bottom_1Animation.play();	
		game1.water_bottom_2 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_2');
		game1.water_bottom_2Animation = game1.water_bottom_2.animations.add('water_bottom_2', [0,1,2,3], 8, false);
		game1.water_bottom_2.visible = false;	
		game1.man_1 = game.add.sprite(94+mobileX, 390+mobileY, 'game1.man_1');
		game1.man_1Animation = game1.man_1.animations.add('game1.man_1', [0,1,2,3,4,5,6,7,8,9,10], 8, true);
		game1.man_1Animation.play();
		game1.win = game.add.sprite(94+mobileX, 390+mobileY, 'game1.win');
		game1.winAnimation = game1.win.animations.add('game1.win', [0,1,2,3,4,5], 8, false);
		game1.win.visible = false;	
		game1.bird_anim_1 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_1');
		game1.bird_anim_1Animation = game1.bird_anim_1.animations.add('bird_anim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44], 8, false);
		game1.bird_anim_1Animation.play();

		game1.bird_anim_2 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_2');
		game1.bird_anim_2Animation = game1.bird_anim_2.animations.add('bird_anim_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 8, false);
		game1.bird_anim_2.visible = false;
		game1.winAnimation.onComplete.add(function(){
			game1.win.visible = false;
		});	
		game1.bird_anim_1Animation.onComplete.add(function(){
			game1.bird_anim_1.visible = false;
			game1.bird_anim_2.visible = true;
			game1.bird_anim_2Animation.play();
		});
		game1.bird_anim_2Animation.onComplete.add(function(){
			game1.bird_anim_1.visible = true;
			game1.bird_anim_2.visible = false;
			game1.bird_anim_1Animation.play();
		});
		game1.water_bottom_1Animation.onComplete.add(function(){
			game1.water_bottom_1.visible = false;
			game1.water_bottom_2.visible = true;
			game1.water_bottom_2Animation.play();
		});
		game1.water_bottom_2Animation.onComplete.add(function(){
			game1.water_bottom_1.visible = true;
			game1.water_bottom_2.visible = false;
			game1.water_bottom_1Animation.play();
		});
		for (var i = 1; i <= 9; ++i) {
			game1.lines[i].sprite = game.add.sprite(134+mobileX, game1.lines[i].coord+mobileY, 'line_' + i);
			game1.lines[i].sprite.visible = false;
			game1.lines[i].number = game.add.sprite(94+mobileX, game1.lines[i].numbercoord+mobileY, 'win_' + i);
			game1.lines[i].number.visible = false;
			game1.lines[i].sprite = game.add.sprite(132+mobileX, game1.lines[i].coord+mobileY, 'linefull_' + i);
			game1.lines[i].sprite.visible = false;
		}
		for (var i = 1; i <= 6; ++i) {
			game1.page = game.add.sprite(94+mobileX, 22+mobileY, 'game1.page_' + i);
			game1.page.visible = false;
			game1.pages[i] = game1.page;
		}
		prev_page = game.add.sprite(94+mobileX, 444+mobileY, 'prev_page');
		prev_page.visible = false;
		prev_page.inputEnabled = true;
		prev_page.input.useHandCursor = true;
		prev_page.events.onInputUp.add(function(){
			if (game1.settingsMode)  {
				pageSound.play();
				if (game1.currentPage == 1)
					game1.currentPage = 6;
				else{
					game1.pages[game1.currentPage].visible = false;
					game1.currentPage -=1;
				}
			}
			game1.pages[game1.currentPage].visible = true;
		});
		exit_btn = game.add.sprite(319+mobileX, 444+mobileY, 'exit_btn');
		exit_btn.visible = false;
		exit_btn.inputEnabled = true;
		exit_btn.input.useHandCursor = true;
		exit_btn.events.onInputUp.add(function(){
			pageSound.play();
			for (var i = 1; i <= 6; ++i) {
				game1.pages[i].visible = false;
			}            
			prev_page.visible = false;
			exit_btn.visible = false;
			next_page.visible = false;
			game1.settingsMode = false;
			if (!isMobile){
				unDisableLines()
				game1.automatic_start.loadTexture('automatic_start');
				game1.bet_max.loadTexture('bet_max');
				game1.bet_one.loadTexture('bet_one');
				game1.paytable.loadTexture('paytable');
				game1.select_game.loadTexture('select_game');
			} else {
				doubleb.visible = true;
				dollar.visible = true;
				home.visible = true;
				button.visible = true;
				bet1.visible = true;
				gear.visible = true;
			}
		});
		next_page = game.add.sprite(509+mobileX, 444+mobileY, 'next_page');
		next_page.visible = false;
		next_page.inputEnabled = true;
		next_page.input.useHandCursor = true;
		next_page.events.onInputUp.add(function(){
			if (game1.settingsMode)  {
				pageSound.play();
				if (game1.currentPage == 6){
					game1.pages[game1.currentPage].visible = false;
					game1.currentPage = 1;
				} else if (game1.currentPage == 1){
					game1.currentPage +=1;
				} else {                    
					game1.pages[game1.currentPage].visible = false;
					game1.currentPage +=1;
				}
			}
			game1.pages[game1.currentPage].visible = true;
		});
		if(!isMobile){
			game.add.sprite(0,0, 'main_window');
			full_and_sound();
		}
		if(!isMobile){
			for (var i = 1; i <= 9; ++i) {
				if (i % 2 != 0) {
					game1.lines[i].sound = game.add.audio('line' + i);
					game1.lines[i].button = game.add.sprite(game1.lines[i].btncoord+mobileX, 510+mobileY, 'btnline' + i);
					game1.lines[i].button.scale.setTo(0.7, 0.7);
					game1.lines[i].button.inputEnabled = true;
					game1.lines[i].button.input.useHandCursor = true;
					(function (n) {
						game1.lines[n].button.events.onInputOver.add(function(){
							if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
								return;
							game1.lines[n].button.loadTexture('btnline_p' + n);
						});
						game1.lines[n].button.events.onInputOut.add(function(){
							if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
								return;
							game1.lines[n].button.loadTexture('btnline' + n);
						});
						game1.lines[n].button.events.onInputUp.add(function () {
							if(game1.spinStatus || game1.takeWin || game1.settingsMode)
								return;
							hideLines();
							selectLine(n);
						}, this);					
						game1.lines[n].button.events.onInputDown.add(function () {
							if(game1.spinStatus || game1.takeWin || game1.settingsMode)
								return;						
							preselectLine(n);
							game1.lines[n].sound.play();
						}, this);
					})(i);
				}
			}            
		}
		if (isMobile){
			button = game.add.sprite(544, 188, 'spin');
			button.inputEnabled = true;
			button.input.useHandCursor = true;

			bet1 = game.add.sprite(548, 274, 'bet1');
			bet1.inputEnabled = true;
			bet1.input.useHandCursor = true;
			bet1.events.onInputDown.add(function(){
				bet1.loadTexture('bet1_p');
			});
			bet1.events.onInputUp.add(function(){
				document.getElementById('betMode').style.display = 'block';
				widthVisibleZone = $('.betWrapper .visibleZone').height();
				$('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
				$('canvas').css('display', 'none');
				bet1.loadTexture('bet1');
			});
			home = game.add.sprite(3, 3, 'home');
			home.inputEnabled = true;
			home.input.useHandCursor = true;
			home.events.onInputDown.add(function(){
				home.loadTexture('home_p');
			});
			home.events.onInputUp.add(function(){
				home.loadTexture('home');
			});
			dollar = game.add.sprite(435, 3, 'dollar');
			dollar.inputEnabled = true;
			dollar.input.useHandCursor = true;
			dollar.events.onInputDown.add(function(){
			});

			gear = game.add.sprite(539, 3, 'gear');
			gear.inputEnabled = true;
			gear.input.useHandCursor = true;
			gear.events.onInputDown.add(function(){
				game1.pages[1].visible = true;
				prev_page.visible = true;
				exit_btn.visible = true;
				next_page.visible = true;
				game1.settingsMode = true;
				game1.currentPage = 1;
				doubleb.visible = false;
				dollar.visible = false;
				home.visible = false;
				button.visible = false;
				bet1.visible = false;
				gear.visible = false;
			});

			doubleb = game.add.sprite(546, 133, 'double');
			doubleb.inputEnabled = true;
			doubleb.input.useHandCursor = true;
			doubleb.events.onInputDown.add(function(){
				game.state.start('game2');
				game2.ropePositionX = 254;
			});
			function buttonClicked() {
				if (game1.spinStatus) {
					return;
				}
				button.loadTexture('spin_p');
			}			
			button.events.onInputDown.add(buttonClicked, this);
			button.events.onInputUp.add(btnStartUp, this);
		}
		if(!isMobile){
			game1.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
			game1.startButton.scale.setTo(0.7, 0.7);
			game1.startButton.inputEnabled = true;
			game1.startButton.input.useHandCursor = true;
			game1.startButton.events.onInputUp.add(btnStartUp, this);
			game1.startButton.events.onInputOver.add(function(){
				if(game1.spinStatus)
					return;
				game1.startButton.loadTexture('start_p');
			});
			game1.startButton.events.onInputOut.add(function(){
				if(game1.spinStatus)
					return;
				game1.startButton.loadTexture('start');
			});

			game1.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');        
			game1.automatic_start.scale.setTo(0.7, 0.7);
			game1.automatic_start.inputEnabled = true;
			game1.automatic_start.input.useHandCursor = true;
			game1.automatic_start.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.automatic_start.loadTexture('automatic_start_p');
			});
			game1.automatic_start.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.automatic_start.loadTexture('automatic_start');
			});
			game1.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
			game1.bet_max.scale.setTo(0.7, 0.7);
			game1.bet_max.inputEnabled = true;
			game1.bet_max.input.useHandCursor = true;
			game1.bet_max.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_max.loadTexture('bet_max_p');
			});
			game1.bet_max.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_max.loadTexture('bet_max');
			});
			game1.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
			game1.bet_one.scale.setTo(0.7, 0.7);
			game1.bet_one.inputEnabled = true;
			game1.bet_one.input.useHandCursor = true;
			game1.bet_one.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_one.loadTexture('bet_one_p');
			});
			game1.bet_one.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_one.loadTexture('bet_one');
			});
			game1.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
			game1.paytable.scale.setTo(0.7, 0.7);
			game1.paytable.inputEnabled = true;
			game1.paytable.input.useHandCursor = true;
			game1.paytable.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.paytable.loadTexture('paytable_p');
			});
			game1.paytable.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.paytable.loadTexture('paytable');
			});
			game1.paytable.events.onInputUp.add(function(){
				game1.pages[1].visible = true;
				prev_page.visible = true;
				exit_btn.visible = true;
				next_page.visible = true;
				game1.settingsMode = true;
				game1.currentPage = 1;
				game1.automatic_start.loadTexture('automatic_start_d');
				game1.bet_max.loadTexture('bet_max_d');
				game1.bet_one.loadTexture('bet_one_d');
				game1.paytable.loadTexture('paytable_d');
				game1.select_game.loadTexture('select_game_d');
				game1.lines[3].button.loadTexture('btnline_d' + 3);
				game1.lines[5].button.loadTexture('btnline_d' + 5);
				game1.lines[7].button.loadTexture('btnline_d' + 7);
			}, this);
			game1.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
			game1.select_game.scale.setTo(0.7, 0.7);
			game1.select_game.inputEnabled = true;
			game1.select_game.input.useHandCursor = true;
			game1.select_game.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.select_game.loadTexture('select_game_p');
			});
			game1.select_game.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.select_game.loadTexture('select_game');
			});
		}
		stopSound = game.add.audio('stop');
		rotateSound = game.add.audio('rotate');
		tadaSound = game.add.audio('tada');
		pageSound = game.add.audio('page');
		rowWinSound = game.add.audio('rowWin');
		takeWinSound = game.add.audio('takeWin');
		takeWinSound.addMarker('take', 0, 0.6);
		rotateSound.loop = true;

		if(!isMobile){
			game1.lines[1].button.events.onInputUp.add(function () {
				if (game1.settingsMode)  {
					pageSound.play();
					if (game1.currentPage == 1)
						game1.currentPage = 6;
					else{
						game1.pages[game1.currentPage].visible = false;
						game1.currentPage -=1;
					}
					game1.pages[game1.currentPage].visible = true;
				}
			});      
			game1.lines[9].button.events.onInputUp.add(function () {
				if (game1.settingsMode)  {
					pageSound.play();
					if (game1.currentPage == 6){
						game1.pages[game1.currentPage].visible = false;
						game1.currentPage = 1;
					} else if (game1.currentPage == 1){
						game1.currentPage +=1;
					} else {                    
						game1.pages[game1.currentPage].visible = false;
						game1.currentPage +=1;
					}
					game1.pages[game1.currentPage].visible = true;
				}
			});
			game1.lines[1].button.events.onInputOver.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[1].button.loadTexture('btnline_p' + 1);
			});
			game1.lines[1].button.events.onInputOut.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[1].button.loadTexture('btnline' + 1);
			});
			game1.lines[9].button.events.onInputOver.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[9].button.loadTexture('btnline_p' + 9);
			});
			game1.lines[9].button.events.onInputOut.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[9].button.loadTexture('btnline' + 9);
			});
		}
		function btnStartUp(){
			if(game1.spinStatus)
				return;
			if (game1.settingsMode){
				pageSound.play();
				for (var i = 1; i <= 6; ++i) {
					game1.pages[i].visible = false;
				}            
				prev_page.visible = false;
				exit_btn.visible = false;
				next_page.visible = false;
				game1.settingsMode = false;
				unDisableLines()
				game1.automatic_start.loadTexture('automatic_start');
				game1.bet_max.loadTexture('bet_max');
				game1.bet_one.loadTexture('bet_one');
				game1.paytable.loadTexture('paytable');
				game1.select_game.loadTexture('select_game');
			} else{
				if (game1.takeWin){
					game1.takeWin = false;
					unDisableLines();
					// selectLine(game1.currentLine);
					clearInterval(flickerInterval);
					takeWinSound.play('take');
				} else{
					if (!isMobile){
						game1.startButton.loadTexture('start_d');
						game1.automatic_start.loadTexture('automatic_start_d');
						game1.bet_max.loadTexture('bet_max_d');
						game1.bet_one.loadTexture('bet_one_d');
						game1.paytable.loadTexture('paytable_d');
						game1.select_game.loadTexture('select_game_d');
						disableLinesBtn();
					} else   
					button.loadTexture('spin');
					game1.countPlayBars = 5;
					game1.barsCurrentSpins = [0, 0, 0, 0, 0];
					game1.spinStatus = true;
					hideLines();
					rotateSound.play();
				}
			}
		};
		function hideLines() {
			for (var i = 1; i <= 9; ++i) {
				game1.lines[i].sprite.visible = false;
			}
		}   
		createLevelButtons();
		game1.strawberry_bar_anim_1Animation.onComplete.add(function(){
			game.state.start('game3');
		});
		if (isMobile)
			game1.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
		preselectLine(game1.currentLine);     
	},
	update:function(){
		if (game1.spinStatus){
			for (var i in game1.bars) {
				game1.barsCurrentSpins[i]++;
				if (game1.barsCurrentSpins[i] < game1.barsTotalSpins[i]) {
					game1.bars[i].tilePosition.y += 112;					
				} else if (game1.barsCurrentSpins[i] == game1.barsTotalSpins[i]) {
					game1.countPlayBars--;
					stopSound.play();
				}
			}
			if (game1.countPlayBars === 0){
				game1.spinStatus = false;
				rotateSound.stop();
				if (!isMobile){   
					game1.startButton.loadTexture('start');
					game1.automatic_start.loadTexture('automatic_start');
					game1.bet_max.loadTexture('bet_max');
					game1.bet_one.loadTexture('bet_one');
					game1.paytable.loadTexture('paytable');
					game1.select_game.loadTexture('select_game');
				}
				if(game1.currentLine == 3)   {
					game1.takeWin = true;
					winLine(3);
					tadaSound.play();
					game1.win.visible = true;
					game1.winAnimation.play();
				} else if (game1.currentLine == 5){
					rowWinSound.play();
					game1.strawberry_bar_anim_1Animation.play();
					game1.strawberry_bar_anim_1.visible = true;
					game1.strawberry_bar_anim_2Animation.play();
					game1.strawberry_bar_anim_2.visible = true;
					game1.strawberry_bar_anim_3Animation.play();
					game1.strawberry_bar_anim_3.visible = true;
				} else {
					if(!isMobile){  
						unDisableLines();
					}
					// selectLine(game1.currentLine);   
				}
			}
		}
	}
};
game.state.add('game1', game1); 
var game2 = {
	cards : [],
	ropePositionX : 254,
	cardValues : {1:5, 2:7, 3:12, 4:13, 5:15},
	cardIndexes : null,
	create : function() {
		game2.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top1');        
		game.add.sprite(94+mobileX,22+mobileY, 'bg_top2');            
		game1.pirate_win = game.add.sprite(110+mobileX, 358+mobileY, 'game1.pirate_win');       
		pickCardSound = game.add.audio('pickCard');
		cardWin = game.add.audio('cardWin');
		for(var i=1; i<=4; ++i) {
			game2.cards[i] = game.add.sprite(game2.ropePositionX+mobileX, 150+mobileY, 'shirt_cards');
			game2.cards[i].inputEnabled = true;
			game2.cards[i].input.useHandCursor = true;
			game2.ropePositionX += 112;
		}
		var buttonsPositionsX = {1:{pos:295,i:3}, 2:{pos:340,i:5}, 3:{pos:385,i:7}, 4:{pos:430,i:9}};
		var buttons = {};
        // var cardIndexes = {1 : '2b', 2: '2c', 3: '2p', 4: '2t'};        
        game2.cardIndexes = shuffle(Object.keys(game2.cardValues));
        game2.dealer_card = game.add.sprite(126+mobileX, 150+mobileY, 'card_'+game2.cardValues[game2.cardIndexes[0]]);
        // game.add.sprite(113+mobileX, 317+mobileY, 'dealer');
        // game.add.sprite(141+mobileX, 134+mobileY, 'border_card'); 
        // game2.win_bg = game.add.sprite(238+mobileX, 406+mobileY, 'game2.win_bg');
        // game.add.sprite(206+mobileX, 118+mobileY, 'border_card');  	

        game2.bg2 = game.add.sprite(94+mobileX,54+mobileY, 'game2.bg');
        game2.water_anim = game.add.sprite(542+mobileX, 486+mobileY, 'water_anim');
        game2.water_animAnimation = game2.water_anim.animations.add('water_anim', [0,1,2,3], 8, true);
        game2.water_animAnimation.play();	
        game2.water_bottom_1 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_1');
        game2.water_bottom_1Animation = game2.water_bottom_1.animations.add('water_bottom_1', [0,1,2,3,4,5,6,7,8,9], 8, false);
        game2.water_bottom_1Animation.play();	
        game2.water_bottom_2 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_2');
        game2.water_bottom_2Animation = game2.water_bottom_2.animations.add('water_bottom_2', [0,1,2,3], 8, false);
        game2.water_bottom_2.visible = false;	
        game2.man_1 = game.add.sprite(94+mobileX, 390+mobileY, 'game1.man_1');
        game2.man_1Animation = game2.man_1.animations.add('game1.man_1', [0,1,2,3,4,5,6,7,8,9,10], 8, true);
        game2.man_1Animation.play();	
        game2.bird_anim_1 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_1');
        game2.bird_anim_1Animation = game2.bird_anim_1.animations.add('bird_anim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44], 8, false);
        game2.bird_anim_1Animation.play();

        game2.bird_anim_2 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_2');
        game2.bird_anim_2Animation = game2.bird_anim_2.animations.add('bird_anim_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 8, false);
        game2.bird_anim_2.visible = false;
        game2.bird_anim_1Animation.onComplete.add(function(){
        	game2.bird_anim_1.visible = false;
        	game2.bird_anim_2.visible = true;
        	game2.bird_anim_2Animation.play();
        });
        game2.bird_anim_2Animation.onComplete.add(function(){
        	game2.bird_anim_1.visible = true;
        	game2.bird_anim_2.visible = false;
        	game2.bird_anim_1Animation.play();
        });
        game2.water_bottom_1Animation.onComplete.add(function(){
        	game2.water_bottom_1.visible = false;
        	game2.water_bottom_2.visible = true;
        	game2.water_bottom_2Animation.play();
        });
        game2.water_bottom_2Animation.onComplete.add(function(){
        	game2.water_bottom_1.visible = true;
        	game2.water_bottom_2.visible = false;
        	game2.water_bottom_1Animation.play();
        });
        if(!isMobile){
        	game.add.sprite(0,0, 'main_window');
        }
        if(!isMobile)
        	game.add.sprite(250+mobileX, 510+mobileY, 'btnline_d1').scale.setTo(0.7, 0.7);
        for(var i in buttonsPositionsX) {
        	if(!isMobile){
        		var button = game.add.sprite(buttonsPositionsX[i].pos+mobileX, 510+mobileY, 'btnline' + buttonsPositionsX[i].i);
        		button.scale.setTo(0.7, 0.7);
        		button.inputEnabled = true;
        		button.input.useHandCursor = true;
        		(function(n, btn, pic){
        			btn.events.onInputUp.add(function () {
        				var card = game2.cards[n];
        				card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
        				// 268 311
        				game.add.sprite(card.position.x+14+mobileX, 311+mobileY, 'pick_card'); 
        				game2.lose = game.add.sprite(185+mobileX, 345+mobileY, 'game2.win_lose');
        				game2.loseAnimation = game2.lose.animations.add('game2.lose', [0,1,3,3,3,3,3,3,3,3,3,3], 5, false);
        				game2.lose.visible = false;
        				game2.win = game.add.sprite(185+mobileX, 345+mobileY, 'game2.win_lose');
        				game2.winAnimation = game2.win.animations.add('game2.win_lose', [0,1,2,2,2,2,2,2,2,2,2,2], 5, false);
        				game2.win.visible = false;                   
        				game2.forward = game.add.sprite(185+mobileX, 345+mobileY, 'game2.win_lose');
        				game2.forwardAnimation = game2.forward.animations.add('game2.win_lose', [0,1,4,4,4,4,4,4,4,4,4,4], 5, false);
        				game2.forward.visible = false;       			
        				game2.winAnimation.onComplete.add(function(){
        					game.state.start('game2');        
        					game2.ropePositionX = 254;
        				});       
        				game2.forwardAnimation.onComplete.add(function(){
        					game.state.start('game2');        
        					game2.ropePositionX = 254;
        				});      
        				game2.loseAnimation.onComplete.add(function(){
        					game.state.start('game1'); 
        				});   
        				pickCardSound.play();
        				var dealerValue = game2.cardValues[game2.cardIndexes[0]];
        				var userValue = game2.cardValues[game2.cardIndexes[n]];
        				console.log('dealer ' + dealerValue);
        				console.log('user ' + userValue);
        				if(dealerValue < userValue) { 
        					game2.win.visible = true;
        					game2.winAnimation.play();
        					cardWin.play();
        				} else if(dealerValue == userValue) {
        					game2.forward.visible = true;
        					game2.forwardAnimation.play();
        				} else {
        					game2.lose.visible = true;
        					game2.loseAnimation.play();
        				}

        				for(var c in game2.cards) {
        					game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
        				}
        				for(var b in buttons) {
        					buttons[b].loadTexture('btnline_d' + b);
        					buttons[b].inputEnabled = false;
        				}


        			}, this);
        		})(i, button, buttonsPositionsX[i].i);
        	} else {
        		(function(n, btn, pic){
        			btn.events.onInputUp.add(function () {
        				var card = game2.cards[n];
        				card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
        				game.add.sprite(card.position.x-18+(n-1)*1.1, 288+mobileY, 'pick_card');   
        				game2.lose = game.add.sprite(190+mobileX, 310+mobileY, 'game2.win_lose');
        				game2.loseAnimation = game2.lose.animations.add('game2.lose', [0,1,3,3,3,3,3,3,3,3,3,3], 5, false);
        				game2.lose.visible = false;
        				game2.win = game.add.sprite(190+mobileX, 310+mobileY, 'game2.win_lose');
        				game2.winAnimation = game2.win.animations.add('game2.win_lose', [0,1,2,2,2,2,2,2,2,2,2,2], 5, false);
        				game2.win.visible = false;                   
        				game2.forward = game.add.sprite(190+mobileX, 310+mobileY, 'game2.win_lose');
        				game2.forwardAnimation = game2.forward.animations.add('game2.win_lose', [0,1,4,4,4,4,4,4,4,4,4,4], 5, false);
        				game2.forward.visible = false;      				
        				game2.winAnimation.onComplete.add(function(){
        					game.state.start('game2');        
        					game2.ropePositionX = 254;
        				});           
        				game2.forwardAnimation.onComplete.add(function(){
        					game.state.start('game2');        
        					game2.ropePositionX = 254;
        				});         
        				game2.loseAnimation.onComplete.add(function(){
        					game.state.start('game1'); 
        				});   
        				pickCardSound.play();
        				var dealerValue = game2.cardValues[game2.cardIndexes[0]];
        				var userValue = game2.cardValues[game2.cardIndexes[n]];

        				if(dealerValue < userValue) { 
        					game2.win.visible = true;
        					game2.winAnimation.play();
        					cardWin.play();
        				} else if(dealerValue == userValue) {
        					game2.forward.visible = true;
        					game2.forwardAnimation.play();
        				} else {
        					game2.lose.visible = true;
        					game2.loseAnimation.play();
        				}

        				for(var c in game2.cards) {
        					game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
        				}
        				for(var b in game2.cards) {
        					game2.cards[b].inputEnabled = false;
        				}                        

        			}, this);
        		})(i, game2.cards[i], buttonsPositionsX[i].i)
        	}


        	if (!isMobile){
        		buttons[buttonsPositionsX[i].i] = button;
        	} 
        }     
        if(!isMobile){
        	game2.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
        	game2.startButton.scale.setTo(0.7, 0.7);
        	game2.startButton.inputEnabled = true;
        	game2.startButton.input.useHandCursor = true;
        	game2.startButton.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.startButton.loadTexture('start_p');
        	});
        	game2.startButton.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.startButton.loadTexture('start');
        	});
        	game2.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
        	game2.automatic_start.scale.setTo(0.7, 0.7);
        	game2.automatic_start.inputEnabled = true;
        	game2.automatic_start.input.useHandCursor = true;
        	game2.automatic_start.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.automatic_start.loadTexture('automatic_start_p');
        	});
        	game2.automatic_start.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.automatic_start.loadTexture('automatic_start');
        	});
        	game2.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
        	game2.bet_max.scale.setTo(0.7, 0.7);
        	game2.bet_max.inputEnabled = true;
        	game2.bet_max.input.useHandCursor = true;
        	game2.bet_max.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_max.loadTexture('bet_max_p');
        	});
        	game2.bet_max.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_max.loadTexture('bet_max');
        	});
        	game2.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
        	game2.bet_one.scale.setTo(0.7, 0.7);
        	game2.bet_one.inputEnabled = true;
        	game2.bet_one.input.useHandCursor = true;
        	game2.bet_one.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_one.loadTexture('bet_one_p');
        	});
        	game2.bet_one.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_one.loadTexture('bet_one');
        	});
        	game2.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
        	game2.paytable.scale.setTo(0.7, 0.7);
        	game2.paytable.inputEnabled = true;
        	game2.paytable.input.useHandCursor = true;
        	game2.paytable.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.paytable.loadTexture('paytable_p');
        	});
        	game2.paytable.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.paytable.loadTexture('paytable');
        	});
        	game2.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
        	game2.select_game.scale.setTo(0.7, 0.7);
        	game2.select_game.inputEnabled = true;
        	game2.select_game.input.useHandCursor = true;
        	game2.select_game.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.select_game.loadTexture('select_game_p');
        	});
        	game2.select_game.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.select_game.loadTexture('select_game');
        	});
        }
        if (isMobile)
        	game2.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
        createLevelButtons();
        if(!isMobile)
        	full_and_sound();
      }
    };
    game.state.add('game2', game2);
var game3 = {
	freeze : false,
	climb : false,
	buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
	ropesafeCount : 5,
	menPositionsX : {1:94, 3:222, 5:350, 7:478, 9:606},
	selectedRopeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
	barrel1: [3,4,5,9],
	barrel2: [1,2,6,7,8,10],
	baller_open : {1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0},
	selectedSafe : null,
	safe_hit_arr : [],
	barrel_arr : [],
	barrel_arr_open : null,
	barrel_top_arr : null,
	barrel_lose_arr : null,
	barrel_d_arr : null,
	spin_dice : true,
	deck1_val1 : null,
	deck1_val2 : null,
	dice_arr : [],
	dice_d_arr : [],
	pick_dice : null,
	bars_dice: [],
	currentbarrel : 10,
	stone_arr : [],
	hook_arr : [],
	blue_screen_arr : [],
	hookfinal_arr : [],
	bottom_bg_arr : [],
	safe_door_arr : [],
	safe_door_arr_anim : [],
	barrel_arr_i : null,
	tween1: null,
	tween2: null,
	tween3: null,
	tween4: null,
	bg_main : null,
	floor: 1,
	selectedRope : null,
	button : [],
	push_baller : [],
	currentBar : 1,
	bars_line: [],
	barsCurrentSpins : [0, 0, 0],
	barsTotalSpins : [27, 51, 75],
	countPlayBars : 0,
	bar_arr: [],
	bar_arr_w: [],
	baller_count : null,
	first_spin : true,
	meat_arr: [],
	pick_meat: null,

	create : function() {    	
		stopSound = game.add.audio('stop');
		rotateSound = game.add.audio('rotate');		

		game3.bg = game.add.sprite(94+mobileX,54+mobileY, 'game3.bg');    	
		for (var i = 0; i <= 1; ++i) {
			game3.meat_arr[i] = game.add.sprite(260+mobileX+i*256, 54+mobileY, 'game3.meat');
			game3.meat_arr[i].animations.add('game3.meat', [0,1,2,3,4,5,6,7,8,9,10,11,10,9,8,7,6,5,4,3,2,1], 8, true).play();
			game3.meat_arr[i].inputEnabled = true;
		}
		if(!isMobile){
			game.add.sprite(0,0, 'main_window');
		}
    		//
//game3.barrel_lose_arr.animations.getAnimation('barrel_lose').stop();
    		//
    		game3.shadow1 = game.add.sprite(286+mobileX, 374+mobileY, 'shadow1');
    		game3.shadow2 = game.add.sprite(94+mobileX, 374+mobileY, 'shadow2');
    		// game3.shadow2.visible = false;
    		// game3.shadow3 = game.add.sprite(286+mobileX, 374+mobileY, 'shadow3');
    		// game3.shadow3.visible = false;
    		game3.pick = game.add.sprite(342+mobileX, 198+mobileY, 'game3.pick');
    		game3.pick.animations.add('game3.pick', [0,1,2,3,4,5,6,5,6,5,6,4,3,2,1,0,0,0,7,8,9,10,11,12,11,12,11,12,10,9,8,7,0,0,0], 8, true).play();
    		game3.get_meat_star = game.add.sprite(132+mobileX, 43+mobileY, 'game3.get_meat_star');
    		game3.get_meat_star.animations.add('game3.get_meat_star', [0,1,2,3,4,5], 8, false);
    		game3.get_meat_star.visible = false;
    		game3.get_meat_win = game.add.sprite(132+mobileX, 43+mobileY, 'game3.get_meat_win');
    		game3.get_meat_win.animations.add('game3.get_meat_win', [0,1,2], 8, false);
    		game3.get_meat_win.visible = false;
    		game3.get_meat_lose_1 = game.add.sprite(388+mobileX, 43+mobileY, 'game3.get_meat_lose_1');
    		game3.get_meat_lose_1.animations.add('game3.get_meat_lose_1', [0,1,2], 8, false);
    		game3.get_meat_lose_1.visible = false;
    		game3.get_meat_lose_2 = game.add.sprite(388+mobileX, 43+mobileY, 'game3.get_meat_lose_2');
    		game3.get_meat_lose_2.animations.add('game3.get_meat_lose_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 8, false);
    		game3.get_meat_lose_2.visible = false;
    		game3.get_meat_star.animations.getAnimation('game3.get_meat_star').onComplete.add(function(){
    			game3.get_meat_star.visible = false ;
    			if (game3.pick_meat == 1) {
    				game3.get_meat_win.visible = true;
    				game3.get_meat_win.animations.getAnimation('game3.get_meat_win').play();    				
    			} else {
    				game3.get_meat_lose_1.visible = true;
    				game3.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').play();
    			}
    		});
    		game3.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').onComplete.add(function(){
    			game3.get_meat_lose_1.visible = false ;
    			game3.get_meat_lose_2.visible = true;
    			game3.get_meat_lose_2.animations.getAnimation('game3.get_meat_lose_2').play();
    		});    			
    		game3.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top3');
    		for(var i in game3.buttonsPositionsX) {
    			if(!isMobile){
    				game3.button[i] = game.add.sprite(game3.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline' + i);
    				game3.button[i].scale.setTo(0.7, 0.7);
    				if (i == 3 || i == 5 || i == 7){
    					game3.button[i] = game.add.sprite(game3.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline_d' + i);
    					game3.button[i].scale.setTo(0.7, 0.7);
    					game3.button[i].inputEnabled = false;
    				} else {             
    					game3.button[i].inputEnabled = true;
    					game3.button[i].input.useHandCursor = true;
    					(function(n, btn){
    						btn.events.onInputUp.add(function () {
    							if(game3.freeze || n==7 || n==3 || n==5) {
    								return;
    							} 
    							game3.freeze = true;
    							//game3.pick_meat = n;
    							game3.pick.visible = false;
    							game3.get_meat_star.visible = true;
    							if (n == 1){
    								btn.loadTexture('btnline_d' + 1); 
    								game3.button[9].loadTexture('btnline_d' + 9);    								
    								game3.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
    								game3.meat_arr[0].visible = false;
    							} else {      						
    								btn.loadTexture('btnline_d' + 9); 
    								game3.button[1].loadTexture('btnline_d' + 1);
    								game3.get_meat_star.position.x = 388+mobileX;
    								game3.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
    								game3.meat_arr[1].visible = false;
    							}      					
    						}, this);
    					})(i, game3.button[i]);
    				}
    			} 
    		}
    		if(isMobile){
    			for (var j = 0; j <= 1; ++j) {    					
    				(function(n, btn){
    					btn.events.onInputUp.add(function () {
    						if(game3.freeze) {
    							return;
    						} 
    						game3.freeze = true;
    						game3.pick_meat = n+1;
    						game3.pick.visible = false;
    						game3.get_meat_star.visible = true;
    						if (n == 0){	
    							console.log('left')				
    							game3.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
    							game3.meat_arr[0].visible = false;
    						} else {      						
    							game3.get_meat_star.position.x = 388+mobileX;
    							game3.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
    							game3.meat_arr[1].visible = false;
    						}      					
    					}, this);
    				})(j, game3.meat_arr[j]);
    			}
    		}
    		if(!isMobile){
    			game3.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
    			game3.startButton.scale.setTo(0.7, 0.7);
    			game3.startButton.inputEnabled = true;
    			game3.startButton.input.useHandCursor = true;
    			game3.startButton.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.startButton.loadTexture('start_p');
    			});
    			game3.startButton.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.startButton.loadTexture('start');
    			});
    			game3.startButton.events.onInputUp.add(function(){   
    			});
    			game3.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
    			game3.automatic_start.scale.setTo(0.7, 0.7);
    			game3.automatic_start.inputEnabled = true;
    			game3.automatic_start.input.useHandCursor = true;
    			game3.automatic_start.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.automatic_start.loadTexture('automatic_start_p');
    			});
    			game3.automatic_start.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.automatic_start.loadTexture('automatic_start');
    			});
    			game3.automatic_start.events.onInputUp.add(function(){ 
    			});
    			game3.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
    			game3.bet_max.scale.setTo(0.7, 0.7);
    			game3.bet_max.inputEnabled = true;
    			game3.bet_max.input.useHandCursor = true;
    			game3.bet_max.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_max.loadTexture('bet_max_p');
    			});
    			game3.bet_max.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_max.loadTexture('bet_max');
    			});
    			game3.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
    			game3.bet_one.scale.setTo(0.7, 0.7);
    			game3.bet_one.inputEnabled = true;
    			game3.bet_one.input.useHandCursor = true;
    			game3.bet_one.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_one.loadTexture('bet_one_p');
    			});
    			game3.bet_one.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_one.loadTexture('bet_one');
    			});
    			game3.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
    			game3.paytable.scale.setTo(0.7, 0.7);
    			game3.paytable.inputEnabled = true;
    			game3.paytable.input.useHandCursor = true;
    			game3.paytable.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.paytable.loadTexture('paytable_p');
    			});
    			game3.paytable.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.paytable.loadTexture('paytable');
    			});
    			game3.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
    			game3.select_game.scale.setTo(0.7, 0.7);
    			game3.select_game.inputEnabled = true;
    			game3.select_game.input.useHandCursor = true;
    			game3.select_game.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.select_game.loadTexture('select_game_p');
    			});
    			game3.select_game.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.select_game.loadTexture('select_game');
    			});
    		};
    		createLevelButtons();
    		game3.countPlayBars = 3;
    		game3.spinStatus = true;
    		if (isMobile)
    			game3.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
    		if(!isMobile)
    			full_and_sound();
    	}
    };
    game.state.add('game3', game3);
    var game4 = {
    	arrow_arr : [],
    	dolphin_arr : [],
    	shark_arr : [],
    	fin_arr : [],
    	button : [],
    	pick_line : null,
    	selectedRopeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
    	buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
    	create : function() {    	

    		game4.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top4');
    		game4.bg = game.add.sprite(94+mobileX,22+mobileY, 'game4.bg');  
    		for (var i = 1; i <= 5; ++i) {
    			game4.arrow_arr[i] = game.add.sprite(130+mobileX+(i-1)*127, 272+mobileY, 'game4.arrow');
    			game4.arrow_arr[i].animations.add('game4.arrow', [0,1,2,3,4], 6, true).play();
    			if (i % 2 != 0) {
    				game4.fin_arr[i] = game.add.sprite(104+mobileX+(i-1)*127, 214+mobileY, 'game4.fin');
    				game4.fin_arr[i].animations.add('game4.fin', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, true).play();
    				game4.fin_arr[i].inputEnabled = true;
    			} else {
    				game4.fin_arr[i] = game.add.sprite(104+mobileX+(i-1)*127, 214+mobileY, 'game4.fin');
    				game4.fin_arr[i].animations.add('game4.fin', [7,8,9,10,11,12,0,1,2,3,4,5,6], 12, true).play();
    				game4.fin_arr[i].inputEnabled = true;
    			}
    		}

    		game4.water = game.add.sprite(94+mobileX, 256+mobileY, 'game4.water');
    		game4.water.animations.add('game4.water', [0,1,2,3,4,5], 12, true).play();
    		game4.dolphin = game.add.sprite(104+mobileX, 170+mobileY, 'game4.dolphin');
    		game4.dolphin.animations.add('game4.dolphin', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, false);
    		game4.dolphin.visible = false;
    		game4.shark = game.add.sprite(130+mobileX, 170+mobileY, 'game4.shark');
    		game4.shark.animations.add('game4.shark', [0,1,2,3,4,5,6,7,8,9,10,11,12,12,12,12,12,12], 8, false);
    		game4.shark.visible = false;
    		game4.captain_lose = game.add.sprite(355+mobileX, 340+mobileY, 'game4.captain_lose');
    		game4.captain_lose.animations.add('game4.captain_lose', [0,1,2,3,4,5,6,7,8,9], 8, false);
    		game4.captain_lose.visible = false;   
    		game4.captain_pick = game.add.sprite(355+mobileX, 327+mobileY, 'game4.captain_pick');
    		game4.captain_pick.animations.add('game4.captain_pick', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,12,11,10,9,8,8,8,8,14,15,16,17,18,19,20,21,22,23,24,0,0,0], 8, true).play();
    		game4.captain_swim_1 = game.add.sprite(355+mobileX, 327+mobileY, 'game4.captain_swim_1');
    		game4.captain_swim_1.animations.add('game4.captain_swim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 10, false);
    		game4.captain_swim_1.visible = false;
    		game4.captain_win = game.add.sprite(355+mobileX, 327+mobileY, 'game4.captain_win');
    		game4.captain_win.animations.add('game4.captain_win', [0,1,2,3,4], 8, false);
    		game4.captain_win.visible = false;
    		game4.fin_left_end = game.add.sprite(104+mobileX, 199+mobileY, 'game4.fin_left_end');
    		game4.fin_left_end.animations.add('game4.fin_left_end', [0,1,2,3,4,5], 12, false);
    		game4.fin_left_end.visible = false;
    		game4.fin_right_end = game.add.sprite(104+mobileX, 199+mobileY, 'game4.fin_right_end');
    		game4.fin_right_end.animations.add('game4.fin_right_end', [0,1,2,3,4,5,6], 12, false);  
    		game4.fin_right_end.visible = false;      
    		game4.fin_end = game.add.sprite(104+mobileX, 214+mobileY, 'game4.fin_end');
    		game4.fin_end.animations.add('game4.fin_end', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, false);  
    		game4.fin_end.visible = false;  
    		game4.captain_under = game.add.sprite(307+mobileX, 407+mobileY, 'game4.captain_under');
    		game4.captain_under.animations.add('game4.captain_under', [0,1,2,3,4,5], 8, true).play();
    		game4.captain_swim_2 = game.add.sprite(37+mobileX, 327+mobileY, 'game4.captain_swim_2');
    		game4.captain_swim_2.animations.add('game4.captain_swim_2', [0,1,2,3,4,5,0,1,2,3,4,5,0], 12, false);
    		game4.captain_swim_2.visible = false;

    		if(!isMobile){
    			game.add.sprite(0,0, 'main_window');
    		};
    		for(var i in game4.buttonsPositionsX) {
    			if(!isMobile){
    				game4.button[i] = game.add.sprite(game4.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline' + i);
    				game4.button[i].scale.setTo(0.7, 0.7);           
    				game4.button[i].inputEnabled = true;
    				game4.button[i].input.useHandCursor = true;
    				(function(n, btn){
    					btn.events.onInputUp.add(function () {
    						if(game4.freeze) {
    							return;
    						} 
    						game4.freeze = true;
    						game4.pick_line = game4.selectedRopeNormal[n];
    						btn.loadTexture('btnline_d' + n); 
    						game4.fin_arr[game4.pick_line].visible = false;
    						game4.captain_pick.visible = false;
    						for (var i = 1; i <= 5; ++i) {
    							game4.arrow_arr[i].visible = false;
    						};
    						game4.captain_swim_1.position.x = 101 + (game4.pick_line-1)*127;
    						game4.captain_under.position.x = 53 + (game4.pick_line-1)*127;
    						game4.captain_swim_1.visible = true;
    						game4.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
    							game4.captain_swim_1.visible = false;
    							game4.captain_swim_2.position.x = 37 + (game4.pick_line-1)*127;
    							game4.captain_swim_2.visible = true;
    							game4.fin_end.position.x = 104 + (game4.pick_line-1)*127;
    							game4.fin_end.visible = true;    							
    							game4.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
    								game4.fin_end.visible = false;
    								if (game4.pick_line == 3) {
    									game4.shark.position.x = 104 + (game4.pick_line-1)*127;
    									game4.shark.visible = true;
    									game4.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
    										game.state.start('game1');
    									});
    								} else {    				
    									game4.dolphin.position.x = 104 + (game4.pick_line-1)*127;
    									game4.dolphin.visible = true;
    									game4.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
    										game4.dolphin.visible = false;
    										game4.captain_swim_2.visible = false;
    										game4.captain_pick.visible = true;
    										game4.captain_pick.position.x = 101 + (game4.pick_line-1)*127;
    										game4.fin_end.visible = false;  
    										game4.freeze = false;
    									});
    								}
    							});
    							game4.fin_end.animations.getAnimation('game4.fin_end').play();
    						});
    					}, this);
    				})(i, game4.button[i]);
    			}
    		}
    		
    	}		
    };
    game.state.add('game4', game4);
    var gamePreload = {

    	preload:function(){
    		if (isMobile){
    			game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
    				if(progress % 8 == 0) {
    					document.getElementById('preloaderBar').style.width = progress + '%';                    
    				}
    			});
    		}
    		game.scale.scaleMode = 2;
    		game.scale.pageAlignHorizontally = true;
    		game.scale.pageAlignVertically = true;
    		game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        // game.load.image('main_window', 'img/main_window.png');
        if(!isMobile){
        	game.load.image('main_window', 'img/shape1206.svg');
        }
        game.load.image('game1.bg', 'img/main_bg.png');
        game.load.image('game2.bg', 'img/bg_game2.png');
        game.load.image('game3.bg', 'img/game3_bg.png');
        game.load.image('game4.bg', 'img/bg_game4.png');
        game.load.image('bg_top1', 'img/main_bg_top1.png');        
        game.load.image('bg_top2', 'img/main_bg_top2.png');        
        game.load.image('bg_top3', 'img/main_bg_top3.png');        
        game.load.image('bg_top4', 'img/main_bg_top4.png');        
        game.load.image('bg_top3_2', 'img/main_bg_top3_2.png');        
        game.load.image('bg_3_2', 'img/bg_3_2.png');        
        game.load.image('game1.bottom_line', 'img/bottom_line.png');
        game.load.image('x', 'img/x.png');
        for (var i = 1; i <= 6; ++i) {
        	game.load.image('game1.page_' + i, 'img/page_' + i + '.png');
        }

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');

        game.load.spritesheet('bonus', 'img/bonus.png', 96, 96, 8);
        game.load.spritesheet('game1.pirate_win', 'img/pirate_win.png', 224, 144, 18);

        game.load.spritesheet('bird_anim_1', 'img/bird_anim_1_80x96_45.png', 80, 96, 45);
        game.load.spritesheet('bird_anim_2', 'img/bird_anim_2_80x96_22.png', 80, 96, 22);
        game.load.spritesheet('water_anim', 'img/water_anim_192x16_4.png', 192, 16, 4);
        game.load.spritesheet('water_bottom_1', 'img/water_bottom_1_368x32_10.png', 368, 32, 10);
        game.load.spritesheet('water_bottom_2', 'img/water_bottom_2_368x32_4.png', 368, 32, 4);
        game.load.spritesheet('game1.man_1', 'img/game1.man_1_368_112_11.png', 368, 112, 11);
        game.load.spritesheet('game1.win', 'img/game1.win_368_112_6.png', 368, 112, 6);
        
        game.load.image('game1.bar', 'img/shape_fullv3.png');
        game.load.image('dice_spin', 'img/dice_spin.png');
        game.load.image('border_card', 'img/border.png');
        if(!isMobile){
        	game.load.image('start', 'img/btns/btn_start.png');
        	game.load.image('start_p', 'img/btns/btn_start_p.png');
        	game.load.image('start_d', 'img/btns/btn_start_d.png');
        	game.load.image('automatic_start', 'img/btns/btn_automatic_start.png');
        	game.load.image('automatic_start_p', 'img/btns/btn_automatic_start_p.png');
        	game.load.image('automatic_start_d', 'img/btns/btn_automatic_start_d.png');
        	game.load.image('bet_max', 'img/btns/btn_bet_max.png');
        	game.load.image('bet_max_p', 'img/btns/btn_bet_max_p.png');
        	game.load.image('bet_max_d', 'img/btns/btn_bet_max_d.png');
        	game.load.image('bet_one', 'img/btns/btn_bet_one.png');
        	game.load.image('bet_one_p', 'img/btns/btn_bet_one_p.png');
        	game.load.image('bet_one_d', 'img/btns/btn_bet_one_d.png');
        	game.load.image('paytable', 'img/btns/btn_paytable.png');
        	game.load.image('paytable_p', 'img/btns/btn_paytable_p.png');
        	game.load.image('paytable_d', 'img/btns/btn_paytable_d.png');
        	game.load.image('select_game', 'img/btns/btn_select_game.png');
        	game.load.image('select_gamev2', 'img/btns/btn_select_gamev2.png');
        	game.load.image('select_game_p', 'img/btns/btn_select_game_p.png');
        	game.load.image('select_game_d', 'img/btns/btn_select_game_d.png');
        	game.load.image('non_full', 'img/non_full.png');
        	game.load.image('full', 'img/full.png');
        	game.load.image('sound_on', 'img/sound_on.png');
        	game.load.image('sound_off', 'img/sound_off.png');
        }
        if(isMobile) {
        	game.load.image('spin', 'img/spin.png');
        	game.load.image('spin_p', 'img/spin_p.png');
        	game.load.image('spin_d', 'img/spin_d.png');
        	game.load.image('bet1', 'img/bet1.png');
        	game.load.image('bet1_p', 'img/bet1_p.png');
        	game.load.image('home', 'img/home.png');
        	game.load.image('home_p', 'img/home_p.png');
        	game.load.image('dollar', 'img/dollar.png');
        	game.load.image('gear', 'img/gear.png');
        	game.load.image('double', 'img/double.png');
        }
        game.load.image('shirt_cards', 'img/shirt_cards.png');
        // game.load.image('dealer', 'img/dealer.png');
        game.load.image('pick_card', 'img/pick_game2.png');
        game.load.image('card_15', 'img/cards/joker.png');
        game.load.image('card_2b', 'img/cards/2b.png');
        game.load.image('card_5', 'img/cards/5b.png');
        game.load.image('card_2c', 'img/cards/2c.png');
        game.load.image('card_7', 'img/cards/7c.png');
        game.load.image('card_2p', 'img/cards/2p.png');
        game.load.image('card_12', 'img/cards/12p.png');
        game.load.image('card_2t', 'img/cards/2t.png');
        game.load.image('card_13', 'img/cards/13t.png');

        game.load.image('shadow1', 'img/shadow1.png');
        game.load.image('shadow2', 'img/shadow2.png');
        game.load.image('shadow3', 'img/shadow3.png');
        game.load.spritesheet('game3.meat', 'img/meat_64x256_12.png', 64, 256, 12);
        game.load.spritesheet('game3.pick', 'img/game3.pick_192x256_13.png', 192, 256, 13);
        game.load.spritesheet('game3.get_meat_star', 'img/get_meat_star_192x416_6.png', 192, 416, 6);
        game.load.spritesheet('game3.get_meat_win', 'img/get_meat_win_192x416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_1', 'img/get_meat_lose_1_192_416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_2', 'img/get_meat_lose_2_240x432_16.png', 240, 432, 16);

        game.load.spritesheet('game4.arrow', 'img/arrow_64x48_5.png', 64, 48, 5);
        game.load.spritesheet('game4.captain_lose', 'img/captain_lose_128x112_10.png', 128, 112, 10);
        game.load.spritesheet('game4.captain_pick', 'img/captain_pick_128x112_25.png', 128, 112, 25);
        game.load.spritesheet('game4.captain_swim_1', 'img/captain_swim_1_128x112_19.png', 128, 112, 19);
        game.load.spritesheet('game4.captain_swim_2', 'img/captain_swim_2_256x160_6.png', 256, 160, 6);
        game.load.spritesheet('game4.captain_under', 'img/captain_under_224x80_6.png', 224, 80, 6);
        game.load.spritesheet('game4.captain_win', 'img/captain_win_128x112_5.png', 128, 112, 5);
        game.load.spritesheet('game4.dolphin', 'img/dolphin_128х128_13.png', 128, 128, 13);
        game.load.spritesheet('game4.shark', 'img/shark_128х144_13.png', 128, 144, 13);
        game.load.spritesheet('game4.fin', 'img/fin_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.fin_left_end', 'img/fin_left_end_128x64_6.png', 128, 64, 6);
        game.load.spritesheet('game4.fin_right_end', 'img/fin_right_end_128x64_7.png', 128, 64, 7);
        game.load.spritesheet('game4.fin_end', 'img/fin_left_end_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.water', 'img/water_640x224_6.png', 640, 224, 6);

        game.load.spritesheet('game2.win_lose', 'img/game2_win_lose.png', 112, 96, 5);

        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('pickCard', 'sounds/pickCard.mp3');
        game.load.audio('cardWin', 'sounds/cardWin.mp3');
        game.load.audio('hit', 'sounds/hit.mp3');
        game.load.audio('winGame3', 'sounds/winGame3.mp3');
        game.load.audio('rowWin', 'sounds/rowWin.mp3');
        game.load.audio('page', 'sounds/page.mp3');
        for (var i = 1; i <= 9; ++i) {
        	game.load.image('linefull_' + i, 'img/lines/win/linefull' + i + '.png');
        	game.load.image('line_' + i, 'img/lines/select/line' + i + '.png');
        	game.load.image('win_' + i, 'img/win_' + i + '.png');

        	if (i % 2 != 0) {
        		game.load.audio('line' + i, 'sounds/line' + i + '.wav');
        		if(!isMobile){
        			game.load.image('btnline' + i, 'img/btns/btn' + i + '.png');
        			game.load.image('btnline_p' + i, 'img/btns/btn' + i + '_p.png');
        			game.load.image('btnline_d' + i, 'img/btns/btn' + i + '_d.png');
        		}
        	}
        }        
        game.load.audio('rotate', 'sounds/rotate.wav');

      },
      create:function(){
        game.state.start('game1'); //переключение на 1 игру
        if(isMobile)
        	document.getElementById('preloader').style.display = 'none';
      }

    };
game.state.add('gamePreload', gamePreload); //добавление загрузчика в игру

game.state.start('gamePreload'); //начало игры с локации загрузчика

$(document).ready(function(){
	var HeightNow = document.documentElement.clientHeight; 
	console.log(HeightNow);
	function checkWidth() {

		var realHeight = document.documentElement.clientHeight; 
            if (HeightNow > realHeight){    // Если текущий размер  меньше размера в полном экране, то выставляем заглушку
            	console.log('выход из полноэкранного режима');
            	full.loadTexture('non_full');
            	fullStatus = false;
            }
            HeightNow = realHeight; 
          };
          $( window ).resize(function() {
          	var realHeight = document.documentElement.clientHeight; 
          });
          window.addEventListener('resize', checkWidth); 
          checkWidth();
        });