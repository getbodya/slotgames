function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;
        
        //изображения
        background = game.add.sprite(0,0, 'game.background');
        game.add.sprite(93,53, 'game.background1');
        totalBet = game.add.sprite(95,22, 'game.backgroundTotal2');
        backgroundGame3 = game.add.sprite(95,54, 'game.backgroundGame3');

        addTableTitle(game, 'winTitleGame2', 235,358);
        hideTableTitle();
        
        // кнопки
        addButtonsGame2(game);


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[230, 23, 20], [435, 23, 20], [610, 23, 20], [697, 369, 16], [697, 369, 16]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[127,118], [253,118], [365,118], [480,118], [592,118]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация

        topGroup = game.add.group();
        var sourceArray = [
            ['game.baba1', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37], 5],
            ['game.baba2', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5],
            ['game.baba3', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36], 5],
            ['game.baba4', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5],
            ['game.baba5', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5],
            ['game.baba6', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 5]
        ];

        playAnimQueue(sourceArray);

        cat = game.add.sprite(221,374, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58], 5, true);
        cat.animations.getAnimation('cat').play();

        testo = game.add.sprite(557,390, 'game.testo');
        testo.animations.add('testo', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, true);
        testo.animations.getAnimation('testo').play();

        fontsize = 20;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 335,360, fontsize);

        full_and_sound();
    };

    game2.update = function () {

    };

    game.state.add('game2', game2);

    // звуки
}