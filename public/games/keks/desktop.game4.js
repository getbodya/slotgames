function game4() {
    ropeValues = [1,1,1,1,1,1];
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {

            //звуки
            winCards = game.add.audio("game.winCards");
            babaLoseFromWolf = game.add.audio('game.babaLoseFromWolf');

            //изображения
            //Добавление фона
            background = game.add.sprite(0,0, 'game.background');
            totalBet = game.add.sprite(95,22, 'game.totalBet');
            backgroundForGame2 = game.add.sprite(94,54, 'game.backgroundForGame2');
            titleForGame2 = game.add.sprite(222,54, 'game.titleForGame2');

            //счет
            scorePosions = [[260, 23, 20], [435, 23, 20], [610, 23, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, stepTotalWinR, balanceR, betline);


            //кнопки и анимации событий нажатия

            //для данной игры в каждую функцию вносится по две анимации (win и lose) для каждой кнопки
            selectionsAmin = [];
            timeout1Game4ToGame1 = 3000;
            timeout2Game4ToGame1 = 6000;

            // анимация для кнопки 3
            selectionsAmin[0] = function (ropeValues, ropeStep) {
                if(ropeValues[5] != 0){
                    lockDisplay();

                    setTimeout("winCards.play();",1000);

                    hideBaba();
                    babaLegs.visible = false;
                    arrow1.visible = false;
                    arrow2.visible = false;
                    if(boogiman != undefined){
                        boogiman.animations.stop();
                        boogiman.visible = false;
                    } else {
                        boogiman2.animations.stop();
                        boogiman2.visible = false;
                    }

                    babaTakeKeks1 = game.add.sprite(40,215, 'game.babaTakeKeks1');
                    babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
                    babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
                        babaTakeKeks1.visible = false;

                        var win = game.add.text(300, 220, 'x2', {
                            font: scorePosions[1][2]+'px "Press Start 2P"',
                            fill: '#fcfe6e',
                            stroke: '#000000',
                            strokeThickness: 3,
                        });

                        babaTakeKeks2 = game.add.sprite(40,215, 'game.babaTakeKeks2');
                        babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
                        babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
                            babaTakeKeks2.visible = false;
                        });
                    });

                    setTimeout("unlockDisplay();",3000);
                } else {
                    lockDisplay();

                    hideBaba();
                    babaLegs.visible = false;

                    if(boogiman != undefined){
                        boogiman.animations.stop();
                        boogiman.visible = false;
                    } else {
                        boogiman2.animations.stop();
                        boogiman2.visible = false;
                    }


                    babaStrike1_wolf = game.add.sprite(95+40,252-15, 'game.babaStrike1_wolf');
                    babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
                    babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
                        babaStrike1_wolf.visible = false;

                        upWolf = game.add.sprite(215+40,20-15, 'game.upWolf');
                        upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
                        upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
                            upWolf.visible = false;
                        });

                        babaLoseFromWolf.play();

                        arrow1.visible = false;
                        arrow2.visible = false;

                        babaStrike2_wolf = game.add.sprite(115+40,300-15, 'game.babaStrike2_wolf');
                        babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
                        babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
                            babaStrike2_wolf.visible = false;

                            babaAndFolf11 = game.add.sprite(-33+40,50-15, 'game.babaAndFolf11');
                            babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
                            babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
                                babaAndFolf11.visible = false;

                                babaAndFolf12 = game.add.sprite(-33+40,50-15, 'game.babaAndFolf12');
                                babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
                                babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
                                    babaAndFolf12.visible = false;

                                    var win = game.add.text(300, 220, '', {
                                        font: scorePosions[1][2]+'px "Press Start 2P"',
                                        fill: '#fcfe6e',
                                        stroke: '#000000',
                                        strokeThickness: 3,
                                    });

                                    babaAndFolf21 = game.add.sprite(-33+40,50-15, 'game.babaAndFolf21');
                                    babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
                                    babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
                                        babaAndFolf21.visible = false;

                                        babaAndFolf22 = game.add.sprite(-33+40,50-15, 'game.babaAndFolf22');
                                        babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
                                        babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
                                            babaAndFolf22.visible = false;
                                        });
                                    });

                                });
                            });
                        });
                    });

                    setTimeout("unlockDisplay();",6000);
                }
            };

            // анимация для кнопки 7
            selectionsAmin[1] = function (ropeValues, ropeStep) {
                if(ropeValues[5] != 0){
                    lockDisplay();

                    setTimeout("winCards.play();",1000);

                    hideBaba();
                    babaLegs.visible = false;
                    arrow1.visible = false;
                    arrow2.visible = false;
                    if(boogiman != undefined){
                        boogiman.animations.stop();
                        boogiman.visible = false;
                    } else {
                        boogiman2.animations.stop();
                        boogiman2.visible = false;
                    }

                    babaTakeKeks1 = game.add.sprite(300,215, 'game.babaTakeKeks1');
                    babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
                    babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
                        babaTakeKeks1.visible = false;

                        var win = game.add.text(600, 220, 'x2', {
                            font: scorePosions[1][2]+'px "Press Start 2P"',
                            fill: '#fcfe6e',
                            stroke: '#000000',
                            strokeThickness: 3,
                        });

                        babaTakeKeks2 = game.add.sprite(300,215, 'game.babaTakeKeks2');
                        babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
                        babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
                            babaTakeKeks2.visible = false;
                        });
                    });

                    setTimeout("unlockDisplay();",3000);
                } else {
                    lockDisplay();

                    hideBaba();
                    babaLegs.visible = false;

                    if(boogiman != undefined){
                        boogiman.animations.stop();
                        boogiman.visible = false;
                    } else {
                        boogiman2.animations.stop();
                        boogiman2.visible = false;
                    }


                    babaStrike1_wolf = game.add.sprite(95+40+260,252-15, 'game.babaStrike1_wolf');
                    babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
                    babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
                        babaStrike1_wolf.visible = false;

                        upWolf = game.add.sprite(215+40+260,20-15, 'game.upWolf');
                        upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
                        upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
                            upWolf.visible = false;
                        });

                        babaLoseFromWolf.play();

                        arrow1.visible = false;
                        arrow2.visible = false;

                        babaStrike2_wolf = game.add.sprite(115+40+260,300-15, 'game.babaStrike2_wolf');
                        babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
                        babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
                            babaStrike2_wolf.visible = false;

                            babaAndFolf11 = game.add.sprite(-33+40+260,50-15, 'game.babaAndFolf11');
                            babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
                            babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
                                babaAndFolf11.visible = false;

                                babaAndFolf12 = game.add.sprite(-33+40+260,50-15, 'game.babaAndFolf12');
                                babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
                                babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
                                    babaAndFolf12.visible = false;

                                    var win = game.add.text(600, 220, '', {
                                        font: scorePosions[1][2]+'px "Press Start 2P"',
                                        fill: '#fcfe6e',
                                        stroke: '#000000',
                                        strokeThickness: 3,
                                    });
                                    
                                    babaAndFolf21 = game.add.sprite(-33+40+260,50-15, 'game.babaAndFolf21');
                                    babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
                                    babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
                                        babaAndFolf21.visible = false;

                                        babaAndFolf22 = game.add.sprite(-33+40+260,50-15, 'game.babaAndFolf22');
                                        babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
                                        babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
                                            babaAndFolf22.visible = false;
                                        });
                                    });

                                });
                            });
                        });
                    });

                    setTimeout("unlockDisplay();",6000);
                }
            };

            thicket1 = game.add.sprite(240,290, 'thicket');
            thicket2 = game.add.sprite(530,290, 'thicket');

            thicket1.inputEnabled = true;
            thicket1.input.useHandCursor = true;
            thicket1.events.onInputUp.add(function(){
                thicket1.inputEnabled = false;
                thicket2.inputEnabled = false;

                buttonLine3.loadTexture('buttonLine3_d');
                buttonLine3.inputEnabled = false;
                buttonLine3.input.useHandCursor = false;

                selectionsAmin[0](ropeValues, ropeStep);

                if(ropeValues[5] != 0){
                    lockDisplay();
                    hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

                    setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout1Game4ToGame1);
                } else {
                    lockDisplay();
                    hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

                    setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout2Game4ToGame1);
                }
            });

            thicket2.inputEnabled = true;
            thicket2.input.useHandCursor = true;
            thicket2.events.onInputUp.add(function(){
                thicket1.inputEnabled = false;
                thicket2.inputEnabled = false;

                buttonLine7.loadTexture('buttonLine7_d');
                buttonLine7.inputEnabled = false;
                buttonLine7.input.useHandCursor = false;

                selectionsAmin[1](ropeValues, ropeStep);

                if(ropeValues[5] != 0){
                    lockDisplay();
                    hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

                    setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout1Game4ToGame1);
                } else {
                    lockDisplay();
                    hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

                    setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout2Game4ToGame1);
                }
            });

            addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1);



            //анимации
            function showRandBaba(){
                var randBaba = randomNumber(7,10);

                switch(randBaba) {
                    case 7:
                        hideBaba();
                        baba7.visible = true;
                        baba7.animations.getAnimation('baba7').play();
                        break;
                    case 8:
                        hideBaba();
                        baba8.visible = true;
                        baba8.animations.getAnimation('baba8').play();
                        break;
                    case 9:
                        hideBaba();
                        baba9.visible = true;
                        baba9.animations.getAnimation('baba9').play();
                        break;
                    case 10:
                        hideBaba();
                        baba10.visible = true;
                        baba10.animations.getAnimation('baba10').play();
                        break;
                    case 12:
                        hideBaba();
                        baba12.visible = true;
                        baba12.animations.getAnimation('baba12').play();
                        break;

                }
            }

            function showBabaLegs(x,y){
                babaLegs = game.add.sprite(x,y, 'game.babaLegs');
            }

            function createBaba(x,y){
                baba7 = game.add.sprite(x,y, 'game.baba7');
                baba7.animations.add('baba7', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
                baba7.animations.getAnimation('baba7').play().onComplete.add(function(){
                    baba7.animations.stop();
                    showRandBaba();
                });
                baba7.visible = false;

                baba8 = game.add.sprite(x,y, 'game.baba8');
                baba8.animations.add('baba8', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 5, false);
                baba8.animations.getAnimation('baba8').play().onComplete.add(function(){
                    baba8.animations.stop();
                    showRandBaba();
                });
                baba8.visible = false;

                baba9 = game.add.sprite(x,y, 'game.baba9');
                baba9.animations.add('baba9', [0,1,2,3,4,5,6,7,8], 5, false);
                baba9.animations.getAnimation('baba9').play().onComplete.add(function(){
                    baba9.animations.stop();
                    showRandBaba();
                });
                baba9.visible = false;

                baba10 = game.add.sprite(x,y, 'game.baba10');
                baba10.animations.add('baba10', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
                baba10.animations.getAnimation('baba10').play().onComplete.add(function(){
                    baba10.animations.stop();
                    showRandBaba();
                });
                baba10.visible = false;

                baba11 = game.add.sprite(x,y, 'game.baba11');
                baba11.animations.add('baba11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 5, false);
                baba11.animations.getAnimation('baba11').play().onComplete.add(function(){
                    baba11.animations.stop();
                    showRandBaba();
                });
                baba11.visible = false;

                baba12 = game.add.sprite(x,y, 'game.baba12');
                baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                    baba12.animations.stop();
                    showRandBaba();
                });
                baba12.visible = false;
            }

            function showBoogiman(){
                arrayXY = [[250,300],[270,320],[[230],[300]],[[260],[310]]];

                var rand = randomNumber(1,2);

                switch (rand) {
                    case 1:
                        var x = arrayXY[0][0];
                        var y = arrayXY[0][1];

                        boogiman = game.add.sprite(x,y, 'game.boogiman');
                        boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                            boogiman.visible = false;

                            boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                            boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                            boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                                boogiman2.visible = false;

                                showBoogiman();
                            });
                        });

                        break;
                    case 2:
                        var x = arrayXY[1][0];
                        var y = arrayXY[1][1];

                        boogiman = game.add.sprite(x,y, 'game.boogiman');
                        boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                            boogiman.visible = false;

                            boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                            boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                            boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                                boogiman2.visible = false;

                                showBoogiman();
                            });
                        });

                        break;
                    case 3:
                        var x = arrayXY[2][0];
                        var y = arrayXY[2][1];

                        boogiman = game.add.sprite(x,y, 'game.boogiman');
                        boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                            boogiman.visible = false;

                            boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                            boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                            boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                                boogiman2.visible = false;

                                showBoogiman();
                            });
                        });
                        break;
                    case 4:
                        var x = arrayXY[3][0];
                        var y = arrayXY[3][1];

                        boogiman = game.add.sprite(x,y, 'game.boogiman');
                        boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                            boogiman.visible = false;

                            boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                            boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                            boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                                boogiman2.visible = false;

                                showBoogiman();
                            });
                        });
                        break;
                }

            }

            function hideBaba(){
                baba7.visible = false;
                baba8.visible = false;
                baba9.visible = false;
                baba10.visible = false;
                baba11.visible = false;
                baba12.visible = false;
            }

            arrow1 = game.add.sprite(295,232, 'game.arrow1');
            
            arrow2 = game.add.sprite(595,232, 'game.arrow2');

            arrow1.animations.add('arrow1', [0,1], 2, true);
            arrow2.animations.add('arrow2', [0,1], 2, true);
            arrow1.animations.getAnimation('arrow1').play();
            arrow2.animations.getAnimation('arrow2').play();

            showBoogiman();
            showBabaLegs(117,452);
            createBaba(101,292);
            showRandBaba();


            full_and_sound();
        };

        game4.update = function () {
        };

        game.state.add('game4', game4);

    })();


}