//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

//Функции связанные с линиями и их номерами

var line1; var linefull1; var number1;
var line2; var linefull2; var number2;
var line3; var linefull3; var number3;
var line4; var linefull4; var number4;
var line5; var linefull5; var number5;
var line6; var linefull6; var number6;
var line7; var linefull7; var number7;
var line8; var linefull8; var number8;
var line9; var linefull9; var number9;

//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9'];
    var lineNames = ['line1', 'line2', 'line3', 'line4', 'line5', 'line6', 'line7', 'line8', 'line9'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);

    linefull1.visible = false;
    linefull2.visible = false;
    linefull3.visible = false;
    linefull4.visible = false;
    linefull5.visible = false;
    linefull6.visible = false;
    linefull7.visible = false;
    linefull8.visible = false;
    linefull9.visible = false;

    line1 = game.add.sprite(linePosition[0][0], linePosition[0][1], lineNames[0]);
    line2 = game.add.sprite(linePosition[1][0], linePosition[1][1], lineNames[1]);
    line3 = game.add.sprite(linePosition[2][0], linePosition[2][1], lineNames[2]);
    line4 = game.add.sprite(linePosition[3][0], linePosition[3][1], lineNames[3]);
    line5 = game.add.sprite(linePosition[4][0], linePosition[4][1], lineNames[4]);
    line6 = game.add.sprite(linePosition[5][0], linePosition[5][1], lineNames[5]);
    line7 = game.add.sprite(linePosition[6][0], linePosition[6][1], lineNames[6]);
    line8 = game.add.sprite(linePosition[7][0], linePosition[7][1], lineNames[7]);
    line9 = game.add.sprite(linePosition[8][0], linePosition[8][1], lineNames[8]);

    line1.visible = false;
    line2.visible = false;
    line3.visible = false;
    line4.visible = false;
    line5.visible = false;
    line6.visible = false;
    line7.visible = false;
    line8.visible = false;
    line9.visible = false;

    number1 = game.add.sprite(numberPosition[0][0], numberPosition[0][1], 'game.number1');
    number2 = game.add.sprite(numberPosition[1][0], numberPosition[1][1], 'game.number2');
    number3 = game.add.sprite(numberPosition[2][0], numberPosition[2][1], 'game.number3');
    number4 = game.add.sprite(numberPosition[3][0], numberPosition[3][1], 'game.number4');
    number5 = game.add.sprite(numberPosition[4][0], numberPosition[4][1], 'game.number5');
    number6 = game.add.sprite(numberPosition[5][0], numberPosition[5][1], 'game.number6');
    number7 = game.add.sprite(numberPosition[6][0], numberPosition[6][1], 'game.number7');
    number8 = game.add.sprite(numberPosition[7][0], numberPosition[7][1], 'game.number8');
    number9 = game.add.sprite(numberPosition[8][0], numberPosition[8][1], 'game.number9');

    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;

}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
function hideLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        line1.visible = false;
        line2.visible = false;
        line3.visible = false;
        line4.visible = false;
        line5.visible = false;
        line6.visible = false;
        line7.visible = false;
        line8.visible = false;
        line9.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = false;
                break;
                case 2:
                linefull2.visible = false;
                break;
                case 3:
                linefull3.visible = false;
                break;
                case 4:
                linefull4.visible = false;
                break;
                case 5:
                linefull5.visible = false;
                break;
                case 6:
                linefull6.visible = false;
                break;
                case 7:
                linefull7.visible = false;
                break;
                case 8:
                linefull8.visible = false;
                break;
                case 9:
                linefull9.visible = false;
                break;
                case 11:
                line1.visible = false;
                break;
                case 12:
                line2.visible = false;
                break;
                case 13:
                line3.visible = false;
                break;
                case 14:
                line4.visible = false;
                break;
                case 15:
                line5.visible = false;
                break;
                case 16:
                line6.visible = false;
                break;
                case 17:
                line7.visible = false;
                break;
                case 18:
                line8.visible = false;
                break;
                case 19:
                line9.visible = false;
                break;
            }
        });
    }
}

var linesArray = [];
function showLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        line1.visible = true;
        line2.visible = true;
        line3.visible = true;
        line4.visible = true;
        line5.visible = true;
        line6.visible = true;
        line7.visible = true;
        line8.visible = true;
        line9.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = true;
                break;
                case 2:
                linefull2.visible = true;
                break;
                case 3:
                linefull3.visible = true;
                break;
                case 4:
                linefull4.visible = true;
                break;
                case 5:
                linefull5.visible = true;
                break;
                case 6:
                linefull6.visible = true;
                break;
                case 7:
                linefull7.visible = true;
                break;
                case 8:
                linefull8.visible = true;
                break;
                case 9:
                linefull9.visible = true;
                break;
                case 11:
                line1.visible = true;
                break;
                case 12:
                line2.visible = true;
                break;
                case 13:
                line3.visible = true;
                break;
                case 14:
                line4.visible = true;
                break;
                case 15:
                line5.visible = true;
                break;
                case 16:
                line6.visible = true;
                break;
                case 17:
                line7.visible = true;
                break;
                case 18:
                line8.visible = true;
                break;
                case 19:
                line9.visible = true;
                break;
            }
        });
    }
}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = false;
        number2.visible = false;
        number3.visible = false;
        number4.visible = false;
        number5.visible = false;
        number6.visible = false;
        number7.visible = false;
        number8.visible = false;
        number9.visible = false;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = false;
                break;
                case 2:
                number2.visible = false;
                break;
                case 3:
                number3.visible = false;
                break;
                case 4:
                number4.visible = false;
                break;
                case 5:
                number5.visible = false;
                break;
                case 6:
                number6.visible = false;
                break;
                case 7:
                number7.visible = false;
                break;
                case 8:
                number8.visible = false;
                break;
                case 9:
                number9.visible = false;
                break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = true;
                break;
                case 2:
                number2.visible = true;
                break;
                case 3:
                number3.visible = true;
                break;
                case 4:
                number4.visible = true;
                break;
                case 5:
                number5.visible = true;
                break;
                case 6:
                number6.visible = true;
                break;
                case 7:
                number7.visible = true;
                break;
                case 8:
                number8.visible = true;
                break;
                case 9:
                number9.visible = true;
                break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}



// Функции связанные с кнопками
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;

//кнопки для слотов
function addButtonsGame1(game) {

    //звуки для кнопок
    var line1Sound = game.add.audio('line1Sound');
    var line3Sound = game.add.audio('line3Sound');
    var line5Sound = game.add.audio('line5Sound');
    var line7Sound = game.add.audio('line7Sound');
    var line9Sound = game.add.audio('line9Sound');

    //var soundForBattons = []; - массив содержащий объекты звуков для кнопок
    var soundForBattons = [];
    soundForBattons.push(line1Sound, line3Sound, line5Sound, line7Sound, line9Sound);

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150,510, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490,510, 'betone');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        betone.loadTexture('betone_p');
    });
    betone.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            upBetline(betlineOptions);
            updateBetinfo(game, scorePosions, lines, betline);
        }
    });
    betone.events.onInputOut.add(function(){
        betone.loadTexture('betone');
    });


    betmax = game.add.sprite(535,510, 'betmax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        betmax.loadTexture('betmax_p');
    });
    betmax.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            maxBetline();
            updateBetinfo(game, scorePosions, lines, betline);
            //betMaxSound.play();
        }
    });
    betmax.events.onInputOut.add(function(){
        betmax.loadTexture('betmax');
    });

    automaricstart = game.add.sprite(685,510, 'automaricstart');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = true;
    automaricstart.input.useHandCursor = true;
    automaricstart.events.onInputOver.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart_p');
        }
    });
    automaricstart.events.onInputOut.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart');
        }
    });
    automaricstart.events.onInputDown.add(function(){
        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;
                    hideLines([]);
                    requestSpin(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                autostart = true;
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton');
        }
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([]);
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }

    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;
    buttonLine1.events.onInputOver.add(function(){
        buttonLine1.loadTexture('buttonLine1_p');
    });
    buttonLine1.events.onInputOut.add(function(){
        buttonLine1.loadTexture('buttonLine1');
    });
    buttonLine1.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1]);
    });
    buttonLine1.events.onInputDown.add(function(){
        soundForBattons[0].play();
        hideLines([]);
        linesArray = [11];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1];
        showNumbers(numberArray);

        lines = 1;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3]);
    });
    buttonLine3.events.onInputDown.add(function(){
        soundForBattons[1].play();

        hideLines([]);
        linesArray = [11, 12, 13];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3];
        showNumbers(numberArray);

        lines = 3;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5]);
    });
    buttonLine5.events.onInputDown.add(function(){
        soundForBattons[2].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5];
        showNumbers(numberArray);

        lines = 5;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7]);
    });
    buttonLine7.events.onInputDown.add(function(){
        soundForBattons[3].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7];
        showNumbers(numberArray);

        lines = 7;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });
    buttonLine9.events.onInputDown.add(function(){
        soundForBattons[4].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17, 18, 19];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        showNumbers(numberArray);

        lines = 9;
        updateBetinfo(game, scorePosions, lines, betline);
    });

}

//кнопки для карт
function addButtonsGame2(game) {

    var soundForBattons = [];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        startButton.loadTexture('startButton_p');
    });
    startButton.events.onInputOut.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputDown.add(function(){
        requestDouble(gamename, 1, lines, bet, sid);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputDown.add(function(){
        requestDouble(gamename, 2, lines, bet, sid);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputDown.add(function(){
        requestDouble(gamename, 3, lines, bet, sid);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputDown.add(function(){
        requestDouble(gamename, 4, lines, bet, sid);
    });

}

//кнопки и логика для игры с последовательным выбором
var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
function addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm){
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;

    buttonLine1.events.onInputOver.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1_p');
        }
    });
    buttonLine1.events.onInputOut.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1');
        }
    });

    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine5.events.onInputOver.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5_p');
        }
    });
    buttonLine5.events.onInputOut.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5');
        }
    });

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });

    buttonLine9.events.onInputOver.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9_p');
        }
    });
    buttonLine9.events.onInputOut.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9');
        }
    });

    buttonLine1.events.onInputDown.add(function(){
        pechka1.inputEnabled = false;
        buttonLine1.loadTexture('buttonLine1_d');
        buttonLine1.inputEnabled = false;
        buttonLine1.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) {
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[0](ropeValues, ropeStep, checkHelm);

        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[0][0],winRopeNumberPosition[0][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine3.events.onInputDown.add(function(){
        pechka2.inputEnabled = false;
        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }


        ropesAnim[1](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[1][0],winRopeNumberPosition[1][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine5.events.onInputDown.add(function(){
        pechka3.inputEnabled = false;
        buttonLine5.loadTexture('buttonLine5_d');
        buttonLine5.inputEnabled = false;
        buttonLine5.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[2](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[2][0],winRopeNumberPosition[2][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine7.events.onInputDown.add(function(){
        pechka4.inputEnabled = false;
        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[3](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[3][0],winRopeNumberPosition[3][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine9.events.onInputDown.add(function(){
        pechka5.inputEnabled = false;
        buttonLine9.loadTexture('buttonLine9_d');
        buttonLine9.inputEnabled = false;
        buttonLine9.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[4](ropeValues, ropeStep);
        setTimeout(showWinGame3, timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[4][0],winRopeNumberPosition[4][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
}

// для выбора одного из двух (действия при нажатии некоторых кнопок нужно задать в самой игре)
var selectionsAmin = []; //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
function addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1) {
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = false;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });
    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine3.events.onInputDown.add(function(){
        thicket1.inputEnabled = false;
        thicket2.inputEnabled = false;

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        selectionsAmin[0](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

            setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

            setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout2Game4ToGame1);
        }
    });
    buttonLine7.events.onInputDown.add(function(){
        thicket1.inputEnabled = false;
        thicket2.inputEnabled = false;

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        selectionsAmin[1](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

            setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);

            setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout2Game4ToGame1);
        }
    });
}

//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
function hideButtons(buttonsArray) {
    if(!isMobile){
        if(buttonsArray.length == 0) {
            if(autostart == false){
                automaricstart.loadTexture('automaricstart_d');
                automaricstart.inputEnabled = false;
                automaricstart.input.useHandCursor = false;
            }

            selectGame.loadTexture('selectGame_d');
            selectGame.inputEnabled = false;
            selectGame.input.useHandCursor = false;

            payTable.loadTexture('payTable_d');
            payTable.inputEnabled = false;
            payTable.input.useHandCursor = false;

            betone.loadTexture('betone_d');
            betone.inputEnabled = false;
            betone.input.useHandCursor = false;

            betmax.loadTexture('betmax_d');
            betmax.inputEnabled = false;
            betmax.input.useHandCursor = false;

            startButton.loadTexture('startButton_d');
            startButton.inputEnabled = false;
            startButton.input.useHandCursor = false;

            buttonLine1.loadTexture('buttonLine1_d');
            buttonLine1.inputEnabled = false;
            buttonLine1.input.useHandCursor = false;

            buttonLine3.loadTexture('buttonLine3_d');
            buttonLine3.inputEnabled = false;
            buttonLine3.input.useHandCursor = false;

            buttonLine5.loadTexture('buttonLine5_d');
            buttonLine5.inputEnabled = false;
            buttonLine5.input.useHandCursor = false;

            buttonLine7.loadTexture('buttonLine7_d');
            buttonLine7.inputEnabled = false;
            buttonLine7.input.useHandCursor = false;

            buttonLine9.loadTexture('buttonLine9_d');
            buttonLine9.inputEnabled = false;
            buttonLine9.input.useHandCursor = false;

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]+'_d');
                item[0].inputEnabled = false;
                item[0].input.useHandCursor = false;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = false;
            double.visible = false;
            bet1.visible = false;
            dollar.visible = false;
            gear.visible = false;
            home.visible = false;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = false;
            })
        }
    }
}

function showButtons(buttonsArray) {
    if(!isMobile) {
        if(buttonsArray.length == 0) {

            if(autostart == false){
                automaricstart.loadTexture('automaricstart');
                automaricstart.inputEnabled = true;
                automaricstart.input.useHandCursor = true;

                selectGame.loadTexture('selectGame');
                selectGame.inputEnabled = true;
                selectGame.input.useHandCursor = true;

                payTable.loadTexture('payTable');
                payTable.inputEnabled = true;
                payTable.input.useHandCursor = true;

                betone.loadTexture('betone');
                betone.inputEnabled = true;
                betone.input.useHandCursor = true;

                betmax.loadTexture('betmax');
                betmax.inputEnabled = true;
                betmax.input.useHandCursor = true;

                startButton.loadTexture('startButton');
                startButton.inputEnabled = true;
                startButton.input.useHandCursor = true;

                buttonLine1.loadTexture('buttonLine1');
                buttonLine1.inputEnabled = true;
                buttonLine1.input.useHandCursor = true;

                buttonLine3.loadTexture('buttonLine3');
                buttonLine3.inputEnabled = true;
                buttonLine3.input.useHandCursor = true;

                buttonLine5.loadTexture('buttonLine5');
                buttonLine5.inputEnabled = true;
                buttonLine5.input.useHandCursor = true;

                buttonLine7.loadTexture('buttonLine7');
                buttonLine7.inputEnabled = true;
                buttonLine7.input.useHandCursor = true;

                buttonLine9.loadTexture('buttonLine9');
                buttonLine9.inputEnabled = true;
                buttonLine9.input.useHandCursor = true;
            }

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]);
                item[0].inputEnabled = true;
                item[0].input.useHandCursor = true;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = true;
            double.visible = true;
            bet1.visible = true;
            dollar.visible = true;
            gear.visible = true;
            home.visible = true;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = true;
            })
        }
    }
}


//слоты и их вращение

//переменные содержащие все объекты слотов
var bar1Anim; var bar2Anim; var bar3Anim; var bar4Anim; var bar5Anim;
var slotArray =[];
function addSlots(game, slotPosition) {
    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
    var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];
    var slotCellAnimName = 'cellAnim';

    slotArray[0] = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[0]);
    slotArray[1]= game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[1]);
    slotArray[2]= game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[3]);
    slotArray[3]= game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[4]);
    slotArray[4]= game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[7]);
    slotArray[5]= game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[2]);
    slotArray[6]= game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[4]);
    slotArray[7]= game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[3]);
    slotArray[8]= game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[8]);
    slotArray[9] = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[2]);
    slotArray[10] = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[0]);
    slotArray[11] = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[1]);
    slotArray[12] = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[5]);
    slotArray[13] = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[3]);
    slotArray[14] = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[5]);

    slotArray[15] = game.add.sprite(slotPosition[15][0],slotPosition[15][1], slotValueNames[3]);
    slotArray[16] = game.add.sprite(slotPosition[16][0],slotPosition[16][1], slotValueNames[2]);
    slotArray[17] = game.add.sprite(slotPosition[17][0],slotPosition[17][1], slotValueNames[8]);
    slotArray[18] = game.add.sprite(slotPosition[18][0],slotPosition[18][1], slotValueNames[1]);
    slotArray[19] = game.add.sprite(slotPosition[19][0],slotPosition[19][1], slotValueNames[5]);

    bar1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], 'barSpin');
    bar2Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], 'barSpin');
    bar3Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], 'barSpin');
    bar4Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], 'barSpin');
    bar5Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], 'barSpin');

    bar1Anim.animations.add('bar1Anim', [0,1,2,3,4], 15, true);
    bar2Anim.animations.add('bar2Anim', [1,2,3,4,0], 15, true);
    bar3Anim.animations.add('bar3Anim', [2,3,4,0,1], 15, true);
    bar4Anim.animations.add('bar4Anim', [3,4,0,1,2], 15, true);
    bar5Anim.animations.add('bar5Anim', [4,0,1,2,3], 15, true);

    bar1Anim.animations.getAnimation('bar1Anim').play();
    bar2Anim.animations.getAnimation('bar2Anim').play();
    bar3Anim.animations.getAnimation('bar3Anim').play();
    bar4Anim.animations.getAnimation('bar4Anim').play();
    bar5Anim.animations.getAnimation('bar5Anim').play();

    bar1Anim.visible = false;
    bar2Anim.visible = false;
    bar3Anim.visible = false;
    bar4Anim.visible = false;
    bar5Anim.visible = false;
}

var finalValues; var wlValues; var balanceR; var totalWin; var totalWinR; var dcard; var linesR; var betlineR; //totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep = 0;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {

    dataArray = dataSpinRequest;

    if (dataArray['state']) {

        if (dataArray['rope']) {
            //получаем ячейки в которых содержатся обезьяны
            monkeyCell = [];
            dataArray['info'].forEach(function (item, i) {
                if(item == 8) {
                    monkeyCell.push(i);
                }
            });

            ropeValues = dataArray['rope'];
        }

        wlValues = dataArray['wl'];
        finalValues = dataArray['info'];
        totalWinR = dataArray['allWin'];

        balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
        balanceR = dataArray['balance'];
        balance = dataArray['balance'] + dataArray['allWin']; // получаем реальный баланс

        linesR = dataArray['linesInGame'];
        betlineR = dataArray['betLine'];
        dcard = dataArray['dcard'];

        checkSpinResult(totalWinR);

        slotRotation(game, finalValues);
    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons([]);

        checkRopeGameAnim = 1;

        var winMonkey = game.add.audio('game.winPech');
        winMonkey.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 5000);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('topScoreGame2'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = []; // массив для объектов звуков
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');
        soundWinLines[5] = game.add.audio('soundWinLine6');
        soundWinLines[6] = game.add.audio('soundWinLine7');
        soundWinLines[7] = game.add.audio('soundWinLine8');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];


        for (key in wlValues) {
            if(wlValues[key] > 0) {
                wlWinValuesArray.push(+(key));
            }
        }

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {
                if(!isMobile) {
                    showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);
                } else {
                    showButtons([[startButton], [home], [gear], [dollar], [double]]);
                }

                hideNumberWinLine();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk1');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex]]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий
                if(isMobile) {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 365-mobileX, 360-mobileY);
                } else {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 365,360);
                }
                soundWinLines[soundWinLinesCounter].play();
                soundWinLinesCounter += 1;

                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld);
            hideLines([]);
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('play1To');
            updateBalance(game, scorePosions, balanceOld);
            if(isMobile){
                showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            } else {
                showButtons([]);
            }
            checkAutoStart = false;
        }
    }

}


//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons([]);

    $.ajax({
        type: "get",
        url: getNeedUrlPath() + '/spin/'+gamename+'?sessionName='+sessionName+'&betLine='+betline+'&linesInGame='+lines,
        dataType: 'html',
        success: function (data) {

            dataSpinRequest = JSON.parse(data);

            //console.log(dataSpinRequest.info);

            if(dataSpinRequest['state']) {
                if(dataSpinRequest['rope']) {
                    checkRopeGame = true;
                    parseSpinAnswer(dataSpinRequest);
                } else {
                    parseSpinAnswer(dataSpinRequest);
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка 3';
            console.log(errorText);
            setTimeout("requestSpin(gamename, betline, lines, bet, sid)", 3000);
        }
    });
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [2,1,0], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

function hideSelectionOfTheManyCellAnim(monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item].visible = false;
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    bar1Anim.visible = true;
    bar2Anim.visible = true;
    bar3Anim.visible = true;
    bar4Anim.visible = true;
    bar5Anim.visible = true;

    setTimeout(function() {
        // stopSound.play();
        endspin(0);
        bar1Anim.visible = false;

        slotArray[0].loadTexture('cell'+finalValues[0]);
        slotArray[1].loadTexture('cell'+finalValues[1]);
        slotArray[2].loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        // stopSound.play();
        endspin(1);
        bar2Anim.visible = false;

        slotArray[3].loadTexture('cell'+finalValues[3]);
        slotArray[4].loadTexture('cell'+finalValues[4]);
        slotArray[5].loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        // stopSound.play();
        endspin(2);
        bar3Anim.visible = false;

        slotArray[6].loadTexture('cell'+finalValues[6]);
        slotArray[7].loadTexture('cell'+finalValues[7]);
        slotArray[8].loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        // stopSound.play();
        endspin(3);
        bar4Anim.visible = false;

        slotArray[9].loadTexture('cell'+finalValues[9]);
        slotArray[10].loadTexture('cell'+finalValues[10]);
        slotArray[11].loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        // stopSound.play();
        endspin(4);
        bar5Anim.visible = false;

        slotArray[12].loadTexture('cell'+finalValues[12]);
        slotArray[13].loadTexture('cell'+finalValues[13]);
        slotArray[14].loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}
function endspin(number){
    var stopSound = game.add.audio('stopSound');
    slotArray[0+number*3].position.y = 54-70;
    slotArray[1+number*3].position.y = 150-70;
    slotArray[2+number*3].position.y = 246-70;
    slotArray[15+number].position.y = 342-70;
    game.add.tween(slotArray[0+number*3]).to({y:slotArray[0+number*3].position.y+70}, 40, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        game.add.tween(slotArray[0+number*3]).to({y:slotArray[0+number*3].position.y-5}, 15, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(slotArray[0+number*3]).to({y:slotArray[0+number*3].position.y+5}, 15, Phaser.Easing.LINEAR, true);
        });
    });
    game.add.tween(slotArray[1+number*3]).to({y:slotArray[1+number*3].position.y+70}, 40, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        game.add.tween(slotArray[1+number*3]).to({y:slotArray[1+number*3].position.y-5}, 15, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(slotArray[1+number*3]).to({y:slotArray[1+number*3].position.y+5}, 15, Phaser.Easing.LINEAR, true);
        });
    });
    game.add.tween(slotArray[2+number*3]).to({y:slotArray[2+number*3].position.y+70}, 40, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        game.add.tween(slotArray[2+number*3]).to({y:slotArray[2+number*3].position.y-5}, 15, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(slotArray[2+number*3]).to({y:slotArray[2+number*3].position.y+5}, 15, Phaser.Easing.LINEAR, true);
        });
    });
    game.add.tween(slotArray[15+number]).to({y:slotArray[15+number].position.y+70}, 40, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        stopSound.play();
        game.add.tween(slotArray[15+number]).to({y:slotArray[15+number].position.y-5}, 15, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(slotArray[15+number]).to({y:slotArray[15+number].position.y+5}, 15, Phaser.Easing.LINEAR, true);
        });
    });
}
var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('take');
    hideButtons([]);

    hideNumbersAmin();
    hideLines([]);

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
var tableTitle; // название изображения заданное в прелодере
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle.visible = true;
    tableTitle.loadTexture(loadTexture);
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
        font: '20px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}

function getNeedUrlPath() {
    if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
        var number = location.pathname.indexOf('/games/');
        var needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname.substring(0,number) + '/';

        return needLocation;
    } else if (location.href.indexOf('public') !== -1 && location.href.indexOf('/game/') !== -1) {
        var number = location.pathname.indexOf('/public/');
        var needLocation = location.href.substring(0,location.href.indexOf('public')) + 'public';

        return needLocation;
    } else if (location.href.indexOf('public') === -1) {
        needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname;

        return needLocation;
    }
}

// ajax-запросы

//init-запрос
function requestInit() {
    $.ajax({
        type: "get",
        //url: 'http://'+location.hostname + '/slotgames/public/init',
        url: getNeedUrlPath() + '/init',
        dataType: 'html',
        success: function (data) {
            dataString = data;

            sessionName = data;

            requestState();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = 'ошибка 1';
            console.log(errorText);
            setTimeout("requestInit()", 1000);
        }
    });

}

//state-запрос
var jackpots;
function requestState() {
    $.ajax({
        type: "get",
        url: getNeedUrlPath() + '/state?sessionName='+sessionName,
        dataType: 'html',
        success: function (data) {

            dataArray = JSON.parse(data);

            if(dataArray['state']) {

                balance = dataArray['balance'];

                game1();
                game2();
                game3();
                game4();

            } else {
                console.log('Ошбика 21');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = 'Ошбика 20';
            console.log(errorText);
            setTimeout("requestState()",1000);
        }
    });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    if(checkGame == 1){
        betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
            font: scorePosions[4][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    if(checkGame == 2){
        riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }
}

var takeWin;
var textCounter;
var topBarImage;
var ActionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR) {
    var balance = balanceR - betline * lines + totalWinR;

    if(balanceR == '') { //подстраиваемся для случая, когда api присылает баланс только после спин-запроса
        balanceR = balance - parseInt(finalValues[15]);
    }

    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'startButton']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        ActionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время
                if(isMobile) {
                    showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
                } else {
                    showButtons([]);
                }

                takeWin.stop();
                changeTableTitle('play1To');
                clearInterval(textCounter);

                topBarImage.loadTexture('game.backgroundGame1Score1'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    betScore.visible = false;
    linesScore.visible = false;
    betline1Score.visible = false;
    betline2Score.visible = false;

    bet = lines*betline;
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
        font: scorePosions[4][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];
    } else {
        betlineCounter = 0;
        betline = betlineOptions[betlineCounter];
    }
}

function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard) {
    hideButtons([[startButton, 'startButton']]);
    disableInputCards();
    lockDisplay();
    $.ajax({
        type: "GET",
        url: getNeedUrlPath() + '/double/'+gamename+'?sessionName='+sessionName+'&selectedCard='+selectedCard,
        dataType: 'html',
        success: function (data) {

            dataDoubleRequest = JSON.parse(data);
            parseDoubleAnswer(dataDoubleRequest);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = 'ошибка 4';
            console.log(errorText);
            setTimeout("requestDouble(gamename, selectedCard)", 1000);
        }
    });
}

var dwin; var dwinS; var dcard2; var selectedCardR; var dwinOld = -841284;
function parseDoubleAnswer(dataDoubleRequest) {

    if(dataDoubleRequest['dwin'] != 0) {
        dwinS = dataDoubleRequest['dwin'];
    }

    dwin = dataDoubleRequest['dwin'];


    resutlState = dataDoubleRequest['resutlState'];
    totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
    balance = dataDoubleRequest['balance'];
    dcard2 = dataDoubleRequest['dcard'];
    dcard = dcard2;
    valuesOfAllCards = dataDoubleRequest['valuesOfAllCards'];
    selectedCardR = dataDoubleRequest['selectedCardR'];


    showDoubleResult(dwin, selectedCardR, valuesOfAllCards);
}

var step = 1;
function showDoubleResult(dwin, selectedCardR, valuesOfAllCards) {
    if (!isMobile) {

        if(resutlState == 1) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout('hideAllCards(cardArray); tableTitle.visible = false; step += 1; updateTotalWin(game, dwinS, step)', 2000);
            setTimeout('openDCard(dcard); showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "startButton"]]); showDoubleToAndTakeOrRiskTexts(game, dwinS, xSave, ySave, fontsize);', 3000);
        } else if(resutlState == 2) {

            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('forwadTitleGame2');
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout('hideAllCards(cardArray); disableInputCards(); tableTitle.visible = false; step += 1; updateTotalWin(game, dwinS, step)', 2000);
            setTimeout('openDCard(dcard); showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "startButton"]]); showDoubleToAndTakeOrRiskTexts(game, dwinS, xSave, ySave, fontsize);', 3000);
        } else if(resutlState == 0) {

            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            disableInputCards();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 2000);
        }
    } else {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); lockDisplay(); disableInputCards();', 500);
            setTimeout('hideAllCards(cardArray); openDCard(dcard); disableInputCards(); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step);', 2000);
            setTimeout('enableInputCards(); unlockDisplay(); showButtons([[startButton]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); disableInputCards(); lockDisplay();', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 3000);
        }
    }
}

var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
var fontsize;
function showDoubleToAndTakeOrRiskTexts(game, dwinS, x,y, fontsize) {
    xSave = x;
    ySave = y;

    var i = 1;
    timerTitleAmin = setInterval(function() {
        if(i == 0) {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            doubleToText = game.add.text(x, y, 'DOUBLE TO '+dwinS*2+' ?', {
                font: '22px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 1;
        } else {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            takeOrRiskText = game.add.text(x, y, 'TAKE OR RISK', {
                font: '22px "Press Start 2P"',
                fill: '#ffffff',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 0;
        }

    }, 500);
}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    takeOrRiskText.visible = false;
    clearInterval(timerTitleAmin);
}

function updateTotalWin(game, dwinS, step){
    //обновление totalWin в игре с картами

    linesScore.visible = false;
    riskStep.visible = false;

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], dwinS, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], step, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];

//var cardPosition = [[x,y], [x,y], [x,y], [x,y], [x,y]]  - нулевой элемент массива карта диллера
var openCard; //звуки
var winCard;
function addCards(game, cardPosition) {
    if(!isMobile) {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        card2.inputEnabled = true;
        card2.input.useHandCursor = true;
        card2.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid);
        });
        card3.inputEnabled = true;
        card3.input.useHandCursor = true;
        card3.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        card4.inputEnabled = true;
        card4.input.useHandCursor = true;
        card4.events.onInputDown.add(function(){
            requestDouble(gamename, 3, lines, bet, sid);
        });
        card5.inputEnabled = true;
        card5.input.useHandCursor = true;
        card5.events.onInputDown.add(function(){
            requestDouble(gamename, 4, lines, bet, sid);
        });

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    } else {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        card2.inputEnabled = true;
        card2.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid);
        });
        card3.inputEnabled = true;
        card3.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        card4.inputEnabled = true;
        card4.events.onInputDown.add(function(){
            requestDouble(gamename, 3, lines, bet, sid);
        });
        card5.inputEnabled = true;
        card5.events.onInputDown.add(function(){
            requestDouble(gamename, 4, lines, bet, sid);
        });

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    }
}

function openDCard(dcard) {
    lockDisplay();
    disableInputCards();
    setTimeout("card1.loadTexture('card_'+dcard); openCard.play(); enableInputCards(); unlockDisplay();", 1000);
}

var selectedCardValue; //значение карты выбранной игроком
function openSelectedCard(selectedCardR, valuesOfAllCards) {
    openCard.play();
    cardArray[selectedCardR].loadTexture("card_"+valuesOfAllCards[selectedCardR-1]);
}

var valuesOfAllCards; // значения остальных карт [,,,,]
function openAllCards(valuesOfAllCards) {

    openCard.play();
    cardArray.forEach(function (item, i) {
        if(i > 0) {
            item.loadTexture('card_'+valuesOfAllCards[i-1]);
        }
    });

}

function hideAllCards(cardArray) {
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_bg');
        item.inputEnabled
    });
}

function disableInputCards() {
    card2.inputEnabled = false;
    card3.inputEnabled = false;
    card4.inputEnabled = false;
    card5.inputEnabled = false;
}

function enableInputCards() {
    card2.inputEnabled = true;
    card3.inputEnabled = true;
    card4.inputEnabled = true;
    card5.inputEnabled = true;
}





//функции для игры с последовательным выбором (веревки, ящики, бочки и т.д.)

// отображает результат выбора веревки (подлетает цифра и пересчитвается значение totalWin)
var loseTryInGame3 = false;
function showWinGame3(winRopeNumberSize, x, y, win, stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim) {
    if(typeWinRopeNumberAnim == 0) {
        if(loseTryInGame3 == false) {
            if(ropeValues[ropeStep - 1] > 0 && ropeStep == 5) {

                if(ropeValues[ropeStep - 1] !== 6) {
                    var text = game.add.text(x,y, win*betlineR*lines, { font: winRopeNumberSize+'px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                    linesScore.visible = false;
                    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                        font: scorePosions[1][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }

                ropeStep = 0;
                loseTryInGame3 = false; //удаляем память о проигрышной попытке (использовании шлема)

                setTimeout("game.state.start('game4')", timeoutGame3ToGame4);
            } else {
                if(win == 0) {
                    updateBalanceGame3(game, scorePosions, balanceR);
                } else {
                    if(ropeValues[ropeStep - 1] !== 6) {
                        var text = game.add.text(x,y, win*betlineR*lines, { font: winRopeNumberSize+'px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                        linesScore.visible = false;
                        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                            font: scorePosions[1][2]+'px "Press Start 2P"',
                            fill: '#fcfe6e',
                            stroke: '#000000',
                            strokeThickness: 3,
                        });
                    }
                }
            }
        } else {
            if(ropeValues[ropeStep - 1] > 0 && ropeStep == 5) {
                if(ropeValues[ropeStep - 1] !== 6) {
                    var text = game.add.text(x,y, win*betlineR*lines, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                    linesScore.visible = false;
                    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                        font: scorePosions[1][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }

                ropeStep = 0;
                loseTryInGame3 = false; //удаляем память о проигрышной попытке (использовании шлема)

                setTimeout("game.state.start('game1')", timeoutGame3ToGame4);
            } else {
                if(win == 0) {
                    updateBalanceGame3(game, scorePosions, balanceR);
                } else {
                    if(ropeValues[ropeStep - 1] !== 6) {
                        var text = game.add.text(x,y, win*betlineR*lines, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                        linesScore.visible = false;
                        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                            font: scorePosions[1][2]+'px "Press Start 2P"',
                            fill: '#fcfe6e',
                            stroke: '#000000',
                            strokeThickness: 3,
                        });
                    }
                }
            }
        }

    }

}

function updateBalanceGame3(game, scorePosions, balanceR) {
    if(!isMobile) {
        hideButtons([[buttonLine1, 'buttonLine1'],[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], ]);
    }

    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = totalWinR;

    console.log(ropeValuesResult);

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], totalWinR+balanceR, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], 0, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}





//функции для игры с выбором из двух вариантов

function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;

    var ckeck = true;
    ropeValues.forEach(function (item) {
        if(parseInt(item) != 0 && parseInt(item) != 6 && ckeck == true) {
            ropeValuesResult += parseInt(item)*lines*betline;
        } else {
            ckeck = false;
        }
    });

    ropeValuesResult += parseInt(ropeValues[5]);

    var balanceDifference = parseInt(totalWinR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(totalWinR));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        unlockDisplay();
        game.state.start("game1");
    }, timeInterval);
}





//функции для мобильной версии

var startButton; var double; var bet1; var dollar; var gear; var home;
function addButtonsGame1Mobile(game) {

    startButton = game.add.sprite(588, 228, 'startButton');
    startButton.bringToTop();
    startButton.anchor.setTo(0.5, 0.5);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            startButton.loadTexture('startButton_d');
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    double = game.add.sprite(549, 133, 'double');
    double.inputEnabled = true;
    double.input.useHandCursor = true;
    double.events.onInputDown.add(function(){
        checkWin = 0;
        hideNumbersAmin();
        game.state.start('game2');
    });
    double.events.onInputUp.add(function(){
        double.loadTexture('double');
    });
    double.visible = false;

    bet1 = game.add.sprite(546, 274, 'bet1');
    bet1.inputEnabled = true;
    bet1.input.useHandCursor = true;
    bet1.events.onInputDown.add(function(){
        /*lines = 9;
        betline = 25;*/

        bet1.loadTexture('bet1_p');
        document.getElementById('betMode').style.display = 'block';
        widthVisibleZone = $('.betWrapper .visibleZone').height();
        //console.log(widthVisibleZone);
        $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
        $('canvas').css('display', 'none');
    });
    bet1.events.onInputUp.add(function(){
        bet1.loadTexture('bet1');
    });

    dollar = game.add.sprite(445, 30, 'dollar');
    dollar.scale.setTo(0.7,0.7);
    dollar.inputEnabled = true;
    dollar.input.useHandCursor = true;
    dollar.events.onInputDown.add(function(){
        //game.state.start('game4');
    });

    gear = game.add.sprite(519, 30, 'gear');
    gear.scale.setTo(0.7,0.7);
    gear.inputEnabled = true;
    gear.input.useHandCursor = true;
    gear.events.onInputDown.add(function(){
        //game.state.start('game3');
    });

    home = game.add.sprite(45, 30, 'home');
    home.scale.setTo(0.7,0.7);
    home.inputEnabled = true;
    home.input.useHandCursor = true;
    home.events.onInputDown.add(function(){
        home.loadTexture('home_d');
    });
    home.events.onInputUp.add(function(){
        home.loadTexture('home');
    });
}

function addButtonsGame2Mobile(game) {
    startButton = game.add.sprite(538, 300, 'collect');
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('collect');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });
    startButton.scale.setTo(0.7,0.7);
}

//выбор множителя в меню bet
var denomination = 1;
function selectDenomination(el) {
    denomination = el.innerText;

    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;

    var selectedElement = document.getElementsByClassName('denomSize selected');
    selectedElement[0].className = 'denomSize';

    el.className = 'denomSize selected';
}

//выставление максимального значения ставки на линию
function maxBetlineForBetMenu() {
    maxBetline();
    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    document.getElementsByClassName('checkCssTopBetLineRange')[0].style.top = '34.5%';
    document.querySelectorAll('.checkCssTopBetLineRange')[0].querySelectorAll('.selected')[0].classList.remove('selected');
    document.getElementById('cellBetLine25').classList.add('selected');
}

// функция для создания последовательных анимаций
var sourceArray;// [[name, [x,y], numbersOfFrames], frameFrequency]
function playAnimQueue(sourceArray) {

    var animArray = [];

    var countAnim = -1; // номер запускаемой анимации

    sourceArray.forEach(function(anim, index) {

        animArray.push(
            game.add.sprite(anim[1][0], anim[1][1], anim[0])
            );

        //создаем массив кадров
        var framesArray = [];
        for (var i = 0; i < anim[2]; i++) {
            framesArray.push(i);
        }

        // создаем анимацию
        animArray[index].animations.add(sourceArray[index][0], framesArray, sourceArray[index][3], false);
        animArray[index].animations.getAnimation(sourceArray[index][0]).play().onComplete.add(function(){
            recursiveFunction(animArray, sourceArray);
        });
        animArray[index].visible = false;
        topGroup.add(animArray[index]);

    });

    // создаем функцию, которая будет рекурсивно запускать анимации
    var recursiveFunction = function (animArray, sourceArray) {

        if ((countAnim + 1) == animArray.length) {
            countAnim = 0;

            animArray[animArray.length - 1].visible = false;
            animArray[countAnim].visible = true;
            animArray[countAnim].animations.getAnimation(sourceArray[countAnim][0]).play();
        } else {
            countAnim += 1;
            if(animArray[countAnim - 1]) {
                animArray[countAnim - 1].visible = false;
            }
            animArray[countAnim].visible = true;
            animArray[countAnim].animations.getAnimation(sourceArray[countAnim][0]).play();
        }

    };

    recursiveFunction(animArray, sourceArray, countAnim);
}
