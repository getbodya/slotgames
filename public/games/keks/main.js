function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}



(function () {
    var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example', {
        preload: preload,
        create: create,
        update: update
    });


    var bars = [];
    var rotateSound;
    var stopSound;
    var tadaSound;
    var spinning = false;
    var barsCurrentSpins = [0, 0, 0, 0, 0];
    var barsTotalSpins = [];
    var spinningBars = 0;
    var button;
    var currentLine = 1;

    /*MONKEY*/
    var monkey;
    var monkeyHand;
    var monkeyEyesAround;
    var monkeyEyesClose;
    var monkeyLipsMove;
    var monkeyBananMove;
    var monkeySeeSlot;
    var monkeySetSlot;
    var monkeyAnimations = {};

    var lines = {
        1: {
            coord: 242,
            sprite: null,
            btncoord: 247,
            button: null
        },
        2: {
            coord: 99,
            sprite: null
        },
        3: {
            coord: 385,
            sprite: null,
            btncoord: 312,
            button: null
        },
        4: {
            coord: 153,
            sprite: null
        },
        5: {
            coord: 335 - 200,
            sprite: null,
            btncoord: 380,
            button: null
        },
        6: {
            coord: 126,
            sprite: null
        },
        7: {
            coord: 361 - 100,
            sprite: null,
            btncoord: 444,
            button: null
        },
        8: {
            coord: 266,
            sprite: null
        },
        9: {
            coord: 218 - 68,
            sprite: null,
            btncoord: 508,
            button: null
        }
    };

    var tmpSpins = 15;
    for (var i = 0; i < 5; ++i) {
        barsTotalSpins[i] = tmpSpins;
        tmpSpins += 12;
    }


    function preload() {
        game.load.image('bars', 'bars.png');
        game.load.image('start', 'start.png');
        game.load.image('start_p', 'start_p.png');
        game.load.image('start_d', 'start_d.png');
        game.load.image('canvasbg', 'canvas-bg.png');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');

        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        preloadMonkey();
        preloadLevelButtons();
    }

    function hideLines() {
        console.log(lines);
        for (var i in lines) {
            lines[i].sprite.visible = false;
        }
    }

    function selectLine(n) {
        currentLine = n;

        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.visible = true;
            lines[i].sprite.loadTexture('line_' + i);
        }
    }

    function preselectLine(n) {
        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.loadTexture('linefull_' + i);
            lines[i].sprite.visible = true;
        }
    }

    function create() {

        rotateSound = game.add.audio('rotate');
        rotateSound.loop = true;
        stopSound = game.add.audio('stop');
        tadaSound = game.add.audio('tada');
        game.add.sprite(0, 0, 'canvasbg');

        var positions = [
            game.world.centerX - 270,
            game.world.centerX - 158,
            game.world.centerX - 47,
            game.world.centerX + 65,
            game.world.centerX + 177
        ];

        for (var i = 0; i < 5; ++i) {
            bars[i] = game.add.tileSprite(positions[i], game.world.centerY - 58, 96, 320, 'bars');
            bars[i].anchor.setTo(0.5, 0.5);
            bars[i].tilePosition.y = randomNumber(0, 8) * 112 - 8;
        }

        window.test = function () {
            bars[0].tilePosition.y -= 1;
        };

        function randomiseSpin() {
            return [
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106
            ];
        }

        button = game.add.sprite(game.world.centerX - 332, game.world.centerY + 242, 'start');

        function buttonClicked() {
            if (spinning) {
                return;
            }
            button.loadTexture('start_p');
        }

        function buttonRelease() {
            if (spinning) {
                return;
            }
            hideLines();
            barsCurrentSpins = [0, 0, 0, 0, 0];
            spinningBars = bars.length;
            spinning = true;
            rotateSound.play();
            monkeySeeSlotAnim();
            button.loadTexture('start_d');
        }

        button.anchor.setTo(0.5, 0.5);
        button.inputEnabled = true;
        button.input.useHandCursor = true;
        button.events.onInputDown.add(buttonClicked, this);
        button.events.onInputUp.add(buttonRelease, this);

        for (var i = 1; i <= 9; ++i) {
            lines[i].sprite = game.add.sprite(74, lines[i].coord, 'line_' + i);
            lines[i].sprite.visible = false;
            if (i % 2 != 0) {
                lines[i].sound = game.add.audio('line' + i);
                lines[i].button = game.add.sprite(lines[i].btncoord, 526, 'btnline' + i);
                lines[i].button.inputEnabled = true;
                (function (n) {
                    lines[n].button.events.onInputDown.add(function () {
                        hideLines();
                        preselectLine(n);
                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                    lines[n].button.events.onInputUp.add(function () {
                        hideLines();
                        selectLine(n);
                        lines[n].button.loadTexture('btnline' + n);
                        lines[n].sound.play();
                    }, this);
                })(i);
            }
        }
        preselectLine(1);
        createMonkey();
        createLevelButtons();
    }

    function update() {
        if (spinning) {
            for (var i in bars) {
                barsCurrentSpins[i]++;
                if (barsCurrentSpins[i] < barsTotalSpins[i]) {
                    bars[i].tilePosition.y += 112;
                } else if (barsCurrentSpins[i] == barsTotalSpins[i]) {
                    spinningBars--;
                    stopSound.play();
                }
            }
            if (!spinningBars) {
                spinning = false;
                rotateSound.stop();
                button.loadTexture('start');
                selectLine(currentLine);
                console.log('spin end');
                if (currentLine == 3) {
                    monkeySetSlotAnim();
                    preselectLine(3);
                    tadaSound.play();
                } else {
                    playRandomMonkeyAnim();
                }
            }
        }
    }

    function playRandomMonkeyAnim() {
        var keys = Object.keys(monkeyAnimations);
        var key = randomNumber(0, keys.length - 1);
        monkeyAnimations[keys[key]]();
    }

    function preloadMonkey() {
        game.load.image('monkey', 'monkey1/monkey.png');
        game.load.image('monkey_left', 'monkey1/monkey_left.png');
        game.load.spritesheet('monkey_eyes_around', 'monkey1/eyes_around.png', 35, 9, 8);
        game.load.spritesheet('monkey_eyes_close', 'monkey1/eyes_close.png', 34, 9, 7);
        game.load.spritesheet('monkey_lips_move', 'monkey1/lips_move.png', 60, 50, 7);
        game.load.spritesheet('monkey_banan_move', 'monkey1/banan_move.png', 124, 99, 8);
        game.load.spritesheet('monkey_see_slot', 'monkey1/see_slot.png', 116, 95, 2);
        game.load.spritesheet('monkey_set_slot', 'monkey1/set_slot.png', 140, 95, 3);
    }

    function monkeySeeSlotAnim() {
        monkeyEyesAround.animations.stop(true);
        monkeyEyesClose.animations.stop(true);
        monkeyLipsMove.animations.stop(true);
        monkeyBananMove.animations.stop(true);
        monkey.visible = false;
        monkeyHand.visible = false;
        monkeySeeSlot.visible = true;
        monkeySetSlot.visible = false;
        monkeyEyesAround.visible = false;
        monkeyEyesClose.visible = false;
        monkeyLipsMove.visible = false;
        monkeyBananMove.visible = false;
        monkeySeeSlot.animations.play('stand', 5, true);
    }

    function monkeySetSlotAnim() {
        monkeyEyesAround.animations.stop(true);
        monkeyEyesClose.animations.stop(true);
        monkeyLipsMove.animations.stop(true);
        monkeyBananMove.animations.stop(true);
        monkey.visible = false;
        monkeyHand.visible = false;
        monkeySeeSlot.visible = false;
        monkeySetSlot.visible = true;
        monkeyEyesAround.visible = false;
        monkeyEyesClose.visible = false;
        monkeyLipsMove.visible = false;
        monkeyBananMove.visible = false;
        monkeySetSlot.animations.play('stand', 5, true);
    }

    function createMonkey() {
        monkey = game.add.sprite(292, 405 - 6, 'monkey');
        monkeyHand = game.add.sprite(255, 405 - 6, 'monkey_left');

        //see slot
        monkeySeeSlot = game.add.sprite(265, 409 - 6, 'monkey_see_slot');
        monkeySeeSlot.visible = false;
        monkeySeeSlot.animations.add('stand');

        //set slot
        monkeySetSlot = game.add.sprite(265, 409 - 6, 'monkey_set_slot');
        monkeySetSlot.visible = false;
        monkeySetSlot.animations.add('stand');

        //eyes around
        monkeyEyesAround = game.add.sprite(309, 426 - 5, 'monkey_eyes_around');
        monkeyEyesAround.visible = false;
        monkeyEyesAround.animations.add('stand');
        monkeyAnimations.eyesAround = function () {
            monkey.visible = true;
            monkeyHand.visible = true;
            monkeyEyesAround.visible = true;
            monkeySeeSlot.visible = false;
            monkeySetSlot.visible = false;
            monkeyEyesAround.animations.play('stand', 5, false);
        };
        monkeyEyesAround.animations.currentAnim.onComplete.add(function () {
            monkeyEyesAround.visible = false;
            playRandomMonkeyAnim();
        });

        //eyes close
        monkeyEyesClose = game.add.sprite(309, 426 - 5, 'monkey_eyes_close');
        monkeyEyesClose.visible = false;
        monkeyEyesClose.animations.add('stand');
        monkeyAnimations.eyesClose = function () {
            monkey.visible = true;
            monkeyHand.visible = true;
            monkeySeeSlot.visible = false;
            monkeySetSlot.visible = false;
            monkeyEyesClose.visible = true;
            monkeyEyesClose.animations.play('stand', 5, false);
        };
        monkeyEyesClose.animations.getAnimation('stand').onComplete.add(function () {
            monkeyEyesClose.visible = false;
            playRandomMonkeyAnim();
        });

        //lips move
        monkeyLipsMove = game.add.sprite(292, 424 - 6, 'monkey_lips_move');
        monkeyLipsMove.visible = false;
        monkeyLipsMove.animations.add('stand');
        monkeyAnimations.lipsMove = function () {
            monkey.visible = true;
            monkeyHand.visible = true;
            monkeyLipsMove.visible = true;
            monkeySeeSlot.visible = false;
            monkeySetSlot.visible = false;
            monkeyLipsMove.animations.play('stand', 5, false);
        };
        monkeyLipsMove.animations.getAnimation('stand').onComplete.add(function () {
            monkeyLipsMove.visible = false;
            playRandomMonkeyAnim();
        });

        //banan move
        monkeyBananMove = game.add.sprite(292, 405 - 6, 'monkey_banan_move');
        monkeyBananMove.visible = false;
        monkeyBananMove.animations.add('stand');
        monkeyAnimations.bananMove = function () {
            monkeyBananMove.visible = true;
            monkey.visible = false;
            monkeyHand.visible = true;
            monkeySeeSlot.visible = false;
            monkeySetSlot.visible = false;
            monkeyBananMove.animations.play('stand', 5, false);
        };
        monkeyBananMove.animations.getAnimation('stand').onComplete.add(function () {
            monkeyBananMove.visible = false;
            monkey.visible = true;
            playRandomMonkeyAnim();
        });


        playRandomMonkeyAnim();
    }

    function preloadLevelButtons() {
        game.load.image('btnline11', 'lines/line11.png');
        game.load.image('btnline_p11', 'lines/line11_p.png');
        game.load.image('btnline13', 'lines/line13.png');
        game.load.image('btnline_p13', 'lines/line13_p.png');
        game.load.image('btnline15', 'lines/line15.png');
        game.load.image('btnline_p15', 'lines/line15_p.png');
    }

    function createLevelButtons() {
        var lvl1 = game.add.sprite(573, 526, 'btnline11');
        lvl1.inputEnabled = true;
        lvl1.input.useHandCursor = true;
        lvl1.events.onInputDown.add(function () {
            lvl1.loadTexture('btnline_p11');
        }, this);
        lvl1.events.onInputUp.add(function () {
            lvl1.loadTexture('btnline11');
            location.href = 'index.html';
        }, this);

        var lvl2 = game.add.sprite(639, 526, 'btnline13');
        lvl2.inputEnabled = true;
        lvl2.input.useHandCursor = true;
        lvl2.events.onInputDown.add(function () {
            lvl2.loadTexture('btnline_p13');
        }, this);
        lvl2.events.onInputUp.add(function () {
            lvl2.loadTexture('btnline13');
            location.href = 'index3.html';
        }, this);

        var lvl3 = game.add.sprite(703, 526, 'btnline15');
        lvl3.inputEnabled = true;
        lvl3.input.useHandCursor = true;
        lvl3.events.onInputDown.add(function () {
            lvl3.loadTexture('btnline_p15');
        }, this);
        lvl3.events.onInputUp.add(function () {
            lvl3.loadTexture('btnline15');
            location.href = 'index2.html';
        }, this);
    }

})();