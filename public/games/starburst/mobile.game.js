var game = new Phaser.Game(1024, 768, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

isMobile = true;

var checkHelm = false; //проверка надела ли каска

//переменные получаемые из api
//переменные получаемые из api
var gamename = 'starburst'; //название игры
var result;
var state;
var sid;
var user;
var min;
var id;
var balance = 5000;
var extralife = 45;
var jackpots;
var betLines = 10;
var betline = 10;
var betLvl = 10;
var coins = 5000;
var coinsValue = 1.00;
var lines = 10;
var bet = 100;
var mainBet = 100;
var mainWin = 0;
var info;
var wl;
var win = null;
var cash = 5000;

//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

var preFreeSpinAnim = false;
var wlWinValuesArray = [];
var wcvWinValuesArray = [];
function checkWin(){
    copyCellArr(info);
    wlWinValuesArray = [];
    wcvWinValuesArray = [];
    if (allWin > 0) {         
        stopWinAnim = false;        
        updateBalanceStatus = false;
        game.add.tween(justText).to({alpha: 0}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){         
            if (updateBalanceStatus === true){
                return;
            }          
            updateBalance();
        });
        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);       
            }
        });
        winCellInfo.forEach(function (cell, i) {
            if(cell !== false) {
                wcvWinValuesArray.push(i);
            } else {
                cellArray2[i].visible = true;
                cellArray[i].visible = false;
            }
        });
        var minBigWinValue = linesValue * levelValue * 15;
        if (allWin >= minBigWinValue){
            bigWinShow();
        } else{         
            showWin(wlWinValuesArray, wcvWinValuesArray);   
            if (wlWinValuesArray.length  == 1){
                WinSnd1.play();
            } else if (wlWinValuesArray.length >1 || wlWinValuesArray.length < 6){
                WinSnd2.play();
            } else if (wlWinValuesArray.length > 5){
                WinSnd3.play();
            }   
        }
    } else {        
        showButtons();
    }   


}    

var allWinOld = 0;
var updateBalanceStatus = true;
function updateBalance(){
    winCountLoop.play();
    var x = 0;
    var interval;
    winText.visible = true;
    winText.alpha = 1;
    winText.setText(0);
    coinsText.setText(balance)
    // allwinUpd = allWin+betline*20;
    allwinUpd = allWin;
    interval = 200;
    (function() {
        if (x < allWin) {
            if (updateBalanceStatus === true){
                return;
            }
                if (interval >= 1){
                    interval = interval*0.90;
                    x += 1;
                } else{
                    x = (x*1.01).toFixed();
                }
                winText.setText(x);
                if (winText.scale.x < 1.5){
                    winText.scale.x = winText.scale.x * 1.02;
                    winText.scale.y = winText.scale.y * 1.02;
                }
                setTimeout(arguments.callee, interval);
        } else {           
            winCountEnd.play();
            winCountLoop.stop();
            winText.setText(allWin);
            afterUpdateBalance();
        }
    })();
}
var afterUpdateBalanceStatus = false;
function afterUpdateBalance() {
    afterUpdateBalanceStatus = true;
    game.add.tween(winText.scale).to({x: 2, y: 2},150, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){ 
        if (afterUpdateBalanceStatus === false)
            return;                  
        game.add.tween(winText.scale).to({x: 1.5, y: 1.5},150, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){  
            if (afterUpdateBalanceStatus === false)
                return;                    
            game.add.tween(winText.scale).to({x: 2, y: 2},150, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){  
                if (afterUpdateBalanceStatus === false)
                    return;                    
                game.add.tween(winText.scale).to({x: 1.5, y: 1.5},150, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){ 
                    if (afterUpdateBalanceStatus === false)
                        return;                     
                    game.add.tween(winText.scale).to({x: 2, y: 2},150, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){   
                        afterUpdateBalanceStatus = false;
                        updateBalanceStatus = true; 
                        showWinValue();
                    });
                });
            });
        });
    });

}
function bigWinShow() {
    BigWin_1.play();
    sun.visible = true;
    game.add.tween(sun.scale).to({x: 1, y: 1},400, Phaser.Easing.Quadratic.Out, true);
    game.add.tween(sun).to({alpha: 1},200, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){        
        bigWinBG.visible = true;
        game.add.tween(bigWinBG.scale).to({x: 1, y: 1},200, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){ 
            setTimeout(function() {
                tweenTint(bigWinBG,  0xfffaa1,  0xffffff, 400);
                setTimeout(function() {
                    afterBigWinShow();
                }, 500);
            }, 400);
        });
        game.add.tween(bigWinBG).to({alpha: 1},1100, Phaser.Easing.Quadratic.Out, true);
    });
}

var tintColorArr = ['0xffffff', '0xd77be1', '0x7896f4', '0x4cd7be', '0xd77be1', '0xd8b64d', '0x61b546', '0x4ca6d7', '0x3cb56e', '0xf778a6', '0x7497f3', '0xffffff']
function afterBigWinShow() {
    BigWin_2.play();
    sun.scale.setTo(0.1, 0.1);
    sun.alpha = 0.5;
    sun.visible = false;
    bigWinText.visible = true;
    bigWinText.scale.setTo(0.8, 0.8);
    bigWinBG.scale.setTo(1.3, 1.3);
    game.add.tween(bigWinText.scale).to({x: 1, y: 1},6000, Phaser.Easing.Quadratic.Out, true);
    game.add.tween(bigWinBG.scale).to({x: 1, y: 1},300, Phaser.Easing.Quadratic.Out, true);
    for (var i = 0; i < tintColorArr.length-1; i++) {   
        var timer = 300+i*600;  
        startTwenTint (bigWinBG, tintColorArr[i], tintColorArr[i+1], timer, bigWinText, tintColorArr[tintColorArr.length-1-i], tintColorArr[tintColorArr.length-2-i]);
    };
    setTimeout(function() {
        game.add.tween(bigWinText.scale).to({x: 1.4, y: 1.4},100, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){
            bigWinText.visible = false;
            bigWinText.scale.setTo(1,1);
        });
        game.add.tween(bigWinBG).to({alpha: 0},200, Phaser.Easing.Quadratic.Out, true).onComplete.add(function(){
            bigWinBG.scale.setTo(0.1, 0.1);
            bigWinBG.visible = false;
            bigWinBG.tint = 0xfffaa1;
            showWin(wlWinValuesArray, wcvWinValuesArray);
        });
    }, 6900);
}
function startTwenTint(obj1, startColor1, endColor1, timer, obj2, startColor2, endColor2 ) {
    setTimeout(function() {
        tweenTint(obj1,  startColor1,  endColor1, 600);
        tweenTint(obj2,  startColor2,  endColor2, 600);
    }, timer);
}
function tweenTint(obj, startColor, endColor, time) {
    var colorBlend = {step: 0};    
    var colorTween = game.add.tween(colorBlend).to({step: 100}, time);      
    colorTween.onUpdateCallback(function() {
        obj.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step);     
    });        
    colorTween.onComplete.add(() => {
    });
    obj.tint = startColor;  
    colorTween.start();
}
var WInAnimCellArr = [];
function showWin(line, cell) {
    showButtons();
    WInAnimCellArr = [];
    line.forEach(function (lineNumber, i) {
        if (lineNumber <= 10){
            forwardWin(lineNumber);
        } else {
            reverseWin(lineNumber);
        }
    });
    showWinAnim();
}

function forwardWin(lineNumber) {
    var WInCellArr = [];
    WInCellArr.push(lineCellValue[lineNumber-1][0]);    
    WInCellArr.push(lineCellValue[lineNumber-1][1]);    
    WInCellArr.push(lineCellValue[lineNumber-1][2]);
    if ((winCellInfo[lineCellValue[lineNumber-1][3]] == winCellInfo[lineCellValue[lineNumber-1][0]]) || (winCellInfo[lineCellValue[lineNumber-1][3]] == 7)) {
        WInCellArr.push(lineCellValue[lineNumber-1][3]); 
        if ((winCellInfo[lineCellValue[lineNumber-1][4]] == winCellInfo[lineCellValue[lineNumber-1][0]]) || (winCellInfo[lineCellValue[lineNumber-1][4]] == 7)) {
            WInCellArr.push(lineCellValue[lineNumber-1][4]); 
        }
    }
    WInAnimCellArr.push(WInCellArr);
}
function reverseWin(lineNumber) {
    var WInCellArr = [];
    console.log(lineNumber)
    console.log(lineCellValue[lineNumber-11])
    console.log(lineCellValue[lineNumber-11][4])
    WInCellArr.push(lineCellValue[lineNumber-11][4]);    
    WInCellArr.push(lineCellValue[lineNumber-11][3]);    
    WInCellArr.push(lineCellValue[lineNumber-11][2]);
    if ((winCellInfo[lineCellValue[lineNumber-11][1]] == winCellInfo[lineCellValue[lineNumber-11][0]]) || (winCellInfo[lineCellValue[lineNumber-11][1]] == 7)) {
        WInCellArr.push(lineCellValue[lineNumber-11][1]); 
        if ((winCellInfo[lineCellValue[lineNumber-11][0]] == winCellInfo[lineCellValue[lineNumber-11][0]]) || (winCellInfo[lineCellValue[lineNumber-11][0]] == 7)) {
            WInCellArr.push(lineCellValue[lineNumber-11][0]); 
        }
    }
    WInAnimCellArr.push(WInCellArr);
}
var lineAnim = 0;
function showWinAnim(){
    var lineValue = winCellInfo[WInAnimCellArr[lineAnim][0]] // цвет выйгрынных ячеек
    var i = 0;
    (function() {
        if (stopWinAnim == true){
            // скрываем анимации звёзд при нажатии на кнопку старт
            WInAnimCellArr.forEach(function(value, j) {           
                WInAnimCellArr[j].forEach(function(value2, k) { 
                    animWinArr[winCellInfo[WInAnimCellArr[j][0]]][WInAnimCellArr[j][k]].visible = false;
                })
            })
            for (var j = 0; j < 10; j++) {
                winTextLineMove[j].visible = false;
                winTextLineMove[j+10].visible = false;
            }
            return;
        }
        if (i === 0){
            numberLineMove(lineValue, wlWinValuesArray[lineAnim]-1, wlValues[wlWinValuesArray[lineAnim]-1], WInAnimCellArr[lineAnim].length);
        }
        if (i < WInAnimCellArr[lineAnim].length) {
            var cellStar = animWinArr[lineValue][WInAnimCellArr[lineAnim][i]];
            cellStar.visible = true;
            winSymbol[WInAnimCellArr[lineAnim][i]].play();
            cellStar.animations.getAnimation('WinAnim' + lineValue).play().onComplete.add(function () {
                cellStar.visible = false;
            });
            i++;
            setTimeout(arguments.callee, 150);
        } else if (WInAnimCellArr.length > lineAnim+1){
            lineAnim = lineAnim + 1;
            if (lineAnim == 1){
                comboTextAnim(2);
            } else if (lineAnim == 4){
                comboTextAnim(5);
            } else if (lineAnim == 8){
                comboTextAnim(9);
            }
            showWinAnim();
        } else if (WInAnimCellArr.length == lineAnim+1){
            setTimeout(function() {
                recursiveWinLine();
            }, 800);
        }
    })();  
}
var lineflash = 0;
function recursiveWinLine() {
    if (stopWinAnim == true){
        return;
    }
    showLine(wlWinValuesArray[lineflash]);
    if (wlWinValuesArray[lineflash] <= 10){
        circleHoverArrLeft[wlWinValuesArray[lineflash]-1].visible = true;
        circleArrLeft[wlWinValuesArray[lineflash]-1].visible = false;
        circleHoverArrRight[wlWinValuesArray[lineflash]-1].visible = true;
        circleArrRight[wlWinValuesArray[lineflash]-1].visible = false;
    } else {
        circleHoverArrLeft[wlWinValuesArray[lineflash]-11].visible = true;
        circleArrLeft[wlWinValuesArray[lineflash]-11].visible = false;
        circleHoverArrRight[wlWinValuesArray[lineflash]-11].visible = true;
        circleArrRight[wlWinValuesArray[lineflash]-11].visible = false;
    }
    if ( WInAnimCellArr[lineflash][2] == 6){
        winTextLine1.setText(wlValues[wlWinValuesArray[lineflash]-1]);
        winTextLine1.visible = true;
        game.add.tween(winTextLine1).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true);
        game.add.tween(winTextLine1.scale).to({x: 1, y : 1}, 200, Phaser.Easing.LINEAR, true);
    } else if ( WInAnimCellArr[lineflash][2] == 7){
        winTextLine2.setText(wlValues[wlWinValuesArray[lineflash]-1]);
        winTextLine2.visible = true;
        game.add.tween(winTextLine2).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true);
        game.add.tween(winTextLine2.scale).to({x: 1, y : 1}, 200, Phaser.Easing.LINEAR, true);
    } else if ( WInAnimCellArr[lineflash][2] == 8){
        winTextLine3.setText(wlValues[wlWinValuesArray[lineflash]-1]);
        winTextLine3.visible = true;
        game.add.tween(winTextLine3).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true);
        game.add.tween(winTextLine3.scale).to({x: 1, y : 1}, 200, Phaser.Easing.LINEAR, true);
    }
    WInAnimCellArr[lineflash].forEach(function (cellNumber, i) {   
        copyCellArray[cellNumber].visible = true;   
        game.add.tween(copyCellArray[cellNumber]).to({alpha: 1}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            setTimeout(function() {
                game.add.tween(copyCellArray[cellNumber]).to({alpha: 0}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    copyCellArray[cellNumber].visible = false;
                    hideLines();
                    hideCircles();
                    hideWinTextLine();
                    if (i === WInAnimCellArr[lineflash].length - 1){ 
                        if (lineflash === WInAnimCellArr[lineflash].length - 1){
                            lineflash = 0;
                        } else {                        
                            lineflash = lineflash + 1;
                        }
                        recursiveWinLine();
                    }
                });
            }, 200);
        });
    });
}
var winLineMoveColor = ['#fbe10f', '#95e309', '#ff4f11', '#4e9be1', '#9f30ff', ,'#ffb630', '#44cefc']
function numberLineMove(color, line, winValue, countWinCell) {
    var stepAnim = 1;
    var scaleTime = (countWinCell-1)*300-200;
    var startPosX = winTextLineMove[line].position.x;
    var startPosY = winTextLineMove[line].position.y;
    winTextLineMove[line].visible = true;
    winTextLineMove[line].setText(winValue);
    winTextLineMove[line].stroke = winLineMoveColor[color];
    if (line <= 9){     
        game.add.tween(winTextLineMove[line]).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(winTextLineMove[line].scale).to({x: 1.3, y: 1.3}, scaleTime/2, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game.add.tween(winTextLineMove[line].scale).to({x: 1, y: 1}, scaleTime/2, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    game.add.tween(winTextLineMove[line]).to({alpha: 0}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        winTextLineMove[line].visible = false;
                        winTextLineMove[line].position.x = startPosX;
                        winTextLineMove[line].position.y = startPosY;
                    });
                });
            });
        });
        game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line][stepAnim]][0], y: cellCenterPosition[lineCellValue[line][stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            stepAnim = stepAnim +1;
            game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line][stepAnim]][0], y: cellCenterPosition[lineCellValue[line][stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                stepAnim = stepAnim +1;
                if (countWinCell >= stepAnim){
                    game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line][stepAnim]][0], y: cellCenterPosition[lineCellValue[line][stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        stepAnim = stepAnim +1;
                        if (countWinCell >= stepAnim){
                            game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line][stepAnim]][0], y: cellCenterPosition[lineCellValue[line][stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true);
                        } else{
                            winTextLineMove[line].visible = false;
                            winTextLineMove[line].position.x = startPosX;
                            winTextLineMove[line].position.y = startPosY;
                        }
                    });
                } else{
                    winTextLineMove[line].visible = false;
                    winTextLineMove[line].position.x = startPosX;
                    winTextLineMove[line].position.y = startPosY;
                }
            });
        });
    } else {
        game.add.tween(winTextLineMove[line]).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(winTextLineMove[line].scale).to({x: 1.3, y: 1.3}, scaleTime/2, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game.add.tween(winTextLineMove[line].scale).to({x: 1, y: 1}, scaleTime/2, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    game.add.tween(winTextLineMove[line]).to({alpha: 0}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        winTextLineMove[line].visible = false;
                        winTextLineMove[line].position.x = startPosX;
                        winTextLineMove[line].position.y = startPosY;
                    });
                });
            });
        });
        game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][0], y: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            stepAnim = stepAnim +1;
            game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][0], y: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                stepAnim = stepAnim +1;
                if (countWinCell >= stepAnim){
                    game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][0], y: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        stepAnim = stepAnim +1;
                        if (countWinCell >= stepAnim){
                            game.add.tween(winTextLineMove[line]).to({x: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][0], y: cellCenterPosition[lineCellValue[line-10][4-stepAnim]][1]}, 300, Phaser.Easing.LINEAR, true);
                        } else{
                            winTextLineMove[line].visible = false;
                            winTextLineMove[line].position.x = startPosX;
                            winTextLineMove[line].position.y = startPosY;
                        }
                    });
                } else {
                    winTextLineMove[line].visible = false;
                    winTextLineMove[line].position.x = startPosX;
                    winTextLineMove[line].position.y = startPosY;
                }
            });
        });
    }
}
function comboTextAnim(number) {
    if (number == 2){
        comboWinText.alpha = 0;
        comboWinText.visible = true;
        comboWinText.scale.x = 1.5;
        comboWinText.scale.y = 1.5;
        game.add.tween(comboWinText).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true);
        game.add.tween(comboWinText.scale).to({x: 1, y : 1}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(comboWinText.scale).to({x: 1.4, y : 1.4}, 400, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game.add.tween(comboWinText.scale).to({x: 1.5, y : 1.5}, 100, Phaser.Easing.LINEAR, true);
                game.add.tween(comboWinText).to({alpha: 0}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    comboWinText.visible = false;
                });
            });
        });
    } else if (number == 5){
        superWinText.alpha = 0;
        superWinText.visible = true;
        superWinText.scale.x = 1.5;
        superWinText.scale.y = 1.5;
        game.add.tween(superWinText).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true);
        game.add.tween(superWinText.scale).to({x: 1, y : 1}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(superWinText.scale).to({x: 1.4, y : 1.4}, 400, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game.add.tween(superWinText.scale).to({x: 1.5, y : 1.5}, 100, Phaser.Easing.LINEAR, true);
                game.add.tween(superWinText).to({alpha: 0}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    superWinText.visible = false;
                });
            });
        });
    } else if (number == 9){
        megaWinText.alpha = 0;
        megaWinText.visible = true;
        megaWinText.scale.x = 1.5;
        megaWinText.scale.y = 1.5;
        game.add.tween(megaWinText).to({alpha: 1}, 100, Phaser.Easing.LINEAR, true);
        game.add.tween(megaWinText.scale).to({x: 1, y : 1}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game.add.tween(megaWinText.scale).to({x: 1.4, y : 1.4}, 400, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game.add.tween(megaWinText.scale).to({x: 1.5, y : 1.5}, 100, Phaser.Easing.LINEAR, true);
                game.add.tween(megaWinText).to({alpha: 0}, 100, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    megaWinText.visible = false;
                });
            });
        });
    }
}
var cellArray = []; //массив содержащий все объекты не анимированных значений слотов
var numberOfSlotValues = 9; // кол-во возможных значений слотов
function addCell(game, slotLayer1Position, numberOfSlotValues) {

    var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
    var slotValueNames = []; // slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения

    for (var i = 0; i < numberOfSlotValues; i++) {
        slotValueNames.push('cell'+i);
    } //var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];

    slot1 = game.add.sprite(slotLayer1Position[0][0],slotLayer1Position[0][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot2 = game.add.sprite(slotLayer1Position[1][0],slotLayer1Position[1][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot3 = game.add.sprite(slotLayer1Position[2][0],slotLayer1Position[2][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot4 = game.add.sprite(slotLayer1Position[3][0],slotLayer1Position[3][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot5 = game.add.sprite(slotLayer1Position[4][0],slotLayer1Position[4][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot6 = game.add.sprite(slotLayer1Position[5][0],slotLayer1Position[5][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot7 = game.add.sprite(slotLayer1Position[6][0],slotLayer1Position[6][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot8 = game.add.sprite(slotLayer1Position[7][0],slotLayer1Position[7][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot9 = game.add.sprite(slotLayer1Position[8][0],slotLayer1Position[8][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot10 = game.add.sprite(slotLayer1Position[9][0],slotLayer1Position[0][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot11 = game.add.sprite(slotLayer1Position[10][0],slotLayer1Position[10][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot12 = game.add.sprite(slotLayer1Position[11][0],slotLayer1Position[11][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot13 = game.add.sprite(slotLayer1Position[12][0],slotLayer1Position[12][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot14 = game.add.sprite(slotLayer1Position[13][0],slotLayer1Position[13][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot15 = game.add.sprite(slotLayer1Position[14][0],slotLayer1Position[14][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);

    cellArray = [slot1, slot2, slot3, slot4, slot5, slot6, slot7, slot8, slot9, slot10, slot11, slot12, slot13, slot14, slot15];

    // qwe = game.add.sprite(cellCenterPosition[3][0], cellCenterPosition[3][1], '3');
    // qwer = game.add.sprite(cellCenterPosition[10][0], cellCenterPosition[10][1], '4');
    // qwe.anchor.setTo(0.5, 0.5);
    // qwer.anchor.setTo(0.5, 0.5);
}

var cellArray2 = [];
function addCell2(game, cellLayer1Position, numberOfSlotValues) {

    var cell1; var cell2; var cell3;  var cell4; var cell5; var cell6; var cell7; var cell8; var cell9; var cell10; var cell11; var cell12; var cell13; var cell14; var cell15;
    var cellValueNames = []; // cellValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения

    for (var i = 0; i < numberOfSlotValues; i++) {
        cellValueNames.push('cell'+i);
    } //var cellValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];

    cell1 = game.add.sprite(cellLayer1Position[0][0],cellLayer1Position[0][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell2 = game.add.sprite(cellLayer1Position[1][0],cellLayer1Position[1][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell3 = game.add.sprite(cellLayer1Position[2][0],cellLayer1Position[2][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell4 = game.add.sprite(cellLayer1Position[3][0],cellLayer1Position[3][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell5 = game.add.sprite(cellLayer1Position[4][0],cellLayer1Position[4][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell6 = game.add.sprite(cellLayer1Position[5][0],cellLayer1Position[5][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell7 = game.add.sprite(cellLayer1Position[6][0],cellLayer1Position[6][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell8 = game.add.sprite(cellLayer1Position[7][0],cellLayer1Position[7][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell9 = game.add.sprite(cellLayer1Position[8][0],cellLayer1Position[8][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell10 = game.add.sprite(cellLayer1Position[9][0],cellLayer1Position[0][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell11 = game.add.sprite(cellLayer1Position[10][0],cellLayer1Position[10][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell12 = game.add.sprite(cellLayer1Position[11][0],cellLayer1Position[11][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell13 = game.add.sprite(cellLayer1Position[12][0],cellLayer1Position[12][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell14 = game.add.sprite(cellLayer1Position[13][0],cellLayer1Position[13][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell15 = game.add.sprite(cellLayer1Position[14][0],cellLayer1Position[14][1], cellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    cell1.alpha = 0.5;
    cell2.alpha = 0.5;
    cell3.alpha = 0.5;
    cell4.alpha = 0.5;
    cell5.alpha = 0.5;
    cell6.alpha = 0.5;
    cell7.alpha = 0.5;
    cell8.alpha = 0.5;
    cell9.alpha = 0.5;
    cell10.alpha = 0.5;
    cell11.alpha = 0.5;
    cell12.alpha = 0.5;
    cell13.alpha = 0.5;
    cell14.alpha = 0.5;
    cell15.alpha = 0.5;

    cell1.visible = false;
    cell2.visible = false;
    cell3.visible = false;
    cell4.visible = false;
    cell5.visible = false;
    cell6.visible = false;
    cell7.visible = false;
    cell8.visible = false;
    cell9.visible = false;
    cell10.visible = false;
    cell11.visible = false;
    cell12.visible = false;
    cell13.visible = false;
    cell14.visible = false;
    cell15.visible = false;

    cellArray2 = [cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8, cell9, cell10, cell11, cell12, cell13, cell14, cell15];
}
var bars = [];
function addBars() { 
    bars = [];
    bars[0] = game.add.tileSprite(125, 82, 160, 453, 'slotBar');
    bars[0].tilePosition.y =  randomNumber(0,7)*151 ;
    bars[1] = game.add.tileSprite(280, 82, 160, 453, 'slotBar');
    bars[1].tilePosition.y =  randomNumber(0,7)*151;
    bars[2] = game.add.tileSprite(435 , 82, 160, 453, 'slotBar');
    bars[2].tilePosition.y =  randomNumber(0,7)*151;
    bars[3] = game.add.tileSprite(590, 82, 160, 453, 'slotBar');
    bars[3].tilePosition.y =  randomNumber(0,7)*151;
    bars[4] = game.add.tileSprite(745, 82, 160, 453, 'slotBar');
    bars[4].tilePosition.y =  randomNumber(0,7)*151;
    bars[0].visible = false;
    bars[1].visible = false;
    bars[2].visible = false;
    bars[3].visible = false;
    bars[4].visible = false;
}
var copyCellArray = [];
function addCell3(game, cellLayer1Position, numberOfSlotValues) {

    var copyCell1; var copyCell2; var copyCell3;  var copyCell4; var copyCell5; var copyCell6; var copyCell7; var copyCell8; var copyCell9; var copyCell10; var copyCell11; var copyCell12; var copyCell13; var copyCell14; var copyCell15;
     // cellValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
     var copyCellValueNames = [];
     for (var i = 0; i < numberOfSlotValues; i++) {
        copyCellValueNames.push('cell'+i);
    } //var cellValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];

    copyCell1 = game.add.sprite(cellLayer1Position[0][0],cellLayer1Position[0][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell2 = game.add.sprite(cellLayer1Position[1][0],cellLayer1Position[1][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell3 = game.add.sprite(cellLayer1Position[2][0],cellLayer1Position[2][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell4 = game.add.sprite(cellLayer1Position[3][0],cellLayer1Position[3][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell5 = game.add.sprite(cellLayer1Position[4][0],cellLayer1Position[4][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell6 = game.add.sprite(cellLayer1Position[5][0],cellLayer1Position[5][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell7 = game.add.sprite(cellLayer1Position[6][0],cellLayer1Position[6][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell8 = game.add.sprite(cellLayer1Position[7][0],cellLayer1Position[7][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell9 = game.add.sprite(cellLayer1Position[8][0],cellLayer1Position[8][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell10 = game.add.sprite(cellLayer1Position[9][0],cellLayer1Position[0][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell11 = game.add.sprite(cellLayer1Position[10][0],cellLayer1Position[10][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell12 = game.add.sprite(cellLayer1Position[11][0],cellLayer1Position[11][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell13 = game.add.sprite(cellLayer1Position[12][0],cellLayer1Position[12][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell14 = game.add.sprite(cellLayer1Position[13][0],cellLayer1Position[13][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell15 = game.add.sprite(cellLayer1Position[14][0],cellLayer1Position[14][1], copyCellValueNames[randomNumber(0, numberOfSlotValues-1)]);
    copyCell1.visible = false;
    copyCell2.visible = false;
    copyCell3.visible = false;
    copyCell4.visible = false;
    copyCell5.visible = false;
    copyCell6.visible = false;
    copyCell7.visible = false;
    copyCell8.visible = false;
    copyCell9.visible = false;
    copyCell10.visible = false;
    copyCell11.visible = false;
    copyCell12.visible = false;
    copyCell13.visible = false;
    copyCell14.visible = false;
    copyCell15.visible = false;
    copyCell1.alpha = 0;
    copyCell2.alpha = 0;
    copyCell3.alpha = 0;
    copyCell4.alpha = 0;
    copyCell5.alpha = 0;
    copyCell6.alpha = 0;
    copyCell7.alpha = 0;
    copyCell8.alpha = 0;
    copyCell9.alpha = 0;
    copyCell10.alpha = 0;
    copyCell11.alpha = 0;
    copyCell12.alpha = 0;
    copyCell13.alpha = 0;
    copyCell14.alpha = 0;
    copyCell15.alpha = 0;
    copyCellArray = [copyCell1, copyCell2, copyCell3, copyCell4, copyCell5, copyCell6, copyCell7, copyCell8, copyCell9, copyCell10, copyCell11, copyCell12, copyCell13, copyCell14, copyCell15];

}

var slotPosition;

// spin запрос
var finalValues; var wlValues; var balance; var totalWin; var totalWinR;
var dcard; var lines; var linesR; var betline; var betlineR; var bet; var extralife; var wcvValues;
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeStep = 0; var spinWave = 0;
var allWin; var freeSpin; var dataArrValue = 0;;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest;

    if (dataArray['state']) {

        winCellInfo = dataArray['winCellInfo'];
        wlValues = dataArray['wl'];

        balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
        balance = dataArray['balance']; // получаем реальный баланс

        betlineR = dataArray['betLine'];
        betline = dataArray['betLine'];

        allWin = dataArray['allWin'];

        info = dataArray['info'];
        fallNewCellAnim(info);
    }

}

//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса

var sessionName;

function requestInit() {
    $.ajax({
        type: "get",
        url: 'http://'+location.hostname+'/init',
        dataType: 'html',
        success: function (data) {
            dataString = data;
            if(dataString) {
                sessionName = data;
                requestState()
            } else {
                alert('Ошибка 11');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = 'ошибка 10';
            console.log(errorText);
            setTimeout("requestState();", 2000);
        }
    });
}

function requestState() {
    $.ajax({
        type: "get",
        url: 'http://'+location.hostname+'/state?sessionName='+sessionName,
        dataType: 'html',
        success: function (data) {

            dataArray = JSON.parse(data);

            if(dataArray['state']) {
                balance = dataArray['balance'];
                info = dataArray['info'];
                // game.state.start('gamePreload'); //начало игры с локации загрузчика
            } else {
                alert('Ошбика 21');
            }
            game1();
            // game2();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = 'ошибка 20';
            alert(errorText);
            setTimeout("requestInit();", 2000);
        }
    });
}
var data;
function requestSpin(gamename, sessionName, betline, lines) {

    // Когда будет АПИ

    // $.ajax({
    //     type: "get",
    //     url: 'http://'+location.hostname+'/spin/'+gamename+'?sessionName='+sessionName+'&betLine='+betline+'&linesInGame='+lines,
    //     dataType: 'html',
    //     success: function (data) {
    //         console.log(data);

    //         dataSpinRequest = JSON.parse(data);
    //         if(dataSpinRequest[0]['state']) {
    //             parseSpinAnswer(dataSpinRequest);
    //         }
    //     },
    //     error: function (xhr, ajaxOptions, thrownError) {
    //         var errorText = '//ошибка 30';
    //         console.log(errorText);
    //         setTimeout("requestSpin(gamename, sessionName, betline, lines)", 2000);
    //     }
    // });
    // data = '{"info":[3,3,5,0,1,5,4,7,7,3,1,0,6,5,6],"winCellInfo":[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false],"allWin":0,"betLine":"1","linesInGame":"30","state":true,"balance":500000,"rope":false,"freeSpin":false}';
   //win
   data = '{"info":[3,3,5,3,1,5,3,7,3,2,3,0,3,5,6],"winCellInfo":[3,3,false,3,false,false,3,false,3,false,3,false,3,false,false],"wl":[0,50,0,0,0,0,0,0,25,0,0,0,0,25,0,0,0,0,0,0],"allWin":100,"betLine":"10","state":true,"balance":500000}'
   dataSpinRequest = JSON.parse(data);
   if(dataSpinRequest['state']) {
    parseSpinAnswer(dataSpinRequest);
   }
}

var bet_level, coin_value, max_bet, paytable, start, fullButton, soundButton, settings, autoStart;
var flyEnable = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
var flyEnable2 = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
var flyEnable3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var visibleStatus = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
var rotateEnable = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
var betNumber = 0;
var currentPage = null;
var pagePaytables =[];
var coinValueArr = [0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1.00];
var coinValueNumber = 6;
var soundStatus = true;
var fullStatus = false;
var linesValue = 10;
var levelValue = 10;
var paytable_status = false;
var lineValue;
var levelValue;
var lineStep;
var levelStep;
var stopWinAnim = false;
function addButtonsGame1(game) {
    auto_play = game.add.sprite(351, 651, 'auto_play');
    auto_play.inputEnabled = true;
    auto_play.input.useHandCursor = true;
    auto_play.events.onInputOver.add(function(){
        auto_play.loadTexture('auto_play_h');
    });
    auto_play.events.onInputOut.add(function(){
        auto_play.loadTexture('auto_play');
    });
    auto_play.events.onInputDown.add(function(){
        auto_play.loadTexture('auto_play_p');
    });
    auto_play.events.onInputUp.add(function(){
        PanelButton.play();
        if (paytable_status){
            close_paytable();
        }
        if (stopWinAnim === false){
            stopWinAnimFun();
        }
        if (updateBalanceStatus === false){
            preShowWinValue();
        }
        auto_play.loadTexture('auto_play');
        //     coinValueNumber += 1;
        //     if (coinValueNumber == coinValueArr.length){
        //       coinValueNumber = 0;
        //       coinsValueText.setText(coinValueArr[coinValueNumber]);
        //       coinsText.setText(coins/coinValueArr[coinValueNumber]);
        //   } else {
        //     coinsValueText.setText(coinValueArr[coinValueNumber]);
        //     coinsText.setText(coins/coinValueArr[coinValueNumber]);
        // }
        // mainBetText.setText(betArr[betNumber]*coinValueArr[coinValueNumber])
    });
    max_bet = game.add.sprite(558, 651, 'max_bet');
    max_bet.inputEnabled = true;
    max_bet.input.useHandCursor = true;
    max_bet.events.onInputOver.add(function(){
        max_bet.loadTexture('max_bet_h');
    });
    max_bet.events.onInputOut.add(function(){
        max_bet.loadTexture('max_bet');
    });
    max_bet.events.onInputDown.add(function(){
        max_bet.loadTexture('max_bet_p');
    });
    max_bet.events.onInputUp.add(function(){
        max_bet.loadTexture('max_bet');
        SpinButton.play();        
        if (stopWinAnim === false){
            stopWinAnimFun();
        }
        if (updateBalanceStatus === false){
            preShowWinValue();
        }
        hideButtons();
        setLines(10);
        setLevel(10);
        showAllLine();        
        setTimeout(function(){
            hideWinText();
            pressStart();  
        }, 1000);
    });

    start = game.add.sprite(468, 638, 'start');
    start.inputEnabled = true;
    start.input.useHandCursor = true;
    start.events.onInputOver.add(function(){
        start.loadTexture('start_h');
    });
    start.events.onInputOut.add(function(){
        start.loadTexture('start');
    });
    start.events.onInputDown.add(function(){
        start.loadTexture('start_p');
    });
    start.events.onInputUp.add(function(){
        SpinButton.play(); 
        if (stopWinAnim === false){
            stopWinAnimFun();
        }
        if (updateBalanceStatus === false){
            preShowWinValue();
        }
        hideWinText();
        pressStart();
    });
    leftLines = game.add.sprite(36, 668, 'left_btn');
    leftLines.inputEnabled = true;
    leftLines.input.useHandCursor = true;
    leftLines.events.onInputOver.add(function(){
        leftLines.loadTexture('left_btn_h');
    });
    leftLines.events.onInputOut.add(function(){
        leftLines.loadTexture('left_btn');
    });
    leftLines.events.onInputDown.add(function(){
        leftLines.loadTexture('left_btn_p');
        PanelButton.play();
    });
    leftLines.events.onInputUp.add(function(){       
        linesValue -= 1;
        setLines(linesValue);
    });
    rightLines = game.add.sprite(137, 668, 'right_btn');
    rightLines.inputEnabled = true;
    rightLines.input.useHandCursor = true;
    rightLines.events.onInputOver.add(function(){
        rightLines.loadTexture('right_btn_h');
    });
    rightLines.events.onInputOut.add(function(){
        rightLines.loadTexture('right_btn');
    });
    rightLines.events.onInputDown.add(function(){
        rightLines.loadTexture('right_btn_p');
        PanelButton.play();
    });
    rightLines.events.onInputUp.add(function(){
        linesValue += 1;
        setLines(linesValue);
    });

    leftLevel = game.add.sprite(196, 668, 'left_btn');
    leftLevel.inputEnabled = true;
    leftLevel.input.useHandCursor = true;
    leftLevel.events.onInputOver.add(function(){
        leftLevel.loadTexture('left_btn_h');
    });
    leftLevel.events.onInputOut.add(function(){
        leftLevel.loadTexture('left_btn');
    });
    leftLevel.events.onInputDown.add(function(){
        leftLevel.loadTexture('left_btn_p');
        PanelButton.play();
    });
    leftLevel.events.onInputUp.add(function(){
        levelValue -= 1;
        setLevel(levelValue);
    });

    rightLevel = game.add.sprite(297, 668, 'right_btn');
    rightLevel.inputEnabled = true;
    rightLevel.input.useHandCursor = true;
    rightLevel.events.onInputOver.add(function(){
        rightLevel.loadTexture('right_btn_h');
    });
    rightLevel.events.onInputOut.add(function(){
        rightLevel.loadTexture('right_btn');
    });
    rightLevel.events.onInputDown.add(function(){
        rightLevel.loadTexture('right_btn_p');
        PanelButton.play();
    });
    rightLevel.events.onInputUp.add(function(){
        levelValue += 1;
        setLevel(levelValue);
    });

    leftCoinValue = game.add.sprite(701, 668, 'left_btn');
    leftCoinValue.inputEnabled = true;
    leftCoinValue.input.useHandCursor = true;
    leftCoinValue.events.onInputOver.add(function(){
        leftCoinValue.loadTexture('left_btn_h');
    });
    leftCoinValue.events.onInputOut.add(function(){
        leftCoinValue.loadTexture('left_btn');
    });
    leftCoinValue.events.onInputDown.add(function(){
        leftCoinValue.loadTexture('left_btn_p');
        PanelButton.play();
    });
    leftCoinValue.events.onInputUp.add(function(){
        coinValueNumber -= 1;
        setCoinValue(coinValueNumber);
    });

    rightCoinValue = game.add.sprite(850, 668, 'right_btn');
    rightCoinValue.inputEnabled = true;
    rightCoinValue.input.useHandCursor = true;
    rightCoinValue.events.onInputOver.add(function(){
        rightCoinValue.loadTexture('right_btn_h');
    });
    rightCoinValue.events.onInputOut.add(function(){
        rightCoinValue.loadTexture('right_btn');
    });
    rightCoinValue.events.onInputDown.add(function(){
        rightCoinValue.loadTexture('right_btn_p');
        PanelButton.play();
    });
    rightCoinValue.events.onInputUp.add(function(){
        coinValueNumber += 1;
        setCoinValue(coinValueNumber);
    });

    paytable_mask = game.add.sprite(-9+25, 525+23, 'paytable_mask2');
    paytable = game.add.sprite(-9, 525, 'paytable');
    paytable_mask.inputEnabled = true;
    paytable_mask.input.useHandCursor = true;
    paytable_mask.events.onInputOver.add(function(){
        paytable.loadTexture('paytable_h');
    });
    paytable_mask.events.onInputOut.add(function(){
        paytable.loadTexture('paytable');
    });
    paytable_mask.events.onInputDown.add(function(){
        paytable.loadTexture('paytable_p');
    });
    paytable_mask.events.onInputUp.add(function(){
        PaytableButton.play();
        paytable.loadTexture('paytable');
        pagePaytables[1].visible = true;
        left_btn.visible = true;
        center_btn.visible = true;
        right_btn.visible = true;
        currentPage = 1;
        paytable.visible = false;
        paytable_mask.visible = false;
        paytable_status = true;
        paytableVideo.visible = true;
        paytableVideoAnim.play();
    });
    pagePaytables[1] = game.add.sprite(76, 46, 'page_1');
    pagePaytables[1].visible = false;
    paytableVideo = game.add.sprite(355, 261, 'paytableVideo');
    paytableVideoAnim = paytableVideo.animations.add('paytableVideo',[], 10, true);
    paytableVideo.visible = false;    
    pagePaytables[2] = game.add.sprite(76, 46, 'page_2');
    pagePaytables[2].visible = false;

    left_btn = game.add.sprite(129, 538, 'paytable_left_btn');
    left_btn.visible = false;
    left_btn.inputEnabled = true;
    left_btn.input.useHandCursor = true;
    left_btn.events.onInputOver.add(function(){
        left_btn.loadTexture('paytable_left_btn_h');
    });
    left_btn.events.onInputOut.add(function(){
        left_btn.loadTexture('paytable_left_btn');
    });
    left_btn.events.onInputDown.add(function(){
        left_btn.loadTexture('paytable_left_btn_p');
        PaytableButton.play();
    });
    left_btn.events.onInputUp.add(function(){
        // btn.play();
        left_btn.loadTexture('paytable_left_btn');
        if (currentPage == 1){
            pagePaytables[currentPage].visible = false;
            paytableVideo.visible = false;
            paytableVideoAnim.stop();
            currentPage = 2;        
        } else{
            pagePaytables[currentPage].visible = false;
            currentPage -=1;
            paytableVideo.visible = true;
            paytableVideoAnim.play();
        }
        pagePaytables[currentPage].visible = true;
    });

    right_btn = game.add.sprite(221, 538, 'paytable_right_btn');
    right_btn.visible = false;
    right_btn.inputEnabled = true;
    right_btn.input.useHandCursor = true;
    right_btn.events.onInputOver.add(function(){
        right_btn.loadTexture('paytable_right_btn_h');
    });
    right_btn.events.onInputOut.add(function(){
        right_btn.loadTexture('paytable_right_btn');
    });
    right_btn.events.onInputDown.add(function(){
        right_btn.loadTexture('paytable_right_btn_p');
        PaytableButton.play();
    });
    right_btn.events.onInputUp.add(function(){
        // btn.play();
        right_btn.loadTexture('paytable_right_btn');
        if (currentPage == 2){
            pagePaytables[currentPage].visible = false;
            currentPage = 1;
            paytableVideo.visible = true;
            paytableVideoAnim.play();
        } else {                    
            pagePaytables[currentPage].visible = false;
            paytableVideo.visible = false;
            paytableVideoAnim.stop();
            currentPage +=1;
        }
        pagePaytables[currentPage].visible = true;
    });

    center_btn = game.add.sprite(172, 538, 'paytable_center_btn');
    center_btn.visible = false;
    center_btn.inputEnabled = true;
    center_btn.input.useHandCursor = true;
    center_btn.events.onInputOver.add(function(){
        center_btn.loadTexture('paytable_center_btn_h');
    });
    center_btn.events.onInputOut.add(function(){
        center_btn.loadTexture('paytable_center_btn');
    });
    center_btn.events.onInputDown.add(function(){
        center_btn.loadTexture('paytable_center_btn_p');
        PaytableButton.play();
    });
    center_btn.events.onInputUp.add(function(){
        // btn.play();
        center_btn.loadTexture('paytable_center_btn');
        close_paytable();
    });

    // fullButton and soundButton
    if (!fullStatus)
        fullButton = game.add.sprite(1000,4, 'nonFull');
    else
        fullButton = game.add.sprite(1000,4, 'fullButton');
    fullButton.inputEnabled = true;
    fullButton.input.useHandCursor = true;
    fullButton.events.onInputUp.add(function(){
        if (fullStatus == false){
            fullButton.loadTexture('fullButton');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            fullButton.loadTexture('nonFull');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus){
        soundButton = game.add.sprite(43,740, 'soundOn');
    }else{
        soundButton = game.add.sprite(43,740, 'soundOff');
    }
    soundButton.inputEnabled = true;
    soundButton.input.useHandCursor = true;
    soundButton.events.onInputOver.add(function(){
        if (soundStatus){
            soundButton.loadTexture('soundOn_h');
        }else{
            soundButton.loadTexture('soundOff_h');
        }
    });
    soundButton.events.onInputOut.add(function(){
        if (soundStatus){
            soundButton.loadTexture('soundOn');
        }else{
            soundButton.loadTexture('soundOff');
        }
    });
    soundButton.events.onInputUp.add(function(){
        if (soundStatus == true){
            soundButton.loadTexture('soundOff');
            soundStatus = false;
            game.sound.mute = true;
        } else {
            soundButton.loadTexture('soundOn');
            soundStatus = true;
            game.sound.mute = false;
        }
    });

    settings = game.add.sprite(5, 740, 'settings');
    settings.inputEnabled = true;
    settings.input.useHandCursor = true;
    settings.events.onInputOver.add(function(){
        settings.loadTexture('settings_h');
    });
    settings.events.onInputOut.add(function(){
        settings.loadTexture('settings');
    });
    settings.events.onInputDown.add(function(){

    });

    autoStart = game.add.sprite(81, 740, 'autoStart');
    autoStart.inputEnabled = true;
    autoStart.input.useHandCursor = true;
    autoStart.events.onInputOver.add(function(){
        autoStart.loadTexture('autoStart_h');
    });
    autoStart.events.onInputOut.add(function(){
        autoStart.loadTexture('autoStart');
    });
    autoStart.events.onInputDown.add(function(){

    });

    black_line_1 = game.add.sprite(36, 718, 'black_line_2');
    green_line_1 = game.add.sprite(36, 718, 'green_line_2');
    black_line_1.inputEnabled = true;
    black_line_1.input.useHandCursor = true;
    black_line_1.events.onInputOver.add(function(){
        green_line_1.loadTexture('green_line_2_h');
    });
    black_line_1.events.onInputOut.add(function(){
        green_line_1.loadTexture('green_line_2');
    });
    black_line_1.events.onInputDown.add(function(){
        green_line_1.loadTexture('green_line_2_p');
        lineValue = game.input.x - black_line_1.x;
        lineStep = Math.floor(lineValue/(black_line_1.width/10))+1;
        setLines(lineStep);
    });
    black_line_1.events.onInputUp.add(function(){
        green_line_1.loadTexture('green_line_2_p');
    });
    black_line_2 = game.add.sprite(197, 718, 'black_line_2');
    green_line_2 = game.add.sprite(197, 718, 'green_line_2');
    black_line_2.inputEnabled = true;
    black_line_2.input.useHandCursor = true;
    black_line_2.events.onInputOver.add(function(){
        green_line_2.loadTexture('green_line_2_h');
    });
    black_line_2.events.onInputOut.add(function(){
        green_line_2.loadTexture('green_line_2');
    });
    black_line_2.events.onInputDown.add(function(){
        green_line_2.loadTexture('green_line_2_p');
        levelValue = game.input.x - black_line_2.x;
        levelStep = Math.floor(levelValue/(black_line_2.width/10))+1;
        setLevel(levelStep);
    });
    black_line_2.events.onInputUp.add(function(){
        green_line_2.loadTexture('green_line_2_p');
    });
    black_line_3 = game.add.sprite(703, 718, 'black_line_1');
    green_line_3 = game.add.sprite(703, 718, 'green_line_1');
    black_line_3.inputEnabled = true;
    black_line_3.input.useHandCursor = true;
    black_line_3.events.onInputOver.add(function(){
        green_line_3.loadTexture('green_line_1_h');
    });
    black_line_3.events.onInputOut.add(function(){
        green_line_3.loadTexture('green_line_1');
    });
    black_line_3.events.onInputDown.add(function(){
        green_line_3.loadTexture('green_line_1_p');
        coinValuePos = game.input.x - black_line_3.x;
        coinValueStep = Math.floor(coinValuePos/(black_line_3.width/7));
        setCoinValue(coinValueStep);
    });
    black_line_3.events.onInputUp.add(function(){
        green_line_3.loadTexture('green_line_1_p');
    });
    hideButtons([[rightLevel,'right_btn']]);
    hideButtons([[rightLines,'right_btn']]);
    hideButtons([[rightCoinValue,'right_btn']]);
}
var leftNumbers = []; var rightNumbers = []; var circleArrLeft = []; var circleHoverArrLeft = [];
var circleArrRight = []; var circleHoverArrRight = [];var leftHoverNumbers = []; var rightHoverNumbers = [];
function addNumbers(game, numberPosition) {
    leftHoverNumbers = []; 
    leftNumbers = []; 
    rightHoverNumbers = [];
    rightNumbers = [];
    circleArrLeft = [];
    circleArrRight = [];
    circleHoverArrLeft = [];
    circleHoverArrRight = [];
    for (var i = 0; i < 10; i++) {
        if(numberPosition[0][i][0] != 0 && numberPosition[0][i][1] != 0) {
            var circle = game.add.sprite(numberPosition[0][i][0]+6,numberPosition[0][i][1]+7, 'circle');
            circleArrLeft.push(circle);
            var circle_h = game.add.sprite(numberPosition[0][i][0],numberPosition[0][i][1], 'circle_h');
            circle_h.visible = false;
            circleHoverArrLeft.push(circle_h);
            var number = game.add.sprite(numberPosition[0][i][0]+10,numberPosition[0][i][1]+13, 'number' + (i + 1));
            number.visible = false;
            leftNumbers.push(number);
            var number_h = game.add.sprite(numberPosition[0][i][0]+5,numberPosition[0][i][1]+8, 'number' + (i + 1) + '_h');
            leftHoverNumbers.push(number_h);
        }
    }

    for (var i = 0; i < 10; i++) {
        if(numberPosition[1][i][0] != 0 && numberPosition[1][i][1] != 0) {
            var circle = game.add.sprite(numberPosition[1][i][0]+6,numberPosition[1][i][1]+7, 'circle');
            circleArrRight.push(circle);
            var circle_h = game.add.sprite(numberPosition[1][i][0],numberPosition[1][i][1], 'circle_h');
            circle_h.visible = false;
            circleHoverArrRight.push(circle_h);
            var number = game.add.sprite(numberPosition[1][i][0]+10,numberPosition[1][i][1]+13, 'number' + (i + 1));
            number.visible = false;
            rightNumbers.push(number);
            var number_h = game.add.sprite(numberPosition[1][i][0]+5,numberPosition[1][i][1]+8, 'number' + (i + 1) + '_h');
            rightHoverNumbers.push(number_h);
        }

    }    

}

function addHoverForNumbers(hoverSquarePostion, numberPosition) {
    var numberCounter = 0;
    numberPosition[0].forEach(function (position, i) {
        if(position[0] != 0 && position[1] != 0) {
            circleArrLeft[numberCounter].inputEnabled = true;
            circleArrLeft[numberCounter].input.useHandCursor = true;
            circleArrLeft[numberCounter].events.onInputOver.add(function(){
                showLine(i+1);
                circleHoverArrLeft[i].visible = true;
                circleArrLeft[i].visible = false;
                circleHoverArrRight[i].visible = true;
                circleArrRight[i].visible = false;
            });
            circleHoverArrLeft[numberCounter].inputEnabled = true;
            circleHoverArrLeft[numberCounter].input.useHandCursor = true;
            circleHoverArrLeft[numberCounter].events.onInputOut.add(function(){
                hideLines();
                circleHoverArrLeft[i].visible = false;
                circleArrLeft[i].visible = true;
                circleHoverArrRight[i].visible = false;
                circleArrRight[i].visible = true;
            });
            circleHoverArrLeft[numberCounter].events.onInputUp.add(function(){
                setLines(i+1)
            });

            numberCounter += 1;
        }
    });

    // правые цифры
    var numberCounter = 0;
    numberPosition[1].forEach(function (position, i) {
        if(position[0] != 0 && position[1] != 0) {
            circleArrRight[numberCounter].inputEnabled = true;
            circleArrRight[numberCounter].input.useHandCursor = true;
            circleArrRight[numberCounter].events.onInputOver.add(function(){
                showLine(i+1);
                circleHoverArrLeft[i].visible = true;
                circleArrLeft[i].visible = false;
                circleHoverArrRight[i].visible = true;
                circleArrRight[i].visible = false;
            });
            circleHoverArrRight[numberCounter].inputEnabled = true;
            circleHoverArrRight[numberCounter].input.useHandCursor = true;
            circleHoverArrRight[numberCounter].events.onInputOut.add(function(){
                hideLines();
                circleHoverArrLeft[i].visible = false;
                circleArrLeft[i].visible = true;
                circleHoverArrRight[i].visible = false;
                circleArrRight[i].visible = true;
            });
            circleHoverArrRight[numberCounter].events.onInputUp.add(function(){
                setLines(i+1)
            });
            numberCounter += 1;
        }

    });
}

var linesArray = [];
function addLines(game, linePosition) {
    linesArray = [];
    for (var i = 0; i < 10; i++) {
        var line = game.add.sprite(linePosition[i][0],linePosition[i][1], 'line' + (i + 1));
        linesArray.push(line);
    }
}

function hideLines() {
    linesArray.forEach(function (line) {
        line.visible = false;
    });
}

function showLine(lineNumber) {
    if (lineNumber <= 10){
        linesArray[lineNumber - 1].visible = true;
    } else {
        linesArray[lineNumber - 11].visible = true;
    }
}
function showAllLine() {
    linesArray.forEach(function (line) {
        line.visible = true;
    });
}
function hideCircles() {
    circleHoverArrLeft.forEach(function (argument, i) {
        circleHoverArrLeft[i].visible = false;
        circleArrLeft[i].visible = true;
        circleHoverArrRight[i].visible = false;
        circleArrRight[i].visible = true;
    })
}
var betText, betLevelText, betLineText, coinsText, coinsValueText, winText, betLvl, justText, winTopText, winTextLine1, winTextLine2, winTextLine3;
function addBetScore(game, batScorePosion, bet, betline, betLines, coins, coinsValue, win) {
    betText = game.add.text(batScorePosion[0][0], batScorePosion[0][1], bet, {
        font: batScorePosion[0][2]+'px "Bauhaus 93"',
        fill: '#ffffb5'
    });
    betText.setShadow(0, 0, 'yellow', 5);
    betText.anchor.setTo(0.5, 0.5);

    betWord = game.add.text(105, 606, 'BET', {
        font: '21px "Bauhaus 93"',
        fill: '#ffffff'
    });
    betWord.anchor.setTo(0.5, 0.5);

    betLineText = game.add.text(batScorePosion[1][0], batScorePosion[2][1], betLines, {
        font: batScorePosion[1][2]+'px "Bauhaus 93"',
        fill: '#ffffb5'
    });
    betLineText.anchor.setTo(0.5, 0.5);
    betLineText.setShadow(0, 0, 'yellow', 5);

    betLineWord = game.add.text(105, 660, 'LINES', {
        font: '20px "Bauhaus 93"',
        fill: '#ffffff'
    });
    betLineWord.anchor.setTo(0.5, 0.5);

    betLevelText = game.add.text(batScorePosion[2][0], batScorePosion[1][1], betLvl, {
        font: batScorePosion[2][2]+'px "Bauhaus 93"',
        fill: '#ffffb5'
    });
    betLevelText.anchor.setTo(0.5, 0.5);
    betLevelText.setShadow(0, 0, 'yellow', 5);

    betLvltWord = game.add.text(263, 660, 'LEVEL', {
        font: '20px "Bauhaus 93"',
        fill: '#ffffff'
    });
    betLvltWord.anchor.setTo(0.5, 0.5);

    coinsText = game.add.text(batScorePosion[3][0], batScorePosion[3][1], balance, {
        font: batScorePosion[3][2]+'px "Bauhaus 93"',
        fill: '#ffffb5'
    });
    coinsText.anchor.setTo(0.5, 0.5);
    coinsText.setShadow(0, 0, 'yellow', 5);

    coinsWord = game.add.text(792, 660, 'COIN VALUE', {
        font: '20px "Bauhaus 93"',
        fill: '#ffffff'
    });
    coinsWord.anchor.setTo(0.5, 0.5);

    coinsValueText = game.add.text(batScorePosion[4][0], batScorePosion[4][1], coinsValue.toFixed(2), {
        font: batScorePosion[4][2]+'px "Bauhaus 93"',
        fill: '#ffffb5'
    });
    coinsValueText.anchor.setTo(0.5, 0.5);
    coinsValueText.setShadow(0, 0, 'yellow', 5);

    coinsValueWord = game.add.text(924, 606, 'COINS', {
        font: '20px "Bauhaus 93"',
        fill: '#ffffff'
    });
    coinsValueWord.anchor.setTo(0.5, 0.5);

    justText = game.add.text(512, 585, 'Win up to 50000 coins', {
        font: '35px "Bauhaus 93"',
        fill: '#ffffb5'
    });
    justText.anchor.setTo(0.5, 0.5);
    justText.setShadow(0, 0, 'yellow', 5);

    winTopText = game.add.text(512, 560, 'WIN', {
        font: '22px "Bauhaus 93"',
        fill: '#ffffff'
    });
    winTopText.anchor.setTo(0.5, 0.5);
    winTopText.alpha = 0;

    winText = game.add.text(512, 585, '1', {
        font: '40px "Bauhaus 93"',
        fill: '#ffffb5'
    });
    winText.anchor.setTo(0.5, 0.5); 
    winText.setShadow(0, 0, 'yellow', 5);   
    winText.visible = false;
}

var cashText,mainBetText, mainWinText;
function addMainScore(game, mainScorePosition, cash, mainBet, mainWin) {
    cashText = game.add.text(mainScorePosition[0][0], mainScorePosition[0][1], balance, {
        font: mainScorePosition[0][2]+'px "Arial"',
        fill: '#ffffff'
    });
    mainBetText = game.add.text(mainScorePosition[1][0], mainScorePosition[1][1], mainBet.toFixed(2), {
        font: mainScorePosition[1][2]+'px "Arial"',
        fill: '#ffffff'
    });
    mainWinText = game.add.text(mainScorePosition[2][0], mainScorePosition[2][1], mainWin, {
        font: mainScorePosition[2][2]+'px "Arial"',
        fill: '#ffffff'
    });
}
function addWinLineScore(){
    winTextLine1 = game.add.text(516, 162, '1', {
        font: '60px "Bauhaus 93"',
        fill: '#ffffb5',
        stroke: 'orange',
        strokeThickness: 4
    });
    winTextLine1.anchor.setTo(0.5, 0.5); 
    winTextLine1.setShadow(0, 0, 'black', 10);   
    winTextLine1.alpha = 0;
    winTextLine1.scale.x = 0.1;
    winTextLine1.scale.y = 0.1;
    winTextLine1.visible = false;

    winTextLine2 = game.add.text(516, 312, '2', {
        font: '60px "Bauhaus 93"',
        fill: '#ffffb5',
        stroke: 'orange',
        strokeThickness: 4
    });
    winTextLine2.anchor.setTo(0.5, 0.5); 
    winTextLine2.setShadow(0, 0, 'black', 15);   
    winTextLine2.alpha = 0;
    winTextLine2.scale.x = 0.1;
    winTextLine2.scale.y = 0.1;
    winTextLine2.visible = false;

    winTextLine3 = game.add.text(516, 462, '3', {
        font: '60px "Bauhaus 93"',
        fill: '#ffffb5',
        stroke: 'orange',
        strokeThickness: 4
    });
    winTextLine3.anchor.setTo(0.5, 0.5); 
    winTextLine3.setShadow(0, 0, 'black', 10);   
    winTextLine3.alpha = 0;
    winTextLine3.scale.x = 0.1;
    winTextLine3.scale.y = 0.1;
    winTextLine3.visible = false;
}
var winTextLineMove = [];
function addWinLineScoreMove() {
    for (var i = 0; i < 10; i++) {
        var lineNumber = lineCellValue[i][0];
        winTextLineMove[i] = game.add.text(cellCenterPosition[lineNumber][0], cellCenterPosition[lineNumber][1], i+1, {
            font: '60px "Bauhaus 93"',
            fill: '#ffffff',
            stroke: 'orange',
            strokeThickness: 5
        });
        winTextLineMove[i].anchor.setTo(0.5, 0.5);    
        winTextLineMove[i].alpha = 0;
        winTextLineMove[i].visible = false;
    }
    for (var i = 0; i < 10; i++) {
        var lineNumber = lineCellValue[i][4];
        winTextLineMove[i+10] = game.add.text(cellCenterPosition[lineNumber][0], cellCenterPosition[lineNumber][1], i+1+10, {
            font: '60px "Bauhaus 93"',
            fill: '#ffffff',
            stroke: 'orange',
            strokeThickness: 5
        });
        winTextLineMove[i+10].anchor.setTo(0.5, 0.5);    
        winTextLineMove[i+10].alpha = 0;
        winTextLineMove[i+10].visible = false;
    }
}
var paytableGroup, slotLayer1Group, slotLayer2Group, slotLayer3Group, treeLeavesGroup, panelGroup, buttonGroup, scoreGroup;

function fallOldCellAnim() {
    startspin(0);
    setTimeout(function() {
        startspin(1);
    }, 200);
    setTimeout(function() {
        startspin(2);
    }, 400);
    setTimeout(function() {
        startspin(3);
    }, 600);
    setTimeout(function() {
        startspin(4);
    }, 800);
}

function fallNewCellAnim(info) {
    setTimeout(function() {
        spinStatus1 = false;
        bars[0].visible = false;
        cellArray[0].visible = true;
        cellArray[1].visible = true;
        cellArray[2].visible = true;
        cellArray[0].loadTexture('cell'+info[0]);
        cellArray[1].loadTexture('cell'+info[1]);
        cellArray[2].loadTexture('cell'+info[2]);
        endspin(0);
    }, 2000);
    setTimeout(function() {
        spinStatus2 = false;
        bars[1].visible = false;
        cellArray[3].visible = true;
        cellArray[4].visible = true;
        cellArray[5].visible = true;
        cellArray[3].loadTexture('cell'+info[3]);
        cellArray[4].loadTexture('cell'+info[4]);
        cellArray[5].loadTexture('cell'+info[5]);
        endspin(1);
    }, 2200);
    setTimeout(function() {
        spinStatus3 = false;
        bars[2].visible = false;
        cellArray[6].visible = true;
        cellArray[7].visible = true;
        cellArray[8].visible = true;
        cellArray[6].loadTexture('cell'+info[6]);
        cellArray[7].loadTexture('cell'+info[7]);
        cellArray[8].loadTexture('cell'+info[8]);
        endspin(2);
    }, 2400);
    setTimeout(function() {
        spinStatus4 = false;
        bars[3].visible = false;
        cellArray[9].visible = true;
        cellArray[10].visible = true;
        cellArray[11].visible = true;
        cellArray[9].loadTexture('cell'+info[9]);
        cellArray[10].loadTexture('cell'+info[10]);
        cellArray[11].loadTexture('cell'+info[11]);
        endspin(3);
    }, 2600);

    setTimeout(function() {
        spinStatus5 = false;
        bars[4].visible = false;
        cellArray[12].visible = true;
        cellArray[13].visible = true;
        cellArray[14].visible = true;
        cellArray[12].loadTexture('cell'+info[12]);
        cellArray[13].loadTexture('cell'+info[13]);
        cellArray[14].loadTexture('cell'+info[14]);
        endspin(4);
    }, 2800);
}
function endspin(number) {
    cellArray[0+number*3].position.y = 82+30;
    cellArray[1+number*3].position.y = 232+30;
    cellArray[2+number*3].position.y = 382+30;
    ReelBounce.play();
    game.add.tween(cellArray[0+number*3]).to({y:cellArray[0+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true);
    game.add.tween(cellArray[1+number*3]).to({y:cellArray[1+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true);
    game.add.tween(cellArray[2+number*3]).to({y:cellArray[2+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        if (number == 4){
            reelSpin.stop();
            checkWin();
        }
    });
}

var spinStatus1 = false; var spinStatus2 = false; var spinStatus3 = false; var spinStatus4 = false; var spinStatus5 = false;
function startspin(number){
    cellArray[0+number*3].visible = true;
    cellArray2[0+number*3].visible = false;
    game.add.tween(cellArray[0+number*3]).to({y:cellArray[0+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        cellArray[0+number*3].visible = false;
    });
    cellArray[1+number*3].visible = true;
    cellArray2[1+number*3].visible = false;
    game.add.tween(cellArray[1+number*3]).to({y:cellArray[1+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        cellArray[1+number*3].visible = false;
    });
    cellArray[2+number*3].visible = true;
    cellArray2[2+number*3].visible = false;
    game.add.tween(cellArray[2+number*3]).to({y:cellArray[2+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
        cellArray[2+number*3].visible = false;
        bars[number].visible = true;
        if (number == 0){
            spinStatus1 = true;
        }
        if (number == 1){
            spinStatus2 = true;
        }
        if (number == 2){
            spinStatus3 = true;
        }
        if (number == 3){
            spinStatus4 = true;
        }
        if (number == 4){
            spinStatus5 = true;
        }
    });
};
var firstSpin = false;

function copyCellArr(finalValues) {
    for (var j = 0; j < 15; j++) {
        copyCellArray[j].loadTexture('cell'+finalValues[j]+"_f");
        cellArray[j].loadTexture('cell'+finalValues[j]);
        cellArray2[j].loadTexture('cell'+finalValues[j]);
    }
}

function hideButtons(buttonsArray = []) {
    if(buttonsArray.length == 0) {

        auto_play.loadTexture('auto_play_d');
        auto_play.inputEnabled = false;
        auto_play.input.useHandCursor = false;

        max_bet.loadTexture('max_bet_d'); 
        max_bet.inputEnabled = false;
        max_bet.input.useHandCursor = false;

        start.loadTexture('start_d'); 
        start.inputEnabled = false;
        start.input.useHandCursor = false;


        leftLines.loadTexture('left_btn_d'); 
        leftLines.inputEnabled = false;
        leftLines.input.useHandCursor = false;

        rightLines.loadTexture('right_btn_d'); 
        rightLines.inputEnabled = false;
        rightLines.input.useHandCursor = false;

        leftLevel.loadTexture('left_btn_d'); 
        leftLevel.inputEnabled = false;
        leftLevel.input.useHandCursor = false;

        rightLevel.loadTexture('right_btn_d'); 
        rightLevel.inputEnabled = false;
        rightLevel.input.useHandCursor = false;

        leftCoinValue.loadTexture('left_btn_d'); 
        leftCoinValue.inputEnabled = false;
        leftCoinValue.input.useHandCursor = false;

        rightCoinValue.loadTexture('right_btn_d'); 
        rightCoinValue.inputEnabled = false;
        rightCoinValue.input.useHandCursor = false; 

        green_line_1.loadTexture('green_line_2_d'); 
        black_line_1.inputEnabled = false;
        black_line_1.input.useHandCursor = false; 

        green_line_2.loadTexture('green_line_2_d'); 
        black_line_2.inputEnabled = false;
        black_line_2.input.useHandCursor = false; 

        green_line_3.loadTexture('green_line_1_d'); 
        black_line_3.inputEnabled = false;
        black_line_3.input.useHandCursor = false; 

    } else {
        buttonsArray.forEach(function (item) {
            item[0].loadTexture(item[1]+'_d');
            item[0].inputEnabled = false;
            item[0].input.useHandCursor = false;
        })
    }
}
var coinValueNumber = 6;
var linesValue = 10;
var levelValue = 10;
function showButtons(buttonsArray = []) {
    if(buttonsArray.length == 0) {

        auto_play.loadTexture('auto_play');
        auto_play.inputEnabled = true;
        auto_play.input.useHandCursor = true;

        max_bet.loadTexture('max_bet'); 
        max_bet.inputEnabled = true;
        max_bet.input.useHandCursor = true;

        start.loadTexture('start'); 
        start.inputEnabled = true;
        start.input.useHandCursor = true;
        if (linesValue !== 1){    
            leftLines.loadTexture('left_btn'); 
            leftLines.inputEnabled = true;
            leftLines.input.useHandCursor = true;
        }

        if (linesValue !== 10){   
            rightLines.loadTexture('right_btn'); 
            rightLines.inputEnabled = true;
            rightLines.input.useHandCursor = true;
        }

        if (levelValue !== 1){
            leftLevel.loadTexture('left_btn'); 
            leftLevel.inputEnabled = true;
            leftLevel.input.useHandCursor = true;
        }

        if (levelValue !== 10){
            rightLevel.loadTexture('right_btn'); 
            rightLevel.inputEnabled = true;
            rightLevel.input.useHandCursor = true;
        }

        if (coinValueNumber !== 0){            
            leftCoinValue.loadTexture('left_btn'); 
            leftCoinValue.inputEnabled = true;
            leftCoinValue.input.useHandCursor = true;
        }

        if (coinValueNumber !== 6){
            rightCoinValue.loadTexture('right_btn'); 
            rightCoinValue.inputEnabled = true;
            rightCoinValue.input.useHandCursor = true; 
        }

        green_line_1.loadTexture('green_line_2'); 
        black_line_1.inputEnabled = true;
        black_line_1.input.useHandCursor = true; 

        green_line_2.loadTexture('green_line_2'); 
        black_line_2.inputEnabled = true;
        black_line_2.input.useHandCursor = true; 

        green_line_3.loadTexture('green_line_1'); 
        black_line_3.inputEnabled = true;
        black_line_3.input.useHandCursor = true; 

    } else {
        buttonsArray.forEach(function (item) {
            item[0].loadTexture(item[1]);
            item[0].inputEnabled = true;
            item[0].input.useHandCursor = true;
        })
    }
}
function close_paytable() {
    paytable_status = false;
    pagePaytables[1].visible = false;
    pagePaytables[2].visible = false;
    paytableVideo.visible = false;
    paytableVideoAnim.stop();
    left_btn.visible = false;
    center_btn.visible = false;
    right_btn.visible = false;
    paytable.visible = true;
    paytable_mask.visible = true;
}
var countdown = 0;
// var timerId = setTimeout(function() { hideLines() }, 1500);
var timerId ;
function setLines(number) {
    linesValue = number;
    hideLines();
    if (stopWinAnim === false){
        stopWinAnimFun();
    }
    if (updateBalanceStatus === false){
        preShowWinValue();
    }
    clearTimeout(timerId);
    timerId = setTimeout(function() { hideLines() }, 1500);
    betLineText.setText(number);
    bet = number*levelValue;
    betText.setText(bet);
    mainBetText.setText((bet*coinValueArr[coinValueNumber]).toFixed(2));    
    green_line_1.width = black_line_1.width/10*number;
    showButtons([[rightLines,'right_btn'],[leftLines,'left_btn']]);
    if (number === 1){
        hideButtons([[leftLines,'left_btn']]);
    } else if (number === 10){
        hideButtons([[rightLines,'right_btn']]);
    } 
    if (paytable_status){
        close_paytable();
    }     
    for (var i = 1; i < 11; i++) {
        if (i > number){
            leftNumbers[i-1].visible = true; 
            leftHoverNumbers[i-1].visible = false;               
            rightNumbers[i-1].visible = true; 
            rightHoverNumbers[i-1].visible = false;               
        } else {
            showLine(i);
            leftHoverNumbers[i-1].visible = true;            
            leftNumbers[i-1].visible = false; 
            rightHoverNumbers[i-1].visible = true;            
            rightNumbers[i-1].visible = false; 
        }        
    }
}
function setLevel(number) {
    levelValue = number;
    if (stopWinAnim === false){
        stopWinAnimFun();
    }
    if (updateBalanceStatus === false){
        preShowWinValue();
    }
    betLevelText.setText(number);
    bet = linesValue*number;
    betText.setText(bet);
    mainBetText.setText((bet*coinValueArr[coinValueNumber]).toFixed(2))
    green_line_2.width = black_line_2.width/10*number;
    showButtons([[rightLevel,'right_btn'],[leftLevel,'left_btn']]);
    if (number === 1){
        hideButtons([[leftLevel,'left_btn']]);
    } else if (number === 10){
        hideButtons([[rightLevel,'right_btn']]);
    } 
    if (paytable_status){
        close_paytable();
    }     
}
function setCoinValue(number) {
    coinValueNumber = number;
    if (stopWinAnim === false){
        stopWinAnimFun();
    }
    coinsValueText.setText(coinValueArr[number].toFixed(2));
    mainBetText.setText((linesValue*levelValue*coinValueArr[number]).toFixed(2))
    green_line_3.width = black_line_3.width/7*(number+1);
    showButtons([[rightCoinValue,'right_btn'],[leftCoinValue,'left_btn']]);
    if (number === 0){
        hideButtons([[leftCoinValue,'left_btn']]);
    } else if (number === 6){
        hideButtons([[rightCoinValue,'right_btn']]);
    } 
    if (paytable_status){
        close_paytable();
    }     
}
var animWinArr = [];
function addWInAnim(game, cellCenterPosition, numberOfSlotValues) {
    animWinArr = [];
    for (var i = 0; i < 8; i++) {
        var infoArray = [];
        for (var j = 0; j < 15; j++) {
            infoArray.push(game.add.sprite(cellCenterPosition[j][0], cellCenterPosition[j][1], 'WinAnim' + i));
            infoArray[j].animations.add('WinAnim' + i, [], 30, false);
            infoArray[j].animations.getAnimation('WinAnim' + i).onComplete.add(function () {
                // infoArray[j].visible = false;
            });
            infoArray[j].anchor.setTo(0.5, 0.5);
            // infoArray[j].alpha = 0.5;
            infoArray[j].visible = false;
        } 
        animWinArr.push(infoArray);
    } 
}

var superWinText = null;
var comboWinText = null;
var megaWinText = null;
function comboText() {
    superWinText = game.add.text(512, 312, 'SUPER COMBO', {
        font: '70px "Bauhaus 93"',
        fill: '#ffffb5',
        stroke: 'black',
        strokeThickness: 10
    });
    superWinText.anchor.setTo(0.5, 0.5);
    superWinText.angle = 6;
    var whiteGreenGradient = superWinText.context.createLinearGradient(0, 0, 0, superWinText.height);

    //  Add in 2 color stops
    whiteGreenGradient.addColorStop(0, '#e8ff88');   
    whiteGreenGradient.addColorStop(1, '#99ff33');
    superWinText.fill = whiteGreenGradient;
    superWinText.visible = false;
    
    comboWinText = game.add.text(512, 222, 'COMBO', {
        font: '70px "Bauhaus 93"',
        fill: '#ffffb5',
        stroke: 'black',
        strokeThickness: 10
    });
    comboWinText.anchor.setTo(0.5, 0.5);
    comboWinText.angle = 10;
    var whiteBlueGradient = comboWinText.context.createLinearGradient(0, 0, 0, comboWinText.height);

    //  Add in 2 color stops
    whiteBlueGradient.addColorStop(0, '#ddf5ff');   
    whiteBlueGradient.addColorStop(1, '#3399ff');
    comboWinText.fill = whiteBlueGradient;
    comboWinText.visible = false;

    megaWinText = game.add.text(512, 250, 'MEGA COMBO', {
        font: '70px "Bauhaus 93"',
        fill: '#ffffb5',
        stroke: 'black',
        strokeThickness: 10
    });
    megaWinText.anchor.setTo(0.5, 0.5);
    megaWinText.angle = -10;
    var yellowOrangeGradient = megaWinText.context.createLinearGradient(0, 0, 0, megaWinText.height);

    //  Add in 2 color stops
    yellowOrangeGradient.addColorStop(0, '#fff288');   
    yellowOrangeGradient.addColorStop(1, '#e68900');
    megaWinText.fill = yellowOrangeGradient;
    megaWinText.visible = false;

}
var bigWinText = null;
function bigText() {
    bigWinText = game.add.text(512, 312, 'BIG WIN!', {
        font: '160px "Bauhaus 93"',
        fill: '#d77be1',
        stroke: '#00f',
        strokeThickness: 8
    });
    bigWinText.anchor.setTo(0.5, 0.5);
    var whiteGreenGradient = bigWinText.context.createLinearGradient(0, 0, 0, bigWinText.height);

    bigWinText.setShadow(0, 0, "#ffffff", 10, true, false);
    //  Add in 2 color stops
    whiteGreenGradient.addColorStop(0, '#ffffff');   
    whiteGreenGradient.addColorStop(1, '#e0d95e');
    bigWinText.fill = whiteGreenGradient;
    bigWinText.visible = false;
    
};
function pressStart() {
    if (paytable_status){
        close_paytable();
    }
    hideButtons();
    coinsText.setText(balance-bet);
    cashText.setText(balance-bet);
    hideCircles();
    hideWinTextLine();
    lineAnim = 0;
    lineflash = 0;
    hideLines();
    stopWinAnim = true;
    copyCellArray.forEach(function (value, i) {
        copyCellArray[i].visible = false;
    });
    requestSpin(gamename, sessionName, betline, lines);
    reelSpin.play();
    fallOldCellAnim();       
}
function hideWinTextLine() {
    winTextLine1.alpha = 0;
    winTextLine1.scale.x = 0.1;
    winTextLine1.scale.y = 0.1;
    winTextLine1.visible = false;
    winTextLine2.alpha = 0;
    winTextLine2.scale.x = 0.1;
    winTextLine2.scale.y = 0.1;
    winTextLine2.visible = false;
    winTextLine3.alpha = 0;
    winTextLine3.scale.x = 0.1;
    winTextLine3.scale.y = 0.1;
    winTextLine3.visible = false;
}
function stopWinAnimFun() {
    stopWinAnim = true;
    hideLines();
    hideWinTextLine();
    hideCircles();
    for (var i = 0; i < 15; i++) {
        cellArray[i].visible = true;
        cellArray2[i].visible = false;
        copyCellArray[i].visible = false;
    }
}
function preShowWinValue() {
    if (afterUpdateBalanceStatus === false){
        winCountEnd.play();
        winCountLoop.stop();
        winText.setText(allWin);
    } else {
        afterUpdateBalanceStatus = false;
    }
    updateBalanceStatus = true;
    showWinValue();
}
function showWinValue() {
    winText.visible = true;
    winText.alpha = 1;
    winText.scale.x = 1.5;
    winText.scale.y = 1.5;
    winText.fontSize = '50px';
    game.add.tween(winTopText).to({alpha: 1}, 200, Phaser.Easing.LINEAR, true);          
    game.add.tween(winText.scale).to({x: 1, y: 1},200, Phaser.Easing.Quadratic.Out, true);
    game.add.tween(winText).to({y : winText.position.y + 10},200, Phaser.Easing.Quadratic.Out, true);
}
function hideWinText() {
    setTimeout(function() {
        game.add.tween(winTopText).to({alpha: 0}, 400, Phaser.Easing.LINEAR, true);          
        game.add.tween(winText).to({alpha: 0}, 400, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            winText.position.y = 585;
            game.add.tween(justText).to({alpha: 1}, 400, Phaser.Easing.LINEAR, true);
        });
    }, 500);
}



//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

/* init-запрос */

requestInit();

var checkGame;
var lineCellValue = [];
var winSymbol = [];
function game1() {

    var game1 = {};

    game1.preload = function () {};

    // game1.soundStatus = false;
    game1.create = function () {
        game.physics.startSystem(Phaser.Physics.ARCADE);

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        // звуки
        bgSound = game.add.audio('bgSound');
        SpinButton = game.add.audio('SpinButton');
        PaytableButton = game.add.audio('PaytableButton');
        PanelButton = game.add.audio('PanelButton');
        ReelBounce = game.add.audio('ReelBounce');
        reelSpin = game.add.audio('ReelSpin');
        winCountLoop = game.add.audio('winCountLoop');
        winCountEnd = game.add.audio('winCountEnd');
        WinSnd1 = game.add.audio('WinSnd1');
        WinSnd2 = game.add.audio('WinSnd2');
        WinSnd3 = game.add.audio('WinSnd3');
        BigWin_1 = game.add.audio('BigWin_1');
        BigWin_2 = game.add.audio('BigWin_2');
        for (var i = 0; i < 15; i++) {
            winSymbol[i] = game.add.audio('WinSymbol');
        }
        winCountLoop.loop = true;
        reelSpin.loop = true;
        bgSound.loop = true;

        bgSound.play();
        // изображения

        bg1 = game.add.sprite(0,0, 'bg1');
        bg_fs = game.add.sprite(0,0, 'bg_fs');
        bg_fs.alpha = 0;

        //слоты
        cellLayer1Position = [
        [125, 82],
        [125, 232],
        [125, 382],
        [280, 82],
        [280, 232],
        [280, 382],
        [435, 82],
        [435, 232],
        [435, 382],
        [590, 82],
        [590, 232],
        [590, 382],
        [745, 82],
        [745, 232],
        [745, 382]
        ];
        cellCenterPosition = [
        [205, 162],
        [205, 312],
        [205, 462],
        [360, 162],
        [360, 312],
        [360, 462],
        [515, 162],
        [515, 312],
        [515, 462],
        [670, 162],
        [670, 312],
        [670, 462],
        [825, 162],
        [825, 312],
        [825, 462]
        ];
        
        // линии и номера
        var linePosition = [
        [204,314],
        [204,163],
        [204,466],
        [204,162],
        [204,158],
        [204,174],
        [204,302],
        [204,322],
        [204,173],
        [204,190]
        ];
        var numberPosition = [
        [[24,304], [28,133], [28,432], [32,93], [32,474], [26,177], [26,388], [24,346], [24,220],[22,262]],
        [[960,304], [951 ,133], [951,432], [947,93], [947,474], [955,177], [956,388], [960,346], [959,220], [960,262]]
        ];
        lineCellValue = [
        [1,4,7,10,13],
        [0,3,6,9,12],
        [2,5,8,11,14],
        [0,4,8,10,12],
        [2,4,6,10,14],
        [0,3,7,9,12],
        [2,5,7,11,14],
        [1,5,8,11,13],
        [1,3,6,9,13],
        [1,3,7,9,13],
        ];
        addCell2(game, cellLayer1Position, 8);
        addNumbers(game, numberPosition);
        addLines(game, linePosition);
        addHoverForNumbers(game, numberPosition);
        addCell(game, cellLayer1Position, 8);
        hideLines();
        addBars();
        addCell3(game, cellLayer1Position, 8);
        addWInAnim(game, cellCenterPosition, 8);
        bg2 = game.add.sprite(101,24, 'bg2');
        comboText();
        addWinLineScore();
        addWinLineScoreMove();
        sun = game.add.sprite(512,306, 'sun');
        sun.anchor.set(0.5, 0.5);
        sun.scale.setTo(0.1, 0.1);
        sun.alpha = 0.5;
        sun.visible = false;
        bigWinBG = game.add.sprite(512,316, 'bigWinBG');
        bigWinBG.anchor.set(0.5,0.5);
        bigWinBG.scale.setTo(0.1, 0.1);
        bigWinBG.alpha = 0;
        bigWinBG.visible = false;
        bigWinBG.tint = 0xfffaa1;
        bigText();
        buttonPanel = game.add.sprite(0,505, 'buttonPanel');
        score = game.add.sprite(0,738, 'score');
        // кнопки
        addButtonsGame1(game);
        

        bg_top = game.add.sprite(306,-13, 'bg_top');
        //счет
        var betScorePosion = [[103, 630, 21], [102, 692, 35], [262, 692, 35], [925, 630, 21], [792, 692, 35], [885, 638, 21]];
        addBetScore(game, betScorePosion, bet, betline, betLines, coins, coinsValue, win);

        var mainScorePosition = [[285, 745, 15], [518, 745, 15], [685, 745, 15]];
        addMainScore(game, mainScorePosition, cash, mainBet, mainWin);

        game.sound.mute = true;
    };

    game1.update = function () {
        if (spinStatus1){
            bars[0].tilePosition.y += 50;
        }
        if (spinStatus2){
            bars[1].tilePosition.y += 50;
        }
        if (spinStatus3){
            bars[2].tilePosition.y += 50;
        }
        if (spinStatus4){
            bars[3].tilePosition.y += 50;
        }
        if (spinStatus5){
            bars[4].tilePosition.y += 50;
        };
    };
    game.state.add('game1', game1);
};

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;

        /* bg */
        game.load.image('bg1', '../starburst/img/bg/4.jpg');
        game.load.image('bg2', '../starburst/img/bg/90.png');
        game.load.image('bg_top', '../starburst/img/bg/215.png');
        game.load.image('bg_fs', '../starburst/img/bg/1.png');

        /* Big WIn */ 
        game.load.image('bigWinBG', '../starburst/img/430.png');
        game.load.image('sun', '../starburst/img/437.png');

        /* panelGroup */
        game.load.image('score', '../starburst/img/bottom_line/661.png');
        game.load.image('buttonPanel', '../starburst/img/desktopButtons/91.png');

        /* buttonGroup */
        game.load.image('fullButton', '../starburst/img/bottom_line/680.png');
        game.load.image('nonFull', '../starburst/img/bottom_line/679.png');
        game.load.image('soundOn', '../starburst/img/bottom_line/3.png');
        game.load.image('soundOn_h', '../starburst/img/bottom_line/3_h.png');
        game.load.image('soundOff', '../starburst/img/bottom_line/2.png');
        game.load.image('soundOff_h', '../starburst/img/bottom_line/2_h.png');
        game.load.image('settings', '../starburst/img/bottom_line/1.png');
        game.load.image('settings_h', '../starburst/img/bottom_line/1_h.png');
        game.load.image('autoStart', '../starburst/img/bottom_line/4.png');
        game.load.image('autoStart_h', '../starburst/img/bottom_line/4_h.png');
        game.load.image('auto_play', '../starburst/img/desktopButtons/auto_play.png');
        game.load.image('auto_play_d', '../starburst/img/desktopButtons/auto_play_d.png');
        game.load.image('auto_play_h', '../starburst/img/desktopButtons/auto_play_h.png');
        game.load.image('auto_play_p', '../starburst/img/desktopButtons/auto_play_p.png');
        game.load.image('max_bet', '../starburst/img/desktopButtons/max_bet.png');
        game.load.image('max_bet_d', '../starburst/img/desktopButtons/max_bet_d.png');
        game.load.image('max_bet_h', '../starburst/img/desktopButtons/max_bet_h.png');
        game.load.image('max_bet_p', '../starburst/img/desktopButtons/max_bet_p.png');
        game.load.image('start', '../starburst/img/desktopButtons/start.png');
        game.load.image('start_d', '../starburst/img/desktopButtons/start_d.png');
        game.load.image('start_h', '../starburst/img/desktopButtons/start_h.png');
        game.load.image('start_p', '../starburst/img/desktopButtons/start_p.png');        
        game.load.image('right_btn', '../starburst/img/desktopButtons/right_btn.png');
        game.load.image('right_btn_d', '../starburst/img/desktopButtons/right_btn_d.png');
        game.load.image('right_btn_h', '../starburst/img/desktopButtons/right_btn_h.png');
        game.load.image('right_btn_p', '../starburst/img/desktopButtons/right_btn_p.png');
        game.load.image('left_btn', '../starburst/img/desktopButtons/left_btn.png');
        game.load.image('left_btn_d', '../starburst/img/desktopButtons/left_btn_d.png');
        game.load.image('left_btn_h', '../starburst/img/desktopButtons/left_btn_h.png');
        game.load.image('left_btn_p', '../starburst/img/desktopButtons/left_btn_p.png');
        game.load.image('paytable', '../starburst/img/desktopButtons/paytable.png');
        game.load.image('paytable_h', '../starburst/img/desktopButtons/paytable_h.png');
        game.load.image('paytable_p', '../starburst/img/desktopButtons/paytable_p.png');
        // game.load.image('paytable_mask', '../starburst/img/desktopButtons/paytable_mask.png');
        game.load.image('paytable_mask2', '../starburst/img/desktopButtons/paytable_mask2.png');
        game.load.image('green_line_1', '../starburst/img/desktopButtons/green_line_1.png');
        game.load.image('green_line_2', '../starburst/img/desktopButtons/green_line_2.png');
        game.load.image('black_line_1', '../starburst/img/desktopButtons/black_line_1.png');
        game.load.image('black_line_2', '../starburst/img/desktopButtons/black_line_2.png');
        game.load.image('green_line_1_p', '../starburst/img/desktopButtons/green_line_1_p.png');
        game.load.image('green_line_2_p', '../starburst/img/desktopButtons/green_line_2_p.png');
        game.load.image('green_line_1_h', '../starburst/img/desktopButtons/green_line_1_h.png');
        game.load.image('green_line_2_h', '../starburst/img/desktopButtons/green_line_2_h.png');
        game.load.image('green_line_1_d', '../starburst/img/desktopButtons/green_line_1_d.png');
        game.load.image('green_line_2_d', '../starburst/img/desktopButtons/green_line_2_d.png');

        /* Paytable */
        game.load.image('page_1', '../starburst/img/paytable/page_1.png');
        game.load.image('page_2', '../starburst/img/paytable/page_2.png');        
        game.load.image('paytable_right_btn', '../starburst/img/paytable/right_btn.png');
        game.load.image('paytable_right_btn_h', '../starburst/img/paytable/right_btn_h.png');
        game.load.image('paytable_right_btn_p', '../starburst/img/paytable/right_btn_p.png');
        game.load.image('paytable_center_btn', '../starburst/img/paytable/centr_btn.png');
        game.load.image('paytable_center_btn_h', '../starburst/img/paytable/centr_btn_h.png');
        game.load.image('paytable_center_btn_p', '../starburst/img/paytable/centr_btn_p.png');
        game.load.image('paytable_left_btn', '../starburst/img/paytable/left_btn.png');
        game.load.image('paytable_left_btn_h', '../starburst/img/paytable/left_btn_h.png');
        game.load.image('paytable_left_btn_p', '../starburst/img/paytable/left_btn_p.png');
        game.load.atlasJSONHash('paytableVideo', '../starburst/img/paytable/paytableVideo.png', '../starburst/img/paytable/paytableVideo.json');

        /* slotLayer1Group */
        game.load.image('cell0', '../starburst/img/slotValues/0.png');
        game.load.image('cell1', '../starburst/img/slotValues/1.png');
        game.load.image('cell2', '../starburst/img/slotValues/2.png');
        game.load.image('cell3', '../starburst/img/slotValues/3.png');
        game.load.image('cell4', '../starburst/img/slotValues/4.png');
        game.load.image('cell5', '../starburst/img/slotValues/5.png');
        game.load.image('cell6', '../starburst/img/slotValues/6.png');
        game.load.image('cell7', '../starburst/img/slotValues/7.png');
        game.load.image('cell0_f', '../starburst/img/slotValues/0_f.png');
        game.load.image('cell1_f', '../starburst/img/slotValues/1_f.png');
        game.load.image('cell2_f', '../starburst/img/slotValues/2_f.png');
        game.load.image('cell3_f', '../starburst/img/slotValues/3_f.png');
        game.load.image('cell4_f', '../starburst/img/slotValues/4_f.png');
        game.load.image('cell5_f', '../starburst/img/slotValues/5_f.png');
        game.load.image('cell6_f', '../starburst/img/slotValues/6_f.png');
        game.load.image('cell7_f', '../starburst/img/slotValues/7_f.png');
        game.load.image('slotBar', '../starburst/img/slotValues/slotColumn.png');

        /* slotLayer2Group */
        game.load.image('line1', '../starburst/img/lines/1.png');
        game.load.image('line2', '../starburst/img/lines/2.png');
        game.load.image('line3', '../starburst/img/lines/3.png');
        game.load.image('line4', '../starburst/img/lines/4.png');
        game.load.image('line5', '../starburst/img/lines/5.png');
        game.load.image('line6', '../starburst/img/lines/6.png');
        game.load.image('line7', '../starburst/img/lines/7.png');
        game.load.image('line8', '../starburst/img/lines/8.png');
        game.load.image('line9', '../starburst/img/lines/9.png');
        game.load.image('line10', '../starburst/img/lines/10.png');


        /* numberGroup */
        game.load.image('number1_h', '../starburst/img/lineNumbers/1_h.png');
        game.load.image('number2_h', '../starburst/img/lineNumbers/2_h.png');
        game.load.image('number3_h', '../starburst/img/lineNumbers/3_h.png');
        game.load.image('number4_h', '../starburst/img/lineNumbers/4_h.png');
        game.load.image('number5_h', '../starburst/img/lineNumbers/5_h.png');
        game.load.image('number6_h', '../starburst/img/lineNumbers/6_h.png');
        game.load.image('number7_h', '../starburst/img/lineNumbers/7_h.png');
        game.load.image('number8_h', '../starburst/img/lineNumbers/8_h.png');
        game.load.image('number9_h', '../starburst/img/lineNumbers/9_h.png');
        game.load.image('number10_h', '../starburst/img/lineNumbers/10_h.png');
        game.load.image('number1', '../starburst/img/lineNumbers/1.png');
        game.load.image('number2', '../starburst/img/lineNumbers/2.png');
        game.load.image('number3', '../starburst/img/lineNumbers/3.png');
        game.load.image('number4', '../starburst/img/lineNumbers/4.png');
        game.load.image('number5', '../starburst/img/lineNumbers/5.png');
        game.load.image('number6', '../starburst/img/lineNumbers/6.png');
        game.load.image('number7', '../starburst/img/lineNumbers/7.png');
        game.load.image('number8', '../starburst/img/lineNumbers/8.png');
        game.load.image('number9', '../starburst/img/lineNumbers/9.png');
        game.load.image('number10', '../starburst/img/lineNumbers/10.png');
        game.load.image('circle_h', '../starburst/img/lineNumbers/circle_h.png');
        game.load.image('circle', '../starburst/img/lineNumbers/circle.png');

        // animWin JSON
        game.load.atlasJSONHash('WinAnim0', '../starburst/img/WinAnim/WinAnim0.png', '../starburst/img/WinAnim/WinAnim0.json');
        game.load.atlasJSONHash('WinAnim1', '../starburst/img/WinAnim/WinAnim1.png', '../starburst/img/WinAnim/WinAnim1.json');
        game.load.atlasJSONHash('WinAnim2', '../starburst/img/WinAnim/WinAnim2.png', '../starburst/img/WinAnim/WinAnim2.json');
        game.load.atlasJSONHash('WinAnim3', '../starburst/img/WinAnim/WinAnim3.png', '../starburst/img/WinAnim/WinAnim3.json');
        game.load.atlasJSONHash('WinAnim4', '../starburst/img/WinAnim/WinAnim4.png', '../starburst/img/WinAnim/WinAnim4.json');
        game.load.atlasJSONHash('WinAnim5', '../starburst/img/WinAnim/WinAnim5.png', '../starburst/img/WinAnim/WinAnim5.json');
        game.load.atlasJSONHash('WinAnim6', '../starburst/img/WinAnim/WinAnim6.png', '../starburst/img/WinAnim/WinAnim6.json');
        game.load.atlasJSONHash('WinAnim7', '../starburst/img/WinAnim/WinAnim7.png', '../starburst/img/WinAnim/WinAnim7.json');
        game.load.atlasJSONHash('superWin', '../starburst/img/WinAnim/super.png', '../starburst/img/WinAnim/super.json');

        /* Soldier Anim*/ 
        // game.load.spritesheet('bigWin_gold_10', '../starburst/img/game1/bigWin_anim/7_3.png', 368, 768);

        /* Sound */ 
        game.load.audio('bgSound', '../starburst/sound/24_AmbienceBasic.mp3');
        game.load.audio('SpinButton', '../starburst/sound/20_SpinButtonSnd.mp3');
        game.load.audio('PaytableButton', '../starburst/sound/21_PaytableButtonSnd.mp3');
        game.load.audio('PanelButton', '../starburst/sound/23_DefaultKeypadButtonSnd.mp3');
        game.load.audio('ReelBounce', '../starburst/sound/13_ReelBounceSnd.mp3');
        game.load.audio('ReelSpin', '../starburst/sound/14_ReelSpinSnd.mp3');
        game.load.audio('winCountLoop', '../starburst/sound/16_WinCountUpLoopSnd.mp3');
        game.load.audio('winCountEnd', '../starburst/sound/15_WinCountUpEndSnd.mp3');
        game.load.audio('WinSnd1', '../starburst/sound/5_WinSnd1.mp3');
        game.load.audio('WinSnd2', '../starburst/sound/3_WinSnd2.mp3');
        game.load.audio('WinSnd3', '../starburst/sound/4_WinSnd3.mp3');
        game.load.audio('WinSymbol', '../starburst/sound/1_WinSymbolMedSnd.mp3');
        game.load.audio('BigWin_1', '../starburst/sound/17_BigWinCrescendoAndInitialSnd.mp3');
        game.load.audio('BigWin_2', '../starburst/sound/18_BigWinAmbienceSnd.mp3');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');
