var checkGame;
var lineCellValue = [];
var winSymbol = [];
function game1() {

    var game1 = {};

    game1.preload = function () {};

    // game1.soundStatus = false;
    game1.create = function () {
        game.physics.startSystem(Phaser.Physics.ARCADE);

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        // звуки
        bgSound = game.add.audio('bgSound');
        SpinButton = game.add.audio('SpinButton');
        PaytableButton = game.add.audio('PaytableButton');
        PanelButton = game.add.audio('PanelButton');
        ReelBounce = game.add.audio('ReelBounce');
        reelSpin = game.add.audio('ReelSpin');
        winCountLoop = game.add.audio('winCountLoop');
        winCountEnd = game.add.audio('winCountEnd');
        WinSnd1 = game.add.audio('WinSnd1');
        WinSnd2 = game.add.audio('WinSnd2');
        WinSnd3 = game.add.audio('WinSnd3');
        BigWin_1 = game.add.audio('BigWin_1');
        BigWin_2 = game.add.audio('BigWin_2');
        for (var i = 0; i < 15; i++) {
            winSymbol[i] = game.add.audio('WinSymbol');
        }
        winCountLoop.loop = true;
        reelSpin.loop = true;
        bgSound.loop = true;

        bgSound.play();
        // изображения

        bg1 = game.add.sprite(0,0, 'bg1');
        bg_fs = game.add.sprite(0,0, 'bg_fs');
        bg_fs.alpha = 0;

        //слоты
        cellLayer1Position = [
        [125, 82],
        [125, 232],
        [125, 382],
        [280, 82],
        [280, 232],
        [280, 382],
        [435, 82],
        [435, 232],
        [435, 382],
        [590, 82],
        [590, 232],
        [590, 382],
        [745, 82],
        [745, 232],
        [745, 382]
        ];
        cellCenterPosition = [
        [205, 162],
        [205, 312],
        [205, 462],
        [360, 162],
        [360, 312],
        [360, 462],
        [515, 162],
        [515, 312],
        [515, 462],
        [670, 162],
        [670, 312],
        [670, 462],
        [825, 162],
        [825, 312],
        [825, 462]
        ];
        
        // линии и номера
        var linePosition = [
        [204,314],
        [204,163],
        [204,466],
        [204,162],
        [204,158],
        [204,174],
        [204,302],
        [204,322],
        [204,173],
        [204,190]
        ];
        var numberPosition = [
        [[24,304], [28,133], [28,432], [32,93], [32,474], [26,177], [26,388], [24,346], [24,220],[22,262]],
        [[960,304], [951 ,133], [951,432], [947,93], [947,474], [955,177], [956,388], [960,346], [959,220], [960,262]]
        ];
        lineCellValue = [
        [1,4,7,10,13],
        [0,3,6,9,12],
        [2,5,8,11,14],
        [0,4,8,10,12],
        [2,4,6,10,14],
        [0,3,7,9,12],
        [2,5,7,11,14],
        [1,5,8,11,13],
        [1,3,6,9,13],
        [1,3,7,9,13],
        ];
        addCell2(game, cellLayer1Position, 8);
        addNumbers(game, numberPosition);
        addLines(game, linePosition);
        if (!isMobile){
            addHoverForNumbers(game, numberPosition);
        }
        addCell(game, cellLayer1Position, 8);
        hideLines();
        addBars();
        addCell3(game, cellLayer1Position, 8);
        addWInAnim(game, cellCenterPosition, 8);
        bg2 = game.add.sprite(101,24, 'bg2');
        comboText();
        addWinLineScore();
        addWinLineScoreMove();
        sun = game.add.sprite(512,306, 'sun');
        sun.anchor.set(0.5, 0.5);
        sun.scale.setTo(0.1, 0.1);
        sun.alpha = 0.5;
        sun.visible = false;
        bigWinBG = game.add.sprite(512,316, 'bigWinBG');
        bigWinBG.anchor.set(0.5,0.5);
        bigWinBG.scale.setTo(0.1, 0.1);
        bigWinBG.alpha = 0;
        bigWinBG.visible = false;
        bigWinBG.tint = 0xfffaa1;
        bigText();
        buttonPanel = game.add.sprite(0,505, 'buttonPanel');
        score = game.add.sprite(0,738, 'score');
        // кнопки
        addButtonsGame1(game);
        

        bg_top = game.add.sprite(306,-13, 'bg_top');
        //счет
        var betScorePosion = [[103, 630, 21], [102, 692, 35], [262, 692, 35], [925, 630, 21], [792, 692, 35], [885, 638, 21]];
        addBetScore(game, betScorePosion, bet, betline, betLines, coins, coinsValue, win);

        var mainScorePosition = [[285, 745, 15], [518, 745, 15], [685, 745, 15]];
        addMainScore(game, mainScorePosition, cash, mainBet, mainWin);

        game.sound.mute = true;
    };

    game1.update = function () {
        if (spinStatus1){
            bars[0].tilePosition.y += 50;
        }
        if (spinStatus2){
            bars[1].tilePosition.y += 50;
        }
        if (spinStatus3){
            bars[2].tilePosition.y += 50;
        }
        if (spinStatus4){
            bars[3].tilePosition.y += 50;
        }
        if (spinStatus5){
            bars[4].tilePosition.y += 50;
        };
    };
    game.state.add('game1', game1);
};
