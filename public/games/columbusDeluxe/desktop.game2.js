function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        // звуки
        sound16 = game.add.audio('sound16');
        sound16.loop = true;
        //sound16.play();


        //изображения
        var border = game.add.sprite(0,0, 'border');
        var backgroundGame2 = game.add.sprite(137,119, 'backgroundGame2');

        // кнопки
        addButtonsGame2TypeBORDelux(game);

        var status = game.add.sprite(150,830, 'status');
        var shadow_btn = game.add.sprite(150,925, 'shadow_btn');

        //счет
        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[391, 962, 28], [750, 962, 28], [180, 962, 28], [580, 962, 28], [580, 962, 28]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        addTitles();

        //подписи для gamble
        var gambleAmountText1 = game.add.text(325, 170, 'Gamble amount', {
            font: '34px "Press Start 2P"',
            fill: '#e8e4e4',
            stroke: '#000000',
            strokeThickness: 3,
            align: 'center'
        });
        var gambleAmountText2 = game.add.text(995, 170, 'Gamble to win', {
            font: '34px "Press Start 2P"',
            fill: '#e8e4e4',
            stroke: '#000000',
            strokeThickness: 3,
            align: 'center'
        });
        var gambleAmountText3 = game.add.text(425, 270+30, 'Previous cards', {
            font: '34px "Press Start 2P"',
            fill: '#e8e4e4',
            stroke: '#000000',
            strokeThickness: 3,
            align: 'center'
        });

        var gamblePosition = [[420,210],[1080,210]];
        totalWinR = 25;
        addGamble(totalWinR, gamblePosition);

        //карты и масти
        cardPositionTypeBOR = [[640,400], [[415,265], [670,265], [780,265], [890,265], [1000,265], [1110,265]]];
        addCardsTypeBOR(cardPositionTypeBOR);

        addMs();

        //анимации
        addCardAnim(game);

        full_and_sound();
    };

    game2.update = function () {

    };

    game.state.add('game2', game2);


}