var stepTotalWinR = 0;

function showWin(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y+100, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

    var timeInterval = 450;
    var textCounter = setInterval(function () {
        text.position.y -= 3;
    }, 10);

    setTimeout(function() {
        clearInterval(textCounter);
    }, timeInterval);

    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
//        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}




var ropeIndex, ropeDif;
var fisher, blockEvent = false, posX = 0, game3;
function createGame3() {
    game3={
        anime: {},
        create: function(){
            ropeIndex = 0;
            ropeDif = {
//                0: 'bear',
                2: 'fishSmall',
                5: 'fishBig',
                10: 'squid',
                15: 'mermaid',
                x: 'shield',
            };
            //------------------- shield - переход на супер игру !!!!!!!!
            checkGame = 3;
            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            checkWin = 0;
            
            game.add.sprite(94-mobileX,23-mobileY, 'game3.bg');
            game.add.sprite(139-mobileX,24-mobileY, 'game3.bg_top');
            game3.fisher = game.add.sprite(307-mobileX, 260-mobileY, 'game3.fisher');
            game3.fisherSound = game.add.audio('shoot');
            game3.sound = game.add.sound('bonusGame');
            game3.sound.loop = true;
            game3.sound.play();
            
            hole1 = game.add.sprite(100-mobileX, 447-mobileY, 'game3.hole');
            hole2 = game.add.sprite(217-mobileX, 447-mobileY, 'game3.hole');
            hole3 = game.add.sprite(334-mobileX, 447-mobileY, 'game3.hole');
            hole4 = game.add.sprite(451-mobileX, 447-mobileY, 'game3.hole');
            hole5 = game.add.sprite(568-mobileX, 447-mobileY, 'game3.hole');
            
            hole1.inputEnabled = true;
            hole1.input.useHandCursor = true;
            hole1.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(78 - mobileX);
            });
            
            hole2.inputEnabled = true;
            hole2.input.useHandCursor = true;
            hole2.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(195 - mobileX);
            });
            
            hole3.inputEnabled = true;
            hole3.input.useHandCursor = true;
            hole3.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(312 - mobileX);
            });
            
            hole4.inputEnabled = true;
            hole4.input.useHandCursor = true;
            hole4.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(429 - mobileX);
            });
            
            hole5.inputEnabled = true;
            hole5.input.useHandCursor = true;
            hole5.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(546 - mobileX);
            });
            
            //счет
//            scorePosions = [[265, 26, 20], [470, 26, 20], [640, 26, 20], [92, 56, 20], [695, 56, 20]];
            addScore(game, scorePosions, bet, '', balance, betline);
            
            function makeShot(x){
                blockEvent = true;
                game3.shot.position.x = x;
                game3.shot.visible = true;
                game3.fisher.visible = false;
                game3.shotAnimation.play();
                setTimeout(function(){
                    game3.fisherSound.play();
                }, 600);
            }
            function showAnimation(name, posX){
                var all = ['mermaid', 'fishSmall','fishBig', 'squid'], index = all.indexOf(name);
                var an = name + 'Animation', border, bAnime;
                game3.fisher.position.x = posX;
                game3.anime[name].visible = true;
                game3.anime[name].position.x = posX;
                game3.anime[an].play();

//                if(!isMobile) disablePanel(game3);
                if(index != -1){
                    switch(name){
                        case 'mermaid':
                            border = 'mermaidBorder';
                            bAnime = 'mermaidBorderAnimation';
                            break;
                        case 'fishSmall':
                            border = 'sFish_border';
                            bAnime = 'sFish_borderAnimation';
                            break;
                        case 'fishBig':
                            border = 'bFish_border';
                            bAnime = 'bFish_borderAnimation';
                            break;
                        case 'squid':
                            border = 'squidBorder';
                            bAnime = 'squidBorderAnimation';
                            break;
                    }
                }
//                if(!isMobile){
//                    unDisableNumberLine(game3);
//                    game3.startButton.loadTexture('start');
//                }
                if(index != -1){
                    game3[border].visible = true;
                    game3[bAnime].play();
                }
                game3.anime[name].sound.play();

            }
            function getAnime(){
                var str; 
                if(ropeValues[ropeIndex] in ropeDif) str = ropeDif[ropeValues[ropeIndex]];
                else str = 'shield';
                ropeIndex++;
                return str;
            }
            
            game3.anime.mermaid = game.add.sprite(200-mobileX ,265-mobileY,'mermaid');
            game3.anime.mermaid.sound = game.add.audio('mermaid');
            game3.anime.mermaid.visible = false;
            game3.anime.mermaidAnimation = game3.anime.mermaid.animations.add('mermaid',[0, 1, 2, 3, 4, 5, 4, 5, 4, 5],7,false);
            game3.anime.mermaidAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.mermaid.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.fishBig = game.add.sprite(200-mobileX,260-mobileY,'fish_big');
            game3.anime.fishBig.sound = game.add.audio('bFish');
            game3.anime.fishBig.visible = false;
            game3.anime.fishBigAnimation = game3.anime.fishBig.animations.add('fish_big',[0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2],7,false);
            game3.anime.fishBigAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.fishBig.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.fishSmall = game.add.sprite(200-mobileX,260-mobileY,'fish_small');
            game3.anime.fishSmall.sound = game.add.audio('sFish');
            game3.anime.fishSmall.visible = false;
            game3.anime.fishSmallAnimation = game3.anime.fishSmall.animations.add('fish_small', [0, 1, 2, 3, 2, 3, 2, 3, 2, 3],7,false);
            game3.anime.fishSmallAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.fishSmall.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.squid = game.add.sprite(200-mobileX,260-mobileY,'squid');
            game3.anime.squid.sound = game.add.audio('squid');
            game3.anime.squid.visible = false;
            game3.anime.squidAnimation = game3.anime.squid.animations.add('squid', [0, 1, 2, 3, 2, 3, 2, 3, 2, 3],7,false);
            game3.anime.squidAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.squid.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.bear = game.add.sprite(200-mobileX,260-mobileY,'bear');
            game3.anime.bear.sound = game.add.audio('bear');
            game3.anime.bear.visible = false;
            game3.anime.bearAnimation = game3.anime.bear.animations.add('bear', [0, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4],7,false);
            game3.anime.bearAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.bear.visible = false;
                    game3.fisher.visible = true;
                    game3.sound.stop();
                    game.state.start('game1');
                }, 300);
            });

            game3.anime.shield = game.add.sprite(200-mobileX,260-mobileY,'shield');
            game3.anime.shield.sound = game.add.audio('shield');
            game3.anime.shield.visible = false;
            game3.anime.shieldAnimation = game3.anime.shield.animations.add('shield', [0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2],7,false);
            game3.anime.shieldAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.shield.visible = false;
                    game3.fisher.visible = true;
                    game3.sound.stop();
                    game.state.start('game4');
                }, 300);
            });
            
            game3.dance = game.add.sprite(300,170-mobileY,'dance');
            game3.dance.visible = true;
            game3.danceAnimation = game3.dance.animations.add('dance', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66], 30, true);
            game3.danceAnimation.play();

            game3.mermaidBorder = game.add.sprite(658,164-mobileY, 'mermaidBorder');
            game3.mermaidBorder.visible = false;
            game3.mermaidBorderAnimation = game3.mermaidBorder.animations.add('mermaidBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.sFish_border = game.add.sprite(109-mobileX,97-mobileY, 'sFishBorder');
            game3.sFish_border.visible = false;
            game3.sFish_borderAnimation = game3.sFish_border.animations.add('sFishBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.bFish_border = game.add.sprite(109-mobileX,135-mobileY, 'bFishBorder');
            game3.bFish_border.visible = false;
            game3.bFish_borderAnimation = game3.bFish_border.animations.add('bFishBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.squidBorder = game.add.sprite(659-mobileX,86-mobileY, 'squidBorder');
            game3.squidBorder.visible = false;
            game3.squidBorderAnimation = game3.squidBorder.animations.add('squidBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.shot = game.add.sprite(307-mobileX, 260-mobileY, 'shot');
            game3.shotAnimation = game3.shot.animations.add('shot',[0,1,2,3,4,5,6,7,8,9,10], 5, false);
            game3.shotAnimation.onComplete.add(function(){
                game3.shot.visible = false;
                showAnimation(getAnime(), game3.shot.position.x);
            }); 
        }
    };
    
    game.state.add("game3", game3);
}