var stepTotalWinR = 0;

function showWin(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y+100, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

    var timeInterval = 450;
    var textCounter = setInterval(function () {
        text.position.y -= 3;
    }, 10);

    setTimeout(function() {
        clearInterval(textCounter);
    }, timeInterval);

    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
//        setTimeout("game.state.start('game1');", 1000);
}, timeInterval);
}




var ropeIndex, ropeDif;
var fisher, blockEvent = false, posX = 0, game3;
function game3() {
    var game3 = {
        anime: {},
    };        
    game3.create = function () {
        ropeIndex = 0;
        // anime: {}
        ropeDif = {
            2: 'fishSmall',
            5: 'fishBig',
            10: 'squid',
            15: 'mermaid',
            x: 'shield',
        };
            //------------------- shield - переход на супер игру !!!!!!!!
            checkGame = 3;
            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            checkWin = 0;
            
            game.add.sprite(0,0, 'game.background');
            game.add.sprite(95,23, 'game3.bg');
            game.add.sprite(139,24, 'game3.bg_top');
            game3.fisher = game.add.sprite(307, 260, 'game3.fisher');
            game3.fisherSound = game.add.audio('shoot');
            game3.sound = game.add.sound('bonusGame');
            game3.sound.loop = true;
            game3.sound.play();
            
            hole1 = game.add.sprite(100, 447, 'game3.hole');
            hole2 = game.add.sprite(217, 447, 'game3.hole');
            hole3 = game.add.sprite(334, 447, 'game3.hole');
            hole4 = game.add.sprite(451, 447, 'game3.hole');
            hole5 = game.add.sprite(568, 447, 'game3.hole');
            
            //счет
//            scorePosions = [[265, 26, 20], [470, 26, 20], [640, 26, 20], [92, 56, 20], [695, 56, 20]];
addScore(game, scorePosions, bet, '', balance, betline);

var selectGame = game.add.sprite(70,510, 'selectGame_d');
selectGame.scale.setTo(0.7, 0.7);
selectGame.inputEnabled = false;

var payTable = game.add.sprite(150,510, 'payTable_d');
payTable.scale.setTo(0.7, 0.7);
payTable.inputEnabled = false;

var betone = game.add.sprite(490,510, 'betOne_d');
betone.scale.setTo(0.7, 0.7);
betone.inputEnabled = false;


var betmax = game.add.sprite(535,510, 'betMax_d');
betmax.scale.setTo(0.7, 0.7);
betmax.inputEnabled = false;

var automaricstart = game.add.sprite(685,510, 'automaricStart_d');
automaricstart.scale.setTo(0.7, 0.7);
automaricstart.inputEnabled = false;

var startButton = game.add.sprite(597, 510, 'start_d');
startButton.scale.setTo(0.7,0.7);
startButton.inputEnabled = false;

var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
buttonLine1.scale.setTo(0.7,0.7);
buttonLine1.inputEnabled = true;
buttonLine1.input.useHandCursor = true;

var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
buttonLine3.scale.setTo(0.7,0.7);
buttonLine3.inputEnabled = true;
buttonLine3.input.useHandCursor = true;

var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
buttonLine5.scale.setTo(0.7,0.7);
buttonLine5.inputEnabled = true;
buttonLine5.input.useHandCursor = true;

var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
buttonLine7.scale.setTo(0.7,0.7);
buttonLine7.inputEnabled = true;
buttonLine7.input.useHandCursor = true;

var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
buttonLine9.scale.setTo(0.7,0.7);
buttonLine9.inputEnabled = true;
buttonLine9.input.useHandCursor = true;

buttonLine1.events.onInputOver.add(function(){
    if(blockEvent) return;
    buttonLine1.loadTexture('buttonLine1_p');
});
buttonLine1.events.onInputOut.add(function(){
    buttonLine1.loadTexture('buttonLine1');
});

buttonLine3.events.onInputOver.add(function(){
    if(blockEvent) return;
    buttonLine3.loadTexture('buttonLine3_p');
});
buttonLine3.events.onInputOut.add(function(){
    buttonLine3.loadTexture('buttonLine3');
});

buttonLine5.events.onInputOver.add(function(){
    if(blockEvent) return;
    buttonLine5.loadTexture('buttonLine5_p');
});
buttonLine5.events.onInputOut.add(function(){
    buttonLine5.loadTexture('buttonLine5');
});

buttonLine7.events.onInputOver.add(function(){
    if(blockEvent) return;
    buttonLine7.loadTexture('buttonLine7_p');
});
buttonLine7.events.onInputOut.add(function(){
    buttonLine7.loadTexture('buttonLine7');
});

buttonLine9.events.onInputOver.add(function(){
    if(blockEvent) return;
    buttonLine9.loadTexture('buttonLine9_p');
});
buttonLine9.events.onInputOut.add(function(){
    buttonLine9.loadTexture('buttonLine9');
});

buttonLine1.events.onInputDown.add(function(){
    console.log(blockEvent);
    if(blockEvent) return;
    makeShot(78);
});
buttonLine3.events.onInputDown.add(function(){
    if(blockEvent) return;
    makeShot(195);
});
buttonLine5.events.onInputDown.add(function(){
    if(blockEvent) return;
    makeShot(312);
});
buttonLine7.events.onInputDown.add(function(){
    if(blockEvent) return;
    makeShot(429);
});
buttonLine9.events.onInputDown.add(function(){
    if(blockEvent) return;
    makeShot(546);
});

function makeShot(x){
    blockEvent = true;
    game3.shot.position.x = x;
    game3.shot.visible = true;
    game3.fisher.visible = false;
    game3.shotAnimation.play();
    setTimeout(function(){
        game3.fisherSound.play();
    }, 600);
}
function showAnimation(name, posX){
    var all = ['mermaid', 'fishSmall','fishBig', 'squid'], index = all.indexOf(name);
    var an = name + 'Animation', border, bAnime;
    game3.fisher.position.x = posX;
    game3.anime[name].visible = true;
    game3.anime[name].position.x = posX;
    game3.anime[an].play();

//                if(!isMobile) disablePanel(game3);
if(index != -1){
    switch(name){
        case 'mermaid':
        border = 'mermaidBorder';
        bAnime = 'mermaidBorderAnimation';
        break;
        case 'fishSmall':
        border = 'sFish_border';
        bAnime = 'sFish_borderAnimation';
        break;
        case 'fishBig':
        border = 'bFish_border';
        bAnime = 'bFish_borderAnimation';
        break;
        case 'squid':
        border = 'squidBorder';
        bAnime = 'squidBorderAnimation';
        break;
    }
}
//                if(!isMobile){
//                    unDisableNumberLine(game3);
//                    game3.startButton.loadTexture('start');
//                }
if(index != -1){
    game3[border].visible = true;
    game3[bAnime].play();
}
game3.anime[name].sound.play();

}
function getAnime(){
    var str; 
    if(ropeValues[ropeIndex] in ropeDif) str = ropeDif[ropeValues[ropeIndex]];
    else str = 'shield';
    ropeIndex++;
    return str;
}

game3.anime.mermaid = game.add.sprite(200 ,265,'mermaid');
game3.anime.mermaid.sound = game.add.audio('mermaid');
game3.anime.mermaid.visible = false;
game3.anime.mermaidAnimation = game3.anime.mermaid.animations.add('mermaid',[0, 1, 2, 3, 4, 5, 4, 5, 4, 5],7,false);
game3.anime.mermaidAnimation.onComplete.add(function(){
    setTimeout(function(){
        blockEvent = false;
        game3.anime.mermaid.visible = false;
        game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
}, 300);
});

game3.anime.fishBig = game.add.sprite(200,260,'fish_big');
game3.anime.fishBig.sound = game.add.audio('bFish');
game3.anime.fishBig.visible = false;
game3.anime.fishBigAnimation = game3.anime.fishBig.animations.add('fish_big',[0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2],7,false);
game3.anime.fishBigAnimation.onComplete.add(function(){
    setTimeout(function(){
        blockEvent = false;
        game3.anime.fishBig.visible = false;
        game3.fisher.visible = true;
        updateBalanceGame3(game, scorePosions, balanceR);
    }, 300);
});

game3.anime.fishSmall = game.add.sprite(200,260,'fish_small');
game3.anime.fishSmall.sound = game.add.audio('sFish');
game3.anime.fishSmall.visible = false;
game3.anime.fishSmallAnimation = game3.anime.fishSmall.animations.add('fish_small', [0, 1, 2, 3, 2, 3, 2, 3, 2, 3],7,false);
game3.anime.fishSmallAnimation.onComplete.add(function(){
    setTimeout(function(){
        blockEvent = false;
        game3.anime.fishSmall.visible = false;
        game3.fisher.visible = true;
        updateBalanceGame3(game, scorePosions, balanceR);
    }, 300);
});

game3.anime.squid = game.add.sprite(200,260,'squid');
game3.anime.squid.sound = game.add.audio('squid');
game3.anime.squid.visible = false;
game3.anime.squidAnimation = game3.anime.squid.animations.add('squid', [0, 1, 2, 3, 2, 3, 2, 3, 2, 3],7,false);
game3.anime.squidAnimation.onComplete.add(function(){
    setTimeout(function(){
        blockEvent = false;
        game3.anime.squid.visible = false;
        game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
}, 300);
});

game3.anime.bear = game.add.sprite(200,260,'bear');
game3.anime.bear.sound = game.add.audio('bear');
game3.anime.bear.visible = false;
game3.anime.bearAnimation = game3.anime.bear.animations.add('bear', [0, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4],7,false);
game3.anime.bearAnimation.onComplete.add(function(){
    setTimeout(function(){
        blockEvent = false;
        game3.anime.bear.visible = false;
        game3.fisher.visible = true;
        game3.sound.stop();
        game.state.start('game1');
    }, 300);
});

game3.anime.shield = game.add.sprite(200,260,'shield');
game3.anime.shield.sound = game.add.audio('shield');
game3.anime.shield.visible = false;
game3.anime.shieldAnimation = game3.anime.shield.animations.add('shield', [0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2],7,false);
game3.anime.shieldAnimation.onComplete.add(function(){
    setTimeout(function(){
        blockEvent = false;
        game3.anime.shield.visible = false;
        game3.fisher.visible = true;
        game3.sound.stop();
        game.state.start('game4');
    }, 300);
});

game3.dance = game.add.sprite(300,170,'dance');
game3.dance.visible = true;
game3.danceAnimation = game3.dance.animations.add('dance', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66], 30, true);
game3.danceAnimation.play();

game3.mermaidBorder = game.add.sprite(658,164, 'mermaidBorder');
game3.mermaidBorder.visible = false;
game3.mermaidBorderAnimation = game3.mermaidBorder.animations.add('mermaidBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

game3.sFish_border = game.add.sprite(109,97, 'sFishBorder');
game3.sFish_border.visible = false;
game3.sFish_borderAnimation = game3.sFish_border.animations.add('sFishBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

game3.bFish_border = game.add.sprite(109,135, 'bFishBorder');
game3.bFish_border.visible = false;
game3.bFish_borderAnimation = game3.bFish_border.animations.add('bFishBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

game3.squidBorder = game.add.sprite(659,86, 'squidBorder');
game3.squidBorder.visible = false;
game3.squidBorderAnimation = game3.squidBorder.animations.add('squidBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

game3.shot = game.add.sprite(307, 260, 'shot');
game3.shotAnimation = game3.shot.animations.add('shot',[0,1,2,3,4,5,6,7,8,9,10], 5, false);
game3.shotAnimation.onComplete.add(function(){
    game3.shot.visible = false;
    showAnimation(getAnime(), game3.shot.position.x);
}); 
full_and_sound();
},
game3.update = function () {
    if (game.scale.isFullScreen)
    {
      full.loadTexture('game.full');
      fullStatus = true;
  }
  else
  {
   full.loadTexture('game.non_full');
   fullStatus = false;
}
};

game.state.add("game3", game3);
}