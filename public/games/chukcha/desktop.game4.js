function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        game.state.start('game1');
    }, timeInterval);
}

function winResultGame4(game, x,y) {
    monkeyHangingHide();
    mankeyLeftPoint.visible = true;
    setTimeout("bitMonkey.play();", 500);
    take_or_risk_anim.visible = false;
    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        winPoint1.play();
        goldPoint = game.add.sprite(207,263, 'game.goldPoint');
        goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
        goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
            mankeyLeftPoint.visible = false;
            winPoint2.play();
            mankeyWinPoint.visible = true;
            mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
                updateBalanceGame4(game, scorePosions, balanceR);
                game.state.start('game1');
            });
        });
    });
}

function loseResultGame4(game, x,y) {
    setTimeout("bitMonkey.play();", 500);
    monkeyHangingHide();
    take_or_risk_anim.visible = false;
    mankeyLeftPoint.visible = true;
    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        //winPoint1.play();
        losePoint.play();
        spiderPoint = game.add.sprite(207,263, 'game.spiderPoint');
        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
        spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
            mankeyLeftPoint.visible = false;

            mankeyLosePoint.visible = true;
            mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
                updateBalanceGame4(game, scorePosions, balanceR);
                game.state.start('game1');
            });
        });

    });
}

function getResultGame4() {
    console.log(ropeValues);
    if(ropeValues[5] != 0){
//        lockDisplay();
        return true;// - win
    } else {
//        lockDisplay();
        return false;// - lose
    }
}

var shaman;
function game4() {
    (function () {
        var button;

        var game4 = {};

        game4.create = function () {
            checkGame = 4;
            blockEvent = false;

            game.add.sprite(0,0, 'game.background');
            game.add.sprite(95,23, 'game4.bg');
            game.add.sprite(139,24, 'game3.bg_top');
            shaman = game.add.sprite(320, 320, 'game4.shaman');

            game4.green_eyes = game.add.sprite(228, 155, 'game4.green_eyes');
            game4.green_eyes.visible = false;
            game4.text = game.add.sprite(80, 455, 'game4.your_choice');
            game4.text.scale.setTo(0.8,0.8);
            game4.sound = game.add.audio('superbonus');
            game4.sound.loop = true;

            addScore(game, scorePosions, bet, '', balance, betline);

            selectGame = game.add.sprite(70,510, 'selectGame_d');
            selectGame.scale.setTo(0.7, 0.7);
            selectGame.inputEnabled = false;

            payTable = game.add.sprite(150,510, 'payTable_d');
            payTable.scale.setTo(0.7, 0.7);
            payTable.inputEnabled = false;

            betone = game.add.sprite(490,510, 'betOne_d');
            betone.scale.setTo(0.7, 0.7);
            betone.inputEnabled = false;


            betmax = game.add.sprite(535,510, 'betMax_d');
            betmax.scale.setTo(0.7, 0.7);
            betmax.inputEnabled = false;

            automaricstart = game.add.sprite(685,510, 'automaricStart_d');
            automaricstart.scale.setTo(0.7, 0.7);
            automaricstart.inputEnabled = false;

            startButton = game.add.sprite(597, 510, 'start_d');
            startButton.scale.setTo(0.7,0.7);
            startButton.inputEnabled = false;

            buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
            buttonLine1.scale.setTo(0.7,0.7);
            buttonLine1.inputEnabled = false;

            buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
            buttonLine5.scale.setTo(0.7,0.7);
            buttonLine5.inputEnabled = false;

            buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
            buttonLine9.scale.setTo(0.7,0.7);
            buttonLine9.inputEnabled = false;

            buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
            buttonLine3.scale.setTo(0.7,0.7);
            buttonLine3.inputEnabled = true;
            buttonLine3.input.useHandCursor = true;

            buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
            buttonLine7.scale.setTo(0.7,0.7);
            buttonLine7.inputEnabled = true;
            buttonLine7.input.useHandCursor = true;

            buttonLine7.events.onInputOver.add(function(){
                if(blockEvent) return;
                buttonLine7.loadTexture('buttonLine7_p');
            });
            buttonLine7.events.onInputOut.add(function(){
                if(blockEvent) return;
                buttonLine7.loadTexture('buttonLine7');
            });
            buttonLine3.events.onInputOver.add(function(){
                if(blockEvent) return;
                buttonLine3.loadTexture('buttonLine3_p');
            });
            buttonLine3.events.onInputOut.add(function(){
                if(blockEvent) return;
                buttonLine3.loadTexture('buttonLine3');
            });

            buttonLine3.events.onInputDown.add(function(){
                if(blockEvent) return;
                buttonLine3.loadTexture('buttonLine3_d');
                posX = 170;
                makeDance();

            });
            buttonLine7.events.onInputDown.add(function(){
                if(blockEvent) return;
                buttonLine7.loadTexture('buttonLine7_d');
                posX = 450;
                makeDance();

            });
            game4.lTotem = game.add.sprite(219,120, 'game4.totem');
            game4.lTotem.inputEnabled = true;
            game4.lTotem.input.useHandCursor = true;
            game4.lTotem.events.onInputUp.add(function(){
                if(blockEvent) return;
                posX = 170;
                makeDance();
            });

            game4.rTotem = game.add.sprite(507,120, 'game4.totem');
            game4.rTotem.inputEnabled = true;
            game4.rTotem.input.useHandCursor = true;
            game4.rTotem.events.onInputUp.add(function(){
                if(blockEvent) return;
                posX = 450;
                makeDance();
            });
            function makeDance(){

                blockEvent = true;
                game4.text.visible = false;
                game4.choice.visible = true;
                shaman.visible = false;
                game4.choice.position.x = posX;
                game4.choice.sound.play();
                game4.choiceAnimation.play();
            }
            function makeWin(){
                game4.gold_rain.visible = true;
                game4.gold_rain.position.x = game4.choice.position.x + 47;
                game4.gold_rain.sound.play();
                game4.gold_rainAnimation.play();
            }
            function makeLose(){
                game4.choice.visible = false;
                game4.pain.position.x = game4.choice.position.x;
                game4.pain.visible = true;
                game4.pain.sound.play();
                game4.painAnimation.play();

                game4.lightning.position.x = game4.choice.position.x - 50;
                game4.lightning.visible = true;
                game4.lightningAnimation.play();
            }

            function openPanel(){
                game4.green_eyes.visible = false;
                game4.choice.visible = false;
                shaman.visible = true;
                game4.text.visible = true;
                game4.pain.visible = false;
                blockEvent = false;

            }
            game4.choice = game.add.sprite(300, 300, 'choice');
            game4.choice.visible = false;
            game4.choice.sound = game.add.audio('choice');
            game4.choiceAnimation = game4.choice.animations.add('choice', [0,1,2,3,4,5,6,7,8,7,8,6,5,4,5,6,7,8,9,7,8,6,7,8,9,9,10], 8 , false);
            game4.choiceAnimation.onComplete.add(function(){
                if(getResultGame4()) makeWin();
                else makeLose();
                game4.green_eyes.visible = true;
            });

            game4.pain = game.add.sprite(100, 300, 'pain');
            game4.pain.visible = false;
            game4.pain.sound = game.add.audio('pain');
            game4.painAnimation = game4.pain.animations.add('pain', [0,1,2,3,4,5,6,7,8,9,10,11], 5 , false);
            game4.painAnimation.onComplete.add(function(){
                setTimeout(function(){
                    game.state.start('game1');
                }, 2000);
            });

            game4.lightning = game.add.sprite(400, 30, 'lightning');
            game4.lightning.scale.setTo(0.9,0.9);
            game4.lightning.visible = false;
            game4.lightning.sound = game.add.audio('lightning');
            game4.lightningAnimation = game4.lightning.animations.add('lightning', [0,1,2,3], 5 , false);
            game4.lightningAnimation.onComplete.add(function(){
                game4.lightning.visible = false;
            });

            game4.gold_rain = game.add.sprite(100, 100, 'gold_rain');
            game4.gold_rain.scale.setTo(0.9,0.9);
            game4.gold_rain.visible = false;
            game4.gold_rain.sound = game.add.audio('gold_rain');
            game4.gold_rainAnimation = game4.gold_rain.animations.add('gold_rain', [0,1,2,3], 5 , false);
            game4.gold_rainAnimation.onComplete.add(function(){
                game4.gold_rain.visible = false;
                setTimeout(function(){
                    game.state.start('game1');
                }, 2000);
            });
            full_and_sound();
        };
        game4.update = function () {
            if (game.scale.isFullScreen) {
              full.loadTexture('game.full');
              fullStatus = true;
          } else {
             full.loadTexture('game.non_full');
             fullStatus = false;
         }
     };
     game.state.add('game4', game4);
 })();
};
