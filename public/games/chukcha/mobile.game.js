//'use strict';
//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    var full, sound;
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

function makeStaticAnimations(){
    game.fire = game.add.sprite(400-mobileX,455-mobileY, 'fire');
    game.fireAnimation = game.fire.animations.add('fire',[0,1,2,3,4],10,true);
    game.fire.visible = true;
    game.smoke = game.add.sprite(348-mobileX,430-mobileY,'smoke');
    game.smokeAnimation = game.smoke.animations.add('smoke', [0, 0, 0, 0, 0, 1, 1, 1, 2, 3, 4], 5, true);
    game.smoke.visible = true;
    game.smokeAnimation.play();
    game.fireAnimation.play();
}
//Функции связанные с линиями и их номерами

var line1; var linefull1; var number1;
var line2; var linefull2; var number2;
var line3; var linefull3; var number3;
var line4; var linefull4; var number4;
var line5; var linefull5; var number5;
var line6; var linefull6; var number6;
var line7; var linefull7; var number7;
var line8; var linefull8; var number8;
var line9; var linefull9; var number9;

//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9'];
    var lineNames = ['line1', 'line2', 'line3', 'line4', 'line5', 'line6', 'line7', 'line8', 'line9'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);

    linefull1.visible = false;
    linefull2.visible = false;
    linefull3.visible = false;
    linefull4.visible = false;
    linefull5.visible = false;
    linefull6.visible = false;
    linefull7.visible = false;
    linefull8.visible = false;
    linefull9.visible = false;

    line1 = game.add.sprite(linePosition[0][0], linePosition[0][1], lineNames[0]);
    line2 = game.add.sprite(linePosition[1][0], linePosition[1][1], lineNames[1]);
    line3 = game.add.sprite(linePosition[2][0], linePosition[2][1], lineNames[2]);
    line4 = game.add.sprite(linePosition[3][0], linePosition[3][1], lineNames[3]);
    line5 = game.add.sprite(linePosition[4][0], linePosition[4][1], lineNames[4]);
    line6 = game.add.sprite(linePosition[5][0], linePosition[5][1], lineNames[5]);
    line7 = game.add.sprite(linePosition[6][0], linePosition[6][1], lineNames[6]);
    line8 = game.add.sprite(linePosition[7][0], linePosition[7][1], lineNames[7]);
    line9 = game.add.sprite(linePosition[8][0], linePosition[8][1], lineNames[8]);

    line1.visible = false;
    line2.visible = false;
    line3.visible = false;
    line4.visible = false;
    line5.visible = false;
    line6.visible = false;
    line7.visible = false;
    line8.visible = false;
    line9.visible = false;

    number1 = game.add.sprite(numberPosition[0][0], numberPosition[0][1], 'num_d_1');
    number2 = game.add.sprite(numberPosition[1][0], numberPosition[1][1], 'num_d_2');
    number3 = game.add.sprite(numberPosition[2][0], numberPosition[2][1], 'num_d_3');
    number4 = game.add.sprite(numberPosition[3][0], numberPosition[3][1], 'num_d_4');
    number5 = game.add.sprite(numberPosition[4][0], numberPosition[4][1], 'num_d_5');
    number6 = game.add.sprite(numberPosition[5][0], numberPosition[5][1], 'num_d_6');
    number7 = game.add.sprite(numberPosition[6][0], numberPosition[6][1], 'num_d_7');
    number8 = game.add.sprite(numberPosition[7][0], numberPosition[7][1], 'num_d_8');
    number9 = game.add.sprite(numberPosition[8][0], numberPosition[8][1], 'num_d_9');
}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
function hideLines(linesArray) {
    if(!linesArray){
        linesArray = [];
    }
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        line1.visible = false;
        line2.visible = false;
        line3.visible = false;
        line4.visible = false;
        line5.visible = false;
        line6.visible = false;
        line7.visible = false;
        line8.visible = false;
        line9.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                    linefull1.visible = false;
                    break;
                case 2:
                    linefull2.visible = false;
                    break;
                case 3:
                    linefull3.visible = false;
                    break;
                case 4:
                    linefull4.visible = false;
                    break;
                case 5:
                    linefull5.visible = false;
                    break;
                case 6:
                    linefull6.visible = false;
                    break;
                case 7:
                    linefull7.visible = false;
                    break;
                case 8:
                    linefull8.visible = false;
                    break;
                case 9:
                    linefull9.visible = false;
                    break;
                case 11:
                    line1.visible = false;
                    break;
                case 12:
                    line2.visible = false;
                    break;
                case 13:
                    line3.visible = false;
                    break;
                case 14:
                    line4.visible = false;
                    break;
                case 15:
                    line5.visible = false;
                    break;
                case 16:
                    line6.visible = false;
                    break;
                case 17:
                    line7.visible = false;
                    break;
                case 18:
                    line8.visible = false;
                    break;
                case 19:
                    line9.visible = false;
                    break;
            }
        });
    }
}

var linesArray = [];
function showLines(linesArray) {
    if(!linesArray){
        linesArray = [];
    }
    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        line1.visible = true;
        line2.visible = true;
        line3.visible = true;
        line4.visible = true;
        line5.visible = true;
        line6.visible = true;
        line7.visible = true;
        line8.visible = true;
        line9.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                    linefull1.visible = true;
                    break;
                case 2:
                    linefull2.visible = true;
                    break;
                case 3:
                    linefull3.visible = true;
                    break;
                case 4:
                    linefull4.visible = true;
                    break;
                case 5:
                    linefull5.visible = true;
                    break;
                case 6:
                    linefull6.visible = true;
                    break;
                case 7:
                    linefull7.visible = true;
                    break;
                case 8:
                    linefull8.visible = true;
                    break;
                case 9:
                    linefull9.visible = true;
                    break;
                case 11:
                    line1.visible = true;
                    break;
                case 12:
                    line2.visible = true;
                    break;
                case 13:
                    line3.visible = true;
                    break;
                case 14:
                    line4.visible = true;
                    break;
                case 15:
                    line5.visible = true;
                    break;
                case 16:
                    line6.visible = true;
                    break;
                case 17:
                    line7.visible = true;
                    break;
                case 18:
                    line8.visible = true;
                    break;
                case 19:
                    line9.visible = true;
                    break;
            }
        });
    }
}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(!numberArray){
        numberArray = [];
    }
    if(numberArray.length == 0) {
        //--------------------------------------------------------------treason
        number1.loadTexture('num_d_1');
        number2.loadTexture('num_d_2');
        number3.loadTexture('num_d_3');
        number4.loadTexture('num_d_4');
        number5.loadTexture('num_d_5');
        number6.loadTexture('num_d_6');
        number7.loadTexture('num_d_7');
        number8.loadTexture('num_d_8');
        number9.loadTexture('num_d_9');
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                    number1.loadTexture('num_d_1');
                    break;
                case 2:
                    number2.loadTexture('num_d_2');
                    break;
                case 3:
                    number3.loadTexture('num_d_3');
                    break;
                case 4:
                    number4.loadTexture('num_d_4');
                    break;
                case 5:
                    number5.loadTexture('num_d_5');
                    break;
                case 6:
                    number6.loadTexture('num_d_6');
                    break;
                case 7:
                    number7.loadTexture('num_d_7');
                    break;
                case 8:
                    number8.loadTexture('num_d_8');
                    break;
                case 9:
                    number9.loadTexture('num_d_9');
                    break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(!numberArray){
        numberArray = [];
    }
    //--------------------------------------------------------------treason
    if(numberArray.length == 0) {
        number1.loadTexture('num_p_1');
        number2.loadTexture('num_p_2');
        number3.loadTexture('num_p_3');
        number4.loadTexture('num_p_4');
        number5.loadTexture('num_p_5');
        number6.loadTexture('num_p_6');
        number7.loadTexture('num_p_7');
        number8.loadTexture('num_p_8');
        number9.loadTexture('num_p_9');
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                    number1.loadTexture('num_p_1');
                    break;
                case 2:
                    number2.loadTexture('num_p_2');
                    break;
                case 3:
                    number3.loadTexture('num_p_3');
                    break;
                case 4:
                    number4.loadTexture('num_p_4');
                    break;
                case 5:
                    number5.loadTexture('num_p_5');
                    break;
                case 6:
                    number6.loadTexture('num_p_6');
                    break;
                case 7:
                    number7.loadTexture('num_p_7');
                    break;
                case 8:
                    number8.loadTexture('num_p_8');
                    break;
                case 9:
                    number9.loadTexture('num_p_9');
                    break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    if(!numberArray){
        numberArray = [];
    }
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}



// Функции связанные с кнопками
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;

//кнопки для слотов
function addButtonsGame1(game) {

    //звуки для кнопок
    var line1Sound = game.add.audio('line1Sound');
    var line3Sound = game.add.audio('line3Sound');
    var line5Sound = game.add.audio('line5Sound');
    var line7Sound = game.add.audio('line7Sound');
    var line9Sound = game.add.audio('line9Sound');

    //var soundForBattons = []; - массив содержащий объекты звуков для кнопок
    var soundForBattons = [line1Sound, line3Sound, line5Sound, line7Sound, line9Sound];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150,510, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490,510, 'betOne');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        betone.loadTexture('betOne_p');
    });
    betone.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            upBetline(betlineOptions);
            updateBetinfo(game, scorePosions, lines, betline);
        }
    });
    betone.events.onInputOut.add(function(){
        betone.loadTexture('betOne');
    });


    betmax = game.add.sprite(535,510, 'betMax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        betmax.loadTexture('betMax_p');
    });
    betmax.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            maxBetline();
            updateBetinfo(game, scorePosions, lines, betline);
            //betMaxSound.play();
        }
    });
    betmax.events.onInputOut.add(function(){
        betmax.loadTexture('betMax');
    });

    automaricstart = game.add.sprite(685,510, 'automaricStart');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = true;
    automaricstart.input.useHandCursor = true;
    automaricstart.events.onInputOver.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricStart_p');
        }
    });
    automaricstart.events.onInputOut.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricStart');
        }
    });
    automaricstart.events.onInputDown.add(function(){
        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;
                    hideLines();
                    requestSpin(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                autostart = true;
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    startButton = game.add.sprite(597, 510, 'start');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('start_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('start');
        }
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {
                hideLines();
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons();
            takeWin.stop();
            changeTableTitle('playTo');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }

    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;
    buttonLine1.events.onInputOver.add(function(){
        buttonLine1.loadTexture('buttonLine1_p');
    });
    buttonLine1.events.onInputOut.add(function(){
        buttonLine1.loadTexture('buttonLine1');
    });
    buttonLine1.events.onInputUp.add(function(){
        hideLines();
        showLines([1]);
    });
    buttonLine1.events.onInputDown.add(function(){
        soundForBattons[0].play();
        hideLines();
        linesArray = [11];
        showLines(linesArray);

        hideNumbers();
        numberArray = [1];
        showNumbers(numberArray);

        lines = 1;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputUp.add(function(){
        hideLines();
        showLines([1, 2, 3]);
    });
    buttonLine3.events.onInputDown.add(function(){
        soundForBattons[1].play();

        hideLines();
        linesArray = [11, 12, 13];
        showLines(linesArray);

        hideNumbers();
        numberArray = [1, 2, 3];
        showNumbers(numberArray);

        lines = 3;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputUp.add(function(){
        hideLines();
        showLines([1, 2, 3, 4, 5]);
    });
    buttonLine5.events.onInputDown.add(function(){
        soundForBattons[2].play();

        hideLines();
        linesArray = [11, 12, 13, 14, 15];
        showLines(linesArray);

        hideNumbers();
        numberArray = [1, 2, 3, 4, 5];
        showNumbers(numberArray);

        lines = 5;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputUp.add(function(){
        hideLines();
        showLines([1, 2, 3, 4, 5, 6, 7]);
    });
    buttonLine7.events.onInputDown.add(function(){
        soundForBattons[3].play();

        hideLines();
        linesArray = [11, 12, 13, 14, 15, 16, 17];
        showLines(linesArray);

        hideNumbers();
        numberArray = [1, 2, 3, 4, 5, 6, 7];
        showNumbers(numberArray);

        lines = 7;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputUp.add(function(){
        hideLines();
        showLines([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });
    buttonLine9.events.onInputDown.add(function(){
        soundForBattons[4].play();

        hideLines();
        linesArray = [11, 12, 13, 14, 15, 16, 17, 18, 19];
        showLines(linesArray);

        hideNumbers();
        numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        showNumbers(numberArray);

        lines = 9;
        updateBetinfo(game, scorePosions, lines, betline);
    });

}

//кнопки для карт
function addButtonsGame2(game) {

    var soundForBattons = [];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betOne_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betMax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricStart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'start');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        startButton.loadTexture('start_p');
    });
    startButton.events.onInputOut.add(function(){
        startButton.loadTexture('start');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputDown.add(function(){
        requestDouble(gamename, 1, lines, bet, sid);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputDown.add(function(){
        requestDouble(gamename, 2, lines, bet, sid);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputDown.add(function(){
        requestDouble(gamename, 3, lines, bet, sid);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputDown.add(function(){
        requestDouble(gamename, 4, lines, bet, sid);
    });

}

//кнопки для игры с последовательным выбором (действия при нажатии некоторых кнопок нужно задать в самой игре)
/*function addButtonsGame3(game) {
    var selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    var payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    var betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    var betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    var startButton = game.add.sprite(597, 510, 'start_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;

    var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;

    var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;

    buttonLine1.events.onInputOver.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1_p');
        }
    });
    buttonLine1.events.onInputOut.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1');
        }
    });

    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine5.events.onInputOver.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5_p');
        }
    });
    buttonLine5.events.onInputOut.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5');
        }
    });

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });

    buttonLine9.events.onInputOver.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9_p');
        }
    });
    buttonLine9.events.onInputOut.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9');
        }
    });

    buttonLine1.events.onInputDown.add(function(){
        takeBananaRope(180,22,1);

        buttonLine1.loadTexture('buttonLine1_d');
        buttonLine1.inputEnabled = false;
        buttonLine1.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) {
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        setTimeout(showWinGame3,2500, 180+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
    });
    buttonLine3.events.onInputDown.add(function(){
        takeBananaRope(275,22,2);

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        setTimeout(showWinGame3,2500, 275+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
    });
    buttonLine5.events.onInputDown.add(function(){
        takeBananaRope(365,22,3);

        buttonLine5.loadTexture('buttonLine5_d');
        buttonLine5.inputEnabled = false;
        buttonLine5.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        setTimeout(showWinGame3,2500, 365+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
    });
    buttonLine7.events.onInputDown.add(function(){
        takeBananaRope(455,22,4);

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        setTimeout(showWinGame3,2500, 455+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
    });
    buttonLine9.events.onInputDown.add(function(){
        takeBananaRope(540,22,5);

        buttonLine9.loadTexture('buttonLine9_d');
        buttonLine9.inputEnabled = false;
        buttonLine9.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        setTimeout(showWinGame3,2500, 540+85,22+200 , ropeValues[ropeStep], stepTotalWinR);
    });
}*/

// для выбора одного из двух (действия при нажатии некоторых кнопок нужно задать в самой игре)
function addButtonsGame4(game) {
    var selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    var payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    var betone = game.add.sprite(490,510, 'betOne_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    var betmax = game.add.sprite(535,510, 'betMax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    var automaricstart = game.add.sprite(685,510, 'automaricStart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    var startButton = game.add.sprite(597, 510, 'start_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = false;

    var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = false;
}

//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
function hideButtons(buttonsArray) {
    if(!buttonsArray){
        buttonsArray = [];
    }
    if(!isMobile){
        if(buttonsArray.length == 0) {
            if(autostart == false){
                automaricstart.loadTexture('automaricStart_d');
                automaricstart.inputEnabled = false;
                automaricstart.input.useHandCursor = false;
            }

            selectGame.loadTexture('selectGame_d');
            selectGame.inputEnabled = false;
            selectGame.input.useHandCursor = false;

            payTable.loadTexture('payTable_d');
            payTable.inputEnabled = false;
            payTable.input.useHandCursor = false;

            betone.loadTexture('betOne_d');
            betone.inputEnabled = false;
            betone.input.useHandCursor = false;

            betmax.loadTexture('betMax_d');
            betmax.inputEnabled = false;
            betmax.input.useHandCursor = false;

            startButton.loadTexture('start_d');
            startButton.inputEnabled = false;
            startButton.input.useHandCursor = false;

            buttonLine1.loadTexture('buttonLine1_d');
            buttonLine1.inputEnabled = false;
            buttonLine1.input.useHandCursor = false;

            buttonLine3.loadTexture('buttonLine3_d');
            buttonLine3.inputEnabled = false;
            buttonLine3.input.useHandCursor = false;

            buttonLine5.loadTexture('buttonLine5_d');
            buttonLine5.inputEnabled = false;
            buttonLine5.input.useHandCursor = false;

            buttonLine7.loadTexture('buttonLine7_d');
            buttonLine7.inputEnabled = false;
            buttonLine7.input.useHandCursor = false;

            buttonLine9.loadTexture('buttonLine9_d');
            buttonLine9.inputEnabled = false;
            buttonLine9.input.useHandCursor = false;

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]+'_d');
                item[0].inputEnabled = false;
                item[0].input.useHandCursor = false;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = false;
            double.visible = false;
            bet1.visible = false;
            dollar.visible = false;
            gear.visible = false;
            home.visible = false;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = false;
            })
        }
    }
}

function showButtons(buttonsArray) {
    if(!buttonsArray){
        buttonsArray = [];
    }
    if(!isMobile) {
        if(buttonsArray.length == 0) {

            if(autostart == false){
                automaricstart.loadTexture('automaricStart');
                automaricstart.inputEnabled = true;
                automaricstart.input.useHandCursor = true;

                selectGame.loadTexture('selectGame');
                selectGame.inputEnabled = true;
                selectGame.input.useHandCursor = true;

                payTable.loadTexture('payTable');
                payTable.inputEnabled = true;
                payTable.input.useHandCursor = true;

                betone.loadTexture('betOne');
                betone.inputEnabled = true;
                betone.input.useHandCursor = true;

                betmax.loadTexture('betMax');
                betmax.inputEnabled = true;
                betmax.input.useHandCursor = true;

                startButton.loadTexture('start');
                startButton.inputEnabled = true;
                startButton.input.useHandCursor = true;

                buttonLine1.loadTexture('buttonLine1');
                buttonLine1.inputEnabled = true;
                buttonLine1.input.useHandCursor = true;

                buttonLine3.loadTexture('buttonLine3');
                buttonLine3.inputEnabled = true;
                buttonLine3.input.useHandCursor = true;

                buttonLine5.loadTexture('buttonLine5');
                buttonLine5.inputEnabled = true;
                buttonLine5.input.useHandCursor = true;

                buttonLine7.loadTexture('buttonLine7');
                buttonLine7.inputEnabled = true;
                buttonLine7.input.useHandCursor = true;

                buttonLine9.loadTexture('buttonLine9');
                buttonLine9.inputEnabled = true;
                buttonLine9.input.useHandCursor = true;
            }

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]);
                item[0].inputEnabled = true;
                item[0].input.useHandCursor = true;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = true;
            double.visible = true;
            bet1.visible = true;
            dollar.visible = true;
            gear.visible = true;
            home.visible = true;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = true;
            })
        }
    }
}


//слоты и их вращение

//переменные содержащие все объекты слотов
var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
var slot1Anim; var slot2Anim; var slot3Anim;  var slot4Anim; var slot5Anim; var slot6Anim; var slot7Anim; var slot8Anim; var slot9Anim; var slot10Anim; var slot11Anim; var slot12Anim; var slot13Anim; var slot14Anim; var slot15Anim;

function addSlots(game, slotPosition) {
    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
    var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];
    var slotCellAnimName = 'cellAnim';

    slot1 = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[0]);
    slot2 = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[1]);
    slot3 = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[3]);
    slot4 = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[4]);
    slot5 = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[7]);
    slot6 = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[2]);
    slot7 = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[4]);
    slot8 = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[3]);
    slot9 = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[8]);
    slot10 = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[2]);
    slot11 = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[0]);
    slot12 = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[1]);
    slot13 = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[5]);
    slot14 = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[3]);
    slot15 = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[5]);

    slot1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotCellAnimName);
    slot2Anim = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotCellAnimName);
    slot3Anim = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotCellAnimName);
    slot4Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotCellAnimName);
    slot5Anim = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotCellAnimName);
    slot6Anim = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotCellAnimName);
    slot7Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotCellAnimName);
    slot8Anim = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotCellAnimName);
    slot9Anim = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotCellAnimName);
    slot10Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotCellAnimName);
    slot11Anim = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotCellAnimName);
    slot12Anim = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotCellAnimName);
    slot13Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotCellAnimName);
    slot14Anim = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotCellAnimName);
    slot15Anim = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotCellAnimName);

    slot1Anim.animations.add('slot1Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot2Anim.animations.add('slot2Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot3Anim.animations.add('slot3Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot4Anim.animations.add('slot4Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot5Anim.animations.add('slot5Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot6Anim.animations.add('slot6Anim', [5,6,7,8,0,1,2,3,4], 20, true);
    slot7Anim.animations.add('slot7Anim', [6,7,8,0,1,2,3,4,5], 20, true);
    slot8Anim.animations.add('slot8Anim', [7,8,0,1,2,3,4,5,6], 20, true);
    slot9Anim.animations.add('slot9Anim', [8,0,1,2,3,4,5,6,7], 20, true);
    slot10Anim.animations.add('slot10Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot11Anim.animations.add('slot11Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot12Anim.animations.add('slot12Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot13Anim.animations.add('slot13Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot14Anim.animations.add('slot14Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot15Anim.animations.add('slot15Anim', [7,8,0,1,2,3,4,5,6], 20, true);

    slot1Anim.animations.getAnimation('slot1Anim').play();
    slot2Anim.animations.getAnimation('slot2Anim').play();
    slot3Anim.animations.getAnimation('slot3Anim').play();
    slot4Anim.animations.getAnimation('slot4Anim').play();
    slot5Anim.animations.getAnimation('slot5Anim').play();
    slot6Anim.animations.getAnimation('slot6Anim').play();
    slot7Anim.animations.getAnimation('slot7Anim').play();
    slot8Anim.animations.getAnimation('slot8Anim').play();
    slot9Anim.animations.getAnimation('slot9Anim').play();
    slot10Anim.animations.getAnimation('slot10Anim').play();
    slot11Anim.animations.getAnimation('slot11Anim').play();
    slot12Anim.animations.getAnimation('slot12Anim').play();
    slot13Anim.animations.getAnimation('slot13Anim').play();
    slot14Anim.animations.getAnimation('slot14Anim').play();
    slot15Anim.animations.getAnimation('slot15Anim').play();

    slot1Anim.visible = false;
    slot2Anim.visible = false;
    slot3Anim.visible = false;
    slot4Anim.visible = false;
    slot5Anim.visible = false;
    slot6Anim.visible = false;
    slot7Anim.visible = false;
    slot8Anim.visible = false;
    slot9Anim.visible = false;
    slot10Anim.visible = false;
    slot11Anim.visible = false;
    slot12Anim.visible = false;
    slot13Anim.visible = false;
    slot14Anim.visible = false;
    slot15Anim.visible = false;
}

var finalValues; var wlValues; var balance; var balanceR; var totalWin; var totalWinR; var dcard; var lines; var linesR; var betline; var betlineR; var bet; var extralife;//totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');
    if (dataArray.indexOf('result=ok') != -1 && dataArray.indexOf('state=0') != -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        totalWinR = finalValues[15];
        linesR = finalValues[16];
        betlineR = finalValues[17];

        checkSpinResult(totalWinR);

        slotRotation(game, finalValues);
    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons();

        checkRopeGameAnim = 1;

//        var winMonkey = game.add.audio('winMonkey');
//        winMonkey.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 7000);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('game1.bg_top_win'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = game.add.audio('winLine');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {
                if(!isMobile) {
                    showButtons([[startButton, 'start'], [betmax, 'betMax'], [betone, 'betOne'], [payTable, 'payTable']]);
                } else {
                    showButtons([[startButton], [home], [gear], [dollar], [double]]);
                }

                hideNumberWinLine();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий
                if(isMobile) {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 370-mobileX, 345-mobileY);
                } else {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 370, 345);
                }
                soundWinLines.play();
                soundWinLinesCounter += 1;
                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            hideLines();
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('playTo');
            updateBalance(game, scorePosions, balanceOld, balance);
            if(isMobile){
                showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            } else {
                showButtons();
            }
            checkAutoStart = false;
        }
    }

}
function getNeedUrlPath() {
    if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
        var number = location.pathname.indexOf('/games/');
        var needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname.substring(0,number) + '/';

        return needLocation;
    } else if (location.href.indexOf('public') !== -1 && location.href.indexOf('/game/') !== -1) {
        var number = location.pathname.indexOf('/public/');
        var needLocation = location.href.substring(0,location.href.indexOf('public')) + 'public';

        return needLocation;
    } else if (location.href.indexOf('public') === -1) {
        needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname;

        return needLocation;
    }
}

//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons();
    console.log('betline: '+betline+', lines: '+lines+', bet: '+bet+', sid: '+sid);
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
        dataType: 'html',
        success: function (data) {
            console.log('requestSpin = ', data);
            //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|0&extralife=45&jackpots=1822.16|4122.16|6122.16';
            //alert(data);
            dataSpinRequest = data;

            parseSpinAnswer(dataSpinRequest);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
//------------------------------------------
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [0,1], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

function hideSelectionOfTheManyCellAnim(monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item].visible = false;
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('takeOrRisk');
    hideButtons();

    hideNumbersAmin();
    hideLines();

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle[tableTitle.current].visible = false;
    if(tableTitle.currentAnim){
        tableTitle[tableTitle.currentAnim].stop();
    }
    switch(loadTexture){
        case 'playTo':
            tableTitle.current = 'playTo';
            tableTitle.currentAnim = 'playToAnim';
            break;
        case 'takeOrRisk':
            tableTitle.current = 'takeOrRisk';
            tableTitle.currentAnim = 'takeOrRiskAnim';
            break;
        case 'bonusGame':
            tableTitle.current = 'bonusGame';
            tableTitle.currentAnim = null;
            break;
    }
    tableTitle[tableTitle.current].visible = true;
    if(tableTitle.currentAnim){
        tableTitle[tableTitle.currentAnim].play();
    }
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
        font: '20px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}


// ajax-запросы

//init-запрос
function requestInit() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=init',
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            var dataString = data;

            var initDataArray = dataString.split('&');

            initDataArray.forEach(function (item) {
                if(item.indexOf('SID=') + 1) {
                    sid = item.replace('SID=','');
                }
                if(item.indexOf('user=') + 1) {
                    user = item.replace('user=','');
                }
            });

            if (data.length != 0 && initDataArray.indexOf('result=ok') != -1 && initDataArray.indexOf('state=0') != -1) {
                requestState();
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//state-запрос
var jackpots;
function requestState() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=state&min=1&game='+gamename+'&SID='+sid,
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            var dataString = data;

            var startDataArray = dataString.split('&');

            if (data.length !== 0 && startDataArray.indexOf('result=ok') != -1 && startDataArray.indexOf('state=0') != -1) {

                startDataArray = dataString.split('&');

                startDataArray.forEach(function (item) {
                    if(item.indexOf('balance=') + 1) {
                        balance = item.replace('balance=', '').replace('.00','');
                    }
                    if(item.indexOf('extralife=') + 1) {
                        extralife = item.replace('extralife=','');
                    }
                    if(item.indexOf('jackpots=') + 1) {
                        var jackpotsString = item.replace('jackpots=','');
                        jackpots =  jackpotsString.split('|');
                    }
                    if(item.indexOf('id=') + 1) {
                        id = item.replace('id=','');
                    }
                    if(item.indexOf('min=') + 1) {
                        min = item.replace('min=','');
                    }
                    
                    createGame1();
                    createGame2();
                    createGame3();
                    createGame4();
                });
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    console.log("checkGame = ", checkGame);
    console.log("scorePosions = ", scorePosions);
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
    if(checkGame != 4 && checkGame != 3){
        betline1Score = game.add.text(0, 0, betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
            boundsAlignH: 'center'
        });
        betline1Score.setTextBounds(scorePosions[3][0], scorePosions[3][1], 40,20);

        betline2Score = game.add.text(0, 0, betline, {
            font: scorePosions[4][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
            boundsAlignH: 'center'
        });
        betline2Score.setTextBounds(scorePosions[4][0], scorePosions[4][1], 40, 20);
    }
//        if(checkGame == 2){
//            riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
//                font: scorePosions[3][2]+'px "Press Start 2P"',
//                fill: '#fcfe6e',
//                stroke: '#000000',
//                strokeThickness: 3,
//            });
//        }
}

var takeWin;
var textCounter;
var ActionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'start']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        ActionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время
                if(isMobile) {
                    showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
                } else {
                    showButtons();
                }

                takeWin.stop();
                changeTableTitle('playTo');
                clearInterval(textCounter);

                topBarImage.loadTexture('game1.bg_top'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines();
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines();
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    bet = lines*betline;
    betScore.setText(bet, true);
    linesScore.setText(lines, true);
    if(checkGame != 4){
        betline1Score.setText(betline, true);
        betline2Score.setText(betline, true);
    }
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];
    } else {
        betlineCounter = 0;
        betline = betlineOptions[betlineCounter];
    }
}

function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard, lines, bet, sid) {
    hideButtons([[startButton, 'start']]);
    disableInputCards();
    lockDisplay();
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
        dataType: 'html',
        success: function (data) {
            console.log('requestDouble --- ', data);
            dataDoubleRequest = data.split('&');
            parseDoubleAnswer(dataDoubleRequest);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

var dwin; var dcard2; var selectedCardR;
function parseDoubleAnswer(dataDoubleRequest) {
    console.log(dataDoubleRequest);
    if (dataDoubleRequest.indexOf('result=ok') != -1) {
        //&& dataDoubleRequest.indexOf('state=0') != -1
console.log('parseDoubleAnswer');
        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResult(dwin, selectedCardR, valuesOfAllCards);

    }
}

var step = 1;
function showDoubleResult(dwin, selectedCardR, valuesOfAllCards) {
    console.log('showDoubleResult');
    console.log(isMobile);
    if (!isMobile) {
        if(dwin > 0) {
            console.log('desctop');
            hideDoubleToAndTakeOrRiskTexts();
//            tableTitle.visible = true;
//            changeTableTitle('winTitleGame2');
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'start']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout(function(){openAllCards(valuesOfAllCards)}, 1000);
            setTimeout(function(){
                hideAllCards(cardArray); 
//                tableTitle.visible = false; 
                step += 1; 
                updateTotalWin(game, dwin, step)
            }, 2000);
            setTimeout(function(){
                openDCard(dcard); 
                showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "start"]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave);
            }, 3000);
        } else {
//            tableTitle.visible = true;
//            changeTableTitle('loseTitleGame2');
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'start']]);
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout(function(){openAllCards(valuesOfAllCards)}, 1000);
            setTimeout(function(){
//                tableTitle.visible = false; 
                unlockDisplay(); 
                game.state.start('game1');
            }, 2000);
        }
    } else {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
//            tableTitle.visible = true;
//            changeTableTitle('winTitleGame2');
            winCard.play();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout(function(){
                openAllCards(valuesOfAllCards); 
                lockDisplay(); 
                disableInputCards();
            }, 500);
            setTimeout(function(){
                hideAllCards(cardArray); 
                openDCard(dcard); 
                disableInputCards(); 
                tableTitle.visible = false; 
                step += 1; 
                updateTotalWin(game, dwin, step);
            }, 2000);
            setTimeout(function(){
                enableInputCards(); 
                unlockDisplay(); 
                showButtons([[startButton]]); 
                showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave);
            }, 3000);
        } else {
//            tableTitle.visible = true;
//            changeTableTitle('loseTitleGame2');
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout(function(){
                openAllCards(valuesOfAllCards); 
                disableInputCards(); 
                lockDisplay();
            }, 1000);
            setTimeout(function(){
                tableTitle.visible = false; 
                unlockDisplay(); 
                game.state.start('game1');
            }, 3000);
        }
    }
}

var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
function showDoubleToAndTakeOrRiskTexts(game, totalWin, x,y) {
    xSave = x;
    ySave = y;

    var i = 1;
    timerTitleAmin = setInterval(function() {
        if(i == 0) {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            doubleToText = game.add.text(x, y, 'DOUBLE TO '+totalWin*2+' ?', {
                font: '14px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 1;
        } else {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            takeOrRiskText = game.add.text(x, y, 'TAKE OR RISK', {
                font: '14px "Press Start 2P"',
                fill: '#ffffff',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 0;
        }

    }, 500);
}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    takeOrRiskText.visible = false;
    clearInterval(timerTitleAmin);
}

function updateTotalWin(game, dwin, step){
    //обновление totalWin в игре с картами

    linesScore.visible = false;
//    riskStep.visible = false;

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], dwin, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

//    riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], step, {
//        font: scorePosions[3][2]+'px "Press Start 2P"',
//        fill: '#fcfe6e',
//        stroke: '#000000',
//        strokeThickness: 3,
//    });
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];

//var cardPosition = [[x,y], [x,y], [x,y], [x,y], [x,y]]  - нулевой элемент массива карта диллера
var openCard; //звуки
var winCard;
function addCards(game, cardPosition) {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card1.scale.setTo(0.6,0.6);
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card2.scale.setTo(0.6,0.6);
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card3.scale.setTo(0.6,0.6);
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card4.scale.setTo(0.6,0.6);
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');
        card5.scale.setTo(0.6,0.6);

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    
    if(isMobile) {
        card2.inputEnabled = true;
        card2.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid);
        });
        card3.inputEnabled = true;
        card3.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        card4.inputEnabled = true;
        card4.events.onInputDown.add(function(){
            requestDouble(gamename, 3, lines, bet, sid);
        });
        card5.inputEnabled = true;
        card5.events.onInputDown.add(function(){
            requestDouble(gamename, 4, lines, bet, sid);
        });
    }
}

function openDCard(dcard) {
    lockDisplay();
    disableInputCards();
    setTimeout(function(){
        card1.loadTexture('card_'+dcard); 
        openCard.play(); 
        enableInputCards(); 
        unlockDisplay();
    }, 1000);
}

var selectedCardValue; //значение карты выбранной игроком
function openSelectedCard(selectedCardR, valuesOfAllCards) {
    openCard.play();
    pick.position.x = cardPosition[selectedCardR][0] + 13;
    pick.visible = true;
    cardArray[selectedCardR].loadTexture("card_"+valuesOfAllCards[selectedCardR]);
}

var valuesOfAllCards; // значения остальных карт [,,,,]
function openAllCards(valuesOfAllCards) {
    openCard.play();
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_'+valuesOfAllCards[i]);
    });
}

function hideAllCards(cardArray) {
    pick.visible = false;
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_bg');
        item.inputEnabled
    });
}

function disableInputCards() {
    card2.inputEnabled = false;
    card3.inputEnabled = false;
    card4.inputEnabled = false;
    card5.inputEnabled = false;
}

function enableInputCards() {
    card2.inputEnabled = true;
    card3.inputEnabled = true;
    card4.inputEnabled = true;
    card5.inputEnabled = true;
}


//функции для игры с последовательным выбором (веревки, ящики, бочки и т.д.)

// отображает результат выбора веревки (подлетает цифра и пересчитвается значение totalWin)
var stepTotalWinR = 0;
function showWinGame3(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y+100, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

    var timeInterval = 450;
    var textCounter = setInterval(function () {
        text.position.y -= 3;
    }, 10);

    setTimeout(function() {
        clearInterval(textCounter);
    }, timeInterval);

    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}





//функции для игры с выбором из двух вариантов








//функции для мобильной версии

var startButton; var double; var bet1; var dollar; var gear; var home;
function addButtonsGame1Mobile(game) {

    startButton = game.add.sprite(588, 228, 'start');
    startButton.bringToTop();
    startButton.anchor.setTo(0.5, 0.5);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('start');
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            startButton.loadTexture('start_d');
            if(checkWin == 0) {
                hideLines();
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            takeWin.stop();
            changeTableTitle('playTo');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    double = game.add.sprite(549, 133, 'double');
    double.inputEnabled = true;
    double.input.useHandCursor = true;
    double.events.onInputDown.add(function(){
        checkWin = 0;
        hideNumbersAmin();
        game.state.start('game2');
    });
    double.events.onInputUp.add(function(){
        double.loadTexture('double');
    });
    double.visible = false;

    bet1 = game.add.sprite(546, 274, 'bet1');
    bet1.inputEnabled = true;
    bet1.input.useHandCursor = true;
    bet1.events.onInputDown.add(function(){
        /*lines = 9;
        betline = 25;*/

        bet1.loadTexture('bet1_p');
        document.getElementById('betMode').style.display = 'block';
        widthVisibleZone = $('.betWrapper .visibleZone').height();
        console.log(widthVisibleZone);
        $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
        $('canvas').css('display', 'none');
    });
    bet1.events.onInputUp.add(function(){
        bet1.loadTexture('bet1');
    });

    dollar = game.add.sprite(445, 30, 'dollar');
    dollar.scale.setTo(0.7,0.7);
    dollar.inputEnabled = true;
    dollar.input.useHandCursor = true;
    dollar.events.onInputDown.add(function(){
        //game.state.start('game4');
    });

    gear = game.add.sprite(519, 30, 'gear');
    gear.scale.setTo(0.7,0.7);
    gear.inputEnabled = true;
    gear.input.useHandCursor = true;
    gear.events.onInputDown.add(function(){
        //game.state.start('game3');
    });

    home = game.add.sprite(45, 30, 'home');
    home.scale.setTo(0.7,0.7);
    home.inputEnabled = true;
    home.input.useHandCursor = true;
    home.events.onInputDown.add(function(){
        home.loadTexture('home_d');
    });
    home.events.onInputUp.add(function(){
        home.loadTexture('home');
    });
}

function addButtonsGame2Mobile(game) {
    startButton = game.add.sprite(538, 300, 'collect');
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('collect');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });
    startButton.scale.setTo(0.7,0.7);
}

//выбор множителя в меню bet
var denomination = 1;
function selectDenomination(el) {
    denomination = el.innerText;

    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    
    var selectedElement = document.getElementsByClassName('denomSize selected');
    selectedElement[0].className = 'denomSize';

    el.className = 'denomSize selected';
}

//выставление максимального значения ставки на линию
function maxBetlineForBetMenu() {
    maxBetline();
    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    document.getElementsByClassName('checkCssTopBetLineRange')[0].style.top = '34.5%';
    document.querySelectorAll('.checkCssTopBetLineRange')[0].querySelectorAll('.selected')[0].classList.remove('selected');
    document.getElementById('cellBetLine25').classList.add('selected');
}

console.log("end functions");


//-----------------------------------------------------------------------------------


//переменные получаемые из api
var gamename = 'crmonkey2rus'; //название игры
var result;
var state;
var sid;
var user;
var min;
var id;
var balance = 10000000;
var extralife = 45;
var jackpots;
var betline = 1;
var lines = 9;
var bet = 9;
var info;
var wl;
var dcard;
var dwin;
var dcard2;
var select;

//звуки и полноэкранный режим
var fullStatus = false;
var soundStatus = true;

/* init-запрос */

requestInit();

//-----------------------------------------------------------------------------------


var game = new Phaser.Game(640, 480, Phaser.AUTO, null, null, 'ld29', null, false, false);

var slotPosition = [[145-mobileX, 85-mobileY], [145-mobileX, 195-mobileY], [145-mobileX, 304-mobileY], [257-mobileX, 85-mobileY], [257-mobileX, 195-mobileY], [257-mobileX, 304-mobileY], [369-mobileX, 85-mobileY], [369-mobileX, 195-mobileY], [369-mobileX, 304-mobileY], [481-mobileX, 85-mobileY], [481-mobileX, 195-mobileY], [481-mobileX, 304-mobileY], [593-mobileX, 85-mobileY], [593-mobileX, 195-mobileY], [593-mobileX, 304-mobileY]];

var linePosition = [[135-mobileX,240-mobileY], [135-mobileX,97-mobileY], [135-mobileX,382-mobileY], [135-mobileX,159-mobileY], [135-mobileX,119-mobileY], [135-mobileX,132-mobileY], [135-mobileX,261-mobileY], [135-mobileX,272-mobileY], [135-mobileX,151-mobileY]];

var numberPosition = [[97-mobileX,230-mobileY], [97-mobileX,86-mobileY], [97-mobileX,374-mobileY], [97-mobileX,150-mobileY], [97-mobileX,310-mobileY], [97-mobileX,118-mobileY], [97-mobileX,342-mobileY], [97-mobileX,262-mobileY], [97-mobileX,198-mobileY]];
                      
var scorePosions = [[265-mobileX, 26-mobileY, 20], [470-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [92-mobileX, 56-mobileY, 20], [695-mobileX, 56-mobileY, 20]];

var linesPlay = ['line1','line2','line3','line4','line5','line6','line7','line8','line9'];

var checkGame = 1, 
    topBarImage,
    tableTitle = {
        playTo: null,
        playToAnim: null,
        bonusGame: null,
        takeOrRisk: null,
        takeOrRiskAnim: null,
        current: null,
        currentAnim: null
    };

function createGame1(){
    game.stage.disableVisibilityChange = true;
    game1 = {
        create: function(){
            checkGame = 1;
//            game.add.sprite(0,0, 'game.background');
            game.add.sprite(93-mobileX,23-mobileY, 'game.background1');
            game.add.sprite(560-mobileX,450-mobileY, 'game.bottom_pan');
            
            topBarImage = game.add.sprite(139-mobileX,24-mobileY, 'game1.bg_top');
            
            tableTitle.playTo = game.add.sprite(585-mobileX,460-mobileY, 'playTo');
            tableTitle.playToAnim = tableTitle.playTo.animations.add('playTo', [0, 1], 5, true);
            tableTitle.playToAnim.play();
            tableTitle.current = 'playTo';
            tableTitle.currentAnim = 'playToAnim';
            
            tableTitle.takeOrRisk = game.add.sprite(585-mobileX,460-mobileY, 'takeOrRisk');
            tableTitle.takeOrRisk.visible = false;
            tableTitle.takeOrRiskAnim = tableTitle.takeOrRisk.animations.add('takeOrRisk', [0, 1], 5, true);
            
            tableTitle.bonusGame = game.add.sprite(585-mobileX,460-mobileY, 'bonusGame');
            tableTitle.bonusGame.visible = false;
            
            
//            addTableTitle(game, 'play1To',235,343);
            addSlots(game, slotPosition);
            // betScore, linesScore, balanceScore, betline1Score, betline2Score
            addScore(game, scorePosions, bet, lines, balance, betline);
            addLinesAndNumbers(game, linePosition, numberPosition);
            showLines(linesPlay);
            showNumbers(numberArray);

            // кнопки
            addButtonsGame1Mobile(game);
            full_and_sound();
            
            makeStaticAnimations();
        }
    }
    game.state.add('game1', game1);
}


//----------------------------------------------------------------------------



var cardPosition = [[152-mobileX,159-mobileY], [259-mobileX,159-mobileY], [366-mobileX,159-mobileY], [473-mobileX,159-mobileY], [580-mobileX,159-mobileY]];
var pick;
function createGame2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        game.add.sprite(93-mobileX,23-mobileY, 'game.background1');
        game.add.sprite(125-mobileX,85-mobileY, 'game.background2');
        game.add.sprite(139-mobileX,24-mobileY, 'game2.bg_top');
        game.add.sprite(560-mobileX,450-mobileY, 'game.bottom_pan');
        

//        hideTableTitle();

        //счет
        step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
//        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
        
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);
        //карты
        addCards(game, cardPosition);
        game.add.sprite(165-mobileX,300-mobileY, 'game2.dealer');
        pick = game.add.sprite(161-mobileX, 300-mobileY, 'game2.pick');
        pick.visible = false;
        
        openDCard(dcard);
        
        // кнопки
        addButtonsGame2Mobile(game);

        showDoubleToAndTakeOrRiskTexts(game, totalWin, 585-mobileX, 465-mobileY);

        full_and_sound();
        makeStaticAnimations();
    };
    game.state.add('game2', game2);

    // звуки
}




//----------------------------------------------------------------------




var stepTotalWinR = 0;

function showWin(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y+100, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

    var timeInterval = 450;
    var textCounter = setInterval(function () {
        text.position.y -= 3;
    }, 10);

    setTimeout(function() {
        clearInterval(textCounter);
    }, timeInterval);

    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
//        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}




var ropeIndex, ropeDif;
var fisher, blockEvent = false, posX = 0, game3;
function createGame3() {
    game3={
        anime: {},
        create: function(){
            ropeIndex = 0;
            ropeDif = {
//                0: 'bear',
                2: 'fishSmall',
                5: 'fishBig',
                10: 'squid',
                15: 'mermaid',
                x: 'shield',
            };
            //------------------- shield - переход на супер игру !!!!!!!!
            checkGame = 3;
            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            checkWin = 0;
            
            game.add.sprite(94-mobileX,23-mobileY, 'game3.bg');
            game.add.sprite(139-mobileX,24-mobileY, 'game3.bg_top');
            game3.fisher = game.add.sprite(307-mobileX, 260-mobileY, 'game3.fisher');
            game3.fisherSound = game.add.audio('shoot');
            game3.sound = game.add.sound('bonusGame');
            game3.sound.loop = true;
            game3.sound.play();
            
            hole1 = game.add.sprite(100-mobileX, 447-mobileY, 'game3.hole');
            hole2 = game.add.sprite(217-mobileX, 447-mobileY, 'game3.hole');
            hole3 = game.add.sprite(334-mobileX, 447-mobileY, 'game3.hole');
            hole4 = game.add.sprite(451-mobileX, 447-mobileY, 'game3.hole');
            hole5 = game.add.sprite(568-mobileX, 447-mobileY, 'game3.hole');
            
            hole1.inputEnabled = true;
            hole1.input.useHandCursor = true;
            hole1.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(78 - mobileX);
            });
            
            hole2.inputEnabled = true;
            hole2.input.useHandCursor = true;
            hole2.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(195 - mobileX);
            });
            
            hole3.inputEnabled = true;
            hole3.input.useHandCursor = true;
            hole3.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(312 - mobileX);
            });
            
            hole4.inputEnabled = true;
            hole4.input.useHandCursor = true;
            hole4.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(429 - mobileX);
            });
            
            hole5.inputEnabled = true;
            hole5.input.useHandCursor = true;
            hole5.events.onInputUp.add(function(){
                if(blockEvent) return;
                makeShot(546 - mobileX);
            });
            
            //счет
//            scorePosions = [[265, 26, 20], [470, 26, 20], [640, 26, 20], [92, 56, 20], [695, 56, 20]];
            addScore(game, scorePosions, bet, '', balance, betline);
            
            function makeShot(x){
                blockEvent = true;
                game3.shot.position.x = x;
                game3.shot.visible = true;
                game3.fisher.visible = false;
                game3.shotAnimation.play();
                setTimeout(function(){
                    game3.fisherSound.play();
                }, 600);
            }
            function showAnimation(name, posX){
                var all = ['mermaid', 'fishSmall','fishBig', 'squid'], index = all.indexOf(name);
                var an = name + 'Animation', border, bAnime;
                game3.fisher.position.x = posX;
                game3.anime[name].visible = true;
                game3.anime[name].position.x = posX;
                game3.anime[an].play();

//                if(!isMobile) disablePanel(game3);
                if(index != -1){
                    switch(name){
                        case 'mermaid':
                            border = 'mermaidBorder';
                            bAnime = 'mermaidBorderAnimation';
                            break;
                        case 'fishSmall':
                            border = 'sFish_border';
                            bAnime = 'sFish_borderAnimation';
                            break;
                        case 'fishBig':
                            border = 'bFish_border';
                            bAnime = 'bFish_borderAnimation';
                            break;
                        case 'squid':
                            border = 'squidBorder';
                            bAnime = 'squidBorderAnimation';
                            break;
                    }
                }
//                if(!isMobile){
//                    unDisableNumberLine(game3);
//                    game3.startButton.loadTexture('start');
//                }
                if(index != -1){
                    game3[border].visible = true;
                    game3[bAnime].play();
                }
                game3.anime[name].sound.play();

            }
            function getAnime(){
                var str; 
                if(ropeValues[ropeIndex] in ropeDif) str = ropeDif[ropeValues[ropeIndex]];
                else str = 'shield';
                ropeIndex++;
                return str;
            }
            
            game3.anime.mermaid = game.add.sprite(200-mobileX ,265-mobileY,'mermaid');
            game3.anime.mermaid.sound = game.add.audio('mermaid');
            game3.anime.mermaid.visible = false;
            game3.anime.mermaidAnimation = game3.anime.mermaid.animations.add('mermaid',[0, 1, 2, 3, 4, 5, 4, 5, 4, 5],7,false);
            game3.anime.mermaidAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.mermaid.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.fishBig = game.add.sprite(200-mobileX,260-mobileY,'fish_big');
            game3.anime.fishBig.sound = game.add.audio('bFish');
            game3.anime.fishBig.visible = false;
            game3.anime.fishBigAnimation = game3.anime.fishBig.animations.add('fish_big',[0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2],7,false);
            game3.anime.fishBigAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.fishBig.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.fishSmall = game.add.sprite(200-mobileX,260-mobileY,'fish_small');
            game3.anime.fishSmall.sound = game.add.audio('sFish');
            game3.anime.fishSmall.visible = false;
            game3.anime.fishSmallAnimation = game3.anime.fishSmall.animations.add('fish_small', [0, 1, 2, 3, 2, 3, 2, 3, 2, 3],7,false);
            game3.anime.fishSmallAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.fishSmall.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.squid = game.add.sprite(200-mobileX,260-mobileY,'squid');
            game3.anime.squid.sound = game.add.audio('squid');
            game3.anime.squid.visible = false;
            game3.anime.squidAnimation = game3.anime.squid.animations.add('squid', [0, 1, 2, 3, 2, 3, 2, 3, 2, 3],7,false);
            game3.anime.squidAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.squid.visible = false;
                    game3.fisher.visible = true;
//                    updateBalanceGame3(game, scorePosions, balanceR);
                }, 300);
            });

            game3.anime.bear = game.add.sprite(200-mobileX,260-mobileY,'bear');
            game3.anime.bear.sound = game.add.audio('bear');
            game3.anime.bear.visible = false;
            game3.anime.bearAnimation = game3.anime.bear.animations.add('bear', [0, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4],7,false);
            game3.anime.bearAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.bear.visible = false;
                    game3.fisher.visible = true;
                    game3.sound.stop();
                    game.state.start('game1');
                }, 300);
            });

            game3.anime.shield = game.add.sprite(200-mobileX,260-mobileY,'shield');
            game3.anime.shield.sound = game.add.audio('shield');
            game3.anime.shield.visible = false;
            game3.anime.shieldAnimation = game3.anime.shield.animations.add('shield', [0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2],7,false);
            game3.anime.shieldAnimation.onComplete.add(function(){
                setTimeout(function(){
                    blockEvent = false;
                    game3.anime.shield.visible = false;
                    game3.fisher.visible = true;
                    game3.sound.stop();
                    game.state.start('game4');
                }, 300);
            });
            
            game3.dance = game.add.sprite(300,170-mobileY,'dance');
            game3.dance.visible = true;
            game3.danceAnimation = game3.dance.animations.add('dance', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66], 30, true);
            game3.danceAnimation.play();

            game3.mermaidBorder = game.add.sprite(658,164-mobileY, 'mermaidBorder');
            game3.mermaidBorder.visible = false;
            game3.mermaidBorderAnimation = game3.mermaidBorder.animations.add('mermaidBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.sFish_border = game.add.sprite(109-mobileX,97-mobileY, 'sFishBorder');
            game3.sFish_border.visible = false;
            game3.sFish_borderAnimation = game3.sFish_border.animations.add('sFishBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.bFish_border = game.add.sprite(109-mobileX,135-mobileY, 'bFishBorder');
            game3.bFish_border.visible = false;
            game3.bFish_borderAnimation = game3.bFish_border.animations.add('bFishBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.squidBorder = game.add.sprite(659-mobileX,86-mobileY, 'squidBorder');
            game3.squidBorder.visible = false;
            game3.squidBorderAnimation = game3.squidBorder.animations.add('squidBorder',[0,1,0,1,0,1,0,1,0,1,0,1], 7, false);

            game3.shot = game.add.sprite(307-mobileX, 260-mobileY, 'shot');
            game3.shotAnimation = game3.shot.animations.add('shot',[0,1,2,3,4,5,6,7,8,9,10], 5, false);
            game3.shotAnimation.onComplete.add(function(){
                game3.shot.visible = false;
                showAnimation(getAnime(), game3.shot.position.x);
            }); 
        }
    };
    
    game.state.add("game3", game3);
}


//-------------------------------------------------------




function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        game.state.start('game1');
    }, timeInterval);
}

function winResultGame4(game, x,y) {
    monkeyHangingHide();
    mankeyLeftPoint.visible = true;
    setTimeout("bitMonkey.play();", 500);
    take_or_risk_anim.visible = false;
    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        winPoint1.play();
        goldPoint = game.add.sprite(207,263, 'game.goldPoint');
        goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
        goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
            mankeyLeftPoint.visible = false;
            winPoint2.play();
            mankeyWinPoint.visible = true;
            mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
                updateBalanceGame4(game, scorePosions, balanceR);
                game.state.start('game1');
            });
        });
    });
}

function loseResultGame4(game, x,y) {
    setTimeout("bitMonkey.play();", 500);
    monkeyHangingHide();
    take_or_risk_anim.visible = false;
    mankeyLeftPoint.visible = true;
    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        //winPoint1.play();
        losePoint.play();
        spiderPoint = game.add.sprite(207,263, 'game.spiderPoint');
        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
        spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
            mankeyLeftPoint.visible = false;

            mankeyLosePoint.visible = true;
            mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
                updateBalanceGame4(game, scorePosions, balanceR);
                game.state.start('game1');
            });
        });

    });
}

function getResultGame4() {
    console.log(ropeValues);
    if(ropeValues[5] != 0){
//        lockDisplay();
        return true;// - win
    } else {
//        lockDisplay();
        return false;// - lose
    }
}

var shaman;
function createGame4() {

    var button;

    var game4 = {};

    game4.create = function () {
        checkGame = 4;
        blockEvent = false;
        
        game.add.sprite(95-mobileX,23-mobileY, 'game4.bg');
        game.add.sprite(139-mobileX,24-mobileY, 'game3.bg_top');
        shaman = game.add.sprite(320-mobileX, 320, 'game4.shaman');
        
        game4.green_eyes = game.add.sprite(228-mobileX, 155-mobileY, 'game4.green_eyes');
        game4.green_eyes.visible = false;
        game4.text = game.add.sprite(80-mobileX, 455-mobileY, 'game4.your_choice');
        game4.text.scale.setTo(0.8,0.8);
        game4.sound = game.add.audio('superbonus');
        game4.sound.loop = true;
        game4.sound.play();
        
//        addButtonsGame4();
        addScore(game, scorePosions, bet, '', balance, betline);
        
        game4.lTotem = game.add.sprite(219-mobileX,120-mobileY, 'game4.totem');
        game4.lTotem.inputEnabled = true;
        game4.lTotem.input.useHandCursor = true;
        game4.lTotem.events.onInputUp.add(function(){
            if(blockEvent) return;
            posX = 170-mobileX;
            makeDance();
        });
        
        game4.rTotem = game.add.sprite(507-mobileX,120-mobileY, 'game4.totem');
        game4.rTotem.inputEnabled = true;
        game4.rTotem.input.useHandCursor = true;
        game4.rTotem.events.onInputUp.add(function(){
            if(blockEvent) return;
            posX = 450-mobileX;
            makeDance();
        });
        function makeDance(){
//            if(!isMobile){
//                disablePanel(game4);
//            }
            blockEvent = true;
            game4.text.visible = false;
            game4.choice.visible = true;
            shaman.visible = false;
            game4.choice.position.x = posX;
            game4.choice.sound.play();
            game4.choiceAnimation.play();
        }
        function makeWin(){
            game4.gold_rain.visible = true;
            game4.gold_rain.position.x = game4.choice.position.x + 47;
            game4.gold_rain.sound.play();
            game4.gold_rainAnimation.play();
        }
        function makeLose(){
            game4.choice.visible = false;
            game4.pain.position.x = game4.choice.position.x;
            game4.pain.visible = true;
            game4.pain.sound.play();
            game4.painAnimation.play();
            
            game4.lightning.position.x = game4.choice.position.x - 50;
            game4.lightning.visible = true;
            game4.lightningAnimation.play();
        }
        
        function openPanel(){
            game4.green_eyes.visible = false;
            game4.choice.visible = false;
            shaman.visible = true;
            game4.text.visible = true;
            game4.pain.visible = false;
            blockEvent = false;
//            if(!isMobile){
//                game4.startButton.loadTexture('start');
//                game4.lines[3].button.loadTexture('btnline3');
//                game4.lines[7].button.loadTexture('btnline7');
//            }
        }
        game4.choice = game.add.sprite(300-mobileX, 300-mobileY, 'choice');
        game4.choice.visible = false;
        game4.choice.sound = game.add.audio('choice');
        game4.choiceAnimation = game4.choice.animations.add('choice', [0,1,2,3,4,5,6,7,8,7,8,6,5,4,5,6,7,8,9,7,8,6,7,8,9,9,10], 8 , false);
        game4.choiceAnimation.onComplete.add(function(){
            if(getResultGame4()) makeWin();
            else makeLose();
            game4.green_eyes.visible = true;
        });
        
        game4.pain = game.add.sprite(100-mobileX, 300-mobileY, 'pain');
        game4.pain.visible = false;
        game4.pain.sound = game.add.audio('pain');
        game4.painAnimation = game4.pain.animations.add('pain', [0,1,2,3,4,5,6,7,8,9,10,11], 5 , false);
        game4.painAnimation.onComplete.add(function(){
            setTimeout(function(){
//                updateBalanceGame4(game, scorePosions, balanceR);
                game.state.start('game1');
            }, 2000);
        });
        
        game4.lightning = game.add.sprite(400-mobileX, 30-mobileY, 'lightning');
        game4.lightning.scale.setTo(0.9,0.9);
        game4.lightning.visible = false;
        game4.lightning.sound = game.add.audio('lightning');
        game4.lightningAnimation = game4.lightning.animations.add('lightning', [0,1,2,3], 5 , false);
        game4.lightningAnimation.onComplete.add(function(){
            game4.lightning.visible = false;
        });
        
        game4.gold_rain = game.add.sprite(100-mobileX, 100-mobileY, 'gold_rain');
        game4.gold_rain.scale.setTo(0.9,0.9);
        game4.gold_rain.visible = false;
        game4.gold_rain.sound = game.add.audio('gold_rain');
        game4.gold_rainAnimation = game4.gold_rain.animations.add('gold_rain', [0,1,2,3], 5 , false);
        game4.gold_rainAnimation.onComplete.add(function(){
            game4.gold_rain.visible = false;
            setTimeout(function(){
//                updateBalanceGame4(game, scorePosions, balanceR);
                game.state.start('game1');
            }, 2000);
        });
    };
    game.state.add('game4', game4);
}



//---------------------------------------------------------------------------------



(function(){
    var preload = {};
    console.log("preload");
    console.log("game = ", game);
    
    preload.preload = function(){
        
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });
        
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        
        game.load.image('start', 'img/btns/mobile/spin.png');
        game.load.image('start_p', 'img/btns/mobile/spin_p.png');
        game.load.image('start_d', 'img/btns/mobile/spin_d.png');
        game.load.image('bet1', 'img/btns/mobile/bet1.png');
        game.load.image('bet1_p', 'img/btns/mobile/bet1_p.png');
        game.load.image('home', 'img/btns/mobile/home.png');
        game.load.image('home_p', 'img/btns/mobile/home_p.png');
        game.load.image('dollar', 'img/btns/mobile/dollar.png');
        game.load.image('gear', 'img/btns/mobile/gear.png');
        game.load.image('double', 'img/btns/mobile/double.png');
        game.load.image('collect', 'img/btns/mobile/collect.png');
        game.load.image('collect_p', 'img/btns/mobile/collect_p.png');
        game.load.image('collect_d', 'img/btns/mobile/collect_d.png');
        
        game.load.image('x', 'img/x.png');
        game.load.image('game.background1', 'img/main_bg.jpg');
        game.load.image('game.background2', 'img/bg_game_2.png');
        game.load.image('game3.bg', 'img/bg_game_3.png');
        game.load.image('game4.bg', 'img/bg_game_4.png');
        game.load.image('game1.bg_top', 'img/main_top_1.png');
        game.load.image('game1.bg_top_win', 'img/main_top_1_win.png');
        game.load.image('game2.bg_top', 'img/main_top_2.png');
        game.load.image('game3.bg_top', 'img/main_top_3.png');
        game.load.image('game.bottom_pan', 'img/info.png');
        
        game.load.image('game2.dealer', 'img/dealer.png');
        game.load.image('game2.pick', 'img/pick.png');
        game.load.image('card_bg', 'img/cards/back.png');
        
        game.load.image('game3.hole', 'img/ice-hole.png');
        game.load.image('game3.fisher', 'img/fisher.png');
        
        game.load.image('game4.shaman', 'img/shaman.png');
        game.load.image('game4.green_eyes', 'img/green_eyes.png');
        game.load.image('game4.your_choice', 'img/your_choice.png');
        game.load.image('game4.totem', 'img/totem.png');
        
        for(var i =0; i < 52; i++){
            game.load.image('card_' + i, 'img/cards/'+i + '.png');
        }
        
//        game.load.image('game.background', 'img/machine.png');
//        game.load.image('start', 'img/btns/btn_start.png');
//        game.load.image('start_p', 'img/btns/btn_start_p.png');
//        game.load.image('start_d', 'img/btns/btn_start_d.png');
//        game.load.image('automaricStart', 'img/btns/btn_automatic_start.png');
//        game.load.image('automaricStart_p', 'img/btns/btn_automatic_start_p.png');
//        game.load.image('automaricStart_d', 'img/btns/btn_automatic_start_d.png');
//        game.load.image('betMax', 'img/btns/btn_bet_max.png');
//        game.load.image('betMax_p', 'img/btns/btn_bet_max_p.png');
//        game.load.image('betMax_d', 'img/btns/btn_bet_max_d.png');
//        game.load.image('betOne', 'img/btns/btn_bet_one.png');
//        game.load.image('betOne_p', 'img/btns/btn_bet_one_p.png');
//        game.load.image('betOne_d', 'img/btns/btn_bet_one_d.png');
//        game.load.image('payTable', 'img/btns/btn_paytable.png');
//        game.load.image('payTable_p', 'img/btns/btn_paytable_p.png');
//        game.load.image('payTable_d', 'img/btns/btn_paytable_d.png');
//        game.load.image('selectGame', 'img/btns/btn_select_game.png');
//        game.load.image('selectGame_p', 'img/btns/btn_select_game_p.png');
//        game.load.image('selectGame_d', 'img/btns/btn_select_game_d.png');
        game.load.image('game.non_full', 'img/non_full.png');
        game.load.image('game.full', 'img/full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');
        game.load.image('bonusGame', 'img/bonusGame.png');
        game.load.spritesheet('game1.press_1_res', 'img/press_1_res.png', 96,96,3);
        game.load.spritesheet('game1.press_1', 'img/press_1.png', 96, 96, 2);
        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/bonus.png',96, 96, 2);
        game.load.spritesheet('smoke', 'img/smoke.png', 42, 56, 5);
        game.load.spritesheet('playTo', 'img/playTo.png', 94, 32, 2);
        game.load.spritesheet('takeOrRisk', 'img/takeOrRisk.png', 95, 32, 2);
        
        game.load.spritesheet('fire', 'img/sprite_fire.png', 35, 41, 5);
        game.load.spritesheet('dance', 'img/momo.png',263,77,67);
        game.load.spritesheet('mermaid', 'img/mermaid.png', 199,221,6);
        game.load.spritesheet('mermaidBorder', 'img/mermaid_border.png', 72,92,2);
        game.load.spritesheet('fish_big', 'img/big-fish.png', 199,221,3);
        game.load.spritesheet('bFishBorder', 'img/bFish_border.png', 70,80,2);
        game.load.spritesheet('fish_small', 'img/small-fish.png', 199,221,4);
        game.load.spritesheet('sFishBorder', 'img/sFish_border.png', 71, 32, 2);
        game.load.spritesheet('squid', 'img/squid.png', 199,221,4);
        game.load.spritesheet('squidBorder', 'img/squid_border.png', 52,79,2);
        game.load.spritesheet('shield', 'img/shield.png', 199,221,3);
        game.load.spritesheet('bear', 'img/bear.png', 199,221,5);
        game.load.spritesheet('choice', 'img/choice.png', 215,177,11);
        game.load.spritesheet('pain', 'img/pain.png', 215,177,12);
        game.load.spritesheet('lightning', 'img/lightning.png', 360, 473, 4);
        game.load.spritesheet('gold_rain', 'img/gold_rain.png', 120, 356, 4);
        game.load.spritesheet('shot', 'img/shoot.png', 199, 221, 11);
        game.load.spritesheet('cellAnim', 'img/shape_full.png',96,96,9);
        
        game.load.image('cell0', 'img/cell0.png');
        game.load.image('cell1', 'img/cell1.png');
        game.load.image('cell2', 'img/cell2.png');
        game.load.image('cell3', 'img/cell3.png');
        game.load.image('cell4', 'img/cell4.png');
        game.load.image('cell5', 'img/cell5.png');
        game.load.image('cell6', 'img/cell6.png');
        game.load.image('cell7', 'img/cell7.png');
        game.load.image('cell8', 'img/cell8.png');
        
        for(var i = 1; i < 6; i++){
            game.load.image('game.page_' + i, 'img/page_' + i + '.png');
        }
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line' + i, 'img/lines/select/line' + i + '.png');
            game.load.image('linefull' + i, 'img/lines/win/linefull' + i + '.png');
            game.load.image('num_p_' + i, 'img/num_p_' + i + '.png');
            game.load.image('num_d_' + i, 'img/num_d_' + i + '.png');
//            if (i % 2 != 0) {
//                game.load.audio('line' + i + 'Sound', 'sounds/line' + i + '.mp3');
//                if(!isMobile){
//                    game.load.image('buttonLine' + i, 'img/btns/btn' + i + '.png');
//                    game.load.image('buttonLine'+i+'_p', 'img/btns/btn' + i + '_p.png');
//                    game.load.image('buttonLine'+i+'_d', 'img/btns/btn' + i + '_d.png');
//                }
//            }
        }
        
        game.load.audio('bet_one', 'sounds/betOne.mp3');
        game.load.audio('stopSound', 'sounds/stop.mp3');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('winLine', 'sounds/winLine.mp3');
        game.load.audio('rotateSound', 'sounds/rotate.mp3');
        game.load.audio('tada', 'sounds/tada.mp3');
        game.load.audio('automatic', 'sounds/automatic.mp3');
        game.load.audio('openCard', 'sounds/opencard.mp3');
        game.load.audio('winCard', 'sounds/openuser.mp3');
        
        game.load.audio('shoot', 'sounds/bonusman_kidok.mp3');
        game.load.audio('bear', 'sounds/bonus_gr_med.mp3');
        game.load.audio('mermaid', 'sounds/bonus_find_rusalka.mp3');
        game.load.audio('sFish', 'sounds/bonus_find_ruba.mp3');
        game.load.audio('bFish', 'sounds/bonus_find_big_ruba.mp3');
        game.load.audio('squid', 'sounds/bonus_find_meduza.mp3');
        game.load.audio('shield', 'sounds/bonus_find_buben.mp3');
        game.load.audio('bonusGame', 'sounds/bonus_game.mp3');
        
        game.load.audio('pain', 'sounds/super_bonus_die.mp3');
        game.load.audio('gold_rain', 'sounds/super_bonus_win.mp3');
        game.load.audio('choice', 'sounds/super_bonus_man_say.mp3');
        game.load.audio('lightning', 'sounds/super_bonus_molnia.mp3');
        game.load.audio('superbonus', 'sounds/superbonus.mp3');
    }
    
     preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    }
    game.state.add('preload', preload);
    
})();

game.state.start('preload');

