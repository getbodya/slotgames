var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var slotPosition = [[145, 85], [145, 195], [145, 304], [257, 85], [257, 195], [257, 304], [369, 85], [369, 195], [369, 304], [481, 85], [481, 195], [481, 304], [593, 85], [593, 195], [593, 304]];

var linePosition = [[135,240], [135,97], [135,382], [135,159], [135,119], [135,132], [135,261], [135,272], [135,151]];

var numberPosition = [[97,230], [97,86], [97,374], [97,150], [97,310], [97,118], [97,342], [97,262], [97,198]];

var scorePosions = [[265, 26, 20], [470, 26, 20], [640, 26, 20], [92, 56, 20], [695, 56, 20], [482, 123, 24]];

var linesPlay = ['line1','line2','line3','line4','line5','line6','line7','line8','line9'];

var checkGame = 1, 
topBarImage,
tableTitle = {
    playTo: null,
    playToAnim: null,
    bonusGame: null,
    takeOrRisk: null,
    takeOrRiskAnim: null,
    current: null,
    currentAnim: null
};

function game1(){
    var game1 = {};
    game1.create = function () {
        checkGame = 1;
        game.add.sprite(0,0, 'game.background');
        game.add.sprite(93,23, 'game.background1');
        game.add.sprite(560,450, 'game.bottom_pan');

        topBarImage = game.add.sprite(139,24, 'game1.bg_top');

        tableTitle.playTo = game.add.sprite(585,460, 'playTo');
        tableTitle.playToAnim = tableTitle.playTo.animations.add('playTo', [0, 1], 5, true);
        tableTitle.playToAnim.play();
        tableTitle.current = 'playTo';
        tableTitle.currentAnim = 'playToAnim';

        tableTitle.takeOrRisk = game.add.sprite(585,460, 'takeOrRisk');
        tableTitle.takeOrRisk.visible = false;
        tableTitle.takeOrRiskAnim = tableTitle.takeOrRisk.animations.add('takeOrRisk', [0, 1], 5, true);

        tableTitle.bonusGame = game.add.sprite(585,460, 'bonusGame');
        tableTitle.bonusGame.visible = false;


            //            addTableTitle(game, 'play1To',235,343);
            addSlots(game, slotPosition);
            // betScore, linesScore, balanceScore, betline1Score, betline2Score
            addScore(game, scorePosions, bet, lines, balance, betline);
            addLinesAndNumbers(game, linePosition, numberPosition);
            showLines(linesPlay);
            showNumbers(numberArray);

            // кнопки
            addButtonsGame1(game);
            full_and_sound();
            
            makeStaticAnimations();
        },
        game1.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
             full.loadTexture('game.non_full');
             fullStatus = false;
         }
     };
     game.state.add('game1', game1);
 };