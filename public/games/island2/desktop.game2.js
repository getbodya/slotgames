function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        background = game.add.sprite(0,0, 'game.background');
        game2.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top1');
        game.add.sprite(94+mobileX,22+mobileY, 'bg_top2');
        game1.pirate_win = game.add.sprite(110+mobileX, 358+mobileY, 'game1.pirate_win');
        game2.bg2 = game.add.sprite(94+mobileX,54+mobileY, 'game2.bg');

        addTableTitle(game, 'winTitleGame2', 557,422);
        hideTableTitle();
        
        // кнопки
        addButtonsGame2(game);

        //карты
        var cardPosition = [[113+23-10,129+21], [263-9,129+21], [375-9,129+21], [490-9,129+21], [602-9,129+21]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация

        game2.water_anim = game.add.sprite(542+mobileX, 486+mobileY, 'water_anim');
        game2.water_animAnimation = game2.water_anim.animations.add('water_anim', [0,1,2,3], 8, true);
        game2.water_animAnimation.play();
        game2.water_bottom_1 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_1');
        game2.water_bottom_1Animation = game2.water_bottom_1.animations.add('water_bottom_1', [0,1,2,3,4,5,6,7,8,9], 8, false);
        game2.water_bottom_1Animation.play();
        game2.water_bottom_2 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_2');
        game2.water_bottom_2Animation = game2.water_bottom_2.animations.add('water_bottom_2', [0,1,2,3], 8, false);
        game2.water_bottom_2.visible = false;
        game2.man_1 = game.add.sprite(94+mobileX, 390+mobileY, 'game1.man_1');
        game2.man_1Animation = game2.man_1.animations.add('game1.man_1', [0,1,2,3,4,5,6,7,8,9,10], 8, true);
        game2.man_1Animation.play();
        game2.bird_anim_1 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_1');
        game2.bird_anim_1Animation = game2.bird_anim_1.animations.add('bird_anim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44], 8, false);
        game2.bird_anim_1Animation.play();

        game2.bird_anim_2 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_2');
        game2.bird_anim_2Animation = game2.bird_anim_2.animations.add('bird_anim_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 8, false);
        game2.bird_anim_2.visible = false;
        game2.bird_anim_1Animation.onComplete.add(function(){
            game2.bird_anim_1.visible = false;
            game2.bird_anim_2.visible = true;
            game2.bird_anim_2Animation.play();
        });
        game2.bird_anim_2Animation.onComplete.add(function(){
            game2.bird_anim_1.visible = true;
            game2.bird_anim_2.visible = false;
            game2.bird_anim_1Animation.play();
        });
        game2.water_bottom_1Animation.onComplete.add(function(){
            game2.water_bottom_1.visible = false;
            game2.water_bottom_2.visible = true;
            game2.water_bottom_2Animation.play();
        });
        game2.water_bottom_2Animation.onComplete.add(function(){
            game2.water_bottom_1.visible = true;
            game2.water_bottom_2.visible = false;
            game2.water_bottom_1Animation.play();
        });

        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 84, 30]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, '');

        fontsize = 16;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 565, 435, fontsize);

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}