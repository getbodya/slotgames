var game = new Phaser.Game(1500, 1024, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;

        // звуки
        // заполняем массив звуками выигрышей
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');

        soundWinLineDurations[0] = 1000;
        soundWinLineDurations[1] = 1000;
        soundWinLineDurations[2] = 1000;
        soundWinLineDurations[3] = 2000;
        soundWinLineDurations[4] = 4000;


        // изображения
        var border = game.add.sprite(0,0, 'border');

        //слоты
        totalWinPosion = [200,850];
        slotPosition = [[155+15, 142],
                        [155+15, 364],
                        [155+15, 586],
                            [399+15, 142],
                            [399+15, 364],
                            [399+15, 586],
                                [643+15, 142],
                                [643+15, 364],
                                [643+15, 586],
                                    [887+13, 142],
                                    [887+13, 364],
                                    [887+13, 586],
                                        [1131+11, 142],
                                        [1131+11, 364],
                                        [1131+11, 586]];
        addSlots(game, slotPosition, 8);

        framesArray = [
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
        ];

        // линии и номера
        linePosition = [[95+23,457], [95+23,262], [95+23,658], [120,198], [120 ,220], [94+23,392], [94+23,210], [94+23,335], [94+23,325], [94+23,150]];
        numberPosition = [[45,436], [45,240], [45,635], [45,175], [45,700], [45,370], [45,500], [45,570], [45,305], [45,765], [1385,436], [1385,240], [1385,635], [1385,175], [1385,700], [1385,370], [1385,500], [1385,305], [1385,570], [1385,115]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showLines(lineArray);

        //позиции ячеек относящиеся к линиям
        cellPositionOnLines = [[1,4,7,10,13], [0,3,6,9,12], [2,5,8,11,14], [0,4,8,10,12], [2,4,6,10,14]];
        slotAnimPosition = slotPosition; //позиции анимированных ячеек


        //создаем фон для слотов (нужно для скрытия линий в выигрышных ячейках)
        addSlotBg(slotPosition);


        //создаем массив содержащий все анимации
        addAllCellAnim(game, slotPosition, 8, framesArray);

        //цветные рамки для ячеек
        addSquares(game, slotPosition, 11);

        // кнопки
        addButtonsGame1TypeBORDelux(game);
        
        var groupLineNumbers = game.add.group();
        groupLineNumbers.add(lineNumber1left);
        groupLineNumbers.add(lineNumber2left);
        groupLineNumbers.add(lineNumber3left);
        groupLineNumbers.add(lineNumber4left);
        groupLineNumbers.add(lineNumber5left);
        groupLineNumbers.add(lineNumber1right);
        groupLineNumbers.add(lineNumber2right);
        groupLineNumbers.add(lineNumber3right);
        groupLineNumbers.add(lineNumber4right);
        groupLineNumbers.add(lineNumber5right);
        game.world.bringToTop(groupLineNumbers);

        //изображения для кнопок и надписей
        var status = game.add.sprite(150,830, 'status');
        var shadow_btn = game.add.sprite(150,925, 'shadow_btn');

        //счет
        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[750, 962, 28], [391, 962, 28], [180, 962, 28], [580, 962, 28], [580, 962, 28]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        addTitles();

        full_and_sound();

    };

    game1.update = function () {
        
    };

    game.state.add('game1', game1);

};
