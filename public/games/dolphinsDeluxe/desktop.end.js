(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        /* *
        *  файлы с фиксироваными именами переменных
        *
        * */
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        /* кнопки звука и режима экрана */
        game.load.image('game.non_full', needUrlPath + '/img/full.png');
        game.load.image('game.full', needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        /* номера линий */
        game.load.image('number1left', needUrlPath + '/img/game1/lineNumbers/240.png');
        game.load.image('number2left', needUrlPath + '/img/game1/lineNumbers/237.png');
        game.load.image('number3left', needUrlPath + '/img/game1/lineNumbers/243.png');
        game.load.image('number4left', needUrlPath + '/img/game1/lineNumbers/234.png');
        game.load.image('number5left', needUrlPath + '/img/game1/lineNumbers/246.png');
        game.load.image('number6left', needUrlPath + '/img/game1/lineNumbers/258.png');
        game.load.image('number7left', needUrlPath + '/img/game1/lineNumbers/270.png');
        game.load.image('number8left', needUrlPath + '/img/game1/lineNumbers/252.png');
        game.load.image('number9left', needUrlPath + '/img/game1/lineNumbers/264.png');
        game.load.image('number10left', needUrlPath + '/img/game1/lineNumbers/276.png');

        game.load.image('number1right', needUrlPath + '/img/game1/lineNumbers/225.png');
        game.load.image('number2right', needUrlPath + '/img/game1/lineNumbers/222.png');
        game.load.image('number3right', needUrlPath + '/img/game1/lineNumbers/228.png');
        game.load.image('number4right', needUrlPath + '/img/game1/lineNumbers/219.png');
        game.load.image('number5right', needUrlPath + '/img/game1/lineNumbers/231.png');
        game.load.image('number6right', needUrlPath + '/img/game1/lineNumbers/267.png');
        game.load.image('number7right', needUrlPath + '/img/game1/lineNumbers/255.png');
        game.load.image('number8right', needUrlPath + '/img/game1/lineNumbers/249.png');
        game.load.image('number9right', needUrlPath + '/img/game1/lineNumbers/261.png');
        game.load.image('number10right', needUrlPath + '/img/game1/lineNumbers/273.png');

        /* линии */
        game.load.image('linefull1', needUrlPath + '/img/game1/lines/linefull1.png');
        game.load.image('linefull2', needUrlPath + '/img/game1/lines/linefull2.png');
        game.load.image('linefull3', needUrlPath + '/img/game1/lines/linefull3.png');
        game.load.image('linefull4', needUrlPath + '/img/game1/lines/linefull4.png');
        game.load.image('linefull5', needUrlPath + '/img/game1/lines/linefull5.png');
        game.load.image('linefull6', needUrlPath + '/img/game1/lines/linefull6.png');
        game.load.image('linefull7', needUrlPath + '/img/game1/lines/linefull7.png');
        game.load.image('linefull8', needUrlPath + '/img/game1/lines/linefull8.png');
        game.load.image('linefull9', needUrlPath + '/img/game1/lines/linefull9.png');
        game.load.image('linefull10', needUrlPath + '/img/game1/lines/linefull10.png');

        /* звуки слотов */
        game.load.audio('rotateSound', needUrlPath + '/sound/game1/rotateSound.mp3');
        game.load.audio('stopSound', needUrlPath + '/sound/game1/stopSound.mp3');
        //game.load.audio('tada', needUrlPath + '/sound/game1/tada.wav');
        //game.load.audio('play', needUrlPath + '/sound/game1/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/game1/takeWin.mp3');
        game.load.audio('win', needUrlPath + '/sound/game1/win.mp3');
        //game.load.audio('winMonkey', needUrlPath + '/sound/game1/winBonusGame.mp3'); //выпадение бонусной игры

        /* звуки нажатия кнопок выбора линий */
        /*game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');*/

        /* звуки выигрышных линий */
        game.load.audio('soundWinLine1', needUrlPath + '/sound/game1/randomWinLineSounds_first/2_sound8.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/game1/randomWinLineSounds_first/3_sound7.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/game1/randomWinLineSounds_first/4_sound6.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/game1/randomWinLineSounds_first/5_sound5.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/game1/randomWinLineSounds_first/9_sound2.mp3');

        /* значения слотов */
        game.load.image('cellShadow', needUrlPath + '/img/game1/slotValues/cellShadow.png');
        game.load.image('cell0', needUrlPath + '/img/game1/slotValues/cell0.jpg');
        game.load.image('cell1', needUrlPath + '/img/game1/slotValues/cell1.jpg');
        game.load.image('cell2', needUrlPath + '/img/game1/slotValues/cell2.jpg');
        game.load.image('cell3', needUrlPath + '/img/game1/slotValues/cell3.jpg');
        game.load.image('cell4', needUrlPath + '/img/game1/slotValues/cell4.jpg');
        game.load.image('cell5', needUrlPath + '/img/game1/slotValues/cell5.jpg');
        game.load.image('cell6', needUrlPath + '/img/game1/slotValues/cell6.jpg');
        game.load.image('cell7', needUrlPath + '/img/game1/slotValues/cell7.jpg');
        game.load.spritesheet('cellAnims', needUrlPath + '/img/game1/slotValues/cellAnims.png', 222, 222); //анимация вращения слота
        //game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/slotValues/selectionOfTheManyCellAnim.png', 96, 96); //анимированое значение слота перехода в третью игру

        game.load.image('slot1bg', needUrlPath + '/img/game1/slotValues/slot1bg.png');
        game.load.image('slot2bg', needUrlPath + '/img/game1/slotValues/slot2bg.png');
        game.load.image('slot3bg', needUrlPath + '/img/game1/slotValues/slot3bg.png');
        game.load.image('slot4bg', needUrlPath + '/img/game1/slotValues/slot4bg.png');
        game.load.image('slot5bg', needUrlPath + '/img/game1/slotValues/slot5bg.png');
        game.load.image('slot6bg', needUrlPath + '/img/game1/slotValues/slot6bg.png');
        game.load.image('slot7bg', needUrlPath + '/img/game1/slotValues/slot7bg.png');
        game.load.image('slot8bg', needUrlPath + '/img/game1/slotValues/slot8bg.png');
        game.load.image('slot9bg', needUrlPath + '/img/game1/slotValues/slot9bg.png');
        game.load.image('slot10bg', needUrlPath + '/img/game1/slotValues/slot10bg.png');
        game.load.image('slot11bg', needUrlPath + '/img/game1/slotValues/slot11bg.png');
        game.load.image('slot12bg', needUrlPath + '/img/game1/slotValues/slot12bg.png');
        game.load.image('slot13bg', needUrlPath + '/img/game1/slotValues/slot13bg.png');
        game.load.image('slot14bg', needUrlPath + '/img/game1/slotValues/slot14bg.png');
        game.load.image('slot15bg', needUrlPath + '/img/game1/slotValues/slot15bg.png');


        game.load.spritesheet('cell0Anim', needUrlPath + '/img/game1/slotAnim/cell0Anim.png', 222, 222);
        game.load.spritesheet('cell0_2Anim', needUrlPath + '/img/game1/slotAnim/cell0_2Anim.png', 222, 222);
        game.load.spritesheet('cell1Anim', needUrlPath + '/img/game1/slotAnim/cell1Anim.png', 222, 222);
        game.load.spritesheet('cell1_2Anim', needUrlPath + '/img/game1/slotAnim/cell1_2Anim.png', 222, 222);
        game.load.spritesheet('cell2Anim', needUrlPath + '/img/game1/slotAnim/cell2Anim.png', 222, 222);
        game.load.spritesheet('cell2_2Anim', needUrlPath + '/img/game1/slotAnim/cell2_2Anim.png', 222, 222);
        game.load.spritesheet('cell3Anim', needUrlPath + '/img/game1/slotAnim/cell3Anim.png', 222, 222);
        game.load.spritesheet('cell3_2Anim', needUrlPath + '/img/game1/slotAnim/cell3_2Anim.png', 222, 222);
        game.load.spritesheet('cell4Anim', needUrlPath + '/img/game1/slotAnim/cell4Anim.png', 222, 222);
        game.load.spritesheet('cell4_2Anim', needUrlPath + '/img/game1/slotAnim/cell4_2Anim.png', 222, 222);
        game.load.spritesheet('cell5Anim', needUrlPath + '/img/game1/slotAnim/cell5Anim.png', 222, 222);
        game.load.spritesheet('cell5_2Anim', needUrlPath + '/img/game1/slotAnim/cell5_2Anim.png', 222, 222);
        game.load.spritesheet('cell6Anim', needUrlPath + '/img/game1/slotAnim/cell6Anim.png', 222, 222);
        game.load.spritesheet('cell7Anim', needUrlPath + '/img/game1/slotAnim/cell7Anim.png', 222, 222);
        game.load.spritesheet('cell7_2Anim', needUrlPath + '/img/game1/slotAnim/cell7_2Anim.png', 222, 222);
        game.load.spritesheet('cell8Anim', needUrlPath + '/img/game1/slotAnim/cell8Anim.png', 222, 222);
        game.load.spritesheet('cell8_2Anim', needUrlPath + '/img/game1/slotAnim/cell8_2Anim.png', 222, 222);
        game.load.spritesheet('cell9Anim', needUrlPath + '/img/game1/slotAnim/cell9Anim.png', 222, 222);
        game.load.spritesheet('cell9_2Anim', needUrlPath + '/img/game1/slotAnim/cell9_2Anim.png', 222, 222);
        game.load.spritesheet('cell9_3Anim', needUrlPath + '/img/game1/slotAnim/cell9_3Anim.png', 222, 222);
        game.load.spritesheet('cell10_1Anim', needUrlPath + '/img/game1/slotAnim/cell10_1Anim.png', 222, 222);
        game.load.spritesheet('cell10_2Anim', needUrlPath + '/img/game1/slotAnim/cell10_2Anim.png', 222, 222);
        game.load.spritesheet('cell10_3Anim', needUrlPath + '/img/game1/slotAnim/cell10_3Anim.png', 222, 222);
        game.load.spritesheet('cell11_1Anim_long', needUrlPath + '/img/game1/slotAnim/cell11_1Anim_long.png', 222, 222);
        game.load.spritesheet('cell11_1Anim_short', needUrlPath + '/img/game1/slotAnim/cell11_1Anim_short.png', 222, 222);
        game.load.spritesheet('cell11_2Anim_long', needUrlPath + '/img/game1/slotAnim/cell11_2Anim_long.png', 222, 222);
        game.load.spritesheet('cell11_2Anim_short', needUrlPath + '/img/game1/slotAnim/cell11_2Anim_short.png', 222, 222);
        game.load.spritesheet('cell11_3Anim_long', needUrlPath + '/img/game1/slotAnim/cell11_3Anim_long.png', 222, 222);
        game.load.spritesheet('cell11_4Anim_long', needUrlPath + '/img/game1/slotAnim/cell11_4Anim_long.png', 222, 222);
        game.load.spritesheet('cell12Anim', needUrlPath + '/img/game1/slotAnim/cell12Anim.png', 222, 222);


        game.load.image('square1', needUrlPath + '/img/game1/square/square_line1.png');
        game.load.image('square2', needUrlPath + '/img/game1/square/square_line2.png');
        game.load.image('square3', needUrlPath + '/img/game1/square/square_line3.png');
        game.load.image('square4', needUrlPath + '/img/game1/square/square_line4.png');
        game.load.image('square5', needUrlPath + '/img/game1/square/square_line5.png');
        game.load.image('square6', needUrlPath + '/img/game1/square/square_line6.png');
        game.load.image('square7', needUrlPath + '/img/game1/square/square_line7.png');
        game.load.image('square8', needUrlPath + '/img/game1/square/square_line8.png');
        game.load.image('square9', needUrlPath + '/img/game1/square/square_line9.png');
        game.load.image('square10', needUrlPath + '/img/game1/square/square_line10.png');


        //карты
        game.load.image('m1', needUrlPath + '/img/game2/cards/m1.png');
        game.load.image('m2', needUrlPath + '/img/game2/cards/m2.png');
        game.load.image('m3', needUrlPath + '/img/game2/cards/m3.png');
        game.load.image('m4', needUrlPath + '/img/game2/cards/m4.png');

        game.load.image('card1', needUrlPath + '/img/game2/cards/card1.png');
        game.load.image('card2', needUrlPath + '/img/game2/cards/card2.png');
        game.load.image('card3', needUrlPath + '/img/game2/cards/card3.png');
        game.load.image('card4', needUrlPath + '/img/game2/cards/card4.png');

        game.load.image('status', needUrlPath + '/img/desktopButtons/status.png');
        game.load.image('shadow_btn', needUrlPath + '/img/desktopButtons/shadow_btn.png');
        
        //кнопки для delux версии
        game.load.spritesheet('startAnim', needUrlPath + '/img/desktopButtons/startAnim.png', 178, 52);

        game.load.image('autoplay', needUrlPath + '/img/desktopButtons/autoplay.png');
        game.load.image('autoplay_d', needUrlPath + '/img/desktopButtons/autoplay_d.png');
        game.load.image('autoplay_h', needUrlPath + '/img/desktopButtons/autoplay_h.png');
        game.load.image('autoplay_p', needUrlPath + '/img/desktopButtons/autoplay_p.png');

        game.load.image('black', needUrlPath + '/img/desktopButtons/black.png');
        game.load.image('black_p', needUrlPath + '/img/desktopButtons/black_p.png');
        game.load.image('black_f', needUrlPath + '/img/desktopButtons/black_f.png');
        game.load.image('black_d', needUrlPath + '/img/desktopButtons/black_d.png');

        game.load.image('gamble', needUrlPath + '/img/desktopButtons/gamble.png');
        game.load.image('gamble_f', needUrlPath + '/img/desktopButtons/gamble_f.png');
        game.load.image('gamble_h', needUrlPath + '/img/desktopButtons/gamble_h.png');
        game.load.image('gamble_p', needUrlPath + '/img/desktopButtons/gamble_p.png');

        game.load.image('minus', needUrlPath + '/img/desktopButtons/minus.png');
        game.load.image('minus_d', needUrlPath + '/img/desktopButtons/minus_d.png');
        game.load.image('minus_h', needUrlPath + '/img/desktopButtons/minus_h.png');
        game.load.image('minus_p', needUrlPath + '/img/desktopButtons/minus_p.png');

        game.load.image('paytable', needUrlPath + '/img/desktopButtons/paytable.png');
        game.load.image('paytable_d', needUrlPath + '/img/desktopButtons/paytable_d.png');
        game.load.image('paytable_h', needUrlPath + '/img/desktopButtons/paytable_h.png');
        game.load.image('paytable_p', needUrlPath + '/img/desktopButtons/paytable_p.png');

        game.load.image('plus', needUrlPath + '/img/desktopButtons/plus.png');
        game.load.image('plus_d', needUrlPath + '/img/desktopButtons/plus_d.png');
        game.load.image('plus_h', needUrlPath + '/img/desktopButtons/plus_h.png');
        game.load.image('plus_p', needUrlPath + '/img/desktopButtons/plus_p.png');

        game.load.image('red', needUrlPath + '/img/desktopButtons/red.png');
        game.load.image('red_d', needUrlPath + '/img/desktopButtons/red_d.png');
        game.load.image('red_f', needUrlPath + '/img/desktopButtons/red_f.png');
        game.load.image('red_p', needUrlPath + '/img/desktopButtons/red_p.png');

        game.load.image('spin', needUrlPath + '/img/desktopButtons/spin.png');
        game.load.image('spin_d', needUrlPath + '/img/desktopButtons/spin_d.png');
        game.load.image('spin_h', needUrlPath + '/img/desktopButtons/spin_h.png');
        game.load.image('spin_p', needUrlPath + '/img/desktopButtons/spin_p.png');

        game.load.image('start', needUrlPath + '/img/desktopButtons/start.png');
        game.load.image('start_d', needUrlPath + '/img/desktopButtons/start_d.png');
        game.load.image('start_h', needUrlPath + '/img/desktopButtons/start_h.png');
        game.load.image('start_p', needUrlPath + '/img/desktopButtons/start_p.png');
        game.load.image('start_f', needUrlPath + '/img/desktopButtons/start_f.png');

        game.load.image('status', needUrlPath + '/img/desktopButtons/status.png');
        game.load.image('status_2', needUrlPath + '/img/desktopButtons/status_2.png');


        /* *
         *  файлы с произвольными именами переменных
         *
         * */

        game.load.image('border', needUrlPath + '/img/game1/slot-background.jpg'); //внешняя часть автомата (слота)

        game.load.image('backgroundGame2', needUrlPath + '/img/game2/background_game2.jpg');
        
        game.load.spritesheet('cardAnim', needUrlPath + '/img/game2/cardAnim.png', 230, 328);

        game.load.audio('sound16', needUrlPath + '/sound/game2/16_sound15.mp3');

        game.load.image('redButtonOnScreen', needUrlPath + '/img/game2/658.png');
        game.load.image('redButtonOnScreen_d', needUrlPath + '/img/game2/651.png');
        game.load.image('redButtonOnScreen_p', needUrlPath + '/img/game2/661.png');
        game.load.image('blackButtonOnScreen', needUrlPath + '/img/game2/655.png');
        game.load.image('blackButtonOnScreen_d', needUrlPath + '/img/game2/647.png');
        game.load.image('blackButtonOnScreen_p', needUrlPath + '/img/game2/666.png');
        game.load.image('redButtonOnScreen_f', needUrlPath + '/img/game2/5971.png');
        game.load.image('blackButtonOnScreen_f', needUrlPath + '/img/game2/5972.png');

        game.load.image('buttonRed', needUrlPath + '/img/desktopButtons/red.png');
        game.load.image('buttonRed_d', needUrlPath + '/img/desktopButtons/red_d.png');
        game.load.image('buttonRed_p', needUrlPath + '/img/desktopButtons/red_p.png');
        game.load.image('buttonBlack', needUrlPath + '/img/desktopButtons/black.png');
        game.load.image('buttonBlack_d', needUrlPath + '/img/desktopButtons/black_d.png');
        game.load.image('buttonBlack_p', needUrlPath + '/img/desktopButtons/black_p.png');

        game.load.image('collect', needUrlPath + '/img/desktopButtons/collect.png');
        game.load.image('collect_d', needUrlPath + '/img/desktopButtons/collect_d.png');
        game.load.image('collect_p', needUrlPath + '/img/desktopButtons/collect_p.png');
        game.load.image('collect_h', needUrlPath + '/img/desktopButtons/collect_h.png');

        game.load.spritesheet('redButtonAnim', needUrlPath + '/img/desktopButtons/redButtonAnim.png', 170, 52);
        game.load.spritesheet('blackButtonAnim', needUrlPath + '/img/desktopButtons/blackButtonAnim.png', 170, 51);
        game.load.spritesheet('collectButtonAnim', needUrlPath + '/img/desktopButtons/collectButtonAnim.png', 178, 52);

        game.load.audio('autoplay_start', needUrlPath + '/sound/game1/autoplay_start.mp3');
        game.load.audio('autoplay_stop', needUrlPath + '/sound/game1/autoplay_stop.mp3');
        game.load.audio('press_bet', needUrlPath + '/sound/game1/press_bet.mp3');

        game.load.image('gamble', needUrlPath + '/img/desktopButtons/gamble.png');
        game.load.image('gamble_d', needUrlPath + '/img/desktopButtons/gamble_d.png');
        game.load.image('gamble_h', needUrlPath + '/img/desktopButtons/gamble_h.png');
        game.load.image('gamble_p', needUrlPath + '/img/desktopButtons/gamble_p.png');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

