function game4() {
  (function () {

    var button;

    var game4 = {
      buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
      selectedDoor : null,
      freeze : false,
    };

    game4.preload = function () {

    };

    game4.create = function () {

            //звуки
            winCover = game.add.audio('winCover');


            //изображения
            //Добавление фона



            //кнопки и анимации событий нажатия

            //для данной игры в каждую функцию вносится по две анимации (win и lose) для каждой кнопки
            selectionsAmin = [];
            timeout1Game4ToGame1 = 6000;
            timeout2Game4ToGame1 = 4000;

            // анимация для кнопки 3
            selectionsAmin[0] = function (ropeValues, ropeStep) {
              game4.select_door.position.x = 71;
              game4.soldierhead.visible = false;
              game4.soldier.visible = false;
              game4.select_door.visible = true;
              codeDoorSound.play();
              game4.select_doorAnim.play().onComplete.add(function(){

                if(ropeValues[5] != 0){
                 game4.soldier_win.position.x = 71;                    
                 game4.doorLeft.visible = false; 
                 game4.green_door.visible = true; 
                 game4.girl.visible = true;    
                 game4.eyes_animationAnim.play()
                 game4.eyes_animation.visible = true; 
                 game4.bustAnim.play()
                 game4.bust.visible = true; 
                 game4.one_heartsAnim.play()
                 game4.one_hearts.visible = true; 
                 game4.two_heartsAnim.play()
                 game4.two_hearts.visible = true;
                 game4.buttonleft.visible = true;
                 game4.soldier_win.visible = true;
                 game4.soldier_winAnimation.play(); 
                 game4.select_door.visible = false;
                 game4_winSound.play();
               } else {
                 game4.doorLeft.visible = false; 
                 game4.green_door.visible = true; 
                 game4.security.position.x = 238;
                 game4.security.visible = true;
                 game4.securityAnim.play()
                 game4.buttonleft.visible = true;
                 game4.soldier_lose.visible = true;
                 game4.soldier_loseAnimation.play();
                 game4.select_door.visible = false; 
                 game4_loseSound.play();
               }
             });  
            };

            // анимация для кнопки 7
            selectionsAmin[1] = function (ropeValues, ropeStep) {
             game4.select_door.position.x = 392;
             game4.soldierhead.visible = false;
             game4.soldier.visible = false;
             game4.select_door.visible = true; 
             codeDoorSound.play();
             game4.green_door.position.x = 493;
             game4.select_doorAnim.play().onComplete.add(function(){
              if(ropeValues[5] != 0){
               game4.soldier_win.position.x = 392;                    
               game4.doorRight.visible = false; 
               game4.green_door.visible = true; 
               game4.girl.position.x = 575;   
               game4.girl.visible = true;    
               game4.eyes_animation.position.x = 575;
               game4.eyes_animation.visible = true; 
               game4.eyes_animationAnim.play()
               game4.bust.position.x = 591;
               game4.bust.visible = true; 
               game4.bustAnim.play()
               game4.one_hearts.position.x = 639;
               game4.one_hearts.visible = true; 
               game4.one_heartsAnim.play()
               game4.two_hearts.position.x = 620;
               game4.two_hearts.visible = true;
               game4.two_heartsAnim.play()
               game4.buttonright.visible = true;
               game4.soldier_win.visible = true;
               game4.soldier_winAnimation.play(); 
               game4.select_door.visible = false;
               game4_winSound.play();
             } else {
               game4.doorRight.visible = false; 
               game4.green_door.visible = true; 
               game4.security.visible = true;
               game4.securityAnim.play()
               game4.buttonright.visible = true;
               game4.soldier_lose.position.x = 392;
               game4.soldier_lose.visible = true;
               game4.soldier_loseAnimation.play();
               game4.select_door.visible = false; 
               game4_loseSound.play();
             }
           });
           };






            //анимации
            codeDoorSound = game.add.audio('codeDoor');
            game4_winSound = game.add.audio('game4_win');
            game4_loseSound = game.add.audio('game4_lose');

            game4.bg = game.add.sprite(94,54, 'game4.bg');
            game4.bg_top = game.add.sprite(95,22, 'game2.bg_top1');



            game4.buttonleft = game.add.sprite(222,54, 'game4.buttonleft');
            game4.buttonleft.visible = false;
            game4.buttonright = game.add.sprite(542,54, 'game4.buttonright');
            game4.buttonright.visible = false;
            game4.girl = game.add.sprite(255,183, 'game4.girl');
            game4.girl.visible = false;
            game4.doorLeft = game.add.sprite(238,166, 'game4.door');
            game4.doorLeft.visible = true;
            game4.doorRight = game.add.sprite(558,166, 'game4.door');
            game4.doorRight.visible = true;

            game4.green_door = game.add.sprite(173,255, 'game4.green_door');
            game4.green_door.visible = false;

            game4.soldierhead = game.add.sprite(304, 262, 'game2.soldierhead_animation');
            game4.soldierheadAnimation = game4.soldierhead.animations.add('game4.soldierhead', [0,1,2,3], 5, false);
            game4.soldierhead.visible = false;
            game4.soldier = game.add.sprite(304, 262, 'game2.soldier_animation');
            game4.soldierAnimation = game4.soldier.animations.add('game2.soldier', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
            game4.soldier.visible = true;
            game4.soldierAnimation.play();
            game4.soldierAnimation.onComplete.add(function(){
              game4.soldierhead.visible = true;
              game4.soldier.visible = false;
              game4.soldierheadAnimation.play();
            });
            game4.soldierheadAnimation.onComplete.add(function(){
              game4.soldierhead.visible = false;
              game4.soldier.visible = true;
              game4.soldierAnimation.play();
            });
            game4.eyes_animation = game.add.sprite(255, 183, 'game4.eyes_animation');
            game4.eyes_animationAnim = game4.eyes_animation.animations.add('game4.eyes_animation', [0,1,2,3], 10, true);
            game4.eyes_animation.visible = false;
            game4.security = game.add.sprite(558, 183, 'game4.security');
            game4.securityAnim = game4.security.animations.add('game4.security', [0,1,2,3,4,5,6,7,8], 5, false);
            game4.security.visible = false;
            game4.select_door = game.add.sprite(71, 198, 'game4.select_door');
            // game4.select_door = game.add.sprite(382, 198, 'game4.select_door');
            game4.select_doorAnim = game4.select_door.animations.add('game4.select_door', [0,1,2,3,4,5,6,7,8], 6, false);
            game4.select_door.visible = false;
            game4.bust = game.add.sprite(271, 231, 'game4.bust');
            game4.bustAnim = game4.bust.animations.add('game4.bust', [0,1,2,3,4,5,6], 5, true);
            game4.bust.visible = false;
            game4.one_hearts = game.add.sprite(319, 221, 'game4.one_hearts');
            game4.one_heartsAnim = game4.one_hearts.animations.add('game4.one_hearts', [5,6,7,8,9,1,2,3,4], 10, true);
            game4.one_hearts.visible = false;
            game4.two_hearts = game.add.sprite(300, 241, 'game4.two_hearts');
            game4.two_heartsAnim = game4.two_hearts.animations.add('game4.two_hearts', [0,1,2,3,4,5,6,7,8,9], 10, true);
            game4.two_hearts.visible = false;
            game4.soldier_lose = game.add.sprite(71, 198, 'game2.soldier_lose');
            game4.soldier_loseAnimation = game4.soldier_lose.animations.add('game2.soldier_lose', [0,1], 5, false);
            game4.soldier_lose.visible = false;
            game4.soldier_win = game.add.sprite(71, 198, 'game2.soldier_win');
            game4.soldier_winAnimation = game4.soldier_win.animations.add('game2.soldier_win', [0,1,2,3], 5, false);
            game4.soldier_win.visible = false;

            background = game.add.sprite(0,0, 'game.background');
            addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1);

            //счет
            scorePosions = [[260, 26, 20], [435, 26, 20], [610, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, stepTotalWinR, balance, betline);

            full_and_sound();
          };

          game4.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
            }
            else
            {
             full.loadTexture('game.non_full');
             fullStatus = false;
           }
         };

         game.state.add('game4', game4);

       })();


     }