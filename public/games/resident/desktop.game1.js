var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');



        // изображения
        topBarImage = game.add.sprite(95,22, 'game1.bg_top2');

        game.add.sprite(0,0, 'game.background');

        game1.bg = game.add.sprite(94,86, 'game1.bg');

        var xxx = game1.bg_top = game.add.sprite(95,22, 'game1.bg_top1');

        slotPosition = [[143, 87], [143, 193], [143, 299], [254, 87], [254, 193], [254, 299], [365, 87], [365, 193], [365, 299], [477, 87], [477, 193], [477, 299], [589, 87], [589, 193], [589, 299]];
        addSlots(game, slotPosition);

        group1 = game.add.group();
        group1.add(game1.bg);
        game.world.bringToTop(group1);

        addTableTitle(game, 'play1To',543,440);



        var linePosition = [[134,199+50], [134,71+33], [134,322+64], [134,130+33], [134,95+34], [134,102+33], [134,228+34], [134,226+50], [134,120+34]];
        var numberPosition = [[93,183+50], [93,54+33], [93,310+64], [93,118+33], [93,246+64], [93,86+33], [93,278+64], [93,214+50], [93,150+50]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);



        // кнопки
        addButtonsGame1(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230, 26, 20], [435, 26, 20], [610, 26, 20], [99, 59, 16], [707, 59, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        game1.page = game.add.sprite(238, 390, 'page');
        game1.pageAnimation = game1.page.animations.add('page', [0,1,2,3,4,5,6,7], 5, true).play();

        game1.smoke = game.add.sprite(94, 406, 'smoke');
        game1.smokeAnimation = game1.smoke.animations.add('smoke', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 10, true).play();

        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
   if(bet >= extralife && checkHelm == false) {
            //появление огнетушителя
            big_fire_emergence = game.add.sprite(94, 422, 'big_fire_emergence');
            big_fire_emergence.animations.add('big_fire_emergence', [0,1,2,3], 10, false).play();


            checkHelm = true;
        }
        
        if(bet < extralife && checkHelm == true) {
            checkHelm = false;

            big_fire_emergence.visible = false;

            big_fire_emergence = game.add.sprite(94, 422, 'big_fire_emergence');
            big_fire_emergence.animations.add('big_fire_emergence', [2,1,0], 10, false).play().onComplete.add(function(){
                big_fire_emergence.visible = false;
            });
        }
        
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        /*if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
            if(checkHelm == true) {
                function hideMonkey(){
                    monkey1.visible = false;
                    monkey2.visible = false;
                    monkey3.visible = false;
                    monkey4.visible = false;
                    monkey5.visible = false;
                }

                hideMonkey();

                monkeyTakeMushroom1 = game.add.sprite(195,359, 'game.monkeyTakeMushroom1');
                monkeyTakeMushroom1.animations.add('monkeyTakeMushroom1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 10, false);
                monkeyTakeMushroom1.visible = false;

                monkeyTakeMushroom2 = game.add.sprite(195,359, 'game.monkeyTakeMushroom2');
                monkeyTakeMushroom2.animations.add('monkeyTakeMushroom2', [1,2,3,4], 10, false);
                monkeyTakeMushroom2.visible = false;

                function monkeyTakeMushroomAnim() {
                    monkeyTakeMushroom1.visible = true;
                    monkeyTakeMushroom1.animations.getAnimation('monkeyTakeMushroom1').play().onComplete.add(function(){
                        monkeyTakeMushroom1.visible = false;

                        monkeyTakeMushroom2.visible = true;
                        monkeyTakeMushroom2.animations.getAnimation('monkeyTakeMushroom2').play();
                    });
                }

                monkeyTakeMushroomAnim();
            }
        }*/
    };

    game.state.add('game1', game1);

};
