function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        game.add.sprite(0,0, 'game.background');
        game2.bg = game.add.sprite(94,86, 'game1.bg');
        game2.bg2 = game.add.sprite(94,54, 'game2.bg');
        game2.bg_top = game.add.sprite(95,22, 'game2.bg_top1');

        game.add.sprite(141, 296, 'dealer');

        addTableTitle(game, 'winTitleGame2', 543,440);
        hideTableTitle();
        
        // кнопки
        addButtonsGame2(game);


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 84, 30]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[113+15,135], [253,129], [365,129], [480,129], [592,129]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация

        game2.soldierhead = game.add.sprite(304, 262, 'game2.soldierhead_animation');
        game2.soldierheadAnimation = game2.soldierhead.animations.add('game2.soldierhead', [0,1,2,3], 5, false);
        game2.soldierhead.visible = false;
        game2.soldier = game.add.sprite(304, 262, 'game2.soldier_animation');
        game2.soldierAnimation = game2.soldier.animations.add('game2.soldier', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
        game2.soldier.visible = true;
        game2.soldierAnimation.onComplete.add(function(){
            game2.soldierhead.visible = true;
            game2.soldier.visible = false;
            game2.soldierheadAnimation.play();
        });
        game2.soldierheadAnimation.onComplete.add(function(){
            game2.soldierhead.visible = false;
            game2.soldier.visible = true;
            game2.soldierAnimation.play();
        });
        game2.soldierAnimation.play();

        fontsize = 16;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 547, 445, fontsize);

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}