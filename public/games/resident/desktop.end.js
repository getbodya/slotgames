(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        game.load.image('game.background', needUrlPath + '/img/canvas-bg.svg');

        game.load.image('startButton', needUrlPath + '/img/image1445.png');
        game.load.image('startButton_p', needUrlPath + '/img/image1447.png');
        game.load.image('startButton_d', needUrlPath + '/img/image1451.png');
        game.load.image('selectGame', needUrlPath + '/img/image1419.png');
        game.load.image('selectGame_p', needUrlPath + '/img/image1421.png');
        game.load.image('selectGame_d', needUrlPath + '/img/image1425.png');
        game.load.image('payTable', needUrlPath + '/img/image1428.png');
        game.load.image('payTable_p', needUrlPath + '/img/image1430.png');
        game.load.image('payTable_d', needUrlPath + '/img/image1433.png');
        game.load.image('automaricstart', needUrlPath + '/img/image1436.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/image1438.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/image1442.png');
        game.load.image('betone', needUrlPath + '/img/image1471.png');
        game.load.image('betone_p', needUrlPath + '/img/image1473.png');
        game.load.image('betone_d', needUrlPath + '/img/image1477.png');
        game.load.image('betmax', needUrlPath + '/img/image1480.png');
        game.load.image('betmax_p', needUrlPath + '/img/image1482.png');
        game.load.image('betmax_d', needUrlPath + '/img/image1485.png');
        game.load.image('buttonLine1', needUrlPath + '/img/image1505.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/image1507.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/image1511.png');
        game.load.image('buttonLine3', needUrlPath + '/img/image1496.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/image1498.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/image1502.png');
        game.load.image('buttonLine5', needUrlPath + '/img/image1488.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/image1490.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/image1493.png');
        game.load.image('buttonLine7', needUrlPath + '/img/image1462.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/image1464.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/image1468.png');
        game.load.image('buttonLine9', needUrlPath + '/img/image1454.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/image1456.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/image1459.png');

        game.load.image('game.number1', needUrlPath + '/img/win_1.png');
        game.load.image('game.number2', needUrlPath + '/img/win_2.png');
        game.load.image('game.number3', needUrlPath + '/img/win_3.png');
        game.load.image('game.number4', needUrlPath + '/img/win_4.png');
        game.load.image('game.number5', needUrlPath + '/img/win_5.png');
        game.load.image('game.number6', needUrlPath + '/img/win_6.png');
        game.load.image('game.number7', needUrlPath + '/img/win_7.png');
        game.load.image('game.number8', needUrlPath + '/img/win_8.png');
        game.load.image('game.number9', needUrlPath + '/img/win_9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.image('line1', needUrlPath + '/lines/select/1.png');
        game.load.image('line2', needUrlPath + '/lines/select/2.png');
        game.load.image('line3', needUrlPath + '/lines/select/3.png');
        game.load.image('line4', needUrlPath + '/lines/select/4.png');
        game.load.image('line5', needUrlPath + '/lines/select/5.png');
        game.load.image('line6', needUrlPath + '/lines/select/6.png');
        game.load.image('line7', needUrlPath + '/lines/select/7.png');
        game.load.image('line8', needUrlPath + '/lines/select/8.png');
        game.load.image('line9', needUrlPath + '/lines/select/9.png');

        game.load.image('linefull1', needUrlPath + '/lines/win/1.png');
        game.load.image('linefull2', needUrlPath + '/lines/win/2.png');
        game.load.image('linefull3', needUrlPath + '/lines/win/3.png');
        game.load.image('linefull4', needUrlPath + '/lines/win/4.png');
        game.load.image('linefull5', needUrlPath + '/lines/win/5.png');
        game.load.image('linefull6', needUrlPath + '/lines/win/6.png');
        game.load.image('linefull7', needUrlPath + '/lines/win/7.png');
        game.load.image('linefull8', needUrlPath + '/lines/win/8.png');
        game.load.image('linefull9', needUrlPath + '/lines/win/9.png');

        game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sound/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sound/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sound/stop.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('play', needUrlPath + '/sound/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sound/winLines/sound19.mp3');

        //карты
        game.load.image('card_bg', needUrlPath + '/img/cards/shirt_cards.png');

        game.load.image('card_39', needUrlPath + '/img/cards/2b.png');
        game.load.image('card_40', needUrlPath + '/img/cards/3b.png');
        game.load.image('card_41', needUrlPath + '/img/cards/4b.png');
        game.load.image('card_42', needUrlPath + '/img/cards/5b.png');
        game.load.image('card_43', needUrlPath + '/img/cards/6b.png');
        game.load.image('card_44', needUrlPath + '/img/cards/7b.png');
        game.load.image('card_45', needUrlPath + '/img/cards/8b.png');
        game.load.image('card_46', needUrlPath + '/img/cards/9b.png');
        game.load.image('card_47', needUrlPath + '/img/cards/10b.png');
        game.load.image('card_48', needUrlPath + '/img/cards/11b.png');
        game.load.image('card_49', needUrlPath + '/img/cards/12b.png');
        game.load.image('card_50', needUrlPath + '/img/cards/13b.png');
        game.load.image('card_51', needUrlPath + '/img/cards/14b.png');

        game.load.image('card_26', needUrlPath + '/img/cards/2c.png');
        game.load.image('card_27', needUrlPath + '/img/cards/3c.png');
        game.load.image('card_28', needUrlPath + '/img/cards/4c.png');
        game.load.image('card_29', needUrlPath + '/img/cards/5c.png');
        game.load.image('card_30', needUrlPath + '/img/cards/6c.png');
        game.load.image('card_31', needUrlPath + '/img/cards/7c.png');
        game.load.image('card_32', needUrlPath + '/img/cards/8c.png');
        game.load.image('card_33', needUrlPath + '/img/cards/9c.png');
        game.load.image('card_34', needUrlPath + '/img/cards/10c.png');
        game.load.image('card_35', needUrlPath + '/img/cards/11c.png');
        game.load.image('card_36', needUrlPath + '/img/cards/12c.png');
        game.load.image('card_37', needUrlPath + '/img/cards/13c.png');
        game.load.image('card_38', needUrlPath + '/img/cards/14c.png');

        game.load.image('card_0', needUrlPath + '/img/cards/2t.png');
        game.load.image('card_1', needUrlPath + '/img/cards/3t.png');
        game.load.image('card_2', needUrlPath + '/img/cards/4t.png');
        game.load.image('card_3', needUrlPath + '/img/cards/5t.png');
        game.load.image('card_4', needUrlPath + '/img/cards/6t.png');
        game.load.image('card_5', needUrlPath + '/img/cards/7t.png');
        game.load.image('card_6', needUrlPath + '/img/cards/8t.png');
        game.load.image('card_7', needUrlPath + '/img/cards/9t.png');
        game.load.image('card_8', needUrlPath + '/img/cards/10t.png');
        game.load.image('card_9', needUrlPath + '/img/cards/11t.png');
        game.load.image('card_10', needUrlPath + '/img/cards/12t.png');
        game.load.image('card_11', needUrlPath + '/img/cards/13t.png');
        game.load.image('card_12', needUrlPath + '/img/cards/14t.png');

        game.load.image('card_13', needUrlPath + '/img/cards/2p.png');
        game.load.image('card_14', needUrlPath + '/img/cards/3p.png');
        game.load.image('card_15', needUrlPath + '/img/cards/4p.png');
        game.load.image('card_16', needUrlPath + '/img/cards/5p.png');
        game.load.image('card_17', needUrlPath + '/img/cards/6p.png');
        game.load.image('card_18', needUrlPath + '/img/cards/7p.png');
        game.load.image('card_19', needUrlPath + '/img/cards/8p.png');
        game.load.image('card_20', needUrlPath + '/img/cards/9p.png');
        game.load.image('card_21', needUrlPath + '/img/cards/10p.png');
        game.load.image('card_22', needUrlPath + '/img/cards/11p.png');
        game.load.image('card_23', needUrlPath + '/img/cards/12p.png');
        game.load.image('card_24', needUrlPath + '/img/cards/13p.png');
        game.load.image('card_25', needUrlPath + '/img/cards/14p.png');

        game.load.image('card_52', needUrlPath + '/img/cards/joker.png');

        game.load.image('cell0', needUrlPath + '/img/cell0.png');
        game.load.image('cell1', needUrlPath + '/img/cell1.png');
        game.load.image('cell2', needUrlPath + '/img/cell2.png');
        game.load.image('cell3', needUrlPath + '/img/cell3.png');
        game.load.image('cell4', needUrlPath + '/img/cell4.png');
        game.load.image('cell5', needUrlPath + '/img/cell5.png');
        game.load.image('cell6', needUrlPath + '/img/cell6.png');
        game.load.image('cell7', needUrlPath + '/img/cell7.png');
        game.load.image('cell8', needUrlPath + '/img/cell8.png');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.jpg', 96, 112);
        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/safe_row_win.png', 96, 112);

        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('bonusGame', needUrlPath + '/img/image536.png');
        game.load.image('takeOrRisk1', needUrlPath + '/img/image474.png');
        game.load.image('take', needUrlPath + '/img/image554.png');

        game.load.image('topScoreGame2', needUrlPath + '/img/main_bg_top1.png');

        game.load.image('winTitleGame2', needUrlPath + '/img/image486.png');
        game.load.image('loseTitleGame2', needUrlPath + '/img/image484.png');
        //game.load.image('forwadTitleGame2', needUrlPath + '/img/image504.png');

        game.load.audio('openCard', needUrlPath + '/sound/sound31.mp3');
        game.load.audio('winCard', needUrlPath + '/sound/sound30.mp3');

        /* sources */

        game.load.image('game1.bg', needUrlPath + '/img/main_bg.png');
        game.load.image('game2.bg', needUrlPath + '/img/bg_game2.png');
        game.load.image('game3.bg', needUrlPath + '/img/bg_game3.png');
        game.load.image('game4.bg', needUrlPath + '/img/bg_game4.png');
        game.load.image('game1.bg_top1', needUrlPath + '/img/main_bg_1.png');
        game.load.image('game1.bg_top2', needUrlPath + '/img/main_bg_2.png');
        game.load.image('game2.bg_top1', needUrlPath + '/img/main_bg_top1.png');
        game.load.image('game2.bg_top2', needUrlPath + '/img/main_bg_top2.png');
        game.load.image('game1.bottom_line', needUrlPath + '/img/bottom_line.png');
        game.load.image('game1.big_fire', needUrlPath + '/img/big_fire.png');

        game.load.image('game3.bottle', needUrlPath + '/img/bottle.png');
        game.load.image('game3.book', needUrlPath + '/img/book.png');
        game.load.image('game3.destroy_door_3', needUrlPath + '/img/destroy_door_3.png');
        game.load.image('game3.big_fire', needUrlPath + '/img/game3_big_fire.png');
        game.load.spritesheet('game3.dynamite', needUrlPath + '/img/dynamite.png', 64, 80, 8);
        game.load.spritesheet('game3.gold', needUrlPath + '/img/gold.png', 64, 80, 3);
        game.load.spritesheet('game3.soldier_win', needUrlPath + '/img/soldier_animation_good_open.png', 208, 240, 3);

        game.load.image('game4.buttonleft', needUrlPath + '/img/buttonleft.png');
        game.load.image('game4.buttonright', needUrlPath + '/img/buttonright.png');
        game.load.image('game4.door', needUrlPath + '/img/door.png');
        game.load.image('game4.girl', needUrlPath + '/img/girl.png');
        game.load.image('game4.green_door', needUrlPath + '/img/green_door.png');
        game.load.spritesheet('game4.eyes_animation', needUrlPath + '/img/eyes_animation.png', 96, 176, 4);
        game.load.spritesheet('game4.one_hearts', needUrlPath + '/img/one_hearts.png', 32, 32, 10);
        game.load.spritesheet('game4.two_hearts', needUrlPath + '/img/two_hearts.png', 48, 32, 10);
        game.load.spritesheet('game4.security', needUrlPath + '/img/security.png', 128, 224, 9);
        game.load.spritesheet('game4.bust', needUrlPath + '/img/bust.png', 64, 48, 7);
        game.load.spritesheet('game4.select_door', needUrlPath + '/img/select_door.png', 144, 240, 9);

        game.load.image('game3.bottle', needUrlPath + '/img/bottle.png');
        game.load.image('game3.book', needUrlPath + '/img/book.png');
        game.load.image('game3.destroy_door_3', needUrlPath + '/img/destroy_door_3.png');
        game.load.image('game3.big_fire', needUrlPath + '/img/game3_big_fire.png');
        game.load.spritesheet('game3.dynamite', needUrlPath + '/img/dynamite.png', 64, 80, 8);
        game.load.spritesheet('game3.gold', needUrlPath + '/img/gold.png', 64, 80, 3);
        game.load.spritesheet('game3.soldier_win', needUrlPath + '/img/soldier_animation_good_open.png', 208, 240, 3);

        game.load.image('game4.buttonleft', needUrlPath + '/img/buttonleft.png');
        game.load.image('game4.buttonright', needUrlPath + '/img/buttonright.png');
        game.load.image('game4.door', needUrlPath + '/img/door.png');
        game.load.image('game4.girl', needUrlPath + '/img/girl.png');
        game.load.image('game4.green_door', needUrlPath + '/img/green_door.png');
        game.load.spritesheet('game4.eyes_animation', needUrlPath + '/img/eyes_animation.png', 96, 176, 4);
        game.load.spritesheet('game4.one_hearts', needUrlPath + '/img/one_hearts.png', 32, 32, 10);
        game.load.spritesheet('game4.two_hearts', needUrlPath + '/img/two_hearts.png', 48, 32, 10);
        game.load.spritesheet('game4.security', needUrlPath + '/img/security.png', 128, 224, 9);
        game.load.spritesheet('game4.bust', needUrlPath + '/img/bust.png', 64, 48, 7);
        game.load.spritesheet('game4.select_door', needUrlPath + '/img/select_door.png', 144, 240, 9);

        game.load.audio('stop', needUrlPath + '/sound/stop.wav');
        game.load.audio('rotate', needUrlPath + '/sound/rotate.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('pickCard', needUrlPath + '/sound/pickCard.mp3');
        game.load.audio('cardWin', needUrlPath + '/sound/cardWin.mp3');
        game.load.audio('hit', needUrlPath + '/sound/hit.mp3');
        game.load.audio('winGame3', needUrlPath + '/sound/winGame3.mp3');
        game.load.audio('dynamite', needUrlPath + '/sound/dynamite.mp3');
        game.load.audio('boom', needUrlPath + '/sound/boom.mp3');
        game.load.audio('winMonkey', needUrlPath + '/sound/safeWin.mp3');
        game.load.audio('codeDoor', needUrlPath + '/sound/codeDoor.mp3');
        game.load.audio('game4_win', needUrlPath + '/sound/game4_win.mp3');
        game.load.audio('game4_lose', needUrlPath + '/sound/game4_lose.mp3');
        game.load.audio('page', needUrlPath + '/sound/page.mp3');

        game.load.spritesheet('smoke', needUrlPath + '/img/smoke.png', 80, 80, 20);
        game.load.spritesheet('page', needUrlPath + '/img/page_animation2.png', 128, 112, 8);
        game.load.spritesheet('safe_row_win', needUrlPath + '/img/safe_row_win.png', 96, 112, 6);
        game.load.spritesheet('game2.soldierhead_animation', needUrlPath + '/img/game2_soldierhead_animation.png', 192, 240, 4);
        game.load.spritesheet('game2.soldier_animation', needUrlPath + '/img/game2_soldier_animation.png', 192, 240, 11);
        game.load.spritesheet('game2.soldier_lose', needUrlPath + '/img/game2_soldier_lose.png', 192, 240, 2);
        game.load.spritesheet('game2.soldier_win', needUrlPath + '/img/game2_soldier_win.png', 192, 240, 4);
        game.load.spritesheet('game3.safe_shine', needUrlPath + '/img/safe_shine.png', 96, 128, 9);
        game.load.spritesheet('game3.hit_safe', needUrlPath + '/img/soldier_animation_safe_open.png', 208, 240, 5);
        game.load.spritesheet('game3.boom_animation', needUrlPath + '/img/boom_animation.png', 208, 240, 11);
        game.load.spritesheet('game3.safe_door', needUrlPath + '/img/safe_door.png', 96, 128, 3);
        game.load.spritesheet('game3.door_knob', needUrlPath + '/img/door_knob.png', 32, 32, 4);

        for (var i = 1; i <= 7; ++i) {
            if (i % 2 != 0) {
                game.load.spritesheet('game3.safe_hit_'+i, needUrlPath + '/img/safe_hit_' +i+ '.png', 112, 128, 3);
                game.load.image('game3.safe_'+ i, needUrlPath + '/img/safe_'+ i +'.png');
            }
        }

        game.load.spritesheet('big_fire_emergence', needUrlPath + '/img/big_fire_emergence.png', 64, 80, 4);

        game.load.image('game1.bg_top2', needUrlPath + '/img/main_bg_2.png');
        game.load.image('dealer', needUrlPath + '/img/dealer.png');

        game.load.image('game3.bottle', needUrlPath + '/img/bottle.png');
        game.load.image('game3.book', needUrlPath + '/img/book.png');
        game.load.image('game3.destroy_door_3', needUrlPath + '/img/destroy_door_3.png');
        game.load.image('game3.big_fire', needUrlPath + '/img/game3_big_fire.png');
        game.load.spritesheet('game3.dynamite', needUrlPath + '/img/dynamite.png', 64, 80, 8);
        game.load.spritesheet('game3.gold', needUrlPath + '/img/gold.png', 64, 80, 3);
        game.load.spritesheet('game3.cup', needUrlPath + '/img/cup.png', 64, 80, 7);
        game.load.spritesheet('game3.dynamite_off', needUrlPath + '/img/dynamite_off.png', 64, 80, 2);
        game.load.spritesheet('game3.soldier_win', needUrlPath + '/img/soldier_animation_good_open.png', 208, 240, 3);

        game.load.image('game4.buttonleft', needUrlPath + '/img/buttonleft.png');
        game.load.image('game4.buttonright', needUrlPath + '/img/buttonright.png');
        game.load.image('game4.door', needUrlPath + '/img/door.png');
        game.load.image('game4.girl', needUrlPath + '/img/girl.png');
        game.load.image('game4.green_door', needUrlPath + '/img/green_door.png');
        game.load.spritesheet('game4.eyes_animation', needUrlPath + '/img/eyes_animation.png', 96, 176, 4);
        game.load.spritesheet('game4.one_hearts', needUrlPath + '/img/one_hearts.png', 32, 32, 10);
        game.load.spritesheet('game4.two_hearts', needUrlPath + '/img/two_hearts.png', 48, 32, 10);
        game.load.spritesheet('game4.security', needUrlPath + '/img/security.png', 128, 224, 9);
        game.load.spritesheet('game4.bust', needUrlPath + '/img/bust.png', 64, 48, 7);
        game.load.spritesheet('game4.select_door', needUrlPath + '/img/select_door.png', 144, 240, 9);

        game.load.audio('stop', needUrlPath + '/sound/stop.wav');
        game.load.audio('rotate', needUrlPath + '/sound/rotate.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('pickCard', needUrlPath + '/sound/pickCard.mp3');
        game.load.audio('cardWin', needUrlPath + '/sound/cardWin.mp3');
        game.load.audio('hit', needUrlPath + '/sound/hit.mp3');
        game.load.audio('winGame3', needUrlPath + '/sound/winGame3.mp3');
        game.load.audio('dynamite', needUrlPath + '/sound/dynamite.mp3');
        game.load.audio('boom', needUrlPath + '/sound/boom.mp3');
        game.load.audio('safeWin', needUrlPath + '/sound/safeWin.mp3');
        game.load.audio('codeDoor', needUrlPath + '/sound/codeDoor.mp3');
        game.load.audio('game4_win', needUrlPath + '/sound/game4_win.mp3');
        game.load.audio('game4_lose', needUrlPath + '/sound/game4_lose.mp3');
        game.load.audio('page', needUrlPath + '/sound/page.mp3');
        game.load.audio('fire_extinguisher_action', needUrlPath + '/sound/fire_extinguisher_action.mp3');
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, needUrlPath + '/img/lines/select/line' + i + '.png');
            game.load.image('linefull_' + i, needUrlPath + '/img/lines/win/linefull' + i + '.png');
            game.load.image('win_' + i, needUrlPath + '/img/win_' + i + '.png');

            if (i % 2 != 0) {
                game.load.audio('line' + i, needUrlPath + '/sound/line' + i + '.wav');
                if(!isMobile){
                    game.load.image('btnline' + i, needUrlPath + '/img/btns/btn' + i + '.png');
                    game.load.image('btnline_p' + i, needUrlPath + '/img/btns/btn' + i + '_p.png');
                    game.load.image('btnline_d' + i, needUrlPath + '/img/btns/btn' + i + '_d.png');
                }
            }
        }

        game.load.spritesheet('smoke', needUrlPath + '/img/smoke.png', 80, 80, 20);
        game.load.spritesheet('page', needUrlPath + '/img/page_animation2.png', 128, 112, 8);
        game.load.spritesheet('big_fire_emergence', needUrlPath + '/img/big_fire_emergence.png', 64, 80, 4);
        game.load.spritesheet('safe_row_win', needUrlPath + '/img/safe_row_win.png', 96, 112, 6);
        game.load.spritesheet('game2.soldierhead_animation', needUrlPath + '/img/game2_soldierhead_animation.png', 192, 240, 4);
        game.load.spritesheet('game2.soldier_animation', needUrlPath + '/img/game2_soldier_animation.png', 192, 240, 11);
        game.load.spritesheet('game2.soldier_lose', needUrlPath + '/img/game2_soldier_lose.png', 192, 240, 2);
        game.load.spritesheet('game2.soldier_win', needUrlPath + '/img/game2_soldier_win.png', 192, 240, 4);
        game.load.spritesheet('game3.safe_shine', needUrlPath + '/img/safe_shine.png', 96, 128, 9);
        game.load.spritesheet('game3.hit_safe', needUrlPath + '/img/soldier_animation_safe_open.png', 208, 240, 5);
        game.load.spritesheet('game3.boom_animation', needUrlPath + '/img/boom_animation.png', 208, 240, 11);
        game.load.spritesheet('game3.safe_door', needUrlPath + '/img/safe_door.png', 96, 128, 3);
        game.load.spritesheet('game3.door_knob', needUrlPath + '/img/door_knob.png', 32, 32, 4);
        game.load.spritesheet('game3.big_fire_extinguisher_action', needUrlPath + '/img/big_fire_extinguisher_action.png', 208, 240, 11);

        game.load.audio('rotate', needUrlPath + '/sound/rotate.wav');
        for (var i = 1; i <= 7; ++i) {
            if (i % 2 != 0) {
                game.load.spritesheet('game3.safe_hit_'+i, needUrlPath + '/img/safe_hit_' +i+ '.png', 112, 128, 3);
                game.load.image('game3.safe_'+ i, needUrlPath + '/img/safe_'+ i +'.png');
            }
        }
        

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

