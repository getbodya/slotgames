function randomNumber(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}
var width = 1024;
var height = 748;

var line;
var point;
var winding = false;
var betline = 1;
var lines = 30;
var gamename = 'alaskanFishing';
var sessionName;
var balance = 99999;

var game1;
var game2;
var game3;
var gamePreload;

var updateStatus = false;
var pressSpinStatus = false;

var checkGame = 2; // индикатор текущей игры
var checkNewGame = true;
var freeSpinAfterBonus = false;
var freeSpinAfterBonusBalance = 0;
var freeSpinAfterBonusAllWinOld = 0;

var game = new Phaser.Game(width, height, Phaser.AUTO, 'game-area');

var ropeValues, balanceOld, balanceR, balance, linesR, betlineR, info, winCellInfo, allWin, allWinOld;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest;

    if (dataArray['state']) {
        ropeValues = dataArray['rope'];

        winCellInfo = dataArray['winCellInfo'];

        balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
        balanceR = dataArray['balance'];
        balance = dataArray['balance']; // получаем реальный баланс

        linesR = dataArray['linesInGame'];
        betlineR = dataArray['betLine'];

        allWin = dataArray['allWin'];

        info = dataArray['info'];
        if (checkGame !== 3){        
            balanceScore.setText(balanceOld - betlineR*linesR);
        }
    }

}


// добавление информации об анимированных ячейка в массивы типа [[name, [x,y], numbersOfFrames], frameFrequency], ...
var animCellAnimInfo = []; // массив в котором будут храниться все анимированные значения ячеек
var cellPosition = [[51, 136], [51, 310], [51, 484],
[238, 136], [238, 310], [238, 484],
[425, 136], [425, 310], [425, 484],
[612, 136], [612, 310], [612, 484],
[799, 136], [799, 310], [799, 484]
];
function addAnimCells() {
    for (var i = 0; i < 15; i++) {
        var infoArray = [];

        for (var k = 0; k < 12; k++) {
            if (k == 0) {
                var array = [];

                array.push(['cellAnim_0_1', cellPosition[i], 22, 10]);
                array.push(['cellAnim_0_2', cellPosition[i], 8, 10]);

                infoArray.push(array);
            } else {
                if(checkGame == 2) {
                    var array = [];

                    array.push(['cellAnim_' + k, cellPosition[i], 22, 10]);

                    infoArray.push(array);
                } else {
                    var array = [];

                    array.push(['cellAnim_' + k + 'fs', cellPosition[i], 22, 10]);

                    infoArray.push(array);
                }
            }
        }
        animCellAnimInfo.push(infoArray);
    }

}

function getNeedUrlPath() {
    if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
        var number = location.pathname.indexOf('/games/');
        var needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname.substring(0,number) + '/';

        return needLocation;
    } else if (location.href.indexOf('public') !== -1 && location.href.indexOf('/game/') !== -1) {
        var number = location.pathname.indexOf('/public/');
        var needLocation = location.href.substring(0,location.href.indexOf('public')) + 'public';

        return needLocation;
    } else if (location.href.indexOf('public') === -1) {
        needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname;

        return needLocation;
    }
}

//показываем анимации ячеек после кручения (выигрышь, либо "демонстрации")

function showAnimCells(winCellInfo) {

    if (ropeValues !== false) {// проверка на бонусные игры
        if (ropeValues == '15freeSpin') { // бонусная игра с бесплаными спинами
            lockDisplay();
            checkLala = 1;
            winCellInfo.forEach(function (item, i) {
                if (item == 11) {
                    playBonusCellAnim(i, 11);
                }
            });
        } else { // бонусная игра с рыбалкой
            lockDisplay();
            winCellInfo.forEach(function (item, i) {
                if (item == 1) {
                    playBonusCellAnim(i, 1);
                }
            });            
            if (checkGame === 3){                
                setTimeout("unlockDisplay(); hideAnimCells(showedAnimArray); game3.fb.stop(); game.state.start('game1');", 2500);
            } else {
                setTimeout("unlockDisplay(); hideAnimCells(showedAnimArray); game.state.start('game1');", 2500);
            }
        }

    } else {
        winCellInfo.forEach(function (item, i) {
            if (item !== false && item !== 'bonusCell12' && item !== 'bonusCell13') {
                playAnimQueue(animCellAnimInfo[i][item]);
            }
        });
    }

}

var bonusCellAnims = [];
function playBonusCellAnim(i, bonusCellNumber) {

    var speed = 10;
    if (bonusCellNumber == 12 || bonusCellNumber == 13) {
        speed = 20;
    }

    if(checkGame == 2) {
        var bonusCellAnim = game.add.sprite(cellPosition[i][0], cellPosition[i][1], 'cellAnim_' + bonusCellNumber);
        bonusCellAnim.animations.add('cellAnim_' + bonusCellNumber, [], speed, false);
        bonusCellAnim.animations.getAnimation('cellAnim_' + bonusCellNumber).play().onComplete.add(function () {
            bonusCellAnim.visible = false;
        });
    } else {
        var bonusCellAnim = game.add.sprite(cellPosition[i][0], cellPosition[i][1], 'cellAnim_' + bonusCellNumber+'fs');
        bonusCellAnim.animations.add('cellAnim_' + bonusCellNumber+'fs', [], speed, false);
        bonusCellAnim.animations.getAnimation('cellAnim_' + bonusCellNumber+'fs').play().onComplete.add(function () {
            bonusCellAnim.visible = false;
        });
    }

    bonusCellAnims.push(bonusCellAnim);
}

function hideAnimCells(showedAnimArray) {
    if (showedAnimArray != false) {
        showedAnimArray.forEach(function (item, i) {
            item.forEach(function (item2, i2) {
                item2.animations.stop();
                item2.visible = false;
            });
        });

        bonusCellAnims.forEach(function (item, i) {
            item.animations.stop();
            item.visible = false;
        });

        bonusCellAnims = [];
    }
}


// функция для создания последовательных анимаций
var sourceArray;// [[name, [x,y], numbersOfFrames, frameFrequency], ...
var showedAnimArray = [];
function playAnimQueue(sourceArray) {

    var animArray = [];

    var countAnim = -1; // номер запускаемой анимации

    sourceArray.forEach(function (anim, index) {

        animArray.push(game.add.sprite(anim[1][0], anim[1][1], anim[0]));

        //создаем массив кадров
        var framesArray = [];
        for (var i = 0; i < anim[2]; i++) {
            framesArray.push(i);
        }

        // создаем анимацию
        animArray[index].animations.add(anim[0], framesArray, anim[3], false);
        animArray[index].animations.getAnimation(anim[0]).play().onComplete.add(function () {
            recursiveFunction(animArray, sourceArray);
        });
        animArray[index].visible = false;

    });

    // создаем функцию, которая будет рекурсивно запускать анимации
    function recursiveFunction(animArray, sourceArray) {

        if ((countAnim + 1) == animArray.length) {
            countAnim = 0;

            animArray[animArray.length - 1].visible = false;
            animArray[countAnim].visible = true;
            animArray[countAnim].animations.getAnimation(sourceArray[countAnim][0]).play();
        } else {
            countAnim += 1;
            if (animArray[countAnim - 1]) {
                animArray[countAnim - 1].visible = false;
            }
            animArray[countAnim].visible = true;

            animArray[countAnim].animations.getAnimation(sourceArray[countAnim][0]).play();
        }

    };

    showedAnimArray.push(animArray);

    recursiveFunction(animArray, sourceArray, countAnim);
}

var betText;
function addScore() {
    betText = game.add.text(100, 100, betline * lines, {
        font: '28px "Arial"',
        fill: '#000000',
        stroke: '#000000',
        strokeThickness: 0,
    });
}
// var fullStatus = false;
// if(isMobile){
//     window.addEventListener("touchend", touchUpdate);
//     function touchUpdate() {
//         if (!game.scale.isFullScreen){
//             game.scale.startFullScreen(false);
//         }
//     }
// }