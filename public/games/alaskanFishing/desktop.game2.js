function randomNumber(min, max) {
	return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}
var betScore;
var balanceScore;
var allWinScore;
var allWinOld;
var sound;
var game2 = {
	cell : [],
	spinStatus : false,
	bars : [],
	spinStatus1 : false,
	spinStatus2 : false,
	spinStatus3 : false,
	spinStatus4 : false,
	spinStatus5 : false,
	betPanel : false,
	pickBet : 2,
	bet : [],
	betAnim : false,
	sw : null,
	create : function() {
		addAnimCells();

		addScore();
		checkGame = 2;
		//анимация продолжает работать после фокуса на другое окно

		sound = game.add.audio('sound');
		game2.sw = game.add.audio('sound');
		sound.allowMultiple = true;
		sound.addMarker('CoinRush', 1, 1.2);
		sound.addMarker('btn', 40.1, 1.3);
		sound.addMarker('spin', 42.9, 15.4);
		sound.addMarker('bonusWin', 77.5, 4.2);
		// sound.addMarker('win', 58.5, 5.3);
		game2.sw.addMarker('win', 58.5, 5.3);
		game2.Background = game.add.sprite(0,0, 'Background');
		game2.ReelContainer = game.add.sprite(0,132, 'ReelContainer');

		game2.bars[0] = game.add.tileSprite(51, 136, 174, 522, 'bar');
		game2.bars[0].tilePosition.y =  randomNumber(0,11)*204 ;
		game2.bars[1] = game.add.tileSprite(238, 136, 174, 522, 'bar');
		game2.bars[1].tilePosition.y =  randomNumber(0,11)*204;
		game2.bars[2] = game.add.tileSprite(425 , 136, 174, 522, 'bar');
		game2.bars[2].tilePosition.y =  randomNumber(0,11)*204;
		game2.bars[3] = game.add.tileSprite(612, 136, 174, 522, 'bar');
		game2.bars[3].tilePosition.y =  randomNumber(0,11)*204;
		game2.bars[4] = game.add.tileSprite(799, 136, 174, 522, 'bar');
		game2.bars[4].tilePosition.y =  randomNumber(0,11)*204;
		game2.bars[0].visible = false;
		game2.bars[1].visible = false;
		game2.bars[2].visible = false;
		game2.bars[3].visible = false;
		game2.bars[4].visible = false;

		if(checkNewGame == true) {
			info = [0,3,5,6,7,8,5,5,6,7,8,8,4,2,5];
			checkNewGame = false;
		}

		game2.cell[1] = game.add.sprite(51,136, 'cell'+info[0]);
		game2.cell[2] = game.add.sprite(51,310, 'cell'+info[1]);
		game2.cell[3] = game.add.sprite(51,484, 'cell'+info[2]);
		game2.cell[4] = game.add.sprite(238,136, 'cell'+info[3]);
		game2.cell[5] = game.add.sprite(238,310, 'cell'+info[4]);
		game2.cell[6] = game.add.sprite(238,484, 'cell'+info[5]);
		game2.cell[7] = game.add.sprite(425,136, 'cell'+info[6]);
		game2.cell[8] = game.add.sprite(425,310, 'cell'+info[7]);
		game2.cell[9] = game.add.sprite(425,484, 'cell'+info[8]);
		game2.cell[10] = game.add.sprite(612,136, 'cell'+info[9]);
		game2.cell[11] = game.add.sprite(612,310, 'cell'+info[10]);
		game2.cell[12] = game.add.sprite(612,484, 'cell'+info[11]);
		game2.cell[13] = game.add.sprite(799,136, 'cell'+info[12]);
		game2.cell[14] = game.add.sprite(799,310, 'cell'+info[13]);
		game2.cell[15] = game.add.sprite(799,484, 'cell'+info[14]);

		game2.Background_bottom = game.add.sprite(0,658, 'BG1_bottom');
		game2.Logo = game.add.sprite(0,0, 'Logo1');

		//buttons
		game2.amount = game.add.sprite(316,411, 'amount');
		game2.amount.visible = false;
		game2.bet_btn_top = game.add.sprite(316,466, 'bet_btn_top');
		game2.bet_btn_top.visible = false;
		game2.bet_btn_top.inputEnabled = true;
		game2.bet_btn_top.input.useHandCursor = true;
		game2.bet_btn_top.events.onInputUp.add(function(){
			if (game2.pickBet == 2){
				for (var i = 1; i <= 12; ++i) {
					bet[i].visible = false;
				}
				game2.amount.visible = false;
				game2.betPanel = false;
				game2.bet.loadTexture('bet');
				game2.spin.loadTexture('spin');
				game2.spin.inputEnabled = true;
				game2.spin.input.useHandCursor = true;
				game2.bet_btn_top.visible = false;
				game2.bet_btn_middle.visible = false;
				game2.bet_btn_bottom.visible = false;
				game2.amount_top.visible = false;
			} else {
				game.add.tween(bet[game2.pickBet-1]).to({y:568}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				});
				game.add.tween(bet[game2.pickBet]).to({y:626}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
					game2.betAnim = false;
					betScore.setText(bet[game2.pickBet].text);
					betline = bet[game2.pickBet].text/30;
				});
				game.add.tween(bet[game2.pickBet+1]).to({y:684}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				});
				game.add.tween(bet[game2.pickBet-2]).to({y:510}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				});
				game2.pickBet = game2.pickBet - 1;
			}
		});
		game2.bet_btn_middle = game.add.sprite(316,536, 'bet_btn_middle');
		game2.bet_btn_middle.visible = false;
		game2.bet_btn_middle.inputEnabled = true;
		game2.bet_btn_middle.input.useHandCursor = true;
		game2.bet_btn_middle.events.onInputUp.add(function(){
			for (var i = 1; i <= 12; ++i) {
				bet[i].visible = false;
			}
			game2.amount.visible = false;
			game2.betPanel = false;
			game2.bet.loadTexture('bet');
			game2.spin.loadTexture('spin');
			game2.spin.inputEnabled = true;
			game2.spin.input.useHandCursor = true;
			game2.bet_btn_top.visible = false;
			game2.bet_btn_middle.visible = false;
			game2.bet_btn_bottom.visible = false;
			game2.amount_top.visible = false;
		});
		game2.bet_btn_bottom = game.add.sprite(316,596, 'bet_btn_bottom');
		game2.bet_btn_bottom.visible = false;
		game2.bet_btn_bottom.inputEnabled = true;
		game2.bet_btn_bottom.input.useHandCursor = true;
		game2.bet_btn_bottom.events.onInputUp.add(function(){
			if (game2.betAnim)
				return;
			if (game2.pickBet == 11){
				for (var i = 1; i <= 12; ++i) {
					bet[i].visible = false;
				}
				game2.amount.visible = false;
				game2.betPanel = false;
				game2.bet.loadTexture('bet');
				game2.spin.loadTexture('spin');
				game2.spin.inputEnabled = true;
				game2.spin.input.useHandCursor = true;
				game2.bet_btn_top.visible = false;
				game2.bet_btn_middle.visible = false;
				game2.bet_btn_bottom.visible = false;
				game2.amount_top.visible = false;
			} else {
				game.add.tween(bet[game2.pickBet-1]).to({y:452}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				});
				game.add.tween(bet[game2.pickBet]).to({y:510}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
					game2.betAnim = false
					betScore.setText(bet[game2.pickBet].text);
					betline = bet[game2.pickBet].text/30;
				});
				game.add.tween(bet[game2.pickBet+1]).to({y:568}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				});
				game.add.tween(bet[game2.pickBet+2]).to({y:626}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				});
				game2.pickBet = game2.pickBet + 1;
			}
		});
		var bet = [];

		bet[1] = game.add.text(416, 510, 'MIN', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[1].anchor.setTo(0.5, 0.5);
		bet[2] = game.add.text(416, 568, '30', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[2].anchor.setTo(0.5, 0.5);
		bet[3] = game.add.text(416, 626, '60', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[3].anchor.setTo(0.5, 0.5);
		bet[4] = game.add.text(416, 684, '90', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[4].anchor.setTo(0.5, 0.5);
		bet[5] = game.add.text(416, 684, '120', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[5].anchor.setTo(0.5, 0.5);
		bet[6] = game.add.text(416, 684, '150', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[6].anchor.setTo(0.5, 0.5);
		bet[7] = game.add.text(416, 684, '300', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[7].anchor.setTo(0.5, 0.5);
		bet[8] = game.add.text(416, 684, '450', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[8].anchor.setTo(0.5, 0.5);
		bet[9] = game.add.text(416, 684, '600', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[9].anchor.setTo(0.5, 0.5);
		bet[10] = game.add.text(416, 684, '750', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[10].anchor.setTo(0.5, 0.5);
		bet[11] = game.add.text(416, 684, '1500', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[11].anchor.setTo(0.5, 0.5);
		bet[12] = game.add.text(416, 684, 'MAX', {
			font: '28px "Arial"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bet[12].anchor.setTo(0.5, 0.5);
		for (var i = 1; i <= 12; ++i) {
			bet[i].visible = false;
		}
		game2.amount_top = game.add.sprite(316,411, 'amount_top');
		game2.amount_top.visible = false;

		game2.credits = game.add.sprite(17,668, 'credits');
		game2.bet = game.add.sprite(316,668, 'bet');
		game2.bet.inputEnabled = true;
		game2.bet.input.useHandCursor = true;
		game2.bet.events.onInputDown.add(function(){
			game2.bet.loadTexture('bet_p');
			hideAnimCells(showedAnimArray);
		});
		game2.bet.events.onInputUp.add(function(){
			if (game2.betPanel == false){
				game2.betPanel = true;
				game2.amount.visible = true;
				game2.spin.loadTexture('spin_d');
				game2.spin.inputEnabled = false;
				game2.bet_btn_top.visible = true;
				game2.bet_btn_middle.visible = true;
				game2.bet_btn_bottom.visible = true;
				game2.amount_top.visible = true;
				for (var i = 1; i <= 12; ++i) {
					bet[i].visible = true;
				}
			} else {
				for (var i = 1; i <= 12; ++i) {
					bet[i].visible = false;
				}
				game2.amount.visible = false;
				game2.betPanel = false;
				game2.bet.loadTexture('bet');
				game2.spin.loadTexture('spin');
				game2.spin.inputEnabled = true;
				game2.spin.input.useHandCursor = true;
				game2.bet_btn_top.visible = false;
				game2.bet_btn_middle.visible = false;
				game2.bet_btn_bottom.visible = false;
				game2.amount_top.visible = false;
			}
		});

		game2.win = game.add.sprite(529,668, 'win');
		game2.spin = game.add.sprite(753,668, 'spin');
		game2.spin.inputEnabled = true;
		game2.spin.input.useHandCursor = true;
		game2.spin.events.onInputDown.add(function(){
			game2.spin.loadTexture('spin_p');
		});
		game2.spin.events.onInputUp.add(function(){
			game2.spin.loadTexture('spin_d');
			game2.spin.inputEnabled = false;
			game2.bet.loadTexture('bet_d');
			game2.bet.inputEnabled = false;
			betScore.addColor('#465F75', 0);
			if (updateStatus == false){
				beforStartSpin();
				sound.play('btn');
				game2.sw.stop();
			} else {
				pressSpinStatus = true;
			}

		});
		betScore = game.add.text(416, 719, '30', {
			font: '28px "Arial"',
			fill: '#ffffff',
			stroke: '#ffffff',
			strokeThickness: 0,
		});
		betScore.anchor.setTo(0.5, 0.5);
		betScore.setShadow(0,5, "#000000", 10, true, false);
		balanceScore = game.add.text(160, 719, balance, {
			font: '28px "Arial"',
			fill: '#ffffff',
			stroke: '#ffffff',
			strokeThickness: 0,
		});
		balanceScore.anchor.setTo(0.5, 0.5);
		allWinScore = game.add.text(635, 719, null, {
			font: '28px "Arial"',
			fill: '#ffffff',
			stroke: '#ffffff',
			strokeThickness: 0,
		});
		allWinScore.anchor.setTo(0.5, 0.5);
		allWinScore.visible = false;
		
		function beforStartSpin() {
			hideAnimCells(showedAnimArray);
			allWinScore.visible = false;

			requestSpin(gamename, sessionName, betline, lines);

			function requestSpin(gamename, sessionName, betline, lines) {
				$.ajax({
					type: "get",
					url: getNeedUrlPath() + '/spin/'+gamename+'?sessionName='+sessionName+'&betLine='+betline+'&linesInGame='+lines,
					dataType: 'html',
					success: function (data) {
						checkGame = 2;
console.log(data)
						dataSpinRequest = JSON.parse(data);
						if(dataSpinRequest['state']) {
							if(dataSpinRequest['rope']) {
								checkRopeGame = true;
								parseSpinAnswer(dataSpinRequest);
							} else {
								parseSpinAnswer(dataSpinRequest);
							}
						}

						setTimeout(function() {
							sound.play('spin');
						}, 600);
						game2.spinStatus = true;
						startspin(0);
						setTimeout(function() {
							startspin(1);
						}, 200);
						setTimeout(function() {
							startspin(2);
						}, 400);
						setTimeout(function() {
							startspin(3);
						}, 600);
						setTimeout(function() {
							startspin(4);
						}, 800);

					},
					error: function (xhr, ajaxOptions, thrownError) {
						var errorText = '//ошибка 30';
						console.log(errorText);
						setTimeout("requestSpin(gamename, sessionName, betline, lines)", 2000);
					}
				});
			}

		}
		function startspin(number){
			game.add.tween(game2.cell[1+number*3]).to({y:game2.cell[1+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				game2.cell[1+number*3].visible = false;
			});
			game.add.tween(game2.cell[2+number*3]).to({y:game2.cell[2+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				game2.cell[2+number*3].visible = false;
			});
			game.add.tween(game2.cell[3+number*3]).to({y:game2.cell[3+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				game2.cell[3+number*3].visible = false;
				game2.bars[number].visible = true;
				if (number == 0){
					game2.spinStatus1 = true;
					setTimeout(function() {
						game2.spinStatus1 = false;
						game2.bars[0].visible = false;
						game2.cell[1+3*0].visible = true;
						game2.cell[2+3*0].visible = true;
						game2.cell[3+3*0].visible = true;

						game2.cell[1].loadTexture('cell'+info[0]);
						game2.cell[2].loadTexture('cell'+info[1]);
						game2.cell[2].loadTexture('cell'+info[1]);
						game2.cell[3].loadTexture('cell'+info[2]);

					}, 2000);
				}
				if (number == 1){
					game2.spinStatus2 = true;
					setTimeout(function() {
						game2.spinStatus2 = false;
						game2.bars[1].visible = false;
						game2.cell[1+3*1].visible = true;
						game2.cell[2+3*1].visible = true;
						game2.cell[3+3*1].visible = true;

						game2.cell[4].loadTexture('cell'+info[3]);
						game2.cell[5].loadTexture('cell'+info[4]);
						game2.cell[6].loadTexture('cell'+info[5]);

					}, 2000);
				}
				if (number == 2){
					game2.spinStatus3 = true;
					setTimeout(function() {
						game2.spinStatus3 = false;
						game2.bars[2].visible = false;
						game2.cell[1+3*2].visible = true;
						game2.cell[2+3*2].visible = true;
						game2.cell[3+3*2].visible = true;

						game2.cell[7].loadTexture('cell'+info[6]);
						game2.cell[8].loadTexture('cell'+info[7]);
						game2.cell[9].loadTexture('cell'+info[8]);

					}, 2000);
				}
				if (number == 3){
					game2.spinStatus4 = true;
					setTimeout(function() {
						game2.spinStatus4 = false;
						game2.bars[3].visible = false;
						game2.cell[1+3*3].visible = true;
						game2.cell[2+3*3].visible = true;
						game2.cell[3+3*3].visible = true;

						game2.cell[10].loadTexture('cell'+info[9]);
						game2.cell[11].loadTexture('cell'+info[10]);
						game2.cell[12].loadTexture('cell'+info[11]);

					}, 2000);
				}
				if (number == 4){
					game2.spinStatus5 = true;
					setTimeout(function() {
						game2.spinStatus5 = false;
						game2.bars[4].visible = false;
						game2.cell[1+3*4].visible = true;
						game2.cell[2+3*4].visible = true;
						game2.cell[3+3*4].visible = true;

						game2.cell[13].loadTexture('cell'+info[12]);
						game2.cell[14].loadTexture('cell'+info[13]);
						game2.cell[15].loadTexture('cell'+info[14]);

					}, 2000);
				}
				setTimeout(function() {
					endspin(number);
				}, 2000);

			});
		};
		function endspin(number){
			game2.cell[1+number*3].position.y = 136+30;
			game2.cell[2+number*3].position.y = 310+30;
			game2.cell[3+number*3].position.y = 484+30;
			game.add.tween(game2.cell[1+number*3]).to({y:game2.cell[1+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
			});
			game.add.tween(game2.cell[2+number*3]).to({y:game2.cell[2+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
			});
			game.add.tween(game2.cell[3+number*3]).to({y:game2.cell[3+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				if (number == 0){
					if(winCellInfo[0] === 'bonusCell13') {
						playBonusCellAnim(0, 13);
					}
					if(winCellInfo[1] === 'bonusCell13') {
						playBonusCellAnim(1, 13);
					}
					if(winCellInfo[2] === 'bonusCell13') {
						playBonusCellAnim(2, 13);
					}

					if(winCellInfo[0] === 'bonusCell12') {
						playBonusCellAnim(0, 12);
					}
					if(winCellInfo[1] === 'bonusCell12') {
						playBonusCellAnim(1, 12);
					}
					if(winCellInfo[2] === 'bonusCell12') {
						playBonusCellAnim(2, 12);
					}
				}
				if (number == 1){
					if(winCellInfo[3] === 'bonusCell13') {
						playBonusCellAnim(3, 13);
					}
					if(winCellInfo[4] === 'bonusCell13') {
						playBonusCellAnim(4, 13);
					}
					if(winCellInfo[5] === 'bonusCell13') {
						playBonusCellAnim(5, 13);
					}
				}
				if (number == 2){
					if(winCellInfo[6] === 'bonusCell13') {
						playBonusCellAnim(6, 13);
					}
					if(winCellInfo[7] === 'bonusCell13') {
						playBonusCellAnim(7, 13);
					}
					if(winCellInfo[8] === 'bonusCell13') {
						playBonusCellAnim(8, 13);
					}
				}
				if (number == 3){
					if(winCellInfo[9] === 'bonusCell13') {
						playBonusCellAnim(9, 13);
					}
					if(winCellInfo[10] === 'bonusCell13') {
						playBonusCellAnim(10, 13);
					}
					if(winCellInfo[11] === 'bonusCell13') {
						playBonusCellAnim(11, 13);
					}			
				}
				if (number == 4){
					if(winCellInfo[12] === 'bonusCell13') {
						playBonusCellAnim(12, 13);
					}
					if(winCellInfo[13] === 'bonusCell13') {
						playBonusCellAnim(13, 13);
					}
					if(winCellInfo[14] === 'bonusCell13') {
						playBonusCellAnim(14, 13);
					}

					if(winCellInfo[12] === 'bonusCell12') {
						playBonusCellAnim(12, 12);
					}
					if(winCellInfo[13] === 'bonusCell12') {
						playBonusCellAnim(13, 12);
					}
					if(winCellInfo[14] === 'bonusCell12') {
						playBonusCellAnim(14, 12);
					}
				}
				if (number == 4){
					sound.stop('spin');
					showAnimCells(winCellInfo);
					if(ropeValues == false || ropeValues == '15freeSpin'){
						updateBalance();
					} else {
						sound.play('bonusWin');
					}
					if(ropeValues == false){
						setTimeout("game2.spin.loadTexture('spin'); game2.spin.inputEnabled = true;	game2.spin.input.useHandCursor = true; game2.spinStatus = false;",100);
						setTimeout("game2.bet.loadTexture('bet'); game2.bet.inputEnabled = true; game2.bet.input.useHandCursor = true; betScore.addColor('#ffffff', 0);",100);
					}
					//TODO: поставить setTiemout на появление кнопки
				}
			});
		}
		function updateBalance(){
			updateStatus = true;
			var x = 0;
			var interval;
			if (allWin > 0) {
				allWinScore.visible = true;
				allWinScore.setText(0);
				game2.sw.play('win');
			}
			(function() {
				if (x < allWin) {
					if (betlineR*linesR < 500){
						interval = 1000/(betlineR*linesR);
						if (pressSpinStatus == true){
							interval = 250/(betlineR*linesR);
						}
						if (allWin > 1000/interval*5){
							x += Math.round(allWin/(betlineR*linesR*5));
						} else {
							x += 1;
						}
						allWinScore.setText(x);
						setTimeout(arguments.callee, interval);
					} else {
						interval = 20000/(betlineR*linesR);
						if (pressSpinStatus == true){
							interval = 5000/(betlineR*linesR);
						}
						if (allWin > 1000/interval*5*20){
							x += Math.round(allWin*20/(betlineR*linesR*5));
						} else {
							x += 20;
						}
						allWinScore.setText(x);
						setTimeout(arguments.callee, interval);
					}
				} else {
					allWinOld = allWin;
					allWinScore.setText(allWinOld);
					balanceScore.setText(balance);
					updateStatus = false;
					if(ropeValues == '15freeSpin'){
						setTimeout(function() {
							game2.sw.stop();
							unlockDisplay();
							hideAnimCells(showedAnimArray);
							game.state.start('game3');
						}, 1000);
					}
					if (pressSpinStatus == true){
						pressSpinStatus = false;
						beforStartSpin();
						game2.sw.stop();
						sound.play('CoinRush');
					}
				}
			})();
		}
	},
	update:function(){
		if (game2.spinStatus1){
			game2.bars[0].tilePosition.y += 50;
		}
		if (game2.spinStatus2){
			game2.bars[1].tilePosition.y += 50;
		}
		if (game2.spinStatus3){
			game2.bars[2].tilePosition.y += 50;
		}
		if (game2.spinStatus4){
			game2.bars[3].tilePosition.y += 50;
		}
		if (game2.spinStatus5){
			game2.bars[4].tilePosition.y += 50;
		};
	}
};


game.state.add('game2', game2);