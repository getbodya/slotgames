<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Alaskan Fishing</title>
	<link rel="stylesheet" href="main.css" />
	<script src="phaser.min.js"></script>
	<script src="jquery-3.0.0.js"></script>
	<script src="main.js"></script>
</head>
<body>
	<script>
		<?php
		include_once 'functions.js';
		include_once 'game1.js';
		include_once 'game2.js';
		include_once 'game3.js';
		include_once 'preloader.js';
		?>
	</script>
	<div id="preloader">
		<div id="preloaderBackground" class="preloaderBackground" >
			<div class="meter-wrap">
				<div class="meter-wrap-bg-container">
					<div class="meter-wrap-bg-left"></div>
					<div class="meter-wrap-bg-bar"></div>
					<div class="meter-wrap-bg-right"></div>
				</div>
				<div class="meter-value" id="preLoaderProgress">
					<div class="meter-bg-container">
						<div class="meter-bg-left"></div>
						<div class="meter-bg-bar"></div>
						<div class="meter-bg-right"></div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<div id="displayLock" style="display: none; width: 100%; height: 100%; z-index: 9999; position: fixed;" ></div>
</body>
</html>