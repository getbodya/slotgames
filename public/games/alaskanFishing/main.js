jQuery(document).ready(function() {
  $('#preloader').css("display","block");
  realHeight = document.documentElement.clientHeight;
  realWidth = Math.round(1024/748*realHeight);
  $('#preloaderBackground').css("width", realWidth);
  $('#preloaderBackground').css("height", realHeight);
  $(window).resize(function() {
    realHeight = document.documentElement.clientHeight;
    realWidth = Math.round(1024/748*realHeight);
    $('#preloaderBackground').css("width", realWidth);
    $('#preloaderBackground').css("height", realHeight);
  });
});