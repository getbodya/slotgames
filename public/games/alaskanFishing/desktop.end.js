var gamePreload = {

	preload:function(){
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            $('#preLoaderProgress').css("width", progress + '%');
        });

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.stage.disableVisibilityChange = true;
        game.scale.setResizeCallback(function() {       
        });
        // game.load.image('main_window', 'img/main_window.png');
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
		
        game.load.image('Background', needUrlPath + '/img/Background.jpg');
        game.load.image('Background2', needUrlPath + '/img/fsBackground.jpg');
        game.load.image('BG1_bottom', needUrlPath + '/img/BG1_bottom.png');
        game.load.image('BG2_bottom', needUrlPath + '/img/BG2_bottom.png');
        game.load.image('ReelContainer', needUrlPath + '/img/ReelContainer.png');
        game.load.image('ReelContainer2', needUrlPath + '/img/fsReelContainer.png');
        game.load.image('Logo1', needUrlPath + '/img/Logo1.png');
        game.load.image('Logo2', needUrlPath + '/img/Logo2.png');
        game.load.image('freeSpins', needUrlPath + '/img/txtFreeSpins.png');
        game.load.image('txtMultiplier', needUrlPath + '/img/txtMultiplier.png');
        game.load.image('FSCongrats', needUrlPath + '/img/FSCongrats.jpg');
        game.load.image('FreeSpinsWinSum', needUrlPath + '/img/txtFreeSpinsWinSum.png');
        game.load.image('FreeSpinsWinSumCoin', needUrlPath + '/img/txtFreeSpinsWinSumCoin.png');
        game.load.image('introBonus', needUrlPath + '/img/introBonus.png');

        game.load.image('amount', needUrlPath + '/img/btns/amount.png');
        game.load.image('amount_top', needUrlPath + '/img/btns/amount_top.png');
        game.load.image('bet_btn_bottom', needUrlPath + '/img/btns/bet_btn_bottom.png');
        game.load.image('bet_btn_middle', needUrlPath + '/img/btns/bet_btn_middle.png');
        game.load.image('bet_btn_top', needUrlPath + '/img/btns/bet_btn_top.png');
        game.load.image('bet', needUrlPath + '/img/btns/bet.png');
        game.load.image('bet_d', needUrlPath + '/img/btns/bet_d.png');
        game.load.image('bet_p', needUrlPath + '/img/btns/bet_p.png');
        game.load.image('credits', needUrlPath + '/img/btns/credits.png');
        game.load.image('spin', needUrlPath + '/img/btns/spin.png');
        game.load.image('spin_d', needUrlPath + '/img/btns/spin_d.png');
        game.load.image('spin_p', needUrlPath + '/img/btns/spin_p.png');
        game.load.image('win', needUrlPath + '/img/btns/win.png');
        game.load.image('win2', needUrlPath + '/img/btns/win2.png');

        game.load.image('cell0', needUrlPath + '/img/Symbol0.png');
        game.load.image('cell1', needUrlPath + '/img/Symbol1.png');
        game.load.image('cell2', needUrlPath + '/img/Symbol2.png');
        game.load.image('cell3', needUrlPath + '/img/Symbol3.png');
        game.load.image('cell4', needUrlPath + '/img/Symbol4.png');
        game.load.image('cell5', needUrlPath + '/img/Symbol5.png');
        game.load.image('cell6', needUrlPath + '/img/Symbol6.png');
        game.load.image('cell7', needUrlPath + '/img/Symbol7.png');
        game.load.image('cell8', needUrlPath + '/img/Symbol8.png');
        game.load.image('cell9', needUrlPath + '/img/Symbol9.png');
        game.load.image('cell10', needUrlPath + '/img/Symbol10.png');
        game.load.image('cell11', needUrlPath + '/img/Symbol11.png');
        game.load.image('bar', needUrlPath + '/img/bar.png');

        game.load.atlasJSONHash('fisherman1', needUrlPath + '/fisherman_sprite.png', needUrlPath + '/fisherman_sprite_1.json');
        game.load.atlasJSONHash('fisherman2', needUrlPath + '/fisherman_sprite.png', needUrlPath + '/fisherman_sprite_2.json');
        game.load.atlasJSONHash('fisherman3', needUrlPath + '/fisherman_sprite.png', needUrlPath + '/fisherman_sprite_3.json');
        game.load.atlasJSONHash('dog_idle', needUrlPath + '/dog_fish_spot_sprite.png', needUrlPath + '/dog_idle.json');
        game.load.atlasJSONHash('dog_look_left', needUrlPath + '/dog_fish_spot_sprite.png', needUrlPath + '/dog_look_left.json');
        game.load.atlasJSONHash('dog_look_right', needUrlPath + '/dog_fish_spot_sprite.png', needUrlPath + '/dog_look_right.json');
        game.load.atlasJSONHash('fish1', needUrlPath + '/dog_fish_spot_sprite.png', needUrlPath + '/fish_1.json');
        game.load.atlasJSONHash('fish2', needUrlPath + '/dog_fish_spot_sprite.png', needUrlPath + '/fish_2.json');
        game.load.atlasJSONHash('Spot', needUrlPath + '/dog_fish_spot_sprite.png', needUrlPath + '/Spot.json');
        game.load.atlasJSONHash('Spot2', needUrlPath + '/dog_fish_spot_sprite2.png', needUrlPath + '/Spot.json');

        game.load.image('BonusBackground', needUrlPath + '/img/BonusBackground.jpg');
        game.load.image('TotalWinPlaque', needUrlPath + '/img/TotalWinPlaque.png');
        game.load.spritesheet('picks_left_1', needUrlPath + '/img/picks_left_1.png', 182, 131, 10);
        game.load.spritesheet('picks_left_2', needUrlPath + '/img/picks_left_2.png', 182, 131, 10);
        game.load.spritesheet('picks_left_3', needUrlPath + '/img/picks_left_3.png', 182, 131, 10);
        game.load.spritesheet('picks_left_4', needUrlPath + '/img/picks_left_4.png', 182, 131, 10);
        game.load.spritesheet('picks_left_5', needUrlPath + '/img/picks_left_5.png', 182, 131, 10);

        game.load.spritesheet('BigFish_1', needUrlPath + '/img/txtBigFish_1.png', 860, 442, 4);
        game.load.spritesheet('BigFish_2', needUrlPath + '/img/txtBigFish_2.png', 860, 442, 3);
        game.load.spritesheet('BigFish_3', needUrlPath + '/img/txtBigFish_3.png', 860, 442, 4);
        game.load.spritesheet('BigFish_4', needUrlPath + '/img/txtBigFish_4.png', 860, 442, 3);
        game.load.spritesheet('BigFish_5', needUrlPath + '/img/txtBigFish_5.png', 860, 442, 4);
        game.load.spritesheet('BigFish_6', needUrlPath + '/img/txtBigFish_6.png', 860, 442, 4);
        game.load.spritesheet('BigFish_7', needUrlPath + '/img/txtBigFish_7.png', 860, 442, 1);

        game.load.spritesheet('MediumFish_1', needUrlPath + '/img/txtMediumFish_1.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_2', needUrlPath + '/img/txtMediumFish_2.png', 860, 442, 3);
        game.load.spritesheet('MediumFish_3', needUrlPath + '/img/txtMediumFish_3.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_4', needUrlPath + '/img/txtMediumFish_4.png', 860, 442, 3);
        game.load.spritesheet('MediumFish_5', needUrlPath + '/img/txtMediumFish_5.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_6', needUrlPath + '/img/txtMediumFish_6.png', 860, 442, 4);
        game.load.spritesheet('MediumFish_7', needUrlPath + '/img/txtMediumFish_7.png', 860, 442, 1);

        game.load.spritesheet('SmallFish_1', needUrlPath + '/img/txtSmallFish_1.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_2', needUrlPath + '/img/txtSmallFish_2.png', 860, 442, 3);
        game.load.spritesheet('SmallFish_3', needUrlPath + '/img/txtSmallFish_3.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_4', needUrlPath + '/img/txtSmallFish_4.png', 860, 442, 3);
        game.load.spritesheet('SmallFish_5', needUrlPath + '/img/txtSmallFish_5.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_6', needUrlPath + '/img/txtSmallFish_6.png', 860, 442, 4);
        game.load.spritesheet('SmallFish_7', needUrlPath + '/img/txtSmallFish_7.png', 860, 442, 1);

        game.load.spritesheet('bonus_Picks_5_1', needUrlPath + '/img/bonus_Picks_5_intro_1.png', 520, 360, 7);
        game.load.spritesheet('bonus_Picks_5_2', needUrlPath + '/img/bonus_Picks_5_intro_2.png', 520, 360, 7);
        game.load.spritesheet('bonus_Picks_5_3', needUrlPath + '/img/bonus_Picks_5_loop_1.png', 548, 360, 7);
        game.load.spritesheet('bonus_Picks_5_4', needUrlPath + '/img/bonus_Picks_5_loop_2.png', 530, 360, 7);
        game.load.spritesheet('bonus_Picks_5_5', needUrlPath + '/img/bonus_Picks_5_exit_1.png', 520, 360, 7);
        game.load.spritesheet('bonus_Picks_5_6', needUrlPath + '/img/bonus_Picks_5_exit_2.png', 520, 360, 7);
        
        game.load.spritesheet('Congratulations1', needUrlPath + '/img/bonus_Congratulations_loop_1.png', 742, 371, 5);
        game.load.spritesheet('Congratulations2', needUrlPath + '/img/bonus_Congratulations_loop_2.png', 742, 371, 5);
        game.load.spritesheet('Congratulations3', needUrlPath + '/img/bonus_Congratulations_loop_3.png', 742, 371, 5);

        game.load.audio('sound', needUrlPath + '/sounds/AlaskanFishing.mp3');

        game.load.spritesheet('cellAnim_0_1', needUrlPath + '/img/Symbol0_WinAnim_gif_1.png', 174, 174);
        game.load.spritesheet('cellAnim_0_2', needUrlPath + '/img/Symbol0_WinAnim_gif_2.png', 174, 174);
        game.load.spritesheet('cellAnim_1', needUrlPath + '/img/Symbol1_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_2', needUrlPath + '/img/Symbol2_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_3', needUrlPath + '/img/Symbol3_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_4', needUrlPath + '/img/Symbol4_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_5', needUrlPath + '/img/Symbol5_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_6', needUrlPath + '/img/Symbol6_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_7', needUrlPath + '/img/Symbol7_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_8', needUrlPath + '/img/Symbol8_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_9', needUrlPath + '/img/Symbol9_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_10', needUrlPath + '/img/Symbol10_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_11', needUrlPath + '/img/Symbol11_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_12', needUrlPath + '/img/Symbol1_Land.png', 174, 174);
        game.load.spritesheet('cellAnim_13', needUrlPath + '/img/Symbol11_Land.png', 174, 174);

        game.load.spritesheet('cellAnim_0_1fs', needUrlPath + '/img/Symbol0_fs_WinAnim_gif_1.png', 174, 174);
        game.load.spritesheet('cellAnim_0_2fs', needUrlPath + '/img/Symbol0_fs_WinAnim_gif_2.png', 174, 174);
        game.load.spritesheet('cellAnim_1fs', needUrlPath + '/img/Symbol1_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_2fs', needUrlPath + '/img/Symbol2_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_3fs', needUrlPath + '/img/Symbol3_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_4fs', needUrlPath + '/img/Symbol4_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_5fs', needUrlPath + '/img/Symbol5_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_6fs', needUrlPath + '/img/Symbol6_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_7fs', needUrlPath + '/img/Symbol7_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_8fs', needUrlPath + '/img/Symbol8_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_9fs', needUrlPath + '/img/Symbol9_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_10fs', needUrlPath + '/img/Symbol10_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_11fs', needUrlPath + '/img/Symbol11_fs_WinAnim.png', 174, 174);
        game.load.spritesheet('cellAnim_12fs', needUrlPath + '/img/Symbol1_Land_fs.png', 174, 174);
        game.load.spritesheet('cellAnim_13fs', needUrlPath + '/img/cellAnim_13fs.png', 174, 174);

        game.load.spritesheet('cellAnimLand_1', needUrlPath + '/img/Symbol1_Land.png', 174, 174);
        game.load.spritesheet('cellAnimLand_11', needUrlPath + '/img/Symbol11_Land.png', 174, 174);
        game.load.spritesheet('cellAnimBonus_1', needUrlPath + '/img/Symbol1_WinAnim_1_1.png', 174, 174);
        game.load.spritesheet('cellAnimBonus_2', needUrlPath + '/img/Symbol1_WinAnim_1_2.png', 174, 174);

    },
    create:function(){
        game.state.start('game2'); //переключение на 1 игру
        document.getElementById('preloader').style.display = 'none';
    }
};
game.state.add('gamePreload', gamePreload); //добавление загрузчика в игру


// запуск игры

var sessionName;

requestInit();
function getNeedUrlPath() {
    if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
        var number = location.pathname.indexOf('/games/');
        var needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname.substring(0,number) + '/';

        return needLocation;
    } else if (location.href.indexOf('public') !== -1 && location.href.indexOf('/game/') !== -1) {
        var number = location.pathname.indexOf('/public/');
        var needLocation = location.href.substring(0,location.href.indexOf('public')) + 'public';

        return needLocation;
    } else if (location.href.indexOf('public') === -1) {
        needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname;

        return needLocation;
    }
}

function requestInit() {
    $.ajax({
        type: "get",
        url: getNeedUrlPath() +'/init',
        dataType: 'html',
        success: function (data) {
            dataString = data;

            if(dataString) {
                sessionName = data;
                requestState()
            } else {
                alert('Ошибка 11');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = 'ошибка 10';
            console.log(errorText);
            setTimeout("requestState();", 2000);
        }
    });
}

function requestState() {
    $.ajax({
        type: "get",
        url: getNeedUrlPath() +'/state?sessionName='+sessionName,
        dataType: 'html',
        success: function (data) {

            dataArray = JSON.parse(data);

            if(dataArray['state']) {
                balance = dataArray['balance'];
                info = dataArray['info'];

                game.state.start('gamePreload'); //начало игры с локации загрузчика
            } else {
                alert('Ошбика 21');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = 'ошибка 20';
            alert(errorText);
            setTimeout("requestInit();", 2000);
        }
    });
}

