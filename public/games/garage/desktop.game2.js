function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбраcываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения

        var bg1 = game.add.sprite(0,0, 'game.background');
        var bg2 = game.add.sprite(94,86, 'game.background4');
        var cardBg = game.add.sprite(94,86, 'cardBg');

        backgroundTotal = game.add.sprite(95,23, 'topBarType3');
        
        // кнопки
        addButtonsGame2(game);


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 84, 30]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[113+13,133], [263,133], [375,133], [490,133], [602,133]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация

        cat = game.add.sprite(95,406, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        cat.animations.getAnimation('cat').play();

        var bg_bottom = game.add.sprite(94,408, 'bg-bottom');
        var group1 = game.add.group();
        group1.add(cat);


        sourceArray = [['game.man1', [222,390], 21, 5], ['game.man2', [222,390], 14, 5]];
        playAnimQueue(sourceArray, group1);

        game.world.bringToTop(group1);

        fontsize = 18;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 543, 445, fontsize);

        addTableTitle(game, 'winTitleGame2', 540,434);
        hideTableTitle();

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}