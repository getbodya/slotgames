function game4() {
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {

            //звуки
            arrowSound = game.add.audio('game.arrowSound');
            arrowSound.loop = true;
            arrowSound.play();

            openKeySound = game.add.audio('game.openKeySound');
            openKeyBoxLoseSound = game.add.audio('game.openKeyBoxLoseSound');
            openKeyBoxPreLoseSound = game.add.audio('game.openKeyBoxPreLoseSound');
            openKeyBoxWinSound = game.add.audio('game.openKeyBoxWinSound');

            //изображения
            background = game.add.sprite(0,0, 'game.background');
            background2 = game.add.sprite(94,54, 'game.background2');


            //счет
            scorePosions = [[260, 25, 20], [435, 25, 20], [610, 25, 20], [475, 25, 30]];
            addScore(game, scorePosions, bet, stepTotalWinR, balance, betline);


            //кнопки и анимации событий нажатия

            //для данной игры в каждую функцию вносится по две анимации (win и lose) для каждой кнопки
            selectionsAmin = [];
            timeout1Game4ToGame1 = 8000;
            timeout2Game4ToGame1 = 4000;

            // анимация для кнопки 3
            selectionsAmin[0] = function (ropeValues, ropeStep) {
                if(ropeValues[5] != 0){
                    arrowSound.stop();
                    openKeyBoxWinSound.play();

                    var openWinLeftCase = game.add.sprite(173,288, 'game.openWinLeftCase');
                    openWinLeftCase.animations.add('openWinLeftCase', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
                    openWinLeftCase.visible = true;
                    openWinLeftCase.animations.getAnimation('openWinLeftCase').play().onComplete.add(function(){
                        //openWinLeftCase.visible = false;

                        arrowSound.stop();
                        openKeySound.play();

                        lock5.visible = false;
                        arrowRedBlue5.visible = false;

                        var findAKey = game.add.sprite(367,326, 'game.findAKey');
                        findAKey.animations.add('findAKey', [0,1], 1.5, true);
                        findAKey.visible = false;
                        findAKey.animations.getAnimation('findAKey').play();

                        var openLock2 = game.add.sprite(398,326, 'game.openLock2');
                        openLock2.animations.add('openLock2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, false);
                        openLock2.visible = true;
                        openLock2.animations.getAnimation('openLock2').play().onComplete.add(function(){
                            openLock2.visible = false;
                            findAKey.visible = true;
                        });

                        setTimeout("unlockDisplay();", 4000);

                    });
                } else {
                    arrowSound.stop();

                    openKeyBoxLoseSound.play();

                    var openLoseLeftCase = game.add.sprite(173,288, 'game.openLoseLeftCase');
                    openLoseLeftCase.animations.add('openLoseLeftCase', [0,1,2,3,4,4,4,4,4,4,4], 5, false);
                    openLoseLeftCase.visible = true;
                    openLoseLeftCase.animations.getAnimation('openLoseLeftCase').play().onComplete.add(function(){
                        openLoseLeftCase.visible = false;
                    });
                }
            };

            // анимация для кнопки 7
            selectionsAmin[1] = function (ropeValues, ropeStep) {
                if(ropeValues[5] != 0){
                    arrowSound.stop();
                    openKeyBoxWinSound.play();
                    openKeyBoxPreLoseSound.play();

                    var openWinRightCase = game.add.sprite(463,288, 'game.openWinRightCase');
                    openWinRightCase.animations.add('openWinRightCase', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
                    openWinRightCase.visible = true;
                    openWinRightCase.animations.getAnimation('openWinRightCase').play().onComplete.add(function(){
                        openWinRightCase.visible = false;
                    });
                } else {
                    lockDisplay();
                    arrowSound.stop();
                    openKeyBoxLoseSound.play();

                    var openLoseRightCase = game.add.sprite(462,288, 'game.openLoseRightCase');
                    openLoseRightCase.animations.add('openLoseRightCase', [0,1,2,3,4,4,4,4,4,4,4], 5, false);
                    openLoseRightCase.visible = true;
                    openLoseRightCase.animations.getAnimation('openLoseRightCase').play().onComplete.add(function(){
                        openLoseRightCase.visible = false;
                        unlockDisplay();
                    });
                }
            };
            

            addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1);



            //анимации
            //lock
            lock1 = game.add.sprite(398,278, 'game.lock');
            lock2 = game.add.sprite(398,230, 'game.lock');
            lock3 = game.add.sprite(398,182, 'game.lock');
            lock4 = game.add.sprite(398,134, 'game.lock');
            lock5 = game.add.sprite(398,326, 'game.lock');

            //box_for_keys = game.add.sprite(268,256, 'game.box_for_keys');

            boxForKeyLeftC = game.add.sprite(268,305, 'boxForKeyLeftC');
            boxForKeyRightC = game.add.sprite(465,305, 'boxForKeyRightC');

            cat = game.add.sprite(95,406, 'game.cat');

            arrowRedBlue1 = game.add.sprite(365,112, 'game.arrowRedBlue');
            arrowRedBlue2 = game.add.sprite(365,161, 'game.arrowRedBlue');
            arrowRedBlue3 = game.add.sprite(365,209, 'game.arrowRedBlue');
            arrowRedBlue4 = game.add.sprite(365,256, 'game.arrowRedBlue');
            arrowRedBlue5 = game.add.sprite(365,305, 'game.arrowRedBlue');

            //создание анимации для человека

            cat.animations.add('cat', [0,1,2,3,4,5], 2, true);

            arrowRedBlue1.animations.add('arrowRedBlue', [0,1], 1.5, true);
            arrowRedBlue2.animations.add('arrowRedBlue', [0,1], 1.5, true);
            arrowRedBlue3.animations.add('arrowRedBlue', [0,1], 1.5, true);
            arrowRedBlue4.animations.add('arrowRedBlue', [0,1], 1.5, true);
            arrowRedBlue5.animations.add('arrowRedBlue', [0,1], 1.5, true);


            cat.animations.getAnimation('cat').play();
            arrowRedBlue1.animations.getAnimation('arrowRedBlue').play();
            arrowRedBlue2.animations.getAnimation('arrowRedBlue').play();
            arrowRedBlue3.animations.getAnimation('arrowRedBlue').play();
            arrowRedBlue4.animations.getAnimation('arrowRedBlue').play();
            arrowRedBlue5.animations.getAnimation('arrowRedBlue').play();

            move_super_key1 = game.add.sprite(537,109, 'game.move_super_key1');
            move_super_key1.inputEnabled = true;
            move_super_key1.input.useHandCursor = true;
            move_super_key1.events.onInputDown.add(function(){
                if(check_super_key == 0) {
                    game.add.sprite(396,369, 'game.small_red_key');
                    check_super_key = 1;
                }
            });
            move_super_key1.animations.add('move_super_key1', [0,1,2,3,4,5,6,7,8,9,10,11,12,14,16,18,20,22,24,26,28,30,32], 12, true);

            move_super_key2 = game.add.sprite(537,109, 'game.move_super_key2');
            move_super_key2.inputEnabled = true;
            move_super_key2.input.useHandCursor = true;
            move_super_key2.events.onInputDown.add(function(){
                if(check_super_key == 0) {
                    game.add.sprite(396,369, 'game.small_red_key');
                    check_super_key = 1;
                }
            });
            move_super_key2.animations.add('move_super_key2', [1,2,3,4], 12, true);

            move_super_key1.visible = false;
            move_super_key2.visible = false;

            var superkeyNumber = 1;

            function superkeyAnim(){
                if(superkeyNumber == 1){
                    move_super_key1.visible = true;
                    move_super_key1.animations.getAnimation('move_super_key1').play().onComplete.add(function(){
                        move_super_key1.visible = false;
                        superkeyNumber = 2;
                        manAnim(manNumber);
                    });
                } else {
                    move_super_key2.visible = true;
                    move_super_key2.animations.getAnimation('move_super_key2').play().onComplete.add(function(){
                        move_super_key2.visible = false;
                        superkeyNumber = 1;
                        manAnim(manNumber);
                    });
                }
            }

            superkeyAnim(superkeyNumber);

            man1 = game.add.sprite(222,390, 'game.man1');
            man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, true);
            man1.animations.getAnimation('man1').play();

            full_and_sound();
        };

        game4.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game4', game4);

})();


}