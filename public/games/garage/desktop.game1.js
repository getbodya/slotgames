var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play'); // проверить нужно ли это


        // изображения

        var bg1 = game.add.sprite(0,0, 'game.background');
        var bg2 = game.add.sprite(94,86, 'game.background4');


        topBarImage = game.add.sprite(93,23, 'topBarType1');

        slotPosition = [[144, 87], [144, 193], [144, 299], [254, 87], [254, 193], [254, 299], [365, 87], [365, 193], [365, 299], [477, 87], [477, 193], [477, 299], [589, 87], [589, 193], [589, 299]];
        addSlots(game, slotPosition);

        var linePosition = [[134,199+50], [134,71+33], [134,322+64], [134,130+33], [134,95+34], [134,102+33], [134,228+34], [134,226+50], [134,120+34]];
        var numberPosition = [[93,183+50], [93,54+33], [93,310+64], [93,118+33], [93,246+64], [93,86+33], [93,278+64], [93,214+50], [93,150+50]];
        addLinesAndNumbers(game, linePosition, numberPosition);
        
        showLines(linesArray);
        showNumbers(numberArray);



        // кнопки
        addButtonsGame1(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230, 26, 20], [435, 26, 20], [610, 26, 20], [99, 59, 16], [707, 59, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);



        // анимация
        
        cat = game.add.sprite(95,406, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        cat.animations.getAnimation('cat').play();

        var bg_bottom = game.add.sprite(94,408, 'bg-bottom');

        var group1 = game.add.group();
        group1.add(cat);
        //group1.add(man1);
        //group1.add(man2);
        group1.add(bg2);

        sourceArray = [['game.man1', [222,390], 21, 5], ['game.man2', [222,390], 14, 5]];
        playAnimQueue(sourceArray, group1);
        game.world.bringToTop(group1);

        groupLines = game.add.group();
        groupLines.add(line1);
        groupLines.add(line2);
        groupLines.add(line3);
        groupLines.add(line4);
        groupLines.add(line5);
        groupLines.add(line6);
        groupLines.add(line7);
        groupLines.add(line8);
        groupLines.add(line9);
        groupLines.add(linefull1);
        groupLines.add(linefull2);
        groupLines.add(linefull3);
        groupLines.add(linefull4);
        groupLines.add(linefull5);
        groupLines.add(linefull6);
        groupLines.add(linefull7);
        groupLines.add(linefull8);
        groupLines.add(linefull9);
        groupLines.add(number1);
        groupLines.add(number2);
        groupLines.add(number3);
        groupLines.add(number4);
        groupLines.add(number5);
        groupLines.add(number6);
        groupLines.add(number7);
        groupLines.add(number8);
        groupLines.add(number9);
        game.world.bringToTop(groupLines);


        addTableTitle(game, 'play1To',539,436);

        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game1', game1);

};
