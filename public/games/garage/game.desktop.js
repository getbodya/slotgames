var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'phaser-example', 'ld29', null, false, false);

var fullStatus = false;
var soundStatus = true;



function hideNumbers() {
	number1.visible = false;
	number2.visible = false;
	number3.visible = false;
	number4.visible = false;
	number5.visible = false;
	number6.visible = false;
	number7.visible = false;
	number8.visible = false;
	number9.visible = false;
}

function showNumbers(n) {

	if(n == 1){
		number1.visible = true;
	} 

	if(n == 3) {
		number1.visible = true;
    	number2.visible = true;
    	number3.visible = true;
	} 

	if(n == 5) {
		number1.visible = true;
    	number2.visible = true;
    	number3.visible = true;
    	number4.visible = true;
    	number5.visible = true;
	}

	if(n == 7) {
		number1.visible = true;
    	number2.visible = true;
    	number3.visible = true;
    	number4.visible = true;
    	number5.visible = true;
    	number6.visible = true;
    	number7.visible = true;
	}

	if(n == 9) {
		number1.visible = true;
    	number2.visible = true;
    	number3.visible = true;
    	number4.visible = true;
    	number5.visible = true;
    	number6.visible = true;
    	number7.visible = true;
    	number8.visible = true;
    	number9.visible = true;
	}
}

function full_and_sound(){ // Эту функцию вызывать в локациях
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

function createLevelButtons() {
    var lvl1 = game.add.sprite(0,0, 'x');
    lvl1.inputEnabled = true;
    lvl1.input.useHandCursor = true;
    lvl1.events.onInputUp.add(function () {
        game.state.start('game1');

        arrowSound.stop();
    }, this);

    var lvl2 = game.add.sprite(20, 0, 'x');
    lvl2.inputEnabled = true;
    lvl2.input.useHandCursor = true;
    lvl2.events.onInputUp.add(function () {
        game.state.start('game2');

        arrowSound.stop();
    }, this);

    var lvl3 = game.add.sprite(40, 0, 'x');
    lvl3.inputEnabled = true;
    lvl3.input.useHandCursor = true;
    lvl3.events.onInputUp.add(function () {
        game.state.start('game3');

        arrowSound.stop();
    }, this);

    var lvl4 = game.add.sprite(60, 0, 'x');
    lvl4.inputEnabled = true;
    lvl4.input.useHandCursor = true;
    lvl4.events.onInputUp.add(function () {
        game.state.start('game4');

        arrowSound.stop();
    }, this);
}

//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

(function () {

    var bars = [];
    var rotateSound;
    var stopSound;
    var tadaSound;
    var spinning = false;
    var barsCurrentSpins = [0, 0, 0, 0, 0];
    var barsTotalSpins = [];
    var spinningBars = 0;
    var button;
    var currentLine = 3;

    var lines = {
        1: {
            coord: 239,
            sprite: null,
            btncoord: 250,
            button: null
        },
        2: {
            coord: 96,
            sprite: null
        },
        3: {
            coord: 382,
            sprite: null,
            btncoord: 295,
            button: null
        },
        4: {
            coord: 150,
            sprite: null
        },
        5: {
            coord: 335 - 203,
            sprite: null,
            btncoord: 340,
            button: null
        },
        6: {
            coord: 123,
            sprite: null
        },
        7: {
            coord: 361 - 103,
            sprite: null,
            btncoord: 385,
            button: null
        },
        8: {
            coord: 263,
            sprite: null
        },
        9: {
            coord: 218 - 71,
            sprite: null,
            btncoord: 430,
            button: null
        }
    };

    var tmpSpins = 15;
    for (var i = 0; i < 5; ++i) {
        barsTotalSpins[i] = tmpSpins;
        tmpSpins += 12;
    }

    var game1 = {};


    function hideLines() {
        console.log(lines);
        for (var i in lines) {
            lines[i].sprite.visible = false;
        }
    }

    function selectLine(n) {
        currentLine = n;

        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.visible = true;
            lines[i].sprite.loadTexture('line_' + i);
        }
    }

    function preselectLine(n) {
        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.loadTexture('linefull_' + i);
            lines[i].sprite.visible = true;
        }
    }


    function createLevelButtons() {
        var lvl1 = game.add.sprite(0, 0, 'btnline11');
        lvl1.inputEnabled = true;
        lvl1.input.useHandCursor = true;
        lvl1.events.onInputDown.add(function () {
            lvl1.loadTexture('btnline_p11');
        }, this);
        lvl1.events.onInputUp.add(function () {
            lvl1.loadTexture('btnline11');
            game.state.start('game1');
        }, this);

        var lvl2 = game.add.sprite(20, 0, 'btnline13');
        lvl2.inputEnabled = true;
        lvl2.input.useHandCursor = true;
        lvl2.events.onInputDown.add(function () {
            lvl2.loadTexture('btnline_p13');
        }, this);
        lvl2.events.onInputUp.add(function () {
            lvl2.loadTexture('btnline13');
            game.state.start('game2');
        }, this);

        var lvl3 = game.add.sprite(40, 0, 'btnline15');
        lvl3.inputEnabled = true;
        lvl3.input.useHandCursor = true;
        lvl3.events.onInputDown.add(function () {
            lvl3.loadTexture('btnline_p15');
        }, this);
        lvl3.events.onInputUp.add(function () {
            lvl3.loadTexture('btnline15');
            game.state.start('game3');
        }, this);

        var lvl4 = game.add.sprite(60, 0, 'btnline15');
        lvl4.inputEnabled = true;
        lvl4.input.useHandCursor = true;
        lvl4.events.onInputDown.add(function () {
            lvl4.loadTexture('btnline_p15');
        }, this);
        lvl4.events.onInputUp.add(function () {
            lvl4.loadTexture('btnline15');
            game.state.start('game4');
        }, this);
    }

    game1.preload = function () {

    };

    game1.soundStatus = true;

    game1.create = function () {
        stopSound = game.add.audio('stop');
        stopSound.stop();

        var playSound = game.add.audio('play');
        rotateSound = game.add.audio('rotate');
        rotateSound.loop = true;
        stopSound = game.add.audio('stop');
        tadaSound = game.add.audio('tada');
        takeWin = game.add.audio('takeWin');
        takeWin.addMarker('take', 0, 0.6);
        takeBox = game.add.audio('takeBox');
        
        game.add.sprite(0,0, 'game.background');
        game.add.sprite(94,86, 'game.background4');

        selectGame = game.add.sprite(70,510, 'game.selectGame');
        selectGame.scale.setTo(0.7, 0.7);
        selectGame.inputEnabled = true;
        selectGame.input.useHandCursor = true;
        selectGame.events.onInputOver.add(function(){
            selectGame.loadTexture('game.selectGame_p');
        });
        selectGame.events.onInputOut.add(function(){
            selectGame.loadTexture('game.selectGame');
        });
        selectGame.events.onInputDown.add(function(){	
            
        });

        var payTable = game.add.sprite(150,510, 'game.payTable');
        payTable.scale.setTo(0.7, 0.7);
        payTable.inputEnabled = true;
        payTable.input.useHandCursor = true;
        payTable.events.onInputOver.add(function(){
            payTable.loadTexture('game.payTable_p');
        });
        payTable.events.onInputOut.add(function(){
            payTable.loadTexture('game.payTable');
        });
        payTable.events.onInputDown.add(function(){
            

        });


        betOneSound = game.add.audio('game.betOneSound');
        var betone = game.add.sprite(490,510, 'game.betone');
        betone.scale.setTo(0.7, 0.7);
        betone.inputEnabled = true;
        betone.input.useHandCursor = true;
        betone.events.onInputOver.add(function(){
            betone.loadTexture('game.betone_p');
        });
        betone.events.onInputDown.add(function(){
            betOneSound.play();
        });
        betone.events.onInputOut.add(function(){
            betone.loadTexture('game.betone');
        });

        betMaxSound = game.add.audio('game.betMaxSound');
        var betmax = game.add.sprite(535,510, 'game.betmax');
        betmax.scale.setTo(0.7, 0.7);
        betmax.inputEnabled = true;
        betmax.input.useHandCursor = true;
        betmax.events.onInputOver.add(function(){
            betmax.loadTexture('game.betmax_p');
        });
        betmax.events.onInputDown.add(function(){
            betMaxSound.play();
        });
        betmax.events.onInputOut.add(function(){
            betmax.loadTexture('game.betmax');
        });

        var automaricstart = game.add.sprite(685,510, 'game.automaricstart');
        automaricstart.scale.setTo(0.7, 0.7);
        automaricstart.inputEnabled = true;
        automaricstart.input.useHandCursor = true;
        automaricstart.events.onInputOver.add(function(){
            automaricstart.loadTexture('game.automaricstart_p');
        });
        automaricstart.events.onInputOut.add(function(){
            automaricstart.loadTexture('game.automaricstart');
        });

        take = game.add.sprite(566,439, 'game.take');
        take.animations.add('take', [0,1], 1, true);
        take.animations.getAnimation('take').play();
        take.visible = false;

        take_or_risk = game.add.sprite(556,439, 'game.take_or_risk');
        take_or_risk.animations.add('take_or_risk', [0,1], 1, true);
        take_or_risk.animations.getAnimation('take_or_risk').play();
        take_or_risk.visible = false;

        play_1_to = game.add.sprite(556,439, 'game.play_1_to');
        play_1_to.animations.add('play_1_to', [0,1], 1, true);
        play_1_to.animations.getAnimation('play_1_to').play();
        play_1_to.visible = false;

        lock_bonus_game = game.add.sprite(545,436, 'game.lock_bonus_game');
        lock_bonus_game.visible = false;

        box_bonus_game = game.add.sprite(545,436, 'game.box_bonus_game');
        box_bonus_game.visible = false;


        automaricstart.events.onInputDown.add(function(){
            super_key_in_tablo.visible = false;

            var rand = 0 + Math.random() * (6 - 0)
            rand = Math.round(rand);

            switch (rand) {
                case 1:
                    take.visible = true;

                    box_bonus_game.visible = false;
                    lock_bonus_game.visible = false;
                    play_1_to.visible = false;
                    take_or_risk.visible = false;
                    break;
                case 2:
                    take_or_risk.visible = true;

                    take.visible = false;
                    box_bonus_game.visible = false;
                    lock_bonus_game.visible = false;
                    play_1_to.visible = false;
                    break;
                case 3:
                    play_1_to.visible = true;

                    take.visible = false;
                    box_bonus_game.visible = false;
                    lock_bonus_game.visible = false;
                    take_or_risk.visible = false;
                    break;
                case 5:
                    lock_bonus_game.visible = true;

                    take.visible = false;
                    box_bonus_game.visible = false;
                    take_or_risk.visible = false;
                    play_1_to.visible = true;
                    break;
                case 6:
                    box_bonus_game.visible = true;

                    lock_bonus_game.visible = true;
                    take.visible = false;
                    take_or_risk.visible = false;
                    play_1_to.visible = true;
                    break;
            }

        });



        var positions = [
            game.world.centerX - 225,
            game.world.centerX - 113,
            game.world.centerX - 0,
            game.world.centerX + 111,
            game.world.centerX + 223
        ];

        for (var i = 0; i < 5; ++i) {
            bars[i] = game.add.tileSprite(positions[i], game.world.centerY - 53, 96, 320, 'game.bar');
            bars[i].anchor.setTo(0.5, 0.5);
            bars[i].tilePosition.y = randomNumber(0, 8) * 112 - 8;
        }

        window.test = function () {
            bars[0].tilePosition.y -= 1;
        };

        function randomiseSpin() {
            return [
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106
            ];
        }

        button = game.add.sprite(627, 540, 'game.start');
        button.scale.setTo(0.7,0.7);

        line1x = game.add.sprite(250,510, 'game.line1_d');
        line1x.scale.setTo(0.7, 0.7);
        line3x = game.add.sprite(295,510, 'game.line3_d');
        line3x.scale.setTo(0.7, 0.7);
        line5x = game.add.sprite(340,510, 'game.line5_d');
        line5x.scale.setTo(0.7, 0.7);
        line7x = game.add.sprite(385,510, 'game.line7_d');
        line7x.scale.setTo(0.7, 0.7);
        line9x = game.add.sprite(430,510, 'game.line9_d');
        line9x.scale.setTo(0.7, 0.7);
        selectGamex = game.add.sprite(70,510, 'game.selectGame_d');
        selectGamex.scale.setTo(0.7, 0.7);
        payTablex = game.add.sprite(150,510, 'game.payTable_d');
        payTablex.scale.setTo(0.7, 0.7);
        betonex = game.add.sprite(490,510, 'game.betone_d');
        betonex.scale.setTo(0.7, 0.7);
        betmaxx = game.add.sprite(535,510, 'game.betmax_d');
        betmaxx.scale.setTo(0.7, 0.7);
        automaricstartx = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstartx.scale.setTo(0.7, 0.7);

        line1x.visible = false;
        line3x.visible = false;
        line5x.visible = false;
        line7x.visible = false;
        line9x.visible = false;
        selectGamex.visible = false;
        payTablex.visible = false;
        betonex.visible = false;
        betmaxx.visible = false;
        automaricstartx.visible = false;

        game.check_win = 0;

        function buttonClicked() {
        	
    		if (spinning) {
                return;
            }

            if(game.check_win == 0) {
            	lines[1].button.visible = false;
	            lines[3].button.visible = false;
	            lines[5].button.visible = false;
	            lines[7].button.visible = false;
	            lines[9].button.visible = false;

	            line1x.visible = true;
	            line3x.visible = true;
	            line5x.visible = true;
	            line7x.visible = true;
	            line9x.visible = true;
	            selectGamex.visible = true;
	            payTablex.visible = true;
	            betonex.visible = true;
	            betmaxx.visible = true;
	            automaricstartx.visible = true;
	            button.loadTexture('game.start_d');
            }
            
        }

        function buttonRelease() {
        	if(game.check_win == 1) {
        		hideLines();
        		selectLine(3);
        		takeWin.play('take');
        		lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                line1x.visible = false;
                line3x.visible = false;
                line5x.visible = false;
                line7x.visible = false;
                line9x.visible = false;
                selectGamex.visible = false;
                payTablex.visible = false;
                betonex.visible = false;
                betmaxx.visible = false;
                automaricstartx.visible = false;
                button.loadTexture('game.start');

                game.check_win = 0;

                flashNamber1.animations.stop();
                flashNamber2.animations.stop();
                flashNamber3.animations.stop();

                flashNamber1.visible = false;
                flashNamber2.visible = false;
                flashNamber3.visible = false;
        	} else {
        		if (spinning) {
	                return;
	            }
	            hideLines();
	            barsCurrentSpins = [0, 0, 0, 0, 0];
	            spinningBars = bars.length;
	            spinning = true;
	            playSound.play();
        	}
        }

        button.anchor.setTo(0.5, 0.5);
        button.inputEnabled = true;
        button.input.useHandCursor = true;
        button.events.onInputDown.add(buttonClicked, this);
        button.events.onInputUp.add(buttonRelease, this);

        for (var i = 1; i <= 9; ++i) {
            lines[i].sprite = game.add.sprite(134, lines[i].coord, 'line_' + i);
            lines[i].sprite.visible = false;
            if (i % 2 != 0) {
                lines[i].sound = game.add.audio('line' + i);
                lines[i].button = game.add.sprite(lines[i].btncoord, 510, 'btnline' + i);
                lines[i].button.scale.setTo(0.7,0.7);
                lines[i].button.inputEnabled = true;
                lines[i].button.input.useHandCursor = true;
                (function (n) {
                    lines[n].button.events.onInputDown.add(function () {

                        hideLines();
                        preselectLine(n);

                        hideNumbers();
                        showNumbers(n);

                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                    lines[n].button.events.onInputUp.add(function () {
                        hideLines();
                        selectLine(n);
                        lines[n].button.loadTexture('btnline' + n);
                        lines[n].sound.play();
                    }, this);
                    lines[n].button.events.onInputOut.add(function () {
                        lines[n].button.loadTexture('btnline' + n);
                    }, this);
                    lines[n].button.events.onInputOver.add(function () {
                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                })(i);
            }
        }

        number1 = game.add.sprite(94,230, 'game.number1');
		number2 = game.add.sprite(94,86, 'game.number2');
		number3 = game.add.sprite(94,374, 'game.number3');
		number4 = game.add.sprite(94,150, 'game.number4');
		number5 = game.add.sprite(94,310, 'game.number5');
		number6 = game.add.sprite(94,118, 'game.number6');
		number7 = game.add.sprite(94,342, 'game.number7');
		number8 = game.add.sprite(94,262, 'game.number8');
		number9 = game.add.sprite(94,197, 'game.number9');

        preselectLine(9);

        createLevelButtons();
        full_and_sound();

        //создание анимаций
        
        cat = game.add.sprite(95,406, 'game.cat');
        super_key_in_tablo = game.add.sprite(540,436, 'game.super_key_in_tablo');

        
        man1 = game.add.sprite(222,390, 'game.man1');
        man2 = game.add.sprite(222,390, 'game.man2');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);

        //создание анимации для человека
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        super_key_in_tablo.animations.add('super_key_in_tablo', [0,1,2,3,4,5,6,7,8,9,10], 1, true);
        
        cat.animations.getAnimation('cat').play();
        super_key_in_tablo.animations.getAnimation('super_key_in_tablo').play();


    };



    game1.update = function () {
		if (spinning) {
            for (var i in bars) {
                barsCurrentSpins[i]++;
                if (barsCurrentSpins[i] < barsTotalSpins[i]) {
                    bars[i].tilePosition.y += 112;
                } else if (barsCurrentSpins[i] == barsTotalSpins[i]) {
                    spinningBars--;
                }
            }
            if (!spinningBars) {
                spinning = false;
                rotateSound.stop();
                button.loadTexture('game.start');
                selectLine(currentLine);
                console.log('spin end');
                if (currentLine == 3) {
                    preselectLine(3);
                    tadaSound.play();

                    hideNumbers();

                    game.check_win = 1;

                    hideLines();
                    lockDisplay();
                    var openLockFromSlot = game.add.sprite(478,189, 'game.openLockFromSlot');
                    openLockFromSlot.animations.add('openLockFromSlot', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 9, false);
                    openLockFromSlot.visible = true;

                    openLockFromSlot.animations.getAnimation('openLockFromSlot').play().onComplete.add(function(){
                        openLockFromSlot.visible = false;
                        unlockDisplay();
                    });

                    flashNamber1 = game.add.sprite(94,230, 'game.flashNamber1');
                    flashNamber1.animations.add('flashNamber1', [0,1], 1, true);
                    flashNamber1.animations.getAnimation('flashNamber1').play();

                    flashNamber2 = game.add.sprite(94,86, 'game.flashNamber2');
                    flashNamber2.animations.add('flashNamber2', [0,1], 1, true);
                    flashNamber2.animations.getAnimation('flashNamber2').play();

                    flashNamber3 = game.add.sprite(94,374, 'game.flashNamber3');
                    flashNamber3.animations.add('flashNamber3', [0,1], 1, true);
                    flashNamber3.animations.getAnimation('flashNamber3').play();

                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                } else if (currentLine == 5) {
                	takeBox.play();
                	game.check_win = 0;
                	hideLines();
		            lockDisplay();
		            var openBoxFromSlot1 = game.add.sprite(254,190, 'game.openBoxFromSlot');
		            openBoxFromSlot1.animations.add('openBoxFromSlot1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26], 18, false);
		            openBoxFromSlot1.visible = true;
		            openBoxFromSlot1.animations.getAnimation('openBoxFromSlot1').play().onComplete.add(function(){
		                openBoxFromSlot1.visible = false;
		                unlockDisplay();
		            });
		            var openBoxFromSlot2 = game.add.sprite(366,190, 'game.openBoxFromSlot');
		            openBoxFromSlot2.animations.add('openBoxFromSlot2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26], 18, false);
		            openBoxFromSlot2.visible = true;
		            openBoxFromSlot2.animations.getAnimation('openBoxFromSlot2').play().onComplete.add(function(){
		                openBoxFromSlot2.visible = false;
		                unlockDisplay();
		            });
		            var openBoxFromSlot3 = game.add.sprite(478,190, 'game.openBoxFromSlot');
		            openBoxFromSlot3.animations.add('openBoxFromSlot3', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26], 18, false);
		            openBoxFromSlot3.visible = true;
		            openBoxFromSlot3.animations.getAnimation('openBoxFromSlot3').play().onComplete.add(function(){
		                openBoxFromSlot3.visible = false;
		                unlockDisplay();

		                /*lines[1].button.visible = true;
	                    lines[3].button.visible = true;
	                    lines[5].button.visible = true;
	                    lines[7].button.visible = true;
	                    lines[9].button.visible = true;

	                    line1x.visible = false;
	                    line3x.visible = false;
	                    line5x.visible = false;
	                    line7x.visible = false;
	                    line9x.visible = false;
	                    selectGamex.visible = false;
	                    payTablex.visible = false;
	                    betonex.visible = false;
	                    betmaxx.visible = false;
	                    automaricstartx.visible = false;*/

	                    game.state.start('game4');
		            });
                } else {
					lines[1].button.visible = true;
                    lines[3].button.visible = true;
                    lines[5].button.visible = true;
                    lines[7].button.visible = true;
                    lines[9].button.visible = true;

                    line1x.visible = false;
                    line3x.visible = false;
                    line5x.visible = false;
                    line7x.visible = false;
                    line9x.visible = false;
                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                }
            }
        }
    	
    };

    game.state.add('game1', game1);

})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================

(function () {

    var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {
        //Добавление фона
        background = game.add.sprite(0,0, 'game.background');
        background2 = game.add.sprite(94,54, 'game.background2');

        arrowSound = game.add.audio('game.arrowSound');
        arrowSound.loop = true;
        arrowSound.play();

        openKeySound = game.add.audio('game.openKeySound');
        openKeyBoxLoseSound = game.add.audio('game.openKeyBoxLoseSound');
        openKeyBoxPreLoseSound = game.add.audio('game.openKeyBoxPreLoseSound');
        openKeyBoxWinSound = game.add.audio('game.openKeyBoxWinSound');

        //добавление кнопок

        var startButton = game.add.sprite(596, 511, 'game.start');
        startButton.scale.setTo(0.7, 0.7);
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOut.add(function(){
            startButton.loadTexture('game.start');
        });

        var selectGame = game.add.sprite(70,510, 'game.selectGame');
        selectGame.scale.setTo(0.7, 0.7);
        selectGame.inputEnabled = true;
        selectGame.input.useHandCursor = true;
        selectGame.events.onInputOver.add(function(){
            selectGame.loadTexture('game.selectGame_p');
        });
        selectGame.events.onInputOut.add(function(){
            selectGame.loadTexture('game.selectGame');
        });

        var payTable = game.add.sprite(150,510, 'game.payTable');
        payTable.scale.setTo(0.7, 0.7);
        payTable.inputEnabled = true;
        payTable.input.useHandCursor = true;
        payTable.events.onInputOver.add(function(){
            payTable.loadTexture('game.payTable_p');
        });
        payTable.events.onInputOut.add(function(){
            payTable.loadTexture('game.payTable');
        });

        var line1 = game.add.sprite(250,510, 'game.line1_d');
        line1.scale.setTo(0.7, 0.7);
        /*line1.inputEnabled = true;
        line1.input.useHandCursor = true;*/
        line1.events.onInputUp.add(function(){
            line1.loadTexture('game.line1');
        });
        line1.events.onInputOver.add(function(){
            line1.loadTexture('game.line1_p');
        });
        line1.events.onInputOut.add(function(){
            line1.loadTexture('game.line1');
        });
        line1.events.onInputDown.add(function(){
            arrowSound.stop();
            openKeySound.play();

            lock5.visible = false;
            arrowRedBlue5.visible = false;

            var findAKey = game.add.sprite(367,326, 'game.findAKey');
            findAKey.animations.add('findAKey', [0,1], 1.5, true);
            findAKey.visible = false;
            findAKey.animations.getAnimation('findAKey').play();

            var openLock2 = game.add.sprite(398,326, 'game.openLock2');
            openLock2.animations.add('openLock2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, false);
            openLock2.visible = true;
            openLock2.animations.getAnimation('openLock2').play().onComplete.add(function(){
                openLock2.visible = false;
                findAKey.visible = true;
            });
        });

        var line3 = game.add.sprite(295,510, 'game.line3');
        line3.scale.setTo(0.7, 0.7);
        line3.inputEnabled = true;
        line3.input.useHandCursor = true;
        line3.events.onInputUp.add(function(){
        	lockDisplay();
            line3.loadTexture('game.line3');
            /*arrowSound.stop();

            openKeyBoxLoseSound.play();

            var openLoseLeftCase = game.add.sprite(173,288, 'game.openLoseLeftCase');
            openLoseLeftCase.animations.add('openLoseLeftCase', [0,1,2,3,4,4,4,4,4,4,4], 5, false);
            openLoseLeftCase.visible = true;
            openLoseLeftCase.animations.getAnimation('openLoseLeftCase').play().onComplete.add(function(){
                openLoseLeftCase.visible = false;
            });*/

            arrowSound.stop();
            openKeyBoxWinSound.play();

            var openWinLeftCase = game.add.sprite(173,288, 'game.openWinLeftCase');
            openWinLeftCase.animations.add('openWinLeftCase', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
            openWinLeftCase.visible = true;
            openWinLeftCase.animations.getAnimation('openWinLeftCase').play().onComplete.add(function(){
                //openWinLeftCase.visible = false;

                arrowSound.stop();
	            openKeySound.play();

	            lock5.visible = false;
	            arrowRedBlue5.visible = false;

	            var findAKey = game.add.sprite(367,326, 'game.findAKey');
	            findAKey.animations.add('findAKey', [0,1], 1.5, true);
	            findAKey.visible = false;
	            findAKey.animations.getAnimation('findAKey').play();

	            var openLock2 = game.add.sprite(398,326, 'game.openLock2');
	            openLock2.animations.add('openLock2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, false);
	            openLock2.visible = true;
	            openLock2.animations.getAnimation('openLock2').play().onComplete.add(function(){
	                openLock2.visible = false;
	                findAKey.visible = true;
	            });

	            setTimeout("unlockDisplay();", 4000);

            });
        });
        line3.events.onInputOver.add(function(){
            line3.loadTexture('game.line3_p');
        });
        line3.events.onInputOut.add(function(){
            line3.loadTexture('game.line3');
        });

        var line5 = game.add.sprite(340,510, 'game.line5_d');
        line5.scale.setTo(0.7, 0.7);
        /*line5.inputEnabled = true;
        line5.input.useHandCursor = true;*/
        line5.events.onInputOver.add(function(){
            line5.loadTexture('game.line5_p');
        });
        line5.events.onInputDown.add(function(){
            arrowSound.stop();
            openKeyBoxWinSound.play();

            var openWinLeftCase = game.add.sprite(173,288, 'game.openWinLeftCase');
            openWinLeftCase.animations.add('openWinLeftCase', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
            openWinLeftCase.visible = true;
            openWinLeftCase.animations.getAnimation('openWinLeftCase').play().onComplete.add(function(){
                openWinLeftCase.visible = false;
            });

        });
        line5.events.onInputOut.add(function(){
            line5.loadTexture('game.line5');
        });


        var line7 = game.add.sprite(385,510, 'game.line7');
        line7.scale.setTo(0.7, 0.7);
        line7.inputEnabled = true;
        line7.input.useHandCursor = true;
        line7.events.onInputOver.add(function(){
            line7.loadTexture('game.line7_p');
        });
        line7.events.onInputDown.add(function(){
        	lockDisplay();
            arrowSound.stop();
            openKeyBoxLoseSound.play();

            var openLoseRightCase = game.add.sprite(462,288, 'game.openLoseRightCase');
            openLoseRightCase.animations.add('openLoseRightCase', [0,1,2,3,4,4,4,4,4,4,4], 5, false);
            openLoseRightCase.visible = true;
            openLoseRightCase.animations.getAnimation('openLoseRightCase').play().onComplete.add(function(){
                openLoseRightCase.visible = false;
                unlockDisplay();

                game.state.start('game1');
            });
        });
        line7.events.onInputOut.add(function(){
            line7.loadTexture('game.line7');
        });

        var line9 = game.add.sprite(430,510, 'game.line9_d');
        line9.scale.setTo(0.7, 0.7);
        /*line9.inputEnabled = true;
        line9.input.useHandCursor = true;*/
        line9.events.onInputOver.add(function(){
            line9.loadTexture('game.line9_p');
        });
        line9.events.onInputDown.add(function(){
            arrowSound.stop();
            openKeyBoxWinSound.play();
            openKeyBoxPreLoseSound.play();

            var openWinRightCase = game.add.sprite(463,288, 'game.openWinRightCase');
            openWinRightCase.animations.add('openWinRightCase', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
            openWinRightCase.visible = true;
            openWinRightCase.animations.getAnimation('openWinRightCase').play().onComplete.add(function(){
                openWinRightCase.visible = false;
            });

        });
        line9.events.onInputOut.add(function(){
            line9.loadTexture('game.line9');
        });

        var betone = game.add.sprite(490,510, 'game.betone');
        betone.scale.setTo(0.7, 0.7);
        betone.inputEnabled = true;
        betone.input.useHandCursor = true;
        betone.events.onInputOver.add(function(){
            betone.loadTexture('game.betone_p');
        });
        betone.events.onInputOut.add(function(){
            betone.loadTexture('game.betone');
        });

        var betmax = game.add.sprite(535,510, 'game.betmax');
        betmax.scale.setTo(0.7, 0.7);
        betmax.inputEnabled = true;
        betmax.input.useHandCursor = true;
        betmax.events.onInputOver.add(function(){
            betmax.loadTexture('game.betmax_p');
        });
        betmax.events.onInputOut.add(function(){
            betmax.loadTexture('game.betmax');
        });

        var automaricstart = game.add.sprite(685,510, 'game.automaricstart');
        automaricstart.scale.setTo(0.7, 0.7);
        automaricstart.inputEnabled = true;
        automaricstart.input.useHandCursor = true;
        automaricstart.events.onInputOver.add(function(){
            automaricstart.loadTexture('game.automaricstart_p');
        });
        automaricstart.events.onInputOut.add(function(){
            automaricstart.loadTexture('game.automaricstart');
        });

        //создание цифр для верхнего табло
/*        number0_table1 = game.add.sprite(285,32, 'game.number0_table1');
        number0_table1 = game.add.sprite(495,32, 'game.number0_table1');
        number0_table1 = game.add.sprite(690,32, 'game.number0_table1');*/

        //lock
        lock1 = game.add.sprite(398,278, 'game.lock');
        lock2 = game.add.sprite(398,230, 'game.lock');
        lock3 = game.add.sprite(398,182, 'game.lock');
        lock4 = game.add.sprite(398,134, 'game.lock');
        lock5 = game.add.sprite(398,326, 'game.lock');

        box_for_keys = game.add.sprite(268,256, 'game.box_for_keys');

        /*arrows5 = game.add.sprite(367,308, 'game.arrows');
         arrows4 = game.add.sprite(367,260, 'game.arrows');
         arrows3 = game.add.sprite(367,212, 'game.arrows');
         arrows2 = game.add.sprite(367,164, 'game.arrows');
         arrows1 = game.add.sprite(367,116, 'game.arrows');*/

        //superkey = game.add.sprite(50,120,'game.superkey');
        //game.physics.enable(superkey, Phaser.Physics.ARCADE);

        //создание анимаций
        
        cat = game.add.sprite(95,406, 'game.cat');
        
        arrowRedBlue1 = game.add.sprite(365,112, 'game.arrowRedBlue');
        arrowRedBlue2 = game.add.sprite(365,161, 'game.arrowRedBlue');
        arrowRedBlue3 = game.add.sprite(365,209, 'game.arrowRedBlue');
        arrowRedBlue4 = game.add.sprite(365,256, 'game.arrowRedBlue');
        arrowRedBlue5 = game.add.sprite(365,305, 'game.arrowRedBlue');

        //создание анимации для человека
        
        cat.animations.add('cat', [0,1,2,3,4,5], 2, true);
        
        arrowRedBlue1.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue2.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue3.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue4.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue5.animations.add('arrowRedBlue', [0,1], 1.5, true);

        
        cat.animations.getAnimation('cat').play();
        arrowRedBlue1.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue2.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue3.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue4.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue5.animations.getAnimation('arrowRedBlue').play();

        move_super_key1 = game.add.sprite(537,109, 'game.move_super_key1');
        move_super_key1.inputEnabled = true;
        move_super_key1.input.useHandCursor = true;
        move_super_key1.events.onInputDown.add(function(){
            if(check_super_key == 0) {
                game.add.sprite(396,369, 'game.small_red_key');
                check_super_key = 1;
            }
        });
        move_super_key1.animations.add('move_super_key1', [0,1,2,3,4,5,6,7,8,9,10,11,12,14,16,18,20,22,24,26,28,30,32], 12, true);

        move_super_key2 = game.add.sprite(537,109, 'game.move_super_key2');
        move_super_key2.inputEnabled = true;
        move_super_key2.input.useHandCursor = true;
        move_super_key2.events.onInputDown.add(function(){
            if(check_super_key == 0) {
                game.add.sprite(396,369, 'game.small_red_key');
                check_super_key = 1;
            }
        });
        move_super_key2.animations.add('move_super_key2', [1,2,3,4], 12, true);

        move_super_key1.visible = false;
        move_super_key2.visible = false;

        var superkeyNumber = 1;

        function superkeyAnim(){
            if(superkeyNumber == 1){
                move_super_key1.visible = true;
                move_super_key1.animations.getAnimation('move_super_key1').play().onComplete.add(function(){
                    move_super_key1.visible = false;
                    superkeyNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                move_super_key2.visible = true;
                move_super_key2.animations.getAnimation('move_super_key2').play().onComplete.add(function(){
                    move_super_key2.visible = false;
                    superkeyNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        superkeyAnim(superkeyNumber);

        man1 = game.add.sprite(222,390, 'game.man1');
        man2 = game.add.sprite(222,390, 'game.man2');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);

        createLevelButtons();
        full_and_sound();
    };

    game2.update = function () {
    };

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================

function randomCard() {
    var arr = ['2b','3b','4b','5b','6b','7b','8b','9b','10b','jb','qb','kb','ab','2ch','3ch','4ch','5ch','6ch','7ch','8ch','9ch','10ch','jch','qch','kch','ach','2k','3k','4k','5k','6k','7k','8k','9k',
        '10k','jk','qk','kk','ak','2p','3p','4p','5p','6p','7p','8p','9p','10p','jp','qp','kp','ap','joker'];

    var rand = Math.floor(Math.random() * arr.length);

    return 'game.card_'+arr[rand];
}

function hidePick() {
	pick2.visible = false;
	pick3.visible = false;
	pick4.visible = false;
	pick5.visible = false;
}

function openCardSound() {
    openCardAudio.play();
}

function lineButtonHide() {
	line3 = game.add.sprite(295,510, 'game.line3');
}

function lineButtonShow() {
	line3.visible = true;
	line5.visible = true;
	line7.visible = true;
	line9.visible = true;

	line3x.visible = false;
	line5x.visible = false;
	line7x.visible = false;
	line9x.visible = false;
}

function lineButtonHide() {
	line3.visible = false;
	line5.visible = false;
	line7.visible = false;
	line9.visible = false;

	line3x.visible = true;
	line5x.visible = true;
	line7x.visible = true;
	line9x.visible = true;
}

(function () {

    var button;

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {
        background = game.add.sprite(0,0, 'game.background');

        function humanThought_forward() {
            humanThought = game.add.sprite(325,305, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,2,2,2], 3, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }
        function humanThought_loss() {
            humanThought = game.add.sprite(325,305, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,1,1,1], 3, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }
        function humanThought_win() {
            humanThought = game.add.sprite(325,305, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,0,0,0], 3, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }

        var startButton = game.add.sprite(596, 511, 'game.start');
        startButton.scale.setTo(0.7, 0.7);
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOut.add(function(){
            startButton.loadTexture('game.start');
        });

        selectGame = game.add.sprite(70,510, 'game.selectGame_d');
        selectGame.scale.setTo(0.7, 0.7);

        payTable = game.add.sprite(150,510, 'game.payTable_d');
        payTable.scale.setTo(0.7, 0.7);

        line1 = game.add.sprite(250,510, 'game.line1_d');
        line1.scale.setTo(0.7, 0.7);

        line3 = game.add.sprite(295,510, 'game.line3');
        line3.scale.setTo(0.7, 0.7);
        line3.inputEnabled = true;
        line3.input.useHandCursor = true;
        line3.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            humanThought_win();

            card2.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            lineButtonHide();
            
            setTimeout(' card3.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick5.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick(); lineButtonShow();',
                3000);
        });
        line3.events.onInputOver.add(function(){
            line3.loadTexture('game.line3_p');
        });
        line3.events.onInputOut.add(function(){
            line3.loadTexture('game.line3');
        });

        line5 = game.add.sprite(340,510, 'game.line5');
        line5.scale.setTo(0.7, 0.7);
        line5.inputEnabled = true;
        line5.input.useHandCursor = true;
        line5.events.onInputOver.add(function(){
            line5.loadTexture('game.line5_p');
        });
        line5.events.onInputOut.add(function(){
            line5.loadTexture('game.line5');
        });
        line5.events.onInputDown.add(function(){
            lockDisplay();
            humanThought_loss();
            setTimeout('unlockDisplay();',3000);
            lineButtonHide();
            card3.loadTexture(randomCard());
            openCardSound();
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick4.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick(); lineButtonShow(); game.state.start("game1");',
                3000);
        });

        line7 = game.add.sprite(385,510, 'game.line7');
        line7.scale.setTo(0.7, 0.7);
        line7.inputEnabled = true;
        line7.input.useHandCursor = true;
        line7.events.onInputOver.add(function(){
            line7.loadTexture('game.line7_p');
        });
        line7.events.onInputOut.add(function(){
            line7.loadTexture('game.line7');
        });
        line7.events.onInputDown.add(function(){
            humanThought_win();
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            lineButtonHide();
            card4.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick3.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage");hidePick(); lineButtonShow();',
                3000);
        });

        line9 = game.add.sprite(430,510, 'game.line9');
        line9.scale.setTo(0.7, 0.7);
        line9.inputEnabled = true;
        line9.input.useHandCursor = true;
        line9.events.onInputOver.add(function(){
            line9.loadTexture('game.line9_p');
        });
        line9.events.onInputOut.add(function(){
            line9.loadTexture('game.line9');
        });
        line9.events.onInputDown.add(function(){
            lockDisplay();
            humanThought_loss();
            setTimeout('unlockDisplay();',3000);
            openCardSound();
            card5.loadTexture(randomCard());
            pick2.visible = true;
            lineButtonHide();
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card4.loadTexture(randomCard());',1000);

            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage");' +
                'card4.loadTexture("game.card_garage");' +
                'card5.loadTexture("game.card_garage");hidePick(); lineButtonShow(); game.state.start("game1");',
                3000);
        });

        line3x = game.add.sprite(295,510, 'game.line3_d');
        line5x = game.add.sprite(340,510, 'game.line5_d');
        line7x = game.add.sprite(385,510, 'game.line7_d');
        line9x = game.add.sprite(430,510, 'game.line9_d');
        line3x.scale.setTo(0.7, 0.7);
        line5x.scale.setTo(0.7, 0.7);
        line7x.scale.setTo(0.7, 0.7);
        line9x.scale.setTo(0.7, 0.7);
        line3x.visible = false;
        line5x.visible = false;
        line7x.visible = false;
        line9x.visible = false;

        betone = game.add.sprite(490,510, 'game.betone_d');
        betone.scale.setTo(0.7, 0.7);

        betmax = game.add.sprite(535,510, 'game.betmax_d');
        betmax.scale.setTo(0.7, 0.7);

        automaricstart = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstart.scale.setTo(0.7, 0.7);

        //создание цифр для верхнего табло
/*        number0_table1 = game.add.sprite(285,32, 'game.number0_table1');
        number0_table1 = game.add.sprite(495,32, 'game.number0_table1');
        number0_table1 = game.add.sprite(690,32, 'game.number0_table1');*/



        //изображения для данной страницы
        cards_bg = game.add.sprite(94,87, 'game.cards_bg');


        take_or_risk = game.add.sprite(556,439, 'game.take_or_risk');
        take_or_risk.animations.add('take_or_risk', [0,1], 1, true);
        take_or_risk.animations.getAnimation('take_or_risk').play();

        openCardAudio = game.add.audio("game.openCardAudio");
        winCards = game.add.audio("game.winCards");

        lockDisplay();
        setTimeout("unlockDisplay();",500);
        setTimeout('openCardSound(); card1 = game.add.sprite(130,137, randomCard());',500);

        card1 = game.add.sprite(130,137, 'game.card_garage');
        card2 = game.add.sprite(263,137, 'game.card_garage');
        card3 = game.add.sprite(373,137, 'game.card_garage');
        card4 = game.add.sprite(483,137, 'game.card_garage');
        card5 = game.add.sprite(593,137, 'game.card_garage');

        pick2 = game.add.sprite(605,300, 'game.pick');
        pick3 = game.add.sprite(495,300, 'game.pick');
        pick4 = game.add.sprite(385,300, 'game.pick');
        pick5 = game.add.sprite(275,300, 'game.pick');
        pick2.visible = false;
        pick3.visible = false;
        pick4.visible = false;
        pick5.visible = false;

        //создание анимаций
		
        cat = game.add.sprite(95,406, 'game.cat');

        //создание анимации для человека
        
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        
        cat.animations.getAnimation('cat').play();

        man1 = game.add.sprite(222,390, 'game.man1');
        man2 = game.add.sprite(222,390, 'game.man2');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);

        createLevelButtons();
        full_and_sound();
    };

    game3.update = function () {

    };

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================

function randomBox() {
    //добавление массивов изображений
    var arr = [];

    var boxClock = 'game.clockBox';
    var boxHammer = 'game.HummerBox';
    var boxJack = 'game.jackBox';
    var boxFlashlight = 'game.franchBox';
    var boxSaw = 'game.sawBox';
    var boxHammer2 = 'game.hammer2Box';
    var boxWrench = 'game.flashBox';
    var boxDatail = 'game.detailBox';
    var boxPolice = 'game.policeBox';

    arr.push(boxClock,boxHammer,boxJack,boxFlashlight,boxSaw,boxHammer2,boxWrench,boxDatail,boxPolice);

    var rand = Math.floor(Math.random() * arr.length);

    return arr[rand];
}

(function () {

    var button;

    var game4 = {};

    game4.preload = function () {

    };

    game4.create = function () {

        openBoxSound = game.add.sound('game.openBoxSound');
        openPoliceBox = game.add.sound('game.openPoliceBox');
        preOpenBox = game.add.sound('game.preOpenBox');

        //Добавление фона
        background = game.add.sprite(0,0, 'game.background');
        background2 = game.add.sprite(95,54, 'game.background3');

        red_column_1 = game.add.sprite(303,150, 'game.red_column_1_1');
        red_column_2 = game.add.sprite(399,150, 'game.red_column_2_1');
        red_column_3 = game.add.sprite(495,150, 'game.red_column_3_1');

        //добавление кнопок
        var startButton = game.add.sprite(596, 511, 'game.start');
        startButton.scale.setTo(0.7, 0.7);
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOut.add(function(){
            startButton.loadTexture('game.start');
        });

        selectGame = game.add.sprite(70,510, 'game.selectGame');
        selectGame.scale.setTo(0.7, 0.7);
        selectGame.inputEnabled = true;
        selectGame.input.useHandCursor = true;
        selectGame.events.onInputOver.add(function(){
            selectGame.loadTexture('game.selectGame_p');
        });
        selectGame.events.onInputOut.add(function(){
            selectGame.loadTexture('game.selectGame');
        });

        payTable = game.add.sprite(150,510, 'game.payTable');
        payTable.scale.setTo(0.7, 0.7);
        payTable.inputEnabled = true;
        payTable.input.useHandCursor = true;
        payTable.events.onInputOver.add(function(){
            payTable.loadTexture('game.payTable_p');
        });
        payTable.events.onInputOut.add(function(){
            payTable.loadTexture('game.payTable');
        });

        line1 = game.add.sprite(250,510, 'game.line1');
        line1.scale.setTo(0.7, 0.7);
        line1.inputEnabled = true;
        line1.input.useHandCursor = true;
        line1.events.onInputUp.add(function(){
            line1.loadTexture('game.line1');
        });
        line1.events.onInputOver.add(function(){
            line1.loadTexture('game.line1_p');
        });
        line1.events.onInputOut.add(function(){
            line1.loadTexture('game.line1');
        });
        line1.events.onInputDown.add(function(){
            preOpenBox.play();
            box = game.add.sprite(130,185, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(200,320, 'game.number1_for_box');
                red_column_1.loadTexture('game.red_column_1_2');
            });
            line1.loadTexture('game.line1_d');
            line1.inputEnabled = false;
            line1.input.useHandCursor = false;
            line1.visble = false;
            line1x = game.add.sprite(250,510, 'game.line1_d');
            line1x.scale.setTo(0.7, 0.7);
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
        });

        box2 = game.add.sprite(230,185, randomBox());
        box2.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);

        line3 = game.add.sprite(295,510, 'game.line3');
        line3.scale.setTo(0.7, 0.7);
        line3.inputEnabled = true;
        line3.input.useHandCursor = true;
        line3.events.onInputUp.add(function(){
            line3.loadTexture('game.line3');
        });
        line3.events.onInputOver.add(function(){
            line3.loadTexture('game.line3_p');
        });
        line3.events.onInputOut.add(function(){
            line3.loadTexture('game.line3');
        });
        line3.events.onInputDown.add(function(){
            line3.loadTexture('game.line3_d');
            preOpenBox.play();
            //openBoxSound.play();
            lockDisplay();
            setTimeout('unlockDisplay()',2000);

            box2.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(300,320, 'game.number1_for_box');
                red_column_2.loadTexture('game.red_column_2_2');
            });
            line3.inputEnabled = false;
            line3.input.useHandCursor = false;
            line3.visble = false;
            line3x = game.add.sprite(295,510, 'game.line3_d');
            line3x.scale.setTo(0.7, 0.7);
            box_2.visible = false;
            box_2_number.visible = false;
        });

        line5 = game.add.sprite(340,510, 'game.line5');
        line5.scale.setTo(0.7, 0.7);
        line5.inputEnabled = true;
        line5.input.useHandCursor = true;
        line5.events.onInputOver.add(function(){
            line5.loadTexture('game.line5_p');
        });
        line5.events.onInputOut.add(function(){
            line5.loadTexture('game.line5');
        });
        line5.events.onInputDown.add(function(){
            preOpenBox.play();
            //openBoxSound.play();
            box = game.add.sprite(330,185, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(400,320, 'game.number1_for_box');
                red_column_1.loadTexture('game.red_column_1_3');
            });
            line5.inputEnabled = false;
            line5.input.useHandCursor = false;
            line5.visble = false;
            line5x = game.add.sprite(340,510, 'game.line5_d');
            line5x.scale.setTo(0.7, 0.7);
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
        });

        line7 = game.add.sprite(385,510, 'game.line7');
        line7.scale.setTo(0.7, 0.7);
        line7.inputEnabled = true;
        line7.input.useHandCursor = true;
        line7.events.onInputOver.add(function(){
            line7.loadTexture('game.line7_p');
        });
        line7.events.onInputOut.add(function(){
            line7.loadTexture('game.line7');
        });
        line7.events.onInputDown.add(function(){
            preOpenBox.play();
            //openBoxSound.play();
            box = game.add.sprite(430,185, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(500,320, 'game.number1_for_box');
                red_column_2.loadTexture('game.red_column_2_3');
            });
            line7.inputEnabled = false;
            line7.input.useHandCursor = false;
            line7.visble = false;
            line7x = game.add.sprite(385,510, 'game.line7_d');
            line7x.scale.setTo(0.7, 0.7);
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
        });

        line9 = game.add.sprite(430,510, 'game.line9');
        line9.scale.setTo(0.7, 0.7);
        line9.inputEnabled = true;
        line9.input.useHandCursor = true;
        line9.events.onInputOver.add(function(){
            line9.loadTexture('game.line9_p');
        });
        line9.events.onInputOut.add(function(){
            line9.loadTexture('game.line9');
        });
        line9.events.onInputDown.add(function(){
            //openBoxSound.play();
            preOpenBox.play();
            box = game.add.sprite(530,185, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(600,320, 'game.number1_for_box');
                red_column_3.loadTexture('game.red_column_3_2');
            });
            line9.inputEnabled = false;
            line9.input.useHandCursor = false;
            line9.visble = false;
            line9x = game.add.sprite(430,510, 'game.line9_d');
            line9x.scale.setTo(0.7, 0.7);
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
        });

        betone = game.add.sprite(490,510, 'game.betone');
        betone.scale.setTo(0.7, 0.7);
        betone.inputEnabled = true;
        betone.input.useHandCursor = true;
        betone.events.onInputOver.add(function(){
            betone.loadTexture('game.betone_p');
        });
        betone.events.onInputOut.add(function(){
            betone.loadTexture('game.betone');
        });

        betmax = game.add.sprite(535,510, 'game.betmax');
        betmax.scale.setTo(0.7, 0.7);
        betmax.inputEnabled = true;
        betmax.input.useHandCursor = true;
        betmax.events.onInputOver.add(function(){
            betmax.loadTexture('game.betmax_p');
        });
        betmax.events.onInputOut.add(function(){
            betmax.loadTexture('game.betmax');
        });

        automaricstart = game.add.sprite(685,510, 'game.automaricstart');
        automaricstart.scale.setTo(0.7, 0.7);
        automaricstart.inputEnabled = true;
        automaricstart.input.useHandCursor = true;
        automaricstart.events.onInputOver.add(function(){
            automaricstart.loadTexture('game.automaricstart_p');
        });
        automaricstart.events.onInputOut.add(function(){
            automaricstart.loadTexture('game.automaricstart');
        });

        //создание цифр для верхнего табло
/*        number0_table1 = game.add.sprite(285,32, 'game.number0_table1');
        number0_table1 = game.add.sprite(495,32, 'game.number0_table1');
        number0_table1 = game.add.sprite(690,32, 'game.number0_table1');*/

        //другое
        box_1 = game.add.sprite(130,185, 'game.box_1');
        box_2 = game.add.sprite(230,185, 'game.box_2');
        box_3 = game.add.sprite(330,185, 'game.box_3');
        box_4 = game.add.sprite(430,185, 'game.box_4');
        box_5 = game.add.sprite(530,185, 'game.box_5');

        box_1_number = game.add.sprite(130,185, 'game.box_1_number');
        box_2_number = game.add.sprite(230,185, 'game.box_2_number');
        box_3_number = game.add.sprite(330,185, 'game.box_3_number');
        box_4_number = game.add.sprite(430,185, 'game.box_4_number');
        box_5_number = game.add.sprite(530,185, 'game.box_5_number');
        box_1_number.animations.add('box_1_number', [0,1], 1, true);
        box_2_number.animations.add('box_2_number', [0,1], 1, true);
        box_3_number.animations.add('box_3_number', [0,1], 1, true);
        box_4_number.animations.add('box_4_number', [0,1], 1, true);
        box_5_number.animations.add('box_5_number', [0,1], 1, true);
        box_1_number.animations.getAnimation('box_1_number').play();
        box_2_number.animations.getAnimation('box_2_number').play();
        box_3_number.animations.getAnimation('box_3_number').play();
        box_4_number.animations.getAnimation('box_4_number').play();
        box_5_number.animations.getAnimation('box_5_number').play();

        //создание анимаций
        
        cat = game.add.sprite(95,406, 'game.cat');
        
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        
        cat.animations.getAnimation('cat').play();

        man1 = game.add.sprite(222,390, 'game.man1');
        man2 = game.add.sprite(222,390, 'game.man2');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);

        createLevelButtons();
        full_and_sound();
    };

    game4.update = function () {

    };

    game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        //this.scale.pageAlignHorisontally = true; // можно не выравнивать, но я остаил
        this.scale.pageAlignVertically = true;
        //this.scale.forcePortrait = true;

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;

        game.load.image('game.background', 'img/canvas-bg.png');
        game.load.image('game.canvasbg', 'img/canvas-bg.png');
        game.load.image('game.background2', 'img/image558.png');
        game.load.image('game.background3', 'img/shape777.png');
        game.load.image('game.background4', 'img/slot_bg.png');

        game.load.image('game.start', 'img/image1445.png');
        game.load.image('game.start_p', 'img/image1447.png');
        game.load.image('game.start_d', 'img/image1451.png');

        game.load.spritesheet('game.man1', 'img/man1.png', 192, 112);
        game.load.spritesheet('game.man2', 'img/man2.png', 192, 112);
        game.load.spritesheet('game.cat', 'img/cat.png', 128, 96);
        game.load.spritesheet('game.n1', 'img/n1.png',16,16);
        game.load.spritesheet('game.arrowRedBlue', 'img/arrowRedBlue.png',96,48);
        game.load.spritesheet('game.humanThought', 'img/humanThought.png',96,96);

        game.load.image('game.selectGame', 'img/image1419.png');
        game.load.image('game.selectGame_p', 'img/image1421.png');
        game.load.image('game.selectGame_d', 'img/image1425.png');
        game.load.image('game.payTable', 'img/image1428.png');
        game.load.image('game.payTable_p', 'img/image1430.png');
        game.load.image('game.payTable_d', 'img/image1433.png');
        game.load.image('game.automaricstart', 'img/image1436.png');
        game.load.image('game.automaricstart_p', 'img/image1438.png');
        game.load.image('game.automaricstart_d', 'img/image1442.png');
        game.load.image('game.betone', 'img/image1471.png');
        game.load.image('game.betone_p', 'img/image1473.png');
        game.load.image('game.betone_d', 'img/image1477.png');
        game.load.image('game.line1', 'img/image1505.png');
        game.load.image('game.line1_p', 'img/image1507.png');
        game.load.image('game.line1_d', 'img/image1511.png');
        game.load.image('game.betmax', 'img/image1480.png');
        game.load.image('game.betmax_p', 'img/image1482.png');
        game.load.image('game.betmax_d', 'img/image1485.png');
        game.load.image('game.line3', 'img/image1496.png');
        game.load.image('game.line3_p', 'img/image1498.png');
        game.load.image('game.line3_d', 'img/image1502.png');
        game.load.image('game.line5', 'img/image1488.png');
        game.load.image('game.line5_p', 'img/image1490.png');
        game.load.image('game.line5_d', 'img/image1493.png');
        game.load.image('game.line7', 'img/image1462.png');
        game.load.image('game.line7_p', 'img/image1464.png');
        game.load.image('game.line7_d', 'img/image1468.png');
        game.load.image('game.line9', 'img/image1454.png');
        game.load.image('game.line9_p', 'img/image1456.png');
        game.load.image('game.line9_d', 'img/image1459.png');

        game.load.image('game.number1', 'img/shape485.svg');
        game.load.image('game.number2', 'img/shape505.svg');
        game.load.image('game.number3', 'img/shape463.svg');
        game.load.image('game.number4', 'img/shape495.svg');
        game.load.image('game.number5', 'img/shape475.svg');
        game.load.image('game.number6', 'img/shape500.svg');
        game.load.image('game.number7', 'img/shape468.svg');
        game.load.image('game.number8', 'img/shape480.svg');
        game.load.image('game.number9', 'img/shape490.svg');

        game.load.image('game.nt0', 'img/shape199.svg');
        game.load.image('game.nt1', 'img/shape201.svg');
        game.load.image('game.nt2', 'img/shape203.svg');
        game.load.image('game.nt3', 'img/shape205.svg');
        game.load.image('game.nt4', 'img/shape207.svg');
        game.load.image('game.nt5', 'img/shape209.svg');
        game.load.image('game.nt6', 'img/shape211.svg');
        game.load.image('game.nt7', 'img/shape213.svg');
        game.load.image('game.nt8', 'img/shape215.svg');
        game.load.image('game.nt9', 'img/shape217.svg');

        game.load.image('game.number0_table1', 'img/image175.png');
        game.load.image('game.number1_table1', 'img/image177.png');
        game.load.image('game.number2_table1', 'img/image179.png');
        game.load.image('game.number3_table1', 'img/image181.png');
        game.load.image('game.number4_table1', 'img/image183.png');
        game.load.image('game.number5_table1', 'img/image185.png');
        game.load.image('game.number6_table1', 'img/image187.png');
        game.load.image('game.number7_table1', 'img/image189.png');
        game.load.image('game.number8_table1', 'img/image191.png');
        game.load.image('game.number9_table1', 'img/image193.png');

        game.load.image('game.win1', 'img/shape459.svg');
        game.load.image('game.win2', 'img/shape443.svg');
        game.load.image('game.win3', 'img/shape445.svg');
        game.load.image('game.win4', 'img/shape447.svg');
        game.load.image('game.win5', 'img/shape449.svg');
        game.load.image('game.win6', 'img/shape451.svg');
        game.load.image('game.win7', 'img/shape453.svg');
        game.load.image('game.win8', 'img/shape455.svg');
        game.load.image('game.win9', 'img/shape457.svg');

        game.load.image('game.win1_dotted', 'img/shape440.svg');
        game.load.image('game.win2_dotted', 'img/shape424.svg');
        game.load.image('game.win3_dotted', 'img/shape426.svg');
        game.load.image('game.win4_dotted', 'img/shape428.svg');
        game.load.image('game.win5_dotted', 'img/shape430.svg');
        game.load.image('game.win6_dotted', 'img/shape432.svg');
        game.load.image('game.win7_dotted', 'img/shape434.svg');
        game.load.image('game.win8_dotted', 'img/shape436.svg');
        game.load.image('game.win9_dotted', 'img/shape438.svg');


        game.load.image('game.bar', 'img/bars.png');
        game.load.image('game.bar_move', 'img/bar_move.png');

        game.load.image('game.playto', 'img/shape1333.svg');
        game.load.image('game.credits', 'img/shape1335.svg');

        //lock
        game.load.image('game.lock', 'img/shape604.png');
        game.load.image('game.box_for_keys', 'img/shape622.png');
        game.load.image('game.arrows', 'img/shape606.svg');
        game.load.image('game.find_a_kay', 'img/shape611.svg');

        game.load.image('game.cards_bg', 'img/shape222.png');
        //карты
        game.load.image('game.card_garage', 'img/shape225.png');

        game.load.image('game.card_2b', 'img/shape227.png');
        game.load.image('game.card_3b', 'img/shape229.png');
        game.load.image('game.card_4b', 'img/shape231.png');
        game.load.image('game.card_5b', 'img/shape233.png');
        game.load.image('game.card_6b', 'img/shape235.png');
        game.load.image('game.card_7b', 'img/shape237.png');
        game.load.image('game.card_8b', 'img/shape239.png');
        game.load.image('game.card_9b', 'img/shape241.png');
        game.load.image('game.card_10b', 'img/shape243.png');
        game.load.image('game.card_jb', 'img/shape245.png');
        game.load.image('game.card_qb', 'img/shape247.png');
        game.load.image('game.card_kb', 'img/shape249.png');
        game.load.image('game.card_ab', 'img/shape251.png');

        game.load.image('game.card_2ch', 'img/shape253.png');
        game.load.image('game.card_3ch', 'img/shape255.png');
        game.load.image('game.card_4ch', 'img/shape257.png');
        game.load.image('game.card_5ch', 'img/shape259.png');
        game.load.image('game.card_6ch', 'img/shape261.png');
        game.load.image('game.card_7ch', 'img/shape263.png');
        game.load.image('game.card_8ch', 'img/shape265.png');
        game.load.image('game.card_9ch', 'img/shape267.png');
        game.load.image('game.card_10ch', 'img/shape269.png');
        game.load.image('game.card_jch', 'img/shape271.png');
        game.load.image('game.card_qch', 'img/shape273.png');
        game.load.image('game.card_kch', 'img/shape275.png');
        game.load.image('game.card_ach', 'img/shape277.png');

        game.load.image('game.card_2k', 'img/shape279.png');
        game.load.image('game.card_3k', 'img/shape281.png');
        game.load.image('game.card_4k', 'img/shape283.png');
        game.load.image('game.card_5k', 'img/shape285.png');
        game.load.image('game.card_6k', 'img/shape287.png');
        game.load.image('game.card_7k', 'img/shape289.png');
        game.load.image('game.card_8k', 'img/shape291.png');
        game.load.image('game.card_9k', 'img/shape293.png');
        game.load.image('game.card_10k', 'img/shape295.png');
        game.load.image('game.card_jk', 'img/shape297.png');
        game.load.image('game.card_qk', 'img/shape299.png');
        game.load.image('game.card_kk', 'img/shape301.png');
        game.load.image('game.card_ak', 'img/shape303.png');

        game.load.image('game.card_2p', 'img/shape305.png');
        game.load.image('game.card_3p', 'img/shape307.png');
        game.load.image('game.card_4p', 'img/shape309.png');
        game.load.image('game.card_5p', 'img/shape311.png');
        game.load.image('game.card_6p', 'img/shape313.png');
        game.load.image('game.card_7p', 'img/shape315.png');
        game.load.image('game.card_8p', 'img/shape317.png');
        game.load.image('game.card_9p', 'img/shape319.png');
        game.load.image('game.card_10p', 'img/shape321.png');
        game.load.image('game.card_jp', 'img/shape323.png');
        game.load.image('game.card_qp', 'img/shape325.png');
        game.load.image('game.card_kp', 'img/shape327.png');
        game.load.image('game.card_ap', 'img/shape329.png');

        game.load.image('game.card_joker', 'img/shape331.png');
        game.load.image('game.card_red', 'img/shape223.png');
        game.load.image('game.pick', 'img/shape340.png');

        //boxes
        game.load.image('game.box_1', 'img/shape943.svg');
        game.load.image('game.box_2', 'img/shape947.svg');
        game.load.image('game.box_3', 'img/shape951.svg');
        game.load.image('game.box_4', 'img/shape955.svg');
        game.load.image('game.box_5', 'img/shape959.svg');

        game.load.image('game.red_column_1_0', 'img/shape779.png');
        game.load.image('game.red_column_1_1', 'img/shape781.png');
        game.load.image('game.red_column_1_2', 'img/shape783.png');
        game.load.image('game.red_column_1_3', 'img/shape785.png');
        game.load.image('game.red_column_1_4', 'img/shape787.png');
        game.load.image('game.red_column_1_5', 'img/shape789.png');

        game.load.image('game.red_column_2_0', 'img/shape793.png');
        game.load.image('game.red_column_2_1', 'img/shape795.png');
        game.load.image('game.red_column_2_2', 'img/shape797.png');
        game.load.image('game.red_column_2_3', 'img/shape799.png');
        game.load.image('game.red_column_2_4', 'img/shape801.png');
        game.load.image('game.red_column_2_5', 'img/shape803.png');

        game.load.image('game.red_column_3_0', 'img/shape807.png');
        game.load.image('game.red_column_3_1', 'img/shape809.png');
        game.load.image('game.red_column_3_2', 'img/shape811.png');
        game.load.image('game.red_column_3_3', 'img/shape813.png');
        game.load.image('game.red_column_3_4', 'img/shape815.png');
        game.load.image('game.red_column_3_5', 'img/shape817.png');

        game.load.image('game.superkey', 'img/shape760.svg');

        game.load.image('game.flashingNumber1', 'img/shape485.svg');
        game.load.image('game.flashingNumber2', 'img/shape505.svg');
        game.load.image('game.flashingNumber3', 'img/shape463.svg');
        game.load.image('game.flashingNumber4', 'img/shape495.svg');
        game.load.image('game.flashingNumber5', 'img/shape465.svg');
        game.load.image('game.flashingNumber6', 'img/shape500.svg');
        game.load.image('game.flashingNumber7', 'img/shape468.svg');
        game.load.image('game.flashingNumber8', 'img/shape480.svg');
        game.load.image('game.flashingNumber9', 'img/shape490.svg');

        game.load.spritesheet('game.box_1_number', 'img/box_1_number.png', 177, 225);
        game.load.spritesheet('game.box_2_number', 'img/box_2_number.png', 177, 225);
        game.load.spritesheet('game.box_3_number', 'img/box_3_number.png', 177, 225);
        game.load.spritesheet('game.box_4_number', 'img/box_4_number.png', 177, 225);
        game.load.spritesheet('game.box_5_number', 'img/box_5_number.png', 177, 225);

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');

        game.load.image('game.yellow_car','img/non_full.png');
        game.load.image('game.red_car','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');

        game.load.audio('game4.loss', 'bonus2/b2_loss.wav');
        game.load.audio('game4.win', 'bonus2/b2_win.wav');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        function preloadLevelButtons() {
            game.load.image('btnline11', 'lines/line11.png');
            game.load.image('btnline_p11', 'lines/line11_p.png');
            game.load.image('btnline13', 'lines/line13.png');
            game.load.image('btnline_p13', 'lines/line13_p.png');
            game.load.image('btnline15', 'lines/line15.png');
            game.load.image('btnline_p15', 'lines/line15_p.png');
        }

        game.load.image('bars', 'bars.png');
        game.load.image('start', 'start.png');
        game.load.image('start_p', 'start_p.png');
        game.load.image('start_d', 'start_d.png');
        game.load.image('bet', 'bet.png');
        game.load.image('canvasbg', 'canvas-bg.png');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');

        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        preloadLevelButtons();

        game.load.image('background2', 'cards/back.png');
        game.load.image('shlem', 'cards/shlem.png');
        game.load.image('dealer', 'cards/dealer.png');
        game.load.image('pick', 'cards/pick.png');
        game.load.image('card_back', 'cards/cards/back.png');
        game.load.spritesheet('monkey2', 'cards/monkey.png', 182, 209, 15);
        game.load.spritesheet('message', 'cards/message.png', 179, 65, 5);
        var cardValues = {
            1: 11,
            2: 7,
            3: 3,
            4: 14,
            5: 13
        };
        for(var i in cardValues) {
            game.load.image('card_'+i, 'cards/cards/'+i+'.png');
        }

        game.load.audio('opencard', 'cards/opencard.wav');
        game.load.audio('openuser', 'cards/openuser.wav');

        game.load.image('background3', 'bonus/back.png');
        game.load.spritesheet('rope', 'bonus/rope.png', 21, 335, 17);
        game.load.audio('b1_keypress', 'bonus/b1_keypress.wav');
        game.load.audio('b1_prizedown', 'bonus/b1_prizedwn.wav');
        game.load.audio('b1_shit', 'bonus/b1_shit.wav');

        for(var i=0; i<=3; ++i) {
            game.load.image('prize_' + i, 'bonus/prizes/' + i + '.png');
        }

        game.load.audio('game.openCardAudio', 'sound/sound31.mp3');
        game.load.audio('game.arrowSound', 'sound/sound7.mp3');
        game.load.audio('game.openBoxSound', 'sound/sound17.mp3');
        game.load.audio('game.openKeySound', 'sound/sound5.mp3');
        game.load.audio('game.openKeyBoxLoseSound', 'sound/sound8.mp3');
        game.load.audio('game.openKeyBoxPreLoseSound', 'sound/sound4.mp3');
        game.load.audio('game.openKeyBoxPreWinSound', 'sound/sound5.mp3');
        game.load.audio('game.openKeyBoxWinSound', 'sound/sound4.mp3');
        game.load.audio('game.openPoliceBox', 'sound/sound10.mp3');
        game.load.audio('game.preOpenBox', 'sound/sound1.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');
        game.load.audio('game.betOneSound', 'sound/sound20.mp3');
        game.load.audio('game.betMaxSound', 'sound/sound33.mp3');
		game.load.audio('takeWin', 'takeWin.mp3');
		game.load.audio('takeBox', 'sound/sound40.mp3');


        game.load.spritesheet('game.openLockFromSlot', 'img/openLockFromSlot.png', 96, 113);
        game.load.spritesheet('game.openBoxFromSlot', 'img/openBoxFromSlot.png', 96, 113);
        game.load.spritesheet('game.openLock2', 'img/openLock2.png', 48, 48);

        game.load.spritesheet('game.openLoseLeftCase', 'img/openLoseLeftCase.png', 192, 80);
        game.load.spritesheet('game.openLoseRightCase', 'img/openLoseRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinRightCase', 'img/openWinRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinLeftCase', 'img/openWinLeftCase.png', 192, 80);
        game.load.spritesheet('game.findAKey', 'img/findAKey.png', 96, 16);

        game.load.spritesheet('game.clockBox', 'img/clockBox.png', 177, 225);
        game.load.spritesheet('game.detailBox', 'img/detailBox.png', 177, 225);
        game.load.spritesheet('game.flashBox', 'img/flashBox.png', 177, 225);
        game.load.spritesheet('game.franchBox', 'img/franchBox.png', 177, 225);
        game.load.spritesheet('game.hammer2Box', 'img/hammer2Box.png', 177, 225);
        game.load.spritesheet('game.HummerBox', 'img/HummerBox.png', 177, 225);
        game.load.spritesheet('game.jackBox', 'img/jackBox.png', 177, 225);
        game.load.spritesheet('game.policeBox', 'img/policeBox.png', 177, 225);
        game.load.spritesheet('game.sawBox', 'img/sawBox.png', 177, 225);

        game.load.image('game.number1_for_box', 'img/image177.png');
        game.load.image('game.number2_for_box', 'img/image179.png');
        game.load.image('game.number3_for_box', 'img/image181.png');
        game.load.image('game.number4_for_box', 'img/image183.png');
        game.load.image('game.number5_for_box', 'img/image185.png');
        game.load.image('game.number6_for_box', 'img/image187.png');
        game.load.image('game.number7_for_box', 'img/image189.png');
        game.load.image('game.number8_for_box', 'img/image191.png');
        game.load.image('game.number9_for_box', 'img/image193.png');
        game.load.image('game.number0_for_box', 'img/image175.png');
        game.load.image('game.number0_for_box', 'img/image175.png');

        game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 641, 33);
        game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 641, 33);
        game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 641, 33);
        game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 641, 33);
        game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 641, 33);
        game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 641, 33);
        game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 641, 33);
        game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 641, 33);
        game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 641, 33);

        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.spritesheet('game.move_super_key1', 'img/move_super_key1.png', 119, 160);
        game.load.spritesheet('game.move_super_key2', 'img/move_super_key2.png', 119, 160);
        game.load.image('game.small_red_key', 'img/small_red_key.png');

        game.load.spritesheet('game.super_key_in_tablo', 'img/super_key_in_tablo.png', 160, 48);
        game.load.spritesheet('game.take', 'img/take.png', 64, 32);
        game.load.spritesheet('game.take_or_risk', 'img/take_or_risk.png', 128, 32);
        game.load.spritesheet('game.play_1_to', 'img/play_1_to.png', 128, 48);
        game.load.image('game.lock_bonus_game', 'img/shape1292.svg');
        game.load.image('game.box_bonus_game', 'img/shape1295.svg');



    };

    preload.create = function() {
        game.state.start('game1');
        //document.body.removeChild(document.getElementById('loading'));
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');