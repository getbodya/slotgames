(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        game.load.image('game.background', needUrlPath + '/img/shape1206.svg');
        game.load.image('game.black_bg', needUrlPath + '/img/black_bg.png');
        game.load.image('game.background1', needUrlPath + '/img/main_bg.png');

        game.load.image('startButton', needUrlPath + '/img/image1445.png');
        game.load.image('startButton_p', needUrlPath + '/img/image1447.png');
        game.load.image('startButton_d', needUrlPath + '/img/image1451.png');

        game.load.image('selectGame', needUrlPath + '/img/image1419.png');
        game.load.image('selectGame_p', needUrlPath + '/img/image1421.png');
        game.load.image('selectGame_d', needUrlPath + '/img/image1425.png');
        game.load.image('payTable', needUrlPath + '/img/image1428.png');
        game.load.image('payTable_p', needUrlPath + '/img/image1430.png');
        game.load.image('payTable_d', needUrlPath + '/img/image1433.png');
        game.load.image('automaricstart', needUrlPath + '/img/image1436.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/image1438.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/image1442.png');
        game.load.image('betone', needUrlPath + '/img/image1471.png');
        game.load.image('betone_p', needUrlPath + '/img/image1473.png');
        game.load.image('betone_d', needUrlPath + '/img/image1477.png');
        game.load.image('betmax', needUrlPath + '/img/image1480.png');
        game.load.image('betmax_p', needUrlPath + '/img/image1482.png');
        game.load.image('betmax_d', needUrlPath + '/img/image1485.png');
        game.load.image('buttonLine1', needUrlPath + '/img/image1505.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/image1507.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/image1511.png');
        game.load.image('buttonLine3', needUrlPath + '/img/image1496.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/image1498.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/image1502.png');
        game.load.image('buttonLine5', needUrlPath + '/img/image1488.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/image1490.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/image1493.png');
        game.load.image('buttonLine7', needUrlPath + '/img/image1462.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/image1464.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/image1468.png');
        game.load.image('buttonLine9', needUrlPath + '/img/image1454.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/image1456.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/image1459.png');

        game.load.image('game.number1', needUrlPath + '/img/win_1.png');
        game.load.image('game.number2', needUrlPath + '/img/win_2.png');
        game.load.image('game.number3', needUrlPath + '/img/win_3.png');
        game.load.image('game.number4', needUrlPath + '/img/win_4.png');
        game.load.image('game.number5', needUrlPath + '/img/win_5.png');
        game.load.image('game.number6', needUrlPath + '/img/win_6.png');
        game.load.image('game.number7', needUrlPath + '/img/win_7.png');
        game.load.image('game.number8', needUrlPath + '/img/win_8.png');
        game.load.image('game.number9', needUrlPath + '/img/win_9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.audio('sound', needUrlPath + '/sounds/spin.mp3');
        game.load.audio('rotate', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('stop', needUrlPath + '/sounds/stop.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('play', needUrlPath + '/sounds/play.mp3');

        game.load.audio('hithit_game2', needUrlPath + '/sounds/hithit_game2.mp3');
        game.load.audio('final_win_game2', needUrlPath + '/sounds/final_win_game2.mp3');
        game.load.audio('win_first_game2', needUrlPath + '/sounds/win_first_game2.mp3');
        game.load.audio('lose_game2', needUrlPath + '/sounds/lose_game2.mp3');
        

        game.load.image('line1', needUrlPath + '/img/lines/select/line1.png');
        game.load.image('line2', needUrlPath + '/img/lines/select/line2.png');
        game.load.image('line3', needUrlPath + '/img/lines/select/line3.png');
        game.load.image('line4', needUrlPath + '/img/lines/select/line4.png');
        game.load.image('line5', needUrlPath + '/img/lines/select/line5.png');
        game.load.image('line6', needUrlPath + '/img/lines/select/line6.png');
        game.load.image('line7', needUrlPath + '/img/lines/select/line7.png');
        game.load.image('line8', needUrlPath + '/img/lines/select/line8.png');
        game.load.image('line9', needUrlPath + '/img/lines/select/line9.png');

        game.load.image('linefull1', needUrlPath + '/img/lines/win/linefull1.png');
        game.load.image('linefull2', needUrlPath + '/img/lines/win/linefull2.png');
        game.load.image('linefull3', needUrlPath + '/img/lines/win/linefull3.png');
        game.load.image('linefull4', needUrlPath + '/img/lines/win/linefull4.png');
        game.load.image('linefull5', needUrlPath + '/img/lines/win/linefull5.png');
        game.load.image('linefull6', needUrlPath + '/img/lines/win/linefull6.png');
        game.load.image('linefull7', needUrlPath + '/img/lines/win/linefull7.png');
        game.load.image('linefull8', needUrlPath + '/img/lines/win/linefull8.png');
        game.load.image('linefull9', needUrlPath + '/img/lines/win/linefull9.png');

        game.load.audio('line1Sound', needUrlPath + '/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sounds/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sounds/stop.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('play', needUrlPath + '/sounds/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sounds/takeWin.mp3');        
        game.load.audio('page', needUrlPath + '/sounds/page.mp3');
        game.load.audio('hit_game3', needUrlPath + '/sounds/hit_game3.mp3');
        game.load.audio('lose_game3', needUrlPath + '/sounds/lose_game3.mp3');
        game.load.audio('number_win', needUrlPath + '/sounds/number_win.mp3');
        game.load.audio('open_game4', needUrlPath + '/sounds/open_game4.mp3');
        game.load.audio('win_game4', needUrlPath + '/sounds/win_game4.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sounds/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sounds/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sounds/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sounds/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sounds/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sounds/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sounds/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sounds/winLines/sound19.mp3');

        game.load.image('prev_page', needUrlPath + '/img/prev_page.png');
        game.load.image('exit_btn', needUrlPath + '/img/exit_btn.png');
        game.load.image('next_page', needUrlPath + '/img/next_page.png');
        for (var i = 1; i <= 5; ++i) {
            game.load.image('pagePaytable_' + i, needUrlPath + '/img/page_' + i + '.png');            
        }

        game.load.audio('bonusWin', needUrlPath + '/sounds/bonusWin.mp3');


        game.load.image('game.backgroundGame2', needUrlPath + '/img/bg_game2.png');

        game.load.spritesheet('bee', needUrlPath + '/img/bee_144x112_8.png', 144, 112, 8);
        game.load.spritesheet('bear', needUrlPath + '/img/bear_176x112x7.png', 176, 112, 7);
        game.load.spritesheet('game.win1', needUrlPath + '/img/bear_win_176x112x19.png', 176, 112, 19);
        game.load.spritesheet('ears', needUrlPath + '/img/ears_32х32_3.png', 32, 32, 3);
        
        game.load.spritesheet('bear_hit_left', needUrlPath + '/img/bear_hit_left_272x256_7.png', 272, 256, 7);
        game.load.spritesheet('bear_hit_right', needUrlPath + '/img/bear_hit_right_272x256_7.png', 272, 256, 7);
        game.load.spritesheet('bear_lose_down', needUrlPath + '/img/bear_lose_down_272x256_4_3.png', 272, 256, 7);
        game.load.spritesheet('bear_lose_left_1', needUrlPath + '/img/bear_lose_left_272x256_14.png', 272, 256, 14);
        game.load.spritesheet('bear_lose_left_2', needUrlPath + '/img/bear_lose_left_272x256_3.png', 272, 256, 3);
        game.load.spritesheet('bear_lose_right_1', needUrlPath + '/img/bear_lose_right_272x256_14.png', 272, 256, 14);
        game.load.spritesheet('bear_lose_right_2', needUrlPath + '/img/bear_lose_right_272x256_2.png', 272, 256, 2);
        game.load.spritesheet('game2.bear_wait', needUrlPath + '/img/bear_wait_208x256_6.png', 208, 256, 6);
        game.load.spritesheet('bear_win_left_1', needUrlPath + '/img/bear_win_left_1_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_left_2', needUrlPath + '/img/bear_win_left_2_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_right_1', needUrlPath + '/img/bear_win_right_1_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_right_2', needUrlPath + '/img/bear_win_right_2_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_top', needUrlPath + '/img/bear_win_top_272x256_7.png', 272, 256, 7);
        game.load.spritesheet('bee_lose_1', needUrlPath + '/img/bee_lose_1_80x64_5.png', 80, 64, 5);
        game.load.spritesheet('bee_lose_2', needUrlPath + '/img/bee_lose_2_80x64_7.png', 80, 64, 7);
        game.load.spritesheet('beehive_wait', needUrlPath + '/img/beehive_wait_80x64_8.png', 80, 64, 8);
        game.load.spritesheet('leaves_1', needUrlPath + '/img/leaves_1_272x128_6.png', 272, 128, 6);
        game.load.spritesheet('leaves_2', needUrlPath + '/img/leaves_2_272x128_13.png', 272, 128, 13);
        game.load.image('cloud_1', needUrlPath + '/img/cloud_1.png');
        game.load.image('cloud_2', needUrlPath + '/img/cloud_2.png');
        game.load.image('game.double', needUrlPath + '/img/game.double.png');
        game.load.image('tree_1', needUrlPath + '/img/tree_1.png');
        game.load.image('tree_2', needUrlPath + '/img/tree_3.png');

        game.load.spritesheet('game4.bear_wait', needUrlPath + '/img/game4.bear_wait_96x176_24.png', 96, 176, 24);
        game.load.spritesheet('game4.hive_wait', needUrlPath + '/img/hive_wait_128x256_8.png', 128, 256, 8);
        game.load.spritesheet('game4.hive_win', needUrlPath + '/img/hive_win_112x176_8.png', 112, 176, 8);
        game.load.spritesheet('game4.hive_lose', needUrlPath + '/img/hive_lose_112x176_11.png', 112, 176, 11);
        game.load.spritesheet('game4.bear_hit_1', needUrlPath + '/img/bear_hit_256x144_10.png', 256, 144, 10);
        game.load.spritesheet('game4.bear_hit_2', needUrlPath + '/img/bear_hit_288x144_6.png', 288, 144, 6);
        game.load.spritesheet('game4.bear_lose', needUrlPath + '/img/bear_lose_256x144_11.png', 256, 144, 11);
        game.load.spritesheet('game4.bear_win', needUrlPath + '/img/bear_win_256x144_7.png', 256, 144, 7);
        game.load.image('game4.bee_foot', needUrlPath + '/img/bee_foot.png');
        game.load.image('game.backgroundGame3', needUrlPath + '/img/bg_game4.png');
        game.load.image('game.backgroundGame4', needUrlPath + '/img/game3_bg.png');
        game.load.spritesheet('game3.chicken', needUrlPath + '/img/chicken_32x32_32.png', 32, 32, 32);
        game.load.spritesheet('game3.smoke', needUrlPath + '/img/smoke_32x64_8.png', 32, 64, 8);
        game.load.spritesheet('game3.bear_left_empty_1', needUrlPath + '/img/game3_bear_left_empty_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_left_empty_2', needUrlPath + '/img/game3_bear_left_empty_2_320x224_12.png', 320, 224, 12);
        game.load.spritesheet('game3.bear_left_empty_3', needUrlPath + '/img/game3_bear_left_empty_3_320x224_7.png', 320, 224, 7);
        game.load.spritesheet('game3.bear_left_honey_1', needUrlPath + '/img/game3_bear_left_honey_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_left_honey_2', needUrlPath + '/img/game3_bear_left_honey_2_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_left_honey_3', needUrlPath + '/img/game3_bear_left_honey_3_320x208_2.png', 320, 208, 2);
        game.load.spritesheet('game3.bear_right_empty_1', needUrlPath + '/img/game3_bear_right_empty_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_right_empty_2', needUrlPath + '/img/game3_bear_right_empty_2_320x224_12.png', 320, 224, 12);
        game.load.spritesheet('game3.bear_right_empty_3', needUrlPath + '/img/game3_bear_right_empty_3_320x224_7.png', 320, 224, 7);
        game.load.spritesheet('game3.bear_right_honey_1', needUrlPath + '/img/game3_bear_right_honey_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_right_honey_2', needUrlPath + '/img/game3_bear_right_honey_2_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_right_honey_3', needUrlPath + '/img/game3_bear_right_honey_3_320x208_2.png', 320, 208, 2);
        game.load.spritesheet('game3.bear_wait_1', needUrlPath + '/img/game3_bear_wait_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_wait_2', needUrlPath + '/img/game3_bear_wait_2_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_wait_3', needUrlPath + '/img/game3_bear_wait_3_320x208_3.png', 320, 208, 3);


        game.load.image('cell0', needUrlPath + '/img/0.png');
        game.load.image('cell1', needUrlPath + '/img/1.png');
        game.load.image('cell2', needUrlPath + '/img/2.png');
        game.load.image('cell3', needUrlPath + '/img/3.png');
        game.load.image('cell4', needUrlPath + '/img/4.png');
        game.load.image('cell5', needUrlPath + '/img/5.png');
        game.load.image('cell6', needUrlPath + '/img/6.png');
        game.load.image('cell7', needUrlPath + '/img/7.png');
        game.load.image('cell8', needUrlPath + '/img/8.png');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.png', 96, 112);

        game.load.image('bonusGame', needUrlPath + '/img/image536.jpg');
        game.load.image('wildSymbol', needUrlPath + '/img/image537.jpg');
        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('takeOrRisk1', needUrlPath + '/img/image474.png');
        game.load.image('takeOrRisk2', needUrlPath + '/img/image475.png');
        game.load.image('take', needUrlPath + '/img/image554.png');

        game.load.image('topScoreGame1', needUrlPath + '/img/main_bg_1.png');
        game.load.image('topScoreGame2', needUrlPath + '/img/main_bg_2.png');
        game.load.image('topScoreGame3', needUrlPath + '/img/main_bg_3.png');
        game.load.image('topScoreGame4', needUrlPath + '/img/main_bg_4.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/bonus.png', 96, 112);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

