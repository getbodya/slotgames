window.game = new Phaser.Game(640, 480, Phaser.AUTO, 'phaser-example');

isMobile = true;

var mobileX = 94;
var mobileY = 22;

var checkHelm = false; //проверка надела ли каска

//переменные получаемые из api
var gamename = 'sweetlife1rus'; //название игры
var result;
var state;
var sid;
var user;
var min;
var id;
var balance = 10000000;
var extralife = 45;
var jackpots;
var betline = 1;
var lines = 9;
var bet = 9;
var info;
var wl;
var dcard;
var dwin;
var dcard2;
var select;

//звуки и полноэкранный режим
var fullStatus = false;
var soundStatus = true;

//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

//Функции связанные с линиями и их номерами

var line1; var linefull1; var number1;
var line2; var linefull2; var number2;
var line3; var linefull3; var number3;
var line4; var linefull4; var number4;
var line5; var linefull5; var number5;
var line6; var linefull6; var number6;
var line7; var linefull7; var number7;
var line8; var linefull8; var number8;
var line9; var linefull9; var number9;

//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9'];
    var lineNames = ['line1', 'line2', 'line3', 'line4', 'line5', 'line6', 'line7', 'line8', 'line9'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);

    linefull1.visible = false;
    linefull2.visible = false;
    linefull3.visible = false;
    linefull4.visible = false;
    linefull5.visible = false;
    linefull6.visible = false;
    linefull7.visible = false;
    linefull8.visible = false;
    linefull9.visible = false;

    line1 = game.add.sprite(linePosition[0][0]+0, linePosition[0][1], lineNames[0]);
    line2 = game.add.sprite(linePosition[1][0]+0, linePosition[1][1], lineNames[1]);
    line3 = game.add.sprite(linePosition[2][0]+0, linePosition[2][1], lineNames[2]);
    line4 = game.add.sprite(linePosition[3][0]+0, linePosition[3][1], lineNames[3]);
    line5 = game.add.sprite(linePosition[4][0]+0, linePosition[4][1]+13, lineNames[4]);
    line6 = game.add.sprite(linePosition[5][0]+0, linePosition[5][1], lineNames[5]);
    line7 = game.add.sprite(linePosition[6][0]+0, linePosition[6][1]+56, lineNames[6]);
    line8 = game.add.sprite(linePosition[7][0]+0, linePosition[7][1], lineNames[7]);
    line9 = game.add.sprite(linePosition[8][0]+0, linePosition[8][1], lineNames[8]);

    line1.visible = false;
    line2.visible = false;
    line3.visible = false;
    line4.visible = false;
    line5.visible = false;
    line6.visible = false;
    line7.visible = false;
    line8.visible = false;
    line9.visible = false;

    number1 = game.add.sprite(numberPosition[0][0], numberPosition[0][1], 'game.number1');
    number2 = game.add.sprite(numberPosition[1][0], numberPosition[1][1], 'game.number2');
    number3 = game.add.sprite(numberPosition[2][0], numberPosition[2][1], 'game.number3');
    number4 = game.add.sprite(numberPosition[3][0], numberPosition[3][1], 'game.number4');
    number5 = game.add.sprite(numberPosition[4][0], numberPosition[4][1], 'game.number5');
    number6 = game.add.sprite(numberPosition[5][0], numberPosition[5][1], 'game.number6');
    number7 = game.add.sprite(numberPosition[6][0], numberPosition[6][1], 'game.number7');
    number8 = game.add.sprite(numberPosition[7][0], numberPosition[7][1], 'game.number8');
    number9 = game.add.sprite(numberPosition[8][0], numberPosition[8][1], 'game.number9');

    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;

}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
var linesArray = [];
function hideLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        line1.visible = false;
        line2.visible = false;
        line3.visible = false;
        line4.visible = false;
        line5.visible = false;
        line6.visible = false;
        line7.visible = false;
        line8.visible = false;
        line9.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = false;
                break;
                case 2:
                linefull2.visible = false;
                break;
                case 3:
                linefull3.visible = false;
                break;
                case 4:
                linefull4.visible = false;
                break;
                case 5:
                linefull5.visible = false;
                break;
                case 6:
                linefull6.visible = false;
                break;
                case 7:
                linefull7.visible = false;
                break;
                case 8:
                linefull8.visible = false;
                break;
                case 9:
                linefull9.visible = false;
                break;
                case 11:
                line1.visible = false;
                break;
                case 12:
                line2.visible = false;
                break;
                case 13:
                line3.visible = false;
                break;
                case 14:
                line4.visible = false;
                break;
                case 15:
                line5.visible = false;
                break;
                case 16:
                line6.visible = false;
                break;
                case 17:
                line7.visible = false;
                break;
                case 18:
                line8.visible = false;
                break;
                case 19:
                line9.visible = false;
                break;
            }
        });
    }
}

function showLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        line1.visible = true;
        line2.visible = true;
        line3.visible = true;
        line4.visible = true;
        line5.visible = true;
        line6.visible = true;
        line7.visible = true;
        line8.visible = true;
        line9.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = true;
                break;
                case 2:
                linefull2.visible = true;
                break;
                case 3:
                linefull3.visible = true;
                break;
                case 4:
                linefull4.visible = true;
                break;
                case 5:
                linefull5.visible = true;
                break;
                case 6:
                linefull6.visible = true;
                break;
                case 7:
                linefull7.visible = true;
                break;
                case 8:
                linefull8.visible = true;
                break;
                case 9:
                linefull9.visible = true;
                break;
                case 11:
                line1.visible = true;
                break;
                case 12:
                line2.visible = true;
                break;
                case 13:
                line3.visible = true;
                break;
                case 14:
                line4.visible = true;
                break;
                case 15:
                line5.visible = true;
                break;
                case 16:
                line6.visible = true;
                break;
                case 17:
                line7.visible = true;
                break;
                case 18:
                line8.visible = true;
                break;
                case 19:
                line9.visible = true;
                break;
            }
        });
    }
}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = false;
        number2.visible = false;
        number3.visible = false;
        number4.visible = false;
        number5.visible = false;
        number6.visible = false;
        number7.visible = false;
        number8.visible = false;
        number9.visible = false;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = false;
                break;
                case 2:
                number2.visible = false;
                break;
                case 3:
                number3.visible = false;
                break;
                case 4:
                number4.visible = false;
                break;
                case 5:
                number5.visible = false;
                break;
                case 6:
                number6.visible = false;
                break;
                case 7:
                number7.visible = false;
                break;
                case 8:
                number8.visible = false;
                break;
                case 9:
                number9.visible = false;
                break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = true;
                break;
                case 2:
                number2.visible = true;
                break;
                case 3:
                number3.visible = true;
                break;
                case 4:
                number4.visible = true;
                break;
                case 5:
                number5.visible = true;
                break;
                case 6:
                number6.visible = true;
                break;
                case 7:
                number7.visible = true;
                break;
                case 8:
                number8.visible = true;
                break;
                case 9:
                number9.visible = true;
                break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}



// Функции связанные с кнопками
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;

//кнопки для слотов
function addButtonsGame1(game) {

    //звуки для кнопок
    var line1Sound = game.add.audio('line1Sound');
    var line3Sound = game.add.audio('line3Sound');
    var line5Sound = game.add.audio('line5Sound');
    var line7Sound = game.add.audio('line7Sound');
    var line9Sound = game.add.audio('line9Sound');

    //var soundForBattons = []; - массив содержащий объекты звуков для кнопок
    var soundForBattons = [];
    soundForBattons.push(line1Sound, line3Sound, line5Sound, line7Sound, line9Sound);

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150,510, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490,510, 'betone');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        betone.loadTexture('betone_p');
    });
    betone.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            upBetline(betlineOptions);
            updateBetinfo(game, scorePosions, lines, betline);
        }
    });
    betone.events.onInputOut.add(function(){
        betone.loadTexture('betone');
    });


    betmax = game.add.sprite(535,510, 'betmax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        betmax.loadTexture('betmax_p');
    });
    betmax.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            maxBetline();
            updateBetinfo(game, scorePosions, lines, betline);
            //betMaxSound.play();
        }
    });
    betmax.events.onInputOut.add(function(){
        betmax.loadTexture('betmax');
    });

    automaricstart = game.add.sprite(685,510, 'automaricstart');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = true;
    automaricstart.input.useHandCursor = true;
    automaricstart.events.onInputOver.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart_p');
        }
    });
    automaricstart.events.onInputOut.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart');
        }
    });
    automaricstart.events.onInputDown.add(function(){
        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;
                    hideLines([]);
                    requestSpin(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                autostart = true;
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton');
        }
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons();
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }

    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;
    buttonLine1.events.onInputOver.add(function(){
        buttonLine1.loadTexture('buttonLine1_p');
    });
    buttonLine1.events.onInputOut.add(function(){
        buttonLine1.loadTexture('buttonLine1');
    });
    buttonLine1.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1]);
    });
    buttonLine1.events.onInputDown.add(function(){
        soundForBattons[0].play();
        hideLines([]);
        linesArray = [11];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1];
        showNumbers(numberArray);

        lines = 1;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3]);
    });
    buttonLine3.events.onInputDown.add(function(){
        soundForBattons[1].play();

        hideLines([]);
        linesArray = [11, 12, 13];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3];
        showNumbers(numberArray);

        lines = 3;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5]);
    });
    buttonLine5.events.onInputDown.add(function(){
        soundForBattons[2].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5];
        showNumbers(numberArray);

        lines = 5;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7]);
    });
    buttonLine7.events.onInputDown.add(function(){
        soundForBattons[3].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7];
        showNumbers(numberArray);

        lines = 7;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });
    buttonLine9.events.onInputDown.add(function(){
        soundForBattons[4].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17, 18, 19];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        showNumbers(numberArray);

        lines = 9;
        updateBetinfo(game, scorePosions, lines, betline);
    });

}

//кнопки для карт
function addButtonsGame2(game) {

    var soundForBattons = [];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        startButton.loadTexture('startButton_p');
    });
    startButton.events.onInputOut.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputDown.add(function(){
        requestDouble(gamename, 1, lines, bet, sid);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputDown.add(function(){
        requestDouble(gamename, 2, lines, bet, sid);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputDown.add(function(){
        requestDouble(gamename, 3, lines, bet, sid);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputDown.add(function(){
        requestDouble(gamename, 4, lines, bet, sid);
    });

}

//кнопки и логика для игры с последовательным выбором
var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
function addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm){
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;

    buttonLine1.events.onInputOver.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1_p');
        }
    });
    buttonLine1.events.onInputOut.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1');
        }
    });

    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine5.events.onInputOver.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5_p');
        }
    });
    buttonLine5.events.onInputOut.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5');
        }
    });

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });

    buttonLine9.events.onInputOver.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9_p');
        }
    });
    buttonLine9.events.onInputOut.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9');
        }
    });

    buttonLine1.events.onInputDown.add(function(){

        buttonLine1.loadTexture('buttonLine1_d');
        buttonLine1.inputEnabled = false;
        buttonLine1.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) {
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[0](ropeValues, ropeStep, checkHelm);

        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[0][0],winRopeNumberPosition[0][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }


        ropesAnim[1](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[1][0],winRopeNumberPosition[1][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine5.events.onInputDown.add(function(){

        buttonLine5.loadTexture('buttonLine5_d');
        buttonLine5.inputEnabled = false;
        buttonLine5.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[2](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[2][0],winRopeNumberPosition[2][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[3](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[3][0],winRopeNumberPosition[3][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine9.events.onInputDown.add(function(){

        buttonLine9.loadTexture('buttonLine9_d');
        buttonLine9.inputEnabled = false;
        buttonLine9.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[4](ropeValues, ropeStep);
        setTimeout(showWinGame3, timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[4][0],winRopeNumberPosition[4][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
}

// для выбора одного из двух (действия при нажатии некоторых кнопок нужно задать в самой игре)
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
function addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1) {
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = false;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });
    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        selectionsAmin[0](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }

    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        selectionsAmin[1](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }
    });
}

//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
var buttonsArray = [];
function hideButtons(buttonsArray) {
    if(!isMobile){
        if(buttonsArray.length == 0) {
            if(autostart == false){
                automaricstart.loadTexture('automaricstart_d');
                automaricstart.inputEnabled = false;
                automaricstart.input.useHandCursor = false;
            }

            selectGame.loadTexture('selectGame_d');
            selectGame.inputEnabled = false;
            selectGame.input.useHandCursor = false;

            payTable.loadTexture('payTable_d');
            payTable.inputEnabled = false;
            payTable.input.useHandCursor = false;

            betone.loadTexture('betone_d');
            betone.inputEnabled = false;
            betone.input.useHandCursor = false;

            betmax.loadTexture('betmax_d');
            betmax.inputEnabled = false;
            betmax.input.useHandCursor = false;

            startButton.loadTexture('startButton_d');
            startButton.inputEnabled = false;
            startButton.input.useHandCursor = false;

            buttonLine1.loadTexture('buttonLine1_d');
            buttonLine1.inputEnabled = false;
            buttonLine1.input.useHandCursor = false;

            buttonLine3.loadTexture('buttonLine3_d');
            buttonLine3.inputEnabled = false;
            buttonLine3.input.useHandCursor = false;

            buttonLine5.loadTexture('buttonLine5_d');
            buttonLine5.inputEnabled = false;
            buttonLine5.input.useHandCursor = false;

            buttonLine7.loadTexture('buttonLine7_d');
            buttonLine7.inputEnabled = false;
            buttonLine7.input.useHandCursor = false;

            buttonLine9.loadTexture('buttonLine9_d');
            buttonLine9.inputEnabled = false;
            buttonLine9.input.useHandCursor = false;

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]+'_d');
                item[0].inputEnabled = false;
                item[0].input.useHandCursor = false;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = false;
            double.visible = false;
            bet1.visible = false;
            dollar.visible = false;
            gear.visible = false;
            home.visible = false;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = false;
            })
        }
    }
}

function showButtons(buttonsArray) {
    if(!isMobile) {
        if(buttonsArray.length == 0) {

            if(autostart == false){
                automaricstart.loadTexture('automaricstart');
                automaricstart.inputEnabled = true;
                automaricstart.input.useHandCursor = true;

                selectGame.loadTexture('selectGame');
                selectGame.inputEnabled = true;
                selectGame.input.useHandCursor = true;

                payTable.loadTexture('payTable');
                payTable.inputEnabled = true;
                payTable.input.useHandCursor = true;

                betone.loadTexture('betone');
                betone.inputEnabled = true;
                betone.input.useHandCursor = true;

                betmax.loadTexture('betmax');
                betmax.inputEnabled = true;
                betmax.input.useHandCursor = true;

                startButton.loadTexture('startButton');
                startButton.inputEnabled = true;
                startButton.input.useHandCursor = true;

                buttonLine1.loadTexture('buttonLine1');
                buttonLine1.inputEnabled = true;
                buttonLine1.input.useHandCursor = true;

                buttonLine3.loadTexture('buttonLine3');
                buttonLine3.inputEnabled = true;
                buttonLine3.input.useHandCursor = true;

                buttonLine5.loadTexture('buttonLine5');
                buttonLine5.inputEnabled = true;
                buttonLine5.input.useHandCursor = true;

                buttonLine7.loadTexture('buttonLine7');
                buttonLine7.inputEnabled = true;
                buttonLine7.input.useHandCursor = true;

                buttonLine9.loadTexture('buttonLine9');
                buttonLine9.inputEnabled = true;
                buttonLine9.input.useHandCursor = true;
            }

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]);
                item[0].inputEnabled = true;
                item[0].input.useHandCursor = true;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = true;
            double.visible = true;
            bet1.visible = true;
            dollar.visible = true;
            gear.visible = true;
            home.visible = true;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = true;
            })
        }
    }
}


//слоты и их вращение

//переменные содержащие все объекты слотов
var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
var slot1Anim; var slot2Anim; var slot3Anim;  var slot4Anim; var slot5Anim; var slot6Anim; var slot7Anim; var slot8Anim; var slot9Anim; var slot10Anim; var slot11Anim; var slot12Anim; var slot13Anim; var slot14Anim; var slot15Anim;

function addSlots(game, slotPosition) {
    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
    var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];
    var slotCellAnimName = 'cellAnim';

    slot1 = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[0]);
    slot2 = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[1]);
    slot3 = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[3]);
    slot4 = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[4]);
    slot5 = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[7]);
    slot6 = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[2]);
    slot7 = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[4]);
    slot8 = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[3]);
    slot9 = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[8]);
    slot10 = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[2]);
    slot11 = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[0]);
    slot12 = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[1]);
    slot13 = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[5]);
    slot14 = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[3]);
    slot15 = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[5]);

    slot1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotCellAnimName);
    slot2Anim = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotCellAnimName);
    slot3Anim = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotCellAnimName);
    slot4Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotCellAnimName);
    slot5Anim = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotCellAnimName);
    slot6Anim = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotCellAnimName);
    slot7Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotCellAnimName);
    slot8Anim = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotCellAnimName);
    slot9Anim = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotCellAnimName);
    slot10Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotCellAnimName);
    slot11Anim = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotCellAnimName);
    slot12Anim = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotCellAnimName);
    slot13Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotCellAnimName);
    slot14Anim = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotCellAnimName);
    slot15Anim = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotCellAnimName);

    slot1Anim.animations.add('slot1Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot2Anim.animations.add('slot2Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot3Anim.animations.add('slot3Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot4Anim.animations.add('slot4Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot5Anim.animations.add('slot5Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot6Anim.animations.add('slot6Anim', [5,6,7,8,0,1,2,3,4], 20, true);
    slot7Anim.animations.add('slot7Anim', [6,7,8,0,1,2,3,4,5], 20, true);
    slot8Anim.animations.add('slot8Anim', [7,8,0,1,2,3,4,5,6], 20, true);
    slot9Anim.animations.add('slot9Anim', [8,0,1,2,3,4,5,6,7], 20, true);
    slot10Anim.animations.add('slot10Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot11Anim.animations.add('slot11Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot12Anim.animations.add('slot12Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot13Anim.animations.add('slot13Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot14Anim.animations.add('slot14Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot15Anim.animations.add('slot15Anim', [7,8,0,1,2,3,4,5,6], 20, true);

    slot1Anim.animations.getAnimation('slot1Anim').play();
    slot2Anim.animations.getAnimation('slot2Anim').play();
    slot3Anim.animations.getAnimation('slot3Anim').play();
    slot4Anim.animations.getAnimation('slot4Anim').play();
    slot5Anim.animations.getAnimation('slot5Anim').play();
    slot6Anim.animations.getAnimation('slot6Anim').play();
    slot7Anim.animations.getAnimation('slot7Anim').play();
    slot8Anim.animations.getAnimation('slot8Anim').play();
    slot9Anim.animations.getAnimation('slot9Anim').play();
    slot10Anim.animations.getAnimation('slot10Anim').play();
    slot11Anim.animations.getAnimation('slot11Anim').play();
    slot12Anim.animations.getAnimation('slot12Anim').play();
    slot13Anim.animations.getAnimation('slot13Anim').play();
    slot14Anim.animations.getAnimation('slot14Anim').play();
    slot15Anim.animations.getAnimation('slot15Anim').play();

    slot1Anim.visible = false;
    slot2Anim.visible = false;
    slot3Anim.visible = false;
    slot4Anim.visible = false;
    slot5Anim.visible = false;
    slot6Anim.visible = false;
    slot7Anim.visible = false;
    slot8Anim.visible = false;
    slot9Anim.visible = false;
    slot10Anim.visible = false;
    slot11Anim.visible = false;
    slot12Anim.visible = false;
    slot13Anim.visible = false;
    slot14Anim.visible = false;
    slot15Anim.visible = false;
}

var finalValues; var wlValues; var balanceR; var totalWin; var totalWinR; var dcard; var linesR; var betlineR; //totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep = 0;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');

    if (find(dataArray, 'result=ok') !== -1 && find(dataArray, 'state=0') !== -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        if(typeof finalValues != "undefined") {
            totalWinR = finalValues[15];
            linesR = finalValues[16];
            betlineR = finalValues[17];

            checkSpinResult(totalWinR);

            slotRotation(game, finalValues);
        }




    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons([]);

        checkRopeGameAnim = 1;

        var bonusWin = game.add.audio('bonusWin');
        bonusWin.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 5500);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('topScoreGame2'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = []; // массив для объектов звуков
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');
        soundWinLines[5] = game.add.audio('soundWinLine6');
        soundWinLines[6] = game.add.audio('soundWinLine7');
        soundWinLines[7] = game.add.audio('soundWinLine8');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {
                if(!isMobile) {
                    showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);
                } else {
                    showButtons([[startButton], [home], [gear], [dollar], [double]]);
                }

                hideNumberWinLine();

                game.bear.visible = false;
                game.ears.visible = false;
                game.bee.visible = false;
                game.win.visible = true; 
                game.winAnimation.play();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk1');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий
                if(isMobile) {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 576-mobileX, 429-mobileY);
                } else {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 576, 429);
                }
                soundWinLines[soundWinLinesCounter].play();
                soundWinLinesCounter += 1;
                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            hideLines([]);
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('play1To');
            updateBalance(game, scorePosions, balanceOld, balance);
            if(isMobile){
                showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            } else {
                showButtons();
            }
            checkAutoStart = false;
        }
    }

}


//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons([]);
    //alert('betline: '+betline+', lines: '+lines+', bet: '+bet+', sid: '+sid);
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
        dataType: 'html',
        success: function (data) {
            //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|1&extralife=45&jackpots=1822.16|4122.16|6122.16';
            //alert(data);
            // data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|55|5|5|5|1|1|extralife=45&jackpots=1822.16|4122.16|6122.16';
            
            dataSpinRequest = data;

            parseSpinAnswer(dataSpinRequest);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1,2,3], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [0,1,2,3], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

function hideSelectionOfTheManyCellAnim(monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item].visible = false;
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('take');
    hideButtons([]);

    hideNumbersAmin();
    hideLines([]);

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
var tableTitle; // название изображения заданное в прелодере
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle.visible = true;
    tableTitle.loadTexture(loadTexture);
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
       font: '24px "TeX Gyre Schola Bold"',
       fill: '#fff567',
       stroke: '#000000',
       strokeThickness: 3,
   });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}


// ajax-запросы

//init-запрос
function requestInit() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=init',
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            dataString = data;

            initDataArray = dataString.split('&');

            initDataArray.forEach(function (item) {
                if(item.indexOf('SID=') + 1) {
                    sid = item.replace('SID=','');
                }
                if(item.indexOf('user=') + 1) {
                    user = item.replace('user=','');
                }
            });

            if (data.length != 0 && (find(initDataArray, 'result=ok')) != -1 && (find(initDataArray, 'state=0')) != -1) {
                requestState();
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//state-запрос
var jackpots;
function requestState() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=state&min=1&game='+gamename+'&SID='+sid,
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            var dataString = data;

            startDataArray = dataString.split('&');

            if (data.length !== 0 && (find(startDataArray, ' result=ok')) != -1 && (find(startDataArray, 'state=0')) != -1) {

                startDataArray = dataString.split('&');

                startDataArray.forEach(function (item) {
                    if(item.indexOf('balance=') + 1) {
                        balance = item.replace('balance=', '').replace('.00','');
                    }
                    if(item.indexOf('extralife=') + 1) {
                        extralife = item.replace('extralife=','');
                    }
                    if(item.indexOf('jackpots=') + 1) {
                        var jackpotsString = item.replace('jackpots=','');
                        jackpots =  jackpotsString.split('|');
                    }
                    if(item.indexOf('id=') + 1) {
                        id = item.replace('id=','');
                    }
                    if(item.indexOf('min=') + 1) {
                        min = item.replace('min=','');
                    }

                    /*game1();
                     game2();
                     game3();
                     game4();*/
                 });
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    if(checkGame != 2){
        betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
            font: scorePosions[0][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    if(checkGame == 1){
        betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Arial Black"',
            fill: '#ffff00',
            stroke: '#000000',
            strokeThickness: 3,
        });

        betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
            font: scorePosions[4][2]+'px "Arial Black"',
            fill: '#ffff00',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    if(checkGame == 2){
        riskStep = game.add.text(scorePosions[0][0], scorePosions[0][1], betline, {
            font: scorePosions[3][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }
}

var takeWin;
var textCounter;
var topBarImage;
var ActionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'startButton']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        ActionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время
                if(isMobile) {
                    showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
                } else {
                    showButtons();
                }

                takeWin.stop();
                changeTableTitle('play1To');
                clearInterval(textCounter);

                topBarImage.loadTexture('topScoreGame1'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                        fill: '#fff567',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                        fill: '#fff567',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    betScore.visible = false;
    linesScore.visible = false;
    betline1Score.visible = false;
    betline2Score.visible = false;

    bet = lines*betline;
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
        font: scorePosions[3][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
        font: scorePosions[4][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];
    } else {
        betlineCounter = 0;
        betline = betlineOptions[betlineCounter];
    }
}

function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard, lines, bet, sid, game) {
    hideButtons([[startButton, 'startButton']]);
    lockDisplay();
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
        dataType: 'html',
        success: function (data) {

            dataDoubleRequest = data.split('&');
            parseDoubleAnswer(dataDoubleRequest, selectedCard, game);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

var dwin; var dcard2; var selectedCardR;
function parseDoubleAnswer(dataDoubleRequest, selectedCard, game) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResult(totalWin, selectedCard, game);

    }
}

var step = 1;
function showDoubleResult(totalWin, selectedCard) {
    bear_waitt.visible = false;
    setTimeout('hithit_game2.play()', 500);
    
    if(totalWin > 0) {
        hideDoubleToAndTakeOrRiskTexts();
        if (selectedCard == 1) {
            bear_hit_left.visible = true;

            bear_hit_left.animations.add('bear_hit_left', [0,1,2,3,4,5,6,5,6], 10, false).play().onComplete.add(function(){
                bear_hit_left.visible = false;
                bear_win_left_1.visible = true;
                win_first_game2.play();
                bear_win_left_1.animations.add('bear_win_left_1', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                    bear_win_left_1.visible = false;
                    bear_win_left_2.visible = true;
                    bear_win_left_2.animations.add('bear_win_left_2', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                        bear_win_left_2.visible = false;
                        bear_win_top = game.add.sprite(279-mobileX,148-mobileY, 'bear_win_top');       
                        step += 1;
                        updateTotalWin(game, dwin, step);
                        final_win_game2.play();   
                        bear_win_top.animations.add('bear_hit_left', [0,1,2,3,4,5,6], 8, true).play();
                        for (var i = 1; i <= 30; ++i) { 
                            movingWall(tree[i])
                            movingWall(beehive_l[i])
                            movingWall(beehive_r[i])
                        }
                        for (var i = 1; i <= 20; ++i) {
                            movingWall(cloud_l_l[i])
                            movingWall(cloud_r_l[i])
                            movingWall(cloud_l_s[i])
                            movingWall(cloud_r_s[i])
                        }
                    });
                });
            });
        } else {
            bear_hit_right.visible = true;
            bear_hit_right.animations.add('bear_hit_right', [0,1,2,3,4,5,6,5,6], 10, false).play().onComplete.add(function(){
                bear_hit_right.visible = false;
                bear_win_right_1.visible = true;
                win_first_game2.play();
                bear_win_right_1.animations.add('bear_win_right_1', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                    bear_win_right_1.visible = false;
                    bear_win_right_2.visible = true;
                    bear_win_right_2.animations.add('bear_win_right_2', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                        bear_win_right_2.visible = false;
                        bear_win_top = game.add.sprite(279-mobileX,148-mobileY, 'bear_win_top');    
                        step += 1;
                        updateTotalWin(game, dwin, step); 
                        final_win_game2.play();     
                        bear_win_top.animations.add('bear_hit_right', [0,1,2,3,4,5,6], 8, true).play();
                        for (var i = 1; i <= 30; ++i) { 
                            movingWall(tree[i])
                            movingWall(beehive_l[i])
                            movingWall(beehive_r[i])
                        }
                        for (var i = 1; i <= 20; ++i) {
                            movingWall(cloud_l_l[i])
                            movingWall(cloud_r_l[i])
                            movingWall(cloud_l_s[i])
                            movingWall(cloud_r_s[i])
                        }
                    });
                });
            });
        }


    } else {
        hideDoubleToAndTakeOrRiskTexts();
        if (selectedCard == 1){
            bear_hit_left.visible = true;
            bear_hit_left.animations.add('bear_hit_left', [0,1,2,3,4,5,6,5,6], 8, false).play().onComplete.add(function(){
                bear_hit_left.visible = false;
                bee_lose_1_l.visible = true;
                lose_game2.play();
                bee_lose_1_l.animations.add('bee_lose_1', [0,1,2,3,4], 8, false).play().onComplete.add(function(){
                    bee_lose_1_l.visible = false;
                    bee_lose_2_l.visible = true;
                    bee_lose_2_l.animations.add('bee_lose_2', [0,1,2,3,4,5,6], 8, true).play();
                });
                bear_lose_left_1.visible = true;
                bear_lose_left_1.animations.add('bear_lose_left_1', [0,1,2,3,4,5,6,5,6,7,8,9,10,11,12,13], 8, false).play().onComplete.add(function(){
                    bear_lose_left_1.visible = false;
                    bear_lose_left_2 = game.add.sprite(279-mobileX,148-mobileY, 'bear_lose_left_2');
                    bear_lose_left_2.animations.add('bear_lose_left_2', [0,1,2], 8, false).play().onComplete.add(function(){
                        bear_lose_left_2.visible = false;
                        bear_lose_down_1 = game.add.sprite(279-mobileX,148-mobileY, 'bear_lose_down');
                        bear_lose_down_1.animations.add('bear_lose_down', [0,1,2,3], 8, false).play().onComplete.add(function(){
                            bear_lose_down_1.visible = false;
                            bear_lose_down_2.visible = true;
                            bear_lose_down_2.animations.add('bear_lose_down', [4,5,6], 8, true).play()
                            game.add.tween(bear_lose_down_2).to({y:bear_lose_down_2.position.y+300}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                                bear_lose_down_2.visible = false;
                                leaves_1.visible = true;
                                leaves_1.animations.add('leaves_1', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
                                    leaves_1.visible = false;
                                    leaves_2.visible = true;
                                    leaves_2.animations.add('leaves_2', [0,1,2,3,4,5,6,7,8,9,10,11,12], 8, false).play().onComplete.add(function(){
                                        leaves_2.visible = false;
                                        game.state.start('game1');
                                        unlockDisplay();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        } else {
            bear_hit_right.visible = true;
            bear_hit_right.animations.add('bear_hit_right', [0,1,2,3,4,5,6,5,6], 8, false).play().onComplete.add(function(){
                bear_hit_right.visible = false;
                bee_lose_1_r.visible = true;
                lose_game2.play();
                bee_lose_1_r.animations.add('bee_lose_1', [0,1,2,3,4], 8, false).play().onComplete.add(function(){
                    bee_lose_1_r.visible = false;
                    bee_lose_2_r.visible = true;
                    bee_lose_2_r.animations.add('bee_lose_2', [0,1,2,3,4,5,6], 8, true).play();
                });
                bear_lose_right_1.visible = true;
                bear_lose_right_1.animations.add('bear_lose_right_1', [0,1,2,3,4,5,6,5,6,7,8,9,10,11,12,13], 8, false).play().onComplete.add(function(){
                    bear_lose_right_1.visible = false;
                    bear_lose_right_2 = game.add.sprite(279-mobileX,148-mobileY, 'bear_lose_right_2');
                    bear_lose_right_2.animations.add('bear_lose_right_2', [0,1], 8, false).play().onComplete.add(function(){
                        bear_lose_right_2.visible = false;
                        bear_lose_down_1 = game.add.sprite(279-mobileX,148-mobileY, 'bear_lose_down');
                        bear_lose_down_1.animations.add('bear_lose_down', [0,1,2,3], 8, false).play().onComplete.add(function(){
                            bear_lose_down_1.visible = false;
                            bear_lose_down_2.visible = true;
                            bear_lose_down_2.animations.add('bear_lose_down', [4,5,6], 8, true).play()
                            game.add.tween(bear_lose_down_2).to({y:bear_lose_down_2.position.y+300}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                                bear_lose_down_2.visible = false;
                                leaves_1.visible = true;
                                leaves_1.animations.add('leaves_1', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
                                    leaves_1.visible = false;
                                    leaves_2.visible = true;
                                    leaves_2.animations.add('leaves_2', [0,1,2,3,4,5,6,7,8,9,10,11,12], 8, false).play().onComplete.add(function(){

                                        leaves_2.visible = false;
                                        game.state.start('game1');
                                        unlockDisplay();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
        // tableTitle.visible = true;
        // changeTableTitle('loseTitleGame2');
        // hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
        // hideDoubleToAndTakeOrRiskTexts();
        // openSelectedCard(selectedCardR, valuesOfAllCards);
        // setTimeout('openAllCards(valuesOfAllCards)', 1000);
        // setTimeout("tableTitle.visible = false; game.state.start('game1');unlockDisplay();", 2000);
    }
}
function movingWall(item) {
    game.add.tween(item).to({y:item.position.y+255}, 3000, Phaser.Easing.LINEAR, true).onComplete.add(function(){ 
        bear_win_top.visible = false;
        bear_waitt.visible = true;
        bear_waitt.animations.add('bear_wait', [0,1,2,2,2,2,2,2,2,2,1,0,3,4,5,5,5,5,5,5,5,5,4,3], 8, true).play();
        showButtons([[startButton, "startButton"]]);
        showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave);
        unlockDisplay();
    });
};
var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
function showDoubleToAndTakeOrRiskTexts(game, totalWin, x,y) {
    xSave = x;
    ySave = y;


    if(typeof(doubleWin) != "undefined") {
        doubleWin.visible = false;
    }

    doubleWin = game.add.sprite(359-mobileX,54-mobileY, 'game.double');
    if(typeof(doubleToText) != "undefined") {
        doubleToText.visible = false;
    }

    doubleToText = game.add.text(x-5, y, totalWin*2+' ?', {
        font: '26px "Arial"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    doubleWin.visible = false;
}

function updateTotalWin(game, dwin, step){
    //обновление totalWin в игре с картами

    linesScore.visible = false;
    riskStep.visible = false;

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], dwin, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    riskStep = game.add.text(scorePosions[0][0], scorePosions[0][1], step, {
        font: scorePosions[0][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];

//var cardPosition = [[x,y], [x,y], [x,y], [x,y], [x,y]]  - нулевой элемент массива карта диллера
var openCard; //звуки
var winCard;

//функции для игры с последовательным выбором (веревки, ящики, бочки и т.д.)

// отображает результат выбора веревки (подлетает цифра и пересчитвается значение totalWin)
var loseTryInGame3 = false;
function showWinGame3(winRopeNumberSize, x, y, win, stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim) {

    if(typeWinRopeNumberAnim == 0) {
        if(loseTryInGame3 == false) {
            if(ropeValues[ropeStep] > 0 && ropeStep == 5) {
                number_win = game.add.audio('number_win');
                number_win.play();
                var text = game.add.text(x,y, win, { font: '30px \"Impact\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

                linesScore.visible = false;
                linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                    font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                    fill: '#fff567',
                    stroke: '#000000',
                    strokeThickness: 3,
                });

                ropeStep = 0;
                loseTryInGame3 = false; //удаляем память о проигрышной попытке (использовании шлема)

                setTimeout("game.state.start('game4')", timeoutGame3ToGame4);
            } else {
                if(win == 0) {
                    updateBalanceGame3(game, scorePosions, balanceR);
                } else {
                    number_win = game.add.audio('number_win');
                    number_win.play();
                    var text = game.add.text(x,y, win, { font: '30px \"Impact\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

                    linesScore.visible = false;
                    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                        fill: '#fff567',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
            }
        } else {
            if(ropeValues[ropeStep] > 0 && ropeStep == 5) {
                number_win = game.add.audio('number_win');
                number_win.play();
                var text = game.add.text(x,y, win, { font: '30px \"Impact\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

                linesScore.visible = false;
                linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                    font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                    fill: '#fff567',
                    stroke: '#000000',
                    strokeThickness: 3,
                });

                ropeStep = 0;
                loseTryInGame3 = false; //удаляем память о проигрышной попытке (использовании шлема)

                setTimeout("game.state.start('game1')", timeoutGame3ToGame4);
            } else {
                if(win == 0) {
                    updateBalanceGame3(game, scorePosions, balanceR);
                } else {
                    number_win = game.add.audio('number_win');
                    number_win.play();
                    var text = game.add.text(x,y, win, { font: '30px \"Impact\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

                    linesScore.visible = false;
                    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                        fill: '#fff567',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
            }
        }

    }

}

function updateBalanceGame3(game, scorePosions, balanceR) {
    if(!isMobile) {
        hideButtons([[buttonLine1, 'buttonLine1'],[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], ]);
    }

    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bol"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bol"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}



//функции для игры с выбором из двух вариантов

function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
    }, timeInterval);
}





//функции для мобильной версии

var startButton; var double; var bet1; var dollar; var gear; var home;
function addButtonsGame1Mobile(game) {

    startButton = game.add.sprite(588, 228, 'startButton');
    startButton.bringToTop();
    startButton.anchor.setTo(0.5, 0.5);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            startButton.loadTexture('startButton_d');
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    double = game.add.sprite(549, 133, 'double');
    double.inputEnabled = true;
    double.input.useHandCursor = true;
    double.events.onInputDown.add(function(){
        checkWin = 0;
        hideNumbersAmin();
        game.state.start('game2');
    });
    double.events.onInputUp.add(function(){
        double.loadTexture('double');
    });
    double.visible = false;

    bet1 = game.add.sprite(546, 274, 'bet1');
    bet1.inputEnabled = true;
    bet1.input.useHandCursor = true;
    bet1.events.onInputDown.add(function(){
        /*lines = 9;
        betline = 25;*/

        bet1.loadTexture('bet1_p');
        document.getElementById('betMode').style.display = 'block';
        widthVisibleZone = $('.betWrapper .visibleZone').height();
        console.log(widthVisibleZone);
        $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
        $('canvas').css('display', 'none');
    });
    bet1.events.onInputUp.add(function(){
        bet1.loadTexture('bet1');
    });

    dollar = game.add.sprite(445, 30, 'dollar');
    dollar.scale.setTo(0.7,0.7);
    dollar.inputEnabled = true;
    dollar.input.useHandCursor = true;
    dollar.events.onInputDown.add(function(){
        //game.state.start('game4');
    });

    gear = game.add.sprite(519, 30, 'gear');
    gear.scale.setTo(0.7,0.7);
    gear.inputEnabled = true;
    gear.input.useHandCursor = true;
    gear.events.onInputDown.add(function(){
        //game.state.start('game3');
    });
    gear.events.onInputUp.add(function(){
        // hideButtons([[payTable, 'payTable'], [betmax, 'betmax'], [betone, 'betone'], [automaricstart, 'automaricstart'], [selectGame, 'selectGame'], [buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"]]);
        hideButtons([]);
        pagePaytables[1].visible = true;
        prev_page.visible = true;
        exit_btn.visible = true;
        next_page.visible = true;
        settingsMode = true;
        currentPage = 1;
        betline1Score.visible = false;
        betline2Score.visible = false;
        betScore.visible = false;
        linesScore.visible = false;
        balanceScore.visible = false;
    });

    home = game.add.sprite(45, 30, 'home');
    home.scale.setTo(0.7,0.7);
    home.inputEnabled = true;
    home.input.useHandCursor = true;
    home.events.onInputDown.add(function(){
        home.loadTexture('home_d');
    });
    home.events.onInputUp.add(function(){
        home.loadTexture('home');
    });
}
var pagePaytables =[];
var settingsMode = false;
var currentPage = null;

function addPaytable(pageCount, pageCoord, btnCoord){
    pageSound = game.add.audio('page');
    for (var i = 1; i <= pageCount; ++i) {
        pagePaytable = game.add.sprite(pageCoord[i-1][0], pageCoord[i-1][1], 'pagePaytable_' + i);
        pagePaytable.visible = false;
        pagePaytables[i] = pagePaytable;
    }

    prev_page = game.add.sprite(btnCoord[0][0], btnCoord[0][1], 'prev_page');
    prev_page.visible = false;
    prev_page.inputEnabled = true;
    prev_page.input.useHandCursor = true;
    prev_page.events.onInputUp.add(function(){
        if (settingsMode)  {
            pageSound.play();
            if (currentPage == 1)
                currentPage = pageCount;
            else{
                pagePaytables[currentPage].visible = false;
                currentPage -=1;
            }
        }
        pagePaytables[currentPage].visible = true;
    });
    exit_btn = game.add.sprite(btnCoord[1][0], btnCoord[1][1], 'exit_btn');
    exit_btn.visible = false;
    exit_btn.inputEnabled = true;
    exit_btn.input.useHandCursor = true;
    exit_btn.events.onInputUp.add(function(){
        pageSound.play();
        for (var i = 1; i <= pageCount; ++i) {
            pagePaytables[i].visible = false;
        }            
        prev_page.visible = false;
        exit_btn.visible = false;
        next_page.visible = false;
        settingsMode = false;
        betline1Score.visible = true;
        betline2Score.visible = true;
        betScore.visible = true;
        linesScore.visible = true;
        balanceScore.visible = true;
        showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
        // showButtons([[betmax, 'betmax'], [betone, 'betone'], [automaricstart, 'automaricstart'], [selectGame, 'selectGame'], [payTable, 'payTable'], [buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"]]);
    });
    next_page = game.add.sprite(btnCoord[2][0], btnCoord[2][1], 'next_page');
    next_page.visible = false;
    next_page.inputEnabled = true;
    next_page.input.useHandCursor = true;
    next_page.events.onInputUp.add(function(){
        if (settingsMode)  {
            pageSound.play();
            if (currentPage == pageCount){
                pagePaytables[currentPage].visible = false;
                currentPage = 1;
            } else if (currentPage == 1){
                currentPage +=1;
            } else {                    
                pagePaytables[currentPage].visible = false;
                currentPage +=1;
            }
        }
        pagePaytables[currentPage].visible = true;
    });
};

function addButtonsGame2Mobile(game) {
    startButton = game.add.sprite(538, 300, 'collect');
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('collect');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });
    startButton.scale.setTo(0.7,0.7);
}

var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
var buttonsGame3Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок

function addButtonsGame3Mobile(game, ropesAnim, buttonsGame3Mobile, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm) {
    //логика для событий нажатия на изображения
    buttonsGame3Mobile.forEach(function (button, i) {
        button.events.onInputDown.add(function(){

            button.inputEnabled = false;

            if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                stepTotalWinR += betline*lines*ropeValues[ropeStep];
            }

            ropesAnim[i](ropeValues,ropeStep, checkHelm);

            //setTimeout(showWin, 500, 195+440-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);
            setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[i][0],winRopeNumberPosition[i][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

            ropeStep += 1;
        });
    });
}

//функции для игры с выбором из двух вариантов
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
var buttonsGame4Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок
function addButtonsGame4Mobile(game, selectionsAmin, buttonsGame4Mobile,  timeout1Game4ToGame1, timeout2Game4ToGame1) {
    buttonsGame4Mobile.forEach(function (button, i) {
        button.events.onInputDown.add(function(){
            button.inputEnabled = false;

            selectionsAmin[i](ropeValues, ropeStep);

            if(ropeValues[5] != 0){
                lockDisplay();
                setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
            } else {
                lockDisplay();
                setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
            }
        });
    });
}






//выбор множителя в меню bet
var denomination = 1;
function selectDenomination(el) {
    denomination = el.innerText;

    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;

    var selectedElement = document.getElementsByClassName('denomSize selected');
    selectedElement[0].className = 'denomSize';

    el.className = 'denomSize selected';
}

//выставление максимального значения ставки на линию
function maxBetlineForBetMenu() {
    maxBetline();
    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    document.getElementsByClassName('checkCssTopBetLineRange')[0].style.top = '34.5%';
    document.querySelectorAll('.checkCssTopBetLineRange')[0].querySelectorAll('.selected')[0].classList.remove('selected');
    document.getElementById('cellBetLine25').classList.add('selected');
}


//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

requestInit();

(function () {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;


        // звуки

        var playSound = game.add.audio('play');

        slotPosition = [[142-mobileX, 90-mobileY], [142-mobileX, 202-mobileY], [142-mobileX, 314-mobileY], [254-mobileX, 90-mobileY], [254-mobileX, 202-mobileY], [254-mobileX, 314-mobileY], [365-mobileX, 90-mobileY], [365-mobileX, 202-mobileY], [365-mobileX, 314-mobileY], [477-mobileX, 90-mobileY], [477-mobileX, 202-mobileY], [477-mobileX, 314-mobileY], [589-mobileX, 90-mobileY], [589-mobileX, 202-mobileY], [589-mobileX, 314-mobileY]];
        addSlots(game, slotPosition);
        // изображения

        game.add.sprite(94-mobileX,22-mobileY, 'game.background1');
        topBarImage = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame1');

        addTableTitle(game, 'play1To',558-mobileX,422-mobileY);


        var linePosition = [[134-mobileX,245-mobileY], [134-mobileX,109-mobileY], [134-mobileX,380-mobileY], [134-mobileX,156-mobileY], [134-mobileX,130-mobileY], [134-mobileX,130-mobileY], [134-mobileX,261-mobileY], [134-mobileX,268-mobileY], [134-mobileX,155-mobileY]];
        var numberPosition = [[94-mobileX,230-mobileY], [94-mobileX,86-mobileY], [94-mobileX,374-mobileY], [94-mobileX,150-mobileY], [94-mobileX,310-mobileY], [94-mobileX,118-mobileY], [94-mobileX,342-mobileY], [94-mobileX,262-mobileY], [94-mobileX,198-mobileY]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);

        // кнопки
        addButtonsGame1Mobile(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [99-mobileX, 59-mobileY, 16], [707-mobileX, 59-mobileY, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        game.bear = game.add.sprite(125-mobileX, 391-mobileY, 'bear');
        game.bearAnimation = game.bear.animations.add('bear', [0,1,2,3,4,5,6,2,1,0,0,0,0,0,0,0,0], 8, true);
        game.bearAnimation.play();     
        game.ears = game.add.sprite(126-mobileX, 391-mobileY, 'ears');
        game.earsAnimation = game.ears.animations.add('ears', [0,1,2,1,0,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 12, true);
        game.earsAnimation.play();
        game.win = game.add.sprite(125-mobileX, 391-mobileY, 'game.win1');
        game.winAnimation = game.win.animations.add('game.win1', [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 10, false);
        game.win.visible = false;  
        game.bee = game.add.sprite(414-mobileX, 390-mobileY, 'bee');
        game.beeAnimation = game.bee.animations.add('bee', [0,1,2,3,4,5,6,7,6,5,4,3,2,1], 8, true);
        game.beeAnimation.play();
        game.winAnimation.onComplete.add(function(){
            game.win.visible = false;
            game.bear.visible = true;
            game.ears.visible = true;
            game.bee.visible = true;
        }); 
        var pageCount = 5;
        var pageCoord = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]];   
        var btnCoord = [[140-mobileX, 447-mobileY], [324-mobileX, 447-mobileY], [502-mobileX, 447-mobileY]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);
    };



    game1.update = function () {

    };

    game.state.add('game1', game1);

})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================

var cloud_l_l = [];
var cloud_r_l = [];
var cloud_l_s = [];
var cloud_r_s = [];
var tree = [];
var beehive_l = [];
var beehive_r = [];
(function () {

    var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        
        hithit_game2 = game.add.audio('hithit_game2');
        final_win_game2 = game.add.audio('final_win_game2');
        win_first_game2 = game.add.audio('win_first_game2');
        lose_game2 = game.add.audio('lose_game2');
        //изображения

        bee_lose_1_l = game.add.sprite(262-mobileX,195-mobileY, 'bee_lose_1');
        bee_lose_1_l.inputEnabled = true;
        bee_lose_1_l.input.useHandCursor = true;
        bee_lose_1_l.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid, game);
        });
        bee_lose_1_r = game.add.sprite(486-mobileX,195-mobileY, 'bee_lose_1');
        bee_lose_1_r.inputEnabled = true;
        bee_lose_1_r.input.useHandCursor = true;
        bee_lose_1_r.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        backgroundGame3 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame2');
        for (var i = 1; i <= 20; ++i) {
            cloud_l_l[i] = game.add.sprite(94-mobileX,286-360*(i-1)-mobileY, 'cloud_1');
            cloud_r_l[i] = game.add.sprite(573-mobileX,283-360*(i-1)-mobileY, 'cloud_1');
            cloud_l_s[i] = game.add.sprite(100-mobileX,106-360*(i-1)-mobileY, 'cloud_2');
            cloud_r_s[i] = game.add.sprite(609-mobileX,103-360*(i-1)-mobileY, 'cloud_2');
        }
        for (var i = 1; i <= 30; ++i) { 
            treeNumber = randomNumber(1,2);
            tree[i] = game.add.sprite(262-mobileX,306-255*(i-1)-mobileY, 'tree_' + treeNumber); 
        }
        for (var i = 1; i <= 30; ++i) { 
            beehive_l[i] = game.add.sprite(262-mobileX,450-255*(i-1)-mobileY, 'beehive_wait'); 
            beehive_l[i].animations.add('beehive_wait', [0,1,2,3,4,5,6,7], 8, true).play();
            beehive_r[i] = game.add.sprite(486-mobileX,450-255*(i-1)-mobileY, 'beehive_wait');
            beehive_r[i].animations.add('beehive_wait', [0,1,2,3,4,5,6,7], 8, true).play();
        }

        topBarImage2 = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame3');
        
        leaves_1 = game.add.sprite(279-mobileX,400-mobileY, 'leaves_1');
        leaves_1.visible = false;
        leaves_2 = game.add.sprite(279-mobileX,400-mobileY, 'leaves_2');
        leaves_2.visible = false;
        bee_lose_1_l = game.add.sprite(262-mobileX,195-mobileY, 'bee_lose_1');
        bee_lose_1_l.visible = false;
        bee_lose_1_r = game.add.sprite(486-mobileX,195-mobileY, 'bee_lose_1');
        bee_lose_1_r.visible = false;
        bee_lose_2_l = game.add.sprite(262-mobileX,195-mobileY, 'bee_lose_2');
        bee_lose_2_l.visible = false;
        bee_lose_2_r = game.add.sprite(486-mobileX,195-mobileY, 'bee_lose_2');
        bee_lose_2_r.visible = false;
        bear_lose_down_2 = game.add.sprite(279-mobileX,148-mobileY, 'bear_lose_down');
        bear_lose_down_2.visible = false;
        bear_waitt = game.add.sprite(327-mobileX,148-mobileY, 'game2.bear_wait');
        bear_waitt.animations.add('bear_wait', [0,1,2,2,2,2,2,2,2,2,1,0,3,4,5,5,5,5,5,5,5,5,4,3], 8, true).play();
        bear_hit_left = game.add.sprite(279-mobileX,148-mobileY, 'bear_hit_left');        
        bear_hit_left.visible = false;
        bear_hit_right = game.add.sprite(279-mobileX,148-mobileY, 'bear_hit_right');
        bear_hit_right.visible = false;
        bear_lose_left_1 = game.add.sprite(279-mobileX,148-mobileY, 'bear_lose_left_1');
        bear_lose_left_1.visible = false;
        bear_lose_right_1 = game.add.sprite(279-mobileX,148-mobileY, 'bear_lose_right_1');
        bear_lose_right_1.visible = false;
        bear_win_left_1 = game.add.sprite(279-mobileX,148-mobileY, 'bear_win_left_1');
        bear_win_left_1.visible = false;
        bear_win_left_2 = game.add.sprite(279-mobileX,148-mobileY, 'bear_win_left_2');
        bear_win_left_2.visible = false;
        bear_win_right_1 = game.add.sprite(279-mobileX,148-mobileY, 'bear_win_right_1');
        bear_win_right_1.visible = false;
        bear_win_right_2 = game.add.sprite(279-mobileX,148-mobileY, 'bear_win_right_2');
        bear_win_right_2.visible = false;


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [485-mobileX, 59-mobileY, 24]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        showDoubleToAndTakeOrRiskTexts(game, totalWin, 398-mobileX, 100-mobileY);
        addButtonsGame2Mobile(game);
    };

    game2.update = function () {};

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================

(function () {

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {
        checkGame = 3;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;
        stepTotalWinR = 0;
        ropeStep = 0;
        checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса
        var checkUpLvl = true;

        //звуки
        // preOpenWinCover = game.add.sound('game.preOpenWinCover');
        // openWinCover = game.add.sound('game.openWinCover');
        hit_game3 = game.add.audio('hit_game3');
        lose_game3 = game.add.audio('lose_game3');

        //изображения
        background = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame4');
        background = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame3');

        // lever1Button = game.add.sprite(94-mobileX,390-mobileY, 'game.lever_p1');
        // lever2Button = game.add.sprite(222-mobileX,390-mobileY, 'game.lever_p3');
        // lever3Button = game.add.sprite(350-mobileX,390-mobileY, 'game.lever_p5');
        // lever4Button = game.add.sprite(478-mobileX,390-mobileY, 'game.lever_p7');
        // lever5Button = game.add.sprite(606-mobileX,390-mobileY, 'game.lever_p9');
        // lever1Button.inputEnabled = true;
        // lever2Button.inputEnabled = true;
        // lever3Button.inputEnabled = true;
        // lever4Button.inputEnabled = true;
        // lever5Button.inputEnabled = true;
        game.chicken = game.add.sprite(286-mobileX, 246-mobileY, 'game3.chicken');
        game.chicken.animations.add('game3.chicken', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31], 8, true).play();
        game.smoke = game.add.sprite(350-mobileX, 182-mobileY, 'game3.smoke');
        game.smoke.animations.add('game3.smoke', [0,1,2,3,4,5,6,7], 8, true).play();
        game.bear_wait = game.add.sprite(94-mobileX, 326-mobileY, 'game4.bear_wait');
        game.bear_wait.animations.add('game4.bear_wait', [0,1,2,3,3,3,3,3,2,1,4,5,6,7,8,9,8,7,8,9,8,7,6,5,4,10,11,12,13,14,15,16,17,18,19,20,21,22,23], 8, true).play();

        game.hive_wait_arr = [];
        for (var i = 1; i <= 5; ++i) {
            game.hive_wait_arr[i] = game.add.sprite(142+(i-1)*113-mobileX, 246-mobileY, 'game4.hive_wait');
            game.hive_wait_arr[i].animations.add('game4.hive_wait', [0,1,2,3,4,5,6,7], 8, true).play();
            game.hive_wait_arr[i].inputEnabled = true;
            game.add.sprite(179+(i-1)*113-mobileX, 422-mobileY, 'game4.bee_foot');
        }
        //счет
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
        addScore(game, scorePosions, bet, '', balance, betline);

        //изображения-кнопки и анимации событий нажатия

        ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша
        buttonsGame3Mobile = []; //изображения-кнопки
        game.lever_arr =[];
        winRopeNumberPosition = [[200-mobileX,274-mobileY],[200+113-mobileX,274-mobileY],[200+113+113-mobileX,274-mobileY],[200+113+113+113-mobileX,274-mobileY],[200+113+113+113+113-mobileX,274-mobileY]];
        timeoutForShowWinRopeNumber = 2500;
        timeoutGame3ToGame4 = 2000;
        typeWinRopeNumberAnim = 0;
        checkHelm = false;
        winRopeNumberSize = 30;

        buttonsGame3Mobile[0] = game.hive_wait_arr[1];
        buttonsGame3Mobile[1] = game.hive_wait_arr[2];
        buttonsGame3Mobile[2] = game.hive_wait_arr[3];
        buttonsGame3Mobile[3] = game.hive_wait_arr[4];
        buttonsGame3Mobile[4] = game.hive_wait_arr[5];

        ropesAnim[0] = function (ropeValues, ropeStep, checkHelm) {
         game.bear_wait.visible = false;

         game.bear = game.add.sprite(0+(1-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_1');
         game.bear.visible = true;
         game.bear.animations.add('game4.bear_hit_1', [0,1,2,3,4,5,6,7,8,9], 8, false).play().onComplete.add(function(){
            hit_game3.play();
            game.bear.visible = false;
            game.bear2 = game.add.sprite(0+(1-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_2');
            game.bear2.visible = true;
            game.hive_wait_arr[1].visible = false;
            if(ropeValues[ropeStep] > 0) {
                game.hive_win = game.add.sprite(158+(1-1)*113-mobileX, 246-mobileY, 'game4.hive_win');
                game.hive_win.animations.add('game4.hive_win', [0,1,2,3,4,5,6,7], 8, false).play();
            } else {
                game.hive_lose = game.add.sprite(158+(1-1)*113-mobileX, 246-mobileY, 'game4.hive_lose');
                game.hive_lose.animations.add('game4.hive_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play();
            }
            game.bear2.animations.add('game4.bear_hit_2', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
                game.bear2.visible = false;   
                if(ropeValues[ropeStep] > 0) {
                    game.bear_win = game.add.sprite(0+(1-1)*113-mobileX, 361-mobileY, 'game4.bear_win');
                    game.bear_win.animations.add('game4.bear_win', [0,1,2,3,4,5,6], 8, false).play().onComplete.add(function(){
                        game.bear_win.visible = false;
                        game.bear_wait.visible = true;
                        if(ropeStep == 5 && checkUpLvl == true) {
                            setTimeout("game.state.start('game4');", 1000);
                        } else if(ropeStep == 5) {
                            updateBalanceGame3(game, scorePosions, balanceR);
                        }
                    });
                } else {
                    game.bear_lose = game.add.sprite(0+(1-1)*113-mobileX, 361-mobileY, 'game4.bear_lose');
                    lose_game3.play();
                    game.bear_lose.animations.add('game4.bear_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                        updateBalanceGame3(game, scorePosions, balanceR);
                    });
                }
                ropeStep += 1;
            });
        });
         lockDisplay();
         setTimeout('unlockDisplay()',4000);
     };

     ropesAnim[1] = function (ropeValues, ropeStep, checkHelm) {
         game.bear_wait.visible = false;

         game.bear = game.add.sprite(0+(2-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_1');
         game.bear.visible = true;
         game.bear.animations.add('game4.bear_hit_1', [0,1,2,3,4,5,6,7,8,9], 8, false).play().onComplete.add(function(){
            hit_game3.play();
            game.bear.visible = false;
            game.bear2 = game.add.sprite(0+(2-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_2');
            game.bear2.visible = true;
            game.hive_wait_arr[2].visible = false;
            if(ropeValues[ropeStep] > 0) {
                game.hive_win = game.add.sprite(158+(2-1)*113-mobileX, 246-mobileY, 'game4.hive_win');
                game.hive_win.animations.add('game4.hive_win', [0,1,2,3,4,5,6,7], 8, false).play();
            } else {
                game.hive_lose = game.add.sprite(158+(2-1)*113-mobileX, 246-mobileY, 'game4.hive_lose');
                game.hive_lose.animations.add('game4.hive_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play();
            }
            game.bear2.animations.add('game4.bear_hit_2', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
                game.bear2.visible = false;   
                if(ropeValues[ropeStep] > 0) {
                    game.bear_win = game.add.sprite(0+(2-1)*113-mobileX, 361-mobileY, 'game4.bear_win');
                    game.bear_win.animations.add('game4.bear_win', [0,1,2,3,4,5,6], 8, false).play().onComplete.add(function(){
                        game.bear_win.visible = false;
                        game.bear_wait.visible = true;
                        if(ropeStep == 5 && checkUpLvl == true) {
                            setTimeout("game.state.start('game4');", 1000);
                        } else if(ropeStep == 5) {
                            updateBalanceGame3(game, scorePosions, balanceR);
                        }
                    });
                } else {
                    game.bear_lose = game.add.sprite(0+(2-1)*113-mobileX, 361-mobileY, 'game4.bear_lose');
                    lose_game3.play();
                    game.bear_lose.animations.add('game4.bear_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                        updateBalanceGame3(game, scorePosions, balanceR);
                    });
                }
                ropeStep += 1;
            });
        });
         lockDisplay();
         setTimeout('unlockDisplay()',4000);
     };

     ropesAnim[2] = function (ropeValues, ropeStep, checkHelm) {
       game.bear_wait.visible = false;

       game.bear = game.add.sprite(0+(3-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_1');
       game.bear.visible = true;
       game.bear.animations.add('game4.bear_hit_1', [0,1,2,3,4,5,6,7,8,9], 8, false).play().onComplete.add(function(){
        hit_game3.play();
        game.bear.visible = false;
        game.bear2 = game.add.sprite(0+(3-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_2');
        game.bear2.visible = true;
        game.hive_wait_arr[3].visible = false;
        if(ropeValues[ropeStep] > 0) {
            game.hive_win = game.add.sprite(158+(3-1)*113-mobileX, 246-mobileY, 'game4.hive_win');
            game.hive_win.animations.add('game4.hive_win', [0,1,2,3,4,5,6,7], 8, false).play();
        } else {
            game.hive_lose = game.add.sprite(158+(3-1)*113-mobileX, 246-mobileY, 'game4.hive_lose');
            game.hive_lose.animations.add('game4.hive_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play();
        }
        game.bear2.animations.add('game4.bear_hit_2', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
            game.bear2.visible = false;   
            if(ropeValues[ropeStep] > 0) {
                game.bear_win = game.add.sprite(0+(3-1)*113-mobileX, 361-mobileY, 'game4.bear_win');
                game.bear_win.animations.add('game4.bear_win', [0,1,2,3,4,5,6], 8, false).play().onComplete.add(function(){
                    game.bear_win.visible = false;
                    game.bear_wait.visible = true;
                    if(ropeStep == 5 && checkUpLvl == true) {
                        setTimeout("game.state.start('game4');", 1000);
                    } else if(ropeStep == 5) {
                        updateBalanceGame3(game, scorePosions, balanceR);
                    }
                });
            } else {
                game.bear_lose = game.add.sprite(0+(3-1)*113-mobileX, 361-mobileY, 'game4.bear_lose');
                lose_game3.play();
                game.bear_lose.animations.add('game4.bear_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                    updateBalanceGame3(game, scorePosions, balanceR);
                });
            }
            ropeStep += 1;
        });
    });
       lockDisplay();
       setTimeout('unlockDisplay()',4000);
   };

   ropesAnim[3] = function (ropeValues, ropeStep, checkHelm) {
       game.bear_wait.visible = false;

       game.bear = game.add.sprite(0+(4-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_1');
       game.bear.visible = true;
       game.bear.animations.add('game4.bear_hit_1', [0,1,2,3,4,5,6,7,8,9], 8, false).play().onComplete.add(function(){
        hit_game3.play();
        game.bear.visible = false;
        game.bear2 = game.add.sprite(0+(4-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_2');
        game.bear2.visible = true;
        game.hive_wait_arr[4].visible = false;
        if(ropeValues[ropeStep] > 0) {
            game.hive_win = game.add.sprite(158+(4-1)*113-mobileX, 246-mobileY, 'game4.hive_win');
            game.hive_win.animations.add('game4.hive_win', [0,1,2,3,4,5,6,7], 8, false).play();
        } else {
            game.hive_lose = game.add.sprite(158+(4-1)*113-mobileX, 246-mobileY, 'game4.hive_lose');
            game.hive_lose.animations.add('game4.hive_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play();
        }
        game.bear2.animations.add('game4.bear_hit_2', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
            game.bear2.visible = false;   
            if(ropeValues[ropeStep] > 0) {
                game.bear_win = game.add.sprite(0+(4-1)*113-mobileX, 361-mobileY, 'game4.bear_win');
                game.bear_win.animations.add('game4.bear_win', [0,1,2,3,4,5,6], 8, false).play().onComplete.add(function(){
                    game.bear_win.visible = false;
                    game.bear_wait.visible = true;
                    if(ropeStep == 5 && checkUpLvl == true) {
                        setTimeout("game.state.start('game4');", 1000);
                    } else if(ropeStep == 5) {
                        updateBalanceGame3(game, scorePosions, balanceR);
                    }
                });
            } else {
                game.bear_lose = game.add.sprite(0+(4-1)*113-mobileX, 361-mobileY, 'game4.bear_lose');
                lose_game3.play();
                game.bear_lose.animations.add('game4.bear_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                    updateBalanceGame3(game, scorePosions, balanceR);
                });
            }
            ropeStep += 1;
        });
    });
       lockDisplay();
       setTimeout('unlockDisplay()',4000);
   };

   ropesAnim[4] = function (ropeValues, ropeStep, checkHelm) {
     game.bear_wait.visible = false;

     game.bear = game.add.sprite(0+(5-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_1');
     game.bear.visible = true;
     game.bear.animations.add('game4.bear_hit_1', [0,1,2,3,4,5,6,7,8,9], 8, false).play().onComplete.add(function(){
        hit_game3.play();
        game.bear.visible = false;
        game.bear2 = game.add.sprite(0+(5-1)*113-mobileX, 361-mobileY, 'game4.bear_hit_2');
        game.bear2.visible = true;
        game.hive_wait_arr[5].visible = false;
        if(ropeValues[ropeStep] > 0) {
            game.hive_win = game.add.sprite(158+(5-1)*113-mobileX, 246-mobileY, 'game4.hive_win');
            game.hive_win.animations.add('game4.hive_win', [0,1,2,3,4,5,6,7], 8, false).play();
        } else {
            game.hive_lose = game.add.sprite(158+(5-1)*113-mobileX, 246-mobileY, 'game4.hive_lose');
            game.hive_lose.animations.add('game4.hive_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play();
        }
        game.bear2.animations.add('game4.bear_hit_2', [0,1,2,3,4,5], 8, false).play().onComplete.add(function(){
            game.bear2.visible = false;   
            if(ropeValues[ropeStep] > 0) {
                game.bear_win = game.add.sprite(0+(5-1)*113-mobileX, 361-mobileY, 'game4.bear_win');
                game.bear_win.animations.add('game4.bear_win', [0,1,2,3,4,5,6], 8, false).play().onComplete.add(function(){
                    game.bear_win.visible = false;
                    game.bear_wait.visible = true;
                    if(ropeStep == 5 && checkUpLvl == true) {
                        setTimeout("game.state.start('game4');", 1000);
                    } else if(ropeStep == 5) {
                        updateBalanceGame3(game, scorePosions, balanceR);
                    }
                });
            } else {
                game.bear_lose = game.add.sprite(0+(5-1)*113-mobileX, 361-mobileY, 'game4.bear_lose');
                lose_game3.play();
                game.bear_lose.animations.add('game4.bear_lose', [0,1,2,3,4,5,6,7,8,9,10], 8, false).play().onComplete.add(function(){
                    updateBalanceGame3(game, scorePosions, balanceR);
                });
            }
            ropeStep += 1;
        });
    });
     lockDisplay();
     setTimeout('unlockDisplay()',4000);
 };
        //анимации

        addButtonsGame3Mobile(game, ropesAnim, buttonsGame3Mobile, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);

    };

    game3.update = function () {};

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================
/*ropeValues = [1,2,3,4,5,1];*/
(function () {

    var button;

    var game4 = {};

    game4.preload = function () {};

    game4.create = function () {
        //звуки
        open_game4 = game.add.audio('open_game4');
        win_game4 = game.add.audio('win_game4');
        number_win = game.add.audio('number_win');
        //изображения
        //Добавление фона
        background = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame4');
        backgroundGame4 = game.add.sprite(95-mobileX,55-mobileY, 'game.backgroundGame4');



        //счет
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
        addScore(game, scorePosions, bet, '', balance, betline);


        //логика и анимации
        game.btn_left_mob = game.add.sprite(155, 310, 'game4.btn_left_mob');
        game.btn_left_mob.inputEnabled = true;
        game.btn_right_mob = game.add.sprite(374, 310, 'game4.btn_right_mob');
        game.btn_right_mob.inputEnabled = true;
        game.chicken = game.add.sprite(286-mobileX, 246-mobileY, 'game3.chicken');
        game.chicken.animations.add('game3.chicken', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31], 8, true).play();
        game.smoke = game.add.sprite(350-mobileX, 182, 'game3.smoke');
        game.smoke.animations.add('game3.smoke', [0,1,2,3,4,5,6,7], 8, true).play();   
        game.bear_wait_1 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_wait_1');
        game.bear_wait_1Anim = game.bear_wait_1.animations.add('game3.bear_wait_1', [0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,4,5,6,7,4,5,6,7,8,9,10,11], 8, false).play();
        game.bear_wait_2 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_wait_2');
        game.bear_wait_2.visible = false;
        game.bear_wait_2Anim = game.bear_wait_2.animations.add('game3.bear_wait_2', [0,1,2,3,4,5,6,7,8,9], 8, false)
        game.bear_wait_1Anim.onComplete.add(function(){
            game.bear_wait_1.visible = false;
            game.bear_wait_2.visible = true;
            game.bear_wait_2Anim.play();
        });
        game.bear_wait_2Anim.onComplete.add(function(){
            game.bear_wait_2.visible = false;
            game.bear_wait_1.visible = true;
            game.bear_wait_1Anim.play();
        }); 

        //изображения-кнопки и анимации событий нажатия

        selectionsAmin = [];
        buttonsGame4Mobile = []; //изображения-кнопки
        timeout1Game4ToGame1 = 4500;
        timeout2Game4ToGame1 = 4500;


        buttonsGame4Mobile[0] = game.btn_left_mob;
        buttonsGame4Mobile[1] = game.btn_right_mob;

        game.bear_right_honey_0 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_right_empty_1');
        game.bear_right_honey_0 .visible = false;
        game.bear_right_honey_0Anim = game.bear_right_honey_0.animations.add('game3.bear_right_empty_1', [0,1,2,3,4,5,6], 8, false);  
        game.bear_right_honey_1 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_right_honey_1');
        game.bear_right_honey_1 .visible = false;
        game.bear_right_honey_1Anim = game.bear_right_honey_1.animations.add('game3.bear_right_honey_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
        game.bear_right_honey_2 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_right_honey_2');
        game.bear_right_honey_2 .visible = false;
        game.bear_right_honey_2Anim = game.bear_right_honey_2.animations.add('game3.bear_right_honey_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
        game.bear_right_honey_3 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_right_honey_3');
        game.bear_right_honey_3 .visible = false;
        game.bear_right_honey_3Anim = game.bear_right_honey_3.animations.add('game3.bear_right_honey_3', [0,1,1,1,1,1,1], 8, false);
        game.bear_right_honey_0Anim.onComplete.add(function(){
            game.bear_right_honey_0.visible = false;
            game.bear_right_honey_1.visible = true;
            game.bear_right_honey_1Anim.play();
        });   
        game.bear_right_honey_1Anim.onComplete.add(function(){
            game.bear_right_honey_1.visible = false;
            game.bear_right_honey_2.visible = true;
            game.bear_right_honey_2Anim.play();
            win_game4.play();
        });
        game.bear_right_honey_2Anim.onComplete.add(function(){
            game.bear_right_honey_2.visible = false;
            game.bear_right_honey_3.visible = true;
            game.bear_right_honey_3Anim.play();
            number_win.play();
        }); 
        game.bear_right_honey_3Anim.onComplete.add(function(){
         updateBalanceGame4(game, scorePosions, balanceR);
     });
        game.bear_left_honey_0 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_left_empty_1');
        game.bear_left_honey_0 .visible = false;
        game.bear_left_honey_0Anim = game.bear_left_honey_0.animations.add('game3.bear_left_empty_1', [0,1,2,3,4,5,6], 8, false);  
        game.bear_left_honey_1 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_left_honey_1');
        game.bear_left_honey_1 .visible = false;
        game.bear_left_honey_1Anim = game.bear_left_honey_1.animations.add('game3.bear_left_honey_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
        game.bear_left_honey_2 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_left_honey_2');
        game.bear_left_honey_2 .visible = false;
        game.bear_left_honey_2Anim = game.bear_left_honey_2.animations.add('game3.bear_left_honey_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
        game.bear_left_honey_3 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_left_honey_3');
        game.bear_left_honey_3 .visible = false;
        game.bear_left_honey_3Anim = game.bear_left_honey_3.animations.add('game3.bear_left_honey_3', [0,1,1,1,1,1,1], 8, false);
        game.bear_left_honey_0Anim.onComplete.add(function(){
            game.bear_left_honey_0.visible = false;
            game.bear_left_honey_1.visible = true;
            game.bear_left_honey_1Anim.play();
        });   
        game.bear_left_honey_1Anim.onComplete.add(function(){
            game.bear_left_honey_1.visible = false;
            game.bear_left_honey_2.visible = true;
            game.bear_left_honey_2Anim.play();
            win_game4.play();
        });
        game.bear_left_honey_2Anim.onComplete.add(function(){
            game.bear_left_honey_2.visible = false;
            game.bear_left_honey_3.visible = true;
            game.bear_left_honey_3Anim.play();
            number_win.play();
        }); 

        game.bear_right_honey_3Anim.onComplete.add(function(){
         updateBalanceGame4(game, scorePosions, balanceR);
     });
        game.bear_left_empty_1 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_left_empty_1');
        game.bear_left_empty_1 .visible = false;
        game.bear_left_empty_1Anim = game.bear_left_empty_1.animations.add('game3.bear_left_empty_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
        game.bear_left_empty_2 = game.add.sprite(254-mobileX, 246-mobileY, 'game3.bear_left_empty_2');
        game.bear_left_empty_2 .visible = false;
        game.bear_left_empty_2Anim = game.bear_left_empty_2.animations.add('game3.bear_left_empty_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
        game.bear_left_empty_3 = game.add.sprite(254-mobileX, 246-mobileY, 'game3.bear_left_empty_3');
        game.bear_left_empty_3 .visible = false;
        game.bear_left_empty_3Anim = game.bear_left_empty_3.animations.add('game3.bear_left_empty_3', [0,1,2,3,4,5,6,6,6,6], 8, false);
        game.bear_left_empty_1Anim.onComplete.add(function(){
            game.bear_left_empty_1.visible = false;
            game.bear_left_empty_2.visible = true;
            game.bear_left_empty_2Anim.play();
        });
        game.bear_left_empty_2Anim.onComplete.add(function(){
            game.bear_left_empty_2.visible = false;
            game.bear_left_empty_3.visible = true;
            game.bear_left_empty_3Anim.play();
        });     
        game.bear_left_empty_3Anim.onComplete.add(function(){
         updateBalanceGame4(game, scorePosions, balanceR);
     });
        game.bear_right_empty_1 = game.add.sprite(254-mobileX, 262-mobileY, 'game3.bear_right_empty_1');
        game.bear_right_empty_1 .visible = false;
        game.bear_right_empty_1Anim = game.bear_right_empty_1.animations.add('game3.bear_right_empty_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
        game.bear_right_empty_2 = game.add.sprite(254-mobileX, 246-mobileY, 'game3.bear_right_empty_2');
        game.bear_right_empty_2 .visible = false;
        game.bear_right_empty_2Anim = game.bear_right_empty_2.animations.add('game3.bear_right_empty_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
        game.bear_right_empty_3 = game.add.sprite(254-mobileX, 246-mobileY, 'game3.bear_right_empty_3');
        game.bear_right_empty_3 .visible = false;
        game.bear_right_empty_3Anim = game.bear_right_empty_3.animations.add('game3.bear_right_empty_3', [0,1,2,3,4,5,6,6,6,6], 8, false);
        game.bear_right_empty_1Anim.onComplete.add(function(){
            game.bear_right_empty_1.visible = false;
            game.bear_right_empty_2.visible = true;
            game.bear_right_empty_2Anim.play();
        });
        game.bear_right_empty_2Anim.onComplete.add(function(){
            game.bear_right_empty_2.visible = false;
            game.bear_right_empty_3.visible = true;
            game.bear_right_empty_3Anim.play();
        });     
        game.bear_right_empty_3Anim.onComplete.add(function(){
          updateBalanceGame4(game, scorePosions, balanceR);
      });

        selectionsAmin[0] = function (ropeValues, ropeStep) {
            game.bear_wait_1.visible = false;
            game.bear_wait_2.visible = false;

            game.bear_wait_1Anim.stop();
            game.bear_wait_2Anim.stop();
            if(ropeValues[5] != 0){
             game.bear_left_honey_0Anim.play();
             game.bear_left_honey_0.visible = true;
             setTimeout(function() {                    
                open_game4.play();
            }, 900);
         } else {
            game.bear_left_empty_1Anim.play();
            game.bear_left_empty_1.visible = true;
            setTimeout(function() {                    
                open_game4.play();
            }, 900);
        }
    };

    selectionsAmin[1] = function (ropeValues, ropeStep) {
        game.bear_wait_1.visible = false;
        game.bear_wait_2.visible = false;

        game.bear_wait_1Anim.stop();
        game.bear_wait_2Anim.stop();
        if(ropeValues[5] != 0){
            game.bear_right_honey_0Anim.play();
            game.bear_right_honey_0.visible = true;
            setTimeout(function() {                    
                open_game4.play();
            }, 900);
        } else {
            game.bear_right_empty_1Anim.play();
            game.bear_right_empty_1.visible = true;  
            setTimeout(function() {                    
                open_game4.play();
            }, 900);
        }
    };

    addButtonsGame4Mobile(game, selectionsAmin, buttonsGame4Mobile, timeout1Game4ToGame1, timeout2Game4ToGame1)

};

game4.update = function () {
};

game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('game.background', 'img/shape1206.svg');
        game.load.image('game.background1', 'img/main_bg.png');

        game.load.image('startButton', 'img/mobileButtons/spin.png');
        game.load.image('startButton_p', 'img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', 'img/mobileButtons/spin_d.png');
        game.load.image('bet1', 'img/mobileButtons/bet1.png');
        game.load.image('bet1_p', 'img/mobileButtons/bet1_p.png');
        game.load.image('home', 'img/mobileButtons/home.png');
        game.load.image('home_p', 'img/mobileButtons/home_p.png');
        game.load.image('dollar', 'img/mobileButtons/dollar.png');
        game.load.image('gear', 'img/mobileButtons/gear.png');
        game.load.image('double', 'img/mobileButtons/double.png');
        game.load.image('collect', 'img/mobileButtons/collect.png');
        game.load.image('collect_p', 'img/mobileButtons/collect_p.png');
        game.load.image('collect_d', 'img/mobileButtons/collect_d.png');

        game.load.image('game.number1', 'img/win_1.png');
        game.load.image('game.number2', 'img/win_2.png');
        game.load.image('game.number3', 'img/win_3.png');
        game.load.image('game.number4', 'img/win_4.png');
        game.load.image('game.number5', 'img/win_5.png');
        game.load.image('game.number6', 'img/win_6.png');
        game.load.image('game.number7', 'img/win_7.png');
        game.load.image('game.number8', 'img/win_8.png');
        game.load.image('game.number9', 'img/win_9.png');

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.audio('sound', 'sounds/spin.mp3');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('play', 'sounds/play.mp3');

        game.load.image('line1', 'img/lines/select/line1.png');
        game.load.image('line2', 'img/lines/select/line2.png');
        game.load.image('line3', 'img/lines/select/line3.png');
        game.load.image('line4', 'img/lines/select/line4.png');
        game.load.image('line5', 'img/lines/select/line5.png');
        game.load.image('line6', 'img/lines/select/line6.png');
        game.load.image('line7', 'img/lines/select/line7.png');
        game.load.image('line8', 'img/lines/select/line8.png');
        game.load.image('line9', 'img/lines/select/line9.png');

        game.load.image('linefull1', 'img/lines/win/linefull1.png');
        game.load.image('linefull2', 'img/lines/win/linefull2.png');
        game.load.image('linefull3', 'img/lines/win/linefull3.png');
        game.load.image('linefull4', 'img/lines/win/linefull4.png');
        game.load.image('linefull5', 'img/lines/win/linefull5.png');
        game.load.image('linefull6', 'img/lines/win/linefull6.png');
        game.load.image('linefull7', 'img/lines/win/linefull7.png');
        game.load.image('linefull8', 'img/lines/win/linefull8.png');
        game.load.image('linefull9', 'img/lines/win/linefull9.png');

        game.load.audio('line1Sound', 'sounds/line1.wav');
        game.load.audio('line3Sound', 'sounds/line3.wav');
        game.load.audio('line5Sound', 'sounds/line5.wav');
        game.load.audio('line7Sound', 'sounds/line7.wav');
        game.load.audio('line9Sound', 'sounds/line9.wav');

        game.load.audio('sound', 'sounds/spin.mp3');
        game.load.audio('rotateSound', 'sounds/rotate.wav');
        game.load.audio('stopSound', 'sounds/stop.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('play', 'sounds/play.mp3');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('page', 'sounds/page.mp3');

        game.load.audio('hit_game3', 'sounds/hit_game3.mp3');
        game.load.audio('lose_game3', 'sounds/lose_game3.mp3');
        game.load.audio('number_win', 'sounds/number_win.mp3');
        game.load.audio('open_game4', 'sounds/open_game4.mp3');
        game.load.audio('win_game4', 'sounds/win_game4.mp3');

        game.load.audio('soundWinLine8', 'sounds/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', 'sounds/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', 'sounds/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', 'sounds/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', 'sounds/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', 'sounds/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', 'sounds/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', 'sounds/winLines/sound19.mp3');

        game.load.audio('bonusWin', 'sounds/bonusWin.mp3');


        game.load.image('game.backgroundGame2', 'img/bg_game2.png');
        game.load.audio('openCard', 'sounds/sound31.mp3');
        game.load.audio('winCard', 'sounds/sound30.mp3');

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');
        for (var i = 1; i <= 5; ++i) {
            game.load.image('pagePaytable_' + i, 'img/page_' + i + '.png');            
        }

        game.load.spritesheet('bee', 'img/bee_144x112_8.png', 144, 112, 8);
        game.load.spritesheet('bear', 'img/bear_176x112x7.png', 176, 112, 7);
        game.load.spritesheet('game.win1', 'img/bear_win_176x112x19.png', 176, 112, 19);
        game.load.spritesheet('ears', 'img/ears_32х32_3.png', 32, 32, 3);

        game.load.audio('hithit_game2', 'sounds/hithit_game2.mp3');
        game.load.audio('final_win_game2', 'sounds/final_win_game2.mp3');
        game.load.audio('win_first_game2', 'sounds/win_first_game2.mp3');
        game.load.audio('lose_game2', 'sounds/lose_game2.mp3');
        game.load.spritesheet('bear_hit_left', 'img/bear_hit_left_272x256_7.png', 272, 256, 7);
        game.load.spritesheet('bear_hit_right', 'img/bear_hit_right_272x256_7.png', 272, 256, 7);
        game.load.spritesheet('bear_lose_down', 'img/bear_lose_down_272x256_4_3.png', 272, 256, 7);
        game.load.spritesheet('bear_lose_left_1', 'img/bear_lose_left_272x256_14.png', 272, 256, 14);
        game.load.spritesheet('bear_lose_left_2', 'img/bear_lose_left_272x256_3.png', 272, 256, 3);
        game.load.spritesheet('bear_lose_right_1', 'img/bear_lose_right_272x256_14.png', 272, 256, 14);
        game.load.spritesheet('bear_lose_right_2', 'img/bear_lose_right_272x256_2.png', 272, 256, 2);
        game.load.spritesheet('game2.bear_wait', 'img/bear_wait_208x256_6.png', 208, 256, 6);
        game.load.spritesheet('bear_win_left_1', 'img/bear_win_left_1_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_left_2', 'img/bear_win_left_2_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_right_1', 'img/bear_win_right_1_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_right_2', 'img/bear_win_right_2_272x256_11.png', 272, 256, 11);
        game.load.spritesheet('bear_win_top', 'img/bear_win_top_272x256_7.png', 272, 256, 7);
        game.load.spritesheet('bee_lose_1', 'img/bee_lose_1_80x64_5.png', 80, 64, 5);
        game.load.spritesheet('bee_lose_2', 'img/bee_lose_2_80x64_7.png', 80, 64, 7);
        game.load.spritesheet('beehive_wait', 'img/beehive_wait_80x64_8.png', 80, 64, 8);
        game.load.spritesheet('leaves_1', 'img/leaves_1_272x128_6.png', 272, 128, 6);
        game.load.spritesheet('leaves_2', 'img/leaves_2_272x128_13.png', 272, 128, 13);
        game.load.image('cloud_1', 'img/cloud_1.png');
        game.load.image('cloud_2', 'img/cloud_2.png');
        game.load.image('game.double', 'img/game.double.png');
        game.load.image('tree_1', 'img/tree_1.png');
        game.load.image('tree_2', 'img/tree_3.png');

        game.load.spritesheet('game4.bear_wait', 'img/game4.bear_wait_96x176_24.png', 96, 176, 24);
        game.load.spritesheet('game4.hive_wait', 'img/hive_wait_128x256_8.png', 128, 256, 8);
        game.load.spritesheet('game4.hive_win', 'img/hive_win_112x176_8.png', 112, 176, 8);
        game.load.spritesheet('game4.hive_lose', 'img/hive_lose_112x176_11.png', 112, 176, 11);
        game.load.spritesheet('game4.bear_hit_1', 'img/bear_hit_256x144_10.png', 256, 144, 10);
        game.load.spritesheet('game4.bear_hit_2', 'img/bear_hit_288x144_6.png', 288, 144, 6);
        game.load.spritesheet('game4.bear_lose', 'img/bear_lose_256x144_11.png', 256, 144, 11);
        game.load.spritesheet('game4.bear_win', 'img/bear_win_256x144_7.png', 256, 144, 7);
        game.load.image('game4.bee_foot', 'img/bee_foot.png');
        game.load.image('game.backgroundGame3', 'img/bg_game4.png');
        game.load.image('game.backgroundGame4', 'img/game3_bg.png');
        game.load.image('game4.btn_left_mob', 'img/game4_btn_left_mob.png');
        game.load.image('game4.btn_right_mob', 'img/game4_btn_right_mob.png');
        
        game.load.spritesheet('game3.chicken', 'img/chicken_32x32_32.png', 32, 32, 32);
        game.load.spritesheet('game3.smoke', 'img/smoke_32x64_8.png', 32, 64, 8);
        game.load.spritesheet('game3.bear_left_empty_1', 'img/game3_bear_left_empty_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_left_empty_2', 'img/game3_bear_left_empty_2_320x224_12.png', 320, 224, 12);
        game.load.spritesheet('game3.bear_left_empty_3', 'img/game3_bear_left_empty_3_320x224_7.png', 320, 224, 7);
        game.load.spritesheet('game3.bear_left_honey_1', 'img/game3_bear_left_honey_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_left_honey_2', 'img/game3_bear_left_honey_2_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_left_honey_3', 'img/game3_bear_left_honey_3_320x208_2.png', 320, 208, 2);
        game.load.spritesheet('game3.bear_right_empty_1', 'img/game3_bear_right_empty_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_right_empty_2', 'img/game3_bear_right_empty_2_320x224_12.png', 320, 224, 12);
        game.load.spritesheet('game3.bear_right_empty_3', 'img/game3_bear_right_empty_3_320x224_7.png', 320, 224, 7);
        game.load.spritesheet('game3.bear_right_honey_1', 'img/game3_bear_right_honey_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_right_honey_2', 'img/game3_bear_right_honey_2_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_right_honey_3', 'img/game3_bear_right_honey_3_320x208_2.png', 320, 208, 2);
        game.load.spritesheet('game3.bear_wait_1', 'img/game3_bear_wait_1_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_wait_2', 'img/game3_bear_wait_2_320x208_12.png', 320, 208, 12);
        game.load.spritesheet('game3.bear_wait_3', 'img/game3_bear_wait_3_320x208_3.png', 320, 208, 3);

        game.load.image('cell0', 'img/0.png');
        game.load.image('cell1', 'img/1.png');
        game.load.image('cell2', 'img/2.png');
        game.load.image('cell3', 'img/3.png');
        game.load.image('cell4', 'img/4.png');
        game.load.image('cell5', 'img/5.png');
        game.load.image('cell6', 'img/6.png');
        game.load.image('cell7', 'img/7.png');
        game.load.image('cell8', 'img/8.png');

        game.load.spritesheet('cellAnim', 'img/cellAnim.png', 96, 112);

        game.load.image('bonusGame', 'img/image536.jpg');
        game.load.image('wildSymbol', 'img/image537.jpg');
        game.load.image('play1To', 'img/image546.png');
        game.load.image('takeOrRisk1', 'img/image474.png');
        game.load.image('takeOrRisk2', 'img/image475.png');
        game.load.image('take', 'img/image554.png');

        game.load.image('topScoreGame1', 'img/main_bg_1.png');
        game.load.image('topScoreGame2', 'img/main_bg_2.png');
        game.load.image('topScoreGame3', 'img/main_bg_3.png');
        game.load.image('topScoreGame4', 'img/main_bg_4.png');

        game.load.image('loseTitleGame2', 'img/lose.png');
        game.load.image('winTitleGame2', 'img/win.png');
        game.load.image('forwadTitleGame2', 'img/forward.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/bonus.png', 96, 112);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

