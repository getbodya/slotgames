function game3() {
    (function () {

        var game3 = {};

        game3.preload = function () {};

        game3.create = function () {

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            ropeStep = 0;
            checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса
            
            //звуки
            preOpenWinCover = game.add.sound('game.preOpenWinCover');
            openWinCover = game.add.sound('game.openWinCover');
            
            
            //изображения
            background = game.add.sprite(0,0, 'game.background');
            background2 = game.add.sprite(95,54, 'game.backgroundGame4');
            backgroundTotal = game.add.sprite(95,23, 'game.backgroundTotal');

            //счет
            scorePosions = [[260, 26, 20], [435, 26, 20], [610, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);



            //кнопки и анимации событий нажатия

            ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша   
            
            winRopeNumberPosition = [[195,180],[195+110,180],[195+220,180],[195+330,180],[195+440,180]];
            timeoutForShowWinRopeNumber = 500;
            timeoutGame3ToGame4 = 2000;
            typeWinRopeNumberAnim = 0;
            checkHelm = false;
            winRopeNumberSize = 22;
            

            openCaver1 = game.add.sprite(145,148, 'game.openCaver');
            openCaver1.animations.add('openCaver1', [0,1,2,3,4,5,6], 30, false);

            openCaver2 = game.add.sprite(255,148, 'game.openCaver');
            openCaver2.animations.add('openCaver2', [0,1,2,3,4,5,6], 30, false);

            openCaver3 = game.add.sprite(365,148, 'game.openCaver');
            openCaver3.animations.add('openCaver3', [0,1,2,3,4,5,6], 30, false);

            openCaver4 = game.add.sprite(475,148, 'game.openCaver');
            openCaver4.animations.add('openCaver4', [0,1,2,3,4,5,6], 30, false);

            openCaver5 = game.add.sprite(585,148, 'game.openCaver');
            openCaver5.animations.add('openCaver5', [0,1,2,3,4,5,6], 30, false);

            //добавляем в ropesAnim анимации
            //анимация для кнопки 1
            ropesAnim[0] = function (ropeValues, ropeStep, checkHelm) {
                preOpenWinCover.play();

                openCaver1.animations.getAnimation('openCaver1').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver1.visible = false;
                    if(ropeValues[ropeStep] > 0) {
                        totalBet1 = game.add.sprite(145,148, 'game.totalBet');
                    } else {
                        totalBet1 = game.add.sprite(145,148, 'game.totalExit');
                    }
                });
                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            };

            //анимация для кнопки 3
            ropesAnim[1] = function (ropeValues, ropeStep, checkHelm) {
                preOpenWinCover.play();

                openCaver2.animations.getAnimation('openCaver2').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver2.visible = false;
                    if(ropeValues[ropeStep] > 0) {
                        totalBet2 = game.add.sprite(255,148, 'game.totalBet');
                    } else {
                        totalBet2 = game.add.sprite(255,148, 'game.totalExit');
                    }
                });

                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            };

            //анимация для кнопки 5
            ropesAnim[2] = function (ropeValues, ropeStep, checkHelm) {
                preOpenWinCover.play();

                openCaver3.animations.getAnimation('openCaver3').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver3.visible = false;
                    if(ropeValues[ropeStep] > 0) {
                        totalBet3 = game.add.sprite(365,148, 'game.totalBet');
                    } else {
                        totalBet3 = game.add.sprite(365,148, 'game.totalExit');
                    }
                });

                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            };

            //анимация для кнопки 7
            ropesAnim[3] = function (ropeValues, ropeStep, checkHelm) {
                preOpenWinCover.play();

                openCaver4.animations.getAnimation('openCaver4').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver4.visible = false;
                    if(ropeValues[ropeStep] > 0) {
                        totalBet4 = game.add.sprite(475,148, 'game.totalBet');
                    } else {
                        totalBet4 = game.add.sprite(475,148, 'game.totalExit');
                    }
                });

                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            };

            //анимация для кнопки 9
            ropesAnim[4] = function (ropeValues, ropeStep, checkHelm) {
                preOpenWinCover.play();

                openCaver5.animations.getAnimation('openCaver5').play().onComplete.add(function () {
                    openCaver5.visible = false;
                    if(ropeValues[ropeStep] > 0) {
                        totalBet5 = game.add.sprite(585,148, 'game.totalBet');
                    } else {
                        totalBet5 = game.add.sprite(585,148, 'game.totalExit');
                    }
                });

                lockDisplay();
                setTimeout('unlockDisplay();', 1000);
            };

            addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);


            //анимации

            gramofon4 = game.add.sprite(415,406, 'game.gramofon4');
            gramofon4.animations.add('gramofon4', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 4, true);
            gramofon4.animations.getAnimation('gramofon4').play();
            //gramofonR2 = game.add.sprite(530,406, 'game.gramofonR2');

            barmen_part41 = game.add.sprite(94,390, 'game.barmen_part41');
            barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
            barmen_part41.animations.getAnimation('barmen_part41').play();

            barmenR2 = game.add.sprite(400,394, 'game.barmenR2');


            full_and_sound();

        };

        game3.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game3', game3);

})();

}