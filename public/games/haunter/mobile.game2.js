function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        backgroundGame3 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame3');
        backgroundTotal = game.add.sprite(95-mobileX,23-mobileY, 'game.backgroundTotal');

        addTableTitle(game, 'winTitleGame2', 540-mobileX,432-mobileY);
        hideTableTitle();



        //счет
        step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [475-mobileX, 84-mobileY, 30]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[113+23-mobileX,129-mobileY], [263-mobileX,129-mobileY], [375-mobileX,129-mobileY], [490-mobileX,129-mobileY], [602-mobileX,129-mobileY]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация

        barmen_part41 = game.add.sprite(94-mobileX,390-mobileY, 'game.barmen_part41');
        barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
        barmen_part41.animations.getAnimation('barmen_part41').play();

        grammofon3 = game.add.sprite(414-mobileX,406-mobileY, 'game.grammofon3');
        grammofon3.animations.add('grammofon3', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 4, true);
        grammofon3.animations.getAnimation('grammofon3').play();

        fontsize = 18;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 547-mobileX, 445-mobileY, fontsize);

        // кнопки
        addButtonsGame2Mobile(game);
    };

    game2.update = function () {

    };

    game.state.add('game2', game2);

    // звуки
}