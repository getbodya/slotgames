<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pirate 2</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" href="style.css" />
	<link rel="stylesheet" href="main.css" />	
	<link rel="stylesheet" href="preloader/main.css" />
	<link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-3.1.1.js"></script>	
	<script src="phaser.min.js"></script>
	<script src="detect.js"></script>
	<script src="main.js"></script>
	<script type="text/javascript" src="js/jquery.maskedinput.js"></script>	
	<script type="text/javascript" src="js/main.js"></script>	
</head>
<body>
	<script>
		<?php
		include_once 'functions.js';
		include_once 'desktop.begin.js';
		include_once 'desktop.game1.js';
		include_once 'desktop.game2.js';
		include_once 'desktop.game3.js';
		include_once 'desktop.game4.js';
		include_once 'desktop.end.js';
		?>
	</script>
	<header>
		<div class="content">
			<a class="logo"  href="/" >
				<b>fraer</b>casino
			</a>
			<div class="right-block">
				<div class="account-btn">
					<a href="" class="btn btn-login">Login</a>
					<a href="" class="btn btn-reg">Register Now</a>
				</div>
			</div>
		</div>
	</header>
	<nav class="main-nav">
		<div class="main-nav-wrap">		
			<ul class="content">
				<li>
					<a href="" class="active"><span class="icon fa fa-star"></span>Favourites</a>
				</li>
				<li>
					<a href=""><span class="icon fa fa-star"></span>Slots</a>
				</li>
				<li>
					<a href=""><span class="icon fa fa-star"></span>Jackpots</a>
				</li>
				<li>
					<a href=""><span class="icon fa fa-star"></span>Video poker</a>
				</li>
				<li>
					<a href=""><span class="icon fa fa-star"></span>Table games</a>
				</li>
				<li>
					<a href=""><span class="icon fa fa-star"></span>Live casino</a>
				</li>
				<li>
					<a href=""><span class="icon fa fa-star"></span>A-Z</a>
				</li>
				<li>
					<a href=""><span class="icon fa fa-star"></span>Propotions</a>
				</li>
				<li class="visible-desktop">
					<a href=""><span class="icon fa fa-star"></span>Search</a>
				</li>
			</ul>
		</div>
	</nav>
	<main>
		<div class="btn_games content" style="text-align: center; margin-top: 20px;">
			<button class="btn" OnClick="gameNumber = 3;">1-ая локация</button>
			<button class="btn" OnClick="gameNumber = 4;" disabled style="cursor: default;">2-ая локация и 3-ая локация</button>
			<!-- <button class="btn" OnClick="gameNumber = 5;">3-ая локация</button> -->
		</div>
		<section class="games-wrap content">
			<div class="emulat-games" id="game-area">	
				<div id="displayLock"></div>	
				<div class="preloader" id="preloader">
					<div class="animation-wrap">
						<div class="number-wrap">
							<span id="percent-preload">0</span>
							<span>%</span>
						</div>
						<img src="img/Animation.gif">
					</div>
				</div>		
			</div>
		</section>
	</main>
	<footer>		
		<div class="footer-payments">
			<div class="content">		
				<div class="payment-logo-wrap">
					<img src="https://bps-cdn-level3.bpsgameserver.com/bgr/CasinoEuro/Common/neutral/image/2016/03/d1250265eaed402abe7599c40ba42f0c.png">
				</div>
				<div class="payment-logo-wrap">
					<img src="https://bps-cdn-level3.bpsgameserver.com/bgr/CasinoEuro/Common/neutral/image/2016/03/3e80a5d7a9d54088a0b8f5d4a028ad7a.png">
				</div>
				<div class="payment-logo-wrap">
					<img src="https://bps-cdn-level3.bpsgameserver.com/bgr/CasinoEuro/Common/neutral/image/2016/03/8feb0d01b5f446549561b8499d89857c.png">
				</div>
				<div class="payment-logo-wrap">
					<img src="https://bps-cdn-level3.bpsgameserver.com/bgr/CasinoEuro/Common/neutral/image/2016/03/52381be1b462416a8bd61903a8277f95.png">
				</div>
			</div>
		</div>
		<div class="footer-games-logo">
			<div class="content">		
				<div class="games-logo-wrap">
					<img src="http://img0.liveinternet.ru/images/attach/c/10/127/538/127538124_igrosoft9b398abc99.png">
				</div>
				<div class="games-logo-wrap">
					<img src="https://bps-cdn-level3.bpsgameserver.com/bgr/CasinoEuro/Common/neutral/image/2016/03/d03a6e868c3f46a0b3f088c3b564a41e.png">
				</div>
				<div class="games-logo-wrap">
					<img src="http://www.slotspill.com/wp-content/uploads/default/novomatic-casino.png">
				</div>
				<div class="games-logo-wrap">
					<img src="https://bps-cdn-level3.bpsgameserver.com/bgr/CasinoEuro/Common/neutral/image/2016/03/b1a3d83a49124751a022cb32c0596120.png">
				</div>
			</div>
		</div>
	</footer>
</body>
</html>