<?php
//Test iteration count. Количество иттераций теста. Чем больше тем точнее
$iterations = 100000;

// predvaritelnye statusi
$mesage = "succes";
$lastbal = 'No';

// проверка входящих данных
if (!isset($_GET['balls_selected']) or !isset($_GET['betValue'])) {
    $data = json_encode(array(
        "mesage" => "error with get data"
    ));
    echo $data;
    exit();
}

// получаем шары которые выбрал игрок
$user_numbers = array_map('intval', explode(',', $_GET["balls_selected"]));
$sum_bet = floatval($_GET["betValue"]);
$picked = count($user_numbers);

$winSum = 0;

$balance = $iterations;
echo 'Start balance: ' . $balance . '<br/>';
$sum_bet = 1;
for ($x = 0; $x < $iterations; $x++) {
    $balance = $balance - $sum_bet;
    $status = "lose";
    // Тираж
    $server_numbers = getNumbers(20);
    // Проверка на совпадение
    $result = array_intersect($server_numbers, $user_numbers);
    // Генерация ответа
    $response =
        [
            "server_numbers" => $server_numbers,
            "user_numbers" => $user_numbers,
            "result" => $result
        ];

    $count = count($response['result']);

    $win_sum = 0;

    switch ($picked) {
        case 2:
            if ($count == 2) {
                $win_sum = $sum_bet * 11;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 3:
            if ($count == 3) {
                $win_sum = $sum_bet * 28;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 2;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 4:
            if ($count == 4) {
                $win_sum = $sum_bet * 80;
            } elseif ($count == 3) {
                $win_sum = $sum_bet * 5;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 1;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 5:
            if ($count == 5) {
                $win_sum = $sum_bet * 75;
            } elseif ($count == 4) {
                $win_sum = $sum_bet * 11;
            } elseif ($count == 3) {
                $win_sum = $sum_bet * 2;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 1;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 6:
            if ($count == 6) {
                $win_sum = $sum_bet * 125;
            } elseif ($count == 5) {
                $win_sum = $sum_bet * 30;
            } elseif ($count == 4) {
                $win_sum = $sum_bet * 7;
            } elseif ($count == 3) {
                $win_sum = $sum_bet * 2;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 0;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 7:
            if ($count == 7) {
                $win_sum = $sum_bet * 200;
            } elseif ($count == 6) {
                $win_sum = $sum_bet * 75;
            } elseif ($count == 5) {
                $win_sum = $sum_bet * 12;
            } elseif ($count == 4) {
                $win_sum = $sum_bet * 4;
            } elseif ($count == 3) {
                $win_sum = $sum_bet * 1;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 0;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 8:
            if ($count == 8) {
                $win_sum = $sum_bet * 200;
            } elseif ($count == 7) {
                $win_sum = $sum_bet * 120;
            } elseif ($count == 6) {
                $win_sum = $sum_bet * 17;
            } elseif ($count == 5) {
                $win_sum = $sum_bet * 6;
            } elseif ($count == 4) {
                $win_sum = $sum_bet * 2;
            } elseif ($count == 3) {
                $win_sum = $sum_bet * 1;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 0;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 9:
            if ($count == 9) {
                $win_sum = $sum_bet * 500;
            } elseif ($count == 8) {
                $win_sum = $sum_bet * 200;
            } elseif ($count == 7) {
                $win_sum = $sum_bet * 55;
            } elseif ($count == 6) {
                $win_sum = $sum_bet * 14;
            } elseif ($count == 5) {
                $win_sum = $sum_bet * 5;
            } elseif ($count == 4) {
                $win_sum = $sum_bet * 2;
            } elseif ($count == 3) {
                $win_sum = $sum_bet * 0;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 0;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;

        case 10:
            if ($count == 10) {
                $win_sum += 2500;
            } elseif ($count == 9) {
                $win_sum = $sum_bet * 500;
            } elseif ($count == 8) {
                $win_sum = $sum_bet * 100;
            } elseif ($count == 7) {
                $win_sum = $sum_bet * 24;
            } elseif ($count == 6) {
                $win_sum = $sum_bet * 8;
            } elseif ($count == 5) {
                $win_sum = $sum_bet * 4;
            } elseif ($count == 4) {
                $win_sum = $sum_bet * 1;
            } elseif ($count == 3) {
                $win_sum = $sum_bet * 0;
            } elseif ($count == 2) {
                $win_sum = $sum_bet * 0;
            } elseif ($count == 1) {
                $win_sum = $sum_bet * 0;
            }
            break;
    }

    if (in_array($server_numbers[19], $user_numbers)) {
        $win_sum *= 4;
        $lastbal = "yes";
    }

    $balance = $balance + $win_sum;
    $winSum += $win_sum;
}
echo 'itr: ' . $iterations . '<br/>';
echo 'final balance: ' . $balance . '<br/>';
echo '<br/> <b>' . 'Win percentage: ' . '</b>'. $winSum / $iterations * 100 . '<br/>';
die();

// -------------------------  functions -------------------------

function getNumbers($count = 20)
{
    $numbers = range(1, 80);
    shuffle($numbers);

    return array_slice($numbers, 0, $count);
}
