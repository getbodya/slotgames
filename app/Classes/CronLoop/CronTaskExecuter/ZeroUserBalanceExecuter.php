<?php

namespace App\Classes\CronLoop\CronTaskExecuter;

use App\Classes\CronLoop\CronTaskExecuter\Base\CronTaskExecuterBase;
use App\Services\BridgeApiService;

/**
 * Проверка баланса пользователя на предмет увеличения после того, как он обнулился
 * Условием закрытия задачи: получение от BridgeApi значения баланса != 0
 */
class ZeroUserBalanceExecuter implements CronTaskExecuterBase
{
    /**
     * Делается получение баланса пользователя в данной игре через bridgeApi
     * Если баланс > 0, то через сокет отправляются данные на фронты игры
     * Метод возвращает true либо false в зависимости была ли выполнена задача
     */
    public function executeTask(array $data) : bool
    {
        // получение необходимых данных для отправки запроса к апи
        $token = $data['token'];
        $userId = $data['userId'];
        $gameId = $data['gameId'];

        // получение баланса и выполнение отправки данных для сокетов в ТЕСТЕ
        if (isset($_SESSION['test'])) {
            if ($_SESSION['test'] === true) {

                $balance = 10;

                if ($balance > 0) {
                    // отправка данных через сокет
                    $data = [
                        'topic_id' => 'onUpUserBalance',
                        'data' => $balance
                    ];

                    //\App\Classes\Socket\Pusher::sentDataToServer($data);
                    // отправка в CronLoopDB ответа, что задача выполнена
                    return true;
                } else {
                    // отправка в CronLoopDB ответа, что задача не выполнена
                    return false;
                }
            }
        }

        // получение баланса и выполнение отправки данных для сокетов
        $bridgeApiService = new BridgeApiService;
        $balanceData = $bridgeApiService->getBalance($token, $userId, $gameId);
        if (isset(json_decode($balanceData)->balance)) {
            $balance = (float) json_decode($balanceData)->balance;

            if ($balance > 0) {
                // отправка данных через сокет
                $data = [
                    'topic_id' => 'onUpUserBalance',
                    'data' => $balance
                ];

                \App\Classes\Socket\Pusher::sentDataToServer($data);

                // отправка в CronLoopDB ответа, что задача выполнена
                return true;
            } else {
                // отправка в CronLoopDB ответа, что задача не выполнена
                return false;
            }
        }

        return false;
    }
}
