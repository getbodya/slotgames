<?php

namespace App\Classes\CronLoop\CronTaskExecuter\Base;

/**
 * Интрефейс классов, которые выполняются cron задачи
 */
interface CronTaskExecuterBase
{
    public function executeTask(array $data);
}
