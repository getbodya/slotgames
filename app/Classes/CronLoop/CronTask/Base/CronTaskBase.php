<?php

namespace App\Classes\CronLoop\CronTask\Base;

/**
 * Интерфейс, который должны реализовывать все классы cron задач
 */
abstract class CronTaskBase
{
    protected $data;
    protected $type;

    public function getType() : string
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        return $this->type = $type;
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function setData(array $data)
    {
        $this->data = $data;
    }
}
