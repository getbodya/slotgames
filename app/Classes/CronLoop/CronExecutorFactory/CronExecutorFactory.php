<?php

namespace App\Classes\CronLoop\CronExecutorFactory;

use App\Classes\CronLoop\CronTask\CronTask;
use App\Classes\CronLoop\CronTaskExecuter\ZeroUserBalanceExecuter;
use App\Classes\CronLoop\CronTaskExecuter\Base\CronTaskExecuterBase;

/**
 * Фабрика, которая отдает задачу классу исполнителю
 */
class CronExecutorFactory
{
    /**
     * Метод возвращающий исполнителя исполнителя задачи
     */
    public function createCronTaskExecutor(CronTask $task) : CronTaskExecuterBase
    {
        $dataForExecutor = $task->getData(); // данные необходимые исполнителю для выполнения задачи
        switch ($task->getType()) {
            case 'user_have_zero_balance':
                $cronTaskExecutor = new ZeroUserBalanceExecuter($dataForExecutor);
                break;

            default:

                break;
        }

        return $cronTaskExecutor;
    }
}
