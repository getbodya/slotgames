<?php

namespace App\Classes\CronLoop\CronExecutorFactory\Base;

/**
 * Интерфейс который должны реалиховать классы, которые будут зниматься тем,
 * чтобы создавать исполнителей cron задач
 */
interface CronExecutorFactoryBase
{
    public function createCronTaskExecutor(CronTask $cronTask);
}
