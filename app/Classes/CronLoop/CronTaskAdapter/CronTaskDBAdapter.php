<?php

namespace App\Classes\CronLoop\CronTaskAdapter;

use App\Classes\CronLoop\CronTaskAdapter\Base\CronTaskAdapterBase;
use App\Classes\CronLoop\CronTask\CronTask;
use App\Classes\CronLoop\CronTask\Base\CronTaskBase;

/**
 * Класс, который преобразует данные к типу CronTaskBase
 */
class CronTaskDBAdapter implements CronTaskAdapterBase
{
    public function adapt($task) : CronTaskBase {
        $cronTask = new CronTask;

        $cronTask->setType($task->type);
        $cronTask->setData(unserialize($task->data));

        return $cronTask;
    }
}
