<?php

namespace App\Classes\GameData\Base;

/**
 *  Интерфейс, который должен реализовывать класс работающий с определенными данными
 */
interface IData
{
    public function get();
    public function set();
}
