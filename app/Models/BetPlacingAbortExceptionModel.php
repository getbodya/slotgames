<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BetPlacingAbortExceptionModel extends Model
{
    protected $table = 'bet_placing_abort_exception_models';
}
