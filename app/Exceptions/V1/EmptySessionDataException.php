<?php
namespace App\Exceptions\V1;

use Exception;
use App\Services\ApiService;

class EmptySessionDataException extends Exception
{
    public function __construct()
    {

    }

    public function render()
    {
        echo "нет каких то данных в сессии<br>";
        dd($_SESSION);
    }

    public function setDevSessionData()
    {
        $_SESSION['sessionName'] = '111111';
        $_SESSION['token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImMwYmRkODkyNDVjYjJkN2VjZTA2YzMzMDY4NTkzOTU0MjBmZmIyNjU2YWRmOWM1NTk5ZmNjZjIwMjMyMDkyMmE5NzE3NmRkYzA2YzQ4MTAzIn0.eyJhdWQiOiIxIiwianRpIjoiYzBiZGQ4OTI0NWNiMmQ3ZWNlMDZjMzMwNjg1OTM5NTQyMGZmYjI2NTZhZGY5YzU1OTlmY2NmMjAyMzIwOTIyYTk3MTc2ZGRjMDZjNDgxMDMiLCJpYXQiOjE1Mjk0MjI1ODQsIm5iZiI6MTUyOTQyMjU4NCwiZXhwIjoxNTYwOTU4NTg0LCJzdWIiOiIxIiwic2NvcGVzIjpbImFwaSJdfQ.kjWcg_cutm_LsRfpSrWQDBgJeS5NR9eExC_7SJdeXsUM_GC0rlKIgEaVr76yuPHnZdFPApfNxOxjGX8RvVJH4M9knvSjj1rxKY9-CyJGdAEvbtam71cLPsVnBRNufNA0UgR5WFeSaP4gav6a1a4CcUeTtIuIakkRQ1qn-IB_or2Uz7hTPpzd7hZzMAAK0cdsz7QUF74JuASlOgucqSFQbCz6Ri6g8nJ9y9jU4BS8c_k2apQrAWimaHfm4MRmSQYkt9I7j6u1DpERKRrpTfITloCB3ftaOYLJTHOjpqup4RQJr4oWWXBLg2vlMmTDis32mX16S5QhjVcnnmAH2wyA1LjAa3PI3CgrKX-kQTij8u1pvtHPqx9HrBw5RdGF5OYGuKdPQat-Yxe7l9yzkZ56pIibpL_vzUzbshzPlzADco0p-W5H9vSjku8VKoPXymfvf5RODogagNph8rofgF6X3qnz5S7yjXkQInI_vBCdkvCsxvhsRFMD0cyjyaVSjswOFqFfD9GodRYnMroX0Qvcq_hObLwpS1SYTOiAYp6CmQ1kuHwET147u1cDqUIgtfDXIdQ1oDc6fsGw-fgU_HgWGYj8EneKk1faOdsGuU2UELCFIeq0srsdHJCuSYeUurcIyXckA6w7jBM5xNFa64HeV6gHBbryv7kK-5REpbJGqhI';
        $_SESSION['userId'] = 1;
        $_SESSION['gameId'] = 1;
        $_SESSION['nickname'] = '+70962342342';
        $_SESSION['demo'] = false;
        $_SESSION['platformId'] = '1';
        $_SESSION['freeSpinData'] = false;
    }

}
