<?php
namespace App\Exceptions\V1;

use Error;

class BetPayedAbortException extends Error
{
    public function __construct()
    {
        return ["status" => "false", "message" => "BetPayedAbortException"];
    }
}
