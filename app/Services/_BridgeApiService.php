<?php

namespace App\Services;

use App\Services\BridgeApiService;

class _BridgeApiService extends BridgeApiService
{
    public function _updateMoveFundsDates($params, $type = 'start_of_spin')
    {
        return $this->updateMoveFundsDates($params, $type);
    }
}
