<?php

namespace App\Services\GameServices;

use App\Services\GameServices\ElGallo\ElGalloBaseSpinService;
use App\Models\Jackpot;
use App\Models\UserJackpot;
use App\Models\Stat;
use App\Models\SavedDataBundle;
use App\Services\BridgeApiService;
use App\Exceptions\V1\BetPayedAbortException;
use App\Exceptions\V1\BalanceException;
use App\Exceptions\V1\BetPlacingAbortException;
use Illuminate\Support\Facades\Config;
use App\Exceptions\V1\UnauthenticatedException;
use App\Exceptions\V1\MoveFundsException;
use App\Models\Event;
use Webpatser\Uuid\Uuid;
use App\Models\Session;
use App\Models\V2Game;
use App\Models\Log;

class ElGalloSpinService extends ElGalloBaseSpinService
{
    private $jokerValue = 0;
    private $bonusValue = 2;
    private $wl = [];
    private $balance;
    private $allWin;
    private $rope = false;
    private $freeSpinData;
    private $jackpotPercentageIncrease = 0.005;
    private $bet;

    /**
     * Вычисление результата спина
     */
    public function getSpinResultData($betLine, $linesInGame, $gameName, $data = [])
    {
        $_SESSION['linesInGame'] = $linesInGame;
        $_SESSION['betLine'] = $betLine;
        $_SESSION['bet'] = $linesInGame * $betLine;

        // отправка запроса на снятие средств
        $this->sendBetPlacing();

        if (!isset($_SESSION['reconnect'])) {
            $_SESSION['reconnect'] = false;
        }
        if (!isset($_SESSION['firstSpinAfterFreeSpins'])) {
            $_SESSION['firstSpinAfterFreeSpins'] = false;
        }
        if (!isset($_SESSION['test'])) {
            $_SESSION['test'] = false;
        }

        if ($_SESSION['demo'] === '0') {
            $_SESSION['demo'] = false;
        }

        $this->balance = round($_SESSION['balance'], 2);

        $gameRules =  '{"lines":[[1,4,7,10,13],[0,3,6,9,12],[2,5,8,11,14],[0,4,8,10,12],[2,4,6,10,14],[1,5,8,11,13],[1,3,6,9,13],[1,4,8,10,13],[1,4,6,10,13],[0,3,7,9,12],[2,5,7,11,14],[0,4,7,10,12],[2,4,7,10,14],[0,4,6,10,12],[2,4,8,10,14],[1,3,7,9,13],[1,5,7,11,13],[2,3,8,9,14],[0,5,6,11,12],[1,3,8,9,13],[1,5,6,11,13],[0,3,7,11,14],[2,5,7,9,12],[0,4,7,10,14],[2,4,7,10,12]],"winRules":[[0,3,100,0],[0,4,1000,0],[0,5,10000,0],[1,3,30,0],[1,4,50,0],[1,5,200,0],[3,3,15,0],[3,4,20,0],[3,5,100,0],[4,3,50,0],[4,4,250,0],[4,5,1000,0],[2,2,2,0],[2,3,5,0],[2,4,20,0],[2,5,400,0],[5,3,40,0],[5,4,150,0],[5,5,750,0],[6,3,25,0],[6,4,40,0],[6,5,150,0],[7,3,20,0],[7,4,30,0],[7,5,125,0],[8,3,10,0],[8,4,20,0],[8,5,75,0],[9,3,30,0],[9,4,100,0],[9,5,500,0],[10,3,50,0],[10,4,250,0],[10,5,1500,0],[11,3,25,0],[11,4,50,0],[11,5,175,0]],"cards":[[0,13,26,39],[1,14,27,40],[2,15,28,41],[3,16,29,42],[4,17,30,43],[5,18,31,44],[6,19,32,45],[7,20,33,46],[8,21,34,47],[9,22,35,48],[10,23,36,49],[11,24,37,50],[12,25,38,51]],"numberOfCellValues":9}';

        if (Config::get('app.mod') === 'dev' || $_SESSION['demo'] === true) {
            if ($_SESSION['freeSpinData'] != false) {
                if ($_SESSION['freeSpinData']['count'] === 0) {
                    $_SESSION['freeSpinData'] = false;
                }
            }
        }

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            // проверка активности сессии на сервер с мост-апи (http://slot.pantera.co.ua)
            // $apiService = new BridgeApiService();
            //
            // $checkApiSession = json_decode($apiService->sessionCheck($_SESSION['token'], $_SESSION['userId']));
            // if (!isset($checkApiSession->status)) {
            //     throw new BetPlacingAbortException($linesInGame * $betLine, $linesInGame);
            // }
            // if ($checkApiSession->status === false) {
            //     throw new BetPlacingAbortException($linesInGame * $betLine, $linesInGame);
            // }

            // $bridgeBalance = (float) $getBalanceData->balance + $betLine * $linesInGame;
            // $_SESSION['balance'] = $bridgeBalance;
        } else {
            // прибавляем к балансу предыдущий выигрышь
            if ($_SESSION['freeSpinData'] === false) {
                if ($_SESSION['firstSpinAfterFreeSpins'] === true) {
                    //dd($_SESSION['allWin']);
                    $_SESSION['firstSpinAfterFreeSpins'] = false;
                } else {

                }

                $_SESSION['balance'] += round($_SESSION['allWin'], 2);
            }
        }

        $_SESSION['cardGameIteration'] = 0;

        $_SESSION['allWin'] = 0; // обнуляем общий выигрышь

        if ($data === []) {
            //$info = $this->generatingValuesForCells($gameRules);
            $info = $this->generatingValuesForCellsV2($gameRules);
            //$info = [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]; // freespin
            //$info = [5,3,2,1,7,8,1,7,8,4,2,8,2,11,1]; // win on gallo
            //$info = [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]; // false
            //$info = [1,9,10,1,7,8,1,7,8,4,2,8,2,11,1]; // win;
            //$info = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            //$info = [2,7,3,2,4,3,1,2,3,5,7,6,4,7,3]; // freespin an wl (1,25+0,15)
            //$info = [8,3,3,8,6,6,8,3,3,4,4,4,5,5,5];
        } else {
            $info = $data;
        }

        // для возможности управления результатом с фронта
        if (Config::get('app.mod') === 'dev') {
            if (isset($_GET['code'])) {
                if ($_GET['code'] === 'lose') {
                    $info = [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5];
                }
                if ($_GET['code'] === 'win') {
                    $info = [7,7,3,7,4,0,1,3,4,0,7,6,4,7,3];
                }
                if ($_GET['code'] === 'freespin') {
                    $info = [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1];
                }
            }
        }

        $winBonusSymbolsData = $this->getWinBonusSymbolsData($gameRules, $info, $this->bonusValue); // [count, winValue]

        // джекпот
        if ($_SESSION['freeSpinData'] == false) {
            $this->updateJackpots(floatval($betLine), floatval($linesInGame), $this->jackpotPercentageIncrease);
            $jackpotData = $this->getJackpotData();

            if ($_SESSION['bet'] >= 0.50) {
                $jackpot = $this->getJackpot();
            } else {
                $jackpot = false;
            }
        } else {
            $jackpotData = $this->getJackpotData();
            $jackpot = false;
        }

        //$winLinesData = $this->getWinLinesData($gameRules, $info, $linesInGame);
        $winLinesData = $this->getWinLinesDataV2($gameRules, $info, $linesInGame);

        $_SESSION['checkFreeSpin'] = false;
        if ($_SESSION['freeSpinData'] === false) {
            if ($winBonusSymbolsData[0] > 2) {
                $_SESSION['checkFreeSpin'] = 'freeSpin';
            }
        }

        $winCellInfo = [];

        // проверка превышения лимита
        $currentAllWinSpin = $this->getAllWinWithBonus($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame);
        if ($currentAllWinSpin > 2500) {
            return $this->getSpinResultData($betLine, $linesInGame, 'elGallo');
        }

        if ($_SESSION['checkFreeSpin'] == 'freeSpin') {
            $this->getAllWinWithBonus($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame);
            $this->firstStartFreeSpin($betLine, $linesInGame, $winBonusSymbolsData);

            $winCellInfo = $this->getWinCellInfo($winLinesData, $gameRules, $info);
            $this->wl = $this->getWl($gameRules, $winLinesData, $betLine);

            if (Config::get('app.mod') === 'dev' && $_SESSION['demo'] !== 'false') {
                $this->balance = round(($_SESSION['balance'] - $betLine * $linesInGame), 2);
            }
        } else {
            if ($_SESSION['freeSpinData'] != false) {
                $this->freeSpinIteration($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame);
            } else {
                $this->getAllWinWithBonus($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame);

                if ($_SESSION['allWin'] > 2500 && $_SESSION['freeSpinData'] === false) {
                    return $this->getSpinResultData($betLine, $linesInGame, 'elGallo');
                }

                if (Config::get('app.mod') === 'dev' || $_SESSION['demo'] !== false) {
                    $this->balance = round(($_SESSION['balance'] - $betLine * $linesInGame), 2);
                }
            }

            $winCellInfo = $this->getWinCellInfo($winLinesData, $gameRules, $info);
            $this->wl = $this->getWl($gameRules, $winLinesData, $betLine);
        }

        $_SESSION['balance'] = round($this->balance, 2);

        // info для Api
        $infoForApi = $this->makeInfoForApi($info);

        $spinResultData = [
            'balance' => round($_SESSION['balance'], 2),
            'allWin' => round($_SESSION['allWin'], 2),
            'info' => $info,
            'betLine' => $betLine,
            'linesInGame' => $linesInGame,
            'winCellInfo' => $winCellInfo,
            'wl' => $this->wl,
            'status' => true,
            'rope' => $this->rope,
            'winBonusSymbolsData' => $winBonusSymbolsData,
            'jackpot' => $jackpot,
            'jackpotData' => $jackpotData,
            'freeSpinData' => $_SESSION['freeSpinData'],
            'reconnect' => $_SESSION['reconnect'],
            'info_for_api' => $infoForApi,
            'winLinesData' => $winLinesData
        ];

        // занесение данных в лог для проверки бага в демо режиме
        if (Config::get('app.mod') === 'dev' || $_SESSION['demo'] === "true" || $_SESSION['demo'] === true) {
            $log = new Log;
            $log->type = 'work';
            $log->data = json_encode(array(
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'allWin' => $spinResultData['allWin'],
                'featureGame' => $_SESSION['freeSpinData'],
                'balance' => round($_SESSION['balance'], 2),
                'action' => 'spin'
            ));
            $log->save();
        }

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            // отправка moveFunds с данными результата хода
            // TODO: добавил в услование   && $_SESSION['checkFreeSpin'] !== 'freeSpin', чтобы при выпадении фриспина в extraInfo featureGame = false
            $freeSpinForBridge = false;
            if ($spinResultData['freeSpinData'] != false) {
                if ($spinResultData['freeSpinData']['count'] !== -1) {
                    $freeSpinForBridge = true;
                }
            }

            if ($_SESSION['firstSpinAfterFreeSpins'] === true) {
                $freeSpinForBridge = true;
                $_SESSION['firstSpinAfterFreeSpins'] = false;
            }

            // if ($winLinesData == 'freeSpin') {
            //     $freeSpinForBridge = false;
            // }

            // занесение данных в лог для проверки бага
            $log = new Log;
            $log->type = 'work';
            $log->data = json_encode(array(
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'allWin' => $spinResultData['allWin'],
                'featureGame' => $freeSpinForBridge,
                'balance' => round($_SESSION['balance'], 2),
                'action' => 'spin'
            ));
            $log->save();

            $platformId = 1;
            if (isset($_GET['platformId'])) {
                $platformId = $_GET['platformId'];
            }
            $moveFundsRequestData = array(
                'token' => $_SESSION['token'],
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'direction' => '',
                'platformId' => $platformId,
                'eventType' => '',
                'amount' => $linesInGame * $betLine,
                'extraInfo' => ['selected' => [$betLine * $linesInGame],
                                'result' => $spinResultData['info_for_api'],
                                'featureGame' => $freeSpinForBridge,
                                'jackpotAmount' => $jackpotData['BIG_DADDY']
                ],
                'allWin' => $spinResultData['allWin'],
                'eventID' => $_SESSION['eventID']
            );
            $responseMoveFunds = (new BridgeApiService())->moveFunds($moveFundsRequestData, 'end_of_spin');

            if (!isset(json_decode($responseMoveFunds)->status)) {
                throw new MoveFundsException(serialize($moveFundsRequestData));
            }
            if (json_decode($responseMoveFunds)->status === false) {
                throw new MoveFundsException(serialize($moveFundsRequestData));
            }

            // отправка статистики в бд
            $this->writeStatistics($spinResultData);
        }

        return $spinResultData;
    }

    public function updateJackpots(float $betLine, int $linesInGame, float $jackpotPercentageIncrease)
    {
        $increase = $betLine * $linesInGame * $jackpotPercentageIncrease;

        $jackpotNoteNumber = $this->getJackpotNoteNumber();
        $jackpots = (new Jackpot)->find($jackpotNoteNumber);

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            if (($jackpots->mini + $increase) <= $jackpots->max_mini) {
                $jackpots->mini += $increase;
            } else {
                $jackpots->mini = $jackpots->max_mini;
            }

            if (($jackpots->minor + $increase) <= $jackpots->max_minor) {
                $jackpots->minor += $increase;
            } else {
                $jackpots->minor = $jackpots->max_minor;
            }

            if (($jackpots->major + $increase) <= $jackpots->max_major) {
                $jackpots->major += $increase;
            } else {
                $jackpots->major = $jackpots->max_major;
            }

            if (($jackpots->big_daddy + $increase) <= $jackpots->max_big_daddy) {
                $jackpots->big_daddy += $increase;
            } else {
                $jackpots->big_daddy = $jackpots->max_big_daddy;
            }

            $jackpots->save();
        }

        if (isset($_SESSION['simulation'])) {
            if ($_SESSION['simulation'] === true) {
                if (($jackpots->mini + $increase) <= $jackpots->max_mini) {
                    $jackpots->mini += $increase;
                } else {
                    $jackpots->mini = $jackpots->max_mini;
                }

                if (($jackpots->minor + $increase) <= $jackpots->max_minor) {
                    $jackpots->minor += $increase;
                } else {
                    $jackpots->minor = $jackpots->max_minor;
                }

                if (($jackpots->major + $increase) <= $jackpots->max_major) {
                    $jackpots->major += $increase;
                } else {
                    $jackpots->major = $jackpots->max_major;
                }

                if (($jackpots->big_daddy + $increase) <= $jackpots->max_big_daddy) {
                    $jackpots->big_daddy += $increase;
                } else {
                    $jackpots->big_daddy = $jackpots->max_big_daddy;
                }

                $jackpots->save();
            }
        }
    }

    public function getJackpotData()
    {
        $jackpotNoteNumber = $this->getJackpotNoteNumber();
        $data = (new Jackpot)->find($jackpotNoteNumber)->toArray();

        $jackpotsData = [
          'MINI' => round($data['mini'], 2),
          'MINOR' => round($data['minor'], 2),
          'MAJOR' => round($data['major'], 2),
          'BIG_DADDY' => round($data['big_daddy'], 2)
        ];

        return $jackpotsData;
    }

    public function getWinCellInfo($winLinesData, $gameRules, $info)
    {
        // получаем позиции выйгравших ячеек и их значения
        $winCellPositions = [];
        $winCellInfo = [];
        foreach ($winLinesData as $item) { //получаем информацию по отдельной выигрышной линии
            if ($item !== 0) {
                $savedKey = -1;
                foreach (json_decode($gameRules)->lines[$item[0]] as $key => $cellPosition) {
                    // проверяем есть ли в данной линии символ-joker
                    if ($info[$cellPosition] == 0) {
                        if ($savedKey === ($key - 1)) {
                            $winCellPositions[] = [$cellPosition, 0]; // записываем позицию ячейки и ее значение
                            $savedKey += 1;
                        }
                    }
                    if ($info[$cellPosition] == $item[1]) {
                        if ($savedKey === ($key - 1)) {
                            $winCellPositions[] = [$cellPosition, $item[1]]; // записываем позицию ячейки и ее значение
                            $savedKey += 1;
                        }
                    }
                }
            }
        }

        // получение позиций и значений выигрышных ячеек
        for ($i = 0; $i < 15; $i++) {
            $value = false;

            foreach ($winCellPositions as $winCellPosition) {
                if ($winCellPosition[0] == $i) {
                    $value = $winCellPosition[1];
                    break;
                }
            }

            $winCellInfo[] = $value;
        }

        return $winCellInfo;
    }

    protected function getWl($gameRules, $winLinesData, $betLine)
    {
        $winOfLines = [];
        for ($i = 1; $i <= 25; $i++) {
            foreach ($winLinesData as $winLineData) {
                if (($winLineData[0] + 1) != $i) {
                    $winOfLines[$i] = 0;
                } else {
                    foreach (json_decode($gameRules)->winRules as $rule) {
                        if ($rule[0] == $winLineData[1]) {
                            if ($rule[1] == $winLineData[2]) {
                                $winOfLines[$i] = $betLine * $rule[2];
                            }
                        }
                    }
                    break;
                }
            }
        }

        foreach ($winOfLines as $key => $value) {
            $winOfLines[$key] = round($value, 2);
        }

        return $winOfLines;
    }

    public function getCardSuit($value)
    {
        $suit = '';
        switch ($value) {
            case 'p':
                $suit = 'Black Spades';
                break;
            case 'c':
                $suit = 'Red Hearts';
                break;
            case 'k':
                $suit = 'Black Clubs';
                break;
            case 'b':
                $suit = 'Red Diamonds';
                break;
            case 'red':
                $suit = 'Red';
                break;
            case 'black':
                $suit = 'Black';
                break;

            default:
                $suit = '';
                break;
        }

        return $suit;
    }

    /**
     * Получение jackpot
     * return после каждого блока делается, чтобы несколько джекотов не выпало на одном ходу
     *
     * @return array|bool
     */
    public function getJackpot()
    {
        $jackpots = $this->getJackpotData();

        $jackpotNoteNumber = $this->getJackpotNoteNumber();
        $data = (new Jackpot)->find($jackpotNoteNumber)->toArray();

        if ($_SESSION['demo'] === 'true') {
            $_SESSION['demo'] = true;
        }
        $_SESSION['demo'] === true ? $demo = 'true' : $demo = 'false';

        // выпадает первый достигнувший своего выигрышного значения джекпот
        if ($jackpots['MINI'] >= round($data['result_mini'], 2)) {
            // создание записи в БД о ждущем пользователя джекпоте
            $userJackpot = new UserJackpot;
            $userJackpot->user_id = $_SESSION['userId'];
            $userJackpot->jackpot_type = 'mini';
            $userJackpot->jackpot_value = $jackpots['MINI'];
            $userJackpot->demo = $demo;
            $userJackpot->save();

            $jackpots['result'] = 'MINI';

            // генерация нового выигрышного значения
            $jackpot = (new Jackpot)->find($jackpotNoteNumber);
            $jackpot->result_mini = rand(10, 40);
            $jackpot->save();

            // обнуляем джекпот выигравшей категории
            $this->resetJackpot('MINI');

            // добавляем данные для отображения после джекпота на первом экране
            $jackpots['nextData'] = $this->getJackpotData();

            return $jackpots;
        }

        if ($jackpots['MINOR'] >= round($data['result_minor'], 2)) {
            // создание записи в БД о ждущем пользователя джекпоте
            $userJackpot = new UserJackpot;
            $userJackpot->user_id = $_SESSION['userId'];
            $userJackpot->jackpot_type = 'minor';
            $userJackpot->jackpot_value = $jackpots['MINOR'];
            $userJackpot->demo = $demo;
            $userJackpot->save();

            $jackpots['result'] = 'MINOR';

            // генерация нового выигрышного значения
            $jackpot = (new Jackpot)->find($jackpotNoteNumber);
            $jackpot->result_minor = rand(50, 200);
            $jackpot->save();

            // обнуляем джекпот выигравшей категории
            $this->resetJackpot('MINOR');

            // добавляем данные для отображения после джекпота на первом экране
            $jackpots['nextData'] = $this->getJackpotData();

            return $jackpots;
        }

        if ($jackpots['MAJOR'] >= round($data['result_major'], 2)) {
            // создание записи в БД о ждущем пользователя джекпоте
            $userJackpot = new UserJackpot;
            $userJackpot->user_id = $_SESSION['userId'];
            $userJackpot->jackpot_type = 'major';
            $userJackpot->jackpot_value = $jackpots['MAJOR'];
            $userJackpot->demo = $demo;
            $userJackpot->save();

            $jackpots['result'] = 'MAJOR';

            // генерация нового выигрышного значения
            $jackpot = (new Jackpot)->find($jackpotNoteNumber);
            $jackpot->result_major = rand(250, 500);
            $jackpot->save();

            // обнуляем джекпот выигравшей категории
            $this->resetJackpot('MAJOR');

            // добавляем данные для отображения после джекпота на первом экране
            $jackpots['nextData'] = $this->getJackpotData();

            return $jackpots;
        }

        if ($jackpots['BIG_DADDY'] >= round($data['result_big_daddy'], 2)) {
            // создание записи в БД о ждущем пользователя джекпоте
            $userJackpot = new UserJackpot;
            $userJackpot->user_id = $_SESSION['userId'];
            $userJackpot->jackpot_type = 'big_daddy';
            $userJackpot->jackpot_value = $jackpots['BIG_DADDY'];
            $userJackpot->demo = $demo;
            $userJackpot->save();

            $jackpots['result'] = 'BIG_DADDY';

            // генерация нового выигрышного значения
            $jackpot = (new Jackpot)->find($jackpotNoteNumber);
            $jackpot->result_big_daddy = rand(500, 1000);
            $jackpot->save();

            // обнуляем джекпот выигравшей категории
            $this->resetJackpot('BIG_DADDY');

            // добавляем данные для отображения после джекпота на первом экране
            $jackpots['nextData'] = $this->getJackpotData();

            return $jackpots;
        }


        return false;
    }

    public function resetJackpot(string $jackpotKey)
    {
        $jackpotNoteNumber = $this->getJackpotNoteNumber();
        $jackpotDB = (new Jackpot)->find($jackpotNoteNumber);

        switch ($jackpotKey) {
            case 'MINI':
                $jackpotDB->mini = $jackpotDB->min_mini;
                break;
            case 'MINOR':
                $jackpotDB->minor = $jackpotDB->min_minor;
                break;
            case 'MAJOR':
                $jackpotDB->major = $jackpotDB->min_major;
                break;
            case 'BIG_DADDY':
                $jackpotDB->big_daddy = $jackpotDB->min_big_daddy;
                break;
        }

        $jackpotDB->save();
    }

    /**
     * Получение выигрыша с учетов бонусной игры и выигрыша не по линиям
     *
     * @param $gameRules
     * @param $winLinesData
     * @param $betLine
     * @param $winBonusSymbolsData
     */
    public function getAllWinWithBonus($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame)
    {
        $freeSpinData = $_SESSION['freeSpinData'];

        if ($_SESSION['freeSpinData']) {
            $_SESSION['allWin'] = round($this->getAllWin($gameRules, $winLinesData, $betLine) * $freeSpinData['mul'], 2);
        } else {
            $_SESSION['allWin'] = round($this->getAllWin($gameRules, $winLinesData, $betLine), 2);
        }

        if ($winBonusSymbolsData !== [0, 0]) {
            $_SESSION['allWin'] += round($winBonusSymbolsData[1] * $betLine * $linesInGame, 2);
        }

        return $_SESSION['allWin'];
    }

    /**
     * Запуск игры freeSpin
     *
     * @param $betLine
     * @param $linesInGame
     * @param $winBonusSymbolsData
     */
    public function firstStartFreeSpin($betLine, $linesInGame, $winBonusSymbolsData)
    {
        $this->rope = $this->getRope(); // ['count' => ... , 'mul' => ..., 'allWin' => ..., 'repeat' => ...]
        $this->rope['allWin'] = round($_SESSION['allWin'], 2);

        $_SESSION['linesInGame'] = $linesInGame;
        $_SESSION['freeSpinData'] = $this->rope;
        $_SESSION['preAllWinOnRope'] = round($_SESSION['allWin'], 2);
        $_SESSION['beginFreeSpinCount'] = $_SESSION['freeSpinData']['count'];

        if (Config::get('app.mod') === 'dev' || $_SESSION['demo'] !== false) {
            $this->balance = $_SESSION['balance'] - $betLine * $linesInGame;
        }
    }

    /**
     * Итерация в игре freeSpin
     *
     * @param $gameRules
     * @param $winLinesData
     * @param $betLine
     */
    public function freeSpinIteration($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame)
    {
        $freeSpinData = $_SESSION['freeSpinData'];

        $this->rope = false;

        if ($freeSpinData['count'] < 0) {
            $_SESSION['freeSpinData'] = false;
            $_SESSION['linesInGame'] = false;

            return false;
        }

        $_SESSION['allWin'] = round($this->getAllWin($gameRules, $winLinesData, $betLine) * $freeSpinData['mul'], 2) + round($winBonusSymbolsData[1] * $betLine * $linesInGame, 2);

        $newCount = $this->correctionFreeSpinCounter();

        if (Config::get('app.mod') === 'dev' || $_SESSION['demo'] !== false) {
            if ($newCount === 0) { // компенсация недостатка прибавления выигрыша на следующем ходу. Нужно так как на следующем ходу фриспинов не будет, а данное значение будет в allWin с предыдущего хода
                $_SESSION['firstSpinAfterFreeSpins'] = true;
            }
        }

        $_SESSION['freeSpinData'] = [
            'count' => $newCount,
            'mul' => $freeSpinData['mul'],
            'allWin' => round(($freeSpinData['allWin'] + ($this->getAllWin($gameRules, $winLinesData, $betLine) * $freeSpinData['mul']) + round($winBonusSymbolsData[1] * $betLine * $linesInGame, 2)), 2),
            'repeat' => $freeSpinData['repeat']
        ];

        $this->balance = $_SESSION['balance'];
    }

    /**
     * Получение выигрыша от бонусного символа
     *
     * @param $gameRules
     * @param array $info
     * @param int $bonusValue
     * @return array
     */
    public function getWinBonusSymbolsData($gameRules, array $info, int $bonusValue)
    {
        $bonusSymbolCount = 0;
        foreach ($info as $item) {
            if ($item === $bonusValue) {
                $bonusSymbolCount += 1;
            }
        }

        // в случае если не достаточно бонусных символов, то выходим
        if ($bonusSymbolCount <= 1) {
            return [0, 0];
        }

        $winValue = 0;
        foreach (json_decode($gameRules)  ->winRules as $item) {
            if ($item[0] === $bonusValue) {
                if ($bonusSymbolCount <= 5) {
                    if ($item[1] === $bonusSymbolCount) {
                        $winValue = $item[2];
                        break;
                    }
                } else {
                    // если бонусных символов больше чем пять, то отбираем наибольший возможный выигрыш
                    if ($winValue < $item[2]) {
                        $winValue = $item[2];
                    }
                }
            }
        }

        $winBonusSymbolsData = [$bonusSymbolCount, $winValue];

        // дополнительное умножение выигрыша на множитель от фриспинов
        if ($_SESSION['freeSpinData'] !== false) {
            $winBonusSymbolsData = [$bonusSymbolCount, $winValue * $_SESSION['freeSpinData']['mul']];
        }

        return $winBonusSymbolsData;
    }


    /**
     * Обработка выбора пользователя после freeSpin
     *
     * @param string $choice
     * @return array|string
     */
    public function getChoiceResultData(string $choice)
    {
        $freeSpinData = $_SESSION['freeSpinData'];

        if ($_SESSION['demo'] === 'false') {
            $_SESSION['demo'] = false;
        }

        switch ($choice) {
            case 'repeat':
                if ($freeSpinData['repeat'] === false) {
                    if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
                        $platformId = 1;
                        if (isset($_GET['platformId'])) {
                            $platformId = $_GET['platformId'];
                        }
                        $moveFundsRequestData = array(
                            'token' => $_SESSION['token'],
                            'userId' => $_SESSION['userId'],
                            'gameId' => $_SESSION['gameId'],
                            'direction' => 'debit',
                            'platformId' => $platformId,
                            'eventType' => 'Correction',
                            'amount' => round(($freeSpinData['allWin'] - $_SESSION['preAllWinOnRope']), 2),
                            'extraInfo' => [
                                'featureGame' => true
                            ],
                            'eventID' => $_SESSION['eventID']
                        );
                        $responseMoveFunds = (new BridgeApiService())->moveFunds($moveFundsRequestData, 'end_of_FS_spin');

                        if (!isset(json_decode($responseMoveFunds)->status)) {
                            throw new MoveFundsException(serialize($moveFundsRequestData));
                        }
                        if (json_decode($responseMoveFunds)->status === false) {
                            throw new MoveFundsException(serialize($moveFundsRequestData));
                        }
                    }

                    // занесение данных в лог для проверки бага
                    $log = new Log;
                    $log->type = 'work';
                    $platformId = 1;
                    if (isset($_GET['platformId'])) {
                        $platformId = $_GET['platformId'];
                    }
                    $log->data = json_encode([
                        'userId' => $_SESSION['userId'],
                        'gameId' => $_SESSION['gameId'],
                        'allWin' => $freeSpinData['allWin'],
                        'balance' => $_SESSION['balance'],
                        'direction' => 'debit',
                        'platformId' => $platformId,
                        'amount' => round(($freeSpinData['allWin'] - $_SESSION['preAllWinOnRope']), 2),
                        'action' => 'choice',
                        'actionType' => 'repeat'
                    ]);
                    $log->save();

                    $rope = $this->getRope($freeSpinData['repeat']); // ['count' => ... , 'mul' => ..., 'allWin' => ... , 'repeat' => false]
                    $rope['allWin'] = $_SESSION['preAllWinOnRope'];
                    $_SESSION['preAllWinOnRope'] = 0;
                    $_SESSION['freeSpinData'] = $rope;
                    $_SESSION['allWin'] = 0;

                    return $_SESSION['freeSpinData'];
                } else {
                    $_SESSION['freeSpinData'] = false;
                    $_SESSION['repeat'] = false;
                    return 'repeat error';
                }

                // no break
            case 'get':
                // занесение данных в лог для проверки бага
                $log = new Log;
                $log->type = 'work';
                $log->data = json_encode([
                    'userId' => $_SESSION['userId'],
                    'gameId' => $_SESSION['gameId'],
                    'allWin' => $_SESSION['freeSpinData']['allWin'],
                    'balance' => $_SESSION['balance'],
                    'action' => 'choice',
                    'actionType' => 'get'
                ]);
                $log->save();

                $_SESSION['allWin'] = $freeSpinData['allWin'];

                $this->balance = $_SESSION['balance'];
                $allWin = $_SESSION['freeSpinData']['allWin'];
                $_SESSION['freeSpinData'] = false;
                $_SESSION['repeat'] = false;

                return $allWin;
            case 'random':
                //$allWin = $freeSpinData['allWin'] - $_SESSION['preAllWinOnRope'];
                $allWin = $freeSpinData['allWin'];
                $middle = floor($allWin/2);

                $minRand = round($allWin - $middle, 2);
                $maxRand = round($allWin + $middle, 2);
                $randWin = rand($minRand, $maxRand);

                // отправка запросов к апи
                if ($randWin > $allWin) {
                    $amount = $randWin - $allWin;
                    $direction = 'credit';
                } else {
                    $amount = $allWin - $randWin;
                    $direction = 'debit';
                }
                $amount = round($amount, 2);

                // занесение данных в лог для проверки бага
                $log = new Log;
                $log->type = 'work';
                $platformId = 1;
                if (isset($_GET['platformId'])) {
                    $platformId = $_GET['platformId'];
                }
                $log->data = json_encode([
                    'userId' => $_SESSION['userId'],
                    'gameId' => $_SESSION['gameId'],
                    'allWin' => $allWin,
                    'balance' => $_SESSION['balance'],
                    'minRand' => $minRand,
                    'maxRand' => $maxRand,
                    'randWin' => $randWin,
                    'direction' => $direction,
                    'platformId' => $platformId,
                    'amount' => $amount,
                    'action' => 'choice',
                    'actionType' => 'random'
                ]);
                $log->save();

                $platformId = 1;
                if (isset($_GET['platformId'])) {
                    $platformId = $_GET['platformId'];
                }
                if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
                    $moveFundsRequestData = array(
                        'token' => $_SESSION['token'],
                        'userId' => $_SESSION['userId'],
                        'gameId' => $_SESSION['gameId'],
                        'direction' => $direction,
                        'platformId' => $platformId,
                        'eventType' => 'Correction',
                        'amount' => $amount,
                        'extraInfo' => ['featureGame' => true],
                        'eventID' => $_SESSION['eventID']
                    );
                    $responseMoveFunds = (new BridgeApiService())->moveFunds($moveFundsRequestData, 'end_of_FS_spin');

                    if (!isset(json_decode($responseMoveFunds)->status)) {
                        throw new MoveFundsException(serialize($moveFundsRequestData));
                    }
                    if (json_decode($responseMoveFunds)->status === false) {
                        throw new MoveFundsException(serialize($moveFundsRequestData));
                    }
                }

                //$_SESSION['balance'] = $_SESSION['balance'] + $randWin;
                $this->balance = $_SESSION['balance'];
                $_SESSION['freeSpinData'] = false;
                $_SESSION['repeat'] = false;

                $_SESSION['allWin'] = $randWin;

                return $randWin;
        }
    }

    public function getCountCellValuesOnLinePlusJokers(array $countCellValuesOnLine, int $jokerCount)
    {
        foreach ($countCellValuesOnLine as $key => $item) {
            if ($key !== $this->jokerValue && $key !== $this->bonusValue) {
                $countCellValuesOnLine[$key] += $jokerCount;
            }
        }

        return $countCellValuesOnLine;
    }

    /**
     * Получение данных о выигрышных линиях
     *
     * @return $winLinesData [[номер линии, значение, кол-во значений], ...]
     */
    public function getWinLinesData($gameRules, $info, $linesInGame)
    {
        $winLinesData = [];

        // if ($this->checkBonusGame($info, $this->bonusValue) === 'bonus') {
        //     if (!$_SESSION['freeSpinData']) {
        //         return 'freeSpin';
        //     }
        // }

        if ($_SESSION['freeSpinData']) {
            $linesInGame = $_SESSION['linesInGame'];
        }

        // прогоняем массив содержащий значения позиций ячеек для каждой из линий
        foreach (json_decode($gameRules)->lines as $lineNumber => $cellNumbers) {
            $winLineData = [];

            $countCellValuesOnLine = $this->getCountCellValuesOnLine($cellNumbers, $info);

            //получаем кол-во джокеров на линии
            $jokerCount = $this->getCountJokersOnLine($countCellValuesOnLine, $this->jokerValue);

            $countCellValuesOnLinePlusJokers = $this->getCountCellValuesOnLinePlusJokers($countCellValuesOnLine, $jokerCount);

            if ($lineNumber < $linesInGame) {
                // определяем является ли линия выигрышной
                // $winLineData = [номер линии, значение, кол-во значений]

                $winLineDataArray = [];

                foreach ($countCellValuesOnLinePlusJokers as $value => $valueCounter) {
                    if ($valueCounter > 2) {
                        $checkStep = false;

                        if ($info[$cellNumbers[0]] === $value || $info[$cellNumbers[0]] === $this->jokerValue) {
                            $checkStep = true;
                            for ($i = 0; $i < ($valueCounter - 1); $i++) {
                                if ($value !== $info[$cellNumbers[$i + 1]] && $info[$cellNumbers[$i + 1]] !== $this->jokerValue) {
                                    if ($i > 1) {
                                        $winLineData[0] = $lineNumber;
                                        $winLineData[1] = $value;
                                        $winLineData[2] = $i + 1;

                                        $winLineDataArray[] = $winLineData;
                                    }

                                    $checkStep = false;
                                }
                            }
                        }

                        if ($checkStep === true) {
                            $winLineData[0] = $lineNumber;
                            $winLineData[1] = $value;
                            $winLineData[2] = $valueCounter;

                            $winLineDataArray[] = $winLineData;
                        }
                    }
                }

                $winLinesData[] = $this->getMaxWinLineData($winLineDataArray, $gameRules); // получаем наиболее стоящий выигрышь
            }
        }

        $winLinesDataXX = $this->trimEmptyWinLineData($winLinesData); // отсекаем пустые наборы данных

        // убираем выигрышные линии с петухами, они выигрывают не по линиям
        $winLinesData = [];
        foreach ($winLinesDataXX as $key => $value) {
            if ($value[1] !== 2) {
                $winLinesData[] = $value;
            }
        }

        return $winLinesData;
    }

    /**
     * Получение данных о выигрышных линиях V2
     *
     * @return $winLinesData [[номер линии, значение, кол-во значений], ...]
     */
    public function getWinLinesDataV2($gameRules, $cellArray, $linesInGame)
    {
        $winLinesData = [];

        if ($_SESSION['freeSpinData']) {
            $linesInGame = $_SESSION['linesInGame'];
        }

        // $winLineData = [номер линии, значение, кол-во значений]
        $winLinesData = [];

        $lineCells = [[1,4,7,10,13],[0,3,6,9,12],[2,5,8,11,14],[0,4,8,10,12],
        [2,4,6,10,14],[1,5,8,11,13],[1,3,6,9,13],[1,4,8,10,13],[1,4,6,10,13],
        [0,3,7,9,12],[2,5,7,11,14],[0,4,7,10,12],[2,4,7,10,14],[0,4,6,10,12],
        [2,4,8,10,14],[1,3,7,9,13],[1,5,7,11,13],[2,3,8,9,14],[0,5,6,11,12],
        [1,3,8,9,13],[1,5,6,11,13],[0,3,7,11,14],[2,5,7,9,12],[0,4,7,10,14],
        [2,4,7,10,12]];

        for ($key = 0; $key < $linesInGame; $key++) {
            $lineData = [0, 0]; // ['выигрышный символ', 'кол-во сииволов']
            $winCellCounter = 0;
            $winSymbol = '123123';

            if ($cellArray[$lineCells[$key][0]] !== 0) { // если первый символ НЕ джокер
                $winCellCounter = 1;
                $winSymbol = $cellArray[$lineCells[$key][0]];

                if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][1]] || $cellArray[$lineCells[$key][1]] === 0) {
                    $winCellCounter = 2;

                    if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][2]] || $cellArray[$lineCells[$key][2]] === 0) {
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][3]] || $cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][4]] || $cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                            }
                        }
                    }
                }
            }

            if ($cellArray[$lineCells[$key][0]] === 0) { // если первый символ джокер
                $winCellCounter = 1;

                if ($cellArray[$lineCells[$key][1]] === 0) {
                    $winCellCounter = 2;

                    if ($cellArray[$lineCells[$key][2]] === 0) {
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                                $winSymbol = 0;
                            } else {
                                if ($cellArray[$lineCells[$key][4]] === 10) {
                                    $winCellCounter = 5;
                                    $winSymbol = $cellArray[$lineCells[$key][4]];
                                } else {
                                    $winCellCounter = 4;
                                    $winSymbol = 0;
                                }
                            }
                        } else {
                            // выигрывают джокеры
                            if ($cellArray[$lineCells[$key][3]] === 8) {
                                $winCellCounter = 3;
                                $winSymbol = 0;
                            }

                            // выигрывает 5 символов
                            if ($cellArray[$lineCells[$key][4]] === 0 && $cellArray[$lineCells[$key][3]] !== 8) {
                                $winCellCounter = 5;
                                $winSymbol = $cellArray[$lineCells[$key][3]];
                            }
                            if ($cellArray[$lineCells[$key][3]] === $cellArray[$lineCells[$key][4]] && $cellArray[$lineCells[$key][3]] !== 8) {
                                $winCellCounter = 5;
                                $winSymbol = $cellArray[$lineCells[$key][3]];
                            }

                            // выигрывают 4 символа
                            if ($cellArray[$lineCells[$key][3]] !== $cellArray[$lineCells[$key][4]] && $cellArray[$lineCells[$key][4]] !== 0) {
                                if ($cellArray[$lineCells[$key][3]] === 4 || $cellArray[$lineCells[$key][3]] === 5 || $cellArray[$lineCells[$key][3]] === 10) {
                                    $winCellCounter = 4;
                                    $winSymbol = $cellArray[$lineCells[$key][3]];
                                } else {
                                    $winCellCounter = 3;
                                    $winSymbol = 0;
                                }
                            }
                        }
                    } else {
                        $winSymbol = $cellArray[$lineCells[$key][2]];
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][2]] === $cellArray[$lineCells[$key][3]] || $cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][2]] === $cellArray[$lineCells[$key][4]] || $cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                            }
                        }
                    }

                } else {
                    $winCellCounter = 2;
                    $winSymbol = $cellArray[$lineCells[$key][1]];

                    if ($cellArray[$lineCells[$key][1]] === $cellArray[$lineCells[$key][2]] || $cellArray[$lineCells[$key][2]] === 0) {
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][1]] === $cellArray[$lineCells[$key][3]] || $cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][1]] === $cellArray[$lineCells[$key][4]] || $cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                            }
                        }
                    }
                }
            }

            if ($winCellCounter > 2 && $winSymbol !== 2) {
                $winLinesData[] = [$key, $winSymbol, $winCellCounter];
            }
        }

        return $winLinesData;
    }

    /**
     * Получение значений для бонусной игры
     *
     * @param string $repeat
     * @return array|string
     */
    private function getRope($repeat = 'start')
    {
        $randС = rand(0, 100);
        if ($randС <= 50) {
            $counts = 10;
        } elseif ($randС > 50 && $randС <= 70) {
            $counts = 15;
        } elseif ($randС > 70 && $randС <= 85) {
            $counts = 20;
        } elseif ($randС > 85 && $randС <= 95) {
            $counts = 25;
        } elseif ($randС > 95) {
            $counts = 30;
        }

        $randM = rand(0, 100);
        if ($randM <= 50) {
            $mul = 1;
        } elseif ($randM > 50 && $randM <= 70) {
            $mul = 2;
        } elseif ($randM > 70 && $randM <= 85) {
            $mul = 3;
        } elseif ($randM > 85 && $randM <= 95) {
            $mul = 4;
        } elseif ($randM > 95) {
            $mul = 5;
        }

        if ($repeat === false) {
            $freeSpinData = ['count' => $counts, 'mul' => $mul, 'allWin' => 0, 'repeat' => true];
        } elseif ($repeat === 'start') {
            $freeSpinData = ['count' => $counts, 'mul' => $mul, 'allWin' => 0, 'repeat' => false];
        } else {
            return 'repeat error';
        }

        return $freeSpinData;
    }

    /**
     * Генерация значений для ячеек
     *
     * @return array $valueForCells
     */
    public function generatingValuesForCells($gameRules)
    {
        $allValuesArray = [];
        for ($i = 0; $i < 1000; $i++) {
            if ($_SESSION['bet'] < 0.25) {
                if ($i < 173) {
                    $allValuesArray[] = 8;
                } elseif ($i > 172 && $i < 341) {
                    $allValuesArray[] = 3;
                } elseif ($i > 340 && $i < 481) {
                    $allValuesArray[] = 7;
                } elseif ($i > 480 && $i < 611) {
                    $allValuesArray[] = 6;
                } elseif ($i > 610 && $i < 711) {
                    $allValuesArray[] = 11;
                } elseif ($i > 710 && $i < 781) {
                    $allValuesArray[] = 1;
                } elseif ($i > 780 && $i < 841) {
                    $allValuesArray[] = 9;
                } elseif ($i > 840 && $i < 876) {
                    $allValuesArray[] = 5;
                } elseif ($i > 875 && $i < 916) {
                    $allValuesArray[] = 4;
                } elseif ($i > 915 && $i < 946) {
                    $allValuesArray[] = 10;
                } elseif ($i > 945 && $i < 963) {
                    if ($_SESSION['freeSpinData'] != false) {
                        $allValuesArray[] = 8;
                    } else {
                        $allValuesArray[] = 2;
                    }
                } elseif ($i > 962 && $i < 1000) {
                    $allValuesArray[] = 0;
                }
            } else {
                if ($_SESSION['freeSpinData'] === false) {
                    if ($i < 164) {
                        $allValuesArray[] = 8;
                    } elseif ($i > 163 && $i < 332) {
                        $allValuesArray[] = 3;
                    } elseif ($i > 331 && $i < 472) {
                        $allValuesArray[] = 7;
                    } elseif ($i > 471 && $i < 602) {
                        $allValuesArray[] = 6;
                    } elseif ($i > 601 && $i < 702) {
                        $allValuesArray[] = 11;
                    } elseif ($i > 701 && $i < 742) {
                        $allValuesArray[] = 1;
                    } elseif ($i > 741 && $i < 782) {
                        $allValuesArray[] = 9;
                    } elseif ($i > 781 && $i < 847) {
                        $allValuesArray[] = 5;
                    } elseif ($i > 846 && $i < 901) {
                        $allValuesArray[] = 4;
                    } elseif ($i > 900 && $i < 956) {
                        $allValuesArray[] = 10;
                    } elseif ($i > 955 && $i < 982) {
                        $allValuesArray[] = 2;
                    } elseif ($i > 981 && $i < 1000) {
                        $allValuesArray[] = 0;
                    }
                } else {
                    if ($i < 263) {
                        $allValuesArray[] = 8;
                    } elseif ($i > 262 && $i < 361) {
                        $allValuesArray[] = 3;
                    } elseif ($i > 360 && $i < 551) {
                        $allValuesArray[] = 7;
                    } elseif ($i > 550 && $i < 641) {
                        $allValuesArray[] = 6;
                    } elseif ($i > 640 && $i < 701) {
                        $allValuesArray[] = 11;
                    } elseif ($i > 700 && $i < 841) {
                        $allValuesArray[] = 1;
                    } elseif ($i > 840 && $i < 901) {
                        $allValuesArray[] = 9;
                    } elseif ($i > 900 && $i < 966) {
                        $allValuesArray[] = 5;
                    } elseif ($i > 965 && $i < 986) {
                        $allValuesArray[] = 4;
                    } elseif ($i > 985 && $i < 997) {
                        $allValuesArray[] = 10;
                    } elseif ($i > 996 && $i < 1000) {
                        $allValuesArray[] = 0;
                    }
                }
            }

        }

        $valueForCells = [];

        for ($i = 0; $i < 15; $i++) {
            $valueForCells[] = $allValuesArray[rand(0, 999)];
        }

        $check = true;
        while ($check) {
            $newValueForCells = $this->updateValueForCells($valueForCells, $allValuesArray);

            if ($newValueForCells === $valueForCells) {
                $check = false;
            }

            $valueForCells = $newValueForCells;
        }

        if ($_SESSION['freeSpinData'] != false) {
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === $this->bonusValue) {
                    $valueForCells[$key] = 10;
                }
            }
        }

        return $valueForCells;
    }



    /**
     * Проверка наличия более 5 одинаковых символов
     *
     * @param $valueCounts
     * @return bool|array Возвращает false, либо значение символа, которого больше 5 и его кол-во
     */
    public function checkHave5Symbols($valueCounts)
    {
        foreach ($valueCounts as $value => $count) {
            if ($count > 5) {
                return [$value, $count];
            }
        }

        return false;
    }

    /**
     * Делаем так, чтобы в столбе не было три повторяющиеся символа. Убираем из сгенерированного набора лишние символы, чтобы их было < 5.
     *
     * @param $valueForCells
     * @param $symbol
     * @return array
     */
    public function updateValueForCells($valueForCells, $allValuesArray)
    {
        // задаем вероятность выпадения петухов
        // убираем лишних петухов если ставка < 50 центов
        if ($_SESSION['bet'] < 0.25) {
            $count = 0;
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === 2) {
                    $count += 1;
                }

                if ($count > 2) {
                    $valueForCells = [];

                    for ($i = 0; $i < 15; $i++) {
                        $valueForCells[] = $allValuesArray[rand(0, 999)];
                    }
                }
            }
        }

        // убираем петухов из фреспинов
        if (session('freeSpinData') != false) {
            $count = 0;
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === 2) {
                    $count += 1;
                }

                if ($count > 0) {
                    $valueForCells = [];

                    for ($i = 0; $i < 15; $i++) {
                        $valueForCells[] = $allValuesArray[rand(0, 999)];
                    }
                }
            }
        }

        for ($i = 0; $i < 13; $i += 3) {
            if ($valueForCells[$i] === $valueForCells[$i + 1] || $valueForCells[$i] === $valueForCells[$i + 2]) {
                $valueForCells = [];

                for ($i = 0; $i < 15; $i++) {
                    $valueForCells[] = $allValuesArray[rand(0, 999)];
                }
            } elseif ($valueForCells[$i + 1] === $valueForCells[$i + 2]) {
                $valueForCells = [];

                for ($i = 0; $i < 15; $i++) {
                    $valueForCells[] = $allValuesArray[rand(0, 999)];
                }
            }
        }


        $symbol = $this->checkHave5Symbols($this->getValueCouner($valueForCells));

        for ($i = 0; $i < $symbol[1] - 5; $i++) {
            foreach ($valueForCells as $key => $cell) {
                if ($cell === $symbol[0]) {
                    if ($cell < 5) {
                        $valueForCells[$key] = $cell + 2;
                    } else {
                        $valueForCells[$key] = $cell - 2;
                    }

                    break;
                }
            }
        }

        //проверка наличина несуществующих символов
        foreach ($valueForCells as $key => $cell) {
            if ($cell === -1 || $cell === 12) {
                $valueForCells[$key] = 5;
            }
        }

        // задаем вероятность выпадения фриспинов петухов
        if ($_SESSION['bet'] > 0.25) {
            $count = 0;
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === 2) {
                    $count += 1;
                }

                if ($count > 2) {
                    //if (rand(0, 1)) {
                    if (0) { //вероятность выпадения фриспинов не ограничевается
                        $valueForCells = [];

                        for ($i = 0; $i < 15; $i++) {
                            $valueForCells[] = $allValuesArray[rand(0, 999)];
                        }
                    }
                }
            }
        }

        // убираем лишних петухов если ставка < 50 центов
        if ($_SESSION['bet'] < 0.25) {
            $count = 0;
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === 2) {
                    $count += 1;
                }

                if ($count > 2) {
                    $valueForCells = [];

                    for ($i = 0; $i < 15; $i++) {
                        $valueForCells[] = $allValuesArray[rand(0, 999)];
                    }
                }
            }
        }

        // убираем петухов из фреспинов
        if (session('freeSpinData') != false) {
            $count = 0;
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === 2) {
                    $count += 1;
                }

                if ($count > 0) {
                    $valueForCells = [];

                    for ($i = 0; $i < 15; $i++) {
                        $valueForCells[] = $allValuesArray[rand(0, 999)];
                    }
                }
            }
        }

        return $valueForCells;
    }

    /**
     * Получаем кол-во каждого элемента в массиве
     *
     * @param $valueForCells
     * @return array
     */
    public function getValueCouner($valueForCells)
    {
        $valueCounts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($valueForCells as $cell) {
            switch ($cell) {
                case 0:
                    $valueCounts[0] += 1;
                    break;
                case 1:
                    $valueCounts[1] += 1;
                    break;
                case 2:
                    $valueCounts[2] += 1;
                    break;
                case 3:
                    $valueCounts[3] += 1;
                    break;
                case 4:
                    $valueCounts[4] += 1;
                    break;
                case 5:
                    $valueCounts[5] += 1;
                    break;
                case 6:
                    $valueCounts[6] += 1;
                    break;
                case 7:
                    $valueCounts[7] += 1;
                    break;
                case 8:
                    $valueCounts[8] += 1;
                    break;
                case 9:
                    $valueCounts[9] += 1;
                    break;
                case 10:
                    $valueCounts[10] += 1;
                    break;
                case 11:
                    $valueCounts[11] += 1;
                    break;
            }
        }

        return $valueCounts;
    }

    // проверяем на наличие особых значений запускающих бонусную игру. Если есть, то возвращаем 'freeSpin'
    public function checkBonusGame(array $cellValues, int $bonusValue)
    {
        $counter = 0;
        foreach ($cellValues as $value) {
            if ($value == $bonusValue) {
                $counter += 1;
            }
        }
        if ($counter > 2) {
            return 'bonus';
        }
    }

    // получаем общее кол-во значений на линии
    public function getCountCellValuesOnLine(array $cellNumbers, array $cellValues)
    {
        $countCellValuesOnLine = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($cellNumbers as $cellNumber) {
            switch ($cellValues[$cellNumber]) {
                case 0:
                    $countCellValuesOnLine[0] += 1;
                    break;
                case 1:
                    $countCellValuesOnLine[1] += 1;
                    break;
                case 2:
                    $countCellValuesOnLine[2] += 1;
                    break;
                case 3:
                    $countCellValuesOnLine[3] += 1;
                    break;
                case 4:
                    $countCellValuesOnLine[4] += 1;
                    break;
                case 5:
                    $countCellValuesOnLine[5] += 1;
                    break;
                case 6:
                    $countCellValuesOnLine[6] += 1;
                    break;
                case 7:
                    $countCellValuesOnLine[7] += 1;
                    break;
                case 8:
                    $countCellValuesOnLine[8] += 1;
                    break;
                case 9:
                    $countCellValuesOnLine[9] += 1;
                    break;
                case 10:
                    $countCellValuesOnLine[10] += 1;
                    break;
                case 11:
                    $countCellValuesOnLine[11] += 1;
                    break;
            }
        }

        return $countCellValuesOnLine;
    }


    public function getCountJokersOnLine(array $countCellValuesOnLine, int $jokerValue)
    {
        $count = $countCellValuesOnLine[$jokerValue];

        return $count;
    }

    // получаем наиболее стоящий выигрышь
    public function getMaxWinLineData($winLineDataArray, $gameRules)
    {
        $maxWinLineData = [0, 0, 0];
        $gameRules = json_decode($gameRules);
        $maxWinValue = 0;
        foreach ($winLineDataArray as $item) {
            // подсчет выигрыша по линии с учетом правил
            foreach ($gameRules->winRules as $key => $winArrRule) {
                if ($winArrRule[0] === $item[1] && $winArrRule[1] === $item[2]) {
                    $win = $winArrRule[2];
                }
            }

            // сравнение по выигрышам какое значение больше
            if ($win > $maxWinValue) {
                $maxWinValue = $win;
                $maxWinLineData = $item;
            }
        }

        return $maxWinLineData;
    }

    // отсекаем пустые наборы данных
    public function trimEmptyWinLineData(array $winLinesData)
    {
        $newWinLineData = [];
        foreach ($winLinesData as $item) {
            if ($item[2] !== 0) {
                $newWinLineData[] = $item;
            }
        }

        return $newWinLineData;
    }

    /**
     * Получение общей суммы выигрыша
     *
     * @return int $allWin
     */
    public function getAllWin($gameRules, $winLinesData, $betLine)
    {
        $allWin = 0;

        foreach (json_decode($gameRules)->winRules as $key => $rule) {
            foreach ($winLinesData as $winLineData) {
                if ($rule[0] == $winLineData[1]) {
                    if ($rule[1] == $winLineData[2]) {
                        $allWin += $betLine * $rule[2];
                    }
                }
            }
        }

        return $allWin;
    }

    public function correctionFreeSpinCounter()
    {
        // if (Config::get('app.mod') === 'dev' && $_SESSION['demo'] !== 'false') {
        //     $newCount = $_SESSION['freeSpinData']['count'] - 1;
        // } else {
        //     // проверить наличие записи о фриспинах для данного пользователя и данной иггры
        //     // $freeSpinDB = (new SavedDataBundle())
        //     //     ->where('user_id', '=', $_SESSION['userId'])
        //     //     ->where('game_id', '=', $_SESSION['gameId'])
        //     //     ->where('session_name', '=', $_SESSION['sessionName'])
        //     //     ->get()
        //     //     ->last();
        //
        //     if ($freeSpinDB) {
        //         if (json_decode($freeSpinDB->data)->freeSpinData->count <= 0) {
        //             $freeSpinDB->delete();
        //
        //             $this->setNewFreeSpinDB();
        //             $newCount = $_SESSION['freeSpinData']['count'] - 1;
        //         } else {
        //             $data = json_decode($freeSpinDB->data);
        //             $countDB = $data->freeSpinData->count;
        //             $newCount = $countDB - 1; // отнимаем 1 так как не учитывается предыдущая итерация
        //
        //             // исправление значения в сессии
        //             $freeSpinData = $_SESSION['freeSpinData'];
        //             if (($freeSpinData['count'] - 1) !== $newCount) {
        //                 $freeSpinData['count'] = $newCount;
        //                 $_SESSION['freeSpinData'] = $freeSpinData;
        //             }
        //
        //             $data->freeSpinData->count = $newCount;
        //
        //             $freeSpinDB->data = json_encode($data);
        //             $freeSpinDB->save();
        //         }
        //     } else {
        //         $this->setNewFreeSpinDB();
        //         $newCount = $_SESSION['freeSpinData']['count'] - 1;
        //     }
        // }

        $this->setNewFreeSpinDB();
        $newCount = $_SESSION['freeSpinData']['count'] - 1;

        return $newCount;
    }

    public function setNewFreeSpinDB()
    {
        // $freeSpinData = $_SESSION['freeSpinData'];
        // $freeSpinData['count'] = $freeSpinData['count'] - 1;
        // $freeSpinDB = new SavedDataBundle();
        // $freeSpinDB->user_id = 1;
        // $freeSpinDB->game_id = 2;
        // $freeSpinDB->session_name = $_SESSION['sessionName'];
        // $freeSpinDB->data = json_encode(['freeSpinData' => $freeSpinData]);
        // $freeSpinDB->save();
    }

    public function makeInfoForApi($info)
    {
        $newResult = [];
        foreach ($info as $key => $value) {
            switch ($value) {
                case 0:
                    $newResult[] = 'Diablo';
                    break;
                case 1:
                    $newResult[] = 'Nopal';
                    break;
                case 2:
                    $newResult[] = 'Gallo';
                    break;
                case 3:
                    $newResult[] = 'Borracho';
                    break;
                case 4:
                    $newResult[] = 'Sirena';
                    break;
                case 5:
                    $newResult[] = 'Corazon';
                    break;
                case 6:
                    $newResult[] = 'Muerte';
                    break;
                case 7:
                    $newResult[] = 'Alacran';
                    break;
                case 8:
                    $newResult[] = 'Sol';
                    break;
                case 9:
                    $newResult[] = 'Mundo';
                    break;
                case 10:
                    $newResult[] = 'Chalupa';
                    break;
                case 11:
                    $newResult[] = 'Calavera';
                    break;

                default:
                    $newResult[] = '';
                    break;
            }
        }

        return $newResult;
    }

    private function getJackpotNoteNumber()
    {
        // для демо и дев режимов используется отдельная запись в БД
        if (isset($_SESSION['test'])) {
            if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false && $_SESSION['test'] === false) {
                $jackpotNoteNumber = 1;
            } else {
                $jackpotNoteNumber = 2;
            }
        } else {
            if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
                $jackpotNoteNumber = 1;
            } else {
                $jackpotNoteNumber = 2;
            }
        }

        return $jackpotNoteNumber;
    }

    public function getWinJackpot()
    {
        if ($_SESSION['demo'] === '0') {
            $_SESSION['demo'] = false;
        }
        if ($_SESSION['demo'] === 'false') {
            $_SESSION['demo'] = false;
        }
        if ($_SESSION['demo'] === true) {
            $_SESSION['demo'] = true;
        }
        if ($_SESSION['demo'] === 'true') {
            $_SESSION['demo'] = true;
        }

        // у пользователя может быть накомлен джекпот в demo игре, по этому делается проверка
        if ($_SESSION['demo'] === false) {
            $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->where('demo', '=', 'false')->get()->first();
        } else {
            $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->where('demo', '=', 'true')->get()->first();
        }

        $_SESSION['balance'] += round($userJackpot->jackpot_value, 2);

        $eventID = Uuid::generate()->string;

        // получение значения jackpotType для bridgeApi
        $jackpotType = '';
        switch ($userJackpot->jackpot_type) {
            case 'mini':
                $jackpotType = 'MINI';
                break;
            case 'minor':
                $jackpotType = 'MINOR';
                break;
            case 'major':
                $jackpotType = 'MAJOR';
                break;
            case 'big_daddy':
                $jackpotType = 'MAXI';
                break;

            default:
                break;
        }

        // отправка запроса к апи
        $platformId = 1;
        if (isset($_GET['platformId'])) {
            $platformId = $_GET['platformId'];
        }
        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            $moveFundsRequestData = array(
                'token' => $_SESSION['token'],
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'direction' => 'credit',
                'platformId' => $platformId,
                'eventType' => 'Jackpot',
                'amount' => round($userJackpot->jackpot_value, 2),
                'extraInfo' => [
                    'result' => [$jackpotType]
                ],
                'eventID' => $eventID
            );
            $responseMoveFunds = (new BridgeApiService())
                ->moveFunds($moveFundsRequestData, 'jackpot');
        }

        // занесение данных в лог для проверки бага

        $platformId = 1;
        if (isset($_GET['platformId'])) {
            $platformId = $_GET['platformId'];
        }
        $log = new Log;
        $log->type = 'work';
        $log->data = json_encode(array(
            'userId' => $_SESSION['userId'],
            'gameId' => $_SESSION['gameId'],
            'allWin' => round($userJackpot->jackpot_value, 2),
            'balance' => round($_SESSION['balance'], 2),
            'direction' => 'credit',
            'platformId' => $platformId,
            'jackpotType' => $userJackpot->jackpot_type,
            'amount' => round($userJackpot->jackpot_value, 2),
            'eventID' => $eventID,
            'extraInfo' => ['result' => [$jackpotType]],
            'action' => 'jackpot'
        ));
        $log->save();

        // запись джекпота в статистику
        // выбор записи в бд в зависимости от demo режима
        $demo = 'no';
        if (isset($_SESSION['test'])) {
            if ($_SESSION['test'] === true) {
                $demo = 'yes';
            }
        }
        if ($_SESSION['demo'] === true) {
            $demo = 'yes';
        }

        $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', $demo)->get()->first();
        $stat->sum_win += $userJackpot->jackpot_value;
        $stat->total_jackpot_winning += $userJackpot->jackpot_value;
        $stat->number_of_jackpots += 1;
        $stat->save();

        $userJackpot->delete(); // удаление записи о наличии не начисленного джекпота у пользователя

        // отправка по сокету на фронт данных о победителе
        // $user = (new Session)->where('userId', '=', $_SESSION['userId'])->get()->first();
        // $data = [
        //     'topic_id' => 'onJackpotNewWinner',
        //     'data' => ['win_value' => $_SESSION['allWin'], 'nickname' => $user->nickname]
        // ];
        // \App\Classes\Socket\Pusher::sentDataToServer($data);

        return round($userJackpot->jackpot_value, 2);
    }

    public function sendBetPlacing()
    {
        try {
            if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
                if ($_SESSION['freeSpinData'] === false) {
                    // создание eventId в случае если нет симуляции
                    if (!isset($_SESSION['eventID'])) {
                        $_SESSION['eventID'] = 'simulationEvent';
                    }
                    if ($_SESSION['eventID'] !== false) {
                        if ($_SESSION['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false) {
                            $_SESSION['eventID'] = Uuid::generate()->string;
                            $eventID = $_SESSION['eventID'];
                        } else {
                            $eventID = $_SESSION['eventID'];
                        }
                    } else {
                        $_SESSION['eventID'] = Uuid::generate()->string;
                        $eventID = $_SESSION['eventID'];
                    }
                }

                if ($_SESSION['freeSpinData']['mul'] === null) {
                    $_SESSION['freeSpinData'] = false;
                }

                // отправка события о снятии средств
                if ($_SESSION['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false && $_SESSION['demo'] == false) {
                    $data = Jackpot::firstOrFail()->toArray();

                    $platformId = 1;
                    if (isset($_GET['platformId'])) {
                        $platformId = $_GET['platformId'];
                    }
                    $requestData = array(
                        'token' => $_SESSION['token'],
                        'userId' => $_SESSION['userId'],
                        'gameId' => $_SESSION['gameId'],
                        'direction' => 'debit',
                        'platformId' => $platformId,
                        'eventType' => 'BetPlacing',
                        'amount' => $_SESSION['bet'],
                        'extraInfo' => [
                            'selected' => [$_SESSION['linesInGame']],
                            'jackpotAmount' => $data['big_daddy']
                        ],
                        'eventID' => $eventID
                    );

                    // занесение данных в лог для проверки бага
                    $log = new Log;
                    $log->type = 'work';
                    $platformId = 1;
                    if (isset($_GET['platformId'])) {
                        $platformId = $_GET['platformId'];
                    }
                    $log->data = json_encode(array(
                        'userId' => $_SESSION['userId'],
                        'gameId' => $_SESSION['gameId'],
                        'direction' => 'debit',
                        'platformId' => $platformId,
                        'eventType' => 'BetPlacing',
                        'amount' => $_SESSION['bet'],
                        'eventID' => $eventID
                    ));
                    $log->save();

                    // получение ответа от slot.pantera
                    $responseMoveFunds = (new BridgeApiService())->moveFunds($requestData);

                    // получение баланса
                    $_SESSION['balance'] = (float) json_decode($responseMoveFunds)->balance;

                    // проверка активности сессии на сервер с мост-апи (http://slot.pantera.co.ua)
                    $checkApiSession = json_decode($responseMoveFunds);
                    if (!isset($checkApiSession->status)) {
                        throw new BetPlacingAbortException($linesInGame * $betLine, $linesInGame);
                    }
                    if ($checkApiSession->status === false) {
                        throw new BetPlacingAbortException($linesInGame * $betLine, $linesInGame);
                    }
                }
            }
        } catch (BetPlacingAbortException $e) {
            //$e->sendBetPlacingAbort();
        } catch (BalanceException $e) {
            //return '{"status":"false"}';
        } catch (EmptySessionDataException $e) {
            if (Config::get('app.mod') === 'dev') {
                $e->setDevSessionData();
            } else {
                return response()->json('{"status":"false"}');
            }
        } catch (UnauthenticatedException $e) {
            //return '{"status":"false"}';
        }
    }

    /**
    * Запись статистики результатов хода
    */
    public function writeStatistics($spinResultData)
    {
        if ($_SESSION['demo'] === '0') {
            $_SESSION['demo'] = false;
        }
        if ($_SESSION['demo'] === 'false') {
            $_SESSION['demo'] = false;
        }

        // выбор записи в бд в зависимости от demo режима
        $demo = 'no';
        if (isset($_SESSION['test'])) {
            if ($_SESSION['test'] === true) {
                $demo = 'yes';
            }
        }
        if ($_SESSION['demo'] === true) {
            $demo = 'yes';
        }

        $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', $demo)->get()->first();

        $money_returned_on_main_game = 0;
        $money_returned_on_bonus_game = 0;
        $money_returned_on_jackpot_game = 0;

        // подсчет кол-ва выигрышных спинов
        if ($_SESSION['freeSpinData'] === false || $_SESSION['checkFreeSpin'] === 'freeSpin') {
            if ($spinResultData['allWin'] > 0) {
                $stat->number_of_winning_spins += 1;
            } else {
                $stat->number_of_losing_spins += 1;
            }

            $stat->iteration_count += 1;
        }


        // основная игра
        if ($_SESSION['freeSpinData'] == false) {
            $stat->money_returned_on_main_game += $spinResultData['allWin'];
            $stat->sum_win += $spinResultData['allWin'];
        }

        // последний ход фриспинов
        if ($_SESSION['freeSpinData']) {
            if ($_SESSION['freeSpinData']['count'] === 0) {
                $stat->money_returned_on_bonus_game += $_SESSION['freeSpinData']['allWin'];
                $stat->sum_win += $_SESSION['freeSpinData']['allWin'];
                $stat->number_of_bonus_game += 1;
            }
        }

        // основная игра и джекпот
        if ($_SESSION['freeSpinData'] === false || $_SESSION['checkFreeSpin'] === 'freeSpin') {
            $stat->sum_bet += $_SESSION['bet'];
        }

        $stat->save();
    }

    public function writeStatisticsAfterDouble()
    {
        if ($_SESSION['demo'] === '0') {
            $_SESSION['demo'] = false;
        }
        if ($_SESSION['demo'] === 'false') {
            $_SESSION['demo'] = false;
        }

        // выбор записи в бд в зависимости от demo режима
        $demo = 'no';
        if (isset($_SESSION['test'])) {
            if ($_SESSION['test'] === true) {
                $demo = 'yes';
            }
        }
        if ($_SESSION['demo'] === true) {
            $demo = 'yes';
        }


        $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', $demo)->get()->first();

        if ($_SESSION['allWin'] > 0) {
            $stat->sum_win += $_SESSION['doubleLose'];
        } else {
            $stat->sum_win -= $_SESSION['doubleLose'];
        }

        $stat->save();
    }

    public function importPercentagesEG(Request $request)
    {

    }
}
