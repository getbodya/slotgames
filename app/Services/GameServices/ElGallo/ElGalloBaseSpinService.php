<?php

namespace App\Services\GameServices\ElGallo;

use App\Models\V2Game;
use App\Models\Log;
use Illuminate\Support\Facades\Config;
use App\Services\BridgeApiService;

/**
 * Абстрактный класс для логики EG
 */
abstract class ElGalloBaseSpinService
{
    /**
     * Генерация рандомных значений для игрового поля
     */
    public function generatingValuesForCellsV2()
    {
        $cellsRows = [[],[],[],[],[]];
        for ($i=0; $i < 5; $i++) { // уровень отдельного барабана
            $cellRow = $cellsRows[$i];

            for ($k=0; $k < 3; $k++) { // уроверь отдельной ячейки в барабане
                $cellRow[$k] = $this->getCellValue($cellRow, $i);
            }

            $cellsRows[$i] = $cellRow;
        }

        $valuesForCells = []; // заполнение итогового массива
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 3; $k++) {
                $valuesForCells[] = $cellsRows[$i][$k];
            }
        }

        // удаление лишних символов из итогового массива значений ячеек
        $valuesForCells = $this->excludeGalloIfNotEnoughBet($valuesForCells);

        return $valuesForCells;
    }

    /**
     * Удаление лишних символов из итогового массива значений ячеек
     *
     * @param array $valuesForCells
     * @return array
     */
    public function excludeGalloIfNotEnoughBet(array $valuesForCells): array
    {
        if ($_SESSION['bet'] < 0.25) {
            $countBonusSymbol = 0;
            for ($i=0; $i < 15; $i++) {
                if ($valuesForCells[$i] === 2) {
                    $countBonusSymbol += 1;
                }
            }

            if ($countBonusSymbol > 2) {
                return $this->generatingValuesForCellsV2();
            }
        }

        return $valuesForCells;
    }

    /**
     *  Получение значения для отдельной ячейки в барабане с учетом правил
     */
    public function getCellValue($cellRow, $i)
    {
        $percentArr = $this->getPercentArr($i);
        $rand = rand(0, 99999999);

        $cellValue = '123123';

        foreach ($percentArr as $key => $percentRang) { // поиск выпавшего значения ячейки
            if ($rand >= $percentRang[0] && $rand < $percentRang[1]) {
                $cellValue = $key;
                break;
            }
        }

        if ($cellValue === '123123') {
            dd($cellValue);
        }

        $cellValue = $this->excludeSameCharactersInOneDrum($cellValue, $cellRow, $i);

        return $cellValue;
    }

    /**
     * Исключение возможности выпадения на одном барабане двух одинаковых символов
     */
    public function excludeSameCharactersInOneDrum($cellValue, $cellRow, $i)
    {
        if (!in_array($cellValue, $cellRow) && $cellValue !== null) { // проверка есть ли уже в ряде такая ячейка
            return $cellValue;
        } else {
            return $this->getCellValue($cellRow, $i);
        }
    }

    /**
     * Метод для получения массива диапозонов определяющих вероятности выпадения
     * для элементов
     */
    public function getPercentArr($i)
    {
        if (!isset($_SESSION['columnArrPercentsMain50']) || !isset($_SESSION['columnArrPercentsFree50'])) {
            $game = (new V2Game)->where('name', '=', 'elgallo')->get()->first();
            $data = unserialize($game->data);

            if ($data['percentages']['type'] === 'numbers') {
                // запись процентов в сессию
                $percentagesMainGameJson50 = '{"columnArrPercentsMain50":[';
                for ($l=0; $l < 5; $l++) {
                    $percentagesMainGameJson50 .= '[';
                    for ($k=0; $k < 12; $k++) {
                        if ($k === 11) {
                            $percentagesMainGameJson50 .= $data['percentages']['percentagesMainGame50'][$l][$k];
                        } else {
                            $percentagesMainGameJson50 .= $data['percentages']['percentagesMainGame50'][$l][$k] . ',';
                        }
                    }

                    if ($l === 4) {
                        $percentagesMainGameJson50 .= ']';
                    } else {
                        $percentagesMainGameJson50 .= '],';
                    }
                }
                $percentagesMainGameJson50 .= ']}';
                $_SESSION['columnArrPercentsMain50'] = json_decode($percentagesMainGameJson25);

                $percentagesMainGameJson25 = '{"columnArrPercentsMain25":[';
                for ($l=0; $l < 5; $l++) {
                    $percentagesMainGameJson25 .= '[';
                    for ($k=0; $k < 12; $k++) {
                        if ($k === 11) {
                            $percentagesMainGameJson25 .= $data['percentages']['percentagesMainGame25'][$l][$k];
                        } else {
                            $percentagesMainGameJson25 .= $data['percentages']['percentagesMainGame25'][$l][$k] . ',';
                        }
                    }

                    if ($l === 4) {
                        $percentagesMainGameJson25 .= ']';
                    } else {
                        $percentagesMainGameJson25 .= '],';
                    }
                }
                $percentagesMainGameJson25 .= ']}';
                $_SESSION['columnArrPercentsMain25'] = json_decode($percentagesMainGameJson25);

                $percentagesMainGameJson1 = '{"columnArrPercentsMain1":[';
                for ($l=0; $l < 5; $l++) {
                    $percentagesMainGameJson1 .= '[';
                    for ($k=0; $k < 12; $k++) {
                        if ($k === 11) {
                            $percentagesMainGameJson1 .= $data['percentages']['percentagesMainGame1'][$l][$k];
                        } else {
                            $percentagesMainGameJson1 .= $data['percentages']['percentagesMainGame1'][$l][$k] . ',';
                        }
                    }

                    if ($l === 4) {
                        $percentagesMainGameJson1 .= ']';
                    } else {
                        $percentagesMainGameJson1 .= '],';
                    }
                }
                $percentagesMainGameJson1 .= ']}';
                $_SESSION['columnArrPercentsMain1'] = json_decode($percentagesMainGameJson1);

                $percentagesFreeGameJson50 = '{"columnArrPercentsFree50":[';
                for ($l=0; $l < 5; $l++) {
                    $percentagesFreeGameJson50 .= '[';
                    for ($k=0; $k < 12; $k++) {
                        if ($k === 11) {
                            $percentagesFreeGameJson50 .= $data['percentages']['percentagesFreeGame50'][$l][$k];
                        } else {
                            $percentagesFreeGameJson50 .= $data['percentages']['percentagesFreeGame50'][$l][$k] . ',';
                        }
                    }
                    if ($l === 4) {
                        $percentagesFreeGameJson50 .= ']';
                    } else {
                        $percentagesFreeGameJson50 .= '],';
                    }
                }
                $percentagesFreeGameJson50 .= ']}';
                $_SESSION['columnArrPercentsFree50'] = json_decode($percentagesFreeGameJson50);

                $percentagesFreeGameJson25 = '{"columnArrPercentsFree25":[';
                for ($l=0; $l < 5; $l++) {
                    $percentagesFreeGameJson25 .= '[';
                    for ($k=0; $k < 12; $k++) {
                        if ($k === 11) {
                            $percentagesFreeGameJson25 .= $data['percentages']['percentagesFreeGame25'][$l][$k];
                        } else {
                            $percentagesFreeGameJson25 .= $data['percentages']['percentagesFreeGame25'][$l][$k] . ',';
                        }
                    }
                    if ($l === 4) {
                        $percentagesFreeGameJson25 .= ']';
                    } else {
                        $percentagesFreeGameJson25 .= '],';
                    }
                }
                $percentagesFreeGameJson25 .= ']}';
                $_SESSION['columnArrPercentsFree25'] = json_decode($percentagesFreeGameJson25);

                $percentagesFreeGameJson1 = '{"columnArrPercentsFree1":[';
                for ($l=0; $l < 5; $l++) {
                    $percentagesFreeGameJson1 .= '[';
                    for ($k=0; $k < 12; $k++) {
                        if ($k === 11) {
                            $percentagesFreeGameJson1 .= $data['percentages']['percentagesFreeGame1'][$l][$k];
                        } else {
                            $percentagesFreeGameJson1 .= $data['percentages']['percentagesFreeGame1'][$l][$k] . ',';
                        }
                    }
                    if ($l === 4) {
                        $percentagesFreeGameJson1 .= ']';
                    } else {
                        $percentagesFreeGameJson1 .= '],';
                    }
                }
                $percentagesFreeGameJson1 .= ']}';
                $_SESSION['columnArrPercentsFree1'] = json_decode($percentagesFreeGameJson1);
            }

            // в случае если при экспорте новых процентов был использован json
            if ($data['percentages']['type'] === 'json') {
                $_SESSION['columnArrPercentsMain50'] = json_decode($data['percentages']['jsonMainGame50']);
                $_SESSION['columnArrPercentsMain25'] = json_decode($data['percentages']['jsonMainGame25']);
                $_SESSION['columnArrPercentsMain1'] = json_decode($data['percentages']['jsonMainGame1']);
                $_SESSION['columnArrPercentsFree50'] = json_decode($data['percentages']['jsonFreeGame50']);
                $_SESSION['columnArrPercentsFree25'] = json_decode($data['percentages']['jsonFreeGame25']);
                $_SESSION['columnArrPercentsFree1'] = json_decode($data['percentages']['jsonFreeGame1']);
            }

        }


         $columnArrPercentsMain50 = $_SESSION['columnArrPercentsMain50'];
         $columnArrPercentsMain25 = $_SESSION['columnArrPercentsMain25'];
         $columnArrPercentsMain1 = $_SESSION['columnArrPercentsMain1'];
         $columnArrPercentsFree50 = $_SESSION['columnArrPercentsFree50'];
         $columnArrPercentsFree25 = $_SESSION['columnArrPercentsFree25'];
         $columnArrPercentsFree1 = $_SESSION['columnArrPercentsFree1'];

         // выбор массива с процентами в зависимости от ставки
         if ($_SESSION['bet'] < 0.25) {
             $columnArrPercentsMain = $columnArrPercentsMain1;
             $columnArrPercentsFree = $columnArrPercentsFree1;
         } elseif ($_SESSION['bet'] >= 0.25 && $_SESSION['bet'] < 0.50) {
             $columnArrPercentsMain = $columnArrPercentsMain25;
             $columnArrPercentsFree = $columnArrPercentsFree25;
         } elseif ($_SESSION['bet'] >= 0.50) {
             $columnArrPercentsMain = $columnArrPercentsMain50;
             $columnArrPercentsFree = $columnArrPercentsFree50;
         }

         // конвертация json данных в формат который понимает код
         $percentArr = [];
         $rangArr = [0,0,0,0,0,0,0,0,0,0,0,0];
         if ($_SESSION['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false) {
             foreach ($columnArrPercentsMain->columnArrPercentsMain[$i] as $key => $value) {
                 $percentArr[$key] = $value * 1000000;
             }

             foreach ($percentArr as $key => $value) {
                 if ($key > 0) {
                     $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
                 } else {
                     $rangArr[$key] = [0, $value];
                 }

                 if ($key === 11) {
                     $rangArr[$key] = [$rangArr[$key - 1][1], 100000000];
                 }
             }
         } else {
             foreach ($columnArrPercentsFree->columnArrPercentsFree[$i] as $key => $value) {
                 $percentArr[$key] = $value * 1000000;
             }

             foreach ($percentArr as $key => $value) {
                 if ($key > 0) {
                     $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
                 } else {
                     $rangArr[$key] = [0, $value];
                 }

                 if ($key === 11) {
                     $rangArr[$key] = [$rangArr[$key - 1][1], 100000000];
                 }
             }
         }

         return $rangArr;
    }

    public function getDoubleResultData($selectedCard)
    {
        $_SESSION['doubleLose'] = round($_SESSION['allWin'], 2);

        if ($_SESSION['checkStartDouble'] === false) {
            $_SESSION['savedStartAllWinForDouble'] = round($_SESSION['allWin'], 2);
        }

        // вычисление результата хода
        $win = $this->getResultDoubleGame($selectedCard);

        // проверка превышения максимального выигрыша
        $win = $this->checkExcessWinnings($selectedCard, $win);

        // изменение выигрыша
        $this->changeAllWinAfterDouble($selectedCard, $win);

        // получение рандомного ответа в соответствии с рандомно полученным результатом
        $dcard = $this->getRandomCardValue($selectedCard, $win);

        // отправка данных в статистику
        $this->writeStatisticsAfterDouble();

        $responseData = [
            'win' => $win,
            'allWin' => $_SESSION['allWin'],
            'dcard' => $dcard,
            'count' => $_SESSION['cardGameIteration'],
            'selected_for_api' => [$this->getCardSuit($selectedCard)],
            'result_for_api' => [$this->getCardSuit($dcard)],
        ];

        // отправка запроса к bridgeApi
        $this->sendDoubleMoveFunds($responseData);

        return $responseData;
    }

    /**
     * Получение рандомного значения карты диллера в соответствии с результатом хода
     */
    public function getRandomCardValue(string $selectedCard, bool $win) : string
    {
        if ($selectedCard === 'red' || $selectedCard === 'black') { // если выбран цвет цвет
            if ($win === true) { // выирышь
                if ($selectedCard === 'red') {
                    $array = ['c', 'b'];
                    $dcard = $array[array_rand($array)];
                }

                if ($selectedCard === 'black') {
                    $array = ['p', 'k'];
                    $dcard = $array[array_rand($array)];
                }
            } else { // проигрышь
                if ($selectedCard === 'red') {
                    $array = ['k', 'p'];
                    $dcard = $array[array_rand($array)];
                }

                if ($selectedCard === 'black') {
                    $array = ['c', 'b'];
                    $dcard = $array[array_rand($array)];
                }
            }
        } else { // если выбрана масть
            if ($win === true) { // выигрышь
                if ($selectedCard === 'c') {
                    $dcard = 'c';
                }

                if ($selectedCard === 'b') {
                    $dcard = 'b';
                }

                if ($selectedCard === 'k') {
                    $dcard = 'k';
                }

                if ($selectedCard === 'p') {
                    $dcard = 'p';
                }
            } else {
                // проигрышь
                if ($selectedCard === 'c') {
                    $array = ['k', 'p', 'b'];
                    $dcard = $array[array_rand($array)];
                }

                if ($selectedCard === 'k') {
                    $array = ['c', 'p', 'b'];
                    $dcard = $array[array_rand($array)];
                }

                if ($selectedCard === 'p') {
                    $array = ['c', 'k', 'b'];
                    $dcard = $array[array_rand($array)];
                }

                if ($selectedCard === 'b') {
                    $array = ['c', 'k', 'p'];
                    $dcard = $array[array_rand($array)];
                }
            }
        }

        $_SESSION['dcard'] = $dcard;

        return $dcard;
    }

    /**
     * Изменение общего выигрыша в соответствии с резельтатом хода
     */
    public function changeAllWinAfterDouble(string $selectedCard, bool $win) : void
    {
        if ($selectedCard === 'red' || $selectedCard === 'black') { // если выбран цвет цвет
            if ($win === true) {
                // выигрышь
                $_SESSION['cardGameIteration'] = $_SESSION['cardGameIteration'] + 1;

                $_SESSION['allWin'] = round($_SESSION['allWin'] * 2, 2);
            } else {
                // проигрышь
                $_SESSION['cardGameIteration'] = 0;
                $_SESSION['allWin'] = 0;
            }
        } else {
            if ($win === true) {
                // выигрышь
                $_SESSION['cardGameIteration'] = $_SESSION['cardGameIteration'] + 1;

                $_SESSION['allWin'] = round($_SESSION['allWin'] * 4, 2);
            } else {
                $_SESSION['cardGameIteration'] = 0;
                $_SESSION['allWin'] = 0;
            }
        }
    }

    /**
     * Проверка превышения максимального выигрыша
     */
    public function checkExcessWinnings(string $selectedCard, bool $win) : bool
    {
        // получение выигрыша
        $expectedAllWin = 0;
        if ($selectedCard === 'red' || $selectedCard === 'black') {
            $expectedAllWin = $_SESSION['allWin'] * 2;
        } else {
            $expectedAllWin = $_SESSION['allWin'] * 4;
        }

        if ($expectedAllWin >= 2500) {
            if ($selectedCard === 'c' || $selectedCard === 'b') {
                $array = ['k', 'p'];
                $dcard = $array[array_rand($array)];
                $_SESSION['dcard'] = $dcard;
            }

            if ($selectedCard === 'k' || $selectedCard === 'p') {
                $array = ['c', 'b'];
                $dcard = $array[array_rand($array)];
                $_SESSION['dcard'] = $dcard;
            }

            $_SESSION['cardGameIteration'] = 0;
            $_SESSION['allWin'] = 0;
            $win = false;
        }

        return $win;
    }

    /**
     * Получение рандомного результата исхода игры на удвоение
     *
     * @param string $selectedCard значение выбранное пользователем
     * @param int $rand рандомное число от которого зависит результат хода
     * @return bool выигрышь
     */
    public function getResultDoubleGame(string $selectedCard, int $rand = 0) : bool
    {
        // проверка максимально допустимого кол-ва выигрышей
        if ($_SESSION['cardGameIteration'] >= 5) {
            return false;
        }

        if ($rand === 0) {
            $rand = rand(1, 100);
        }

        if ($selectedCard === 'red' || $selectedCard === 'black') {
            if ($rand > 55) { // 55
                $win = true;
            } else {
                $win = false;
            }
        } else {
            if ($rand > 80) { // 80
                $win = true;
            } else {
                $win = false;
            }
        }

        return $win;
    }

    /**
     * Отправка запроса moveFunds к bridgeApi с данными о результате игры на удвоение
     *
     * @param array $responseData массив данных с результатами игры на удвоение
     * @return void
     */
    public function sendDoubleMoveFunds(array $responseData) : void
    {
        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            // отправка moveFunds с учетом результата
            $platformId = 1;
            if (isset($_GET['platformId'])) {
                $platformId = $_GET['platformId'];
            }
            $data = (new BridgeApiService())->moveFunds(array(
                    'token' => $_SESSION['token'],
                    'userId' => $_SESSION['userId'],
                    'gameId' => $_SESSION['gameId'],
                    'direction' => '',
                    'platformId' => $platformId,
                    'eventType' => 'Raise',
                    'eventID' => $_SESSION['eventID'],
                    'amount' => $_SESSION['allWin'],
                    'extraInfo' => ['selected' => $responseData['selected_for_api'],
                    'result' => $responseData['result_for_api'],
                ],
                'allWin' => $responseData['allWin'],
                'allLose' => $_SESSION['doubleLose']
            ), 'end_of_double');

            // занесение данных в лог для проверки бага
            $log = new Log;
            $log->type = 'work';
            $log->data = json_encode(array(
                    'userId' => $_SESSION['userId'],
                    'gameId' => $_SESSION['gameId'],
                    'eventType' => 'Raise',
                    'amount' => $_SESSION['allWin'],
                    'balance' => $_SESSION['balance'],
                    'allWin' => $responseData['allWin'],
                    'allLose' => $_SESSION['doubleLose'],
                    'action' => 'double',
                    'cardGameIteration' => $_SESSION['cardGameIteration']
            ));
            $log->save();
        }
    }
}
