<?php

namespace App\Services\GameServices\ElGallo;

class ElGalloSpinServiceVFast
{
    public $betLine = 1; // центы
    public $bet = 0.1;
    public $linesInGame = 25;

    /**
     * Получение результатов игры
     */
    public function getResultData()
    {
        $start = microtime(true);
        $time1 = microtime(true) - $start;

        $allWin = 0;
        $time2 = microtime(true) - $start;
        for ($i=0; $i < 1500000; $i++) {
            $time3 = microtime(true) - $start;
            $cellArray = $this->generatingValuesForCellsV2();
            $time4 = microtime(true) - $start;
            $winLinesData = $this->getWinLinesData($cellArray, $this->linesInGame);
            $time5 = microtime(true) - $start;
            $allWin += $this->getAllWin($winLinesData, $this->bet);
            $time6 = microtime(true) - $start;

            dd([
                '1' => $time1,
                '2' => $time2,
                '3' => $time3,
                '4' => $time4,
                '5' => $time5,
                '6' => $time6
            ]);
        }

        return [
            'PAYOUT' => 100 / 1500000 * 25 * 1 * $allWin,
            'execution_time' => microtime(true) - $start,
        ];
    }

    /**
     * Получение выигрышного символа и кол-ва выигрышных символов на линии
     * @return [значение, кол-во значений]
     */
    public function getWinLinesData(array $cellArray, int $linesInGame) {
        // $winLineData = [значение, кол-во значений]
        $winLinesData = [];

        $lineCells = [[1,4,7,10,13],[0,3,6,9,12],[2,5,8,11,14],[0,4,8,10,12],
        [2,4,6,10,14],[1,5,8,11,13],[1,3,6,9,13],[1,4,8,10,13],[1,4,6,10,13],
        [0,3,7,9,12],[2,5,7,11,14],[0,4,7,10,12],[2,4,7,10,14],[0,4,6,10,12],
        [2,4,8,10,14],[1,3,7,9,13],[1,5,7,11,13],[2,3,8,9,14],[0,5,6,11,12],
        [1,3,8,9,13],[1,5,6,11,13],[0,3,7,11,14],[2,5,7,9,12],[0,4,7,10,14],
        [2,4,7,10,12]];

        for ($key = 0; $key < $linesInGame; $key++) {
            $lineData = [0, 0]; // ['выигрышный символ', 'кол-во сииволов']
            $winCellCounter = 0;
            $winSymbol = '123123';

            if ($cellArray[$lineCells[$key][0]] !== 0) { // если первый символ НЕ джокер
                $winCellCounter = 1;
                $winSymbol = $cellArray[$lineCells[$key][0]];

                if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][1]] || $cellArray[$lineCells[$key][1]] === 0) {
                    $winCellCounter = 2;

                    if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][2]] || $cellArray[$lineCells[$key][2]] === 0) {
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][3]] || $cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][0]] === $cellArray[$lineCells[$key][4]] || $cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                            }
                        }
                    }
                }
            }

            if ($cellArray[$lineCells[$key][0]] === 0) { // если первый символ джокер
                $winCellCounter = 1;

                if ($cellArray[$lineCells[$key][1]] === 0) {
                    $winCellCounter = 2;

                    if ($cellArray[$lineCells[$key][2]] === 0) {
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                                $winSymbol = 0;
                            } else {
                                if ($cellArray[$lineCells[$key][4]] === 10) {
                                    $winCellCounter = 5;
                                    $winSymbol = $cellArray[$lineCells[$key][4]];
                                } else {
                                    $winCellCounter = 4;
                                    $winSymbol = 0;
                                }
                            }
                        } else {
                            // выигрывают джокеры
                            if ($cellArray[$lineCells[$key][3]] === 8) {
                                $winCellCounter = 3;
                                $winSymbol = 0;
                            }

                            // выигрывает 5 символов
                            if ($cellArray[$lineCells[$key][4]] === 0 && $cellArray[$lineCells[$key][3]] !== 8) {
                                $winCellCounter = 5;
                                $winSymbol = $cellArray[$lineCells[$key][3]];
                            }
                            if ($cellArray[$lineCells[$key][3]] === $cellArray[$lineCells[$key][4]] && $cellArray[$lineCells[$key][3]] !== 8) {
                                $winCellCounter = 5;
                                $winSymbol = $cellArray[$lineCells[$key][3]];
                            }

                            // выигрывают 4 символа
                            if ($cellArray[$lineCells[$key][3]] !== $cellArray[$lineCells[$key][4]] && $cellArray[$lineCells[$key][4]] !== 0) {
                                if ($cellArray[$lineCells[$key][3]] === 4 || $cellArray[$lineCells[$key][3]] === 5 || $cellArray[$lineCells[$key][3]] === 10) {
                                    $winCellCounter = 4;
                                    $winSymbol = $cellArray[$lineCells[$key][3]];
                                } else {
                                    $winCellCounter = 3;
                                    $winSymbol = 0;
                                }
                            }
                        }
                    } else {
                        $winSymbol = $cellArray[$lineCells[$key][2]];
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][2]] === $cellArray[$lineCells[$key][3]] || $cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][2]] === $cellArray[$lineCells[$key][4]] || $cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                            }
                        }
                    }

                } else {
                    $winCellCounter = 2;
                    $winSymbol = $cellArray[$lineCells[$key][1]];

                    if ($cellArray[$lineCells[$key][1]] === $cellArray[$lineCells[$key][2]] || $cellArray[$lineCells[$key][2]] === 0) {
                        $winCellCounter = 3;

                        if ($cellArray[$lineCells[$key][1]] === $cellArray[$lineCells[$key][3]] || $cellArray[$lineCells[$key][3]] === 0) {
                            $winCellCounter = 4;

                            if ($cellArray[$lineCells[$key][1]] === $cellArray[$lineCells[$key][4]] || $cellArray[$lineCells[$key][4]] === 0) {
                                $winCellCounter = 5;
                            }
                        }
                    }
                }
            }

            if ($winCellCounter > 2 && $winSymbol !== 2) {
                $winLinesData[] = [$winSymbol, $winCellCounter];
            }
        }

        return $winLinesData;
    }

    /**
     * Генерация рандомных значений для игрового поля
     */
    public function generatingValuesForCellsV2()
    {
        $cellsRows = [[],[],[],[],[]];
        for ($i=0; $i < 5; $i++) { // уровень отдельного барабана
            $cellRow = $cellsRows[$i];

            for ($k=0; $k < 3; $k++) { // уроверь отдельной ячейки в барабане
                $cellRow[$k] = $this->getCellValue($cellRow, $i);
            }

            $cellsRows[$i] = $cellRow;
        }

        $valuesForCells = []; // заполнение итогового массива
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 3; $k++) {
                $valuesForCells[] = $cellsRows[$i][$k];
            }
        }

        return $valuesForCells;
    }

    /**
     *  Получение значения для отдельной ячейки в барабане с учетом правил
     */
    public function getCellValue(array $cellRow, int $i)
    {
        $start = microtime(true);
        $time0 = microtime(true) - $start;
        $percentArr = $this->getPercentArr($i);
        $time1 = microtime(true) - $start;
        $rand = rand(0, 99999999);
        $rand = rand(0, 99999999);
        $rand = rand(0, 99999999);
        $time2 = microtime(true) - $start;

        $cellValue = '123123';
        $time3 = microtime(true) - $start;

        foreach ($percentArr as $key => $percentRang) { // поиск выпавшего значения ячейки
            $time4 = microtime(true) - $start;
            if ($rand >= $percentRang[0] && $rand < $percentRang[1]) {
                $time5 = microtime(true) - $start;
                $cellValue = $key;
                $time6 = microtime(true) - $start;
                break;
            }
            $time7 = microtime(true) - $start;
        }
        $time8 = microtime(true) - $start;

        if ($cellValue === '123123') {
            dd($cellValue);
        }
        $time9 = microtime(true) - $start;

        if (!in_array($cellValue, $cellRow) && $cellValue !== null) { // проверка есть ли уже в ряде такая ячейка
            $time10 = microtime(true) - $start;
            dd([
                '0' => $time0 - $start,
                '1' => $time1 - $time0,
                '2' => $time2 - $time1,
                '3' => $time3 - $time2,
                '4' => $time4 - $time3,
                '5' => $time5 - $time4,
                '6' => $time6 - $time5,
                '7' => $time7 - $time4,
                '8' => $time8 - $time7,
                '9' => $time9 - $time8,
                '10' => $time10 - $time9,
            ]);
            return $cellValue;
        } else {
            return $this->getCellValue($cellRow, $i);
        }
    }

    /**
     * Метод для получения массива диапозонов определяющих вероятности выпадения
     * для элементов
     */
    public function getPercentArr(int $i)
    {
         if (!isset($_SESSION['columnArrPercents'])) {
             $_SESSION['columnArrPercents'] = json_decode('{"columnArrPercentsMain": [
                 [1.8471, 10.130945, 3.33, 10.204471, 8.412396, 8.029052, 10.481988, 10.511185, 11.106571, 9.497244, 6.310503, 10.139384],
                 [1.8471, 10.130945, 3.33, 10.204471, 8.412396, 8.029052, 10.481988, 10.511185, 11.106571, 9.497244, 6.310503, 10.139384],
                 [1.8471, 10.130945, 3.33, 10.204471, 8.412396, 8.029052, 10.481988, 10.511185, 11.106571, 9.497244, 6.310503, 10.139384],
                 [1.2147, 14.822795, 3.33, 9.29428, 4.93122, 12.549503, 9.860469, 8.130571, 9.29428, 13.927485, 3.66828, 8.976605],
                 [0.6674, 16.272195, 3.33, 8.443325, 6.01978, 10.047705, 10.967071, 8.713955, 8.443325, 11.732401, 3.160123, 12.203142]
             ],
             "columnArrPercentsFree": [
                 [1.8665, 6.782, 0, 15.0578, 4.7103, 4.1151, 14.8439, 15.374, 15.9878, 5.5287, 3.5434, 12.1907],
                 [1.8665, 6.782, 0, 15.0578, 4.7103, 4.1151, 14.8439, 15.374, 15.9878, 5.5287, 3.5434, 12.1907],
                 [1.8665, 6.782, 0, 15.0578, 4.7103, 4.1151, 14.8439, 15.374, 15.9878, 5.5287, 3.5434, 12.1907],
                 [1.2144, 11.6191, 0, 14.1196, 1.1216, 8.7755, 14.2033, 12.9199, 14.1196, 10.0961, 0.8196, 10.9921],
                 [0.6502, 13.1133, 0, 13.2423, 2.2438, 6.1963, 15.3441, 13.5213, 13.2423, 7.8331, 0.2957, 14.3184]
             ]}');
         }
         $columnArrPercents = $_SESSION['columnArrPercents'];

         // конвертация json данных в формат который понимает код
         $percentArr = [];
         $rangArr = [0,0,0,0,0,0,0,0,0,0,0,0];
         // if ($_SESSION['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false) {
         //     foreach ($columnArrPercents->columnArrPercentsMain[$i] as $key => $value) {
         //         $percentArr[$key] = $value * 1000000;
         //     }
         //
         //     foreach ($percentArr as $key => $value) {
         //         if ($key > 0) {
         //             $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
         //         } else {
         //             $rangArr[$key] = [0, $value];
         //         }
         //
         //         if ($key === 11) {
         //             $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
         //         }
         //     }
         // } else {
         //     foreach ($columnArrPercents->columnArrPercentsFree[$i] as $key => $value) {
         //         $percentArr[$key] = $value * 1000000;
         //     }
         //
         //     foreach ($percentArr as $key => $value) {
         //         if ($key > 0) {
         //             $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
         //         } else {
         //             $rangArr[$key] = [0, $value];
         //         }
         //
         //         if ($key === 11) {
         //             $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
         //         }
         //     }
         // }

         foreach ($columnArrPercents->columnArrPercentsMain[$i] as $key => $value) {
             $percentArr[$key] = $value * 1000000;
         }

         foreach ($percentArr as $key => $value) {
             if ($key > 0) {
                 $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
             } else {
                 $rangArr[$key] = [0, $value];
             }

             if ($key === 11) {
                 $rangArr[$key] = [$rangArr[$key - 1][1], $rangArr[$key - 1][1] + $value];
             }
         }

         return $rangArr;
    }

    public function getAllWin(array $winLinesData, int $bet)
    {
        $winningLineСombinations = [
            0 => [3 => 100, 4 => 1000, 5 => 10000],
            1 => [3 => 30, 4 => 50, 5 => 200],
            3 => [3 => 15, 4 => 20, 5 => 100],
            4 => [3 => 50, 4 => 250, 5 => 1000],
            5 => [3 => 40, 4 => 150, 5 => 750],
            6 => [3 => 25, 4 => 40, 5 => 150],
            7 => [3 => 20, 4 => 30, 5 => 125],
            8 => [3 => 10, 4 => 20, 5 => 75],
            9 => [3 => 30, 4 => 100, 5 => 500],
            10 => [3 => 50, 4 => 250, 5 =>  1500],
            11 => [3 => 25, 4 => 50, 5 => 175],
        ];

        $allWin = 0;
        foreach ($winLinesData as $key => $winLineData) {
            $allWin += $winningLineСombinations[$winLineData[0]][$winLineData[1]];
        }

        return $allWin;
    }

}
