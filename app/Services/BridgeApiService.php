<?php

namespace App\Services;

use Ixudra\Curl\Facades\Curl;
use App\Models\Event;
use App\Models\BridgeApiRequest;
use App\Models\Transaction;
use App\Models\Session;
use App\Exceptions\V1\FirstMoveFundsException;
use Webpatser\Uuid\Uuid;
use App\Models\Log;

class BridgeApiService
{
    private $url = 'https://play777games.com/';

    public function startGame($token, $userId, $nickname, $gameId, $demo, $platformId)
    {
        // поиск активной сессии у пользователя
        $sessions = (new Session())->where('userId', '=', $userId)->get();

        // удадение всех старых сессиий
        foreach ($sessions as $key => $session) {
            $session->delete();
        }

        // для демо режима не обязателен полный набо данных
        // достаточно gameId и demo=true
        $freeSpinData = false;
        $allWin = 0;
        $reconnect = false;
        $check0FreeSpin = false;
        $freeSpinMul = false;
        $freeSpinResultAllWin = false;
        $doubleLose = false;
        $checkStartDouble = false;
        $savedStartAllWinForDouble = false;
        $cardGameIteration = false;
        $dcard = false;
        $eventID = false;
        $preAllWinOnRope = false;
        $linesInGame = 1;
        $freespinStart = false;

        // if ($demo === 'true') {
        //     $token = 'token';
        //     $nickname = 'nickname';
        //     $platformId = 'platformId';
        //     $userId = 1;
        //     $platformId = 1;
        //     $balance = 100;
        // } else {
        //     $balance = 0;
        // }

        if ($demo === 'true') {
            $balance = 100;
        } else {
            $balance = 0;
        }

        // при отсутстии создается
        $session = new Session();
        $session->uuid = Uuid::generate();
        $session->token = $token;
        $session->nickname = $nickname;
        $session->gameId = $gameId;
        $session->demo = $demo;
        $session->platformId = $platformId;
        $session->userId = $userId;
        $session->status = true;
        $session->freeSpinData = serialize($freeSpinData);
        $session->allWin = serialize($allWin);
        $session->reconnect = serialize($reconnect);
        $session->check0FreeSpin = serialize($check0FreeSpin);
        $session->balance = serialize($balance);
        $session->freeSpinMul = serialize($freeSpinMul);
        $session->freeSpinResultAllWin = serialize($freeSpinResultAllWin);
        $session->doubleLose = serialize($doubleLose);
        $session->checkStartDouble = serialize($checkStartDouble);
        $session->savedStartAllWinForDouble = serialize($savedStartAllWinForDouble);
        $session->cardGameIteration = serialize($cardGameIteration);
        $session->dcard = serialize($dcard);
        $session->eventID = serialize($eventID);
        $session->preAllWinOnRope = serialize($preAllWinOnRope);
        $session->linesInGame = serialize($linesInGame);

        $session->save();

        return $session->uuid;
    }

    public function endGame($userId)
    {
        $session = (new Session())->where('userId', '=', $userId)->where('status', '=', true)->get()->first();
        if ($session !== null) {
            $session->status = false;
            $session->save();

            return 'DEACTIVATE_SESSION';
        }

        return 'NO_SESSION';
    }

    public function getBalance($token, $userId, $gameId)
    {
        $responseGetBalance = Curl::to("{$this->url}getBalance?token={$token}&userId={$userId}&gameId={$gameId}")
        ->post();

        return $responseGetBalance;
    }

    public function sessionCheck($token, $userId)
    {
        $responseSessionCheck = Curl::to("{$this->url}sessionCheck?token={$token}&userId={$userId}")
            ->post();

        return $responseSessionCheck;
    }

    public function moveFunds($params, $type = 'start_of_spin')
    {
        $params = $this->updateMoveFundsDates($params, $type);

        $transaction = new Transaction();
        $transaction->uuid = Uuid::generate();
        $transaction->event_uuid = $params['eventID'];
        $transaction->save();

        $platformId = 1;
        if (isset($_GET['platformId'])) {
            $platformId = $_GET['platformId'];
        }
        $urlArray = [
            'token' => $params['token'],
            'userId' => $params['userId'],
            'gameId' => $params['gameId'],
            'eventId' => $params['eventID'],
            'direction' => $params['direction'],
            'platformId' => $platformId,
            'transactionId' => $transaction->uuid->string,
            'eventType' => $params['eventType'],
            'amount' => $params['amount'],
            'extraInfo' => $params['extraInfo']
        ];

        if (isset($_SESSION['test'])) {
            if ($_SESSION['test'] === true) {
                $platformId = 1;
                if (isset($_GET['platformId'])) {
                    $platformId = $_GET['platformId'];
                }
                $urlArray = [
                    'token' => 'token-1111-1111-1111',
                    'userId' => $params['userId'],
                    'gameId' => $params['gameId'],
                    'eventId' => 'eventId-1111-1111-1111',
                    'direction' => $params['direction'],
                    'transactionId' => 'transactionId-1111-1111-1111',
                    'eventType' => $params['eventType'],
                    'amount' => $params['amount'],
                    'platformId' => $platformId,
                    'extraInfo' => $params['extraInfo']
                ];
            }
        }

        $urlData1 = $urlArray;
        unset($urlData1['eventId']);
        unset($urlData1['transactionId']);

        $requestURL = "{$this->url}moveFunds?";
        $requestURL .= http_build_query($urlData1);
        $requestURL .= "&transactionId=" . $urlArray['transactionId'] . "&eventId=" . $urlArray['eventId'];

        // для теста делается дополнительная запись данных в БД
        // и не делается отправка запроса на сервер апи
        if (isset($_SESSION['test'])) {
            if ($_SESSION['test'] === true) {
                $bridgeApiRequest = new BridgeApiRequest;
                //$bridgeApiRequest->token = $urlArray['token'];
                $bridgeApiRequest->token = 'token-1111-1111-1111';
                $bridgeApiRequest->user_id = $urlArray['userId'];
                //$bridgeApiRequest->event_id = $urlArray['eventId'];
                $bridgeApiRequest->event_id = 'eventId-1111-1111-1111';
                $bridgeApiRequest->direction = $urlArray['direction'];
                //$bridgeApiRequest->transaction_id = $urlArray['transactionId'];
                $bridgeApiRequest->transaction_id = 'transactionId-1111-1111-1111';
                $bridgeApiRequest->event_type = $urlArray['eventType'];
                $bridgeApiRequest->amount = $urlArray['amount'];
                $bridgeApiRequest->extra_info = serialize($urlArray['extraInfo']);
                $bridgeApiRequest->requestURL = $requestURL;
                $bridgeApiRequest->save();

                return '{"status":true,"balance":100}'; // выход из функции
            }
        }

        // $log = new Log;
        // $log->type = 'info';
        // $log->data = json_encode(['requestURL' => $requestURL]);
        // $log->save();

        $responseMoveFunds = Curl::to($requestURL)
            ->post();

        // $log = new Log;
        // $log->type = 'info';
        // $log->data = json_encode(['requestURL' => $requestURL, 'responseMoveFunds' => $responseMoveFunds]);
        // $log->save();

        if ($params['eventType'] === 'BetPlacing') {
            if ($responseMoveFunds === false) {
                throw new FirstMoveFundsException(['type' => 'bridgeApi->moveFunds', 'requestURL' => $requestURL, 'responseMoveFunds' => $responseMoveFunds]);
            }
            if (!isset(json_decode($responseMoveFunds)->status)) {
                throw new FirstMoveFundsException(['type' => 'bridgeApi->moveFunds', 'requestURL' => $requestURL, 'responseMoveFunds' => $responseMoveFunds]);
            }
            if (json_decode($responseMoveFunds)->status === false) {
                throw new FirstMoveFundsException(['type' => 'bridgeApi->moveFunds', 'requestURL' => $requestURL, 'responseMoveFunds' => $responseMoveFunds]);
            }
        }

        return $responseMoveFunds;
    }

    public function exitGame($token, $userId, $gameId, $collect, $test)
    {
        if ($collect === 'true') {
            $collect = true;
        } elseif ($collect === 'false') {
            $collect = false;
        } else {
            $collect = false;
        }

        $session = (new Session)->where('userId', '=', $userId)->orderBy('created_at', 'desc')->get()->first();

        if ($session !== null) {
            // закрытие сессии на ezsl
            $session->delete();

            // отправка запроса на 777games
            if ($test !== 'true') {
                $ch = curl_init( "{$this->url}exit" );
                $payload = json_encode( array(
                  'token' => $token,
                  'userId' => $userId,
                  'gameId' => $gameId,
                  'collect' => $collect
                ) );
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                $result = curl_exec($ch);
                curl_close($ch);
            } else {
                $result = 'exit_response_for_test_done';
            }

            // $result = Curl::to("{$this->url}exit")
            //     ->withData([
            //         'token' => $token,
            //         'userId' => $userId,
            //         'gameId' => $gameId,
            //         'collect' => $collect
            //     ])
            //     ->post();
        } else {
            return response()->json(["status" => "false", "message" => "SessionNotExist"]);
        }

        return $result;
    }

    protected function updateMoveFundsDates($params, $type = 'start_of_spin')
    {
        if ($type === 'end_of_spin') { // конец спина
            $params['eventType'] = 'Lose';
            if ($params['allWin'] > 0 && $params['eventType'] !== 'Raise') { // если eventType = 'Raise', то ...
                $params['eventType'] = 'Win';
            }

            $params['direction'] = 'credit';
            $params['amount'] = $params['allWin'];

            // параметр фриспина долже определяется каждой игрой самостоятельно из-за различия
            // в реализациях
        }

        if ($type === 'end_of_double') {
            if ($params['allWin'] > 0) {
                $params['direction'] = 'credit';
                $params['amount'] = $params['allWin'] - $_SESSION['savedStartAllWinForDouble'];
            } else {
                $params['direction'] = 'debit';
                $params['amount'] = $params['allLose'];
            }
        }

        if ($type === 'end_of_double_in_keno') {
            //$params['amount'] = $_SESSION['savedStartAllWinForDouble'] * 2;
        }

        if ($type === 'end_of_FS_spin') {

        }

        if ($type === 'jackpot') {

        }

        return $params;
    }
}
