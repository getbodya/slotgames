<?php
Route::auth();

Route::get('/404', 'Front\IndexController@show404')->name('404');

Route::group(['namespace' => 'Front'], function() {
    Route::get('spinkeno', 'IndexController@getSpinKeno');
    Route::get('doublekeno', 'IndexController@getDoubleKeno');
    Route::get('/', 'IndexController@getIndex');
    Route::get('/home', 'IndexController@getHome');
    Route::get('game/{gameName}', 'IndexController@getGame');
    Route::get('test123', 'IndexController@test');
    Route::get('testCookies', 'IndexController@testCookies');
});

/**
 * Bridge Api
 */
Route::get('/startGame', 'Api\BridgeApiController@startGame');
Route::get('/exit', 'Api\BridgeApiController@exit');
Route::get('/betPlacingAbort', 'Api\BridgeApiController@betPlacingAbort');
Route::get('/moveFundsException', 'Api\BridgeApiController@moveFundsRepeat');
Route::post('/getBalance', 'Api\BridgeApiController@getBalance');
Route::post('/sessionCheck', 'Api\BridgeApiController@sessionCheck');
Route::post('/moveFunds', 'Api\BridgeApiController@moveFunds');
Route::post('/exit', 'Api\BridgeApiController@exit');

Route::group(['namespace' => 'Api'], function() {
    Route::get('init', 'GameController@actionInit');
    Route::get('state', 'GameController@actionState');
    Route::get('spin/{gameName}', 'GameController@actionSpin');
    Route::get('double/{gameName}', 'GameController@actionDouble');
    Route::get('choice/{gameName}', 'GameController@actionChoice');
    Route::get('add-credit', 'GameController@actionAddCredit');
    Route::get('get-user-jackpot', 'GameController@actionGetJackpot');
    Route::get('get-user-balance', 'GameController@getUserBalance');
    Route::get('create-zero-balance-cron-task', 'GameController@actionCreateZeroBalanceCronTask');

    Route::get('update-user-balance', function ()
    {
        $data = [
            'topic_id' => '',
            'data' => ''
        ];

        \App\Classes\Socket\Pusher::sentDataToServer($data);
    });

    Route::get('increaseBalance/{banknote}', 'UserController@increaseBalance');

    Route::get('reset-session', 'GameController@resetSession');
    Route::get('reconnect', 'GameController@reconnect');

    Route::get('test/{gameName}', 'AdminController@testSlots');
    Route::post('test/{gameName}', 'AdminController@testSlots');
    Route::get('jackpot-test/{gameName}', 'AdminController@testJackpots');
    Route::post('jackpot-test/{gameName}', 'AdminController@testJackpots');
    Route::get('jackpot-probability-test/{gameName}', 'AdminController@testJackpots');
    Route::post('jackpot-probability-test/{gameName}', 'AdminController@jackpotProbabilityTest');

    // админка для симуляции и отображения статистики
    Route::get('admin', 'AdminController@showIndex');
    Route::get('admin/all-tables', 'AdminController@showAllTables');
    Route::get('admin/clear-old-data', 'AdminController@clearOldData');
    Route::get('admin/logs', 'AdminController@showLogs');
    Route::get('admin/logs/remove', 'AdminController@removeLogs');
    Route::get('admin/jackpots', 'AdminController@showJackpots');
    Route::get('admin/set-jackpots', 'AdminController@setJackpots');
    Route::get('admin/remove-user-jackpots', 'AdminController@removeUserJackpots');
    Route::get('admin/simulations', 'AdminController@showSimelations');
    Route::get('admin/statistics', 'AdminController@showStatistics');
    Route::get('admin/simelation/elGallo', 'AdminController@commonTest');
    Route::get('admin/simelation/lol', 'AdminController@testLOL');
    Route::get('admin/simelation/superkeno', 'AdminController@superKenoTest');
    Route::get('admin/simelation/doublekeno', 'AdminController@doubleKenoTest');
    Route::get('admin/simelation/elGallo/set-percentages', 'AdminController@showSetPercentagesEG');
    Route::post('admin/simelation/elGallo/set-percentages', 'AdminController@setPercentagesEG');
    Route::post('admin/simelation/elGallo/import-percentages', 'AdminController@importPercentagesEG');
});


// запросы с ошибками
Route::get('/firstMoveFundsException', 'Front\IndexController@firstMoveFundsException');
Route::get('/moveFundsException', 'Front\IndexController@moveFundsException');
Route::get('/balanceException', 'Front\IndexController@balanceException');
Route::get('/unauthenticatedException', 'Front\IndexController@unauthenticatedException');

// routes V2
Route::group(['namespace' => 'Api'], function() {
    Route::get('api-v2/init', 'GameControllerV2@init');
    Route::get('api-v2/action', 'GameControllerV2@action');
    Route::get('api-v2/info', 'GameControllerV2@info');
});
