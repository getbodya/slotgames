<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\GameServices\ElGalloSpinService2;
use App\Models\TestLog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function testLog()
    {
        $betLine = 1;
        $linesInGame = 25;
        $itr = 100000;

        $elGallo = new ElGalloSpinService2();

        $allWinOnSlots = 0;
        for ($i = 0; $i < $itr; $i++) {
            $resultData = $elGallo->getLiteTestSpinData($betLine, $linesInGame, 'elGallo');
            $allWinOnSlots += $resultData['allWin'];
        }

        $allBet = $itr * $betLine * $linesInGame;
        $probability = 100 / $allBet * $allWinOnSlots;

        $testLog = new TestLog();
        $testLog->itr = $itr;
        $testLog->all_win_on_slots = $allWinOnSlots;
        $testLog->all_bet = $allBet;
        $testLog->probability = $probability;
        $testLog->save();


    }

    public function getTestLogs()
    {
        $data = TestLog::all()->toArray();

        foreach ($data as $item) {
            echo $item['probability'].'<br>';
        }
    }
}
