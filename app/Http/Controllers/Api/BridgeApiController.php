<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\BridgeApiService;
use App\Models\BetPlacingAbortExceptionModel;
use App\Models\MoveFundsExceptionModel;
use App\Exceptions\BetPlacingAbortException;

class BridgeApiController extends Controller
{
    public function startGame(Request $request) {
        $token = $request->input('token');
        $userId = $request->input('userId');
        $nickname = $request->input('nickname');
        $gameId = $request->input('gameId');
        $demo = $request->input('demo');
        $token = $request->input('token');
        $platformId = $request->input('platformId');

        $sessionID = (new BridgeApiService)->startGame($token, $userId, $nickname, $gameId, $demo, $platformId);

        // для теста не нужно последующая переадресация, нужен uuid, чтобы выполнить init запрос
        if ($request->input('test') === 'true') {
            return $sessionID;
        }

        $getParams = "sessionID={$sessionID}&demo={$demo}&userId={$userId}&nickname={$nickname}&gameId={$gameId}&platformId={$platformId}&token={$token}";

        if ($_GET['gameId'] === '1') {
            header("Location: https://game.play777games.com/games/elGallo/?{$getParams}");
            die();
        }

        if ($_GET['gameId'] === '2') {
            header("Location: https://game.play777games.com/games/lifeOfLuxury/?{$getParams}");
            die();
        }

        if ($_GET['gameId'] === '3') {
            header("Location: https://game.play777games.com/games/superballKeno/?{$getParams}");
            die();
        }

        if ($_GET['gameId'] === '4') {
            header("Location: https://game.play777games.com/games/superDoubleUp/?{$getParams}");
            die();
        }

        return response()->json($responseStartGame);
    }

    public function endGame(Request $request)
    {
        $userId = $request->input('userId');
        $responseData = (new BridgeApiService)->endGame($userId);
        return response()->json($responseData);
    }

    public function getBalance(Request $request)
    {
        $token = $request->input('token');
        $userId = $request->input('userId');
        $nickname = $request->input('nickname');
        $gameId = $request->input('gameId');
        $demo = $request->input('demo');
        $token = $request->input('token');
        $platformId = $request->input('platformId');

        $responseGetBalance = (new BridgeApiService)->getBalance($token, $userId, $gameId);


        if ($responseGetBalance == false) {
            return json_encode($responseGetBalance);
        }

        return response()->json($responseGetBalance);
    }

    public function sessionCheck(Request $request)
    {
        $token = $request->input('token');
        $userId = $request->input('userId');

        $responseSessionCheck = (new BridgeApiService)->sessionCheck($token, $userId);

        if ($responseSessionCheck == false) {
            return json_encode($responseSessionCheck);
        }

        return response()->json($responseSessionCheck);
    }

    public function moveFunds(Request $request)
    {
        $platformId = 1;
        if (isset($_GET['platformId'])) {
            $platformId = $_GET['platformId'];
        }
        $params = [
            'token' => $request->input('token'),
            'userId' => $request->input('userId'),
            'gameId' => $request->input('gameId'),
            'eventId' => $request->input('eventId'),
            'direction' => $request->input('direction'),
            'platformId' => $platformId,
            'transactionId' => $request->input('transactionId'),
            'amount' => $request->input('amount'),
            'amount' => $request->input('amount'),
            'selected' => $request->input('selected'),
            'result' => $request->input('result'),
            'featureGame' => $request->input('featureGame')
        ];

        $responseMoveFunds = (new BridgeApiService)->moveFunds($params);

        if ($responseMoveFunds == false) {
            return json_encode($responseMoveFunds);
        }

        return response()->json($responseMoveFunds);
    }

    public function exit(Request $request)
    {
        $token = $request->input('token');
        $userId = $request->input('userId');
        $gameId = $request->input('gameId');
        $collect = $request->input('collect');
        $test = $request->input('test');

        $responseExit = (new BridgeApiService)->exitGame($token, $userId, $gameId, $collect, $test);
        if ($test === 'true') {
            return $responseExit;
        }

        if ($responseExit === false) {
            return json_encode(false);
        }
        $result = json_decode($responseExit);
        if (!isset($result->status)) {
            return json_encode(false);
        }
        if ($result->status === false) {
            return json_encode(false);
        }

        return response()->json(true);
    }

    /**
     * Метод отправляет запрос на повторную попытку отправки данных
     * с результатами хода
     * @return json
     */
    public function moveFundsRepeat(Request $request)
    {
        $moveFundsExceptionID = $request->input('moveFundsExceptionID');

        $moveFundsExceptionModel = (new MoveFundsExceptionModel())
            ->where('id', '=', $moveFundsExceptionID)
            ->get()
            ->first();

        if ($moveFundsExceptionModel->count < 5) {
            $moveFundsExceptionModel->count += 1;
            $moveFundsExceptionModel->save();

            $responseMoveFunds = (new BridgeApiService())->moveFunds(unserialize($moveFundsExceptionModel->data), 'end_of_spin');
        } else {
            $unserializeData = unserialize($moveFundsExceptionModel->data);
            throw new BetPlacingAbortException($unserializeData['amount'], $unserializeData['extraInfo']['selected']['0']);
        }

        if ($responseMoveFunds === false) {
            return response()->json(false);
        }
        $result = json_decode($responseMoveFunds);
        if (!isset($result->error)) {
            return response()->json(false);
        }
        if ($result->error !== 1004 && $result->status === false) {
            return response()->json(false);
        }

        return response()->json(true);
    }

    /**
     * Метод отправляет запрос на возврат средств пользователю
     * Если метод возвращает false, то js должен отправлять повторные запросы,
     * пока не вернется true
     * @return json
     */
    public function betPlacingAbort(Request $request)
    {
        $betPlacingAbortExceptionID = $request->input('betPlacingAbortExceptionID');

        $betPlacingAbortException = (new BetPlacingAbortExceptionModel())
            ->where('id', '=', $betPlacingAbortExceptionID)
            ->get()
            ->first();

        $platformId = 1;
        if (isset($_GET['platformId'])) {
            $platformId = $_GET['platformId'];
        }
        // отправка события о возврате средств
        $responseMoveFunds = (new BridgeApiService())->moveFunds(array(
            'token' => $betPlacingAbortException->token,
            'userId' => $betPlacingAbortException->userId,
            'gameId' => $betPlacingAbortException->gameId,
            'direction' => 'credit',
            'platformId' => $platformId,
            'eventType' => 'BetPlacingAbort',
            'eventID' => $betPlacingAbortException->eventID,
            'amount' => $betPlacingAbortException->amount,
            'extraInfo' => [
                'selected' => [$betPlacingAbortException->selected]
            ]
        ));

        if ($responseMoveFunds === false) {
            return response()->json(false);
        }
        $result = json_decode($responseMoveFunds);
        if (!isset($result->error)) {
            return response()->json(false);
        }
        if ($result->error !== 1006 && $result->status === false) {
            return response()->json(false);
        }

        return response()->json(true);
    }


}
