<?php

namespace App\Http\Controllers\Api;

use App\Services\GameServices\ElGalloSpinService;
use App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\GameServices\AlaskaSpinService;
use App\Services\GameServices\KeksSpinService;
use App\Services\GameServices\IgrosoftDoubleService;
use App\Services\GameServices\GonzaSpinService;
use App\Models\User;
use App\Models\SavedDataBundle;
use App\Models\Session;
use App\Models\Gamesassion;
use App\Services\BridgeApiService;
use App\Exceptions\V1\BetPlacingAbortException;
use App\Exceptions\V1\FirstMoveFundsException;
use App\Exceptions\V1\moveFundsException;
use App\Exceptions\V1\BetPayedAbortException;
use App\Exceptions\V1\BalanceException;
use App\Exceptions\V1\UnauthenticatedException;
use App\Exceptions\V1\EmptySessionDataException;
use Illuminate\Support\Facades\Config;
use App\Models\Event;
use App\Models\Transaction;
use Webpatser\Uuid\Uuid;
use App\Models\Jackpot;
use App\Models\CronTask;
use App\Models\Log;
use Illuminate\Support\Facades\Cache;

class GameController extends Controller
{
    public function actionInit(Request $request)
    {
        try {
            session_start();

            if (Config::get('app.mod') !== 'dev') {
                // получение сессии создонной после запроса startGame
                $session = (new Session())->where('uuid', '=', $request->input('sessionID'))->get()->first();

                if ($session !== null) {
                    //$session->uuid = Uuid::generate()->string->string;
                    //$session->save();
                } else {
                    // удаление всех старых сессиий
                    $sessions = (new Session())->where('userId', '=', $request->input('userId'))->get();

                    foreach ($sessions as $key => $session) {
                        $session->delete();
                    }

                    if (!$request->input('platformId')) {
                        return response()->json(["status" => "false", "message" => "UndefinedPlatformId"]);
                    }

                    // для демо режима не обязателен полный набо данных
                    // достаточно gameId и demo=true
                    $freeSpinData = false;
                    $allWin = 0;
                    $reconnect = false;
                    $check0FreeSpin = false;
                    $freeSpinMul = false;
                    $freeSpinResultAllWin = false;
                    $doubleLose = false;
                    $checkStartDouble = false;
                    $savedStartAllWinForDouble = false;
                    $cardGameIteration = false;
                    $dcard = false;
                    $eventID = false;
                    $preAllWinOnRope = false;
                    $linesInGame = 1;
                    $freespinStart = false;

                    if ($request->input('demo') === 'true') {
                        // $token = 'token';
                        // $nickname = 'nickname';
                        // $platformId = 'platformId';
                        // $userId = 1;
                        // $platformId = 1;
                        $balance = 100;
                    } else {
                        $balance = 0;
                    }

                    // при отсутстии создается
                    $session = new Session();
                    $session->uuid = Uuid::generate()->string;
                    $session->token = $request->input('token');
                    $session->nickname = $request->input('nickname');
                    $session->gameId = $request->input('gameId');
                    $session->demo = $request->input('demo');
                    $session->platformId = $request->input('platformId');
                    $session->userId = $request->input('userId');
                    $session->status = true;
                    $session->freeSpinData = serialize($freeSpinData);
                    $session->allWin = serialize($allWin);
                    $session->reconnect = serialize($reconnect);
                    $session->check0FreeSpin = serialize($check0FreeSpin);
                    $session->balance = serialize($balance);
                    $session->freeSpinMul = serialize($freeSpinMul);
                    $session->freeSpinResultAllWin = serialize($freeSpinResultAllWin);
                    $session->doubleLose = serialize($doubleLose);
                    $session->checkStartDouble = serialize($checkStartDouble);
                    $session->savedStartAllWinForDouble = serialize($savedStartAllWinForDouble);
                    $session->cardGameIteration = serialize($cardGameIteration);
                    $session->dcard = serialize($dcard);
                    $session->eventID = serialize($eventID);
                    $session->preAllWinOnRope = serialize($preAllWinOnRope);
                    $session->linesInGame = serialize($linesInGame);
                    $session->save();
                }
            } else {
                $session = new Session();
                $session->uuid = Uuid::generate()->string;
                $session->token = 'token';
                $session->nickname = 'nickname';
                $session->gameId = 1;
                $session->demo = false;
                $session->platformId = 1;
                $session->userId = 1;
                $session->freeSpinData = serialize(false);
                $session->allWin = serialize(0);
                $session->reconnect = serialize(false);
                $session->check0FreeSpin = serialize(false);
                $session->balance = serialize(100);
                $session->freeSpinMul = serialize(false);
                $session->freeSpinResultAllWin = serialize(false);
                $session->doubleLose = serialize(false);
                $session->checkStartDouble = serialize(false);
                $session->savedStartAllWinForDouble = serialize(false);
                $session->cardGameIteration = serialize(false);
                $session->dcard = serialize(false);
                $session->eventID = serialize(false);
                $session->preAllWinOnRope = serialize(false);
                $session->linesInGame = serialize(1);
                $session->save();
            }

            if ($session === null) {
                return response()->json(["status" => "false", "message" => "SessionNotExist"]);
            }


            if (isset($session->uuid->string)) {
                //dd($session->uuid->string);
                return response()->json($session->uuid->string);
            } else {
                return response()->json($session->uuid);
            }


        } catch (EmptySessionDataException $e) {
            if (Config::get('app.mod') === 'dev') {
                $_SESSION['sessionName'] = 111111;

                return response()->json($session['sessionName']);
            } else {
                return view('404');
            }
        }
    }

    public function actionState(Request $request)
    {
        try {
            session_start();

            $session = (new Session())->where('uuid', '=', $request->input('sessionName'))->get()->first();

            if ($session === null) {
                return response()->json(["status" => "false", "message" => "SessionNotExist"]);
            }

            $sessionName = $request->input('sessionName');

            if (Config::get('app.mod') !== 'dev' && $session->demo == 'false') {
                //получаем баланс пользователя
                $apiService = new BridgeApiService();
                $getBalanceJson = $apiService->getBalance($session->token, $session->userId, $session->gameId);

                if ($getBalanceJson === false) {
                    throw new BalanceException();
                }
                if (!isset(json_decode($getBalanceJson)->status)) {
                    throw new BalanceException();
                }
                if (json_decode($getBalanceJson)->status === false) {
                    throw new BalanceException();
                }

                // проверка активности сессии на сервер с мост-апи (http://slot.pantera.co.ua)
                if ($apiService === false) {
                    throw new UnauthenticatedException();
                }
                $checkApiSession = json_decode($apiService->sessionCheck($session->token, $session->userId));
                if (!isset($checkApiSession->status)) {
                    throw new UnauthenticatedException();
                }
                if ($checkApiSession->status === false) {
                    throw new UnauthenticatedException();
                }

                $getBalanceData = json_decode($getBalanceJson);
                if (isset($getBalanceData->balance)) {
                    $balance = (float) $getBalanceData->balance;

                    $session->balance = serialize($balance);
                    $session->save();

                    if ($request->input('game') === 'super_keno') {
                        $answerData = [
                            'status' => 'true',
                            'balance' => $balance,
                            'jackpotValue' => Cache::get('kenoJackPot')
                        ];
                    } elseif ($request->input('game') === 'double_keno') {
                        $answerData = [
                            'status' => 'true',
                            'balance' => $balance,
                            'jackpotValue' => Cache::get('kenoDoubleJP')
                        ];
                    } elseif ($request->input('game') === 'elgallo') {
                        $data = Jackpot::firstOrFail()->toArray();

                        $jackpotsData = [
                          'MINI' => round($data['mini'], 2),
                          'MINOR' => round($data['minor'], 2),
                          'MAJOR' => round($data['major'], 2),
                          'BIG_DADDY' => round($data['big_daddy'], 2)
                        ];

                        $answerData = [
                            'status' => 'true',
                            'balance' => $balance,
                            'jackpotValue' => $jackpotsData
                        ];
                    } else {
                        $answerData = [
                            'status' => 'true',
                            'balance' => $balance
                        ];
                    }

                    return response()->json($answerData);
                }
            } else {
                $balance = 100;

                $session->balance = serialize($balance);
                $session->save();

                if ($request->input('game') === 'super_keno') {
                    $answerData = [
                        'status' => 'true',
                        'balance' => $balance,
                        'mod' => 'demo',
                        'jackpotValue' => Cache::get('kenoJackPot')
                    ];
                } elseif ($request->input('game') === 'double_keno') {
                    $answerData = [
                        'status' => 'true',
                        'balance' => $balance,
                        'mod' => 'demo',
                        'jackpotValue' => Cache::get('kenoDoubleJP')
                    ];
                } elseif ($request->input('game') === 'elgallo') {
                    $data = Jackpot::firstOrFail()->toArray();

                    $jackpotsData = [
                      'MINI' => round($data['mini'], 2),
                      'MINOR' => round($data['minor'], 2),
                      'MAJOR' => round($data['major'], 2),
                      'BIG_DADDY' => round($data['big_daddy'], 2)
                    ];

                    $answerData = [
                        'status' => 'true',
                        'balance' => $balance,
                        'jackpotValue' => $jackpotsData
                    ];
                } else {
                    $answerData = [
                        'status' => 'true',
                        'balance' => $balance
                    ];
                }

                return response()->json($answerData);
            }
        } catch (EmptySessionDataException $e) {
            if (Config::get('app.mod') === 'dev') {
                $e->setDevSessionData();
            } else {
                $e->render();
            }
        } catch (\Exception $e) {
        } catch (UnauthenticatedException $e) {
            return response()->json('{"status":"false"}');
        }

    }

    public function actionSpin(Request $request, GonzaSpinService $gonzaSpinService, AlaskaSpinService $alaskaSpinService, KeksSpinService $keksSpinService, Gamesassion $gamesassion, $gameName)
    {
        session_start();

        $session = (new Session())->where('uuid', '=', $request->input('sessionName'))->get()->first();
        // проверка последней вкладки
        if ($session === null) {
            $log = new Log;
            $log->type = 'error';
            $log->data = 'ActiveUserSessionException';
            $log->save();

            return response()->json(["status" => "false", "message" => "ActiveUserSessionException"]);
        }
        $session = $this->openGameSession($request->input('sessionName'));

        // отлавливание ошибки связанной с потерей данных сессии в safari
        if (!isset($_SESSION['demo'])) {
            $log = new Log;
            $log->type = 'error';
            $log->data = 'ActiveUserSessionException';
            $log->save();

            return "{'status':'false', 'message':'SessionNotExist'}";
        }
        if ($_SESSION['demo'] === 'false') {
            $_SESSION['demo'] = false;
        }

        $sessionName = $request->input('sessionName');
        $betLine = $request->input('betLine');
        $linesInGame = $request->input('linesInGame');
        $_SESSION['checkStartDouble'] = false; // проверка начала игры на удвоение (нужно для сохранения выигрыша на котором делается вход в игру)

        // вычисление результатов хода
        $spinResultData = $this->getSpinResultData($betLine, $linesInGame, $gameName);

        $this->saveGameSession($session, $spinResultData);

        return response()->json($spinResultData);
    }

    public function actionDouble(Request $request, IgrosoftDoubleService $igrosoftDoubleService, $gameName)
    {
        session_start();

        $sessionName = $request->input('sessionName');
        $selectedCard = $request->input('selectedCard');

        $session = $this->openGameSession($request->input('sessionName'));

        if (1 == 1) {
            // получаем данные о раскрытых картах
            $doubleResultData = false;
            switch ($gameName) {
                case 'keks':
                    $doubleResultData = $igrosoftDoubleService->getDoubleResultData($selectedCard, $gameName);
                    break;
                case 'elGallo':
                    $doubleResultData = (new ElGalloSpinService())->getDoubleResultData($selectedCard);
                    break;
            }

            $this->saveGameSession($session, $doubleResultData);

            return $doubleResultData;
        } else {
            $errorData = [
                'status' => 'false'
            ];

            return response()->json($errorData);
        }
    }

    public function actionChoice(Request $request, $gameName)
    {
        session_start();

        $sessionName = $request->input('sessionName');
        $session = $this->openGameSession($request->input('sessionName'));

        // $event = new Event();
        // $event->uuid = Uuid::generate()->string;
        // $event->save();
        // $_SESSION['eventID'] = $event->uuid->string;

        $choiceResultData = false;
        switch ($gameName) {
            case 'elGallo':
                $choiceResultData = (new ElGalloSpinService())->getChoiceResultData($request->input('choice'));
                break;
        }

        $this->saveGameSession($session, $choiceResultData);

        return $choiceResultData;
    }

    public function resetSession()
    {
        //session()->flush();

        return response()->json('reset session');
    }

    public function reconnect()
    {
        $_SESSION['reconnect'] = true;
    }

    public function getSpinResultData($betLine, $linesInGame, $gameName)
    {
        if (1 == 1) {
            // получаем данные о выпавших значениях для ячеек
            $spinResultData = false;
            switch ($gameName) {
                case 'keks':
                    $spinResultData = $keksSpinService->getSpinResultData($betLine, $linesInGame, $gameName);
                    break;
                case 'alaskanFishing':
                    $spinResultData = $alaskaSpinService->getSpinResultData($betLine, $linesInGame, $gameName);
                    break;
                case 'gonzosQuest':
                    $spinResultData = $gonzaSpinService->getSpinResultData($betLine, $gameName);
                    break;
                case 'elGallo':
                    $spinResultData = (new ElGalloSpinService())->getSpinResultData($betLine, $linesInGame, $gameName);
                    break;
                case 'lifeOfLuxury':
                    $spinResultData = (new LifeOfLuxuryService())->getSpinResultData($betLine, $linesInGame, $gameName);
                    break;
                // case 'keno':
                //     $spinResultData = (new kenoService())->getSpinResultData();
                //     break;
            }

            return $spinResultData;
        } else {
            $errorData = [
                'status' => 'false'
            ];

            return response()->json($errorData);
        }
    }

    public function openGameSession($uuid)
    {
        $session = (new Session())->where('uuid', '=', $uuid)->get()->first();

        if (is_object($session->uuid)) {
            $_SESSION['sessionName'] = $session->uuid->string;
        } else {
            $_SESSION['sessionName'] = $session->uuid;
        }

        $_SESSION['balance'] = unserialize($session->balance);
        $_SESSION['token'] = $session->token;
        $_SESSION['userId'] = $session->userId;
        $_SESSION['gameId'] = $session->gameId;
        $_SESSION['nickname'] = $session->nickname;
        $_SESSION['demo'] = $session->demo;
        $_SESSION['platformId'] = $session->platformId;
        $_SESSION['freeSpinData'] = unserialize($session->freeSpinData);
        $_SESSION['allWin'] = unserialize($session->allWin);
        $_SESSION['reconnect'] = unserialize($session->reconnect);
        $_SESSION['check0FreeSpin'] = unserialize($session->check0FreeSpin);
        $_SESSION['freeSpinMul'] = unserialize($session->freeSpinMul);
        $_SESSION['sessionName'] = $session->uuid;
        $_SESSION['freeSpinResultAllWin'] = unserialize($session->freeSpinResultAllWin);
        $_SESSION['doubleLose'] = unserialize($session->doubleLose);
        $_SESSION['checkStartDouble'] = unserialize($session->checkStartDouble);
        $_SESSION['savedStartAllWinForDouble'] = unserialize($session->savedStartAllWinForDouble);
        $_SESSION['cardGameIteration'] = unserialize($session->cardGameIteration);
        $_SESSION['dcard'] = unserialize($session->dcard);
        $_SESSION['eventID'] = unserialize($session->eventID);
        $_SESSION['preAllWinOnRope'] = unserialize($session->preAllWinOnRope);
        $_SESSION['linesInGame'] = unserialize($session->linesInGame);

        return $session;
    }

    public function saveGameSession($session, $spinResultData)
    {
        if (isset($_SESSION['freeSpinData'])) {
            $session->freeSpinData = serialize($_SESSION['freeSpinData']);
        }

        if (isset($_SESSION['allWin'])) {
            $session->allWin = serialize($_SESSION['allWin']);
        }

        if (isset($_SESSION['balance'])) {
            $session->balance = serialize($_SESSION['balance']);
        }

        if (isset($_SESSION['eventID'])) {
            if (isset($_SESSION['eventID']->string)) {
                $session->eventID = serialize($_SESSION['eventID']->string);
            } else {
                $session->eventID = serialize($_SESSION['eventID']);
            }
        } else {
            $session->eventID = serialize(false);
        }

        if (isset($_SESSION['preAllWinOnRope'])) {
            $session->preAllWinOnRope = serialize($_SESSION['preAllWinOnRope']);
        } else {
            $session->preAllWinOnRope = serialize(false);
        }

        if (isset($_SESSION['doubleLose'])) {
            $session->doubleLose = serialize($_SESSION['doubleLose']);
        } else {
            $session->doubleLose = serialize(false);
        }

        if (isset($_SESSION['checkStartDouble'])) {
            $session->checkStartDouble = serialize($_SESSION['checkStartDouble']);
        } else {
            $session->checkStartDouble = serialize(false);
        }

        if (isset($_SESSION['savedStartAllWinForDouble'])) {
            $session->savedStartAllWinForDouble = serialize($_SESSION['savedStartAllWinForDouble']);
        } else {
            $session->savedStartAllWinForDouble = serialize(false);
        }

        if (isset($_SESSION['cardGameIteration'])) {
            $session->cardGameIteration = serialize($_SESSION['cardGameIteration']);
        } else {
            $session->cardGameIteration = serialize(false);
        }

        if (isset($_SESSION['dcard'])) {
            $session->dcard = serialize($_SESSION['dcard']);
        } else {
            $session->dcard = serialize(false);
        }

        if (isset($_SESSION['freeSpinResultAllWin'])) {
            $session->freeSpinResultAllWin = serialize($_SESSION['freeSpinResultAllWin']);
        } else {
            $session->freeSpinResultAllWin = serialize(false);
        }

        if (isset($_SESSION['freeSpinMul'])) {
            $session->freeSpinMul = serialize($_SESSION['freeSpinMul']);
        } else {
            $session->freeSpinMul = serialize(false);
        }

        if (isset($spinResultData['reconnect'])) {
            $session->reconnect = serialize($spinResultData['reconnect']);
        } else {
            $session->reconnect = serialize(false);
        }

        if (isset($spinResultData['check0FreeSpin'])) {
            $session->check0FreeSpin = serialize($spinResultData['check0FreeSpin']);
        } else {
            $session->check0FreeSpin = serialize(false);
        }

        if (isset($spinResultData['linesInGame'])) {
            $session->linesInGame = serialize($spinResultData['linesInGame']);
        } else {
            $session->linesInGame = serialize(false);
        }

        $session->save();
    }

    public function getCurrentJackpotValue(Request $request)
    {
        $game = $request->input('game');

        switch ($game) {
            case 'super_keno':
                return json_encode(Cache::get('kenoJackPot'));
                break;
            case 'double_keno':
                return json_encode(Cache::get('kenoDoubleJP'));
                break;

            default:
                // code...
                break;
        }
    }

    public function actionAddCredit(Request $request)
    {
        session_start();
        $session = (new Session())->where('uuid', '=', $request->input('sessionName'))->get()->first();

        // проверка последней вкладки
        if ($session === null) {
            return response()->json(["status" => "false", "message" => "ActiveUserSessionException"]);
        }
        $session = $this->openGameSession($request->input('sessionName'));

        if (Config::get('app.mod') !== 'dev' && $session->demo == 'false') {
            $_SESSION['eventID'] = Uuid::generate()->string;
            $eventID = $_SESSION['eventID'];

            $platformId = 1;
            if (isset($_GET['platformId'])) {
                $platformId = $_GET['platformId'];
            }
            $requestData = array(
                'token' => $_SESSION['token'],
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'direction' => 'debit',
                'platformId' => $platformId,
                'eventType' => 'BetPlacing',
                'amount' => 0,
                'extraInfo' => [],
                'eventID' => $eventID
            );

            // получение ответа от slot.pantera
            $responseMoveFunds = (new BridgeApiService())->moveFunds($requestData);

            // проверка активности сессии на сервер с мост-апи (http://slot.pantera.co.ua)
            $checkApiSession = json_decode($responseMoveFunds);
            if (!isset($checkApiSession->status)) {
                throw new BetPlacingAbortException(0, 0);
            }
            if ($checkApiSession->status === false) {
                throw new BetPlacingAbortException(0, 0);
            }
        }

        return response()->json(["status" => "true"]);
    }

    public function actionGetJackpot(Request $request)
    {
        $session = $this->openGameSession($request->input('sessionName'));
        $allWin = (new ElGalloSpinService())->getWinJackpot();
        $this->saveGameSession($session, []);

        return $allWin;
    }

    public function actionCreateZeroBalanceCronTask(Request $request)
    {
        $userId = $request->input('userId');
        $gameId = $request->input('gameId');
        $token = $request->input('token');

        // создание cron задачи
        $cronTask = new CronTask;
        $cronTask->type = 'user_have_zero_balance';
        $cronTask->data = serialize(['userId' => $userId, 'gameId' => $gameId, 'token' => $token]);
        $cronTask->save();

        return response()->json(["status" => "true"]);
    }

    public function getUserBalance(Request $request)
    {
        $userId = $request->input('userId');
        $gameId = $request->input('gameId');
        $token = $request->input('token');

        // получения ответа с балансом от bridgeApi
        $response = json_decode((new BridgeApiService())->getBalance($token, $userId, $gameId));

        if (isset($response->balance)) {
            $balance = (float) $response->balance;
        } else {
            return response()->json(["status" => "false", "message" => "WrongResponseFromBridgeApi"]);
        }

        return response()->json(["status" => "true", "balance" => $balance]);
    }
}
