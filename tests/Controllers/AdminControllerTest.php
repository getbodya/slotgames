<?php

use App\Http\Controllers\Api\AdminController;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\GameServices\ElGalloSpinService;
use App\Models\Jackpot;
use App\Models\Stat;
use App\Models\UserJackpot;
use App\Models\Session;
use App\Models\BridgeApiRequest;
use Webpatser\Uuid\Uuid;

class AdminControllerTest extends TestCase
{
    private $gameEG;
    private $gameLOL;
    private $gameLOL2;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->gameEG = new \App\Services\GameServices\ElGalloSpinService();
        $this->gameLOL = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $this->gameLOL2 = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
    }

    /***********************************************************************
    * Тесты симуляции El Gallo
    ************************************************************************/

    /**
    * Тест симуляции. Total spin в full режиме.
    * Делается ход результатом которого является проигрышь.
    * Итог: спин в симуляции увеличивается на 1
    */
    public function testSimulationEGTotalSpin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // проигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['spinCount'] !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест выполняет ряд ходов и считает общий выигрышь, который вычисляет симуляция
     * Делается сравнение с известным Значение
     * Итог: значения вычесленное кодом и посчитанное руками совпадают
     */
    public function testSimulationEGTotalAllWin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $testInfoSet = [
                        	[1,2,2,1,7,8,1,7,8,4,2,8,2,11,1], // 5.3
                        	[0,11,10,5,10,0,4,8,0,10,8,5,5,3,11], // 4.5
                        	[8,4,7,8,7,3,1,10,3,0,11,8,9,7,5], // 0
                        	[6,4,7,5,9,1,8,11,10,5,4,1,5,4,8], // 0
                        	[3,0,11,7,11,0,3,0,8,9,4,10,11,0,1], // 5.7
                        	[7,11,9,8,4,7,3,0,6,7,1,5,0,9,10], // 0
                        	[11,0,3,6,11,7,5,8,6,4,6,10,4,11,7], // 0.5
                        	[4,8,10,9,11,7,8,10,7,4,9,7,3,9,6], // 0
                        	[11,5,10,7,11,10,0,5,6,11,4,8,7,6,3], // 0.5
                        	[1,10,4,7,6,4,0,4,3,11,10,1,9,11,0], // 2
                        	[1,9,5,6,1,0,8,1,7,1,9,6,11,0,10], // 1.2
                        	[1,10,9,0,4,1,6,0,9,7,0,6,11,10,1], // 2.8
                        	[5,3,6,9,4,5,3,10,0,3,1,8,1,11,10], // 0
                        	[5,6,1,9,7,5,1,7,4,9,5,0,1,10,4], // 0
                        	[11,0,8,9,1,6,0,3,6,7,11,4,7,11,8], // 2.2
                        	[1,8,4,4,5,7,0,4,8,7,1,4,1,10,6], // 0
                        ];

        $allWin = 0;
        foreach ($testInfoSet as $key => $info) {
            $result = $this->gameEG->getSpinResultData(0.01, 25, 'elGallo', $info);
            if ($key === 0) {
                $_SESSION['freeSpinData'] = [
                    'count' => 15,
                    'mul' => 2,
                    'allWin' => 5.3,
                    'repeat' => false
                ];
            }

            $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
            $allWin += $simulationResult['freeSpinAllWin'];

        }

        if ($allWin * 100 !== 5270.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    public function testSimulationEGTotalAllWin2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $testInfoSet = [[1,1,1,6,6,6,3,3,3,4,4,4,5,5,5], // 0
                        [1,1,1,6,6,6,3,3,3,2,4,4,5,5,2], // 0.5
                        [7,8,3,7,4,3,1,4,3,5,7,6,4,7,3], // 0.15
                        [7,8,3,7,4,3,1,4,3,5,7,2,4,7,2], // 0.65
                        [1,9,10,1,7,8,1,7,8,4,2,8,2,11,1], // 0.75
                        [4,2,5,7,0,6,0,4,11,2,5,6,0,3,10], // 3.50
                        [1,2,0,4,5,6,0,1,8,11,2,7,8,7,0], // 0.9
                        [2,8,3,3,0,4,1,8,3,2,4,7,7,11,8], // 0.9
                        [7,5,10,10,5,3,4,2,6,6,2,7,8,0,6], // 0.5
                        [11,5,1,11,2,5,4,2,11,5,1,0,1,11,5], // 0.5
                        [1,5,9,4,2,11,6,2,10,7,9,1,0,3,6], // 0.5
                        [8,4,0,2,6,3,5,3,6,0,2,3,11,6,3], // 2
                        [0,7,3,11,0,1,10,8,4,8,10,11,2,5,7], // 3.2
                        [11,9,7,7,3,2,1,8,3,0,7,6,3,10,5], // 0
                        [8,5,4,5,4,6,6,8,11,0,3,8,1,8,5], // 0
                        [3,2,11,0,1,8,3,7,5,9,4,10,11,6,10], // 0.15
                        [0,9,5,2,9,11,8,11,6,10,8,9,11,1,10], // 0
                        [6,10,7,11,0,8,3,4,6,11,5,7,6,8,2], // 0.25
                        [5,8,1,9,4,0,10,8,11,6,8,9,8,4,6], // 0.1
                        [7,4,3,10,9,4,11,6,10,10,7,11,11,7,10], // 0
                        [11,1,7,0,8,9,11,1,10,3,5,2,0,2,4], // 1
                        [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3], // 0.15
                        [4,6,11,8,4,2,9,10,0,7,5,11,9,11,0], // 0.5
                        [6,8,1,11,3,6,0,2,4,0,5,10,4,5,6], // 0.25
                        [11,2,6,11,3,7,8,4,9,11,8,10,8,7,10], // 0
                        [7,9,6,4,11,3,11,9,2,11,2,7,1,9,0], // 0.5
                        [9,6,2,8,11,6,7,5,9,0,11,5,3,0,11], // 0
                        ];

        $allWin = 0;
        foreach ($testInfoSet as $info) {
            $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', $info);
            $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

            $allWin += $simulationResult['allWinOnMainLocation'];
        }

        if ($allWin !== 1700.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Когда выпадает фриспин в full режиме, то Total spin не увеличивается за все время фриспинов
    * кол-во спинов в симуляции не увеличивается из фриспинов
    * Итог: кол-во спинов увеличивается на 1 (из-за хода на котором выпал фриспин)
    */
    public function testSimulationEGTotalSpin2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // проигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['spinCount'] !== 1) {
            $check = false;
        }

        $simulationResult = [];
        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $resultFS = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [4,4,4,5,5,5,6,6,6,8,8,8,9,9,9]);
            $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

            if ($simulationResult['spinCount'] !== 0) {
                $check = false;
            }
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Total spin в full режиме.
    * Делается ход результатом которого является выпадение джекпота.
    * Общее кол-во спинов в симуляции увеличивается на 1.
    */
    public function testSimulationEGTotalSpin4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();
        // проигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['spinCount'] !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Total bet в full режиме.
    * Делается ход результатом которого является выигрышь по линии.
    * Итог: Сумма ставок увеличивается после хода.
    */
    public function testSimulationEGTotalBet1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['allBet'] != 25) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Total bet в full режиме.
    * Находу выпадает фриспин и они полуностью прокручиваются.
    * Итог: Сумма ставок увеличивается после фриспинов только на ставку, которая была на ходу, на котором выпал фриспин
    */
    public function testSimulationEGTotalBet2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['allBet'] != 25) {
            $check = false;
        }

        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $result2 = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
            $simulationResult2 = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

            if ($simulationResult2['allBet'] !== 0) {
                $check = false;
            }
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Total win в full режиме.
    * Делается ход результатом которого является проигрышь.
    * Итог: Общий выигрышь не растет по всем возможным выигрышам (main, jackpot, freespin)
    */
    public function testSimulationEGTotalWimAfterSpin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'] + $simulationResult['jackpotWinnings']) != 0.00) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Total win в full режиме.
    * Делается ход результатом которого является выигрыш по линии.
    * Итог: Общий выигрышь (main) растет и не растут jackpot и freeSpin
    */
    public function testSimulationEGTotalWimAfterSpin2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['allWinOnMainLocation'] !== 15.00 &&
            $simulationResult['freeSpinAllWin'] != 0 &&
            $simulationResult['jackpotWinnings'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Total win в full режиме.
    * Делается ход результатом которого является выигрыш по линии и выпадение фриспинов
    * Итог: Общий выигрышь правильно растет с учетом выигрыша на фриспинах и линии
    */
    public function testSimulationEGTotalWimAfterSpin3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['allWinOnMainLocation'] != 0 &&
            $simulationResult['freeSpinAllWin'] != 0 &&
            $simulationResult['jackpotWinnings'] != 0) {
            $check = false;
        }

        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $result2 = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
            $simulationResult2 = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        }

        if ($simulationResult2['allWinOnMainLocation'] != 0 &&
            $simulationResult2['freeSpinAllWin'] != (525.0 + $result['freeSpinData']['count'] * 15 * $result['freeSpinData']['mul']) &&
            $simulationResult2['jackpotWinnings'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Total win в full режиме.
    * Делается ход результатом которого является выигрыш по линии и выпадение джекпота
    * Итог: Общий выигрышь правильно увеличивается с учетом выигрыша на джекпоте и линии
    */
    public function testSimulationEGTotalWimAfterSpin4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotWinnings'] != 23.13 &&
            $simulationResult['jackpotWinnings'] != 15.0)
        {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of winning spins в full режиме.
    * Делается ход результатом которого является проигрышь
    * Итог: кол-выигрышных спинов не увеличивается
    */
    public function testSimulationEGNumberOfWinningSpins1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfWinningSpins'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of winning spins в full режиме.
    * Делается ход результатом которого является выигрышь по линии
    * Итог: кол-выигрышных спинов увеличивается
    */
    public function testSimulationEGNumberOfWinningSpins2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfWinningSpins'] !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of winning spins в full режиме.
    * Делается ход результатом которого является выигрышь по линии и выпадение фриспина
    * Итог: кол-выигрышных спинов увеличивается на 1 и не учитываются выигрышные спины за фриспины
    */
    public function testSimulationEGNumberOfWinningSpins4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfWinningSpins'] !== 1) {
            $check = false;
        }

        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $result2 = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
            $simulationResult2 = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

            if ($simulationResult2['numberOfWinningSpins'] !== 0) {
                $check = false;
            }
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of losing spins в full режиме.
    * Делается ход результатом которого является выигрышь по линии.
    * Итог: кол-во lose ходов остается = 0
    */
    public function testSimulationEGNumberOfLosingSpins()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfLosingSpins'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of jackpots в full режиме.
    * На ходу не выпадает джекпот
    * Итог: кол-во джекпотов не изменяется
    */
    public function testSimulationEGNumberOfJackpots1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfJackpots'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of jackpots в full режиме.
    * На ходу выпадает джекпот
    * Итог: кол-во джекпотов изменяется
    */
    public function testSimulationEGNumberOfJackpots2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 33;
        $jackpotDB->result_mini = 33;
        $jackpotDB->save();

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfJackpots'] !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of free spins в full режиме.
    * На ходу не выпадает фриспина
    * Итог: кол-во фриспинов не изменяется
    */
    public function testSimulationEGNumberOfFreeSpins1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['countFreeSpin'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. Number of free spins в full режиме.
    * На ходу выпадает фриспина и они прокручивается.
    * Итог: кол-во фриспинов изменяется на 1.
    */
    public function testSimulationEGNumberOfFreeSpins2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $countFreeSpin = 0;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        $countFreeSpin += $simulationResult['countFreeSpin'];

        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $result2 = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
            $simulationResult2 = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
            $countFreeSpin += $simulationResult2['countFreeSpin'];
        }

        if ($countFreeSpin !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on main game в full режиме.
    * На ходу не выпадает выигрышь
    * Итог: Статистика выигрышей в основной игре не изменяется
    */
    public function testSimulationEGMoneyReturnedOnMainGame1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['allWinOnMainLocation'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on main game в full режиме.
    * На ходу выпадает выигрышь по линии
    * Итог: Статистика выигрышей в основной игре изменяется
    */
    public function testSimulationEGMoneyReturnedOnMainGame2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

        if ($simulationResult['allWinOnMainLocation'] !== 15.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on main game в full режиме.
    * На ходу выпадает джекпот
    * Итог: Статистика выигрышей на jackpot в основной игре изменяется
    */
    public function testSimulationEGMoneyReturnedOnMainGame4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotWinnings'] !== 23.13) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on main game в full режиме.
    * На ходу выпадает фриспин.
    * Итог: Статистика выигрышей по основной игре не изменяется
    */
    public function testSimulationEGMoneyReturnedOnMainGame5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['allWinOnMainLocation'] != 0) {
            $check = false;
        }

        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $result2 = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
            $simulationResult2 = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

            if ($simulationResult2['allWinOnMainLocation'] != 0) {
                $check = false;
            }
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on freespin в full режиме.
    * На ходу не выпадает freespin.
    * Итог: статистика денег полученных на фриспинах не изменяется
    */
    public function testSimulationEGMoneyReturnedOnFreespin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeSpinAllWin'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on freespin в full режиме.
    * На ходу выпадает freespin.
    * Итог: статистика денег полученных на фриспинах изменяется после прокрутки всех бесплатных ходов
    */
    public function testSimulationEGMoneyReturnedOnFreespin2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeSpinAllWin'] != 0) {
            $check = false;
        }

        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $result2 = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
            $simulationResult2 = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        }

        if ($simulationResult2['freeSpinAllWin'] == 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on jackpots в full режиме.
    * На ходу не выпадает джекпот
    * Итог: статистика выигрышей полученных от джекотов не изменяется
    */
    public function testSimulationEGMoneyReturnedOnJackpots1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotWinnings'] != 0.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. money returned on jackpots в full режиме.
    * На ходу выпадает джекпот
    * Итог: статистика выигрышей полученных от джекотов изменяется
    */
    public function testSimulationEGMoneyReturnedOnJackpots2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotWinnings'] !== 23.13) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Делаются ходы и сравнимается равенсто данных статистики и симуляции
     * Итого: Данные в симуляции и статистике одинаковые
     */
    public function testSimulationEGSimulationAndStatictics1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

        if ($simulationResult['allBet'] != $stat->sum_bet) {
            $check = false;
        }
        if ($simulationResult['allWinOnMainLocation'] != $stat->money_returned_on_main_game) {
            $check = false;
        }
        if ($simulationResult['numberOfLosingSpins'] != $stat->number_of_losing_spins) {
            $check = false;
        }
        if ($simulationResult['numberOfWinningSpins'] != $stat->number_of_winning_spins) {
            $check = false;
        }
        if ($simulationResult['countFreeSpin'] != $stat->number_of_bonus_game) {
            $check = false;
        }
        if ($simulationResult['freeSpinAllWin'] != $stat->money_returned_on_bonus_game) {
            $check = false;
        }
        if ($simulationResult['jackpotWinnings'] != $stat->total_jackpot_winning) {
            $check = false;
        }
        if ($simulationResult['spinCount'] != $stat->iteration_count) {
            $check = false;
        }
        if ($simulationResult['numberOfJackpots'] != $stat->number_of_jackpots) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Делаются ходы и сравнимается равенсто данных статистики и симуляции
     * Итого: Данные в симуляции и статистике одинаковые
     */
    public function testSimulationEGSimulationAndStatictics2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $сombinationStats = [[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]];
        $jackpotArray = ['MINI' => 0, 'MINOR' => 0, 'MAJOR' => 0, 'BIG_DADDY' => 0];

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        // прибавляем выигрышные комбинации
        foreach ($simulationResult['сombinationStats'] as $key => $valueArray) {
            foreach ($valueArray as $key2 => $value2) {
                $сombinationStats[$key][$key2] += $value2;
            }
        }

        // прибавляем джекпоты
        foreach ($simulationResult['jackpotArray'] as $key => $value) {
            $jackpotArray[$key] += $value;
        }

        $countFreeSpin = $simulationResult['countFreeSpin'];
        $freeSpinItrCounter = $simulationResult['freeSpinItrCounter'];
        $freeSpinAllWin = $simulationResult['freeSpinAllWin'];
        $jackpotWinnings = $simulationResult['jackpotWinnings'];
        $allWinOnMainLocation = $simulationResult['allWinOnMainLocation'];
        $galloCount = $simulationResult['galloCount'];
        $allBet = $simulationResult['allBet'];
        $numberOfWinningSpins = $simulationResult['numberOfWinningSpins'];
        $numberOfLosingSpins = $simulationResult['numberOfLosingSpins'];
        $spinCount = $simulationResult['spinCount'];
        $numberOfJackpots = $simulationResult['numberOfJackpots'];

        if ($simulationResult['freeSpinAllWin'] != 0) {
            $check = false;
        }

        for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
            $result2 = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
            $simulationResult2 = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
            // прибавляем выигрышные комбинации
            foreach ($simulationResult2['сombinationStats'] as $key => $valueArray) {
                foreach ($valueArray as $key2 => $value2) {
                    $сombinationStats[$key][$key2] += $value2;
                }
            }

            // прибавляем джекпоты
            foreach ($simulationResult2['jackpotArray'] as $key => $value) {
                $jackpotArray[$key] += $value;
            }

            $countFreeSpin += $simulationResult2['countFreeSpin'];
            $freeSpinItrCounter += $simulationResult2['freeSpinItrCounter'];
            $freeSpinAllWin += $simulationResult2['freeSpinAllWin'];
            $jackpotWinnings += $simulationResult2['jackpotWinnings'];
            $allWinOnMainLocation += $simulationResult2['allWinOnMainLocation'];
            $galloCount += $simulationResult2['galloCount'];
            $allBet += $simulationResult2['allBet'];
            $numberOfWinningSpins += $simulationResult2['numberOfWinningSpins'];
            $numberOfLosingSpins += $simulationResult2['numberOfLosingSpins'];
            $spinCount += $simulationResult2['spinCount'];
            $numberOfJackpots += $simulationResult2['numberOfJackpots'];
        }

        $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

        if ($allBet != $stat->sum_bet) {
            $check = false;
        }
        if ($allWinOnMainLocation != $stat->money_returned_on_main_game) {
            $check = false;
        }
        if ($numberOfLosingSpins != $stat->number_of_losing_spins) {
            $check = false;
        }
        if ($numberOfWinningSpins != $stat->number_of_winning_spins) {
            $check = false;
        }
        if ($countFreeSpin != $stat->number_of_bonus_game) {
            $check = false;
        }
        if ($freeSpinAllWin != $stat->money_returned_on_bonus_game) {
            $check = false;
        }
        if ($jackpotWinnings != $stat->total_jackpot_winning) {
            $check = false;
        }
        if ($spinCount != $stat->iteration_count) {
            $check = false;
        }
        if ($numberOfJackpots != $stat->number_of_jackpots) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Делается ход на котором выпадает джекпот и сравнимается равенсто данных статистики и симуляции
     * Итого: Данные в симуляции и статистике одинаковые
     */
    public function testSimulationEGSimulationAndStatictics3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

        if ($simulationResult['allBet'] != $stat->sum_bet) {
            $check = false;
        }
        if ($simulationResult['allWinOnMainLocation'] != $stat->money_returned_on_main_game) {
            $check = false;
        }
        if ($simulationResult['numberOfLosingSpins'] != $stat->number_of_losing_spins) {
            $check = false;
        }
        if ($simulationResult['numberOfWinningSpins'] != $stat->number_of_winning_spins) {
            $check = false;
        }
        if ($simulationResult['countFreeSpin'] != $stat->number_of_bonus_game) {
            $check = false;
        }
        if ($simulationResult['freeSpinAllWin'] != $stat->money_returned_on_bonus_game) {
            $check = false;
        }
        if ($simulationResult['jackpotWinnings'] != $stat->total_jackpot_winning) {
            $check = false;
        }
        if ($simulationResult['spinCount'] != $stat->iteration_count) {
            $check = false;
        }
        if ($simulationResult['numberOfJackpots'] != $stat->number_of_jackpots) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * При проигрыше на первом ходу в фриспинах, делается увеличивается $simulationResult['freeSpinLoseSpinCount']
     * Остается = 0 значение $simulationResult['freeSpinWinSpinCount']
     */
    public function testSimulationEGFreeSpinWinSpinCountAndFreeSpinLoseSpinCount1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,2,3,7,2,3,1,4,3,2,7,6,4,7,3]);
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,7,3,3,3,4,4,4,5,5,5,6,6,6]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeSpinLoseSpinCount'] != 1) {
            $check = false;
        }

        if ($simulationResult['freeSpinWinSpinCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * При выигрыше на первом ходу в фриспинах, делается увеличивается $simulationResult['freeSpinLoseSpinCount']
     * Остается = 0 значение $simulationResult['freeSpinWinSpinCount']
     */
    public function testSimulationEGFreeSpinWinSpinCountAndFreeSpinLoseSpinCount2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,2,3,7,2,3,1,4,3,2,7,6,4,7,3]);
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,3,7,3,3,3,4,3,4,5,5,5,6,6,6]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeSpinLoseSpinCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['freeSpinWinSpinCount'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * При выигрыше на последнем ходу в фриспинах, делается увеличивается $simulationResult['freeSpinLoseSpinCount']
     * Остается = 0 значение $simulationResult['freeSpinWinSpinCount']
     */
    public function testSimulationEGFreeSpinWinSpinCountAndFreeSpinLoseSpinCount3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,2,3,7,2,3,1,4,3,2,7,6,4,7,3]);

        $_SESSION['freeSpinData']['count'] = 5;

        for ($i=0; $i < 5; $i++) {
            $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,3,7,3,3,3,4,3,4,5,5,5,6,6,6]);
        }
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeSpinLoseSpinCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['freeSpinWinSpinCount'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * При проигрыше на последнем ходу в фриспинах, делается увеличивается $simulationResult['freeSpinLoseSpinCount']
     * Остается = 0 значение $simulationResult['freeSpinWinSpinCount']
     */
    public function testSimulationEGFreeSpinWinSpinCountAndFreeSpinLoseSpinCount4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,2,3,7,2,3,1,4,3,2,7,6,4,7,3]);

        $_SESSION['freeSpinData']['count'] = 5;

        for ($i=0; $i < 5; $i++) {
            $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [7,7,7,3,3,3,4,3,4,5,5,5,6,6,6]);
        }
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeSpinLoseSpinCount'] != 1) {
            $check = false;
        }

        if ($simulationResult['freeSpinWinSpinCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры отсутствуют
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,3,3,8,8,8,9,9,9,6,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры выпадают
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,0,3,8,0,8,9,9,9,0,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 3) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры отсутствуют и выпадают фриспины.
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,3,8,2,8,9,9,9,2,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры выпадают и выпадают фриспины.
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,0,8,2,8,0,9,9,2,0,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 3) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры не выпадают на первом ходу фриспинов.
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,0,8,2,8,0,9,9,2,0,6,5,5,5]);
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,3,3,8,8,8,9,9,9,6,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры выпадают на первом ходу фриспинов.
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,0,8,2,8,0,9,9,2,0,6,5,5,5]);
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,0,3,8,8,0,9,9,9,6,0,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 3) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры выпадают на последнем ходу фриспинов.
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,0,8,2,8,0,9,9,2,0,6,5,5,5]);

        $_SESSION['freeSpinData']['count'] = 5;

        for ($i=0; $i < 5; $i++) {
            $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,0,3,8,8,0,9,9,9,6,0,6,5,5,5]);
        }

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 3) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва джокерных символов в основной игре и в фриспин игре.
     * Джокеры выпадают на последнем ходу фриспинов.
     */
    public function testJokerInMainGameCountAndJokerInFreeGameCount8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,0,8,2,8,0,9,9,2,0,6,5,5,5]);

        $_SESSION['freeSpinData']['count'] = 5;

        for ($i=0; $i < 5; $i++) {
            $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,3,3,8,8,8,9,9,9,6,6,6,5,5,5]);
        }

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jokerInMainGameCount'] != 0) {
            $check = false;
        }

        if ($simulationResult['jokerInFreeGameCount'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва петухов символов в основной игре.
     * Петухи не выпадают
     */
    public function testDroppedGalloInMaiGame1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,3,3,8,8,8,9,9,9,6,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['galloCounterForSpins'][0] != 1) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва петухов символов в основной игре.
     * Выпадает один питух
     */
    public function testDroppedGalloInMaiGame2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,3,8,8,8,9,9,9,6,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['galloCounterForSpins'][0] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][1] != 1) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва петухов символов в основной игре.
     * Выпадает два питуха
     */
    public function testDroppedGalloInMaiGame3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,3,2,8,8,9,9,9,6,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['galloCounterForSpins'][0] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][2] != 1) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва петухов символов в основной игре.
     * Выпадает три питуха
     */
    public function testDroppedGalloInMaiGame4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,3,2,8,8,2,0,9,6,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['galloCounterForSpins'][0] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][3] != 1) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва петухов символов в основной игре.
     * Выпадает три питуха
     */
    public function testDroppedGalloInMaiGame5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [3,2,3,2,8,8,2,0,9,2,6,6,5,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['galloCounterForSpins'][0] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][4] != 1) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва петухов символов в основной игре.
     * Выпадает три питуха
     */
    public function testDroppedGalloInMaiGame6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(0.01, 25, 'elGallo', [3,2,3,2,8,8,2,0,9,2,6,6,2,5,5]);

        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['galloCounterForSpins'][0] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['galloCounterForSpins'][5] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва diablo символов в выпадющих в фриспин игре. (мак и мин кол-во)
     * Когда не выпадают.
     */
    public function testMaxDroppedAndMinDroppedDiabloInFreeSpin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(0.01, 25, 'elGallo', [3,2,3,2,8,8,2,2,9,2,6,6,2,5,5]);

        $_SESSION['freeSpinData']['count'] = 5;
        for ($i=0; $i < 5; $i++) {
            $result = $this->gameEG->getSpinResultData(0.01, 25, 'elGallo', [3,3,3,8,8,8,7,7,7,2,6,6,5,5,5]);
            $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        }

        if ($_SESSION['minDiabloValueInFreeSpinGame'] != 0) {
            $check = false;
        }
        if ($_SESSION['maxDiabloValueInFreeSpinGame'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест симуляции.
     * Подсчет кол-ва diablo символов в выпадющих в фриспин игре. (мак и мин кол-во)
     * Когда выпадают по одному за ход.
     */
    public function testMaxDroppedAndMinDroppedDiabloInFreeSpin2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->gameEG->getSpinResultData(0.01, 25, 'elGallo', [3,2,3,2,8,8,2,2,9,2,6,6,2,5,5]);

        $_SESSION['freeSpinData']['count'] = 5;
        for ($i=0; $i < 5; $i++) {
            $result = $this->gameEG->getSpinResultData(0.01, 25, 'elGallo', [3,3,0,8,8,8,7,7,7,2,6,6,5,5,5]);
            $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);
        }

        if ($_SESSION['minDiabloValueInFreeSpinGame'] != 5) {
            $check = false;
        }
        if ($_SESSION['maxDiabloValueInFreeSpinGame'] != 5) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест проверяющий ведение статистики выпадения mini джекпотов
    */
    public function testSimulationEGJackpotCount1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();
        // проигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotCountsArray']['MINI'] !== 1) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MINOR'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MAJOR'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['BIG_DADDY'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест проверяющий ведение статистики выпадения minor джекпотов
    */
    public function testSimulationEGJackpotCount2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->minor = 50;
        $jackpotDB->result_minor = 50;
        $jackpotDB->save();
        // проигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotCountsArray']['MINI'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MINOR'] !== 1) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MAJOR'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['BIG_DADDY'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест проверяющий ведение статистики выпадения minor джекпотов
    */
    public function testSimulationEGJackpotCount3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->major = 250;
        $jackpotDB->result_major = 250;
        $jackpotDB->save();
        // проигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotCountsArray']['MINI'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MINOR'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MAJOR'] !== 1) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['BIG_DADDY'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест проверяющий ведение статистики выпадения minor джекпотов
    */
    public function testSimulationEGJackpotCount4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->big_daddy = 500;
        $jackpotDB->result_big_daddy = 500;
        $jackpotDB->save();
        // проигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['jackpotCountsArray']['MINI'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MINOR'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['MAJOR'] !== 0) {
            $check = false;
        }
        if ($simulationResult['jackpotCountsArray']['BIG_DADDY'] !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. numberOfFreeGamesAwardedCounter в full режиме.
    * На ходу не выпадает фриспин игра
    * Итог: numberOfFreeGamesAwardedCounter не увеличивается
    */
    public function testSimulationEGNumberOfFreeGamesAwardedCounter1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfFreeGamesAwardedCounter'][10] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][15] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][20] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][25] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][30] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. numberOfFreeGamesAwardedCounter в full режиме.
    * На ходу выпадает фриспин игра с 10 бесплатными кручениями
    * Итог: numberOfFreeGamesAwardedCounter увеличивается для 10
    */
    public function testSimulationEGNumberOfFreeGamesAwardedCounter2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['count'] = 10;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfFreeGamesAwardedCounter'][10] != 1) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][15] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][20] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][25] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][30] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. numberOfFreeGamesAwardedCounter в full режиме.
    * На ходу выпадает фриспин игра с 15 бесплатными кручениями
    * Итог: numberOfFreeGamesAwardedCounter увеличивается для 15
    */
    public function testSimulationEGNumberOfFreeGamesAwardedCounter3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['count'] = 15;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfFreeGamesAwardedCounter'][10] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][15] != 1) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][20] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][25] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][30] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. numberOfFreeGamesAwardedCounter в full режиме.
    * На ходу выпадает фриспин игра с 20 бесплатными кручениями
    * Итог: numberOfFreeGamesAwardedCounter увеличивается для 20
    */
    public function testSimulationEGNumberOfFreeGamesAwardedCounter4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['count'] = 20;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfFreeGamesAwardedCounter'][10] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][15] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][20] != 1) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][25] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][30] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. numberOfFreeGamesAwardedCounter в full режиме.
    * На ходу выпадает фриспин игра с 25 бесплатными кручениями
    * Итог: numberOfFreeGamesAwardedCounter увеличивается для 25
    */
    public function testSimulationEGNumberOfFreeGamesAwardedCounter5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['count'] = 25;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfFreeGamesAwardedCounter'][10] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][15] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][20] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][25] != 1) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][30] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. numberOfFreeGamesAwardedCounter в full режиме.
    * На ходу выпадает фриспин игра с 30 бесплатными кручениями
    * Итог: numberOfFreeGamesAwardedCounter увеличивается для 30
    */
    public function testSimulationEGNumberOfFreeGamesAwardedCounter6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['count'] = 30;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['numberOfFreeGamesAwardedCounter'][10] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][15] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][20] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][25] != 0) {
            $check = false;
        }
        if ($simulationResult['numberOfFreeGamesAwardedCounter'][30] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. freeGameMultipliersCounter в full режиме.
    * На ходу не выпадает фриспин игра
    * Итог: freeGameMultipliersCounter не увеличивается
    */
    public function testSimulationEGFreeGameMultipliersCounter1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeGameMultipliersCounter'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. freeGameMultipliersCounter в full режиме.
    * На ходу выпадает фриспин игра с множителем 1
    * Итог: freeGameMultipliersCounter увеличивается для 1
    */
    public function testSimulationEGFreeGameMultipliersCounter2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['mul'] = 1;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeGameMultipliersCounter'][1] != 1) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. freeGameMultipliersCounter в full режиме.
    * На ходу выпадает фриспин игра с множителем 2
    * Итог: freeGameMultipliersCounter увеличивается для 2
    */
    public function testSimulationEGFreeGameMultipliersCounter3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['mul'] = 2;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeGameMultipliersCounter'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][2] != 1) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. freeGameMultipliersCounter в full режиме.
    * На ходу выпадает фриспин игра с множителем 3
    * Итог: freeGameMultipliersCounter увеличивается для 3
    */
    public function testSimulationEGFreeGameMultipliersCounter4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['mul'] = 3;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeGameMultipliersCounter'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][3] != 1) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. freeGameMultipliersCounter в full режиме.
    * На ходу выпадает фриспин игра с множителем 4
    * Итог: freeGameMultipliersCounter увеличивается для 4
    */
    public function testSimulationEGFreeGameMultipliersCounter5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['mul'] = 4;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeGameMultipliersCounter'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][4] != 1) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
    * Тест симуляции. freeGameMultipliersCounter в full режиме.
    * На ходу выпадает фриспин игра с множителем 5
    * Итог: freeGameMultipliersCounter увеличивается для 5
    */
    public function testSimulationEGFreeGameMultipliersCounter6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // выигрышь
        $result = $this->gameEG->getSpinResultData(1, 25, 'elGallo', [2,1,1,6,6,2,3,3,3,2,4,4,5,5,5]);
        $result['freeSpinData']['mul'] = 5;
        $simulationResult = (new App\Http\Controllers\Api\AdminController)->getResultSimulationEG($result, $this->gameEG);

        if ($simulationResult['freeGameMultipliersCounter'][1] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][2] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][3] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][4] != 0) {
            $check = false;
        }
        if ($simulationResult['freeGameMultipliersCounter'][5] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }






    /***********************************************************************
    * Тесты симуляции LifeOfLuxury
    ************************************************************************/
    /**
     * Тест подстчета кол-ва алмазов в фриспинах
     * Делается ход, на котором выпадают фриспины.
     * В фриспинах на каждом ходу выпадает алмаз.
     * Итог: кол-во алмазов выпавших в фриспинах = 10
     */
    public function testDiamsCountInLolSimulationInFreeSpinGame1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,2,3,1,3,3,5,2,0,9,6,9,3,1]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,2,3,1,3,3,5,2,0,9,6,9,3,1]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ($simulationResult['freeSpinDiams'] !== 20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест подстчета максимального кол-ва алмазов за один выпавший фриспин (10 бесплатных ходов)
     * Делается ход, на котором выпадают фриспины.
     * В фриспинах на каждом ходу выпадает 1 алмаз.
     * Итог: максимальное кол-во алмазов выпавших в за одни фриспины = 1
     */
    public function testMaxCountDiamsInLolSimulationInFreeSpinGame1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        for ($i=0; $i < 11; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,2,3,1,3,3,5,2,0,9,6,9,3,1]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        for ($i=0; $i < 11; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,2,3,1,3,3,5,2,0,9,6,9,3,1]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ($simulationResult['maxDiamValueInFreeSpinGame'] !== 10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест подстчета кол-ва алмазов в текущей фриспин игре
     * Делается ход, на котором выпадают фриспины.
     * Итог: кол-во алмазов в фриспин игре = 0, так как алмазы с хода на котором
     * выпали фриспины относятся к основной игре
     */
    public function testDiamsInCurrentFreeSpinGameInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['diamsInCurrentFreeSpinGame'] !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест подстчета кол-ва фриспин игр
     * Делается ход, на котором выпадают фриспины
     * Итог: кол-во фриспинов = 1
     */
    public function testCountFreeSpinGameInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['countFreeSpin'] !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест подстчета кол-ва фриспин игр
     * Делается ход, на котором выпадают фриспины и фриспины прокручиваются
     * Итог: кол-во фриспинов = 1
     */
    public function testCountFreeSpinGameInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 11; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,2,3,1,3,3,5,2,0,9,6,9,3,1]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ($simulationResult['countFreeSpin'] !== 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total bet в симуляции при совершении хода
     * в версии lifeOfLuxury
     */
    public function testTotalBetInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['allBet'] !== 15) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total bet в симуляции при совершении хода
     * в версии lifeOfLuxury2
     */
    public function testTotalBetInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['allBet'] !== 15) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total win в симуляции при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testTotalWinInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total win в симуляции при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testTotalWinInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total win в симуляции при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testTotalWinInLolSimulation3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total win в симуляции при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testTotalWinInLolSimulation4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total win в симуляции при выпадении выигрыша в основной игре
     * и прокручивании фриспинов с проигрышами
     * в версии lifeOfLuxury
     */
    public function testTotalWinInLolSimulation5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 30) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка Total win в симуляции при выпадении выигрыша в основной игре
     * и прокручивании фриспинов с проигрышами
     * в версии lifeOfLuxury2
     */
    public function testTotalWinInLolSimulation6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 30) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета общего кол-ва ходов (Spins Count)
     * в симуляции совершаемых в основной игре
     * в версии lifeOfLuxury
     */
    public function testSpinsCountInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['spinCount'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета общего кол-ва ходов (Spins Count)
     * в симуляции совершаемых в основной игре
     * в версии lifeOfLuxury2
     */
    public function testSpinsCountInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['spinCount'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета общего кол-ва ходов (Spins Count)
     * при выпадении фриспинов и прокручивании их с выигрышами
     * в версии lifeOfLuxury
     */
     public function testSpinsCountInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['spinCount'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета общего кол-ва ходов (Spins Count)
      * при выпадении фриспинов и прокручивании их с выигрышами
      * в версии lifeOfLuxury2
      */
      public function testSpinsCountInLolSimulation4()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          if ($simulationResult['spinCount'] != 1) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета кол-ва выигрышных ходов (Win Spins Count)
     * в симуляции при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testWinSpinsCountInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['numberOfWinningSpins'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва выигрышных ходов (Win Spins Count)
     * в симуляции при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testWinSpinsCountInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['numberOfWinningSpins'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва выигрышных ходов (Win Spins Count)
     * в симуляции при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testWinSpinsCountInLolSimulation3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['numberOfWinningSpins'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва выигрышных ходов (Win Spins Count)
     * в симуляции при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testWinSpinsCountInLolSimulation4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['numberOfWinningSpins'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва выигрышных ходов (Win Spins Count)
     * в симуляции при выпадении фриспинов в основной игре
     * в версии lifeOfLuxury
     */
     public function testWinSpinsCountInLolSimulation5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['numberOfWinningSpins'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета кол-ва выигрышных ходов (Win Spins Count)
      * в симуляции при выпадении фриспинов в основной игре
      * в версии lifeOfLuxury2
      */
      public function testWinSpinsCountInLolSimulation6()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          if ($simulationResult['numberOfWinningSpins'] != 1) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
     * при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testLoseSpinsCountInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['numberOfLosingSpins'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
     * при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testLoseSpinsCountInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['numberOfLosingSpins'] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
     * при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury
     */
     public function testLoseSpinsCountInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['numberOfLosingSpins'] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
      * при выпадении выигрыша в основной игре
      * в версии lifeOfLuxury2
      */
      public function testLoseSpinsCountInLolSimulation4()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          if ($simulationResult['numberOfLosingSpins'] != 0) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
     * при выпадении фриспинов в основной игре и их прокручивании с проигрышами
     * в версии lifeOfLuxury
     */
    public function testLoseSpinsCountInLolSimulation5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,4,10,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ($simulationResult['numberOfLosingSpins'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
     * при выпадении фриспинов в основной игре и их прокручивании с проигрышами
     * в версии lifeOfLuxury2
     */
    public function testLoseSpinsCountInLolSimulation6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,4,10,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ($simulationResult['numberOfLosingSpins'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
     * при выпадении фриспинов и их прокручивании с выигрышами
     * в версии lifeOfLuxury
     */
     public function testLoseSpinsCountInLolSimulation7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,4,10,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['numberOfLosingSpins'] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета кол-ва проигрышных ходов (Lose Spins Count)
      * при выпадении фриспинов и их прокручивании с выигрышами
      * в версии lifeOfLuxury2
      */
      public function testLoseSpinsCountInLolSimulation8()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,4,10,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          if ($simulationResult['numberOfLosingSpins'] != 0) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета общего выигрыша за несколько ходов в основной игре
     * (Win Spins Amount -> $allWinOnMainLocation)
     * при выпадении проигрыша в основной игре
     * в верисии lifeOfLuxury
     */
    public function testWinSpinsAmountInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['allWinOnMainLocation'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета общего выигрыша за несколько ходов в основной игре
     * (Win Spins Amount -> $allWinOnMainLocation)
     * при выпадении проигрыша в основной игре
     * в верисии lifeOfLuxury2
     */
    public function testWinSpinsAmountInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['allWinOnMainLocation'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета общего выигрыша за несколько ходов в основной игре
     * (Win Spins Amount)
     * при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testWinSpinsAmountInLolSimulation3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['allWinOnMainLocation'] != 20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета общего выигрыша за несколько ходов в основной игре
     * (Win Spins Amount)
     * при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testWinSpinsAmountInLolSimulation4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['allWinOnMainLocation'] != 20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета общего выигрыша за несколько ходов в основной игре
     * (Win Spins Amount) при выпадении фриспинов в основной игре
     * с проигрышами на всех бесплатных ходах
     * в версии lifeOfLuxury
     */
     public function testWinSpinsAmountInLolSimulation5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,5,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 30) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета общего выигрыша за несколько ходов в основной игре
      * (Win Spins Amount) при выпадении фриспинов в основной игре
      * с проигрышами на всех бесплатных ходах
      * в версии lifeOfLuxury2
      */
      public function testWinSpinsAmountInLolSimulation6()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,5,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 30) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета общего выигрыша за несколько ходов в основной игре
     * (Win Spins Amount) при выпадении фриспинов и их прокручивании
     * с выигрышами на всех бесплатных ходах
     * в версии lifeOfLuxury
     */
     public function testWinSpinsAmountInLolSimulation7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,5,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 430) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета общего выигрыша за несколько ходов в основной игре
      * (Win Spins Amount) при выпадении фриспинов и их прокручивании
      * с выигрышами на всех бесплатных ходах
      * в версии lifeOfLuxury2
      */
      public function testWinSpinsAmountInLolSimulation8()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,5,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          if (($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']) != 430) {
              $check = false;
          }

          $this->assertTrue($check);
      }


    /**
     * Проверка посчета кол-ва фриспин игр (Free Spins Count)
     * при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testFreeSpinsCountInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['countFreeSpin'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка посчета кол-ва фриспин игр (Free Spins Count)
     * при выпадении проигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testFreeSpinsCountInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['countFreeSpin'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка посчета кол-ва фриспин игр (Free Spins Count)
     * при выпадении фриспинов и их прокручивании с проигрышами
     * в версии lifeOfLuxury
     */
     public function testFreeSpinsCountInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['countFreeSpin'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении проигрыша в версии LifeOfLuxuryService
     */
    public function testPayoutInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'])) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении проигрыша в версии LifeOfLuxuryService2
     */
    public function testPayoutInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'])) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении выигрыша в версии LifeOfLuxuryService
     */
    public function testPayoutInLolSimulation3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'])) != 400) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении выигрыша в версии LifeOfLuxuryService2
     */
    public function testPayoutInLolSimulation4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'])) == 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении  фриспинов и их прокручивании с проигрышами во всех ходах
     * в версии LifeOfLuxuryService
     */
    public function testPayoutInLolSimulation5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult2 = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'])) != 200) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении  фриспинов и их прокручивании с проигрышами во всех ходах
     * в версии LifeOfLuxuryService2
     */
    public function testPayoutInLolSimulation6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'])) != 200) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении  фриспинов и их прокручивании с выигрышами на всех ходах
     * в версии LifeOfLuxuryService
     */
    public function testPayoutInLolSimulation7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']))) != 2867) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка общего процента возвращаемого слотом (PAYOUT)
     * при выпадении  фриспинов и их прокручивании с выигрышами на всех ходах
     * в версии LifeOfLuxuryService2
     */
    public function testPayoutInLolSimulation8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']))) != 2867) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении проигрыша в основной игре
     * в версии LifeOfLuxuryService
     */
    public function testPayoutBySpinsInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (round((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation']))) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении проигрыша в основной игре
     * в версии LifeOfLuxuryService2
     */
    public function testPayoutBySpinsInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (round((100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation']))) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury
     */
    public function testPayoutBySpinsInLolSimulation3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (round((100 / $simulationResult['allBet'] * $simulationResult['allWinOnMainLocation'])) != 400) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении выигрыша в основной игре
     * в версии lifeOfLuxury2
     */
    public function testPayoutBySpinsInLolSimulation4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if (round((100 / $simulationResult['allBet'] * $simulationResult['allWinOnMainLocation'])) != 400) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении фриспинов и их прокручивании со всеми проигрышами
     * в версии lifeOfLuxury
     */
    public function testPayoutBySpinsInLolSimulation5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['allWinOnMainLocation'])) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении фриспинов и их прокручивании со всеми проигрышами
     * в версии lifeOfLuxury2
     */
    public function testPayoutBySpinsInLolSimulation6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['allWinOnMainLocation'])) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении фриспинов и их прокручивании со всеми выигрышами
     * в версии lifeOfLuxury
     */
    public function testPayoutBySpinsInLolSimulation7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['allWinOnMainLocation'])) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого основной игрой (PAYOUT by Spins)
     * при выпадении фриспинов и их прокручивании со всеми выигрышами
     * в версии lifeOfLuxury2
     */
    public function testPayoutBySpinsInLolSimulation8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['allWinOnMainLocation'])) != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
     * при выпадении проигрыша в основной игре
     * в версии LifeOfLuxuryService
     */
     public function testPayoutByFreeSpinsInLolSimulation1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
      * при выпадении проигрыша в основной игре
      * в версии LifeOfLuxuryService2
      */
      public function testPayoutByFreeSpinsInLolSimulation2()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 0) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
     * при выпадении выигрыша в основной игре
     * в версии LifeOfLuxuryService
     */
     public function testPayoutByFreeSpinsInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
      * при выпадении выигрыша в основной игре
      * в версии LifeOfLuxuryService2
      */
      public function testPayoutByFreeSpinsInLolSimulation4()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 0) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
     * при выпадении  фриспинов и их прокручивании с проигрышами во всех ходах
     * в версии LifeOfLuxuryService
     */
    public function testPayoutByFreeSpinsInLolSimulation5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 200) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
     * при выпадении  фриспинов и их прокручивании с проигрышами во всех ходах
     * в версии LifeOfLuxuryService2
     */
    public function testPayoutByFreeSpinsInLolSimulation6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 200) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
     * при выпадении  фриспинов и их прокручивании с выигрышами во всех ходах
     * в версии LifeOfLuxuryService
     */
    public function testPayoutByFreeSpinsInLolSimulation7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 2867.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
     * при выпадении  фриспинов и их прокручивании с выигрышами во всех ходах
     * в версии LifeOfLuxuryService2
     */
    public function testPayoutByFreeSpinsInLolSimulation8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
        }

        if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 2867.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
     * при выпадении  фриспинов и их прокручивании с выигрышами во всех ходах
     * и выпадении одного алмаза на каждом ходу
     * в версии LifeOfLuxuryService
     */
     public function testPayoutByFreeSpinsInLolSimulation9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,0]);
             $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
         }

         if ($simulationResult['freeSpinAllWin'] != 1330) {
             $check = false;
         }

         if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 8867) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка процента возвращаемого фриспинами (PAYOUT by Free Spins)
      * при выпадении  фриспинов и их прокручивании с выигрышами во всех ходах
      * и выпадении одного алмаза на каждом ходу
      * в версии LifeOfLuxuryService2
      */
      public function testPayoutByFreeSpinsInLolSimulation10()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result2 = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,0]);
              $simulationResult = $adminController->getResultSimulationLOL($result2, $simulationResult);
          }

          if ($simulationResult['freeSpinAllWin'] != 1330) {
              $check = false;
          }

          if (round((100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin'])) != 8867) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в основной игре
     * (Diamonds in the main game)
     * при выпадении проигрыша и при отсутствии алмазов
     * в верисии LifeOfLuxuryService
     */
    public function testDiamondsInTheMainGameInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['diamsInMainGame'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в основной игре
     * (Diamonds in the main game)
     * при выпадении проигрыша и при отсутствии алмазов
     * в верисии LifeOfLuxuryService2
     */
    public function testDiamondsInTheMainGameInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['diamsInMainGame'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в основной игре
     * (Diamonds in the main game) при выпадении проигрыша и наличии алмаза
     * в версии LifeOfLuxuryService
     */
     public function testDiamondsInTheMainGameInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,0,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['diamsInMainGame'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета кол-ва алмазов выпавших в основной игре
      * (Diamonds in the main game) при выпадении проигрыша и наличии алмаза
      * в версии LifeOfLuxuryService2
      */
      public function testDiamondsInTheMainGameInLolSimulation4()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,0,3,3,4,4,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          if ($simulationResult['diamsInMainGame'] != 1) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в основной игре
     * (Diamonds in the main game) при выпадении фриспинов (из-за 3-х монет)
     * и выпадении алмазов в самих фриспинах
     */
     public function testDiamondsInTheMainGameInLolSimulation5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['diamsInMainGame'] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета кол-ва алмазов выпавших в основной игре
      * (Diamonds in the main game) при выпадении фриспинов (из-за 3-х монет)
      * и выпадении алмазов в самих фриспинах
      * в версии LifeOfLuxuryService2
      */
      public function testDiamondsInTheMainGameInLolSimulation6()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          if ($simulationResult['diamsInMainGame'] != 0) {
              $check = false;
          }

          $this->assertTrue($check);
      }

      /**
       * Проверка подсчета кол-ва алмазов выпавших в основной игре
       * (Diamonds in the main game) при выпадении фриспинов
       * из-за 1 монеты и 2-х алмазов
       * и выпадении алмазов в самих фриспинах
       * в версии LifeOfLuxuryService
       */
       public function testDiamondsInTheMainGameInLolSimulation7()
       {
           $check = true;
           $this->prepareTest();
           $_SESSION['test'] = true;
           $_SESSION['demo'] = false;
           $_SESSION['next0FSpin'] = false;
           $_SESSION['startFreeSpin'] = false;

           $simulationResult = $this->getStartSimulationResultForLol();

           $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
           $adminController = new \App\Http\Controllers\Api\AdminController;
           $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,3,3,3,4,0,4,5,5,5,0,6,6]);
           $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

           if ($simulationResult['diamsInMainGame'] != 2) {
               $check = false;
           }

           $this->assertTrue($check);
       }

       /**
        * Проверка подсчета кол-ва алмазов выпавших в основной игре
        * (Diamonds in the main game) при выпадении фриспинов
        * из-за 1 монеты и 2-х алмазов
        * и выпадении алмазов в самих фриспинах
        * в версии LifeOfLuxuryService2
        */
        public function testDiamondsInTheMainGameInLolSimulation8()
        {
            $check = true;
            $this->prepareTest();
            $_SESSION['test'] = true;
            $_SESSION['demo'] = false;
            $_SESSION['next0FSpin'] = false;
            $_SESSION['startFreeSpin'] = false;

            $simulationResult = $this->getStartSimulationResultForLol();

            $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
            $adminController = new \App\Http\Controllers\Api\AdminController;
            $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,3,3,3,4,0,4,5,5,5,0,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

            if ($simulationResult['diamsInMainGame'] != 2) {
                $check = false;
            }

            $this->assertTrue($check);
        }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в фриспинах
     * (Diamonds in the freespin game)
     * при выпадении прогирыша и без алмазов в основной игре
     * в версии LifeOfLuxuryService
     */
    public function testDiamondsInTheFreespinGameInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['freeSpinDiams'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в фриспинах
     * (Diamonds in the freespin game)
     * при выпадении прогирыша и без алмазов в основной игре
     * в версии LifeOfLuxuryService2
     */
    public function testDiamondsInTheFreespinGameInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['freeSpinDiams'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в фриспинах
     * (Diamonds in the freespin game)
     * при выпадении фриспинов из-за 2-х монет и алмаза
     * и прокручивании фриспинов без выпадения алмазов
     * в версии LifeOfLuxuryService
     */
     public function testDiamondsInTheFreespinGameInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,3,3,10,4,4,4,0,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinDiams'] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета кол-ва алмазов выпавших в фриспинах
      * (Diamonds in the freespin game)
      * при выпадении фриспинов из-за 2-х монет и алмаза
      * и прокручивании фриспинов без выпадения алмазов
      * в версии LifeOfLuxuryService2
      */
      public function testDiamondsInTheFreespinGameInLolSimulation4()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,3,3,10,4,4,4,0,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          if ($simulationResult['freeSpinDiams'] != 0) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка подсчета кол-ва алмазов выпавших в фриспинах
     * (Diamonds in the freespin game)
     * при выпадении фриспинов из-за 2-х монет и алмаза
     * и прокручивании фриспинов с выпадением алмазов
     * в версии LifeOfLuxuryService
     */
     public function testDiamondsInTheFreespinGameInLolSimulation5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,3,3,10,4,4,4,0,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,0,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinDiams'] != 10) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка подсчета кол-ва алмазов выпавших в фриспинах
      * (Diamonds in the freespin game)
      * при выпадении фриспинов из-за 2-х монет и алмаза
      * и прокручивании фриспинов с выпадением алмазов
      * в версии LifeOfLuxuryService2
      */
      public function testDiamondsInTheFreespinGameInLolSimulation6()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,3,3,10,4,4,4,0,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,0,2,3,3,3,4,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          if ($simulationResult['freeSpinDiams'] != 10) {
              $check = false;
          }

          $this->assertTrue($check);
      }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     * (Dropped coins in one spin)*
     * при их не выпадении в основной игре
     */
    public function testDroppedCoinsInOneSpinInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['countCoinsArr'][0] != 1) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][1] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][2] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][3] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][4] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     * (Dropped coins in one spin)*
     * при их не выпадении в основной игре
     * в версии LifeOfLuxuryService2
     */
    public function testDroppedCoinsInOneSpinInLolSimulation2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['countCoinsArr'][0] != 1) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][1] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][2] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][3] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][4] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     * (Dropped coins in one spin) при выпадении одной монеты в основной игре
     * в версии LifeOfLuxuryService
     */
    public function testDroppedCoinsInOneSpinInLolSimulation3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,10,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['countCoinsArr'][0] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][1] != 1) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][2] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][3] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][4] != 0) {
            $check = false;
        }

        if ($simulationResult['countCoinsArr'][5] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     *(Dropped coins in one spin) при выпадении двух монет в основной игре
     */
     public function testDroppedCoinsInOneSpinInLolSimulation4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,10,3,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsArr'][0] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][1] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][2] != 1) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][3] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][4] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][5] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     * (Dropped coins in one spin) при выпадении одной монет и алмаза в основной игре
     */
     public function testDroppedCoinsInOneSpinInLolSimulation5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,0,3,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsArr'][0] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][1] != 1) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][2] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][3] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][4] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][5] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     * (Dropped coins in one spin) при выпадении трех монет в основной игре
     */
     public function testDroppedCoinsInOneSpinInLolSimulation6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,10,3,3,3,4,10,4,5,5,10,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsArr'][0] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][1] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][2] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][3] != 1) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][4] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][5] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     * (Dropped coins in one spin) при выпадении четырех монет в основной игре
     */
     public function testDroppedCoinsInOneSpinInLolSimulation7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,10,3,3,10,4,10,4,5,5,10,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsArr'][0] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][1] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][2] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][3] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][4] != 1) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][5] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпавших монет в основной игре
     * (Dropped coins in one spin) при выпадении пяти монет в основной игре
     */
     public function testDroppedCoinsInOneSpinInLolSimulation8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,10,3,3,10,4,10,4,5,5,10,10,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsArr'][0] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][1] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][2] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][3] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][4] != 0) {
             $check = false;
         }

         if ($simulationResult['countCoinsArr'][5] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 5 монет
     * (Which led to the bonus game) при их отсутсвии в результате хода
     */
    public function testWhichLedToTheBonusGameInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['countCoinsAndDiamsArr']['5coins'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка кол-ва выпадений фриспинов с 5 монет
     * (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,10,5,5,10,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['5coins'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 4 монет и 1 алмаза
     * (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [0,2,2,10,3,3,10,4,4,10,5,5,10,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['4coinsAnd1Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 3 монет и 2 алмазов
     * (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [0,2,2,0,3,3,10,4,4,10,5,5,10,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['3coinsAnd2Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 2 монет и 3 алмазов
     * (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [0,2,2,0,3,3,0,4,4,10,5,5,10,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['2coinsAnd3Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }


    /**
     * Проверка кол-ва выпадений фриспинов с 4 монет (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,10,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['4coins'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 3 монет и 1 алмаза (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,0,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['3coinsAnd1Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 2 монет и 2 алмазов
     * (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,0,4,4,0,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['2coinsAnd2Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 1 монет и 3 алмазов
     * (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,0,3,3,0,4,4,0,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['1coinsAnd3Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 3 монет (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['3coins'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 2 монет и 1 алмаза (Dropped coins in one spin) при их отсутсвии в результате хода
     */
     public function testWhichLedToTheBonusGameInLolSimulation11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,10,3,3,0,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['2coinsAnd1Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка кол-ва выпадений фриспинов с 1 монет и 2 алмазов
     * (Dropped coins in one spin) при их выпадении
     */
     public function testWhichLedToTheBonusGameInLolSimulation12()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,0,3,3,0,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['countCoinsAndDiamsArr']['1coinsAnd2Diams'] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка минимального кол-ва алмазов выпадавших за одну фриспин игру
     * (Minimum number of diamonds from the freespins game)
     * при прокручивании единственного фриспина в котором не выпадает ни одного алмаза
     */
    public function testMinimumNumberOfDiamondsFromTheFreespinsGameInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,0,3,3,0,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }

        if ($simulationResult['minDiamValueInFreeSpinGame'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка минимального кол-ва алмазов выпадавших за одно фриспин игру
     * (Minimum number of diamonds from the freespins game)
     * при прокручивании единственного фриспина в котором выпадает 10 алмазов
     */
     public function testMinimumNumberOfDiamondsFromTheFreespinsGameInLolSimulation2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,0,3,3,0,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [0,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['minDiamValueInFreeSpinGame'] != 10) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка максимального кол-ва алмазов выпадавших за одно фриспин игру
     * (Maximum number of diamonds from the freespins game)
     * при прокручивании единственного фриспина в котором не выпадает ни одного алмаза
     */
     public function testMaximumNumberOfDiamondsFromTheFreespinsGameInLolSimulation1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,0,3,3,0,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['maxDiamValueInFreeSpinGame'] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Проверка максимального кол-ва алмазов выпадавших за одно фриспин игру
     * (Maximum number of diamonds from the freespins game)
     * при прокручивании единственного фриспина в котором выпадает 10 алмазов
     */
     public function testMaximumNumberOfDiamondsFromTheFreespinsGameInLolSimulation2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [10,2,2,0,3,3,0,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [0,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['maxDiamValueInFreeSpinGame'] != 10) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (2)
     * (Combination statistics) при не выпадении в основной игре
     */
    public function testCombinationStatisticsInLolSimulation1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $simulationResult = $this->getStartSimulationResultForLol();

        $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
        $adminController = new \App\Http\Controllers\Api\AdminController;
        $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        if ($simulationResult['сombinationStats'][1][2] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (3) (Combination statistics) при не выпадении в основной игре
     */
     public function testCombinationStatisticsInLolSimulation2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['сombinationStats'][1][3] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (4) (Combination statistics) при не выпадении в основной игре
     */
     public function testCombinationStatisticsInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['сombinationStats'][1][4] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (5) (Combination statistics) при не выпадении в основной игре
     */
     public function testCombinationStatisticsInLolSimulation4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['сombinationStats'][1][5] != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (2) (Combination statistics) при выпадении в основной игре
     */
     public function testCombinationStatisticsInLolSimulation5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['сombinationStats'][1][2] != 2) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (3) (Combination statistics) при выпадении в основной игре
     */
     public function testCombinationStatisticsInLolSimulation6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['сombinationStats'][1][3] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (4) (Combination statistics) при выпадении в основной игре
     */
     public function testCombinationStatisticsInLolSimulation7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(1, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,1,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['сombinationStats'][1][4] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

    /**
     * Тест подсчета кол-ва выпадения комбинации Plane (5) (Combination statistics) при выпадении в основной игре
     */
     public function testCombinationStatisticsInLolSimulation8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,1,5,5,1,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         if ($simulationResult['сombinationStats'][1][5] != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /*************************************************************************
      * Тесты статистики LifeOfLuxury
      ************************************************************************/

     /**
      * Тест подсчета общего кол-ва спинов при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testTotalSpinsInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего кол-ва спинов при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testTotalSpinsInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }


     /**
      * Тест подсчета общего кол-ва спинов при выпадении выигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testTotalSpinsInLolStats3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего кол-ва спинов при выпадении выигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testTotalSpinsInLolStats4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего кол-ва спинов (Total spins)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * в версии lifeOfLuxury
      */
     public function testTotalSpinsInLolStats5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,10,2,1,3,10,4,4,4,5,5,10,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,1,2,1,3,1,4,4,4,5,5,10,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего кол-ва спинов (Total spins)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * в версии lifeOfLuxury2
      */
     public function testTotalSpinsInLolStats6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,10,2,1,3,10,4,4,4,5,5,10,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,1,2,1,3,1,4,4,4,5,5,10,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва сделанных ставок (Total bet)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testTotalBetInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet != 0.15) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва сделанных ставок (Total bet)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testTotalBetInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet != 0.15) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва сделанных ставок (Total bet)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * в версии lifeOfLuxury
      */
     public function testTotalBetInLolStats3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,10,1,3,3,4,10,4,5,5,5,6,10,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet != 0.15) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва сделанных ставок (Total bet)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * в версии lifeOfLuxury2
      */
     public function testTotalBetInLolStats4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,10,1,3,3,4,10,4,5,5,5,6,10,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet != 0.15) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего выигрыша за все ходы (Total win)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testTotalWinInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего выигрыша за все ходы (Total win)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testTotalWinInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего выигрыша за все ходы (Total win)
      * при выпадении выигрыша в основной игре
      * в вервии lifeOfLuxury
      */
     public function testTotalWinInLolStats3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win != 0.2) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего выигрыша за все ходы (Total win)
      * при выпадении выигрыша в основной игре
      * в вервии lifeOfLuxury2
      */
     public function testTotalWinInLolStats4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win != 0.2) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета общего выигрыша за все ходы (Total win)
      * при выпадении проигрыша в основной игре с фриспинами и их прокруткой
      * с проигрышами
      * в версии lifeOfLuxury
      */
      public function testTotalWinInLolStats5()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

          if ($stat->sum_win != 0.3) {
              $check = false;
          }

          $this->assertTrue($check);
      }

      /**
       * Тест подсчета общего выигрыша за все ходы (Total win)
       * при выпадении проигрыша в основной игре с фриспинами и их прокруткой
       * с проигрышами
       * в версии lifeOfLuxury2
       */
       public function testTotalWinInLolStats6()
       {
           $check = true;
           $this->prepareTest();
           $_SESSION['test'] = true;
           $_SESSION['demo'] = false;
           $_SESSION['next0FSpin'] = false;
           $_SESSION['startFreeSpin'] = false;

           $simulationResult = $this->getStartSimulationResultForLol();

           $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
           $adminController = new \App\Http\Controllers\Api\AdminController;
           $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
           $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

           for ($i=0; $i < 10; $i++) {
               $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
               $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
           }

           $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

           if ($stat->sum_win != 0.3) {
               $check = false;
           }

           $this->assertTrue($check);
       }

       /**
        * Тест подсчета общего выигрыша за все ходы (Total win)
        * при выпадении проигрыша в основной игре с фриспинами и их прокруткой
        * с выигрышами
        * в версии lifeOfLuxury
        */
        public function testTotalWinInLolStats7()
        {
            $check = true;
            $this->prepareTest();
            $_SESSION['test'] = true;
            $_SESSION['demo'] = false;
            $_SESSION['next0FSpin'] = false;
            $_SESSION['startFreeSpin'] = false;

            $simulationResult = $this->getStartSimulationResultForLol();

            $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
            $adminController = new \App\Http\Controllers\Api\AdminController;
            $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

            for ($i=0; $i < 10; $i++) {
                $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
                $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
            }
            $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

            if ($stat->sum_win != 4.3) {
                $check = false;
            }

            $this->assertTrue($check);
        }

        /**
         * Тест подсчета общего выигрыша за все ходы (Total win)
         * при выпадении проигрыша в основной игре с фриспинами и их прокруткой
         * с выигрышами
         * в версии lifeOfLuxury2
         */
         public function testTotalWinInLolStats8()
         {
             $check = true;
             $this->prepareTest();
             $_SESSION['test'] = true;
             $_SESSION['demo'] = false;
             $_SESSION['next0FSpin'] = false;
             $_SESSION['startFreeSpin'] = false;

             $simulationResult = $this->getStartSimulationResultForLol();

             $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
             $adminController = new \App\Http\Controllers\Api\AdminController;
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

             for ($i=0; $i < 10; $i++) {
                 $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
                 $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
             }
             $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

             if ($stat->sum_win != 4.3) {
                 $check = false;
             }

             $this->assertTrue($check);
         }

     /**
      * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
      * при выпадении проигрыша в основной игре
      * в ерсии lifeOfLuxury
      */
     public function testNumberOfWinningSpinsInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_winning_spins != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
      * при выпадении проигрыша в основной игре
      * в ерсии lifeOfLuxury2
      */
     public function testNumberOfWinningSpinsInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_winning_spins != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
      * при выигрыше в основной игре
      * в версии lifeOfLuxury
      */
      public function testNumberOfWinningSpinsInLolStats3()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

          if ($stat->number_of_winning_spins != 1) {
              $check = false;
          }

          $this->assertTrue($check);
      }

      /**
       * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
       * при выигрыше в основной игре
       * в версии lifeOfLuxury2
       */
       public function testNumberOfWinningSpinsInLolStats4()
       {
           $check = true;
           $this->prepareTest();
           $_SESSION['test'] = true;
           $_SESSION['demo'] = false;
           $_SESSION['next0FSpin'] = false;
           $_SESSION['startFreeSpin'] = false;

           $simulationResult = $this->getStartSimulationResultForLol();

           $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
           $adminController = new \App\Http\Controllers\Api\AdminController;
           $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
           $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

           $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

           if ($stat->number_of_winning_spins != 1) {
               $check = false;
           }

           $this->assertTrue($check);
       }

     /**
      * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * c проигрышами
      * в версии lifeOfLuxury
      */
      public function testNumberOfWinningSpinsInLolStats5()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

          if ($stat->number_of_winning_spins != 1) {
              $check = false;
          }

          $this->assertTrue($check);
      }

      /**
       * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
       * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
       * c проигрышами
       * в версии lifeOfLuxury2
       */
       public function testNumberOfWinningSpinsInLolStats6()
       {
           $check = true;
           $this->prepareTest();
           $_SESSION['test'] = true;
           $_SESSION['demo'] = false;
           $_SESSION['next0FSpin'] = false;
           $_SESSION['startFreeSpin'] = false;

           $simulationResult = $this->getStartSimulationResultForLol();

           $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
           $adminController = new \App\Http\Controllers\Api\AdminController;
           $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
           $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

           for ($i=0; $i < 10; $i++) {
               $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
               $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
           }

           $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

           if ($stat->number_of_winning_spins != 1) {
               $check = false;
           }

           $this->assertTrue($check);
       }

       /**
        * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
        * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
        * c выигрышами
        * в версии lifeOfLuxury
        */
        public function testNumberOfWinningSpinsInLolStats7()
        {
            $check = true;
            $this->prepareTest();
            $_SESSION['test'] = true;
            $_SESSION['demo'] = false;
            $_SESSION['next0FSpin'] = false;
            $_SESSION['startFreeSpin'] = false;

            $simulationResult = $this->getStartSimulationResultForLol();

            $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
            $adminController = new \App\Http\Controllers\Api\AdminController;
            $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

            for ($i=0; $i < 10; $i++) {
                $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
                $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
            }

            $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

            if ($stat->number_of_winning_spins != 1) {
                $check = false;
            }

            $this->assertTrue($check);
        }

        /**
         * Тест подсчета кол-ва выигрышных спинов (Number of winning spins)
         * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
         * c выигрышами
         * в версии lifeOfLuxury2
         */
         public function testNumberOfWinningSpinsInLolStats8()
         {
             $check = true;
             $this->prepareTest();
             $_SESSION['test'] = true;
             $_SESSION['demo'] = false;
             $_SESSION['next0FSpin'] = false;
             $_SESSION['startFreeSpin'] = false;

             $simulationResult = $this->getStartSimulationResultForLol();

             $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
             $adminController = new \App\Http\Controllers\Api\AdminController;
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,4,10,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

             for ($i=0; $i < 10; $i++) {
                 $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
                 $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
             }

             $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

             if ($stat->number_of_winning_spins != 1) {
                 $check = false;
             }

             $this->assertTrue($check);
         }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testNumberOfLosingSpinsInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_losing_spins != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testNumberOfLosingSpinsInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_losing_spins != 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выигрыша проигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testNumberOfLosingSpinsInLolStats3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_losing_spins != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выигрыша проигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testNumberOfLosingSpinsInLolStats4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_losing_spins != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * c проигрышами
      * в версии lifeOfLuxury
      */
     public function testNumberOfLosingSpinsInLolStats5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,5,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }
        $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

        if ($stat->number_of_losing_spins != 0) {
            $check = false;
        }

        $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * c проигрышами
      * в версии lifeOfLuxury2
      */
     public function testNumberOfLosingSpinsInLolStats6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,5,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }
        $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

        if ($stat->number_of_losing_spins != 0) {
            $check = false;
        }

        $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * c выигрышами
      * в версии lifeOfLuxury
      */
     public function testNumberOfLosingSpinsInLolStats7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,5,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }
        $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

        if ($stat->number_of_losing_spins != 0) {
            $check = false;
        }

        $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва проигрышных спинов (Number of losing spins)
      * при выпадении выигрыша в основной игре с фриспинами и их прокруткой
      * c выигрышами
      * в версии lifeOfLuxury2
      */
     public function testNumberOfLosingSpinsInLolStats8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,5,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

        for ($i=0; $i < 10; $i++) {
            $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,5,5,5,5,6,6,6]);
            $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
        }
        $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

        if ($stat->number_of_losing_spins != 0) {
            $check = false;
        }

        $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва ходов сделанных в фриспин игре (Number of  free spins)
      * при совершении проигрышного хода в основной игре
      * в версии lifeOfLuxury
      */
     public function testNumberOfFreeSpinsInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва ходов сделанных в фриспин игре (Number of  free spins)
      * при совершении проигрышного хода в основной игре
      * в версии lifeOfLuxury2
      */
     public function testNumberOfFreeSpinsInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва ходов сделанных в фриспин игре (Number of  free spins)
      * при совершении выигрышного хода в основной игре
      * в версии lifeOfLuxury
      */
     public function testNumberOfFreeSpinsInLolStats3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва ходов сделанных в фриспин игре (Number of  free spins)
      * при совершении выигрышного хода в основной игре
      * в версии lifeOfLuxury2
      */
     public function testNumberOfFreeSpinsInLolStats4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва ходов сделанных в фриспин игре (Number of  free spins)
      * при выпадении фриспинов и их прокручивании с проигрышами
      * в версии lifeOfLuxury
      */
     public function testNumberOfFreeSpinsInLolStats5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_bonus_game != 10) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва ходов сделанных в фриспин игре (Number of  free spins)
      * при выпадении фриспинов и их прокручивании с проигрышами
      * в версии lifeOfLuxury2
      */
     public function testNumberOfFreeSpinsInLolStats6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_bonus_game != 10) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых основной игрой (money returned on main game)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testMoneyReturnedOnMainGameInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых основной игрой (money returned on main game)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testMoneyReturnedOnMainGameInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых основной игрой (money returned on main game)
      * при выпадении выигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testMoneyReturnedOnMainGameInLolStats3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game != 0.2) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых основной игрой (money returned on main game)
      * при выпадении выигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testMoneyReturnedOnMainGameInLolStats4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game != 0.2) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых основной игрой (money returned on main game)
      * при выпадении выигрыша в основной игре с фриспинами и их прокручиванием с выигрышами
      * в версии lifeOfLuxury
      */
      public function testMoneyReturnedOnMainGameInLolStats5()
      {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['next0FSpin'] = false;
          $_SESSION['startFreeSpin'] = false;

          $simulationResult = $this->getStartSimulationResultForLol();

          $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
          $adminController = new \App\Http\Controllers\Api\AdminController;
          $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,5,5,5,6,6,6]);
          $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

          for ($i=0; $i < 10; $i++) {
              $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
              $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
          }

          $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

          if ($stat->money_returned_on_main_game != 0) {
             $check = false;
          }

          $this->assertTrue($check);
      }

      /**
       * Тест подсчета денег возвращамых основной игрой (money returned on main game)
       * при выпадении выигрыша в основной игре с фриспинами и их прокручиванием с выигрышами
       * в версии lifeOfLuxury2
       */
       public function testMoneyReturnedOnMainGameInLolStats6()
       {
           $check = true;
           $this->prepareTest();
           $_SESSION['test'] = true;
           $_SESSION['demo'] = false;
           $_SESSION['next0FSpin'] = false;
           $_SESSION['startFreeSpin'] = false;

           $simulationResult = $this->getStartSimulationResultForLol();

           $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
           $adminController = new \App\Http\Controllers\Api\AdminController;
           $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,10,4,4,5,5,5,6,6,6]);
           $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

           for ($i=0; $i < 10; $i++) {
               $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
               $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
           }

           $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

           if ($stat->money_returned_on_main_game != 0) {
              $check = false;
           }

           $this->assertTrue($check);
       }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testMoneyReturnedOnFreespinInLolStats1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении проигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testMoneyReturnedOnFreespinInLolStats2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении выигрыша в основной игре
      * в версии lifeOfLuxury
      */
     public function testMoneyReturnedOnFreespinInLolStats3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении выигрыша в основной игре
      * в версии lifeOfLuxury2
      */
     public function testMoneyReturnedOnFreespinInLolStats4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении выигрыша в фриспин игре с фриспинами
      * и их прокручиванием с проигрынами
      * в версии lifeOfLuxury
      */
     public function testMoneyReturnedOnFreespinInLolStats5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 0.3) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении выигрыша в фриспин игре с фриспинами
      * и их прокручиванием с проигрынами
      * в версии lifeOfLuxury2
      */
     public function testMoneyReturnedOnFreespinInLolStats6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 0.3) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении выигрыша в фриспин игре с фриспинами
      * и их прокручиванием с выигрышами
      * в версии lifeOfLuxury
      */
     public function testMoneyReturnedOnFreespinInLolStats7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 4.3) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета денег возвращамых фриспин игрой (money returned on freespin)
      * при выпадении выигрыша в фриспин игре с фриспинами
      * и их прокручиванием с выигрышами
      * в версии lifeOfLuxury2
      */
     public function testMoneyReturnedOnFreespinInLolStats8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 4.3) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва выигрышных спинов в фриспин игре
      * при выигрышах на каждом ходу
      * в версии lifeOfLuxury
      */
     public function testFreeSpinWinSpinCountInLolSimulation1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinWinSpinCount'] != 10) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва выигрышных спинов в фриспин игре
      * при выигрышах на каждом ходу
      * в версии lifeOfLuxury2
      */
     public function testFreeSpinWinSpinCountInLolSimulation2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinWinSpinCount'] != 10) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва прогирышных спинов в фриспин игре
      * при проигрышах на каждом ходу
      * в версии lifeOfLuxury
      */
     public function testfreeSpinLoseSpinCountInLolSimulation1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }


         if ($simulationResult['freeSpinLoseSpinCount'] != 10) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета кол-ва прогирышных спинов в фриспин игре
      * при проигрышах на каждом ходу
      * в версии lifeOfLuxury2
      */
     public function testFreeSpinLoseSpinCountInLolSimulation2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinLoseSpinCount'] != 10) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета WinSpinPercent в фриспин игре
      * при выигрышах на всех ходах
      * в версии lifeOfLuxury
      */
     public function testFreeSpinsWinSpinPercentInLolSimulation1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinsWinSpinPercent'] != 100) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета WinSpinPercent в фриспин игре
      * при выигрышах на всех ходах
      * в версии lifeOfLuxury2
      */
     public function testFreeSpinsWinSpinPercentInLolSimulation2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinsWinSpinPercent'] != 100) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета WinSpinPercent в фриспин игре
      * при проигрынах на всех ходах
      * в версии lifeOfLuxury
      */
     public function testFreeSpinsWinSpinPercentInLolSimulation3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinsWinSpinPercent'] != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета WinSpinPercent в фриспин игре
      * при проигрынах на всех ходах
      * в версии lifeOfLuxury2
      */
     public function testFreeSpinsWinSpinPercentInLolSimulation4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['freeSpinsWinSpinPercent'] != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест подсчета сombinationStats на последнем фриспине
      */
     public function testСombinationStatsOnLastFreeSpin1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['next0FSpin'] = false;
         $_SESSION['startFreeSpin'] = false;

         $simulationResult = $this->getStartSimulationResultForLol();

         $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
         $adminController = new \App\Http\Controllers\Api\AdminController;
         $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);
         $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);

         for ($i=0; $i < 10; $i++) {
             $result = $gameService->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);
             $simulationResult = $adminController->getResultSimulationLOL($result, $simulationResult);
         }

         if ($simulationResult['сombinationStats'][1][2] != 0) {
            $check = false;
         }

         $this->assertTrue($check);
     }



    /**********************************************************************
     * ДОП ФУНКЦИИ
     *********************************************************************/

    /**
     * Подготовительные рабаты для теста
     */
    private function prepareTest()
    {
        $this->resetSession();
        $this->resetJackpot();
        $this->resetUserJackpot();
        $this->clearTableBridgeApiRequests();
        $this->clearTableStats();
        $this->resetSessionTableInDB();
        $this->clearBridgeApiRequests();
    }

    protected function clearBridgeApiRequests()
    {
        $bridgeApiRequests = (new BridgeApiRequest)->all();
        foreach ($bridgeApiRequests as $key => $value) {
            $value->delete();
        }
    }

    private function clearTableStats()
    {
        $stats = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();
        $stats->iteration_count = 0;
        $stats->sum_bet = 0.00;
        $stats->sum_win = 0.00;
        $stats->number_of_winning_spins = 0.00;
        $stats->number_of_losing_spins = 0.00;
        $stats->number_of_jackpots = 0.00;
        $stats->number_of_bonus_game = 0.00;
        $stats->money_returned_on_main_game = 0.00;
        $stats->money_returned_on_bonus_game = 0.00;
        $stats->money_returned_on_jackpots = 0.00;
        $stats->total_jackpot_winning = 0.00;
        $stats->save();

        $stats = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();
        $stats->iteration_count = 0;
        $stats->sum_bet = 0.00;
        $stats->sum_win = 0.00;
        $stats->number_of_winning_spins = 0.00;
        $stats->number_of_losing_spins = 0.00;
        $stats->number_of_jackpots = 0.00;
        $stats->number_of_bonus_game = 0.00;
        $stats->money_returned_on_main_game = 0.00;
        $stats->money_returned_on_bonus_game = 0.00;
        $stats->money_returned_on_jackpots = 0.00;
        $stats->total_jackpot_winning = 0.00;
        $stats->save();
    }

    /**
    * Создание и подготовка переменных в сессии, которые нужны для работы игры
    */
    private function resetSession()
    {
        $_SESSION['sessionName'] = 1;
        $_SESSION['token'] = 1;
        $_SESSION['userId'] = 1;
        $_SESSION['gameId'] = 1;
        $_SESSION['nickname'] = 1;
        $_SESSION['demo'] = true;
        $_SESSION['platformId'] = 1;
        $_SESSION['freeSpinData'] = false;
        $_SESSION['gameData'] = false;
        $_SESSION['balance'] = 100;
        $_SESSION['allWin'] = 0;
        $_SESSION['reconnect'] = false;
        $_SESSION['freeSpinResultAllWin'] = false;
        $_SESSION['check0FreeSpin'] = false;
        $_SESSION['testFreeSpinPrevAllWin'] = 0;
        $_SESSION['eventID'] = 0;
        $_SESSION['diabloCountInCurrentFreeGame'] = 0;
        $_SESSION['maxDiabloValueInFreeSpinGame'] = 0;
        $_SESSION['minDiabloValueInFreeSpinGame'] = 9999;
    }

    /**
    * Приведение джекпота к моменту когда он должен выпасть на следующем ходу.
    * Изменяются значения только demo джекпота
    */
    private function resetJackpot()
    {
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 10;
        $jackpotDB->result_mini = 39;
        $jackpotDB->minor = 50;
        $jackpotDB->result_minor = 199;
        $jackpotDB->major = 250;
        $jackpotDB->result_major = 499;
        $jackpotDB->big_daddy = 500;
        $jackpotDB->result_big_daddy = 999;
        $jackpotDB->save();
    }

    /**
     * Удаление всех записей из таблицы user_jackpots (не начисленные джекпоты у пользователей)
     */
    private function resetUserJackpot()
    {
        $userJackpots = (new UserJackpot)->all();

        foreach ($userJackpots as $key => $userJackpot) {
            $userJackpot->delete();
        }
    }

    /**
     * Очистка табюлицы bridge_api_requests от созданных в ходе теста записей
     * Записи создаются при $_SESSION['test'] === true
     */
    protected function clearTableBridgeApiRequests()
    {
        $bridgeApiRequests = (new BridgeApiRequest)->all();
        foreach ($bridgeApiRequests as $key => $bridgeApiRequest) {
            $bridgeApiRequest->delete();
        }
    }

    protected function resetSessionTableInDB()
    {
        // удаление всех записей у который uuid === '111122223333'
        $sessions = (new Session)->where('uuid', '=', '111122223333')->get();
        foreach ($sessions as $key => $session) {
            $session->delete();
        }

        // создание сессии для теста
        $session = new Session;
        $session->uuid = '111122223333';
        $session->token = 'token';
        $session->userId = 1;
        $session->gameId = 1;
        $session->nickname = 'nick1';
        $session->demo = 'true';
        $session->platformId = 1;
        $session->status = 1;
        $session->freeSpinData = 'false';
        $session->allWin = 0;
        $session->reconnect = 'false';
        $session->check0FreeSpin = '';
        $session->balance = 100;
        $session->freeSpinMul = 0;
        $session->freeSpinResultAllWin = 'false';
        $session->doubleLose = 'false';
        $session->checkStartDouble = 'false';
        $session->savedStartAllWinForDouble = 'false';
        $session->cardGameIteration = '0';
        $session->dcard = 'false';
        $session->eventID = 'false';
        $session->preAllWinOnRope = 'false';
        $session->linesInGame = '1';
        $session->save();
    }

    private function getStartSimulationResultForLol()
    {
        $simulationResult = [];
        $simulationResult['linesInGame'] = 15;
        $simulationResult['betLine'] = 1;
        $simulationResult['itrX'] = 1000;
        $simulationResult['allWinOnMainLocation'] = 0;
        $simulationResult['freeSpinAllWin'] = 0;
        $simulationResult['allWin'] = 0;
        $simulationResult['countFreeSpin'] = 0;
        $simulationResult['numberOfWinningSpins'] = 0;
        $simulationResult['allBet'] = 0;
        $simulationResult['numberOfLosingSpins'] = 0;
        $simulationResult['allWinOnSlots'] = 0;
        $simulationResult['percentageOfMoneyReturned'] = 0; // общий выигрышь в игре
        $simulationResult['percentageOfMoneyReturnedOnMainGame'] = 0; // общий выигрышь только по слотам
        $simulationResult['freeSpinProbability'] = 0; // попытка подсчитать
        $simulationResult['balance'] = 0;
        $simulationResult['countCoinsArr'] = [0,0,0,0,0,0];
        $simulationResult['countCoinsAndDiamsArr'] = [
            '5coins' => 0,
            '4coinsAnd1Diams' => 0,
            '3coinsAnd2Diams' => 0,
            '2coinsAnd3Diams' => 0,
            '4coins' => 0,
            '3coinsAnd1Diams' => 0,
            '2coinsAnd2Diams' => 0,
            '1coinsAnd3Diams' => 0,
            '3coins' => 0,
            '2coinsAnd1Diams' => 0,
            '1coinsAnd2Diams' => 0,
        ];
        $simulationResult['diamsInMainGame'] = 0;
        $simulationResult['freeSpinDiams'] = 0;
        $simulationResult['spinCount'] = 0;
        // ['символ' => ['кол-во символов' => 'кол-во выбадений', 'кол-во символов', ...], ...]
        $simulationResult['сombinationStats'] = [[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]];
        $simulationResult['maxDiamValueInFreeSpinGame'] = 0;
        $simulationResult['minDiamValueInFreeSpinGame'] = 27;
        $simulationResult['diamsInCurrentFreeSpinGame'] = 0;
        $simulationResult['checkLastFreeSpin'] = false;
        $simulationResult['freeSpinsWinSpinPercent'] = 0;
        $simulationResult['freeSpinWinSpinCount'] = 0;
        $simulationResult['freeSpinLoseSpinCount'] = 0;

        return $simulationResult;
    }
}
