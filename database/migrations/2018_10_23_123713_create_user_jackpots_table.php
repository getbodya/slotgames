<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserJackpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_jackpots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('jackpot_type', ['mini', 'minor', 'major', 'big_daddy']);
            $table->float('jackpot_value',16,4);
            $table->enum('demo', ['false', 'true']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_jackpots');
    }
}
