<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid'); // идентификатор сессии и открытой вкладки
            $table->text('token');
            $table->integer('userId');
            $table->integer('gameId');
            $table->string('nickname');
            $table->string('demo');
            $table->integer('platformId')->nullable();
            $table->boolean('status')->default(false);
            $table->string('freeSpinData');
            $table->string('allWin');
            $table->string('reconnect');
            $table->string('check0FreeSpin');
            $table->string('balance');
            $table->string('freeSpinMul');
            $table->string('freeSpinResultAllWin');
            $table->string('doubleLose');
            $table->string('checkStartDouble');
            $table->string('savedStartAllWinForDouble');
            $table->string('cardGameIteration');
            $table->string('dcard');
            $table->string('eventID');
            $table->string('preAllWinOnRope');
            $table->string('linesInGame');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sessions');
    }
}
