<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('iteration_count')->default(0); // кол-во ходов
            $table->float('sum_bet', 15, 2)->default(0); // сумарная ставка
            $table->float('sum_win', 15, 2)->default(0); // сумарный выигрышь
            $table->integer('number_of_winning_spins')->default(0); // кол-во выигрышных спинов
            $table->integer('number_of_losing_spins')->default(0); // кол-во проигранных спинов
            $table->integer('number_of_jackpots')->default(0); // кол-во джекпотов
            $table->integer('number_of_bonus_game')->default(0);
            $table->float('money_returned_on_main_game', 15, 2)->default(0);
            $table->float('money_returned_on_bonus_game', 15, 2)->default(0);
            $table->float('money_returned_on_jackpots', 15, 2)->default(0);
            $table->float('total_jackpot_winning', 15, 2)->default(0);
            $table->text('data'); // serialize данные для чего-нибудь еще
            $table->enum('demo', ['yes', 'no'])->default('no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats');
    }
}
